<?php
	if (!defined('including')) {
         die('Direct access not premitted');
    }
?>

<h2>About Us</h2>
<table width="95%" height="300" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<p>Welcome to Cyprus Insurances website. We are a dedicated group of insurance agents, some of us with more than 20 years of experience
			in the insurance sector. We are representatives of many major insurance companies in the island. This helps us find the best solution 
			suited for your needs, at the best price. Our motto is <b>'All Should Get a Fair Price'</b> without any discrimination.</p>
			<p> This website currently provides quotes for <i>Car Insurance</i>, as well as motorcycle rates. However our office provides insurances 
			for any other type of vehicle, like Vans, Commercial Own Goods, Commercial General Cartage, Private Buses and Motor Trade. </p>
		</td>
		<td>
			<IMG SRC="<?php echo $clientFilesLocation?>/images/aboutUs.jpg" border=0>
		</td>
	</tr>
</table>
<br>


<h2>How it works</h2>
<table width="95%" height="300" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>	
			<IMG SRC="<?php echo $clientFilesLocation?>/images/howItWorks.jpg" border=0>
		</td>
		<td>
			<p>To get a quote all you have to do is fill a simple form. Our system will search through all available insurance companies and 
			find the best price meeting your requirements. Prices may vary between different insurance companies, but also their services can 
			be of various quality standards. <i>Price should not be the only factor</i> for your decision, but also you should consider the reputation 
			of the company towards its customers when they actually need it. Our agents can assist you with this decision.</p>
			
			<p>We will provide the quote instantly. However, in some cases, further approvals from the insurance companies are required. In these cases,
			we will request additional information from you in order to investigate. The quote we give you is not binding for us. The reason for this
			is that the insurance companies change their rates and policies very often and without notice. For more information please read our 
			<a href="./index.php?action=disclaimer" title="Disclaimer">Disclaimer</a>.
			However we always try to keep our system up to date, and under no circumstances will we charge you without 
			your approval. Our agents will inform you of the final cost, so that you can decide to proceed or not.</p>
		</td>		
	</tr>
</table>


