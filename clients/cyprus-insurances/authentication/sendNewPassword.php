<?php
	
	if (!defined('including')) {
		die('Direct access not premitted');
	}

	$username=$_POST['username'];
	
	/* Check if username exists */
	$result = checkUserNameExists($username);
	
	/*username exists*/
	if($result==1){
		$email = $_SESSION['email'];
		//create a random password of length 8
		$newPassword = generateRandomString(8);
		
		//update password in global database
		include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
		updateUserPassword($username, md5($newPassword));
		
		//return to local client database
		include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";
		
		//email new password to user
		$subject = "New Password";
		$message = "Your new Password is ".$newPassword."\n Please try to login again.";
		sendEmail( $subject, $message, $SYSTEM_EMAIL, $email, $username);
		
		?>
	  	<p>The new password has been sent to you email. <a href="./index.php?action=loginForm" onMouseOver="window.status='Try Again'; return true;" onMouseOut="window.status='';" title="Login" >Login again</a> using your new password. </p>
	  	<?php
	}
	/*username doesn't exist*/
	else{
	  	?>
	  	Username doesn't exist. Please <a href="./index.php?action=forgotPassword" onMouseOver="window.status='Forgot Password'; return true;" onMouseOut="window.status='';" title="Forgot Password" >try again</a>.
	  	<?php
	}
	
?>