<?php
	if (!defined('including')) {
         die('Direct access not premitted');
    }
?>


<h2>Home Fire And Other Hazards</h2>

<br>
		<p>Your home is a very valuable asset for you. Any damage on it can cause severe emotional as well as financial hardship.
		We offer a Comprehensive package that covers your property against any imaginable danger. Our package includes:
		<IMG SRC="<?php echo $clientFilesLocation?>/images/house1.gif" border=0 align=right>
			<ul>				
				<li>Basic Coverage against Fire, Lightning, Explosion</li>
				<li>Additional Coverage against Earthquake or Volcanic Eruption, Strike, Riot, Civil Commotion, Malicious Damage, 
				Bursting or Overflowing of Water or Oil Tank Apparatus or Pipe, Aircraft or other Aerial Device, Hurricane, Cyclone, 
				Tornado, Storm, Tempest, Impact with the Buildings, , Flood or Overflow of the Sea, Falling Trees or Branches</li>
				<li>Coverage for the Building, as well as the Contents</li>
				<li>Optional Coverage for Solar Heaters, Towers and Panels, Radio/Television Aerials, Tents and Kiosks and Machinery in the Open</li>
				<li>Loss of rent</li>
				<li>Valuable Objects</li>
				<li>Coverage against Theft</li>
				<li>Architect's, Surveyors, Consultants and Legal Fees</li>
				<li>Removal of Debris Cost</li>
				<li>Personal Accidents</li>
				<li>Public Liability</li>
				<li>Employer Liability for your Housemaid</li>				
			</ul>	
			<br>
			<p>			
			Our plans are very flexible and adjustable to each persons needs. Our agents will discuss with you and provide you with exactly 
			what you need, nothing more and nothing less. Our main goal is to provide you with the coverage you need, so that if an unfortunate 
			event happens, you will be restored to your previous financial condition.
			<br><br>
			<p>People think that insuring their homes can be expensive. But the truth is that with an amount similar to what you pay for your
			car insurance, you can also insure your entire home. In addition, we offer discounts for our existing customers, 
			as well as for people that have fire extinguishers and fire-prevention systems in their homes.
			<br><br>
			To get a home quote, please fill our <a HREF="<?php echo $_SESSION['globalFilesLocation']?>/documents/QuoteQuestionaireHome.doc" TARGET="_blank">Home Questionnaire Form</a>.<br>
			<br>
			For more information, please <a href="./index.php?action=contactUsForm" title="Contact Us">Contact us</a>. We can setup an appointment
			to find what solution best fits your needs.</p>
<br>

