<?php
	if (!defined('including')) {
         die('Direct access not premitted');
    }
?>

<h2>Types of insurances</h2>
<br>
		Our office provides all types of General Field Insurances, including:

			<ul>
				<li><a href="./index.php?action=homeFireAndThift" title="Home Fire And Thift">Home Fire and other Hazards</a></li>
				<li>Shop Fire and other Hazards</li>
				<li>Medical Insurance for Cyprus residents</li>
				<li>Medical Insurance for non-European workers or students</li>
				<li>Personal Accidents</li>
				<li>Public Liability</li>
				<li>Employer Liability</li>
				<li>Travel Insurance</li>				
				<li>Contractors all Risks</li>
			</ul>	
			<br>
			For more information, please <a href="./index.php?action=contactUsForm" title="Contact Us">Contact us</a>. We can setup an appointment
			to find what solution best fits your needs.</p>
<br>

