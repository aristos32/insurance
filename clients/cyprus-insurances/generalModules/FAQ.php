<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>


<script type="text/javascript">	
/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
function setDefaultValues()
{
	var URL = "<?php echo $_SESSION['location']; ?>";
	Jump(URL);
}

function Jump(URL){
	var NURL = new String(this.location.href + "#" + URL);
	window.location.href = NURL;
}
</script>

<a name="multipartyGuaranteeTreaty" id="multipartyGuaranteeTreaty"></a>
<h3>Q.Do I need to login in order to get a quote? </h3>
<p>A.Yes. But don't worry, creating a new account is very easy and fast. All you need to do is select a username/password
and a valid email address. You can update other information like email and address later on.</p>

<h3>Q.How can I change my password? </h3>
<p>A.First login using your username and password. Then on the right side-menu follow link 'User Data'. 
On the 'User Data' screen, follow link 'Update Password'.</p>

<h3>Q.What is the Multiparty Guarantee Treaty?</h3>
<p>A.The Multiparty Guarantee Treaty is an agreement signed by many European countries. According to this the Insurance Companies
have to provide Third Party coverage to their Customers for injury/death or property damage because of a Motor Vehicle Accident
that happens in any of the members of the treaty. The member countries are : 'Austria', 'Belgium', 'France', 'Germany', 'Denmark', 'Switzerland', 'Greece', 'United Kingdom',
'Italy', 'Ireland', 'Spain', 'Croatia', 'Luxembourg', 'Norway', 'Netherlands', 'Hungary', 'Portugal', 'Slovakia', 'Slovenia', 
'Sweden', 'Czech Republic', 'Finland', 'Estonia', 'Latvia', 'Lithuania', 'Malta', 'Poland', 'Cyprus', 'Iceland', 'Andorra',
'Bulgaria', 'Romania' </p>

<h3>Q.Can I cancel my insurance before it expires? </h3>
<p>A.Yes, you can. If you are leaving from the country, or selling your vehicle to another person, then it is possible to 
cancel your vehicle insurance. You will get a refund amount, that depends on the remaining insurance period, minus any 
administrative fees. In order to cancel it, you need to provide us the original Insurance Certificate.</p>

<h3>Q.Can I update my personal information, like address or email? </h3>
<p>A.Yes, you can. First you need to login in the site. Then on the right sidebar, follow the link 'User Data'. Update your data
and press 'Update'</p>

<h3>Q.Can I view the previous quotes I requested? </h3>
<p>A.Yes, you can. First you need to login in the site. Then on the right sidebar, follow the link 'Previous Quotes'.</p>

<h3>Q.What are they types of coverage for Vehicles? </h3>
<p>A.The following three: 
'Third Party', 'Third Party Fire and Theft' and 'Comprehensive'</p>

<h3>Q.What is the 'Third Party' coverage? </h3>
<a name="thirdPartyCoverage" id="thirdPartyCoverage"></a>
<p>A.'Third Party' means persons other than the driver and his passengers. According to the 'Motor Vehicle Law', every person that owns
a vehicle is required to have coverage for death/injury and property damage against Third Party.</p>

<h3>Q.What does the 'Third Party Fire an Theft' cover? </h3>
<a name="thirdPartyFireAndTheftCoverage" id="thirdPartyFireAndTheftCoverage"></a>
<p>A.'Third Party Fire and Theft' offers the same coverages as 'Third Party' and additionaly it offers coverage for the insured vehicle 
for loss or damage from fire, theft or attempt of theft.</p>

<h3>Q.What does the 'Comprehensive' cover? </h3>
<a name="thirdPartyFireAndTheftCoverage" id="thirdPartyFireAndTheftCoverage"></a>
<p>A.'Comprehensive' offers the same coverages as 'Third Party Fire and Theft' and additionaly it offers coverage for the insured vehicle 
for loss or damage.</p>

<h3>Q.What is a 'Cover Note'? </h3>
<a name="coverNote" id="coverNote"></a>
<p>A.Cover Note is a temporary Insurance Certificate. It is provided until the Insurance Certificate is prepared, and it
is a proof that there is a valid insurance coverage. It should be shown to the authorities, if requested.</p>

<h3>Q.Can I setup an appointment in a location other than WWW.CYRPUS-INSURANCES.COM offices? </h3>
<a name="premises" id="premises"></a>
<p>A.Yes, you can. We offer this on-site service for some big companies or villages, in order to facilitate our customers that
cannot come to our offices in Limassol. One of our agents will meet you in your location in order to fill the Insurance Proposal 
Forms and explain you the Terms & Benefits.
Please request for this service, to check if your location is currently available. </p>


<h3>Q.What is the telephone number of my Road Assistance? </h3>
<a name="coverNote" id="coverNote"></a>
<p>A.Each insurance company works with a different Road Assistance Company. We attach the Road Assistance information with 
your Insurance Certificate. You should have this paper always in the car, in case of emergency.</p>


<h3>Q.Where can I find the cubic capacity of my vehicle? </h3>
<a name="cubicCapacity" id="cubicCapacity"></a>
<p>A.Cubic Capacity is specified in the Vehicle Registration Document.</p>

<h3>Q.What does the 'Named Drivers Number' mean, for a Comprehensive Coverage Type? </h3>
<a name="namedDriversNumber" id="namedDriversNumber"></a>
<p>A.For a Comprehensive Coverage only named drivers are allowed. Depending on the insurance company, there is usually a maximum
allowed number, for example 5 drivers. If the 'Maximum Allowed' option is selected, no discount will be applied. If any other option 
is selected that limits the number of the drivers, a corresponding discount will be applied.</p>

<h3>Q.What does the 'Additional Excess' mean, for a Comprehensive Coverage Type? </h3>
<a name="additionalExcess" id="additionalExcess"></a>
<p>A.For a Comprehensive Coverage an Excess Amount is applied, which the Customer has to pay.
By increasing this amount, a discount will be applied.</p>

<h3>Q.What does the 'No Claim Discount' mean, for a Comprehensive Coverage Type? </h3>
<a name="noClaimDiscountYears" id="noClaimDiscountYears"></a>
<p>A.The Customer needs to select the number of years without any claims. The higher the number of years,
the higher discount will be applied.</p>
			

<h3>Q.Why do you need my company name? </h3>
<a name="ownerCompany" id="ownerCompany"></a>
<p>A.We offer a service for employees of big companies. Our agents can come to your company location and arrange all insurance papers for you.
We ask for your company name in order to improve our service and check if your company is already covered by it. This information will only be 
used if you purchase an insurance policy from us, in order to know your location.</p>