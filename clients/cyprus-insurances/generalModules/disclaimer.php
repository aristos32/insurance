<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>

<h2>Disclaimer</h2>
<p>This website is not binded to any specific insurance company. We try to get the best deal for our customers.
We analyze our customers needs, and select the appropriate offer that better matches them. Currently we get quotes from
4 different insurance companies, but we have no legal or financial obligation to any of these. </p>

<p> We try our best to keep our rates database up to date all the time. The way this work is : we get the latest rates from all the insurance 
companies and we insert them in our system. Our system is fully automated. When you request for a quote it will match 
your requested coverages with the coverages offered by each insurance company. Based on the best rates and availability of coverages, it will
select the QUOTE provided to you.
</p>

<p>  The rates we provide are the closest to the actual rates, as far as we are aware of. But <b>they are not binding</b>, 
until the insurance companies verify them. The reason is that the insurance companies often change their rates, or entire policies without prior notice. As a result our system
cannot provide final rates, unless the insurance companies verify this.

<p>In case the rate we provide in this site is different than the actual rate( can be lower as well as higher ), we will not proceed 
unless we notify you and obtain your approval. If any money have been paid and you do no wish to proceed with the new rate, we will
offer a full refund of your money. Also if the insurance company doesn't want to cover our clients, we again will provide
a full refund.</p>

<p>For any further clarifications, please don't hesitate to <a href="./index.php?action=contactUsForm" title="Contact Us">Contact Us</a>. 

<p>Thank you



				