<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
	
	
?>


<script type="text/javascript">	
/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
function setDefaultValues()
{
	var URL = "<?php echo $_SESSION['location']; ?>";
	Jump(URL);
}

function Jump(URL){
	var NURL = new String(this.location.href + "#" + URL);
	window.location.href = NURL;
}
</script>


<table id="glossary">

	<!-- COVERAGE TYPE -->
	<tr>
      <td>
          <a name="coverageType" id="coverageType"></a>
          <h2><?php echo $_SESSION['coverageTypeTab'] ?></h2>
      </td>
    </tr>                           
    <?php
		$i = 1;
		foreach($_SESSION['coverageTypeTabOptions'] as $option){
			?>
			<tr>
			<td class="label"><?php echo "$i - $option->name" ?>: <?php echo $option->description; ?> </td>
			 </tr>
			 <?php
			 $i = $i + 1;
		 }
		 
				 
	?> 	
	
	<!-- COUNTRY OF DRIVING LICENSE -->
	<tr>
      <td>
          <a name="licenseCountry" id="licenseCountry"></a>
          <h2><?php echo $_SESSION['countryOfLicense'] ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">The country of the current license. If someone has a european
		driving license and has changed it to Cypriot Driving License, she should select 'Cyprus'. 
		</td>
	</tr>	
	
	<!-- COVERAGE AMOUNT -->
	<tr>
      <td>
		<a name="coverageAmount" id="coverageAmount"></a>
        <h2><?php echo $_SESSION['coverageAmountTab'] ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">The current value of the vehicle, as estimated by the owner. Should not
		be significantly higher or lower than the current market value, otherwise the insurance request may be rejected.
		</td>
	</tr>		
	
	<!-- DISABILITY -->
	<tr>
      <td>
          <a name="hasDisability" id="hasDisability"></a>
          <h2><?php echo $_SESSION['hasDisabilityTab'] ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">Any form of disabily that may affect the driving ability.
		Some examples are : loss of limp, reduced hearing, reduced sight, or other disease etc.
		</td>
	</tr>
		
	<!-- DEATH COVERAGE -->
	<tr>
      <td>
          <a name="deathOfInsuredDriverAmount" id="deathOfInsuredDriverAmount"></a>
          <h2><?php echo $_SESSION['deathOfInsuredDriver'] ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">Applies to the insurance owner, or to any other 
		authorized person driving the car.
		</td>
	</tr>	
		
	<!-- EXCESS AMOUNT-->
	<tr>
       <td>
          <a name="excessAmount" id="excessAmount"></a>
          <h2><?php echo $_SESSION['excessAmount'] ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">Excess Amount is the initial amount the Customer has to pay, before the insurance pays.
		For example, a Comprehensive Insurance has an Excess of &euro;500. The Customer causes an accident of &euro;1200 damage in his vehicle.
		In this case, the customer will
		pay the first &euro;500 and the insurance company will pay the remaining &euro;700. The Excess usually applies to the Comprehensive Coverage Type.
		</td>
	</tr>	
	
	<!-- LEARNERS LICENCE FOR ADDITIONAL DRIVERS  -->
	<tr>
      <td>
          <a name="hasLearnersLicence" id="hasLearnersLicence"></a>
          <h2><?php echo $_SESSION['hasLearnersLicenceTab'] ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">This applies only for the additional named drivers. If the owner of the insurance
		has a learners driving license, please specify this in the additional information.
		</td>
	</tr>
	
	
	<!-- MEDICAL EXPENSES -->
	<tr>
      <td>
          <a name="medicalExpensesAmount" id="medicalExpensesAmount"></a>
          <h2><?php echo $_SESSION['medicalExpenses']?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">Covers medical expenses of authorized driver 
		that had an accident while driving the vehicle. Common amounts covered are 200, 300, 500 euro. Please insert 
		the coverage amount you want, and we will select the closest from the available.
		</td>
	</tr>	
		
	<!-- MAXIMUM PENALTY POINTS OF ANY DRIVER DURING LAST 3 YEARS-->
	<tr>
		<td>
		<a name="maxPenaltyPointsOfAnyDriverDuringLastThreeYears" id="maxPenaltyPointsOfAnyDriverDuringLastThreeYears"></a>
        <h2><?php echo $_SESSION['penaltyPointsDuringLast3Years'] ?></h2>
		</td>
    </tr>                           
	<tr>
		<td class="label">The maximum number of Penalty Points of the owner or any of the 
		named drivers during the last 3 years. This can be found at the back of the driving license.
		</td>
	</tr>
	
	<!-- NO ADDITIONAL CHARGE BECAUSE OF ACCIDENT COVERAGE -->
	<tr>
      <td>
          <a name="noChargeDueToAccident" id="noChargeDueToAccident"></a>
          <h2><?php echo $_SESSION['noChargeDueToAccidentTab'] ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">If the insured driver has an accident, he will not have 
		any additional charge when renewing the insurance.
		</td>
	</tr>
	
	<!-- NORMAL DRIVING LICENSE YEARS -->
	<tr>
      <td>
          <a name="normalDrivingLicenseTotalYears" id="normalDrivingLicenseTotalYears"></a>
          <h2><?php echo $_SESSION['normalDrivingLicenseTotalYears']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label"><?php echo $_SESSION['normalDrivingLicenseTotalYearsDescription']; ?>
		</td>
	</tr>	
			
	<!-- PASSENGER LIABILITY -->
	<tr>
      <td>
          <a name="passengerLiability" id="passengerLiability"></a>
          <h2><?php echo $_SESSION['passengerLiabilityTab']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">Covers property damage,death or bodily injury of a third party, that is caused
		by the passengers of the vehicle.
		</td>
	</tr>		
	
	<!-- PERSONAL ACCIDENTS -->
	<tr>
      <td>
          <a name="personalAccidentsAmount" id="personalAccidentsAmount"></a>
          <h2><?php echo $_SESSION['personalAccidents']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">Covers personal accidents for the driver of the vehicle. Each
		insurance company has different default coverage amounts. Common amounts covered are &euro;5000, &euro;10000, &euro;15000 or &euro;20000. Please insert the coverage amount you want, and we will select the closest from the available.
		</td>
	</tr>	
		
	<!-- HAS PREVIOUS CYPRIOT INSURANCE -->
	<tr>
      <td>
          <a name="hasPreviousCypriotInsurance" id="hasPreviousCypriotInsurance"></a>
          <h2><?php echo $_SESSION['previousCypriotInsuranceTab']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">The owner had an insurance with a Cypriot Insurance Company
		for a vehicle of the same type as the current one. For some companies this is a criterion of better driving 
		experience, especially for foreign drivers.
		</td>
	</tr>
	
	<!-- ROAD ASSISTANCE  -->
	<tr>
      <td>
          <a name="roadAssistance" id="roadAssistance"></a>
          <h2><?php echo $_SESSION['roadAssistanceTab']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label"> Road assistance is meant to help you when you run into any vehicle problems. 
		It is usually offered for vehicles of less than 3 tones of weigh. Many insurance companies include the Road Assistance in their
		basic packages.
		<br><i>The Road assistance will offer the following services free of charge:</i>
		<li>change of tyres</li>
		<li>refill of tank(fuel is charged to the vehicle owner)</li>
		
		<i>In the event of an accident the Road Assistance will:</i>
		<li>take photos of the scene</li>
		<li>file the insurance claim on the owners behalf</li>
		<li>carry the vehicle to the owners mechanic garage</li>
		</td>
	</tr>
	
	<!-- PACKAGE OF BENEFITS -->
	<tr>
      <td>
          <a name="packageOfBenefits" id="packageOfBenefits"></a>
          <h2><?php echo $_SESSION['findPackages']; ?></h2>
      </td>
    </tr>  
  <tr>
		<td class="label"> Insurance Companies offer Packages of Benefits for better coverage. These package include many 
		benefits together, in a better final price for the client. Some companies only offer Packages, and cannot have individual benefits per client request.
		<br><i>Typical Packages may include:</i>
		<li>Road Assistance</li>
		<li>Accident Care</li>
		<li>Passanger Liability</li>
		<li>Driving other Vehicles of the same type</li>
		<li>Any driver 23-70 with 2 years normal driving license</li>
		<li>Windscreen Cover</li>
		<li>Personal Accidents</li>
		</td>
	</tr>   
	 
	<!-- SPORTS MODEL -->
	<tr>
      <td>
		<a name="sportsModel" id="sportsModel"></a>
        <h2><?php echo $_SESSION['vehicleSportsModel']; ?></h2>
      </td>
    </tr>  
    <tr>
	  <td class="label">A small low car with a high-powered engine; usually seats two persons
	  </td>
	</tr>     
		
	<!-- STEERING WHEEL SIDE -->
	<tr>
      <td>
          <a name="steeringWheelSide" id="steeringWheelSide"></a>
          <h2><?php echo $_SESSION['steeringWheelSideTab']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">In Cyprus the steering wheel of the vehicles is on the right side. For vehicles
		 from abroad, where the steering wheel is on the left side, an additional charge is applied.
		</td>
	</tr>
			
	<!-- TOW A TRAILER -->
	<tr>
      <td>
          <a name="trailerLiability" id="trailerLiability"></a>
          <h2><?php echo $_SESSION['towTrailer']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">A trailer attached or not to a motor vehicle is also considered
		a vehicle for insurance purposes. So the trailer should always have valid insurance coverage, even in the cases
		that it is not attached to the motor vehicle.
		</td>
	</tr>				
		
	<!-- DELIVERIES -->
	<tr>
      <td>
          <a name="isUsedForDeliveries" id="isUsedForDeliveries"></a>
          <h2><?php echo $_SESSION['usefForDeliveries']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label">The vehicle is used for daily deliveries of goods, for a fee. Some
		 examples are: delivering milk to supermarkets, delivering pizza to houses etc.
		</td>
	</tr>	
	
	<!-- VEHICLE TYPE -->
    <tr>
      <td>
          <a name="vehicleTypes" id="vehicleTypes"></a>
          <h2><?php echo $_SESSION['vehicleTypeTab']; ?></h2>
      </td>
    </tr>                           
    <?php
		$i = 1;
		foreach($_SESSION['vehicleTypeTabOptions'] as $option){
			?>
			<tr>
			<td class="label"><?php echo "$i - $option->name" ?>: 
				<?php echo "$option->description" ?>
			</td>
			</tr>
			<?php
			$i = $i + 1;
		 }
	?>
	
	<!-- VEHICLE DESIGN -->
	<tr>
      <td>
          <a name="vehicleDesign" id="vehicleDesign"></a>
          <h2><?php echo $_SESSION['vehicleDesignTab']; ?></h2>
      </td>
    </tr>                           
    <?php
		$i = 1;
		foreach($_SESSION['vehicleDesignTabOptions'] as $option){
			?>
			<tr>
			<td class="label"><?php echo "$i - $option->name" ?>: 
				<?php echo "$option->description" ?> 
			</td>
			</tr>
			<?php
			$i = $i + 1;
		}
	?> 
	
	<!-- WINDSHIELD-GLASSES COVERAGE -->
	<tr>
      <td>
          <a name="windshieldAmount" id="windshieldAmount"></a>
          <h2><?php echo $_SESSION['windshieldCoverage']; ?></h2>
      </td>
    </tr>                           
	<tr>
		<td class="label"><!--&nbsp;&nbsp;&nbsp;&nbsp;-->Covers damage on the windshield or any of the windows 
		of the vehicle, when there is not other damage to the vehicle. This may happen due to a random incident, for 
		example a small rock thrown to the windshield from another vehicle. Each
		insurance company has different default coverage amounts. Common amounts covered are &euro;250, &euro;350 or &euro;500. Please insert 
		the coverage amount you want, and we will select the closest from the available.
		</td>
	</tr>
			
</table>

<h2></h2>