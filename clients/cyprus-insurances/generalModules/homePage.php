<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>

	<div id="wrap">
			
	<div id="upper_side" align=left>
				<h2>Home Insurance</h2>
				<a href="./index.php?action=homeFireAndThift"><IMG SRC="<?php echo $clientFilesLocation?>/images/house2.jpg" border=0 align=right width=350 height=250></a>
				<p align=left>
				Your home is probably the most valuable asset for you. Any damage on it can cause severe emotional as well as financial hardship.
				<p>We offer a Comprehensive package that covers your property against any imaginable danger. Our packages are very flexible and 
				can be adjusted right on your needs. Our goal is to satisfy your needs, and not offer you unnecessary coverages.
				<p>We provide coverages for the Building as well as for the Contents, against Burglary, Earthquake, Flooding and many more. 
				<p>People think that insuring their homes can be expensive. But the truth is that with an amount similar to what you pay for your
				car insurance, you can also insure your entire home.
				<a href="./index.php?action=homeFireAndThift"><font color=red size=1.5em>Learn More...</font></a>
				
				
		</div>
		
		<div class="clear"></div>
		
		<div id="left_side" align=center>
				<h2>Our Special Packages For Vehicles</h2>
				<a href="./index.php?action=specialOffers"><IMG SRC="<?php echo $clientFilesLocation?>/images/specialOffer.jpg" border=0 align=right></a>
				<p align=left>We offer several different packages of benefits. We are sure that one of them will fit YOUR needs!
				Please check our:
				<ul>
					<li><a href="./index.php?action=specialOffers">Car Special Offers</a></li>
					<!-- <li><a href="./index.php?action=motorCycleQuotes">Motorcycle quotes</a></li> -->
				</ul>
		</div>
		
		<div id="right_side" align=center>
			<h2>Get a Quote Instantly!</h2>
				<a href="./index.php?action=carQuoteForm"><IMG SRC="<?php echo $clientFilesLocation?>/images/getQuoteSmaller.jpg" border=0 align=left></a>
				<p align=left>We can provide you with a Quote Instantly! All you need is to <a href="./index.php?action=carQuoteForm">fill</a> a simple form.
				<a href="./index.php?action=howToGetAQuote"><font color=red size=1.5em>Read More...</font></a>
				<br>This service is available only for the site members. However, <a href="./index.php?action=newUser">registration</a> to our site is fast,
				easy and free! <a href="./index.php?action=newUser"><font color=red size=1.5em>Learn More...</font></a>
		</div>
		
		<div class="clear"></div>
		
	</div>