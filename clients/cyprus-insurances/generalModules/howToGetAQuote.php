<?php
	if (!defined('including')) {
         die('Direct access not premitted');
    }
?>

<h2>How to get a quote</h2>
<table width="95%"  border="0" cellpadding="0" cellspacing="0">
	<tr>
		
		<td>
			<p><b>In order to get a quote:</b>
			<ol>
			<li>Login in the system</li>
			<li>Follow the <a href="./index.php?action=carQuoteForm" title="Vehicle Quote">Vehicle Quote</a> link</li>
			<li>Fill all data and Get the Quote</li>
			<li>Proceed with <a href="./index.php?action=howToPurchaseInsurance" title="Purchase">Purchase</a> </li>
			</ol>
			</p>
			
			<p> <b>Cannot get an online quote? No problem! </b> <br>There are some specific cases, when our automated system cannot provide a quote for you. For these cases we 
			have two alternate methods for providing you with a quote:
			<ul>
				<li> Fill our <a HREF="./documents/QuoteQuestionaireVehicles.doc" TARGET="_blank">Questionnaire Form</a>
 				and send it via <a href="./index.php?action=contactUsForm" title="Contact Us">Fax</a> or email in <font color=blue>aristos@cyprus-insurances.com</font>. 
 				</li>
 				<li> OR, send us copies via <a href="./index.php?action=contactUsForm" title="Contact Us">Fax</a> of:
 					<ul>
						<li>your driving license</li>
						<li>any previous vehicle insurance</li>
						<li>the vehicle title(if you don't have it, we need the type, cubic capacity, manufacture year, model)</li>
						<li>write 'Cyprus-Insurances.com' and the quote id(if any) in one of the copies</li>
					</ul>
				</li>
			</ul>
			<br>
			<i>We are commited to respond to you within one working day.</i>
		</td>
		
	</tr>
</table>

<br>


			