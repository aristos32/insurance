<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>


<h2>How to purchase an insurance</h2>
<p>
Purchasing a new insurance from Cyprus-Insurances.com is very simple.


<ol>
	<li>Sends us copies of:
		<ul>
			<li>your driving license(both sides)</li>
			<li>any previous vehicle insurance</li>
			<li>the vehicle title(if you don't have it, we need the type, cubic capacity, manufacture year, model)</li>
			<li>your ID or passport</li>
			<li>write the quote id you want in one of the copies</li>
		</ul>
	You can send this by <a href="./index.php?action=contactUsForm" title="Contact Us">fax</a>, <a href="./index.php?action=contactUsForm" title="Contact Us">email</a> or 
	drop them on person in our <a href="./index.php?action=contactUsForm" title="Contact Us">office</a>.</li>
	<li>We will process your request, and verify the final price.</li>
	<li>One of our agents will contact you via email in order to get your final approval.</li>
	<li>Arrange an appointment in our <a href="./index.php?action=contactUsForm" title="Contact Us">offices</a>, or in your <a href="./index.php?action=FAQ&location=premises">premises</a>. 
	Our agent will explain all the terms of the Agreement and you will sign the Insurance Proposal.</li>
	<li>Pay the full insurance amount on person , or by money transfer.</li>
	<li>We will immediately provide you with a <a href="./index.php?action=FAQ&location=coverNote">Cover Note</a>. We can send this via fax or mail.</li>
	<li>When your insurance is ready, we will mail it to your address. No need to come pick it up.</li>
</ol>

</p>


