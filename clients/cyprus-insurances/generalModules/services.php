<?php
	if (!defined('including')) {
         die('Direct access not premitted');
    }
?>

<h2>Services</h2>
<table width="90%" height="300" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			Our office offers many services, including:
			<ul>
				<li><b>Driving lessons :</b> We have two experienced driving instructors, and we offer lessons
				in manual and automatic cars as well as for all other types of vehicles. Our cars our air-conditioned.</li>
				<li><b>Driving Test:</b> We send the driving test for you, test for learners, also offer fast-test services.
				<li><b>Road Tax Renewal</b></li>
				<li><b>Learners License Renewal(VISA holders or not-VISA holders)</b></li>
				<li><b>Vehicle Title Transfers</b></li>
				<li><b>MOT service :  </b>We can take your vehicle for MOT service.</li>
			</ul> 
		</td>
		<td>
			<IMG SRC="<?php echo $clientFilesLocation?>/images/service.gif" border=0>
			
		</td>
	</tr>
</table>
