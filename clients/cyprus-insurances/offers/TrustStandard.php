<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>

<h2>Standard</h2>

<table width="90%" border="0" cellspacing="1" cellpadding="3">
	
	<tr>
		<td><b>Requirements</b></td><td>&nbsp;</td><td><b></b></td><td></td>
	</tr>
	<tr>
		<td>+ Car up to 1600 CC</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Drivers over 22 years old</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ No previous accidents</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	<tr>
		<td><b>Inclusive Benefits</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Third party Coverage</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Beyond the Road</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ 24 hours Road Assistance</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ 24 hours Accident Care</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Driving other cars of the same type</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>	
	<tr>
		<td>+ Anyone over 23, under 70 with license more than 2 years can drive this car(for the third party coverage only)</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Third party liability during loading/unloading up to €500</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Passenger Liability</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>	
	<tr>
		<td>+ Windshield coverage up to €500</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Tow a trailer</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>	
	<tr>
		<td>+ Personal Accidents for Insured driver and spouse of €5,000</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	
	
</table>
These offers can easily be adjusted depending on your age,cubic capacity of you car etc.<br>
<font color=red>To get an exact quote that fits your requirements, please fill our <a HREF="<?php echo $_SESSION['globalFilesLocation']?>/documents/QuoteQuestionaireVehicles.doc" TARGET="_blank">Questionnaire Form</a>.</font>
<br><br>
<INPUT TYPE="button" VALUE="  Back   " onClick="history.go(-1);return true;">
