<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>

<h2>Basic</h2>

<table width="90%" border="0" cellspacing="1" cellpadding="3">
	
	<tr>
		<td><b>Requirements</b></td><td>&nbsp;</td><td><b></b></td><td></td>
	</tr>
	<tr>
		<td>+ Driver over 23 with license over 2 years</td><td></td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Cypriot or EU driving license</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Saloon Vehicle under 1600cc, under 15 years old</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Driver <font color=red> does not have </font> previous insurance in Cyprus</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ No previous accidents</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	<tr>
		<td><b>Inclusive Benefits</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Third party Coverage</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Beyond the Road</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ 24 hours Road Assistance</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Accident Care</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Medical Expenses of insured driver up to €200</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Tow a trailer</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Driving other cars of the same type</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Windshield coverage up to €350</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Passenger Liability</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Anyone over 23, under 70 with license more than 2 years can drive this car</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>					
	<tr>
		<td>+ Death of insured driver from accident €10000</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	
	
</table>

These offers can easily be adjusted depending on your age,cubic capacity of you car etc.
<br><br>
<INPUT TYPE="button" VALUE="  Back   " onClick="history.go(-1);return true;">

