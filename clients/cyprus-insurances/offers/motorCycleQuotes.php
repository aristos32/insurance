<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>
<h1>Our Rates for Motorcycles</h1>


<table width="95%" border="1" cellspacing="1" cellpadding="3">
	
	<tr align=center>
		<td width="30%"></td><td width="20%"></td><td colspan=2>Cypriots</td><td colspan=2>Non-Cypriots</td>
	</tr>
	<tr align=center>
		<td><b>Cubic Capacity</b></td><td><b>Age</b></td><td width="10%"><b>Cost(�)<sup>*</sup></b></td><td width="10%"><b>+15%(�)<sup>**</sup></b></td><td width="10%"><b>Cost(�)<sup>*</sup></b></td><td width="10%"><b>+15%(�)<sup>**</sup></b></td>
	</tr>
	<tr align=center>
		<td>up to 50</td><td>18-19</td><td>81</td><td>87</td><td>91</td><td>97</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>20-22</td><td>71</td><td>77</td><td>81</td><td>87</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>23-25</td><td>61</td><td>67</td><td>71</td><td>77</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>over 25</td><td>51</td><td>57</td><td>61</td><td>67</td>
	</tr>
	
	<tr align=center>
		<td>over 50, under 101</td><td>18-19</td><td>126</td><td>136</td><td>143</td><td>152</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>20-22</td><td>110</td><td>119</td><td>126</td><td>136</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>23-25</td><td>93</td><td>103</td><td>110</td><td>119</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>over 25</td><td>77</td><td>87</td><td>93</td><td>103</td>
	</tr>
	
	<tr align=center>
		<td>over 100, under 226</td><td>18-19</td><td>171</td><td>185</td><td>194</td><td>208</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>20-22</td><td>148</td><td>162</td><td>171</td><td>185</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>23-25</td><td>126</td><td>139</td><td>148</td><td>162</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>over 25</td><td>103</td><td>116</td><td>126</td><td>139</td>
	</tr>
	
	<tr align=center>
		<td>over 225, under 451</td><td>21-22</td><td>212</td><td>232</td><td>245</td><td>265</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>23-25</td><td>178</td><td>198</td><td>212</td><td>232</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>over 25</td><td>145</td><td>165</td><td>178</td><td>198</td>
	</tr>
	
	<tr align=center>
		<td>over 450, under 601</td><td>21-22</td><td>276</td><td>303</td><td>321</td><td>346</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>23-25</td><td>232</td><td>259</td><td>276</td><td>303</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>over 25</td><td>188</td><td>214</td><td>232</td><td>259</td>
	</tr>
	
	<tr align=center>
		<td>over 600</td><td>21-22</td><td>379</td><td>415</td><td>440</td><td>477</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>23-25</td><td>317</td><td>354</td><td>379</td><td>415</td>
	</tr>
	<tr align=center>
		<td>&nbsp;</td><td>over 25</td><td>256</td><td>293</td><td>317</td><td>354</td>
	</tr>
</table>

<br>
* = normal price when motorcycle is less than 20 years old. <br>
** = price when motorcycle is more than 20 years old.
<br>
<input type="button" value="Print this page" onclick="printpage()" />