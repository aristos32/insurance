<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>
<h1>Our Special Car Packages For UK Citizens</h1>
<p><IMG SRC="<?php echo $clientFilesLocation?>/images/specialOffer.jpg" border=0 align=right> Please check our following offers for UK Citizens:

<table width="80%" border="0" cellspacing="1" cellpadding="3" align=left>
	
	<tr>
		<td>&nbsp;</td>
	</tr>
	
	<tr>
		<td><b>'Standard' - �226</b></td>
	</tr>
	<tr>
		<td>Provides a bundle of benefits for Third Party Coverage</td>
		<td width="20%"> <a href="./index.php?action=TrustStandard"><font color=red size=1.5em>Learn More...</font></a></td>
	</tr>

	<tr>
		<td>&nbsp;   </td>
	</tr>
	
	<tr>
		<td><b>'Comprehensive' - Starting from �300</b></td>
	</tr>
	<tr>
		<td>Provides Third Party as well as Own Vehicle Damages, with many more benefits.</td>
		<td width="20%"> <a href="./index.php?action=TrustExecutive"><font color=red size=1.5em>Learn More...</font></a></td>
	</tr>

		
	<tr>
		<td>&nbsp;   </td>
	</tr>
	
	<tr>
		<td><b>'Comprehensive Plus' - Starting from �300</b></td>
	</tr>
	<tr>
		<td>Provides a complete bundle of benefits for any imaginable danger.</td>
		<td width="20%"> <a href="./index.php?action=TrustExecutivePlus"><font color=red size=1.5em>Learn More...</font></a></td>
	</tr>
	
	<tr>
		<td>&nbsp;   </td>
	</tr>
	
	<tr>
		<td><font color=red>Didn't find what you want? To get an exact quote that fits your requirements, please fill our <a HREF="<?php echo $_SESSION['globalFilesLocation']?>/documents/QuoteQuestionaireVehicles.doc" TARGET="_blank">Questionnaire Form</a>.</font>
		</td>
	</tr>	
		
</table>

