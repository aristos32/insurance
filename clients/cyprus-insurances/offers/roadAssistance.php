<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>

<h2>Road Assistance</h2>

<table width="90%" border="0" cellspacing="1" cellpadding="3">
	
	<tr>
		<td><b>Requirements</b></td><td>&nbsp;</td><td><b></b></td><td></td>
	</tr>
	<tr>
		<td>+ Driver over 23 with license over 2 years</td><td></td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Saloon Vehicle under 1600cc, under 15 years old</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ No previous accidents</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	<tr>
		<td><b>Inclusive Benefits</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Third party Coverage</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ Beyond the Road</td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ 24 hours Road Assistance </td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	<tr>
		<td>+ 24 hours Accident Care </td><td>&nbsp;</td><td></td><td>&nbsp;</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
	
	
</table>

These offers can easily be adjusted depending on your age,cubic capacity of you car etc.<br>
<font color=red>To get an exact quote that fits your requirements, please fill our <a HREF="<?php echo $_SESSION['globalFilesLocation']?>/documents/QuoteQuestionaireVehicles.doc" TARGET="_blank">Questionnaire Form</a>.</font>
<br><br>
<INPUT TYPE="button" VALUE="  Back   " onClick="history.go(-1);return true;">

