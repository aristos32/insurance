<!-- begin main content -->
<?php

if (!defined('including')) {
	die('Direct access not premitted');
}

/* retrieve HOW_DID_YOU_HEAR_ABOUT_US statistics */
$retrieveStatistics = retrieveStatistics($HOW_DID_YOU_HEAR_ABOUT_US);
	
?>

<h2>HOW DID YOU HEAR ABOUT US </h2>	
	
<table width="90%" border="0" cellspacing="1" cellpadding="3">
	<tr>
		<td width="30%"><b>Value</td><td width="10%"><b>Count</td><td><b>Percent</td>
	</tr>
	<?php
	/* calculate the total count */
	$totalCount = 0;
	foreach($retrieveStatistics as $retrieveStatistic)
	{
		$totalCount = $totalCount + $retrieveStatistic->countValue;
	}
	
	/* present */
	foreach($retrieveStatistics as $retrieveStatistic)
	{
		$percent = round($retrieveStatistic->countValue*100/$totalCount,1);
	?>					
	<tr>
		<td class="col40Per"><?php echo $retrieveStatistic->value;?></td>
		<td class="col20Per"><?php echo $retrieveStatistic->countValue;?></td>
		<td class="col40Per"><?php echo $percent;?></td>
	</tr>
	<?php
	}
	?>
</table>

<?php
/* retrieve OWNER_RESIDENCE statistics */
$retrieveStatistics = retrieveStatistics($OWNER_RESIDENCE);
	
?>
<h2>OWNER RESIDENCE</h2>	
	
<table>
	<tr>
		<td class="col40Per"><b>Value</b></td>
		<td class="col20Per"><b>Count</b></td>
		<td class="col40Per"><b>Percent</b></td>
	</tr>
	<?php
	/* calculate the total count */
	$totalCount = 0;
	foreach($retrieveStatistics as $retrieveStatistic)
	{
		$totalCount = $totalCount + $retrieveStatistic->countValue;
	}
	
	/* present */
	foreach($retrieveStatistics as $retrieveStatistic)
	{
		$percent = round($retrieveStatistic->countValue*100/$totalCount,1);
	?>					
	<tr>
		<td class="col40Per"><?php echo $retrieveStatistic->value;?></td>
		<td class="col20Per"><?php echo $retrieveStatistic->countValue;?></td>
		<td class="col40Per"><?php echo $percent;?></td>
	</tr>
	<?php
	}
	?>
</table>

<?php
/* retrieve licenseCountry for all quotes */
$retrieveStatistics = retrieveLicenseCountries();
	
?>
<h2>LICENSE COUNTRIES</h2>	
	
<table>
	<tr>
		<td class="col40Per"><b>Value</b></td>
		<td class="col20Per"><b>Count</b></td>
		<td class="col40Per"><b>Percent</b></td>
	</tr>
	<?php
	/* calculate the total count */
	$totalCount = 0;
	foreach($retrieveStatistics as $retrieveStatistic)
	{
		$totalCount = $totalCount + $retrieveStatistic->countValue;
	}
	
	/* present */
	foreach($retrieveStatistics as $retrieveStatistic)
	{
		$percent = round($retrieveStatistic->countValue*100/$totalCount,1);
	?>					
	<tr>
		<td class="col40Per"><?php echo $retrieveStatistic->value;?></td>
		<td class="col20Per"><?php echo $retrieveStatistic->countValue;?></td>
		<td class="col40Per"><?php echo $percent;?></td>
	</tr>
	<?php
	}
	?>
</table>
<h2></h2>