<?php
	if (!defined('including')) {
		die('Direct access not premitted');
	}
?>

<p>Create New User. All fields in <font color="red">red </font> are mandatory</p>
<form name="createNewUser" action="./index.php" method="post" onSubmit="return checkCreateNewUserForm('userAddresses')">
	<table width="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#FFFFFF">
		<input type="hidden" name="action" value="newUserFormProcess">
		
		<p>
		<tr><td width="20%"><font color="red"><label>Username:</label></font></td>
		<td width="80%"><input type="text" name="newUsername" id="newUsername" size="30" value="" /><br /></td></tr>
		
		<tr><td width="20%"><label><font color="red">Password:</font></label></td>
		<td><input type="password" name="newPassword" id="newPassword" value="" /><br /></td></tr>
		
		<tr><td width="20%"><label><font color="red">Re-type Password:</font></label></td>
		<td><input type="password" name="password2" id="password2" value="" /><br /></td></tr>	
		
		<tr><td width="20%"><label>First Name:</label></td>
		<td><input type="text" name="firstName" id="firstName" size="30" value="" /><br /></td></tr>
		
		<tr><td width="20%"><label>Last Name:</label></td>
		<td><input type="text" name="lastName" id="lastName" size="30" value="" /><br /></td></tr>
		
		<tr><td width="20%"><font color="red"><label>Email:</label><br /></td></font>
		<td><input type="text" name="email" id="email" size="30" value="" /><br /></td></tr>
		
		<tr>
			<td width="27%"><label for="ownerResidence">Place of Residence:</label></td>
			<td><select name="ownerResidence" id="ownerResidence" style="width:230px;">
				<?php
				foreach($_SESSION['cityOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
				 </select>	
			</td>
		</tr>
		
		<tr>
			<td width="27%"><label for="howDidYouHearAboutUs">How did you hear about us?</label></td>
			<td><select name="howDidYouHearAboutUs" id="howDidYouHearAboutUs" style="width:230px;">
				<?php
				foreach($_SESSION['howDidYouHearAboutUsOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
				</select>
			</td>
		</tr>
		
		<tr><td align="left"><INPUT type="submit" class="button" value="   Create   " size="15" ></td><td width="20%"></td></tr>
		</p>
	</table>
</form>


