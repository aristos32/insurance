
<?php

if (!defined('including')) {
	die('Direct access not premitted');
}


//store variables from createNewUser form
$newUser = new user();
$statistics = new statistics();

$newUser->username = $_POST['newUsername'];
$newUser->password = md5($_POST['newPassword']);
$newUser->firstName = $_POST['firstName'];
$newUser->lastName = $_POST['lastName'];
$newUser->email = $_POST['email'];
$newUser->city = $_POST['ownerResidence'];
$newUser->role = $USER_ROLE_CUSTOMER;//new user is set to customer by default
$newUser->producer = $NO_CONSTANT;//new customer created from client location, is not an agent by default
$newUser->status = $USER_STATUS_ACTIVE;//status for new user is set to 'Active' by default
$newUser->city = $_POST['ownerResidence'];
$newUser->clientName = $clientName;//set during authentication.php
$newUser->databaseName = $_SESSION['db_name'];//taken from localDatabaseConstants.php

$howDidYouHearAboutUs = $_POST['howDidYouHearAboutUs'];

//password is updated in the global database
//ATTENTION - use include instead of require_once to override previous values
include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
insertNewUserInGlobalDatabase($newUser);

//return to local database for any following queries
//ATTENTION - use include instead of require_once to override previous values
include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";
		
/* Insert into client database */
$error = insertNewUser($newUser);
	
/* success */
if($error->affectedRows==1){
	/*Send Automated email to administrator */
	sendAutomatedEmail($newUser->username);
	
	/* insert the statistics */
	$statistics->code = $HOW_DID_YOU_HEAR_ABOUT_US;
	$statistics->value = $howDidYouHearAboutUs;
	insertStatistics($statistics);
	
	$statistics->code = $OWNER_RESIDENCE;
	$statistics->value = $newUser->city;
	insertStatistics($statistics);
	
	
  	?>
  	<p> New User was successfully created. You may now login</a> </p>
  	<?php
}
/* failure */
else{
	?>
	<p> Username already exists. Please choose another one</a> </p>
	<?php
}

	
?>
			
	