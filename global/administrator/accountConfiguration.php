<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$searchClientName = $_POST['clientName'];
$clientFilesLocation =  "../clients/".$searchClientName;
//echo $clientFilesLocation;
$_SESSION['parametersArray'] = readClientProperties($clientFilesLocation.'/configurationFiles/config.xml');

	
?>

<form name="accountConfigurationForm" action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" method="POST" >
	<input type="hidden" name="action" value="accountConfigurationFormProcess">
	<input type="hidden" name="clientFilesLocation" value="<?php echo $clientFilesLocation; ?>">
	<table>
	<?php
		foreach($_SESSION['parametersArray'] as $eachParameter)
		{
			$parameterValue = $eachParameter->name."_Value";
			
			switch($eachParameter->name)
			{
				case 'LOG_LEVEL':
					?>
                                    <tr>
                                        <td>
                                        <?php echo $eachParameter->name; ?>
                                        <select name="<?php echo $parameterValue; ?>" id="<?php echo $eachParameter->name; ?>">
                                            <?php
                                            $i = 1;
                                            foreach($_SESSION['logLevelOptions'] as $option){
                                                    ?>
                                                     <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
                                                     <?php
                                                     $i = $i + 1;
                                             }
                                             ?>		
                                             </select>	
                                        </td>
                                     </tr>
                                     <script type="text/javascript">		
                                        setPreviousValue("<?php echo $eachParameter->value; ?>", "LOG_LEVEL");
                                     </script>
				<?php
					break;
				case 'MAX_USERS_NUMBER':
					?>
					<tr>
					<td>
					<?php echo $eachParameter->name; ?>
					<input type="text" name="<?php echo $parameterValue; ?>" id="<?php echo $parameterValue; ?>" size="5" value="<?php echo $eachParameter->value;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('<?php echo $parameterValue; ?>')"/>
					</td>
					<?php
					break;
				case 'SYSTEM_EMAIL':
					?>
					<tr>
					<td>
					<?php echo $eachParameter->name; ?>
					<input type="text" name="<?php echo $parameterValue; ?>" id="<?php echo $parameterValue; ?>" size="25" value="<?php echo $eachParameter->value;?>" />
					</td>
					<?php
					break;
				}
				?>
		<?php
		}
		?>
		<tr><td class="label">&nbsp</td><td class="input">&nbsp</td></tr>
			<tr>
				<td class="input"><INPUT type="submit" value=<?php echo $_SESSION['updateButton']; ?> size="15" ></td>
			</tr>
	</table>
</form>