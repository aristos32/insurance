<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//parse xml file
$databases = array();

$databases = readDatabasePatches($_SESSION['globalFilesLocation'].'/database/databasePatches.xml');

//for each <database> execute all commands
foreach($databases as $eachDatabase)
{
	//$eachDatabase->printData();
	
	//global database
	if($eachDatabase->type == $DATABASE_TYPE_GLOBAL )
	{
		include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
		
		foreach($eachDatabase->commands as $eachCommand)
		{
			echo "Global Database:  Executing - $eachCommand...<br/>";
			$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
			$pdoConnection->setSQLStatement($eachCommand);
			$pdoConnection->executeStatement();
					}
	}
	//client database
	else if($eachDatabase->type == $DATABASE_TYPE_CLIENT )
	{
		//apply to all clients
		if($eachDatabase->client == 'ALL')
		{
			//connect to global database to get ALL the client names
			include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
			$distinctClientNames = retrieveDistinctValues('clientName', 'systemuser' );
				
			//login to all client databases and retrieve users
			foreach($distinctClientNames as $eachClientName)
			{
				//use appropriate database for client
				$clientFilesLocation =  "../clients/".$eachClientName;
				include $clientFilesLocation."/database/localDatabaseConstants.php";
			
				//run all commands for this database
				foreach($eachDatabase->commands as $eachCommand)
				{		
					echo $eachClientName.":  Executing - $eachCommand...<br/>";
					$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
					$pdoConnection->setSQLStatement($eachCommand);
					$pdoConnection->executeStatement();		}
			}
		
		}
		//apply to individual clients
		else
		{
			foreach($eachDatabase->commands as $eachCommand)
			{
				//set correct database
				$clientFilesLocation =  "../clients/".$eachDatabase->client;
				include $clientFilesLocation."/database/localDatabaseConstants.php";
			
				echo $eachDatabase->client.":  Executing - $eachCommand...<br/>";
				$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
				$pdoConnection->setSQLStatement($eachCommand);
				$pdoConnection->executeStatement();
			}

		}
		//echo "client <br/>";
	}
	
	
}

//return to local database for any following queries
include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";

?>
