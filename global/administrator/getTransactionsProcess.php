<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//echo "actionRead=$actionRead";
//process only real requests - not language change
if($actionRead!='')
{
	$transactions = array();

	$parameterNameValueArray = array();
	
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "transactionStartDate";
	$parameterNameValue->value = date("Y-m-d", strtotime($_POST['transactionStartDate'])); 
	$parameterNameValueArray[] = $parameterNameValue;
	
	$transactionEndDate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($_POST['transactionEndDate'])) . " +1 day"));
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "transactionEndDate";
	$parameterNameValue->value = $transactionEndDate;
	$parameterNameValueArray[] = $parameterNameValue;
		
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "producer";
	$parameterNameValue->value = $_POST['producer'];
	$parameterNameValueArray[] = $parameterNameValue;
		
		
	
	$transactions = retrieveTransactions($parameterNameValueArray, "");
	$_SESSION['transactions'] = $transactions;
}

if(count($_SESSION['transactions']) > 0 )
{
	$_SESSION['totalCredit'] = 0;
	$_SESSION['totalDebit'] = 0;
	
	?>
	<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
			<td class="col15Per"><b><?php echo $_SESSION['producerTab']; ?></b></td>
			<td class="col15Per"><b><?php echo $_SESSION['date']; ?></b></td>
			<td class="col15Per"><b><?php echo $_SESSION['contractNumberTab']; ?></b></td>
			<td class="col15Per"><b><?php echo $_SESSION['details']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['receipt']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['debit']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['credit']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['remainder']; ?></b></td>
		</tr>
	<?php
	foreach($_SESSION['transactions'] as $transaction)
	{
		$_SESSION['totalCredit'] = $_SESSION['totalCredit'] + $transaction->credit;
		$_SESSION['totalDebit'] = $_SESSION['totalDebit'] + $transaction->debit;
		?>
		<td class="col15Per"><?php echo $transaction->producer; ?></td>
		<td class="col15Per"><?php echo $transaction->transDate; ?></td>
		<td class="col15Per"><?php echo $transaction->saleId; ?></td>
		<td class="col15Per"><?php echo $transaction->details; ?></td>
		<td class="col10Per"><?php echo $transaction->receiptNo; ?></td>
		<td class="col10Per"><?php echo $transaction->debit; ?></td>
		<td class="col10Per"><?php echo $transaction->credit; ?></td>
		<td class="col10Per"><?php echo $transaction->remainder; ?></td>
		</tr>
		<?php
	}
	?>
	<tr>
	<td class="col15Per"><?php echo $_SESSION['total']; ?></td>
	<td class="col15Per"></td>
	<td class="col15Per"></td>
	<td class="col15Per"></td>
	<td class="col10Per"></td>
	<td class="col10Per"><?php echo $_SESSION['totalDebit']; ?></td>
	<td class="col10Per"><?php echo $_SESSION['totalCredit']; ?></td>
	<td class="col10Per"></td>
	</tr>
	</table>	
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="transactionReportsMenu" method="POST" style="display: none;">
	<input type="text" name="action" value="transactionReportsMenu" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('transactionReportsMenu').submit()"><?php echo $_SESSION['return']; ?></a>
	<?php

}
//no data found
else 
{
	echo $_SESSION['noDataFound']; ?>.  
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="transactionReportsMenu" method="POST" style="display: none;">
	<input type="text" name="action" value="transactionReportsMenu" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('transactionReportsMenu').submit()"><?php echo $_SESSION['tryAgain']; ?></a>
	<?php
}
	