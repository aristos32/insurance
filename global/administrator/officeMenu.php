<script type="text/javascript" src="dropdowntabfiles/dropdowntabs.js">

/***********************************************
* Drop Down Tabs Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<!-- CSS for Drop Down Tabs Menu #1 -->
<link rel="stylesheet" type="text/css" href="dropdowntabfiles/ddcolortabs.css" />

<div id="topbody">
<div id="logo">&nbsp;</div>
 <?php
//show menu only when user is logged in - no access for customer or anonymous
if(isset($_SESSION['login']) && $_SESSION['login']==true && isset($_SESSION['role']) && $_SESSION['role'] > $USER_ROLE_CUSTOMER )
{
	?>
	<div id="authToolbar">	
	
	
	<div id="colortab" class="ddcolortabs">
	<ul>
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="homePage" method="post" style="display: none;">
	<input type="text" name="action" value="homePage" />
	</form>
	<li><a href="javascript:;" onclick="javascript: document.getElementById('homePage').submit()"><span><?php echo $_SESSION['homeTab']; ?></span></a></li>
	
	<li><a href="javascript:;" rel="dropmenu2_a" title="Clients"><span><?php echo $_SESSION['clients']; ?></span></a></li>	
	
	<li><a href="javascript:;" rel="dropmenu3_a" title="Contracts" ><span><?php echo $_SESSION['contractsTab']; ?></span></a></li>
	
	<li><a href="javascript:;" rel="leads" title="Leads" ><span><?php echo $_SESSION['leads']; ?></span></a></li>
	
	<li><a href="javascript:;" rel="dropmenuAdministration" title="administration" ><span><?php echo $_SESSION['administration']; ?></span></a></li>
	
	</ul>
	
	
	</div>

	
	<!--Client -->                                                
	<div id="dropmenu2_a" class="dropmenudiv_a">
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="findClientForm" method="post" style="display: none;">
			<input type="text" name="action" value="findClientForm" />
			<input type="text" name="type" value="account" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('findClientForm').submit()"><?php echo $_SESSION['find']; ?></a>
		
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="newClientDataForm" method="post" style="display: none;">
			<input type="text" name="action" value="newClientDataForm" />
			<input type="text" name="type" value="account" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('newClientDataForm').submit()"><?php echo $_SESSION['new']; ?></a>
		
	</div>
	
	<!--Contract -->                                                   
	<div id="dropmenu3_a" class="dropmenudiv_a">
		<!-- create hidden forn, to send the action as a POST -->
			<form action="./office.php" id="findContractForm" method="post" style="display: none;">
			<input type="text" name="action" value="findContractForm" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('findContractForm').submit()"><?php echo $_SESSION['find']; ?></a>
		
	</div>
	
	<!--Leads -->                                                
	<div id="leads" class="dropmenudiv_a">
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="findLeadForm" method="post" style="display: none;">
			<input type="text" name="action" value="findClientForm" />
			<input type="text" name="type" value="lead" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('findLeadForm').submit()"><?php echo $_SESSION['find']; ?></a>
		
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="newLeadDataForm" method="post" style="display: none;">
			<input type="text" name="action" value="newClientDataForm" />
			<input type="text" name="type" value="lead" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('newLeadDataForm').submit()"><?php echo $_SESSION['new']; ?></a>
		
	</div>
	
	
	<!--Administrator -->                                                   
	<div id="dropmenuAdministration" class="dropmenudiv_a">
	
		<form action="./office.php" id="reportsMenu" method="post" style="display: none;">
		<input type="text" name="action" value="reportsMenu" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('reportsMenu').submit()"><?php echo $_SESSION['reports']?></a>
		<?php
		if($_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
		{ 
			?>
			
			<form action="./office.php" id="userFindMenu" method="post" style="display: none;">
		<input type="text" name="action" value="userFindMenu" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('userFindMenu').submit()"><?php echo $_SESSION['usersTab']?></a>
		
			<form action="./office.php" id="historyMenu" method="post" style="display: none;">
			<input type="text" name="action" value="historyMenu" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('historyMenu').submit()"><?php echo $_SESSION['history']?></a>
			<?php
		}
		?>
		
		<form action="./office.php" id="notesMenuForm" method="post" style="display: none;">
		<input type="text" name="action" value="notesMenuForm" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('notesMenuForm').submit()"><?php echo $_SESSION['notesTab']?></a>
		
		<form action="./office.php" id="yourAccount" method="post" style="display: none;">
			<input type="hidden" name="action" value="yourAccount" />
			<input type="text" name="searchClientName" value="<?php echo $_SESSION['clientName'];?>" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('yourAccount').submit()"><?php echo $_SESSION['yourAccount']?></a>
		
		<?php
		if($_SESSION['role'] == $USER_ROLE_SUPER)
		{ 
			?>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="./office.php" id="superMenu" method="post" style="display: none;">
			<input type="text" name="action" value="superMenu" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('superMenu').submit()">SUPER</a>
			<?php
		}
		?>
	
	</div>
	
 	<?php echo $_SESSION['usernameTab'].":".$_SESSION['username'] ?>. 
  	<!-- send logout using POST in order for POST to work in office.php -->
  	<form style='display:inline;' name="logout" action="./office.php" method="POST" enctype="application/x-www-form-urlencoded">
  		<input type="hidden" name="action" value="logout">
  		<INPUT style='display:inline;' type="submit" class="button" value=<?php echo $_SESSION['logout']; ?> size="10" >
  	</form>
  	
  	
	
	
	</div>
	
	<div id="language">
	<form action="./office.php" id="english" method="post" style="display: none;">
		<input type="hidden" name="lang" value="English" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('english').submit()">English</a> /
	<form action="./office.php" id="greek" method="post" style="display: none;">
		<input type="hidden" name="lang" value="Greek" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('greek').submit()">Ελληνικά</a>
	</div>
	
<?php
}
//no access to the menu
else
{ ?>
	<div id="authentication">
	<?php 
	include $_SESSION['globalFilesLocation']."/authentication/loginForm.php";
	?>
	</div>
	<div id="language">
	<form action="./office.php" id="english" method="post" style="display: none;">
		<input type="hidden" name="lang" value="English" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('english').submit()">English</a> /
	<form action="./office.php" id="greek" method="post" style="display: none;">
		<input type="hidden" name="lang" value="Greek" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('greek').submit()">Ελληνικά</a>
	</div>
	<?php
}
?>
	
	
	
	<div id="clear"> </div> 
	
</div>

<br/><br/> <!-- SOS to go below the fixed topbody element -->

<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script>
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "auto");
</script>


