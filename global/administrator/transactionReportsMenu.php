<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$allProducers = retrieveAllProducers();

?>
<script>
$(document).ready(function(){

	var today = new Date();
	
	$("#transactionStartDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#transactionStartDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#transactionStartDate").datepicker('setDate', today );

    
    $("#transactionEndDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#transactionEndDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#transactionEndDate").datepicker('setDate', today );

          
});
</script>
<!-- EXPIRY DATE FORM -->
<form action="./office.php" id="getTransactions" method="post" onSubmit="return checkGetTransactions();">
<table>
	<tr> <th> <?php echo $_SESSION['transaction']; ?> </th> </tr>
	<input type="hidden" name="action" value="getTransactionsProcess" />
	<tr>
		<td>
	<?php
		
		echo $_SESSION['start']."  ";?>
		<input type="text" style="width: 80px;" id="transactionStartDate" name="transactionStartDate" />
			
		</td>
	</tr>
	<tr>
		<td>
		<?php
		echo $_SESSION['end']."  ";?>
		
		<input type="text" style="width: 80px;" id="transactionEndDate" name="transactionEndDate" /> 
	
		</td>
	</tr>
	
	<tr>
		<td>
		<?php echo $_SESSION['producerTab'] ?>:
		<select name="producer" id="producer" style="width:100px;">
			<?php
			foreach($allProducers as $eachProducer){
				?>
				 <option  value=<?php echo "$eachProducer" ?> ><?php echo "$eachProducer" ?> </option>
				 <?php
			 }
			 ?>
			  <option selected="selected" value="%" ><?php echo $_SESSION['all']; ?> </option>
		</select>
	
		</td>
	</tr>
	<tr>
		<td>
		<input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" />
		</td>
	</tr>
</table>	
</form>
