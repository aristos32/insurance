<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$user = new user();

//I-FRAME CALL FROM AN EXTERNAL WEBSITE
//username and password are send
if(	$username!='' && $password!='' )
{
	//Connect to Global Database in order to get the clientName - this connect is automatic when loading
	include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
	$user = authenticate($username, md5($password));
	
	//echo "username= $username, password= $password ";
	//$user->printdata();
	  	
	$productsArray = array($productType, $PRODUCT_TYPE_ALL);
	//print_r($productsArray);
	
	/*successfull authentication*/
	if( $user->username!=''  && $user->status==$USER_STATUS_ACTIVE && in_array($user->productType, $productsArray) )
	{
	  	$clientName = $user->clientName;
	  	$_SESSION['clientName'] = $clientName;
	  	$_SESSION['login'] = true;
  		$_SESSION['username'] = $user->username;//logged in user
  		$_SESSION['role'] = $user->role;
  		$_SESSION['searchUserName'] = $user->username;//for now it's the same
  		//echo "username=".$_SESSION['username'];
  		
  		
	}
	else
	{
		//echo "setting login to false <br>";
		$_SESSION['login'] = false;
		$_SESSION['username'] = '';
		$_SESSION['searchUserName'] = '';
		//if clientName is not set, we use default as cyprus-insurances.
		$clientName = "cyprus-insurances";
		$_SESSION['clientName'] = $clientName;
		
		//echo "Login Failed. Please try again";
	}
	$_SESSION['clientFilesLocation'] =  "../clients/".$clientName;
	
	//$user->printData();
	
	//if clientName is not set, we use default as cyprus-insurances.
	//if($clientName=='')
	//	$clientName = "cyprus-insurances";
}

//NOT LOGGED-IN YET - SET DEFAULT CLIENT
// 1.called from iFrame, but username and password are not send or are empty from external site.
// 2.when opening standalone quotation.php, without logging in.
if(!isset($_SESSION['login']) || $_SESSION['login']==false)
{
	//echo "setting default client name <br/>";
	$clientName = "cyprus-insurances";
	$_SESSION['clientName'] = $clientName;
	$_SESSION['clientFilesLocation'] =  "../clients/".$clientName;
}
//Case: user is logged -in - Do nothing here
//echo "authentication.php: ".$_SESSION['clientFilesLocation']." <br/>";

//return to local database for any following queries
include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";

?>