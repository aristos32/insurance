<?php
	
//used for logging in

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//echo "inside";

$username=$_POST['username'];
$password=md5($_POST['password']);//in database, password is in MD5. This also prevents SQL injection attacks
$ip = $_SERVER['REMOTE_ADDR'];

//Connect Step 1: Connect to Global Database in order to get the clientName - this connect is automatic when loading
include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";

/* Create a new mysqli object with database connection parameters */
$productsArray = array($productType, $PRODUCT_TYPE_ALL);
$user = authenticate($username, $password);
//$user->printData();
//print_r($productsArray);

//return to local database for any following queries
//ATTENTION - use include instead of require_once to override previous values
include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";

/*successfull authentication.
1. user exists in the database AND
2. user is active */
//echo "user status is $user->status <br> ";

if($user->status==$USER_STATUS_ACTIVE ){
	//user is allowed for this product
	if( in_array($user->productType, $productsArray))
	{
		$clientName = $user->clientName;
		$_SESSION['clientName'] = $clientName;
	  	$_SESSION['login'] = 'true';
	  	$_SESSION['username'] = $user->username;//logged in user
	  	$_SESSION['role'] = $user->role;
	  	$_SESSION['productType'] = $user->productType;
	  	$_SESSION['searchUserName'] = $user->username;//for now it's the same
	  	$_SESSION['clientFilesLocation'] =  "../clients/".$clientName;
	  	//echo "setting clientfileslocato to ".$_SESSION['clientFilesLocation']."<br>";
	  	//read properties of each client - only load at the first call
	  	$auditLogger->writeToFile("Login Attempt. Username=$username, password=".$_POST['password'].", IP=$ip. Success", LOG_LEVEL_ERROR);
		?>
	  	Login Successfull.
	  	<!-- create hidden forn, to send the action as a POST  -->
			<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else if($runFromGlobalLocationOffice==true) echo './office.php'; else echo './members.php'; ?>" id="loginFormProcess" method="POST" style="display: none;">
			<input type="text" name="action" value="homePage" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('loginFormProcess').submit()"><?php echo $_SESSION['continue']; ?></a> 
			
	  	<?php
  	}
  	else
  	{
  		$auditLogger->writeToFile("Login Attempt. Username=$username, password=".$_POST['password'].", IP=$ip. User not allowed for this product", LOG_LEVEL_ERROR);
	  	?>
	  	User is not allowed for this product. Please 
	  	<a href="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php?action=loginForm'; else if($runFromGlobalLocationOffice==true) echo './office.php?action=loginForm'; else echo './members.php?action=loginForm'; ?>" onMouseOver="window.status='Try Again'; return true;" onMouseOut="window.status='';" title="Login" >try again</a>.<br ><br >
	  	<?php
  	}
}
/* failure */
else{
  	//$_SESSION['login'] = 'false';
  	//$user->printData();
  	
  	if($user->status==$USER_STATUS_SUSPENDED)
  	{
  		$auditLogger->writeToFile("Login Attempt. Username=$username, password=".$_POST['password'].", IP=$ip. User suspended.", LOG_LEVEL_ERROR);
	  	?>
	  	User is suspended. Please
	  	<?php
  	}
  	else if($user->status==$USER_STATUS_INVALID_USERNAME)
  	{
  		$auditLogger->writeToFile("Login Attempt. Username=$username, password=".$_POST['password'].", IP=$ip. Invalid username.", LOG_LEVEL_ERROR);
  		?>
  		<p>Invalid Username. Please 
  		<?php
	}
	else 
  	{
  		$auditLogger->writeToFile("Login Attempt. Username=$username, password=".$_POST['password'].", IP=$ip. Invalid password.", LOG_LEVEL_ERROR);
  		?>
  		<p>Wrong Password. Please 
  		<?php
	}
	session_destroy();
	?>
  	<a href="<?php if($runFromGlobalLocationQuotation==true) echo "./quotation.php?action=loginForm"; else echo "./office.php?action=loginForm"; ?>"  onMouseOver="window.status='Try Again'; return true;" onMouseOut="window.status='';" title="Login" >try again</a>.<br ><br >
  	<p>I forgot my <a href="<?php if($runFromGlobalLocationQuotation==true) echo "./quotation.php?action=forgotPassword"; else echo "./office.php?action=forgotPassword"; ?>" onMouseOver="window.status='Forgot Password'; return true;" onMouseOut="window.status='';" title="Forgot Password" >password</a>.
  	<?php
}
?>