<?php
	
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$username = $_SESSION['searchUserName'];

$searchClientName = $_SESSION['searchClientName'];

//connect to appropriate database to add the user and all related information
$clientFilesLocation =  "../clients/".$searchClientName;
include $clientFilesLocation."/database/localDatabaseConstants.php";

//echo "username=$username, clientFilesLocation=$clientFilesLocation <br>";

/* Check if username exists - retrieve the email */
//$result = checkUserNameExists($username);
$users = retrieveUsersInfo($username);

if(count($users) == 1 )
{
/*username exists*/
//if($result==1){
//if($user->username!=''){
	//$email = $_SESSION['email'];
	$email = $users[0]->email;
	$newPassword = $_POST['password2'];
	
	//echo "new password is $newPassword <br>";
	
	//password is updated in the global database
	//ATTENTION - use include instead of require_once to override previous values
	include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
	//update password in database
	updateUserPassword($username, md5($newPassword));
	
	//return to local database for any following queries
	//ATTENTION - use include instead of require_once to override previous values
	include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";
	
	//add to history table only on success
	$localHistory = new history();
	$localHistory->username = $_SESSION['username'];
	$localHistory->type = $HISTORY_TYPE_USER;
	$localHistory->parameterName = "username";
	$localHistory->parameterValue = $username;
	$localHistory->note = "$localHistory->username : Updated password of user=$username";
	insertNewHistory($localHistory);
	
	//email new password to user
	$subject = "New Password";
	$message = "Dear customer, \n\nYour new Password is $newPassword\n Please try to login again.\n\nBest Regards";
	
        
    writeToFile(date("Y-m-d H:i:s"). ": message in updatePassword=$message \n", LOG_LEVEL_INFO);
        
	$headers   = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/plain; charset=iso-8859-1";
	$headers[] = "From: ".$_SESSION['SYSTEM_EMAIL'];
	sendEmail($subject, $message, $email, $email, $headers);
	
	//destroy session in order to force login with new password. when following link is followed, user is directed in login screen
	session_destroy();
	?>
  	<p>Your password has been updated.
  		<!-- create hidden forn, to send the action as a POST.->
		<form action="./office.php" id="updatePasswordSuccess" method="post" style="display: none;">
			<input type="text" name="action" value="logout" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updatePasswordSuccess').submit()">Login again</a> using your new password. </p>
		
  	 <!--<a href="./quotation.php?action=loginForm" onMouseOver="window.status='Try Again'; return true;" onMouseOut="window.status='';" title="Login" >Login again</a> using your new password. </p>-->
  	<?php
}
/*username doesn't exist*/
else{
  	?>
  	Username doesn't exist. Please <a href="./quotation.php?action=forgotPassword" onMouseOver="window.status='Forgot Password'; return true;" onMouseOut="window.status='';" title="Forgot Password" >try again</a>.
  	<?php
}
	
?>