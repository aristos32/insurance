<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$clientId = $_SESSION['clientId'];

deleteFromTable('owner', 'stateId', $clientId);

//add to history table only on success
$localHistory = new history();
$localHistory->username = $_SESSION['username'];
$localHistory->type = $HISTORY_TYPE_CLIENT;
$localHistory->parameterName = "stateId";
$localHistory->parameterValue = $clientId;
$localHistory->note = "$localHistory->username : Deleted Client with stateId=$clientId";
insertNewHistory($localHistory);

echo $_SESSION['deleteSuccessfull']; ?>. 
<!-- create hidden forn, to send the action as a POST -->
<form action="./office.php" id="returnToFindClientForm" method="post" style="display: none;">
	<input type="text" name="action" value="findClientForm" />	
</form>
<a href="javascript:;" onclick="javascript: document.getElementById('returnToFindClientForm').submit()"><?php echo $_SESSION['continue']; ?></a>
