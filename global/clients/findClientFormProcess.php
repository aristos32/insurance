<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');


$parameterNameValueArray = array();

//on language change, POST are not send. So, use ones in session
if(isset($_POST['firstName']) && $_POST['firstName']!='' )
{
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "firstName";
	$parameterNameValue->value = "%".$_POST['firstName']."%";
	
	$parameterNameValueArray[] = $parameterNameValue;
	
	//$_SESSION['firstName'] = $_POST['firstName'];
	//$proposers = retrieveProposerInfo('firstName', "%".$_POST['firstName']."%");
	//$_SESSION['proposers'] = $proposers;
}

if(isset($_POST['surname']) && $_POST['surname']!='' )
{
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "lastName";
	$parameterNameValue->value = "%".$_POST['surname']."%";
	
	$parameterNameValueArray[] = $parameterNameValue;
	
	//$_SESSION['surname'] = $_POST['surname'];
	//$proposers = retrieveProposerInfo('lastName', "%".$_POST['surname']."%");
	//$_SESSION['proposers'] = $proposers;
}

if(isset($_POST['stateId']) && $_POST['stateId']!='' )
{
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "stateId";
	$parameterNameValue->value = "%".$_POST['stateId']."%";
	
	$parameterNameValueArray[] = $parameterNameValue;
	
	
	//$_SESSION['stateId'] = $_POST['stateId'];
	//$proposers = retrieveProposerInfo('stateId', "%".$_POST['stateId']."%");
	//$_SESSION['proposers'] = $proposers;
}	

if(isset($_POST['cellphone']) && $_POST['cellphone']!='' )
{
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "cellphone";
	$parameterNameValue->value = "%".$_POST['cellphone']."%";
	
	$parameterNameValueArray[] = $parameterNameValue;
	
	
	//Step 1 : look for cellphone
	//$proposers = retrieveProposerInfo('cellphone', "%".$_POST['cellphone']."%");
	//$_SESSION['proposers'] = $proposers;
}

//type : account or lead
$parameterNameValue = new parameterNameValue();
$parameterNameValue->name = "type";
$parameterNameValue->value = "%".$_SESSION['type']."%";

$parameterNameValueArray[] = $parameterNameValue;


if(count($parameterNameValueArray)>0)
{
	//echo "INSUIDE";
	$_SESSION['proposers'] = retrieveProposerInfo($parameterNameValueArray);
	//$_SESSION['proposers'] = $proposers;
	//create a list of all the emails
	$allEmails = '';
	$allStateIds = '';
	
	foreach($_SESSION['proposers'] as $eachProposer)
	{
		if($eachProposer->owner->email != '' )
		{
			$allEmails = $allEmails.",".$eachProposer->owner->email;
			$allStateIds = $allStateIds.",".$eachProposer->owner->stateId;
		}
	}
	//echo $allEmails;
}


displayAllProposers($_SESSION['proposers'], $allEmails, $allStateIds);
	
?>