<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//echo "creating new client";
$proposer = new proposer();
$proposer->owner = new owner();
$proposer->license = new license();
$proposer->ownerAddress = new ownerAddress();
	
//include $_SESSION['globalFilesLocation']."/clients/updateClientDataDefaultValues.php";

?>

<script>		
/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
function setDefaultValues()
{
	setPreviousValue("<?php echo $EMAIL_PREFERENCE_CODE_NEWSLETTER; ?>", "preferenceCode");
}

$(document).ready(function(){

    $("#birthDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-90:+10'});
    $('#birthDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    
    $("#licenseIssueDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-90:+10'});
    $('#licenseIssueDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
        
});
</script>

<form name="newClientDataForm" action="./office.php" method="POST" onSubmit="return checkUpdateClientDataForm('userAddresses')">
	<table>
	<input type="hidden" name="action" value="newClientDataFormProcess">

		<tr><td class="label"><?php echo $_SESSION['stateIdTab']; ?>:</td>
		<td class="input"><input type="text" name="stateId" id="stateId" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['nameTab']; ?>:</td>
		<td class="input"><input type="text" name="firstName" id="firstName" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['surname']; ?>:</td>
		<td class="input"><input type="text" name="lastName" id="lastName" size="30" value="" /> </td></tr>
		</table>
		<?php
		
		$emptyAddressesArray = array();
		displayAddresses($emptyAddressesArray);
		?>
		<table>		
		<tr>
			<td class="label"><?php echo $_SESSION['gender'] ?>:</td>
			<td class="input"><select name="gender" id="gender">
					<?php
					foreach($_SESSION['genderOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
				</select>
			</td>
		</tr>

		<tr>
			<td class="label"><?php echo $_SESSION['proposerTypeTab'] ?>:</td>
			<td class="input"><select name="proposerType" id="proposerType">
					<?php
					foreach($_SESSION['proposerTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
				</select>
			</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['telephoneTab']; ?>:</td>
		<td class="input"><input type="text" name="telephone" id="telephone" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['cellphoneTab']; ?>:</td>
		<td class="input"><input type="text" name="cellphone" id="cellphone" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['profession']; ?>:</td>
		<td class="input"><input type="text" name="profession" id="profession" size="30" value="" /> </td></tr>
		
		<tr><td class="label">Email:</td>
		<td class="input"><input type="text" name="email" id="email" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['emailPreference'] ?>:</td>
		<td class="input"><select name="preferenceCode" id="preferenceCode">
				<?php
				foreach($_SESSION['emailPreferenceOptions'] as $option)
				{
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['birthDate']; ?>:</td>
		<td class="input">
		<input type="text" style="width: 80px;" id="birthDate" name="birthDate" />
		
		</td>
		
		<tr><td class="label"><?php echo $_SESSION['licenseDateTab']; ?>:</td>
		<td class="input">
		<input type="text" style="width: 80px;" id="licenseIssueDate" name="licenseIssueDate" />
		
		</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['licenseTypeTab'] ?>:</td>
		<td class="input"><select name="licenseType" id="licenseType">
				<?php
				foreach($_SESSION['licenseTypeTabOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<!-- COUNTRY OF DRIVING LICENSE -->
		<tr>
			<td class="label"><?php echo $_SESSION['countryOfLicense']; ?></td>
			<td class="input">
				<select name="licenseCountry" id="licenseCountry" style="width:250px;">
				<?php
					foreach($_SESSION['countriesOptions'] as $eachCountry)
					{?>
					<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
					<?php	
					}
					?>
					</select>
				<a href="./quotation.php?action=glossary&location=licenseCountry" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.jpg" border=0></a>
				
			</td>
			
		</tr>
		
		<tr><td class="label">&nbsp</td><td class="input">&nbsp</td></tr>
		<tr>
			<td class="label"></td><td class="input"><INPUT type="submit" value=<?php echo $_SESSION['create']; ?> size="15" ></td>
		</tr>
		
		</form>
		
		
	</table>