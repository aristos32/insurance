<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//echo "actionRead=$actionRead";
//process only real requests - not language change
if($actionRead!='')
{	
	$owner = new owner();
	$owner->stateId = trim($_POST['stateId']);
	if( isset($_SESSION['searchStateId']) )
		$owner->oldStateId = trim($_SESSION['searchStateId']);
	$owner->gender = trim($_POST['gender']);
	$owner->firstName = trim($_POST['firstName']);
	$owner->lastName = trim($_POST['lastName']);
	$owner->type = trim($_SESSION['type']);
	$owner->gender = trim($_POST['gender']);
	$owner->telephone = trim($_POST['telephone']);
	$owner->cellphone = trim($_POST['cellphone']);
	$owner->profession = trim($_POST['profession']);
	$owner->email = trim($_POST['email']);
	$owner->birthDate = trim($_POST['birthDate']);
	$owner->proposerType = trim($_POST['proposerType']);
	
	$owner->emailPreference = new emailPreference();
	$owner->emailPreference->stateId = $owner->stateId;
	$owner->emailPreference->preferenceCode = $_POST['preferenceCode'];
	$owner->emailPreference->preferenceFrequency = $FREQUENCY_MONTHLY;//for now, set default as monthly
	
	//read fields that come as arrays
	$addressTypes = @$_POST['addressType'];
	$addressIds = @$_POST['addressId'];
	$streets = @$_POST['street'];
	$codes = @$_POST['code'];
	$citys = @$_POST['city'];
	$countrys = @$_POST['country'];
	
	$owner->addresses = array();//initialize to empty array
	
	//aristosa stateId cannot be null
	//if($owner->stateId!="")
	//{
		if(isset($addressTypes))
		{
			//set all array elements
			foreach($addressTypes as $a => $b)
			{
		  		$localAddress = new ownerAddress();
		  		$localAddress->stateId = $owner->stateId;
		  		$localAddress->addressType = $addressTypes[$a];
				$localAddress->addressId = $addressIds[$a];
				$localAddress->street = $streets[$a];
				$localAddress->areaCode = $codes[$a];
				$localAddress->city = $citys[$a];
				$localAddress->country = $countrys[$a];
				//$localAddress->printData();
				
				$owner->addresses[] = $localAddress;
		  		
			}
		}
	//}
	
	
	//echo "insert new client";
	$localError = insertOwner($owner);
	
	/* Insert emailPreference */
	insertEmailPreference($owner->emailPreference);
				
	if($localError != -1)
	{
		//add to history table only on success
		$localHistory = new history();
		$localHistory->username = $_SESSION['username'];
		$localHistory->type = $HISTORY_TYPE_CLIENT;
		$localHistory->parameterName = "stateId";
		$localHistory->parameterValue = $owner->stateId;
		$localHistory->note = "$localHistory->username : Add Client with stateId=$owner->stateId";
		insertNewHistory($localHistory);
		
		//set new state id, if it was updated
		$_SESSION['searchStateId'] = $owner->stateId;
		//update address only when the owner was updated correctly
		foreach($owner->addresses as $eachAddress)
		{
			//$ownerAddress = fillOwnerAddressClass($owner);
			//$ownerAddress->printData();
			insertOwnerAddress($eachAddress);
		}
		//update LICENSE table
		$license = new license();
		$license->licenseType = $_POST['licenseType'];
		$license->licenseDate = $_POST['licenseIssueDate'];
		$license->licenseCountry = $_POST['licenseCountry'];
		$license->oldStateId = $owner->oldStateId;
		$license->stateId = $owner->stateId;
		$error = insertOwnerLicense($license);
		?>
		New Client Added Correctly. 	
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="updateClientDataForm" method="POST" style="display: none;">
		<input type="text" name="action" value="updateClientDataForm" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updateClientDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
		<?php
	}
	else
	{
		echo "Client already exists in the system.";
		?>
		
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="newClientDataForm" method="POST" style="display: none;">
			<input type="text" name="action" value="newClientDataForm" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('newClientDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
		<?php
	}
		
}
else
	{
	echo $_SESSION['addSuccessfull']; ?>.. 	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="updateClientDataForm" method="POST" style="display: none;">
	<input type="text" name="action" value="updateClientDataForm" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('updateClientDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
	<?php
	}
	?>
