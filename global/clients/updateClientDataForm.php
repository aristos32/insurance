<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');


//echo "modifying an existing client";
//retrieve the username we search for
$currentStateId = $_SESSION['searchStateId'];
//echo "currentStateId = $currentStateId, firstName= ".$_SESSION['firstName'];

$proposer = new proposer();

$parameterNameValueArray = array();
$parameterNameValue = new parameterNameValue();
$parameterNameValue->name = "stateId";
$parameterNameValue->value = $currentStateId;

$parameterNameValueArray[] = $parameterNameValue;

$proposers = retrieveProposerInfo($parameterNameValueArray);

foreach($proposers as $eachProposer)
	if($eachProposer->owner->stateId==$currentStateId)
	{
		$proposer->owner = new owner();
		$proposer->owner = $eachProposer->owner;
		$proposer = $eachProposer;
		unset($_SESSION['proposer']);
		$_SESSION['proposer'] = $proposer;
		//$proposer->printData();
		break;
	}

	//$proposer->printData();

	include $_SESSION['globalFilesLocation']."/clients/updateClientDataDefaultValues.php";

	//phpinfo();
?>
<script>

$(document).ready(function(){

	var today = new Date();
	
    $("#birthDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-90:+10'});
    $('#birthDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
	//alert("<?php echo $proposer->owner->birthDate; ?>");
    $("#birthDate").datepicker('setDate', "<?php echo $proposer->owner->birthDate; ?>");
    
    $("#licenseIssueDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-90:+10'});
    $('#licenseIssueDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#licenseIssueDate").datepicker('setDate', "<?php echo $proposer->license->licenseDate; ?>");

    $("#notesDate1").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-90:+10'});
    $('#notesDate1').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#notesDate1").datepicker('setDate', today);
	    
        
});

//AJAX Functionality	
/* fill specific data, before calling the generic AJAX functions */
function updateUserDataAjax()
{
	
	//file to execute
	url = "./users/updateUserDataAjax.php";
	//read all variables to update automatically
    var stateId = document.getElementById('stateId').value;
    var firstName = document.getElementById('firstName').value;
    var lastName = document.getElementById('lastName').value;
    var email = document.getElementById('email').value;
  	var telephone = document.getElementById('telephone').value;
    var cellphone = document.getElementById('cellphone').value; 
    var residenceAddressStreet = document.getElementById('residenceAddressStreet').value;
    var residenceAddressAreaCode = document.getElementById('residenceAddressAreaCode').value;
    var residenceAddressCity = document.getElementById('residenceAddressCity').value;
  	var residenceAddressState = document.getElementById('residenceAddressState').value;
    var residenceAddressCountry = document.getElementById('residenceAddressCountry').value;
    var correspondenceAddressStreet = document.getElementById('correspondenceAddressStreet').value;
    var correspondenceAddressAreaCode = document.getElementById('correspondenceAddressAreaCode').value;
    var correspondenceAddressCity = document.getElementById('correspondenceAddressCity').value;
  	var correspondenceAddressState = document.getElementById('correspondenceAddressState').value;
    var correspondenceAddressCountry = document.getElementById('correspondenceAddressCountry').value;
    var profession = document.getElementById('profession').value;
    var birthDate = document.getElementById('birthDate').value;
    var licenseIssueDate = document.getElementById('licenseIssueDate').value;
    var gender = document.getElementById('gender').value;
    
    queryString = "?stateId=" + stateId + "&firstName=" + firstName + "&lastName=" + lastName + "&email=" + email + "&telephone=" + telephone + "&cellphone=" + cellphone + "&residenceAddressStreet=" + residenceAddressStreet + "&residenceAddressAreaCode=" + residenceAddressAreaCode + "&residenceAddressCity=" + residenceAddressCity + "&residenceAddressState=" + residenceAddressState + "&residenceAddressCountry=" + residenceAddressCountry + "&correspondenceAddressStreet=" + correspondenceAddressStreet + "&correspondenceAddressAreaCode=" + correspondenceAddressAreaCode + "&correspondenceAddressCity=" + correspondenceAddressCity + "&correspondenceAddressState=" + correspondenceAddressState + "&correspondenceAddressCountry=" + correspondenceAddressCountry + "&birthDate=" + birthDate + "&licenseIssueDate=" + licenseIssueDate + "&profession=" + profession + "&gender=" + gender;
  
    //alert(url+queryString);
  
    myFunction(url+queryString);
    //loadXMLDoc("updateUserDataAjax.php");
    return true;
} 



</script>



<script>		
/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
function setDefaultValues()
{
	//alert("<?php echo $proposerType_content; ?>");
	setPreviousValue("<?php echo $gender_content; ?>", "gender");
	setPreviousValue("<?php echo $proposerType_content; ?>", "proposerType");
	setPreviousValue("<?php echo $licenseType_content; ?>", "licenseType");
	setPreviousValue("<?php echo $licenseCountry_content; ?>", "licenseCountry");
	setPreviousValue("<?php echo $preferenceCode_content; ?>", "preferenceCode");
	setPreviousValue("<?php echo $type_content; ?>", "type");
}
</script>


<form name="updateClientDataForm" action="./office.php" method="POST" onSubmit="return checkUpdateClientDataForm('userAddresses')" accept-charset="utf-8">
	<input type="hidden" name="action" value="updateClientDataFormProcess">
	<table>
	
		<tr>
		<td class="label"><?php echo $_SESSION['stateIdTab']; ?>:</td>
		<td class="input"><input type="text" name="stateId" id="stateId" size="30" value="<?php echo $proposer->owner->stateId;?>" /> </td></tr>
		
		<tr>
		<td class="label"><?php echo $_SESSION['nameTab']; ?>:</td>
		<td class="input"><input type="text" name="firstName" id="firstName" size="30" value="<?php echo $proposer->owner->firstName;?>" /> </td></tr>
		
		<tr>
		<td class="label"><?php echo $_SESSION['surname']; ?>:</td>
		<td class="input"><input type="text" name="lastName" id="lastName" size="30" value="<?php echo $proposer->owner->lastName;?>" />
		 </td></tr>
		
		<tr>
			<td class="label"><?php echo $_SESSION['accountTypeTab'] ?>:</td>
			<td class="input"><select name="type" id="type">
					<?php
					foreach($_SESSION['accountTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
				</select>
			</td>
		</tr>
		 
		<tr>
			<td class="label"><?php echo $_SESSION['gender'] ?>:</td>
			<td class="input"><select name="gender" id="gender">
					<?php
					foreach($_SESSION['genderOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
				</select>
			</td>
		</tr>

		<tr>
			<td class="label"><?php echo $_SESSION['proposerTypeTab'] ?>:</td>
			<td class="input"><select name="proposerType" id="proposerType">
					<?php
					foreach($_SESSION['proposerTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
				</select>
			</td>
		</tr>
		
		<tr>
		<td class="label"><?php echo $_SESSION['telephoneTab']; ?>:</td>
		<td class="input"><input type="text" name="telephone" id="telephone" size="30" value="<?php echo $proposer->owner->telephone;?>" /> </td></tr>
		
		<tr>
		<td class="label"><?php echo $_SESSION['cellphoneTab']; ?>:</td>
		<td class="input"><input type="text" name="cellphone" id="cellphone" size="30" value="<?php echo $proposer->owner->cellphone;?>" /> </td></tr>
		
		<tr>
		<td class="label"><?php echo $_SESSION['profession']; ?>:</td>
		<td class="input"><input type="text" name="profession" id="profession" size="30" value="<?php echo $proposer->owner->profession;?>" /> </td></tr>
		
		<tr>
		<td class="label">Email:</td>
		<td class="input"><input type="text" name="email" id="email" size="30" value="<?php echo $proposer->owner->email;?>" /> 
		<a href="javascript:;" onclick="send_email(document.getElementById('email').value, '<?php echo $_SESSION['globalFilesLocation']; ?>', '<?php echo $_SESSION['SYSTEM_EMAIL']; ?>', '<?php echo $_SESSION['lang']; ?>', '<?php echo $_SESSION['clientName']; ?>', '<?php echo $currentStateId; ?>' )"><?php echo $_SESSION['create']; ?></a>
		</td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['emailPreference'] ?>:</td>
		<td class="input"><select name="preferenceCode" id="preferenceCode">
				<?php
				foreach($_SESSION['emailPreferenceOptions'] as $option)
				{
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<tr>
		<td class="label"><?php echo $_SESSION['birthDate']; ?>:</td>
		<td class="input">
		<input type="text" style="width: 80px;" id="birthDate" name="birthDate" />
		</td>
		
		<?php
		//show license info only for OFFICE product type
		if($productType == $PRODUCT_TYPE_OFFICE)
		{
			?>
			
			<tr>
			<td class="label"><?php echo $_SESSION['licenseDateTab']; ?>:</td>
			<td class="input">
			<input type="text" style="width: 80px;" id="licenseIssueDate" name="licenseIssueDate" />
			</td>
			</tr>
			
			<tr>
			<td class="label"><?php echo $_SESSION['licenseTypeTab'] ?>:</td>
			<td class="input"><select name="licenseType" id="licenseType">
					<?php
					foreach($_SESSION['licenseTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
				</select>
			</td>
			</tr>
			
			<!-- COUNTRY OF DRIVING LICENSE -->
			<tr>
				<td class="label"><?php echo $_SESSION['countryOfLicense']; ?></td>
				<td class="input"><select name="licenseCountry" id="licenseCountry" style="width:250px;">
					<?php
						foreach($_SESSION['countriesOptions'] as $eachCountry)
						{?>
						<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
						<?php	
						}
						?>
					<a href="./quotation.php?action=glossary&location=licenseCountry" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.jpg" border=0></a>
					
				</td>
				
			</tr>
			<?php
		}
		?>
	</table>
		<?php
		displayAddresses($proposer->addresses);
		?>
	<table>
		<tr>
		<td class="label">
		<table>
 		<tr><td><h3><?php echo $_SESSION['notesTab']; ?></h3></td></tr>
 		<tr>
 			<td class="col10Per"><b><?php echo $_SESSION['date']; ?></b></td>
			<td class="col60Per"><b><?php echo $_SESSION['description']; ?></b></td>
			<td class="col1Per"></td>
		</tr>
		
		<?php
 		$notes = array();
 		$parameterNameValueArray = array();
 		
 		$parameterNameValue = new parameterNameValue();
		$parameterNameValue->name = "stateId";
		$parameterNameValue->value = $proposer->owner->stateId;
		$parameterNameValueArray[] = $parameterNameValue;
		
 		$notes = retrieveNotes($parameterNameValueArray);
 		
 			
		$j = 0;	
 		foreach($notes as $note)
 		{
	 		?>
	 		<tr id="<?php echo "note".$note->notesId;?>">
			<td class="col10Per"><?php echo $note->entryDate; ?></td>
			<td class="col60Per"><?php echo $note->description; ?></td>
	 		<td class="col1Per"><a href="javascript:;" onclick="javascript:deleteNoteAjax('<?php echo $note->notesId;?>');"><img src="./images/delete-sign.jpg"/> </a></td>
			</tr>
	 		<?php
	 		$j = $j + 1;
 		}
 		
 		//MADE THIS A FORM, NOT AJAX - FINISH FINISH FORM PROCESSING
 		?>
		<tr>
 			<td class="col10Per"><input type="text" size="8" name="notesDate1" id="notesDate1"  /></td>
 			<td class="col60Per"><input type="text" size="40" name="notesDescription1" id="notesDescription1"  value="" /></td>
 			<td class="col1Per"></td>
 		</tr>
 		</table>
 		
 		</td>
 		
 		<td class="input">&nbsp</td></tr>
 		
	</table>
	
	<table>
		<tr>
		<td class="label">&nbsp</td><td class="input">&nbsp</td></tr>
		<tr>
			<td class="label"></td><td class="input"><INPUT type="submit" value=<?php echo $_SESSION['updateButton']; ?> size="15" ></td>
		</tr>
		
</form>
		
		<?php 
		if($type_content=='account')
		{?>
		
			<tr>
				<td class="label">
					<div id="message" style="color:#00FF00"></div>
				</td>
				<td class="input">
				<!-- create hidden forn, to send the action as a POST -->
				<form action="./office.php" id="newContractForm" method="post" style="display: none;">
					<input type="text" name="action" value="newContractForm" />
				</form>
				<a href="javascript:;" onclick="javascript: document.getElementById('newContractForm').submit()"><?php echo $_SESSION['newContract']; ?></a>
				<!-- following code is not working very well. update is done only when alert()exists.
				<a href="javascript:;" onclick="javascript: if(checkUpdateUserDataFormForAddContractValidity()){ updateUserDataAjax();document.getElementById('newContractForm').submit()}"><?php echo $newContract; ?></a>
				-->
				</td>
			</tr>
			
			
			
			<tr>
				<td class="label"></td>
				<td class="input">
				<!-- create hidden forn, to send the action as a POST -->
				<form action="./office.php" id="retrieveAllClientContracts" method="post" style="display: none;">
					<input type="text" name="action" value="retrieveAllClientContracts" />
					<input type="text" name="stateId" value="<?php echo $proposer->owner->stateId;?>" />
				</form>
				<a href="javascript:;" onclick="javascript: document.getElementById('retrieveAllClientContracts').submit()"><?php echo $_SESSION['contractsTab']; ?></a>
				</td>
			</tr>
			<?php 
		}?>
		
	</table>