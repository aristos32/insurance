<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
	
//echo "actionRead=$actionRead";
//process only real requests - not language change
if($actionRead!='')
{

	$owner = new owner();
	$owner->stateId = trim($_POST['stateId']);
	if( isset($_SESSION['searchStateId']) )
		$owner->oldStateId = trim($_SESSION['searchStateId']);
	$owner->gender = trim($_POST['gender']);
	$owner->firstName = trim($_POST['firstName']);
	$owner->lastName = trim($_POST['lastName']);
	$owner->type = trim($_POST['type']);
	$owner->gender = trim($_POST['gender']);
	$owner->telephone = trim($_POST['telephone']);
	$owner->cellphone = trim($_POST['cellphone']);
	$owner->profession = trim($_POST['profession']);
	$owner->email = trim($_POST['email']);
	$owner->birthDate = trim($_POST['birthDate']);
	$owner->proposerType = trim($_POST['proposerType']);
	
	$owner->emailPreference = new emailPreference();
	$owner->emailPreference->stateId = $owner->stateId;
	$owner->emailPreference->preferenceCode = $_POST['preferenceCode'];
	$owner->emailPreference->preferenceFrequency = $FREQUENCY_MONTHLY;//for now, set default as monthly
	
	//read fields that come as arrays
	//$chks = @$_POST['chk'];
	$addressTypes = @$_POST['addressType'];
	$addressIds = @$_POST['addressId'];
	$streets = @$_POST['street'];
	$codes = @$_POST['code'];
	$citys = @$_POST['city'];
	$countrys = @$_POST['country'];
	
	$owner->addresses = array();//initialize to empty array
	
	if(isset($addressTypes))
	{
		//set all array elements
		foreach($addressTypes as $a => $b)
		{
	  		$localAddress = new ownerAddress();
	  		$localAddress->stateId = $owner->stateId;
	  		$localAddress->addressType = $addressTypes[$a];
			$localAddress->addressId = $addressIds[$a];
			$localAddress->street = $streets[$a];
			$localAddress->areaCode = $codes[$a];
			$localAddress->city = $citys[$a];
			$localAddress->country = $countrys[$a];
			
			//$localAddress->printData();
			
			$owner->addresses[] = $localAddress;
	  		
		}
	}
	
	//$owner->printData();
	
	//echo "update existing client";
	$localError = updateOwnerData($owner);
	
	//STEP 1 - check if entry already exists
	$result = checkEntryExistsInTable("emailpreferences", "stateId", $owner->emailPreference->stateId,"","");
	//echo $result;
			
	//Step 2 - if exists perform update of existing one
	if($result>0)
	{
		updateEmailPreference($owner->emailPreference);
	}
	//Step 3 - if doesn't exist, insert new
	else
	{
		insertEmailPreference($owner->emailPreference);
	}
	
	
	
	/* set back the session variable to the login user */
	//$_SESSION['searchUserName'] = $_SESSION['username'];
	
	//$affectedRows=1 => data updated
	//$affectedRows=-1 => no data had to be updated					
	if($localError!= -1)
	{
		
		//insert note only on success
		if($_POST['notesDescription1'] != '' )
		{
			$note = new note();
			$note->entryDate = $_POST['notesDate1'];
			$note->type = $NOTE_TYPE_CLIENT;
			$note->description = $_POST['notesDescription1'];
			$note->parameterName = "stateId";
			$note->parameterValue = $owner->stateId;
			
			$noteId = insertNewNote($note);
			
		}
		
		//set new state id, if it was updated
		$_SESSION['searchStateId'] = $owner->stateId;
		
		//delete all address
		deleteFromTable('owneraddress', 'stateId', $owner->stateId);
		
		//insert again new/updated addresses
		foreach($owner->addresses as $eachAddress)
		{
			//$ownerAddress = fillOwnerAddressClass($owner);
			//$ownerAddress->printData();
			//insertOwnerAddress($eachAddress);
			insertOwnerAddress($eachAddress);
		}
		
		//update LICENSE table - only for OFFICE product type this is send
		if(isset($_POST['licenseType']))
		{
			$license = new license();
			$license->licenseType = $_POST['licenseType'];
			$license->licenseDate = $_POST['licenseIssueDate'];
			$license->licenseCountry = $_POST['licenseCountry'];
			$license->oldStateId = $owner->oldStateId;
			$license->stateId = $owner->stateId;
			//$license->printData();
			$error = insertOwnerLicense($license);
		}

		?>			
		Customer Data updated correctly. 
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="updateClientDataForm" method="POST" style="display: none;">
		<input type="text" name="action" value="updateClientDataForm" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updateClientDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
		<?php
	}
	else
	{
		echo "Cannot update customer because of Error:".$localError->error.", Number:".$localError->errno;
		?>
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="updateClientDataForm" method="POST" style="display: none;">
		<input type="text" name="action" value="updateClientDataForm" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updateClientDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
		<?php
	}
}
else
{
?>
	Customer Data updated correctly. 
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="updateClientDataForm" method="POST" style="display: none;">
	<input type="text" name="action" value="updateClientDataForm" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('updateClientDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
	<?php
}
