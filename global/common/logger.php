<?php

/*
 * aristos aresti 26/6/2013
 * implement the logging functionality
 * 
 */
class logger
{
	protected $logFileHandler;
	protected $path;
	protected $file;
	protected $logFile;//file path and name
	protected $globalLogLevel;
	
	protected $maxFileSize = 1000000; //1 MByte
	
	/*
	 * $path :  full path
	 * $file :  file name
	 * $logLevel : global log level set in client/configurationFiles/config.xml
	 */
	function __construct($path, $file, $globalLogLevel)
	{
		$this->path = $path;
		$this->file = $file;
		$this->logFile = $path.$file;
		$this->logFileHandler = fopen($this->logFile, 'a') or die("can't open file".$this->logFile);
		$this->globalLogLevel = $globalLogLevel;
		$this->archiveFile();
		
	}
	
	function __destruct()
	{
		fclose($this->logFileHandler);
		$this->archiveFile();
	}
	
	/*
	 * if file is larger than some size, it archives it
	 */
	function archiveFile()
	{
		if(file_exists($this->logFile))
		{			
			if(filesize($this->logFile)>$this->maxFileSize)
			{
				fclose($this->logFileHandler);
				$archiveFileName = $this->path.date("Y_m_d_H_i_s").'.txt';
				rename($this->logFile, $archiveFileName);
				$this->logFileHandler = fopen($this->logFile, 'a') or die("can't open file");
			}
		}
		
	}
	
	/* writes to the error file
	 * $message = the message to write
	* $logLevel = one of the values defined in definitions.php
	*/
	function writeToFile($message, $logLevel)
	{
		$message = date("Y-m-d H:i:s").' '.$message;
		//todo. in XML we set an log level from 1-4
		//if systen log level read from XML >= $logLevel in function then show log
		if($this->globalLogLevel >= $logLevel )
			fwrite($this->logFileHandler, $message."\r\n");
	
	}
	
	
	
	
}