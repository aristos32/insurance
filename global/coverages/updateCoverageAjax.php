<?php

session_start();

// Set flag that this is a parent file.
define('_INC', 1);

//we have independent file load here.
//we need to move one level down
$globalFilesLocation = "..";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/database/insertDatalayers.php");
require_once($globalFilesLocation."/database/deleteDatalayers.php");
require_once($globalFilesLocation."/database/connectorHierarchy.php");
require_once($globalFilesLocation."/generalIncludes/globalFunctions.php");

$coverageInPolicyQuotation = new coveragesInPolicyQuotation();

$coverageInPolicyQuotation->code = $_GET['coverageId'];
$coverageInPolicyQuotation->saleId = $_GET['contractNumber'];
$coverageInPolicyQuotation->parameters[] = $_GET['param1'];
$coverageInPolicyQuotation->parameters[] = $_GET['param2'];
$coverageInPolicyQuotation->parameters[] = $_GET['param3'];
$insertOrDelete = $_GET['checked'];

//$coverageInPolicyQuotation->printData();

//echo $insertOrDelete;

//Add coverage in policy
if($insertOrDelete=='true')
{
	//echo "INSIDE INSERT";

	$affectedRows = insertCoverageInPolicy($coverageInPolicyQuotation);
}
//Delete coverage from policy
else
{
	//echo "INSIDE DELETE";

	$affectedRows = deleteCoverageFromPolicy($coverageInPolicyQuotation->saleId, $coverageInPolicyQuotation->code);
}


//$affectedRows = insertOrDeleteCoverage($coverageInPolicyQuotation, $insertOrDelete)->affectedRows;

$_SESSION['affectedRows'] = $affectedRows;

if($_SESSION['affectedRows']!=-1)
	echo "Coverage Updated";
else
	echo "Coverage Cannot be Updated";

?>