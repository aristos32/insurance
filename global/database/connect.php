<?php
//General functions for connecting to the database	

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

function db_connect()
/*This function just opens a link identifier to the database we choose (that's $db_name) and returns the link to the db server.
***************************************************************************/
{
	//global $db_name, $dbhost, $dbusername, $dbpasswd;
	//writeToFile(date("Y-m-d H:i:s"). ": db_connect() ENTER \n");
		
	//echo "db_name=".$_SESSION['db_name'].", dbhost=". $_SESSION['dbhost']. ", dbusername=" .$_SESSION['dbusername']. ", dbpasswd=" .$_SESSION['dbpasswd']. "<br>";
	$mysqli = mysqli_connect($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'],$_SESSION['db_name']);
	$mysqli->set_charset("utf8");
	
	/* check connection */
	if (mysqli_connect_errno()) {
	    echo "Connect failed. ERROR: mysqli_connect_error() \n";
	    //$logMessage = date("Y-m-d H:i:s"). "Connect failed. ERROR: mysqli_connect_error() \n";
	    //fwrite($logFileHandler, $logMessage);
	    //$logMessage = date("Y-m-d H:i:s"). "db_name=$db_name, dbhost=$dbhost, dbusername=$dbusername, dbpasswd=$dbpasswd \n";
	    //fwrite($logFileHandler, $logMessage);
            //writeToFile(date("Y-m-d H:i:s"). "Connect failed. ERROR: mysqli_connect_error() \n", LOG_LEVEL_ERROR);
	    exit();
	}
	
	//echo "Host information: $mysqli->host_info \n";
	
	/* close connection */
	//$mysqli->close();
	//$logMessage = date("Y-m-d H:i:s"). ": db_connect() EXIT \n";
	//writeToFile(date("Y-m-d H:i:s"). ": db_connect() EXIT \n", LOG_LEVEL_INFO);
	        
	return $mysqli;
}


	
	
?>