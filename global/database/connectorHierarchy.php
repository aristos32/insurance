<?php

/* CLASSES NEEDED FOR DATABASE ACCESS */

abstract class baseConnector {
	
	//sugar database
	protected $host;//database location
	protected $user;//database username
	protected $pass;//database password
	protected $db_name;//database name
		
	protected $connection;
	
	protected $sql = '';
	protected $bindParam = null;//array containing the bind parameters for input in query
	protected $bindResult = null;//array containing the bind parameters output from query
	protected $statement = null;//stores the results of database query
	protected $num_of_rows; //Return the number of rows in statements result set
	protected $results = null;
	
	function __construct($host, $user, $pass, $db_name) {
				
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->db_name = $db_name;
		
		$this->bindParam = new BindParam(); 
		$this->bindResult = new BindParam();
		$this->results = array();
		
    }
	
		
	/* bind the fields to be returned in result set */
	public function setBindResult($bindParamsArray)
	{
		foreach($bindParamsArray as $eachBindParam)
		{
			//echo $eachBindParam."<br/>";
			$this->bindResult->add('s', $eachBindParam); 
		}
	
	}
	
	public function getStatement()
	{
		return $this->statement;
	}
		
	//must return a $resultSet, or true, or false. To handle inserts or selects or updates
	public abstract function executeStatement();
	
	public function fetchResult()
	{
		return $this->results;
	}
	
	
}

/* connect to mySQL database using a PDO connection
   Usage:
   $pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
   $statementString = "select date_FORMAT(t.transDate,'%c') as month, sum(t.debit) as production from sale s,transaction t where s.saleId=t.saleId and s.producer like '".$producer."' and (t.details like 'RENEWAL' OR t.details like 'NEW') and date_FORMAT(t.transDate,'%Y') like ? group by date_FORMAT(t.transDate,'%c') order by date_FORMAT(t.transDate,'%m') asc";
   $pdoConnection->setSQLStatement($statementString);
   $pdoConnection->setBindParams(array(array(PDO::PARAM_INT=> $param1),array(PDO::PARAM_STR=> $param2)));
   $pdoConnection->executeStatement();
   $results = $pdoConnection->fetchAll();
   foreach($results as $eachResult)
        $variable = $eachResult['variablename']
 */
class PDOConnector extends baseConnector {

	function __construct($host, $user, $pass, $db_name)
	{
		parent::__construct($host, $user, $pass, $db_name);
		//create database connection
		try{
			$this->connection = new PDO('mysql:host='.$this->host.';dbname='.$this->db_name.';charset=utf8', $this->user, $this->pass,array(PDO::ATTR_EMULATE_PREPARES => false, 
                                                                                                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
		catch(PDOException $ex) {
			echo "An Error occured : ".$ex->getMessage();
			
			writeToFile(date("Y-m-d H:i:s"). ": An Error occured establishing a database connection : ".$ex->getMessage(), LOG_LEVEL_ERROR);
			echo "Error C100 occured during operation. Please contact support<br/>";
		}

	}
	
	function __destruct() {
		$this->connection = null;
	}
	
	//prepare the sql query
	public function setSQLStatement($sql)
	{
		try{
			$this->sql = $sql;		
			$this->statement = $this->connection->prepare($sql);
			return true;
		}
		catch(PDOException $ex) {
			writeToFile(date("Y-m-d H:i:s"). ": An Error occured on preparing statement $this->sql: ".$ex->getMessage(), LOG_LEVEL_ERROR);
			//echo ": An Error occured on preparing statement $this->sql: ".$ex->getMessage();
			echo "Error S100 occured during operation. Please contact support<br/>";
			return false;
		}
	}
	
	
	/* bind the fields to be send to query.
	 * $bindParamArray array of smaller 2-element arrays. 
	 * Example: array(array(PDO::PARAM_STR, $history->transDate),
					  array(PDO::PARAM_STR, $history->username))
	 * each inclusive array contains 2 elements
	 * element[0]=variable type, i.e PDO::PARAM_STR, or PDO::PARAM_INT
	 * element[1] = variable value */
	public function setBindParams($bindParamArray)
	{
		try{
			$i = 1;//number of variable
			foreach($bindParamArray as $eachBindParam )			
			{
				$this->statement->bindValue($i, $eachBindParam[1], $eachBindParam[0]);
				$i++;
			}
			return true;
		}
		catch(PDOException $ex) {
			writeToFile(date("Y-m-d H:i:s"). ": An Error occured on binding parameters for statement $this->sql: ".$ex->getMessage(), LOG_LEVEL_ERROR);
			echo "Error B100 occured during operation. Please contact support<br/>";
			return false;
		}
		
		//$this->bindParam = null;
	
		//$this->bindParam = $bindParam;
	}
	
	
	/* Execute and handle an sql statement */
	function executeStatement()
	{
		try{
			$this->statement->execute();
		}	
		catch(PDOException $ex) {
			writeToFile(date("Y-m-d H:i:s"). ": An Error occured on executing statement $this->sql: ".$ex->getMessage(), LOG_LEVEL_ERROR);
			echo "Error E100 occured during operation. Please contact support<br/>";
			return -1;
		}
		//affected rows
		return $this->statement->rowCount();
	}
	
	/* Execute and handle an sql statement */
	function fetchAll()
	{
		try{
			$results = $this->statement->fetchAll(PDO::FETCH_ASSOC);
			return $results;
		}
		catch(PDOException $ex) {
			writeToFile(date("Y-m-d H:i:s"). ": An Error occured on fetching results statement $this->sql: ".$ex->getMessage(), LOG_LEVEL_ERROR);
			echo "Error E100 occured during operation. Please contact support<br/>";	
			return false;		
		}
	}
	
	/* Execute and handle an sql statement */
	function fetch()
	{
		try{
			$results = $this->statement->fetch(PDO::FETCH_ASSOC);
			return $results;
		}
		catch(PDOException $ex) {
			writeToFile(date("Y-m-d H:i:s"). ": An Error occured on fetching results statement $this->sql: ".$ex->getMessage(), LOG_LEVEL_ERROR);
			echo "Error E101 occured during operation. Please contact support<br/>";	
			return false;		
		}
	}
	
	/* get the last insert id */
	function getLastInsertId()
	{
		return $this->connection->lastInsertId();
	}
	
	/* begin transaction */
	function beginTransaction()
	{
		return $this->connection->beginTransaction();
	}
	
	/* begin transaction */
	function commit()
	{
		return $this->connection->commit();
	}
	
	/* rollBack transaction */
	function rollBack()
	{
		return $this->connection->rollBack();
	}
	

}

/* specific to MySQL database */
class mySQLConnector extends baseConnector {

	function __construct($host, $user, $pass, $db_name)
	{
		parent::__construct($host, $user, $pass, $db_name);
	//database connection
		$this->connection = mysqli_connect($this->host, $this->user, $this->pass, $this->db_name);

		if($this->connection)
		{
			;//echo "Connection to $this->host established...<br />";
		}
		else{
			echo "Connection to $host could not be established.<br />";
			$logMessage = date("Y-m-d H:i:s"). ": Connection to $host could not be established due to error:".mysqli_connect_error()." \n";
			fwrite($this->logFileHandler, $logMessage);
			 
			die('Connect Error (' . mysqli_connect_errno() . ') '
					. mysqli_connect_error());
		}
		
	}
	
	public function setSQLStatement($sql)
	{
		$this->sql = $sql;
	}
	
	
	/* bind the fields to be send to query */
	public function setBindParams($bindParam)
	{
		$this->bindParam = null;
	
		$this->bindParam = $bindParam;
	}
	
	/* implement mysql transactions.
		$option: transaction option( "start transaction;", "commit;", "rollback;" ) */
	function transaction($option)
	{
		mysqli_query($this->connection, $option);
	}
	
	
	/* Execute and handle an sql statement */
	function executeStatement()
	{
		global $logFileHandler;
		// initialise some empty arrays
		$fields = array();
		
		$result=true;
		
		//prepared statement
		$this->statement = $this->connection->prepare($this->sql);
		
		if($this->statement == true)
		{
			//print_r(refValues($this->bindParam->get()));
			
			if($this->bindParam->getParamValues()!=null && count($this->bindParam->getParamValues())>0 )
			{
				//print_r(refValues($this->bindParam->get()));
				
				//Returns the return value of the callback, or FALSE on error.
				$ok = call_user_func_array( array($this->statement, 'bind_param'), refValues($this->bindParam->get()));
				if($ok==false)
				{
					die("failure during bind_param call for statement: $this->sql. Error =" . mysqli_error($this->connection) . " <br/>");
				}
			}
			else 
				$ok = true;

			//$this->statement->execute() - Returns TRUE on success or FALSE on failure
			if ($this->statement->execute())
			{
								
				// Get metadata in order to retrieve dynamically the field names, for use in bind_result
				//Returns a result object or FALSE if an error occurred.
				//if no result set, returns FALSE
				$meta = $this->statement->result_metadata();
				
				//return some result
				if($meta!=false)
				{
					// Dynamically creating an array of variables to use to bind the results
					while ($field = $meta->fetch_field()) { 
						$var = $field->name; 
						$$var = null; 
						$fields[$var] = &$$var; 
					}
					
					// bind_result
					$ok = call_user_func_array( array($this->statement, 'bind_result'), $fields);
										
					if(!$ok)
						echo die("ERROR on bind_result");
				
					$i = 0;
					while ($this->statement->fetch()) {
						//echo "id=".$row['id'] ."<br/>";
						$this->results[$i] = array();
						foreach($fields as $k => $v)
							$this->results[$i][$k] = $v;
						$i++;
						
					}
				}
				//no result returned. for example in delete - don't die!!!
				else
				{
					;//die(date("Y-m-d H:i:s"). ". $this->sql failed with error=" . mysqli_error($this->connection));
				}
	
				//echo "Execution Success <br/>" ;
			}
			//failure on execute - record error
			else
			{
				//echo "$this->sql failed with error=" . $this->connection->error . " <br/>";
				$result=false;
				//$this->statement -> close();
				die(date("Y-m-d H:i:s"). ". $this->sql failed with Error No=" . $this->statement->errno . ", Error = " . $this->statement->error. " <br/>");
			}
			
			$this->num_of_rows = $this->statement->num_rows;	
			//echo "found number of rows:".$this->num_of_rows." <br/>";
		}
		else
		{			
			die(date("Y-m-d H:i:s"). ". $this->sql - Unable to prepare. Error No =" . $this->connection->errno . ", Error = " . $this->connection->error . " <br/>");
		}
		
		return $result;
	}
		
}


/* specific to MS SQL Server - TODO - IMPLEMENT
class MsSQLServerConnector extends baseConnector {
} */
?>