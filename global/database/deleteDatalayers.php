<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//FUNCTIONS THAT DELETE DATA FROM THE DATABASE
	

/* delete all the quotes for a given user that are not related to a sale.
Sale quoations will be deleted only when the sale is deleted.

Only need to delete from QUOTE table. The rest tables are deleted by CASCADE.
Parameters:
$currentUserName : the username the quotes need to be deleted */
function deleteAllQuotesNotRelatedToSale($currentUserName)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": deleteAllQuotesNotRelatedToSale() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "DELETE FROM quotation WHERE username=? and quoteId not in ( select distinct qo.quoteId from sale s, quotationowner qo where s.stateId=qo.stateId)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $currentUserName)));
	$error = $pdoConnection->executeStatement();
		
	writeToFile(date("Y-m-d H:i:s"). ": deleteAllQuotesNotRelatedToSale() EXIT \n", LOG_LEVEL_INFO);
	
	
}


/* GENERIC FUNCTION 
Delete all entries in the table that match the criteria of the field
Parameters:
$tableName : the table to be accessed
$fieldName : the field in the table to be compared
$fieldValue : any field with this value, the record will be deleted */
function deleteFromTable($tableName, $fieldName, $fieldValue)
{
    
	writeToFile(date("Y-m-d H:i:s"). ": deleteFromTable() ENTER. table name=$tableName, field name=$fieldName, field value=$fieldValue \n", LOG_LEVEL_INFO);	

	$statementString = "DELETE FROM ".$tableName." WHERE ".$fieldName."=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $fieldValue)));
	$affectedRows = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": deleteFromTable() EXIT \n", LOG_LEVEL_INFO);
        
	return $affectedRows;
	
}


/* delete coverage from policy

$contractNumber : policy
$coverageCode: code of the coverage as in coveragesDefinitions.xml

*/

function deleteCoverageFromPolicy($contractNumber, $coverageCode)
{
	
	writeToFile(date("Y-m-d H:i:s"). ": deleteCoverageFromPolicy() ENTER \n", LOG_LEVEL_INFO);
	
	$statementString = "DELETE FROM coveragesinpolicy WHERE saleId=? and code=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $contractNumber),
			array(PDO::PARAM_STR, $coverageCode)));
	$error = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": deleteCoverageFromPolicy() EXIT \n", LOG_LEVEL_INFO);
	return $error;
	//$localError->printData();
	
}


