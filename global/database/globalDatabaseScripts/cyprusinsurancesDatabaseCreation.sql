create database cyprusinsurances;

show databases;

use cyprusinsurances;

show tables;


insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("aristos33", "aristos.aresti@gmail.com", "764490", "Aristos", "Aresti", "25331668", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("kat", "aristos.aresti@gmail.com", "764490", "Katerina", "Aresti", "25352283", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("thios", "aristos.aresti@gmail.com", "764490", "Theodoros", "Pantelidis", "25352283", "NO");
