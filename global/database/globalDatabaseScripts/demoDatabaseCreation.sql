create database demoinsurances;

show databases;

use demoinsurances;

show tables;


insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("demo", "demo.demo@gmail.com", "999999", "Demo", "Demo", "12345678", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("ivi", "ivi.ivi@gmail.com", "999999", "Ivi", "Athinakis", "12345678", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("umbrella", "umbrella.umbrella@gmail.com", "999999", "umbrella", "umbrella", "12345678", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("kazakhstan", "kazakhstan.kazakhstan@gmail.com", "999999", "kazakhstan", "kazakhstan", "12345678", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("commercial", "commercial.commercial@gmail.com", "999999", "commercial", "commercial", "12345678", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("progressive", "progressive.progressive@gmail.com", "999999", "progressive", "progressive", "12345678", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("vournaris", "vournaris.vournaris.com", "999999", "vournaris", "vournaris", "12345678", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("antoniou", "antoniou.antoniou.com", "999999", "antoniou", "antoniou", "12345678", "YES");
insert into systemuser(username, email, stateId, firstName, lastName, telephone, producer ) values ("sica", "sica.sica.com", "999999", "Sica", "Sica", "12345678", "YES");