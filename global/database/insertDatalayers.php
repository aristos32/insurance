<?php
//FUNCTIONS THAT INSERT DATA IN THE DATABASE

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');		
	


/* delete all existing drivers and insert new updated ones
	*/
function insertAdditionalDrivers($drivers, $saleId)
{

	$driverIds = '';//store all IDs of drivers for history records	
	global  $HISTORY_TYPE_CONTRACT;
	
        
    writeToFile(date("Y-m-d H:i:s"). ": insertAdditionalDrivers() ENTER \n", LOG_LEVEL_INFO);
        
	//Step 1 - Delete existing drivers
	deleteFromTable('drivers', 'saleId', $saleId);
	
	//Step 2 - Insert new and updated drivers
	foreach($drivers as $eachDriver)
	{
		$driverIds = $driverIds.",".$eachDriver->stateId;
		insertAdditionalDriver($eachDriver); 
	}
	

	/* insert history only on success */
	$localHistory = new history();
	$localHistory->username = $_SESSION['username'];
	$localHistory->type = $HISTORY_TYPE_CONTRACT;
	$localHistory->parameterName = "saleId";
	$localHistory->parameterValue = $saleId;
	$localHistory->note = "$localHistory->username : Contract with $localHistory->parameterName = $localHistory->parameterValue. Delete all drivers. Adding drivers with IDs=$driverIds";
	insertNewHistory($localHistory);
	  
	writeToFile(date("Y-m-d H:i:s"). ": insertAdditionalDrivers() EXIT \n", LOG_LEVEL_INFO);
	
}

//The problem is that 5.3 requires array values as reference while 5.2 works with real values. 
function refValues($arr){ 
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+ 
    { 
        $refs = array(); 
        foreach($arr as $key => $value) 
            $refs[$key] = &$arr[$key]; 
        return $refs; 
    } 
    return $arr; 
} 

/* insert new user in the database */
function insertNewUser($user)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertNewUser() ENTER \n", LOG_LEVEL_INFO);
	$user->birthDate = date("Y-m-d H:i:s", strtotime($user->birthDate));
	$user->licenseIssueDate = date("Y-m-d H:i:s", strtotime($user->licenseIssueDate));
	
	$statementString = "INSERT INTO systemuser(username, firstName, lastName, email, stateId, producer, profession, birthDate, licenseIssueDate) VALUES(?,?,?,?,?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $user->username),
			array(PDO::PARAM_STR, $user->firstName),
			array(PDO::PARAM_STR, $user->lastName),
			array(PDO::PARAM_STR, $user->email),
			array(PDO::PARAM_STR, $user->stateId),
			array(PDO::PARAM_STR, $user->producer),
			array(PDO::PARAM_STR, $user->profession),
			array(PDO::PARAM_STR, $user->birthDate),
			array(PDO::PARAM_STR, $user->licenseIssueDate)));
	$pdoConnection->executeStatement();
	
	
	
      

      //insert also owner and all addresses only when stateId is filled      
      //28/12/2012 - removed. stateId is always not empty
      //if($user->stateId != '' )
      //{
	      //insert owner - only because addresses is dependent on stateId
	      //set dummy name
	      if($user->firstName=='')
	      	$user->firstName = 'John';
	      if($user->lastName=='')
	      	$user->lastName = 'Doe';
	      $owner = fillOwnerClass($user);
	      insertOwner($owner);
	      
	      //insert addresses
      		//$user->printData();
	      foreach($user->addresses as $eachAddress)
	      	insertOwnerAddress($eachAddress);
  	  //}

	writeToFile(date("Y-m-d H:i:s"). ": insertNewUser() EXIT \n", LOG_LEVEL_INFO);  	
}

/* insert new transactions in the database */
function insertNewTransaction($transaction)
{
	$affectedRows = 0;
	$transId = -1;
		
	writeToFile(date("Y-m-d H:i:s"). ": insertNewTransaction() ENTER \n", LOG_LEVEL_INFO);

	//convert string to correct date formmat - Y-m-d H:i:s is needed to store in the database
	$transDate = date("Y-m-d", strtotime($transaction->transDate))." ".date("H:i:s", time()); //working previously TODO test again!;
	
	$statementString = "INSERT INTO transaction(transDate, details, receiptNo, debit, credit, remainder, saleId, producer) VALUES(?,?,?,?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $transDate),
			array(PDO::PARAM_STR, $transaction->details),
			array(PDO::PARAM_STR, $transaction->receiptNo),
			array(PDO::PARAM_STR, $transaction->debit),
			array(PDO::PARAM_STR, $transaction->credit),
			array(PDO::PARAM_STR, $transaction->remainder),
			array(PDO::PARAM_STR, $transaction->saleId),
			array(PDO::PARAM_STR, $transaction->producer)));
	
	$pdoConnection->executeStatement();
	
	$transId = $pdoConnection->getLastInsertId();
 	
	writeToFile(date("Y-m-d H:i:s"). ": insertNewTransaction() EXIT \n", LOG_LEVEL_INFO);  	
	//return $affectedRows;
	//return $transId;
	return $transId;
}


/* insert new note in the database */
function insertNewNote($note)
{

	$noteId = -1;

	writeToFile(date("Y-m-d H:i:s"). ": insertNewNote() ENTER \n", LOG_LEVEL_INFO);
	
	//add current time at the end of date
	$note->entryDate = $note->entryDate." ".date("H:i:s", time());
	
	//convert string to correct date formmat - Y-m-d H:i:s is needed to store in the database
	$entryDate = date("Y-m-d H:i:s", strtotime($note->entryDate));
	
	$statementString = "INSERT INTO notes(entryDate, type, description, parameterName, parameterValue ) VALUES(?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $entryDate),
			array(PDO::PARAM_STR, $note->type),
			array(PDO::PARAM_STR, $note->description),
			array(PDO::PARAM_STR, $note->parameterName),
			array(PDO::PARAM_STR, $note->parameterValue)));
	
	$pdoConnection->executeStatement();
	
	$noteId = $pdoConnection->getLastInsertId();

	writeToFile(date("Y-m-d H:i:s"). ": insertNewNote() EXIT \n", LOG_LEVEL_INFO);  	
	//return $affectedRows;
	//return $noteId;
	return $noteId;
}


/* insert new claim in the database */
function insertNewClaim($claim)
{
	$affectedRows = 0;
	$claimId = -1;
	
	      
	writeToFile(date("Y-m-d H:i:s"). ": insertNewClaim() ENTER \n", LOG_LEVEL_INFO);
        
		
	//add current time at the end of date
	$claim->entryDate = $claim->claimDate." ".date("H:i:s", time());
	
	//convert string to correct date formmat - Y-m-d H:i:s is needed to store in the database
	$entryDate = date("Y-m-d H:i:s", strtotime($claim->claimDate));
	
	$statementString = "INSERT INTO claims(claimDate, amount, description, quoteId, stateId) VALUES(?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $entryDate),
			array(PDO::PARAM_STR, $claim->amount),
			array(PDO::PARAM_STR, $claim->description),
			array(PDO::PARAM_STR, $claim->quoteId),
			array(PDO::PARAM_STR, $claim->stateId)));
	
	$pdoConnection->executeStatement();
	      
      $claimId = $pdoConnection->getLastInsertId(); 
      


	writeToFile(date("Y-m-d H:i:s"). ": insertNewClaim() EXIT \n", LOG_LEVEL_INFO);
        
	//return $affectedRows;
	//return $claimId;
	return $claimId;
}


/* insert new user in the database */
function insertNewUserInGlobalDatabase($user)
{
	writeToFile(date("Y-m-d H:i:s"). ": insertNewUserInGlobalDatabase() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO systemuser(username, password, role, status, clientName, productType, consecutiveFailLoginAttempts) VALUES(?,?,?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $user->username),
			array(PDO::PARAM_STR, $user->password),
			array(PDO::PARAM_STR, $user->role),
			array(PDO::PARAM_STR, $user->status),
			array(PDO::PARAM_STR, $user->clientName),
			array(PDO::PARAM_STR, $user->productType),
			array(PDO::PARAM_STR, $user->consecutiveFailLoginAttempts)));
	
	$error = $pdoConnection->executeStatement();

	writeToFile(date("Y-m-d H:i:s"). ": insertNewUserInGlobalDatabase() EXIT \n", LOG_LEVEL_INFO);
        
	//return $affectedRows;
	return $error;
}

/* insert new statistic in the database */
function insertStatistics($statistics)
{
	global $USER_ROLE_CUSTOMER;  
        
	writeToFile(date("Y-m-d H:i:s"). ": insertStatistics() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO statistics(code, value) VALUES(?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $statistics->code),
			array(PDO::PARAM_STR, $statistics->value)));
	
	$error = $pdoConnection->executeStatement();

	//return $affectedRows;
	return $error;
}


/* insert sale/contract  information in the SALE table in database */
function insertSaleInfo($sale)
{
	global $HISTORY_TYPE_CONTRACT;
       
	writeToFile(date("Y-m-d H:i:s"). ": insertSaleInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO sale(saleId, company, insuranceType, coverageType, startDate, endDate, associate, stateId, producer, status ) VALUES(?,?,?,?,?,?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $sale->saleId),
			array(PDO::PARAM_STR, $sale->company),
			array(PDO::PARAM_STR, $sale->insuranceType),
			array(PDO::PARAM_STR, $sale->coverageType),
			array(PDO::PARAM_STR, $sale->saleStartDate),
			array(PDO::PARAM_STR, $sale->saleEndDate),
			array(PDO::PARAM_STR, $sale->associate),
			array(PDO::PARAM_STR, $sale->stateId),
			array(PDO::PARAM_STR, $sale->producer),
			array(PDO::PARAM_STR, $sale->status)));
	
	 $error = $pdoConnection->executeStatement();
	
      
   
      /* insert history only on success */
      $localHistory = new history();
      $localHistory->username = $_SESSION['username'];
      $localHistory->type = $HISTORY_TYPE_CONTRACT;
      $localHistory->parameterName = "saleId";
      $localHistory->parameterValue = $sale->saleId;
      $localHistory->note = "$localHistory->username : added a new contract with $localHistory->parameterName = $localHistory->parameterValue";
      insertNewHistory($localHistory);
      
        
	//no error occured if we reach this point
	return $error;
	
}


/* insert new coverage in table COVERAGESINPOLICY.
$coverageInPolicyQuotation : coverage to insert */
function insertCoverageInPolicy($coverageInPolicyQuotation)
{
		
	writeToFile(date("Y-m-d H:i:s"). ": insertCoverageInPolicy() ENTER \n", LOG_LEVEL_INFO);

	//initialize
	$param1 = $param2 = $param3 = "";
	$i=1;
	//dynamically assign values
	foreach($coverageInPolicyQuotation->parameters as $eachParameter)
	{
		${'param'.$i} = $eachParameter;
		//echo $i.":".${'param'.$i};
		$i++;
	}
	
	$statementString = "INSERT INTO coveragesinpolicy(saleId, code, param1, param2, param3) VALUES(?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $coverageInPolicyQuotation->saleId),
			array(PDO::PARAM_STR, $coverageInPolicyQuotation->code),
			array(PDO::PARAM_STR, $param1),
			array(PDO::PARAM_STR, $param2),
			array(PDO::PARAM_STR, $param3)));
	
	$error = $pdoConnection->executeStatement();
		
	writeToFile(date("Y-m-d H:i:s"). ": insertCoverageInPolicy() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* insert new coverage in table COVERAGESINQUOTATION */
function insertCoverageInQuotation($quoteId, $coveragesInPolicyQuotation)
{
     
	writeToFile(date("Y-m-d H:i:s"). ": insertCoverageInQuotation() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO coveragesinquotation(quoteId, code, param1, param2, param3, description, decision, charge) VALUES(?,?,?,?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId),
			array(PDO::PARAM_STR, $coveragesInPolicyQuotation->code),
			array(PDO::PARAM_STR, $coveragesInPolicyQuotation->parameters[0]),
			array(PDO::PARAM_STR, $coveragesInPolicyQuotation->parameters[1]),
			array(PDO::PARAM_STR, $coveragesInPolicyQuotation->parameters[2]),
			array(PDO::PARAM_STR, $coveragesInPolicyQuotation->description),
			array(PDO::PARAM_STR, $coveragesInPolicyQuotation->decision),
			array(PDO::PARAM_STR, $coveragesInPolicyQuotation->charge)));
	
	$error = $pdoConnection->executeStatement();
	

	writeToFile(date("Y-m-d H:i:s"). ": insertCoverageInQuotation() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}



/* insert new discount in table QUOTATION_DISCOUNT */
function insertDiscountInQuotation($quoteId, $discountInQuotation)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertDiscountInQuotation() ENTER \n", LOG_LEVEL_INFO);
        
	     
	$statementString = "INSERT INTO quotation_discounts(quoteId, code, param1, description, charge) VALUES(?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId),
			array(PDO::PARAM_STR, $discountInQuotation->code),
			array(PDO::PARAM_STR, $discountInQuotation->parameters[0]),			
			array(PDO::PARAM_STR, $discountInQuotation->description),
			array(PDO::PARAM_STR, $discountInQuotation->charge)));
	
	$error = $pdoConnection->executeStatement();
	
	
	writeToFile(date("Y-m-d H:i:s"). ": insertDiscountInQuotation() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}

/* insert new Charge in table CHARGESINQUOTATION */
function insertChargeInQuotation($quoteId, $chargesInPolicyQuotation)
{
	if(!isset($chargesInPolicyQuotation->parameters[0]))
		$chargesInPolicyQuotation->parameters[0] = null;
	
	if(!isset($chargesInPolicyQuotation->parameters[1]))
		$chargesInPolicyQuotation->parameters[1] = null;
	
	if(!isset($chargesInPolicyQuotation->parameters[2]))
		$chargesInPolicyQuotation->parameters[2] = null;
	
	
	writeToFile(date("Y-m-d H:i:s"). ": insertChargeInQuotation() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO chargesinquotation(quoteId, code, param1, param2, param3, description, charge) VALUES(?,?,?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId),
			array(PDO::PARAM_STR, $chargesInPolicyQuotation->code),
			array(PDO::PARAM_STR, $chargesInPolicyQuotation->parameters[0]),
			array(PDO::PARAM_STR, $chargesInPolicyQuotation->parameters[1]),
			array(PDO::PARAM_STR, $chargesInPolicyQuotation->parameters[2]),
			array(PDO::PARAM_STR, $chargesInPolicyQuotation->description),
			array(PDO::PARAM_STR, $chargesInPolicyQuotation->charge)));
	
	$error = $pdoConnection->executeStatement();
	
	
	writeToFile(date("Y-m-d H:i:s"). ": insertChargeInQuotation() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* insert new address in OWNERADDRESS table */
function insertOwnerAddress($ownerAddress)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertOwnerAddress() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO owneraddress(addressId, addressType, street, areaCode, city, state, country ,stateId) VALUES(?,?,?,?,?,?,?,?)
			on duplicate key update addressType=?, street=?, areaCode=?, city=?, state=?, country=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $ownerAddress->addressId),
			array(PDO::PARAM_STR, $ownerAddress->addressType),
			array(PDO::PARAM_STR, $ownerAddress->street),
			array(PDO::PARAM_STR, $ownerAddress->areaCode),
			array(PDO::PARAM_STR, $ownerAddress->city),
			array(PDO::PARAM_STR, $ownerAddress->state),
			array(PDO::PARAM_STR, $ownerAddress->country),
			array(PDO::PARAM_STR, $ownerAddress->stateId),
			array(PDO::PARAM_STR, $ownerAddress->addressType),
			array(PDO::PARAM_STR, $ownerAddress->street),
			array(PDO::PARAM_STR, $ownerAddress->areaCode),
			array(PDO::PARAM_STR, $ownerAddress->city),
			array(PDO::PARAM_STR, $ownerAddress->state),
			array(PDO::PARAM_STR, $ownerAddress->country)));
	
	 $error = $pdoConnection->executeStatement();
	
	
	writeToFile(date("Y-m-d H:i:s"). ": insertOwnerAddress() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* insert new vehicle in VEHICLE table */
function insertVehicle($vehicle)
{
   writeToFile(date("Y-m-d H:i:s"). ": insertVehicle() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO vehicle(regNumber, vehicleType, make, model, cubicCapacity, manufacturedYear, sumInsured, vehicleDesign, steeringWheelSide,
  	isTaxFree, isUsedForDeliveries, hasVisitorPlates, saleId ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)
  	on duplicate key update vehicleType=?, make=?, model=?, cubicCapacity=?, manufacturedYear=?, sumInsured=?,
			vehicleDesign=?, steeringWheelSide=?, isTaxFree=?, isUsedForDeliveries=?, hasVisitorPlates=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $vehicle->regNumber),
			array(PDO::PARAM_STR, $vehicle->vehicleType),
			array(PDO::PARAM_STR, $vehicle->make),
			array(PDO::PARAM_STR, $vehicle->model),
			array(PDO::PARAM_STR, $vehicle->cubicCapacity),
			array(PDO::PARAM_STR, $vehicle->manufacturedYear),
			array(PDO::PARAM_STR, $vehicle->sumInsured),
			array(PDO::PARAM_STR, $vehicle->vehicleDesign),
			array(PDO::PARAM_STR, $vehicle->steeringWheelSide),
			array(PDO::PARAM_STR, $vehicle->isTaxFree),
			array(PDO::PARAM_STR, $vehicle->isUsedForDeliveries),
			array(PDO::PARAM_STR, $vehicle->hasVisitorPlates),
			array(PDO::PARAM_STR, $vehicle->saleId),
			array(PDO::PARAM_STR, $vehicle->vehicleType),
			array(PDO::PARAM_STR, $vehicle->make),
			array(PDO::PARAM_STR, $vehicle->model),
			array(PDO::PARAM_STR, $vehicle->cubicCapacity),
			array(PDO::PARAM_STR, $vehicle->manufacturedYear),
			array(PDO::PARAM_STR, $vehicle->sumInsured),
			array(PDO::PARAM_STR, $vehicle->vehicleDesign),
			array(PDO::PARAM_STR, $vehicle->steeringWheelSide),
			array(PDO::PARAM_STR, $vehicle->isTaxFree),
			array(PDO::PARAM_STR, $vehicle->isUsedForDeliveries),
			array(PDO::PARAM_STR, $vehicle->hasVisitorPlates)));
	
	 $error = $pdoConnection->executeStatement();
	
	

	writeToFile(date("Y-m-d H:i:s"). ": insertVehicle() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}



/* insert new vehicle in EMPLOYERSLIABILITY table */
function insertEmployersLiability($employersLiability)
{

	writeToFile(date("Y-m-d H:i:s"). ": insertEmployersLiability() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO employersliability(saleId, employersSocialInsuranceNumber, limitPerEmployee, limitPerEventOrSeriesOfEvents, limitDuringPeriodOfInsurance, employeesNumber, estimatedTotalGrossEarnings ) VALUES(?,?,?,?,?,?,?)
			on duplicate key update employersSocialInsuranceNumber=?, limitPerEmployee=?, limitPerEventOrSeriesOfEvents=?, limitDuringPeriodOfInsurance=?, employeesNumber=?, estimatedTotalGrossEarnings=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $employersLiability->saleId),
			array(PDO::PARAM_STR, $employersLiability->employersSocialInsuranceNumber),
			array(PDO::PARAM_STR, $employersLiability->limitPerEmployee),
			array(PDO::PARAM_STR, $employersLiability->limitPerEventOrSeriesOfEvents),
			array(PDO::PARAM_STR, $employersLiability->limitDuringPeriodOfInsurance),
			array(PDO::PARAM_STR, $employersLiability->employeesNumber),
			array(PDO::PARAM_STR, $employersLiability->estimatedTotalGrossEarnings),
			array(PDO::PARAM_STR, $employersLiability->employersSocialInsuranceNumber),
			array(PDO::PARAM_STR, $employersLiability->limitPerEmployee),
			array(PDO::PARAM_STR, $employersLiability->limitPerEventOrSeriesOfEvents),
			array(PDO::PARAM_STR, $employersLiability->limitDuringPeriodOfInsurance),
			array(PDO::PARAM_STR, $employersLiability->employeesNumber),
			array(PDO::PARAM_STR, $employersLiability->estimatedTotalGrossEarnings)));
	
	 $error = $pdoConnection->executeStatement();
	
	
	writeToFile(date("Y-m-d H:i:s"). ": insertEmployersLiability() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* insert new vehicle in MEDICAL and MEDICALINSUREDPERSON tables */
function insertMedical($medical)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertMedical() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO medical(saleId, frequencyOfPayment, premium, planName, planMaximumLimit, deductible, excess, coInsurancePercentage, roomType, outpatientAmount ) VALUES(?,?,?,?,?,?,?,?,?,?)
			 on duplicate key update frequencyOfPayment=?, premium=?, planName=?, planMaximumLimit=?, deductible=?, excess=?, coInsurancePercentage=?, roomType=?, outpatientAmount=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $medical->saleId),
			array(PDO::PARAM_STR, $medical->frequencyOfPayment),
			array(PDO::PARAM_STR, $medical->premium),
			array(PDO::PARAM_STR, $medical->planName),
			array(PDO::PARAM_STR, $medical->planMaximumLimit),
			array(PDO::PARAM_STR, $medical->deductible),
			array(PDO::PARAM_STR, $medical->excess),
			array(PDO::PARAM_STR, $medical->coInsurancePercentage),
			array(PDO::PARAM_STR, $medical->roomType),
			array(PDO::PARAM_STR, $medical->outpatientAmount),
			array(PDO::PARAM_STR, $medical->frequencyOfPayment),
			array(PDO::PARAM_STR, $medical->premium),
			array(PDO::PARAM_STR, $medical->planName),
			array(PDO::PARAM_STR, $medical->planMaximumLimit),
			array(PDO::PARAM_STR, $medical->deductible),
			array(PDO::PARAM_STR, $medical->excess),
			array(PDO::PARAM_STR, $medical->coInsurancePercentage),
			array(PDO::PARAM_STR, $medical->roomType),
			array(PDO::PARAM_STR, $medical->outpatientAmount)));
	
	 $error = $pdoConnection->executeStatement();
	

	 /*delete all previous insured persons */
	 //deleteFromTable("medicalinsuredperson", "saleId", $medical->oldSaleId);
	 
	 /* insert all insured persons */
	 foreach($medical->medicalInsuredPersonsArray as $eachMedicalInsuredPerson)
	 {
	 	insertMedicalInsuredPerson($eachMedicalInsuredPerson);
	 	//$eachMedicalInsuredPerson->printData();
	 }
	 
     /* insert coverages in coveragesInPolicy */
     foreach($medical->coveragesInPolicyArray as $eachCoverage)
      	insertCoverageInPolicy($eachCoverage);
  	
	writeToFile(date("Y-m-d H:i:s"). ": insertMedical() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* insert in LIFE */
function insertLifeIns($lifeIns)
{

	writeToFile(date("Y-m-d H:i:s"). ": insertLifeIns() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO lifeins(saleId, insuredFirstName, insuredLastName, frequencyOfPayment, annualPremium, monthlyPremium, planName, basicPlanAmount, totalPermanentDisabilityAmount, premiumProtectionAmount ) VALUES(?,?,?,?,?,?,?,?,?,?)
			on duplicate key update insuredFirstName=?, insuredLastName=?, frequencyOfPayment=?, annualPremium=?, monthlyPremium=?, planName=?, basicPlanAmount=?, totalPermanentDisabilityAmount=?, premiumProtectionAmount=? ";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $lifeIns->saleId),
			array(PDO::PARAM_STR, $lifeIns->insuredFirstName),
			array(PDO::PARAM_STR, $lifeIns->insuredLastName),
			array(PDO::PARAM_STR, $lifeIns->frequencyOfPayment),
			array(PDO::PARAM_STR, $lifeIns->annualPremium),
			array(PDO::PARAM_STR, $lifeIns->monthlyPremium),
			array(PDO::PARAM_STR, $lifeIns->planName),
			array(PDO::PARAM_STR, $lifeIns->basicPlanAmount),
			array(PDO::PARAM_STR, $lifeIns->totalPermanentDisabilityAmount),
			array(PDO::PARAM_STR, $lifeIns->premiumProtectionAmount),
			array(PDO::PARAM_STR, $lifeIns->insuredFirstName),
			array(PDO::PARAM_STR, $lifeIns->insuredLastName),
			array(PDO::PARAM_STR, $lifeIns->frequencyOfPayment),
			array(PDO::PARAM_STR, $lifeIns->annualPremium),
			array(PDO::PARAM_STR, $lifeIns->monthlyPremium),
			array(PDO::PARAM_STR, $lifeIns->planName),
			array(PDO::PARAM_STR, $lifeIns->basicPlanAmount),
			array(PDO::PARAM_STR, $lifeIns->totalPermanentDisabilityAmount),
			array(PDO::PARAM_STR, $lifeIns->premiumProtectionAmount)));
	
	$error = $pdoConnection->executeStatement();
	
      /* insert coverages in coveragesInPolicy */
      foreach($lifeIns->coveragesInPolicyArray as $eachCoverage)
      	insertCoverageInPolicy($eachCoverage);
      	
      if($lifeIns->endorsement!="")
      {
	      $endrosement = new endrosement();
	      $endrosement->saleId = $lifeIns->saleId;
	      $endrosement->code = "";
	      $endrosement->description = $lifeIns->endorsement;
      	 // insertEndorsementInPolicy($endrosement);
  	  }
  	
	writeToFile(date("Y-m-d H:i:s"). ": insertLifeIns() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* insert new vehicle in PROPERTYFIRE and ENDORSEMENT tables */
function insertPropertyFire($propertyFire)
{

	writeToFile(date("Y-m-d H:i:s"). ": insertPropertyFire() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO propertyfire(saleId, description, typeOfPremises, buildingValue, outsideFixturesValue, contentsValue, valuableObjectsValue, yearBuilt, areaSqMt ) VALUES(?,?,?,?,?,?,?,?,?)
			on duplicate key update description=?, typeOfPremises=?, buildingValue=?, outsideFixturesValue=?, contentsValue=?, valuableObjectsValue=?, yearBuilt=?, areaSqMt=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $propertyFire->saleId),
			array(PDO::PARAM_STR, $propertyFire->description),
			array(PDO::PARAM_STR, $propertyFire->typeOfPremises),
			array(PDO::PARAM_STR, $propertyFire->buildingValue),
			array(PDO::PARAM_STR, $propertyFire->outsideFixturesValue),
			array(PDO::PARAM_STR, $propertyFire->contentsValue),
			array(PDO::PARAM_STR, $propertyFire->valuableObjectsValue),
			array(PDO::PARAM_STR, $propertyFire->yearBuilt),
			array(PDO::PARAM_STR, $propertyFire->areaSqMt),
			array(PDO::PARAM_STR, $propertyFire->description),
			array(PDO::PARAM_STR, $propertyFire->typeOfPremises),
			array(PDO::PARAM_STR, $propertyFire->buildingValue),
			array(PDO::PARAM_STR, $propertyFire->outsideFixturesValue),
			array(PDO::PARAM_STR, $propertyFire->contentsValue),
			array(PDO::PARAM_STR, $propertyFire->valuableObjectsValue),
			array(PDO::PARAM_STR, $propertyFire->yearBuilt),
			array(PDO::PARAM_STR, $propertyFire->areaSqMt)));
	
	$error = $pdoConnection->executeStatement();
	
	      
       /* insert coverages in coveragesInPolicy */
      foreach($propertyFire->coveragesInPolicyArray as $eachCoverage)
      	insertCoverageInPolicy($eachCoverage);
      
     
	writeToFile(date("Y-m-d H:i:s"). ": insertPropertyFire() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* insert new person in MEDICALINSUREDPERSON table */
function insertMedicalInsuredPerson($medicalInsuredPerson)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertMedicalInsuredPerson() ENTER \n", LOG_LEVEL_INFO);
        
	$medicalInsuredPerson->birthDate = date("Y-m-d H:i:s", strtotime($medicalInsuredPerson->birthDate));
	
	$statementString = "INSERT INTO medicalinsuredperson(personId, saleId, firstName, lastName, birthDate, stateId, telephone, gender ) VALUES(?,?,?,?,?,?,?,?)
			on duplicate key update firstName=?, lastName=?, birthDate=?, stateId=?, telephone=?, gender=? ";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $medicalInsuredPerson->personId),
			array(PDO::PARAM_STR, $medicalInsuredPerson->saleId),
			array(PDO::PARAM_STR, $medicalInsuredPerson->firstName),
			array(PDO::PARAM_STR, $medicalInsuredPerson->lastName),
			array(PDO::PARAM_STR, $medicalInsuredPerson->birthDate),
			array(PDO::PARAM_STR, $medicalInsuredPerson->stateId),
			array(PDO::PARAM_STR, $medicalInsuredPerson->telephone),
			array(PDO::PARAM_STR, $medicalInsuredPerson->gender),
			array(PDO::PARAM_STR, $medicalInsuredPerson->firstName),
			array(PDO::PARAM_STR, $medicalInsuredPerson->lastName),
			array(PDO::PARAM_STR, $medicalInsuredPerson->birthDate),
			array(PDO::PARAM_STR, $medicalInsuredPerson->stateId),
			array(PDO::PARAM_STR, $medicalInsuredPerson->telephone),
			array(PDO::PARAM_STR, $medicalInsuredPerson->gender)));
	
	$error = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": insertMedicalInsuredPerson() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* insert new vehicle in quotationVehicle table */
function insertQuotationVehicle($vehicle)
{
  	writeToFile(date("Y-m-d H:i:s"). ": insertQuotationVehicle() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO quotationvehicle(quoteId, regNumber) VALUES(?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $vehicle->quoteId),
			array(PDO::PARAM_STR, $vehicle->regNumber)));
	
	$error = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": insertQuotationVehicle() EXIT \n", LOG_LEVEL_INFO);
	return $error;
}	




/* insert new owner/client in OWNER table */
function insertOwner($owner)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertOwner() ENTER \n", LOG_LEVEL_INFO);
	$owner->birthDate = date("Y-m-d H:i:s", strtotime($owner->birthDate));
	
	$statementString = "INSERT INTO owner(firstName, lastName, type, stateId, telephone, cellphone, countryOfBirth, countryOfResidence, birthDate,
  	profession, company, proposerType, email) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $owner->firstName),
			array(PDO::PARAM_STR, $owner->lastName),
			array(PDO::PARAM_STR, $owner->type),
			array(PDO::PARAM_STR, $owner->stateId),
			array(PDO::PARAM_STR, $owner->telephone),
			array(PDO::PARAM_STR, $owner->cellphone),
			array(PDO::PARAM_STR, $owner->countryOfBirth),
			array(PDO::PARAM_STR, $owner->countryOfResidence),
			array(PDO::PARAM_STR, $owner->birthDate),
			array(PDO::PARAM_STR, $owner->profession),
			array(PDO::PARAM_STR, $owner->company),
			array(PDO::PARAM_STR, $owner->proposerType),
			array(PDO::PARAM_STR, $owner->email)));
	
	$error = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": insertOwner() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}	


/* insert new driving Experience in DRIVINGEXPERIENCE table */
function insertDrivingExperience($drivingExperience)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertDrivingExperience() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "insert into drivingexperience(stateId, hasPreviousInsurance, countryOfInsurance, insuranceCompany, yearsOfExperience) VALUES(?,?,?,?,?)
			on duplicate key update hasPreviousInsurance=?, countryOfInsurance=?, insuranceCompany=?, yearsOfExperience=? "; 
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $drivingExperience->stateId),
			array(PDO::PARAM_STR, $drivingExperience->hasPreviousInsurance),
			array(PDO::PARAM_STR, $drivingExperience->countryOfInsurance),
			array(PDO::PARAM_STR, $drivingExperience->insuranceCompany),
			array(PDO::PARAM_STR, $drivingExperience->yearsOfExperience),
			array(PDO::PARAM_STR, $drivingExperience->hasPreviousInsurance),
			array(PDO::PARAM_STR, $drivingExperience->countryOfInsurance),
			array(PDO::PARAM_STR, $drivingExperience->insuranceCompany),
			array(PDO::PARAM_STR, $drivingExperience->yearsOfExperience)));
	
	$error = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": insertDrivingExperience() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}	


/* insert new owner/client in quotationOwner table */
function insertQuotationOwner($owner)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertQuotationOwner() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "INSERT INTO quotationowner(quoteId, stateId) VALUES(?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $owner->quoteId),
			array(PDO::PARAM_STR, $owner->stateId)));
	
	$error = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": insertQuotationOwner() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
}	


/* insert an additional driver in DRIVERS table */
function insertAdditionalDriver($driver)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertAdditionalDriver() ENTER \n", LOG_LEVEL_INFO);

	$driver->birthDate = date("Y-m-d H:i:s", strtotime($driver->birthDate));
	$driver->licenseDate = date("Y-m-d H:i:s", strtotime($driver->licenseDate));
	
	$statementString = "INSERT INTO drivers(saleId, stateId, firstName, lastName, birthDate, countryOfBirth, licenseCountry, licenseDate, licenseType, telephone, profession ) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $driver->saleId),
			array(PDO::PARAM_STR, $driver->stateId),
			array(PDO::PARAM_STR, $driver->firstName),
			array(PDO::PARAM_STR, $driver->lastName),
			array(PDO::PARAM_STR, $driver->birthDate),
			array(PDO::PARAM_STR, $driver->countryOfBirth),
			array(PDO::PARAM_STR, $driver->licenseCountry),
			array(PDO::PARAM_STR, $driver->licenseDate),
			array(PDO::PARAM_STR, $driver->licenseType),
			array(PDO::PARAM_STR, $driver->telephone),
			array(PDO::PARAM_STR, $driver->profession)));
	
	$error = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": insertAdditionalDriver() EXIT \n", LOG_LEVEL_INFO);
	return $error;
}


/* insert an owner license in LICENSE table */
function insertOwnerLicense($license)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": insertOwnerLicense() ENTER \n", LOG_LEVEL_INFO);
	
	$license->licenseDate = date("Y-m-d H:i:s", strtotime($license->licenseDate));
	
	$statementString = "INSERT INTO license(licenseType, licenseDate, licenseCountry, stateId ) VALUES(?,?,?,?)
			on duplicate key update licenseType=?, licenseDate=?, licenseCountry=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $license->licenseType),
			array(PDO::PARAM_STR, $license->licenseDate),
			array(PDO::PARAM_STR, $license->licenseCountry),
			array(PDO::PARAM_STR, $license->stateId),
			array(PDO::PARAM_STR, $license->licenseType),
			array(PDO::PARAM_STR, $license->licenseDate),
			array(PDO::PARAM_STR, $license->licenseCountry)));
	
	$error = $pdoConnection->executeStatement();
	
	  
	return $error;
}


/* insert all the quote related information in the database */
function insertAllQuoteInfo($quote, $vehicle, $additionalCharges, $optionalCoverages, $discountsInQuotationArray,
		$license, $claims, $owner, $ownerAddress, $reasonsWeCannotProvideOnlineQuotes, $drivingExperience)
{
	global $YES_CONSTANT,$NO_CONSTANT;
        
	writeToFile(date("Y-m-d H:i:s"). ": insertAllQuoteInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$quoteId = 0;/* stores the newly created quoteId */
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	
	/* INSERT QUOTE */
	$statementString = "INSERT INTO quotation(canProvideOnlineQuote, entryDate, quoteAmount, insuranceCompanyOfferingQuote, userName, userInfo
   	, offerSelected, coverageType) VALUES(?,?,?,?,?,?,?,?)";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quote->canProvideOnlineQuote),
			array(PDO::PARAM_STR, $quote->entryDate),
			array(PDO::PARAM_STR, $quote->quoteAmount),
			array(PDO::PARAM_STR, $quote->insuranceCompanyOfferingQuote),
			array(PDO::PARAM_STR, $quote->userName),
			array(PDO::PARAM_STR, $quote->userInfo),
			array(PDO::PARAM_STR, $quote->offerSelected),
			array(PDO::PARAM_STR, $quote->coverageType)));
	
	$error = $pdoConnection->executeStatement();

	$quoteId = $pdoConnection->getLastInsertId(); 
      
      //echo "quoteId = $quoteId";
   
      writeToFile(date("Y-m-d H:i:s"). ": INSERT INTO quotation - new quoteId is $quoteId \n", LOG_LEVEL_INFO);
          
    
  	
  	
  	/* INSERT VEHICLE */
  	$vehicle->quoteId = $quoteId;
  	
  	//if duplicate owner, this will fail. But we already have entry in insertQuotationOwner, so it is OK
  	insertVehicle($vehicle);
  	
  	
  	//enter new entry in quotationVehicle table.
  	insertQuotationVehicle($vehicle);
  	
  	
  	/* INSERT ADDITIONAL CHARGES */
	foreach($additionalCharges as $eachCharge)
	{
		//$eachCharge->printData();
		//add only when charge is more than 0
		if($eachCharge->decision==$YES_CONSTANT){
			insertChargeInQuotation($quoteId, $eachCharge);
			//$eachCharge->printData();			
		}
	}
	
  	
  	
  	/* INSERT OPTIONAL COVERAGES */
  	foreach($optionalCoverages as $eachCoverage)
	{
		insertCoverageInQuotation($quoteId, $eachCoverage);
	}

  	
  	
  	/* INSERT OWNER */
  	$owner->quoteId = $quoteId;
  	
  	//if duplicate owner, this will fail. But we already have entry in insertQuotationOwner, so it is OK
  	if(checkEntryExistsInTable('owner', 'stateId', $owner->stateId, '', '')==0){  		
  		insertOwner($owner);
  	}
  	
  	insertDrivingExperience($drivingExperience);
  	
  	insertOwnerAddress($ownerAddress);
  	
  	/* INSERT LICENSE */
  	insertOwnerLicense($license);
  	
  	//enter new entry in quotationOwner table.
  	insertQuotationOwner($owner);
  
  	/* INSERT CLAIMS */
  	foreach($claims as $claim)
  	{
	  	$claim->quoteId = $quoteId;//set newly created quote Id
	  	insertNewClaim($claim);
  	
  	}
  	
  	/* INSERT DISCOUNTS FOR COMPREHENSIVE */
  	foreach($discountsInQuotationArray as $eachDiscount)
	{
		$eachDiscount->quoteId = $quoteId;
		insertDiscountInQuotation($quoteId, $eachDiscount);
		//$eachDiscount->printData();
	}
  		
  	/* INSERT REASONSWECANNOTPROVIDEONLINEQUOTE */
  	if($reasonsWeCannotProvideOnlineQuotes != null )
  	{
	  	foreach($reasonsWeCannotProvideOnlineQuotes as $reasonsWeCannotProvideOnlineQuote)
	  	{
		  	//$reasonsWeCannotProvideOnlineQuote->printData();
	  		$statementString = "insert into reasonswecannotprovideonlinequote(quoteId, reason) VALUES(?,?)";
	  		$pdoConnection->setSQLStatement($statementString);
	  		$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId),
	  				array(PDO::PARAM_STR, $reasonsWeCannotProvideOnlineQuote->reason)));
	  		
	  		$error = $pdoConnection->executeStatement();	  	
	  	}
  	}
  	
  		

	
	writeToFile(date("Y-m-d H:i:s"). ": insertAllQuoteInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $quoteId;
}


/* insert new HISTORY entry in the database */
function insertNewHistory($history)
{
	writeToFile(date("Y-m-d H:i:s"). ": insertNewHistory() ENTER \n", LOG_LEVEL_INFO);
        
		//add current time at the end of date
	//echo $transaction->transDate;
	//$transaction->transDate = date("Y-m-d H:i:s", strtotime($transaction->transDate));
	//$transaction->transDate = $transaction->transDate." ".date("H:i:s", time()); working previously TODO test again!
	//echo $transaction->transDate;
	
	//convert string to correct date formmat - Y-m-d H:i:s is needed to store in the database
	//$transDate = date("Y-m-d", strtotime($history->transDate))." ".date("H:i:s", time()); 
	$history->transDate =  date("Y-m-d H:i:s");
	$history->username = $_SESSION['username'];
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "INSERT INTO history(transDate, username, type , subType, parameterName, parameterValue, note) 
			VALUES(?,?,?,?,?,?,?)";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $history->transDate),
			array(PDO::PARAM_STR, $history->username),
			array(PDO::PARAM_STR, $history->type),
			array(PDO::PARAM_STR, $history->subType),
			array(PDO::PARAM_STR, $history->parameterName),
			array(PDO::PARAM_STR, $history->parameterValue),
			array(PDO::PARAM_STR, $history->note)));
	
	$error = $pdoConnection->executeStatement();
	
	writeToFile(date("Y-m-d H:i:s"). ": insertNewHistory() EXIT \n", LOG_LEVEL_INFO);
        
}

/* insert EMAILPREFERENCE entry in the database */
function insertEmailPreference($emailPreference)
{
	writeToFile(date("Y-m-d H:i:s"). ": insertEmailPreference() ENTER \n", LOG_LEVEL_INFO);
        
		$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "INSERT INTO emailpreferences(stateId, preferenceCode, preferenceFrequency ) VALUES(?,?,?)";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $emailPreference->stateId),
			array(PDO::PARAM_STR, $emailPreference->preferenceCode),
			array(PDO::PARAM_STR, $emailPreference->preferenceFrequency)));
	
	$error = $pdoConnection->executeStatement();
	
	
	writeToFile(date("Y-m-d H:i:s"). ": insertEmailPreference() EXIT \n", LOG_LEVEL_INFO);

}
?>