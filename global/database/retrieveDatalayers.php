<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//FUNCTIONS THAT INSERT DATA IN THE DATABASE
		


/* retrieve information from the database COVERAGESINQUOTATION table */
function retrieveOptionalCoverages($quoteId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveOptionalCoverages() ENTER \n", LOG_LEVEL_INFO);
        
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT quoteId, code, param1, param2, param3, description, decision, charge  FROM coveragesinquotation WHERE quoteId=?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	$coveragesInPolicyQuotationArray = array();
	
	/* Create a prepared statement */
   	//if($stmt = $mysqli -> prepare("SELECT quoteId, code, param1, param2, param3, description, decision, charge  FROM coveragesinquotation WHERE quoteId=?" )) {

      /* Bind input parameters
         s - string, b - boolean, i - int, etc */
      //$stmt -> bind_param("s", $quoteId);

      /* Execute it */
     // $stmt -> execute();

      /* Bind results */
      //$stmt -> bind_result($quoteId, $code, $param1, $param2, $param3, $description, $decision, $charge );
      
      
      /* Fetch the single record */
       //while ($stmt->fetch()) {
       foreach($results as $eachResult)
       {
	  
	      $optionalCoverages = new coveragesInPolicyQuotation();
	       
		  $optionalCoverages->quoteId = $eachResult['quoteId'];
		  $optionalCoverages->code = $eachResult['code'];
		  $optionalCoverages->parameters[0] = $eachResult['param1'];
		  $optionalCoverages->parameters[1] = $eachResult['param2'];
		  $optionalCoverages->parameters[2] = $eachResult['param3'];
		  $optionalCoverages->description = $eachResult['description'];
		  $optionalCoverages->decision = $eachResult['decision'];
		  $optionalCoverages->charge = $eachResult['charge'];
		  $coveragesInPolicyQuotationArray[] = $optionalCoverages;
	  }
	  


	writeToFile(date("Y-m-d H:i:s"). ": retrieveOptionalCoverages() EXIT \n", LOG_LEVEL_INFO);
        
	return $coveragesInPolicyQuotationArray;

}



/* retrieve information from the database CHARGESINQUOTATION table */
function retrieveAdditionalCharges($quoteId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveAdditionalCharges() ENTER \n", LOG_LEVEL_INFO);

	$chargesInPolicyQuotationArray = array();
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT quoteId, code, param1, param2, param3, description, charge  FROM chargesinquotation WHERE quoteId=?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
		
	foreach($results as $eachResult)
	{
	  
	      $additionalCharges = new chargesInQuotation();
	       
		  $additionalCharges->quoteId = $eachResult['quoteId'];
		  $additionalCharges->code = $eachResult['code'];
		  $additionalCharges->parameters[0] = $eachResult['param1'];
		  $additionalCharges->parameters[1] = $eachResult['param2'];
		  $additionalCharges->parameters[2] = $eachResult['param3'];
		  $additionalCharges->description = $eachResult['description'];
		  $additionalCharges->charge = $eachResult['charge'];
		  
		  $chargesInPolicyQuotationArray[] = $additionalCharges;
	  }
	  
    

	writeToFile(date("Y-m-d H:i:s"). ": retrieveAdditionalCharges() EXIT \n", LOG_LEVEL_INFO);
        
	return $chargesInPolicyQuotationArray;

}


/* retrieve all system users that are producers */
function retrieveAllProducers()
{
	global $YES_CONSTANT;
        

	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllProducers() ENTER \n", LOG_LEVEL_INFO);
        
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT  username FROM systemuser WHERE producer = '".$YES_CONSTANT."'";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	$producers = array();
	
	
	
        
   	 foreach($results as $eachResult)
		  $producers[] = $eachResult['username'];
	  
    

  	

	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllProducers() EXIT \n", LOG_LEVEL_INFO);
        
	 return $producers;
}


/* retrieve information from the database TRANSACTION table.
Retrieve all transactions
$limitClause : how many records to retrieve. If is empty, we retrieve all records */
function retrieveTransactions($parameterNameValueArray, $limitClause)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllProducers() EXIT() ENTER \n", LOG_LEVEL_INFO);
        

	$transactions = array();
	$selectString = "";
	$eachParameterPair;
	$i=1;//counter to see if we are in the first element of the array
	
	
	$bindparams = array();
	
	
	foreach($parameterNameValueArray as $eachParameterPair)
	{
		//$eachParameterPair->printData();
		
		//for all elements except the first, add 'and' and ','
		if($i>1){
			$selectString = " and ".$selectString;
			//$bindSecondParam = ", ".$bindSecondParam;
		}
		//looking for username
		if($eachParameterPair->name == 'transactionStartDate' )
		{
			$selectString = " transDate >= ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR,$eachParameterPair->value);
		}
		//historyEndDate
		else if($eachParameterPair->name == 'transactionEndDate' )
		{
			$selectString = " transDate < ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR,$eachParameterPair->value);
			
		}
		//looking for parameters, like saleId or stateId
		else
		{
			$selectString = " ". $eachParameterPair->name. " like ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR,$eachParameterPair->value);
		}
		
		$i++;
	}
	//put in same order of appearance 
	$bindparams = array_reverse($bindparams);
	//var_dump($bindparams);
	
	//echo "select string=$selectString ";
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT transId, date_FORMAT(transDate, '%d-%m-%Y') as transDateFormatted, details, receiptNo, debit, credit, remainder, saleId, producer FROM transaction WHERE ".$selectString." order by transDate desc ".$limitClause;
	$pdoConnection->setSQLStatement($statementString);
	//var_dump($bindparams);
	//echo "select sql=$statementString ";
	
	$pdoConnection->setBindParams($bindparams);
    $pdoConnection->executeStatement();
    $results = $pdoConnection->fetchAll();

    //var_dump($results);
    
    foreach($results as $eachResult)
    {
		  $transaction = new transaction();
		  $transaction->transId =  $eachResult['transId'];
 		  $transaction->transDate = $eachResult['transDateFormatted'];
		  $transaction->details = $eachResult['details'];
		  $transaction->receiptNo = $eachResult['receiptNo'];
		  $transaction->debit = $eachResult['debit'];
		  $transaction->credit = $eachResult['credit'];
		  $transaction->remainder = $eachResult['remainder'];
		  $transaction->saleId = $eachResult['saleId'];
		  $transaction->producer = $eachResult['producer'];
		  
		  $transactions[] = $transaction;
	  }
	
	 
	  
	writeToFile(date("Y-m-d H:i:s"). ": retrieveTransactions() EXIT \n", LOG_LEVEL_INFO);
        
	return $transactions;
}



/* 
retrieve information from the database COVERAGESINPOLICY or COVERAGESINQUOTATION table.
$tableName = name of table
$fieldName = name of field. i.e saleId or quoteId
$fieldValue = value of fieldName
 */
function retrieveAllCoverages($tableName, $fieldName, $fieldValue)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllCoverages() ENTER \n", LOG_LEVEL_INFO);
        

	$coveragesInPolicyQuotationArray = array();
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT code, param1, param2, param3, description FROM ".$tableName." WHERE ".$fieldName." like ?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $fieldValue)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
		$coveragesInPolicyQuotation = new coveragesInPolicyQuotation();
		$coveragesInPolicyQuotation->code = $eachResult['code'];
		$coveragesInPolicyQuotation->parameters[0] = $eachResult['param1'];
		$coveragesInPolicyQuotation->parameters[1] = $eachResult['param2'];
		$coveragesInPolicyQuotation->parameters[2] = $eachResult['param3'];
		$coveragesInPolicyQuotation->description = $eachResult['description'];
		
		$coveragesInPolicyQuotationArray[] = $coveragesInPolicyQuotation;
		
	}
	
	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllCoverages() EXIT \n", LOG_LEVEL_INFO);
        
	return $coveragesInPolicyQuotationArray;
}


/* retrieve the remainder for this saleId from TRANSACTIONS table */
function retrieveRemainder($saleId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveRemainder() ENTER \n", LOG_LEVEL_INFO);

	$returnRemainder = 0;

	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT remainder FROM transaction WHERE saleId like ? order by transDate desc limit 1";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $saleId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
		$returnRemainder = $eachResult['remainder'];
	}
	
	writeToFile(date("Y-m-d H:i:s"). ": retrieveRemainder() EXIT \n", LOG_LEVEL_INFO);
	return $returnRemainder;
}


/* retrieve information from the database NOTES table */
function retrieveNotes($parameterNameValueArray)
{

	$i = 1;
	$selectString = "";
	
	
	$notes = array();
	
	foreach($parameterNameValueArray as $eachParameterPair)
	{
		//$eachParameterPair->printData();
		
		//for all elements except the first, add 'and' and ','
		if($i>1){
			$selectString = " and ".$selectString;
			//$bindSecondParam = ", ".$bindSecondParam;
		}
		//historyStartDate
		if($eachParameterPair->name == 'startDate' )
		{
			$selectString = " entryDate >= ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR,$eachParameterPair->value);
		}
		//historyEndDate
		else if($eachParameterPair->name == 'endDate' )
		{
			$selectString = " entryDate < ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR,$eachParameterPair->value);
			
		}
		//looking for parameters, like saleId or stateId
		else
		{
			$selectString = " parameterName like '".$eachParameterPair->name."' and parameterValue like ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR,$eachParameterPair->value);
		}
		//$bindFirstParam = "s".$bindFirstParam;
		//$bindSecondParam = $eachParameterPair->value.$bindSecondParam;
		$i++;
	}
	//echo "select string=$selectString ";
	
	//$statementString = "SELECT notesId, type, description, date_FORMAT(entryDate, '%d-%m-%Y'),parameterName, parameterValue FROM notes WHERE ".$selectString." order by entryDate asc";
	
	//put in same order of appearance
	$bindparams = array_reverse($bindparams);
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT notesId, type, description, date_FORMAT(entryDate, '%d-%m-%Y') as entryDate2,parameterName, parameterValue FROM notes WHERE ".$selectString." order by entryDate asc";
	$pdoConnection->setSQLStatement($statementString);
	//var_dump($bindparams);
	//echo "select sql=$statementString ";
	
	$pdoConnection->setBindParams($bindparams);
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
            
   	foreach($results as $eachResult)
	{
		//$returnRemainder = $eachResult['remainder'];
		
		  $note = new note();
		  $note->notesId = $eachResult['notesId'];
 		  $note->type = $eachResult['type'];
		  $note->description = $eachResult['description'];
		  $note->entryDate = $eachResult['entryDate2'];
		  $note->parameterName = $eachResult['parameterName'];
		  $note->parameterValue = $eachResult['parameterValue'];
		  		  
		  $notes[] = $note;
	  }
	  
 
	//writeToFile(date("Y-m-d H:i:s"). ": retrieveNotes() EXIT \n", LOG_LEVEL_INFO);
	return $notes;

}

/* retrieve information from the database VEHICLE table 
$parameterName = name of field in table. i.e regNumber, saleId 
$parameterValue = value the field should match. i.e 'VM290' or '765489' */
function retrieveVehicleInfo($parameterName, $parameterValue)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveVehicleInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$vehicles = array();
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT * FROM vehicle WHERE ".$parameterName." like ?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $parameterValue)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
		
		  $vehicle = new vehicle();
	  
		  $vehicle->regNumber = $eachResult['regNumber'];
		  $vehicle->engineKw = $eachResult['engineKw'];
		  $vehicle->vehicleType = $eachResult['vehicleType'];
		  $vehicle->model = $eachResult['model'];
		  $vehicle->make = $eachResult['make'];
		  $vehicle->cubicCapacity = $eachResult['cubicCapacity'];
		  $vehicle->manufacturedYear = $eachResult['manufacturedYear'];
		  $vehicle->vehicleDesign = $eachResult['vehicleDesign'];
		  $vehicle->sumInsured = $eachResult['sumInsured'];
		  $vehicle->steeringWheelSide = $eachResult['steeringWheelSide'];
		  $vehicle->isTaxFree = $eachResult['isTaxFree'];
		  $vehicle->isUsedForDeliveries = $eachResult['isUsedForDeliveries'];
		  $vehicle->hasVisitorPlates = $eachResult['hasVisitorPlates'];
		  $vehicle->saleId = $eachResult['saleId'];
		  
		  $vehicles[] = $vehicle;
	  }
	   
	 
	writeToFile(date("Y-m-d H:i:s"). ": retrieveVehicleInfo() EXIT \n", LOG_LEVEL_INFO);
	return $vehicles;

}

/* retrieve current number of different users for this client
from global:SYSTEMUSER table */
function retrieveCurrentNumberOfUsers($clientName)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveCurrentNumberOfUsers() ENTER \n", LOG_LEVEL_INFO);

	$currentNumberOfUsers;
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT count(*) as currentNumberOfUsers FROM systemuser WHERE clientName=?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $clientName)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
      $currentNumberOfUsers = $eachResult['currentNumberOfUsers'];
  	}
  

	writeToFile(date("Y-m-d H:i:s"). ": retrieveCurrentNumberOfUsers() EXIT \n", LOG_LEVEL_INFO);
        
	return $currentNumberOfUsers;

}


/* retrieve information from the database QUOTATION_DISCOUNTS table */
function retrieveDiscounts($quoteId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveDiscounts() ENTER \n", LOG_LEVEL_INFO);

	$discountsInQuotationArray = array();
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT *  FROM quotation_discounts WHERE quoteId=?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
	

	      $localDiscount = new quotation_discounts();
	       
		  $localDiscount->quoteId = $eachResult['quoteId'];
		  $localDiscount->code = $eachResult['code'];
		  $localDiscount->parameters[0] = $eachResult['param1'];
		  $localDiscount->parameters[1] = $eachResult['param2'];
		  $localDiscount->parameters[2] = $eachResult['param3'];
		  $localDiscount->description = $eachResult['description'];
		  $localDiscount->charge = $eachResult['charge'];
		  $discountsInQuotationArray[] = $localDiscount;
	  }
	  
    

	writeToFile(date("Y-m-d H:i:s"). ": retrieveDiscounts() EXIT \n", LOG_LEVEL_INFO);
        
	return $discountsInQuotationArray;

}

/* intermediate function for preparing the sql query
1. Creates the appropriate statement according to the $statementOption
2. Call retrieveContract
3. Return the contract 
$parameterNameValueArray = array of the parameterNameValue structures. Contains pairs of (names,values) for fields to match in the database table. 
	i.e (stateId,'764490'), (company, '*MINERVA*')
$statementOption = how much information we need to retrieve
	0=all information
	1=basic information
*/
function retrieveContractInfo($parameterNameValueArray, $statementOption)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveContractInfo():  statementOption=$statementOption ENTER \n", LOG_LEVEL_INFO);
        
	$contracts = array();
	
	switch ($statementOption) 
	{
    //retrieve all information
	case 0:
		$contracts = retrieveAllContractInfo($parameterNameValueArray);
		break;
	//retrieve only basic information
	case 1:
	    $contracts = retrieveBasicContractInfo($parameterNameValueArray);
        break;
    default:
        break;
	}

	writeToFile(date("Y-m-d H:i:s"). ": retrieveContractInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $contracts;
	
}

/* retrieve all contracts that have a remainder(not 0) for a specific producer
$producer: contacts of this producer. % for ALL */
function retrieveAllContractsWithRemainder($producer)
{
        
    writeToFile(date("Y-m-d H:i:s"). ": retrieveAllContractsWithRemainder() ENTER \n", LOG_LEVEL_INFO);

        
	$contracts = array();
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "select s.saleId, t.remainder from transaction t, sale s where t.saleId=s.saleId and s.producer like '".$producer."' order by t.saleID asc, t.transDate desc";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	//initialize
	$currentSaleId=-1;
	$previousSaleId=-1;
	
	foreach($results as $eachResult)
	{
		 $currentSaleId = $eachResult['saleId'];
		 //only keep first transaction - remaining discard
		 if($currentSaleId != $previousSaleId && $eachResult['remainder']!=0 )
		 {
	   	 	//echo "saleid=$currentSaleId, remainder=$remainder <br>";
			
	   	 	$parameterNameValueArray = array();
	   	 	
	   	 	$parameterNameValue = new parameterNameValue();
	   	 	$parameterNameValue->name = ' s.saleId ';
	   	 	$parameterNameValue->value = $currentSaleId;

	   	 	$parameterNameValueArray[] = $parameterNameValue;
	   	 		   	 	
	   	 	//retrieve all contract info and update also the remainder
			$tmpContracts = retrieveContractInfo($parameterNameValueArray, 1);
			if(count($tmpContracts)>0)
			{
				//echo count($tmpContracts)." contracts found for saleid = ".$currentSaleId." remainder=".$remainder."<br>";
				//fill contracts. Here we should have only one contract per saleId
				foreach($tmpContracts as $eachContract)
				{
					$eachContract->remainder = $eachResult['remainder'];
					$eachContract->discount = 'n/a';
					$contracts[] = $eachContract;
				}
			}
   	 	 }
   	 	 //update, so we only use the latest/first transaction for each contract
   	 	 //after the latest/first transaction of a saleId, all other transactions of that saleId are skipped
	   	 $previousSaleId = $currentSaleId;
	  }	  
	  
         
	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllContractsWithRemainder() EXIT \n", LOG_LEVEL_INFO);
        
	return $contracts;
	
}

/* Intermediate function for preparing the sql query
1. Creates the appropriate statement according to the $statementOption
2. Call retrieveTotalProduction
3. Return the production
$year = year of production i.e '1999'
$statementOption = select choice
$groupByClause = how to group by. i.e company, associate, producer */
function retrieveProductionInfo($year, $statementOption, $groupByClause)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveProductionInfo():  year=$year, statementOption=$statementOption ENTER \n", LOG_LEVEL_INFO);
        
	switch ($statementOption) {
	//retrieve NEW/RENEW contracts
    case 0:
        $statementString = "select ".$groupByClause." as parameter ,sum(t.debit) as production from transaction t, sale s where (t.details like 'RENEWAL' OR t.details like 'NEW') and t.saleId=s.saleId and date_FORMAT(t.transDate,'%Y') like ? group by ".$groupByClause."  order by sum(t.debit) desc";
        break;
    //retrieve CANCELLED contacts group by company
    case 1:
        $statementString = "select ".$groupByClause." as parameter, sum(t.credit) as production from transaction t, sale s where t.details like 'CANCEL' and t.saleId=s.saleId and date_FORMAT(t.transDate,'%Y') like ? group by ".$groupByClause." order by sum(t.credit) desc";
        break;
    //retrieve NEW contacts group by company
    case 2:
      	$statementString = "select ".$groupByClause." as parameter, sum(t.credit) as production from transaction t, sale s where t.details like 'NEW' and t.saleId=s.saleId and date_FORMAT(t.transDate,'%Y') like ? group by ".$groupByClause." order by sum(t.credit) desc";
       	break;
    default:
        break;
	}
	
	$production = retrieveTotalProduction($year, $statementString);

	writeToFile(date("Y-m-d H:i:s"). ": retrieveProductionInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $production;
	
}

/* retrieve total production for all companies, for specific year, using specific statementString */
function retrieveTotalProduction($year, $statementString)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveTotalProduction() ENTER \n", LOG_LEVEL_INFO);
        
		
	$productionArray = array();
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $year)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
	
	
	
		 $localProduction = new production();
		 $localProduction->parameter = $eachResult['parameter'];
		 $localProduction->production = $eachResult['production'];
		 $productionArray[] = $localProduction;
	 }
	  
    

	writeToFile(date("Y-m-d H:i:s"). ": retrieveTotalProduction() EXIT \n", LOG_LEVEL_INFO);
        
	return $productionArray;
	
}


/* retrieve total production per month, from start until end year */
function retrieveProductionPerMonth($startYear, $endYear, $producer)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveProductionPerMonth() ENTER \n", LOG_LEVEL_INFO);
		
	$monthProductionPerYearArray = array();
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	
	for($i=$startYear; $i<=$endYear; $i++)
	{
	
		
		$statementString = "select date_FORMAT(t.transDate,'%c') as month, sum(t.debit) as production from sale s,transaction t where s.saleId=t.saleId and s.producer like '".$producer."' and (t.details like 'RENEWAL' OR t.details like 'NEW') and date_FORMAT(t.transDate,'%Y') like ? group by date_FORMAT(t.transDate,'%c') order by date_FORMAT(t.transDate,'%m') asc";		
		
		$pdoConnection->setSQLStatement($statementString);		
		$pdoConnection->setBindParams(array(array(PDO::PARAM_INT, $i)));
		$pdoConnection->executeStatement();
		$results = $pdoConnection->fetchAll();
		
		//var_dump($results);
		
		      
	     $localMonthProductionPerYear = new monthProductionPerYear();
	     $localMonthProductionPerYear->year = $i;
	    
		 foreach($results as $eachResult)
		 {
			 $localMonthProduction = new monthProduction();
			 $localMonthProduction->month = $eachResult['month'];
			 $localMonthProduction->monthProduction = $eachResult['production'];
			 //echo $month." - ".$production."<br/>";
			 $localMonthProductionPerYear->monthsArray[] = $localMonthProduction;
		 }
		 $monthProductionPerYearArray[] = $localMonthProductionPerYear;
	}

	writeToFile(date("Y-m-d H:i:s"). ": retrieveProductionPerMonth() EXIT \n", LOG_LEVEL_INFO);
        
	return $monthProductionPerYearArray;
	
}



/* retrieve all contracts that have a discount(not 0) for this year */
function retrieveAllContractsWithDiscount($year, $producer)
{
        
    writeToFile(date("Y-m-d H:i:s"). ": retrieveAllContractsWithDiscount() ENTER \n", LOG_LEVEL_INFO);

    $contracts = array();
	
	//retrieve all transactions in descending order, so we can choose the latest
	$statementString = "select saleId, credit from transaction where details like 'DISCOUNT' and credit!=0 and date_FORMAT(transDate,'%Y') like ? and producer like ?";
	
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $year),array(PDO::PARAM_STR, $producer)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
		 $parameterNameValueArray = array();
		 
		 $parameterNameValue = new parameterNameValue();
	   	 $parameterNameValue->name = ' s.saleId ';
	   	 $parameterNameValue->value = $eachResult['saleId'];
	   	 	
	   	 $parameterNameValueArray[] = $parameterNameValue;
	   	 
   	 	//retrieve all contract info and update also the discount
		$tmpContracts = retrieveContractInfo($parameterNameValueArray, 1);
		if(count($tmpContracts)>0)
		{
			//echo count($tmpContracts)." contracts found for saleid = ".$currentSaleId." remainder=".$remainder."<br>";
			//fill contracts. Here we should have only one contract per saleId
			foreach($tmpContracts as $eachContract)
			{
				$eachContract->discount = $eachResult['credit'];
				$contracts[] = $eachContract;
			}
		}
	  }
	  
	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllContractsWithDiscount() EXIT \n", LOG_LEVEL_INFO);
        
	return $contracts;
	
}

/* retrieve ALL contract info from the database tables. Doesn't apply to date ranges, i.e contracts in a whole month. */
function retrieveAllContractInfo($parameterNameValueArray)
{
	global $INSURANCE_TYPE_MOTOR, $INSURANCE_TYPE_EMPLOYER_LIABILITY, $INSURANCE_TYPE_MEDICAL, $INSURANCE_TYPE_FIRE_PROPERTY, $INSURANCE_TYPE_LIFE, $PROPOSER_TYPE_PERSON;
	global $SALE_STATUS_ACTIVE, $SALE_STATUS_EXPIRED;
        

	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllContractInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$contracts = array();
	
	$statementString = "select saleId, company, insuranceType, coverageType, date_FORMAT(startDate, '%d-%m-%Y') as startDate1, date_FORMAT(endDate, '%d-%m-%Y') as endDate1, stateId, associate, producer, status from sale s where ".$parameterNameValueArray[0]->name." like ? order by s.endDate desc";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $parameterNameValueArray[0]->value)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
	
		 $contract = new contract();
		 $contract->motor = new motor();
		 //$contract->contractNumber = $saleId;
 		 $contract->insuranceCompanyOfferingQuote = $eachResult['company'];
 		 $contract->contractType = $eachResult['coverageType'];
 		 
 		 $contract->sale = new sale();
		 $contract->sale->saleId = $eachResult['saleId'];
		 $contract->sale->insuranceType = $eachResult['insuranceType'];
		 $contract->sale->coverageType = $eachResult['coverageType'];
		 $contract->sale->saleStartDate = $eachResult['startDate1'];
		 $contract->sale->saleEndDate = $eachResult['endDate1'];
		 $contract->sale->associate = $eachResult['associate'];
		 $contract->sale->producer = $eachResult['producer'];
		 $contract->sale->status = $eachResult['status'];
		 $contract->sale->stateId = $eachResult['stateId'];
		 
		  //if contract has already expired, set status to expired
		  if(strtotime($contract->sale->saleEndDate) < strtotime(date("Y-m-d")) && $contract->sale->status == $SALE_STATUS_ACTIVE )
		  {
			  $contract->sale->status = $SALE_STATUS_EXPIRED;
			  updateSaleStatus($contract->sale->saleId, $contract->sale->status);
		  }
		  
		  
		 //retrieve owner/proposer info
		 //$owner = retrieveOwner($stateId); //aaresti removed. duplicate call.
		  
		 $contract->owner = new owner();	
		 $contract->owner = retrieveOwner($contract->sale->stateId);
		 
		 //retrieve driving experience
		 $contract->motor->drivingExperience = retrieveDrivingExperience($contract->sale->stateId);
		 
		 //retrieve remainder
		 $remainder = retrieveRemainder($contract->sale->saleId);
		 $contract->remainder = $remainder;
		 $contract->discount = 'n/a';
		  
		 //retrieve address
		 $contract->addresses = retrieveOwnerAddressInfo($contract->sale->stateId);

		 //always retrieve license. For case when we have a 'non-motor' contract, and we change it to 'motor', we need to have the license also		  
		  
		 $contract->motor->license = retrieveLicense($contract->sale->stateId);
		 $contract->motor->vehicle = new vehicle();
		 
		 if( $contract->sale->insuranceType == $INSURANCE_TYPE_MOTOR )
		 {
			  //retrieve Quote Info
			  //TODO - create new function
			  $quoteId = retrieveQuoteIdFromQuotationOwner($contract->sale->stateId);
			  $quoteInfo = retrieveQuoteInfo('q.quoteId', $quoteId);
			  
			  //contract is related with a quote id
			  if(count($quoteInfo)>0 )
			  {
				  //$contract->coverageType = $quoteInfo->coverageType;
				  $contract->offerSelected = $quoteInfo[0]->offerSelected;
				  $contract->quoteId = $quoteId;
			  }
		  
			  //retreive additional Drivers Info
			  $drivers = retrieveDriversInfo($contract->sale->saleId);
			  $contract->drivers = $drivers;
			  //foreach($contract->drivers as $driver)
			  	//$driver->printData();

			  //retrieve vehicle info - array should contain only one element
			  $allVehicles = retrieveVehicleInfo("saleId", $contract->sale->saleId);
			  foreach($allVehicles as $eachVehicle)
			  	$vehicle = $eachVehicle;
			  //$vehicle->printData();
			  
			  $contract->motor->vehicle = $vehicle;
			  
			  //initialize empty structures
			  $contract->employersLiability = new employersLiability();
			  $contract->medical = new medical();
			  $contract->propertyFire = new propertyfire();
			  $contract->lifeIns = new lifeIns();
			  //$contract->employersLiability->printData();
	  	}
	  	else if( $contract->sale->insuranceType == $INSURANCE_TYPE_EMPLOYER_LIABILITY )
	  	{
		  	
		  	$contract->employersLiability = retrieveEmployersLiabilityInfo($contract->sale->saleId);
		  	
		  	//initialize empty structures
			$contract->medical = new medical();
			$contract->propertyFire = new propertyfire();
			$contract->lifeIns = new lifeIns();
		  	//echo "sale id is $saleId";
		  	//$contract->employersLiability->printData();
	  	}
	  	else if( $contract->sale->insuranceType == $INSURANCE_TYPE_MEDICAL )
	  	{
		  	$contract->medical = retrieveMedicalInfo($contract->sale->saleId);
		  	
		  	//initialize empty structures
			$contract->employersLiability = new employersLiability();
			$contract->propertyFire = new propertyfire();
			$contract->lifeIns = new lifeIns();
	  	}
	  	else if( $contract->sale->insuranceType == $INSURANCE_TYPE_LIFE )
	  	{
		  	$contract->lifeIns = retrieveLifeInsInfo($contract->sale->saleId);
		  	
		  	//initialize empty structures
		  	$contract->medical = new medical();
			$contract->employersLiability = new employersLiability();
			$contract->propertyFire = new propertyfire();
	  	}
	  	else if( $contract->sale->insuranceType == $INSURANCE_TYPE_FIRE_PROPERTY )
	  	{
		  	$contract->propertyFire = retrievePropertyFireInfo($contract->sale->saleId);
		  	//$contract->propertyFire->printData();
		  	//initialize empty structures
			$contract->employersLiability = new employersLiability();
			$contract->medical = new medical();
			$contract->lifeIns = new lifeIns();
	  	}
		$contracts[] = $contract;
	  }
	  
	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllContractInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $contracts;
}

/* retrieve only basic info, to display in a list */
function retrieveBasicContractInfo($parameterNameValueArray)
{
	global $INSURANCE_TYPE_MOTOR;
	global $SALE_STATUS_ACTIVE, $SALE_STATUS_EXPIRED;
        

	writeToFile(date("Y-m-d H:i:s"). ": retrieveBasicContractInfo() ENTER \n", LOG_LEVEL_INFO);

	$contracts = array();
	$selectString = "";
	//$bindFirstParam = "";
	//$bindSecondParam = "";
	$eachParameterPair;
	$i=1;//counter to see if we are in the first element of the array
	
	$bindparams = array();	
	
	
	//echo $statementString;
	//create select string
	foreach($parameterNameValueArray as $eachParameterPair)
	{
		//$eachParameterPair->printData();
		
		//for all elements except the first, add 'and' and ','
		if($i>1){
			$selectString = " and ".$selectString;
			
			//$bindSecondParam = ", ".$bindSecondParam;
		}
			
		$selectString = " ".$eachParameterPair->name." like ? ".$selectString;
		$bindparams[] = array(PDO::PARAM_STR, $eachParameterPair->value);
		//$bindFirstParam = "s".$bindFirstParam;
		//$bindSecondParam = $eachParameterPair->value.$bindSecondParam;
		$i++;
	}
	//echo "select string=$selectString ";
	
	$bindparams = array_reverse($bindparams);
	//var_dump($parameterNameValueArray);
	
	$statementString = "select s.saleId, s.company, s.insuranceType, s.coverageType, date_FORMAT(s.startDate, '%d-%m-%Y') as startDate1, date_FORMAT(s.endDate, '%d-%m-%Y') as endDate1, s.stateId, s.associate, s.producer, s.status, o.firstName, o.lastName, o.telephone, o.cellphone, o.email from sale s, owner o where o.stateId=s.stateId and ".$selectString." order by s.endDate asc";
	//echo $statementString;
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams($bindparams);
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
		
		
		 $contract = new contract();
		 $contract->motor = new motor();
		 //$contract->contractNumber = $saleId;
 		 $contract->insuranceCompanyOfferingQuote = $eachResult['company'];
 		 $contract->contractType = $eachResult['coverageType'];
 		 
 		 $contract->sale = new sale();
		 $contract->sale->saleId = $eachResult['saleId'];
		 $contract->sale->insuranceType = $eachResult['insuranceType'];
		 $contract->sale->coverageType = $eachResult['coverageType'];
		 $contract->sale->saleStartDate = $eachResult['startDate1'];
		 $contract->sale->saleEndDate = $eachResult['endDate1'];
		 $contract->sale->producer = $eachResult['producer'];
		 $contract->sale->status = $eachResult['status'];
		 $contract->sale->stateId = $eachResult['stateId'];
		 

		  //retrieve owner/proposer info
		  $contract->owner = new owner();	
		  $contract->owner->firstName = stripcslashes($eachResult['firstName']);
		  $contract->owner->lastName = stripcslashes($eachResult['lastName']);
		  $contract->owner->telephone = $eachResult['telephone'];
		  $contract->owner->cellphone = $eachResult['cellphone'];
		  $contract->owner->email = $eachResult['email'];
		  $contract->owner->stateId = $eachResult['stateId'];
		  
		  //retrieve remainder
		  $remainder = retrieveRemainder($contract->sale->saleId);
		  $contract->remainder = $remainder;
		  $contract->discount = 'n/a';
		  
		  //retrieve vehicle registration only for motor
		  if($contract->sale->insuranceType==$INSURANCE_TYPE_MOTOR)
		  {
			  $contract->motor->vehicle = new vehicle();
			  $vehicles = retrieveVehicleInfo("saleId", $contract->sale->saleId);
			  foreach($vehicles as $eachVehicle)
				  $contract->motor->vehicle = $eachVehicle;
		  }
		  
		  $contracts[] = $contract;
	  }
	  
   
	writeToFile(date("Y-m-d H:i:s"). ": retrieveBasicContractInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $contracts;
}


/* retrieve information for all users that have part or all of 'username' 
from the client database SYSTEMUSER table
To retrieve only exact match, send 'username' without any '%' */
function retrieveUsersInfo($username)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveUsersInfo() ENTER \n", LOG_LEVEL_INFO);
    	
	$users = array();
	
	$statementString = "SELECT username, stateId, gender, firstName, lastName, telephone, cellphone, profession, email, date_FORMAT(birthDate, '%d-%m-%Y') as birthDate1, 
   	date_FORMAT(licenseIssueDate, '%d-%m-%Y') as licenseIssueDate1, producer FROM systemuser WHERE username like ?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $username)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{

		  $user = new user();
		  $user->username = $eachResult['username'];
 		  $user->stateId = $eachResult['stateId'];
		  $user->gender = $eachResult['gender'];
		  $user->firstName = stripcslashes($eachResult['firstName']);
		  $user->lastName = stripcslashes($eachResult['lastName']);
		  $user->telephone = $eachResult['telephone'];
		  $user->cellphone = $eachResult['cellphone'];
		  $user->profession = $eachResult['profession'];
		  $user->email = $eachResult['email'];
		  $user->birthDate = $eachResult['birthDate1'];
		  $user->licenseIssueDate = $eachResult['licenseIssueDate1'];
		  $user->producer = $eachResult['producer'];
		  
		  if($user->stateId!="")
		  	$user->addresses = retrieveOwnerAddressInfo($user->stateId);
		  
		  $users[] = $user;
	  }
	  
     
	writeToFile(date("Y-m-d H:i:s"). ": retrieveUsersInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $users;

}

/* retrieve all owner address from OWNERADDRESSES table */
function retrieveOwnerAddressInfo($stateId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveOwnerAddressInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$ownerAddresses = array();
	
	$statementString = "SELECT addressId, addressType, street, areaCode, city, state, country FROM owneraddress WHERE stateId like ? order by addressID asc";
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $stateId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
	
	
		  $ownerAddress = new ownerAddress();
		  $ownerAddress->addressId = $eachResult['addressId'];
 		  $ownerAddress->addressType = $eachResult['addressType'];
		  $ownerAddress->street = $eachResult['street'];
		  $ownerAddress->areaCode = $eachResult['areaCode'];
		  $ownerAddress->city = $eachResult['city'];	  
		  $ownerAddress->state = $eachResult['state'];
		  $ownerAddress->country = $eachResult['country'];
		  
		  $ownerAddresses[] = $ownerAddress;
	  }
	  
    

	writeToFile(date("Y-m-d H:i:s"). ": retrieveOwnerAddressInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $ownerAddresses;

}


/* retrieve all owner address from EMAILPREFERENCES table */
function retrieveEmailPreference($stateId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveEmailPreference() ENTER \n", LOG_LEVEL_INFO);
        
	$emailPreference = new emailPreference();
	
	$statementString = "SELECT stateId, preferenceCode, preferenceFrequency FROM emailpreferences WHERE stateId like ? ";
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $stateId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
		$emailPreference->stateId = $stateId;
 		$emailPreference->preferenceCode = $eachResult['preferenceCode'];
		$emailPreference->preferenceFrequency = $eachResult['preferenceFrequency'];
	}
	  
    
	writeToFile(date("Y-m-d H:i:s"). ": retrieveEmailPreference() EXIT \n", LOG_LEVEL_INFO);
        
	return $emailPreference;

}

/* retrieve information related to a Proposer
$parameterName = name of field in table. i.e firstName, or stateId
$parameterValue = value the field should match. i.e 'John', or '764490' */
function retrieveProposerInfo($parameterNameValueArray)
{
        
	global $PROPOSER_TYPE_PERSON;

	writeToFile(date("Y-m-d H:i:s"). ": retrieveProposerInfo() ENTER \n", LOG_LEVEL_INFO);

	$proposers = array();
	
	$selectString = "";
	//$bindFirstParam = "";
	//$bindSecondParam = "";
	$eachParameterPair;
	$i=1;//counter to see if we are in the first element of the array
	
	$bindparams = array();
	
	//echo $statementString;
	//create select string
	foreach($parameterNameValueArray as $eachParameterPair)
	{
		//$eachParameterPair->printData();
		
		//for all elements except the first, add 'and' and ','
		if($i>1){
			$selectString = " and ".$selectString;
			//$bindSecondParam = ", ".$bindSecondParam;
		}
		$selectString = " $eachParameterPair->name like ? ".$selectString;
		$bindparams[] = array(PDO::PARAM_STR, $eachParameterPair->value);
		//$bindFirstParam = "s".$bindFirstParam;
		//$bindSecondParam = $eachParameterPair->value.$bindSecondParam;
		$i++;
	}
	
	$bindparams = array_reverse($bindparams);
	
	
	/* Create a prepared statement */
	$statementString = "SELECT stateId, gender, firstName, lastName, type, company, countryOfResidence, date_FORMAT(birthDate, '%d-%m-%Y') as birthDate1, telephone, cellphone, email, profession, proposerType  FROM owner WHERE ".$selectString."  group by stateId";

	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams($bindparams);
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{	
     
		  $proposer = new proposer();
		  $proposer->owner = new owner();
 		  $proposer->owner->stateId = $eachResult['stateId'];
 		  $proposer->owner->oldStateId = $eachResult['stateId'];
		  $proposer->owner->gender = $eachResult['gender'];
		  $proposer->owner->firstName = stripcslashes($eachResult['firstName']);
		  $proposer->owner->lastName = stripcslashes($eachResult['lastName']);
		  $proposer->owner->type = $eachResult['type'];
		  $proposer->owner->profession = stripcslashes($eachResult['profession']);
		  $proposer->owner->company = $eachResult['company'];
		  $proposer->owner->countryOfResidence = $eachResult['countryOfResidence'];
		  $proposer->owner->birthDate = $eachResult['birthDate1'];
		  $proposer->owner->telephone = $eachResult['telephone'];
		  $proposer->owner->cellphone = $eachResult['cellphone'];
		  $proposer->owner->email = $eachResult['email'];
		  $proposer->owner->proposerType = $eachResult['proposerType'];
		  
		  //retrieve email preferences
		  $proposer->owner->emailPreference = retrieveEmailPreference($proposer->owner->stateId);
		  //$proposer->owner->printData();
		  
		  $proposer->addresses = retrieveOwnerAddressInfo($proposer->owner->stateId);	  
		  
		  
		  //owner license exists only for proposer type = PERSON
		  $proposer->license = retrieveLicense($proposer->owner->stateId);
		   
		  $proposers[] = $proposer;
	  }
	  
    writeToFile(date("Y-m-d H:i:s"). ": retrieveProposerInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $proposers;

}

/* retrieve history info, based on all search criteria */
function retrieveHistoriesInfo($parameterNameValueArray)
{	
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveHistoriesInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$histories = array();
	$selectString = "";
	//$bindFirstParam = "";
	//$bindSecondParam = "";
	$eachParameterPair;
	$i=1;//counter to see if we are in the first element of the array
	
	$bindparams = array();
	
	//echo $statementString;
	//create select string
	foreach($parameterNameValueArray as $eachParameterPair)
	{
		//$eachParameterPair->printData();
		
		//for all elements except the first, add 'and' and ','
		if($i>1){
			$selectString = " and ".$selectString;
			//$bindSecondParam = ", ".$bindSecondParam;
		}
		//looking for username
		if($eachParameterPair->name == 'username' )
		{
			$selectString = " ".$eachParameterPair->name." like ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR, $eachParameterPair->value);
		}
		//historyStartDate
		else if($eachParameterPair->name == 'historyStartDate' )
		{
			$selectString = " transDate >= ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR, $eachParameterPair->value);
		}
		//historyEndDate
		else if($eachParameterPair->name == 'historyEndDate' )
		{
			$selectString = " transDate < ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR, $eachParameterPair->value);
			
		}
		//looking for parameters, like saleId or stateId
		else
		{
			$selectString = " parameterName like '".$eachParameterPair->name."' and parameterValue like ? ".$selectString;
			$bindparams[] = array(PDO::PARAM_STR, $eachParameterPair->value);
		}
		$i++;
	}
	
	$bindparams = array_reverse($bindparams);
	
	//echo "select string=$selectString ";
	
	$statementString = "select transDate, username, type , subType, parameterName, parameterValue, note from history where ".$selectString." order by transDate asc";
	
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams($bindparams);
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();	
	
	foreach($results as $eachResult)
	{     
		  $history = new history();
		  $history->transDate = $eachResult['transDate'];
	      $history->username = $eachResult['username'];
	      $history->type = $eachResult['type'];
	      $history->parameterName = $eachResult['parameterName'];
	      $history->parameterValue = $eachResult['parameterValue'];
	      $history->note = $eachResult['note'];
		  
		  $histories[] = $history;
	  }
	  
     
	writeToFile(date("Y-m-d H:i:s"). ": retrieveHistoriesInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $histories;
}


/* retrieve additional drivers info for the contract, from DRIVERS table */
function retrieveDriversInfo($saleId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveDriversInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$drivers = array();
	
	$statementString = "SELECT driverId, stateId, firstName, lastName, date_FORMAT(birthDate, '%d-%m-%Y') as birthDate, countryOfBirth, licenseCountry, date_FORMAT(licenseDate, '%d-%m-%Y') as licenseDate, licenseType, telephone, profession FROM drivers WHERE saleId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $saleId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
	
		  $driver = new drivers();
		  $driver->driverId = $eachResult['driverId'];
 		  $driver->stateId = $eachResult['stateId'];
		  $driver->firstName = $eachResult['firstName'];
		  $driver->lastName = $eachResult['lastName'];
		  $driver->birthDate = $eachResult['birthDate'];	  
		  $driver->countryOfBirth = $eachResult['countryOfBirth'];
 		  $driver->licenseCountry = $eachResult['licenseCountry'];
		  $driver->licenseDate = $eachResult['licenseDate'];
		  $driver->licenseType = $eachResult['licenseType'];
		  $driver->telephone = $eachResult['telephone'];	
		  $driver->profession = $eachResult['profession'];	
		  
		  $drivers[] = $driver;
	  }
	  

	writeToFile(date("Y-m-d H:i:s"). ": retrieveDriversInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $drivers;

}

/* retrieve medical info for the contract, from MEDICAL table */
function retrieveMedicalInfo($saleId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveMedicalInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$medical = new medical();
	$medical->medicalInsuredPersonsArray = array();
	
	$statementString = "SELECT saleId, frequencyOfPayment, premium, planName, planMaximumLimit, deductible, excess, coInsurancePercentage, roomType, outpatientAmount  FROM medical WHERE saleId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $saleId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
	
		  $medical->saleId = $eachResult['saleId'];
 		  $medical->frequencyOfPayment = $eachResult['frequencyOfPayment'];
		  $medical->premium = $eachResult['premium'];
		  $medical->planName = $eachResult['planName'];
		  $medical->planMaximumLimit = $eachResult['planMaximumLimit'];	  
		  $medical->deductible = $eachResult['deductible'];
		  $medical->excess = $eachResult['excess'];
 		  $medical->coInsurancePercentage = $eachResult['coInsurancePercentage'];
		  $medical->roomType = $eachResult['roomType'];
		  $medical->outpatientAmount = $eachResult['outpatientAmount'];
		  
		  //retrieve all persons in the insurance
		  $medical->medicalInsuredPersonsArray = retrieveMedicalInsuredPerson($saleId);
		  
	  }
	  
	

	writeToFile(date("Y-m-d H:i:s"). ": retrieveMedicalInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $medical;

}


/* retrieve life insurance info for the contract, from LIFEINS table */
function retrieveLifeInsInfo($saleId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveLifeInsInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$lifeIns = new lifeIns();
	
	$statementString = "SELECT saleId, insuredFirstName, insuredLastName, frequencyOfPayment, annualPremium, monthlyPremium, planName, basicPlanAmount, totalPermanentDisabilityAmount, premiumProtectionAmount  FROM lifeins WHERE saleId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $saleId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
		
		  $lifeIns->saleId = $eachResult['saleId'];
		  $lifeIns->insuredFirstName = $eachResult['insuredFirstName'];
		  $lifeIns->insuredLastName = $eachResult['insuredLastName'];
 		  $lifeIns->frequencyOfPayment = $eachResult['frequencyOfPayment'];
		  $lifeIns->annualPremium = $eachResult['annualPremium'];
		  $lifeIns->planName = $eachResult['planName'];
		  $lifeIns->monthlyPremium = $eachResult['monthlyPremium'];	  
		  $lifeIns->basicPlanAmount = $eachResult['basicPlanAmount'];	  
		  $lifeIns->totalPermanentDisabilityAmount = $eachResult['totalPermanentDisabilityAmount'];	  
		  $lifeIns->premiumProtectionAmount = $eachResult['premiumProtectionAmount'];	  
	  }
	  
     

	writeToFile(date("Y-m-d H:i:s"). ": retrieveLifeInsInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $lifeIns;

}


/* retrieve property fire info for the contract, from PROPERTYFIRE table */
function retrievePropertyFireInfo($saleId)
{
        

	writeToFile(date("Y-m-d H:i:s"). ": retrievePropertyFireInfo() ENTER \n", LOG_LEVEL_INFO);

	$propertyfire = new propertyfire();
	
	$statementString = "SELECT saleId, description, typeOfPremises, buildingValue, outsideFixturesValue, contentsValue, valuableObjectsValue, yearBuilt, areaSqMt  FROM propertyfire WHERE saleId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $saleId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	foreach($results as $eachResult)
	{
		
	
		  $propertyfire->saleId = $eachResult['saleId'];
 		  $propertyfire->description = $eachResult['description'];
		  $propertyfire->typeOfPremises = $eachResult['typeOfPremises'];
		  $propertyfire->buildingValue = $eachResult['buildingValue'];
		  $propertyfire->outsideFixturesValue = $eachResult['outsideFixturesValue'];	  
		  $propertyfire->contentsValue = $eachResult['contentsValue'];
		  $propertyfire->valuableObjectsValue = $eachResult['valuableObjectsValue'];
 		  $propertyfire->yearBuilt = $eachResult['yearBuilt'];
		  $propertyfire->areaSqMt = $eachResult['areaSqMt'];
	  }
	  
     

	writeToFile(date("Y-m-d H:i:s"). ": retrievePropertyFireInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $propertyfire;

}

/* retrieve medical info for the contract, from MEDICALINSUREDPERSONS table */
function retrieveMedicalInsuredPerson($saleId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveMedicalInsuredPerson() ENTER \n", LOG_LEVEL_INFO);

	$medicalInsuredPersonArray = array();
	
	$statementString = "SELECT personId, saleId, firstName, lastName, date_FORMAT(birthDate, '%d-%m-%Y') as birthDate, stateId, telephone, gender  FROM medicalinsuredperson WHERE saleId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $saleId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	//var_dump($results);
	
	foreach($results as $eachResult)
	{
      
   		  $localMedicalInsuredPerson = new medicalInsuredPerson();
		  $localMedicalInsuredPerson->personId = $eachResult['personId'];
		  $localMedicalInsuredPerson->saleId = $eachResult['saleId'];
 		  $localMedicalInsuredPerson->firstName = $eachResult['firstName'];
		  $localMedicalInsuredPerson->lastName = $eachResult['lastName'];
		  $localMedicalInsuredPerson->birthDate = $eachResult['birthDate'];
		  $localMedicalInsuredPerson->stateId = $eachResult['stateId'];	  
		  $localMedicalInsuredPerson->telephone = $eachResult['telephone'];
		  $localMedicalInsuredPerson->gender = $eachResult['gender'];
		  
		  $medicalInsuredPersonArray[] = $localMedicalInsuredPerson;
	  }
	  
     
	writeToFile(date("Y-m-d H:i:s"). ": retrieveMedicalInsuredPerson() EXIT \n", LOG_LEVEL_INFO);
        
	return $medicalInsuredPersonArray;

}

/* retrieve employers liability info for the contract, from EMPLOYERSLIABILITY table */
function retrieveEmployersLiabilityInfo($saleId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveEmployersLiabilityInfo() ENTER \n", LOG_LEVEL_INFO);
        
	$employersLiability = new employersLiability();
	
	$statementString = "SELECT saleId, employersSocialInsuranceNumber, limitPerEmployee, limitPerEventOrSeriesOfEvents, limitDuringPeriodOfInsurance, employeesNumber, estimatedTotalGrossEarnings FROM employersliability WHERE saleId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $saleId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	

	foreach($results as $eachResult)
	{
		 
	  	  $employersLiability->saleId = $eachResult['saleId'];
 		  $employersLiability->employersSocialInsuranceNumber = $eachResult['employersSocialInsuranceNumber'];
		  $employersLiability->limitPerEmployee = $eachResult['limitPerEmployee'];
		  $employersLiability->limitPerEventOrSeriesOfEvents = $eachResult['limitPerEventOrSeriesOfEvents'];
		  $employersLiability->limitDuringPeriodOfInsurance = $eachResult['limitDuringPeriodOfInsurance'];	  
		  $employersLiability->employeesNumber = $eachResult['employeesNumber'];
 		  $employersLiability->estimatedTotalGrossEarnings = $eachResult['estimatedTotalGrossEarnings'];
	  }
	  
     
	
	writeToFile(date("Y-m-d H:i:s"). ": retrieveEmployersLiabilityInfo() EXIT \n", LOG_LEVEL_INFO);
        
	return $employersLiability;

}

/* retrieves all distinct values of $parameterName from $tableName */
function retrieveDistinctValues($parameterName, $tableName )
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveDistinctValues() ENTER \n", LOG_LEVEL_INFO);
        
	$distinctValues = array();
	
	$statementString = "SELECT distinct ".$parameterName." FROM ".$tableName;
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{
    
		  $distinctValues[] = $eachResult[$parameterName];
	}
	  
    
	
	writeToFile(date("Y-m-d H:i:s"). ": retrieveDistinctValues() EXIT \n", LOG_LEVEL_INFO);
        
	return $distinctValues;
	
	
}
/* retrieve information from the client global SYSTEMUSER table */
function retrieveUserInfoFromGlobalDatabase($username)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveUserInfoFromGlobalDatabase() ENTER \n", LOG_LEVEL_INFO);
        
	$users = array();
	
	$statementString = "SELECT username, role, status, clientName, productType, consecutiveFailLoginAttempts FROM systemuser WHERE username like ?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $username)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{
	
	
		  $user = new user();
		  $user->username = $eachResult['username'];
 		  $user->role = $eachResult['role'];
		  $user->status = $eachResult['status'];
		  $user->clientName = $eachResult['clientName'];	  
		  $user->productType = $eachResult['productType'];
		  $user->consecutiveFailLoginAttempts = $eachResult['consecutiveFailLoginAttempts'];
		  $users[] = $user;
	  }
	  
    

	writeToFile(date("Y-m-d H:i:s"). ": retrieveUserInfoFromGlobalDatabase() EXIT \n", LOG_LEVEL_INFO);
        
	return $users;

}



/* retrieve information from the database LICENSE table */
function retrieveLicense($stateId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveLicense() ENTER \n", LOG_LEVEL_INFO);

	$license = new license();
	
	$statementString = "SELECT licenseType, date_FORMAT(licenseDate, '%d-%m-%Y') as licenseDate, licenseCountry FROM license WHERE stateId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $stateId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{
	  $license->stateId = $stateId;
	  $license->licenseType = $eachResult['licenseType'];
	  $license->licenseDate = $eachResult['licenseDate'];
	  $license->licenseCountry = $eachResult['licenseCountry'];     
  	}
	return $license;

}


/* retrieve information from the database OWNER table */
function retrieveOwner($stateId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveOwner() ENTER \n", LOG_LEVEL_INFO);
    	
	$owner = new owner();
		
	$statementString = "SELECT firstName, lastName, stateId, telephone, cellphone, countryOfBirth, countryOfResidence, 
			date_FORMAT(birthDate, '%d-%m-%Y') as birthDate, profession, company, proposerType, email FROM owner WHERE stateId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $stateId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{
	
	
	  $owner->firstName = stripcslashes($eachResult['firstName']);
	  $owner->lastName = stripcslashes($eachResult['lastName']);
	  $owner->stateId = $eachResult['stateId'];
	  $owner->telephone = $eachResult['telephone'];
	  $owner->cellphone = $eachResult['cellphone'];
	  $owner->countryOfBirth = $eachResult['countryOfBirth'];
	  $owner->countryOfResidence = $eachResult['countryOfResidence'];
	  $owner->birthDate = $eachResult['birthDate'];
	  $owner->profession = $eachResult['profession'];
	  $owner->company = $eachResult['company'];
	  $owner->proposerType = $eachResult['proposerType'];
	  $owner->email = $eachResult['email'];
	
      
	}
	
	writeToFile(date("Y-m-d H:i:s"). ": retrieveOwner() EXIT \n", LOG_LEVEL_INFO);
        
	return $owner;

}

/* retrieve information from the database DRIVINGEXPERIENCE table */
function retrieveDrivingExperience($stateId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveDrivingExperience() ENTER \n", LOG_LEVEL_INFO);
        
	$owner = new owner();
	$drivingExperience = new drivingExperience();
	
	$statementString = "SELECT hasPreviousInsurance, countryOfInsurance, insuranceCompany, yearsOfExperience FROM drivingexperience WHERE stateId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $stateId)));
	$pdoConnection->executeStatement();
	$result = $pdoConnection->fetch();
	
	
	  $drivingExperience->stateId = $stateId;
	  $drivingExperience->hasPreviousInsurance = $result['hasPreviousInsurance'];
	  $drivingExperience->countryOfInsurance = $result['countryOfInsurance'];
	  $drivingExperience->insuranceCompany = $result['insuranceCompany'];
	  $drivingExperience->yearsOfExperience = $result['yearsOfExperience'];
	  
	
	writeToFile(date("Y-m-d H:i:s"). ": retrieveDrivingExperience() EXIT \n", LOG_LEVEL_INFO);
        
	return $drivingExperience;

}


/* retrieve owner state id that corresponds to this quoteid from the database QUOTATIONOWNER table */
function retrieveStateIdFromQuotationOwner($quoteId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveStateIdFromQuotationOwner() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "SELECT stateId FROM quotationowner WHERE quoteId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId)));
	$pdoConnection->executeStatement();
	$result = $pdoConnection->fetch();
	
	$returnStateId = $result['stateId'];
	 
	  
      
	writeToFile(date("Y-m-d H:i:s"). ": retrieveStateIdFromQuotationOwner() EXIT \n", LOG_LEVEL_INFO);
        
	return $returnStateId;

}


/* retrieve owner quote id that corresponds to this stateId from the database QUOTATIONOWNER table */
function retrieveQuoteIdFromQuotationOwner($stateId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveQuoteIdFromQuotationOwner() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "SELECT quoteId FROM quotationowner WHERE stateId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $stateId)));
	$pdoConnection->executeStatement();
	$result = $pdoConnection->fetch();
	
	
	  
	  $returnQuoteId = $result['quoteId'];
	 
	  
   

	writeToFile(date("Y-m-d H:i:s"). ": retrieveQuoteIdFromQuotationOwner() EXIT \n", LOG_LEVEL_INFO);
        
	return $returnQuoteId;

}


/* retrieve vehicle registration number that corresponds to this quoteid from the database QUOTATIONVEHICLE table */
function retrieveRegNumberFromQuotationVehicle($quoteId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveRegNumberFromQuotationVehicle() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "SELECT regNumber FROM quotationvehicle WHERE quoteId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId)));
	$pdoConnection->executeStatement();
	$result = $pdoConnection->fetch();
	
	$returnRegNumber = $result['regNumber'];
	
	writeToFile(date("Y-m-d H:i:s"). ": retrieveRegNumberFromQuotationVehicle() EXIT \n", LOG_LEVEL_INFO);
        
	return $returnRegNumber;

}

/* retrieve information from the database QUOTATION table
$parameterName = name of field in table. i.e stateId, quoteId 
$parameterValue = value the field should match. i.e '764490' or '15' */
function retrieveQuoteInfo($parameterName, $parameterValue)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveQuoteInfo() ENTER with parameterName=$parameterName, parameterValue=$parameterValue \n", LOG_LEVEL_INFO);
        
	$quotationArray = array();
	
	$statementString = "SELECT insuranceCompanyOfferingQuote, userName, q.quoteId as squoteId, canProvideOnlineQuote, date_FORMAT(entryDate, '%d-%m-%Y') as entryDate1, quoteAmount, offerSelected, userInfo, coverageType, o.firstName, o.lastName FROM quotation q, owner o, quotationowner qo WHERE q.quoteId=qo.quoteId and qo.stateId=o.stateId and ".$parameterName." like ? order by q.quoteId asc";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $parameterValue)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{
		
		  $quotation = new quotation();
		  $quotation->quoteId = $eachResult['squoteId'];
		  $quotation->userName = $eachResult['userName'];
		  $quotation->insuranceCompanyOfferingQuote = $eachResult['insuranceCompanyOfferingQuote'];
		  $quotation->canProvideOnlineQuote = $eachResult['canProvideOnlineQuote'];
		  $quotation->entryDate = $eachResult['entryDate1'];
		  $quotation->quoteAmount = $eachResult['quoteAmount'];
		  $quotation->offerSelected = $eachResult['offerSelected'];
		  $quotation->coverageType = $eachResult['coverageType'];
		  $quotation->userInfo= $eachResult['userInfo'];
		  $quotation->fullName= $eachResult['firstName'] . " " . $eachResult['lastName'];
		  
		  $quotationArray[] = $quotation;
	  }
	  
     

	writeToFile(date("Y-m-d H:i:s"). ": retrieveQuoteInfo() EXIT \n", LOG_LEVEL_INFO);
        
	 return $quotationArray;

}


/* retrieve information from the database REASONSWECANNOTPROVIDEONLINEQUOTE table */
function retrieveReasonsWeCannotProvideOnlineQuote($quoteId)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveReasonsWeCannotProvideOnlineQuote() ENTER \n", LOG_LEVEL_INFO);
        
	$reasonsWeCannotProvideOnlineQuotes = array();
	
	$statementString = "SELECT reason FROM reasonswecannotprovideonlinequote WHERE quoteId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quoteId)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{
		
		$reasonsWeCannotProvideOnlineQuote = new reasonsWeCannotProvideOnlineQuote();
		$reasonsWeCannotProvideOnlineQuote->reason = $eachResult['reason'];
		
		$reasonsWeCannotProvideOnlineQuotes[] = $reasonsWeCannotProvideOnlineQuote;
		}
	  
    

	writeToFile(date("Y-m-d H:i:s"). ": retrieveReasonsWeCannotProvideOnlineQuote() EXIT \n", LOG_LEVEL_INFO);
        
	return $reasonsWeCannotProvideOnlineQuotes;

}


/* retrieve all previous quotes from the database QUOTE table,
based on the unique userName. */
function retrieveAllPreviousUserQuotes($userName)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllPreviousUserQuotes() ENTER \n", LOG_LEVEL_INFO);

	$previousQuotes = array();
	
	$statementString = "SELECT quoteId, trim(canProvideOnlineQuote) as canProvideOnlineQuote, date_FORMAT(entryDate, '%d-%m-%Y') as entryDate, 
   	quoteAmount, insuranceCompanyOfferingQuote, userName, userInfo FROM quotation WHERE userName=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $userName)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{
		$quotation = new quotation();
		$quotation->quoteId = $eachResult['quoteId'];
		$quotation->canProvideOnlineQuote = $eachResult['canProvideOnlineQuote'];
		$quotation->entryDate = $eachResult['entryDate'];
		$quotation->quoteAmount = $eachResult['quoteAmount'];
		$quotation->insuranceCompanyOfferingQuote = $eachResult['insuranceCompanyOfferingQuote'];
		$quotation->userName = $eachResult['userName'];
		$quotation->userInfo = $eachResult['userInfo'];
		
		$previousQuotes[] = $quotation;
		}
	  
	  
	  
   
	
	 return $previousQuotes;

}

/* check if the username exists in the database */
function checkUserNameExists($username)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": checkUserNameExists() ENTER \n", LOG_LEVEL_INFO);
        
	$numberOfUsers = 0;
	
	$statementString = "SELECT count(*) as result FROM systemuser WHERE username=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $username)));
	$pdoConnection->executeStatement();
	$result = $pdoConnection->fetch();
	
	$numberOfUsers = $result['result'];

    
	
	return $numberOfUsers;

}
	

/* check for this entry, if there exists in the table in the database 
$tableName : the table to be accessed
$fieldName1 : first field in the table to be compared
$fieldValue1 : value of first field
$fieldName2 : second field in the table to be compared - NOT MANDATORY
$fieldValue2 : value of second field - NOT MANDATORY
 */
function checkEntryExistsInTable($tableName, $fieldName1, $fieldValue1, $fieldName2, $fieldValue2)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": checkEntryExistsInTable() ENTER table name=$tableName, field name1=$fieldName1, field value1=$fieldValue1 \n", LOG_LEVEL_INFO);
        
	$statement = "SELECT count(*) as result FROM ".$tableName." WHERE ".$fieldName1." = ? ";
	//echo $statement;
	
	if($fieldName2!="")
		$statement = $statement." and ".$fieldName2." = ? ";
	
	$statementString = $statement;
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	if($fieldName2=="")
		$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $fieldValue1)));
	else
		$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $fieldValue1),array(PDO::PARAM_STR, $fieldValue2)));

	
	$pdoConnection->executeStatement();
	$result = $pdoConnection->fetch();

	writeToFile(date("Y-m-d H:i:s"). ": checkEntryExistsInTable() EXIT \n", LOG_LEVEL_INFO);
        
	return $result['result'];

}

/* retrieve all claims related to the quoteId, from CLAIMS table 
$parameterName = name of field in table. i.e stateId, quoteId 
$parameterValue = value the field should match. i.e '764490' or '15' */
function retrieveClaims($parameterName, $parameterValue)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": retrieveClaims() ENTER \n", LOG_LEVEL_INFO);
        
	$claims = array();
	
	$statementString = "SELECT claimId, amount, date_FORMAT(claimDate, '%d-%m-%Y') as claimDate1, description, stateId, quoteId FROM claims where ".$parameterName." like ?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $parameterValue)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{
		$claim = new claim();
		$claim->claimId = $eachResult['claimId'];
		$claim->quoteId = $eachResult['quoteId'];
		$claim->stateId = $eachResult['stateId'];
		$claim->amount = $eachResult['amount'];
		$claim->claimDate = $eachResult['claimDate1'];
		$claim->description = $eachResult['description'];
		
		$claims[] = $claim;
		}
	  
	
	return $claims;

}




/* retrieve the emails and ids of all clients for the current account from SYSTEMUSER table */
function retrieveAllUserEmailsIds()
{
        
	$emailsIds = new emailsIds();

	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllUserEmailsIds() ENTER \n", LOG_LEVEL_INFO);
        
	$allEmails = '';
	$allStateIds = '';
	
	$statementString = "SELECT DISTINCT email, stateId FROM systemuser where email <> ''";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetchAll();
	
	
	foreach($results as $eachResult)
	{   	 
		  if($allEmails==' ')
		  {
		  	$allEmails = $eachResult['email'];
		  	$allStateIds = $eachResult['stateId'];
	  	  }
		  else
		  {
		  	$allEmails = $allEmails.', '.$eachResult['email'];
		  	$allStateIds = $allStateIds.', '.$eachResult['stateId'];
	  	  }
	}
		
	$emailsIds->allEmails = $allEmails;
	$emailsIds->allStateIds = $allStateIds;
	  
    

	writeToFile(date("Y-m-d H:i:s"). ": retrieveAllUserEmailsIds() EXIT \n", LOG_LEVEL_INFO);
        
	return $emailsIds;

}