<?php	

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
	
/* Store all the Quote related information to the database.
	if $runFromGlobalLocationOffice==true, then also insert contract information
	$quoteInfo : input from the Client
	$selectedProcessingInfo : result of processing of the request */
function storeQuoteInDatabase($quoteInfo)
{
	global $YES_CONSTANT, $NO_CONSTANT, $ADDRESS_TYPE_CORRESPONDENCE;
	global $selectedProcessingInfo, $runFromGlobalLocation, $runFromGlobalLocationOffice;

	writeToFile(date("Y-m-d H:i:s"). ": storeQuoteInDatabase() ENTER \n", LOG_LEVEL_INFO);
        
	$reasonsWeCannotProvideOnlineQuotes[] = array();
	
	//set insurance company
	$quoteInfo->insuranceCompanyOfferingQuote = $selectedProcessingInfo->insuranceCompanyOfferingQuote;
	//$selectedProcessingInfo->printdata();
	
	/* QUOTE TABLE */
	$quote = fillQuoteClass($quoteInfo);
	
	/* VEHICLE TABLE */
	$vehicle = fillVehicleClass($quoteInfo);
		
	/*ADDITIONALCHARGESFACTORS TABLE*/
	foreach($quoteInfo->additionalChargesInPolicyQuotationArray as $eachCharge)
	{
		//add processing decision. YES, NO, CANNOT_ADD
		//$eachCharge->printData();
		fillChargeDecision($eachCharge);
		//$eachCharge->printData();
	}
	
	//$additionalChargesFactors = fillAdditionalChargesFactorsClass($quoteInfo);
	
	/*OPTIONALCOVERAGES TABLE*/
	//use the coverages in selectedprocessinginfo
	//foreach($quoteInfo->coveragesInPolicyQuotationArray as $eachCoverage)
	foreach($selectedProcessingInfo->coveragesInPolicyQuotationArray as $eachCoverage)
	{
		//add processing decision. YES, NO, CANNOT_ADD
		fillCoverageDecision($eachCoverage);
		//$eachCoverage->printData();
	}
	
		
	/*LICENSE TABLE*/
	$license = fillLicenseClass($quoteInfo);
	
	/* CLAIMS TABLE */
	//TODO TODO Fill stateId
	$claims = $quoteInfo->claims;
	
	/* OWNER TABLE */
	$owner = fillOwnerClass($quoteInfo);
	
	/* DRIVINGEXPERIENCE TABLE */
	$drivingExperience = fillDrivingExperienceClass($quoteInfo);
	
	/* OWNERADDRESS TABLE - Create dummy address for compatibility*/
	$ownerAddress = new ownerAddress();
	$ownerAddress->stateId = $quoteInfo->stateId;
	$ownerAddress->addressType = $ADDRESS_TYPE_CORRESPONDENCE;
	$ownerAddress->street  = "Wake Lane 25";
	$ownerAddress->areaCode  = "3070";
	$ownerAddress->city  = "Raleigh";
	$ownerAddress->state  = "NC";
	$ownerAddress->country  = "USA";
	
	//$ownerAddress->printData();
	
	/* REASONSWECANNOTPROVIDEONLINEQUOTE table 
	only filled when we cannot provide an online quote */
	if($selectedProcessingInfo->canProvideOnlineQuote == $NO_CONSTANT){
		$selectedProcessingInfo->finalCost = 0; //zero this, since it may have some value already
		$reasonsWeCannotProvideOnlineQuotes = fillReasonsWeCannotProvideOnlineQuoteClass();
	}
	else
		$reasonsWeCannotProvideOnlineQuotes = null;
	
	
	
	//insert all quote info - new quote ID is generated for each new contract
	$quoteId = insertAllQuoteInfo($quote, $vehicle, $quoteInfo->additionalChargesInPolicyQuotationArray, 
		$selectedProcessingInfo->coveragesInPolicyQuotationArray, $quoteInfo->discountsInQuotationArray, $license, $claims, $owner, $ownerAddress, $reasonsWeCannotProvideOnlineQuotes, $drivingExperience);
	
	$quoteInfo->quoteId = $quoteId;

	writeToFile(date("Y-m-d H:i:s"). ": storeQuoteInDatabase() EXIT with quoteId=$quoteId \n", LOG_LEVEL_INFO);
        
	return $quoteId;
	
}



/* fill the reasonsWeCannotProvideOnlineQuote Class, for inserting data to REASONSWECANNOTPROVIDEONLINEQUOTE table */
function fillReasonsWeCannotProvideOnlineQuoteClass()
{	
	global $selectedProcessingInfo;
	
	$reasonsWeCannotProvideOnlineQuotes = array();
	
	foreach($selectedProcessingInfo->reasonsWeCannotProvideOnlineQuote as $reasonsWeCannotProvideOnlineQuote)
	{
		$reasonsWeCannotProvideOnlineQuoteClass = new reasonsWeCannotProvideOnlineQuote();
		$reasonsWeCannotProvideOnlineQuoteClass->reason = $reasonsWeCannotProvideOnlineQuote;
		
		$reasonsWeCannotProvideOnlineQuotes[] = $reasonsWeCannotProvideOnlineQuoteClass;
	}
	
	return $reasonsWeCannotProvideOnlineQuotes;
}

/* fill the ownerAddress Class, for inserting data to OWNERADDRESS table */
function fillOwnerAddressClass($inputStructure)
{
	$ownerAddress = new ownerAddress();
	$ownerAddress->addressType = $inputStructure->addressType;
	$ownerAddress->street  = $inputStructure->street;
	$ownerAddress->areaCode  = $inputStructure->areaCode;
	$ownerAddress->city  = $inputStructure->city;
	$ownerAddress->state  = $inputStructure->state;
	$ownerAddress->country  = $inputStructure->country;
	$ownerAddress->stateId  = $inputStructure->stateId;
	$ownerAddress->oldStateId  = $inputStructure->oldStateId;
		
	return $ownerAddress;
}


/* fill the drivingExperience Class, for inserting data to DRIVINGEXPERIENCE table */
function fillDrivingExperienceClass($inputStructure)
{
	$drivingExperience = new drivingExperience();
	$drivingExperience->stateId = $inputStructure->stateId;
	$drivingExperience->hasPreviousInsurance = $inputStructure->hasPreviousInsurance;
	$drivingExperience->countryOfInsurance  = $inputStructure->countryOfInsurance;
	$drivingExperience->insuranceCompany  = $inputStructure->insuranceCompany;
	$drivingExperience->yearsOfExperience  = $inputStructure->yearsOfExperience;
		
	return $drivingExperience;
}


/* fill the sale Class, for inserting data to SALE table */
function fillSaleClass($inputStructure)
{
	$sale = new sale();
	$sale->saleId = trim($inputStructure->contractNumber);
	$sale->company = $inputStructure->insuranceCompanyOfferingQuote;
	$sale->insuranceType  = $inputStructure->insuranceType;
	$sale->coverageType  = $inputStructure->coverageType;
	$sale->saleStartDate  = $inputStructure->saleStartDate;
	$sale->saleEndDate  = $inputStructure->saleEndDate;
	$sale->stateId  = $inputStructure->stateId;
	$sale->oldSaleId  = $inputStructure->oldSaleId;
	$sale->associate  = $inputStructure->associate;
	$sale->producer  = $inputStructure->producer;
	
	return $sale;
}

/* fill the drivers Class, for inserting data to DRIVERS table */
function fillDriversClass($inputStructure)
{	
	$drivers = array();
	
	//if( $contract->drivers->firstName!='' OR $contract->drivers->lastName!='' )
	
	foreach($inputStructure->drivers as $eachDriver)
	{
		$driverClass = new drivers();
		$driverClass->driverId = $eachDriver->driverId;
		$driverClass->saleId = $inputStructure->contractNumber;
		$driverClass->stateId = $eachDriver->stateId;
		$driverClass->firstName = $eachDriver->firstName;
		$driverClass->lastName = $eachDriver->lastName;
		$driverClass->birthDate = $eachDriver->birthDate;
		$driverClass->countryOfBirth = $eachDriver->countryOfBirth;
		$driverClass->licenseCountry = $eachDriver->licenseCountry;
		$driverClass->licenseDate = $eachDriver->licenseDate;
		$driverClass->licenseType = $eachDriver->licenseType;
		$driverClass->telephone = $eachDriver->telephone;
		$driverClass->profession = $eachDriver->profession;
		
		$drivers[] = $driverClass;
	}
	
	return $drivers;
}


/* fill the license Class, for inserting data to lICENSE table */
function fillLicenseClass($inputStructure)
{
	global $NO_CONSTANT, $YES_CONSTANT;
	
	$license = new license();
	$license->licenseType = $inputStructure->licenseType;
	$license->licenseDate = $inputStructure->licenseDate;
	$license->licenseCountry  = $inputStructure->licenseCountry;
	$license->stateId  = $inputStructure->stateId;
	$license->oldStateId = $inputStructure->oldStateId;
	return $license;
}


/* fill the owner Class, for inserting data to OWNER table */
function fillOwnerClass($inputStructure)
{
	global $NO_CONSTANT, $YES_CONSTANT;
	
	$owner = new owner();
	$owner->quoteId = $inputStructure->quoteId;
	$owner->email = $inputStructure->email;
	$owner->firstName = $inputStructure->firstName;
	$owner->lastName = $inputStructure->lastName;
	$owner->stateId = $inputStructure->stateId;
	$owner->oldStateId = $inputStructure->oldStateId;
	$owner->telephone = $inputStructure->telephone;
	$owner->cellphone = $inputStructure->cellphone;
	$owner->profession = $inputStructure->profession;
	$owner->countryOfBirth = $inputStructure->countryOfBirth;
	$owner->countryOfResidence = $inputStructure->licenseCountry;
	$owner->birthDate  = $inputStructure->birthDate;
	$owner->proposerType  = $inputStructure->proposerType;
	return $owner;
}



/* fill the OPTIONALCOVERAGES Class, for inserting data to OPTIONALCOVERAGES table.
These are because of additional benefits the user added. */
function fillCoverageDecision(&$eachCoverageRequestedInQuotation)
{
	global $selectedProcessingInfo;
	global $NO_CONSTANT, $YES_CONSTANT, $CANNOT_ADD;

	writeToFile(date("Y-m-d H:i:s"). ": fillCoverageDecision() for code $eachCoverageRequestedInQuotation->code, ENTER \n", LOG_LEVEL_INFO);
        
	//$coveragesInPolicyQuotation->decision = $YES_CONSTANT;
	
	//NO OFFER SELECTED - ADD BENEFITS INDIVIDUALLY
	if($selectedProcessingInfo->anyOfferSelected==$NO_CONSTANT)
	{
		//not included benefits are not added in this array!!! - BUG	
		$coverageFound = 0;
		//$localVehicleType->printData();
		//check coverages added during processing
		foreach($selectedProcessingInfo->addedNotMandatoryCosts as $eachCoverage)
		{
			writeToFile(date("Y-m-d H:i:s"). ": No package Selected:  code:$eachCoverage->code \n", LOG_LEVEL_INFO);
                        
			if($eachCoverage->code==$eachCoverageRequestedInQuotation->code)
			{
				$eachCoverageRequestedInQuotation->charge = $eachCoverage->charge;
				
				$coverageFound = 1;

                                writeToFile(date("Y-m-d H:i:s"). ": code   match found - breaking \n", LOG_LEVEL_INFO);
                                
				//$eachCoverage->printData();
				
				if($eachCoverage->stillNeeded == -1 )
				{
					$eachCoverageRequestedInQuotation->decision = $YES_CONSTANT;
					//echo "decistion = YES";
				}
				else
				{
					$eachCoverageRequestedInQuotation->decision = $CANNOT_ADD;
					//echo "decistion = cannot add";
				}
				break;
			}
		}
		
		//coverage requested by the client not found in the coverages available in XML
		if($coverageFound == 0)
		{
			$eachCoverageRequestedInQuotation->decision = $CANNOT_ADD;
		}
	}
	//A PACKAGE IS SELECTED - ADD ALL BENEFITS WITHTOUT CHARGES
	else 
	{
		writeToFile(date("Y-m-d H:i:s"). ": fillCoverageDecision(): offer:$selectedProcessingInfo->selectedOfferName \n", LOG_LEVEL_INFO);
                
		//read packages directly from XML
		foreach($selectedProcessingInfo->inclusiveBenefit->optionalCoveragesFromXMLArray as $eachCoverage)
		{
			writeToFile(date("Y-m-d H:i:s"). ": code:$eachCoverage->code \n", LOG_LEVEL_INFO);
                        
			if($eachCoverage->code==$eachCoverageRequestedInQuotation->code)
			{
                                writeToFile(date("Y-m-d H:i:s"). ": code   match found - breaking \n", LOG_LEVEL_INFO);
                                
				if($eachCoverage->applicable == $YES_CONSTANT )
					$eachCoverageRequestedInQuotation->decision = $YES_CONSTANT;
				else
					$eachCoverageRequestedInQuotation->decision = $CANNOT_ADD;
				break;
			}
		}
		
	}

        writeToFile(date("Y-m-d H:i:s"). ": fillCoverageDecision() EXIT, decision=$eachCoverageRequestedInQuotation->decision ENTER \n", LOG_LEVEL_INFO);
		
}



/* fill the Quote Class, for inserting data to QUOTE table */
function fillQuoteClass($quoteInfo)
{
	global $selectedProcessingInfo;
	global $NO_CONSTANT, $YES_CONSTANT;
	
	$quotation = new quotation();
	$quotation->canProvideOnlineQuote = $selectedProcessingInfo->canProvideOnlineQuote;
	$quotation->entryDate = date("Y/m/d");
	$quotation->insuranceCompanyOfferingQuote = $selectedProcessingInfo->insuranceCompanyOfferingQuote;
	$quotation->offerSelected = $selectedProcessingInfo->selectedOfferName;
	$quotation->quoteAmount = ceil($selectedProcessingInfo->finalCost);//round value
	
	if(isset($_SESSION['username']))
	{
		$quotation->userName = $_SESSION['username'];
	}
	else 
		$quotation->userName = '';
		
	$quotation->userInfo = $quoteInfo->userInfo;
	$quotation->coverageType = $quoteInfo->coverageType;
	
	return $quotation;
}


/* fill the Quote Class, for inserting data to QUOTATION table */
function fillQuotationClassForContract($inputStructure)
{
	global $NO_CONSTANT, $YES_CONSTANT;
	
	$quotation = new quotation();
	$quotation->quoteId = $inputStructure->quoteId;
	$quotation->canProvideOnlineQuote = $YES_CONSTANT;
	$quotation->entryDate = date("Y/m/d");
	$quotation->insuranceCompanyOfferingQuote = $inputStructure->insuranceCompanyOfferingQuote;
	$quotation->offerSelected = $inputStructure->offerSelected;
	
	if(isset($_SESSION['username']))
	{
		$quotation->userName = $_SESSION['username'];
	}
	else 
		$quotation->userName = '';
		
	$quotation->coverageType = $inputStructure->sale->coverageType;
	
	return $quotation;
}



/* fill the vehicle Class, for inserting data to VEHICLE table */
function fillVehicleClass($inputStructure)
{	
	$vehicle = new vehicle();
	$vehicle->saleId = trim($inputStructure->contractNumber);
	if(isset($_SESSION['saleId']))
		$vehicle->oldSaleId = $_SESSION['saleId'];
	$vehicle->regNumber = $inputStructure->regNumber;
	$vehicle->vehicleType = $inputStructure->vehicleType;
	$vehicle->make = $inputStructure->vehicleMake;
	$vehicle->model = $inputStructure->vehicleModel;
	$vehicle->cubicCapacity = $inputStructure->cubicCapacity;
	$vehicle->manufacturedYear = $inputStructure->vehicleManufacturedYear;
	$vehicle->sumInsured = $inputStructure->coverageAmount;
	$vehicle->vehicleDesign = $inputStructure->vehicleDesign;
	$vehicle->steeringWheelSide = $inputStructure->steeringWheelSide;
	$vehicle->isTaxFree = $inputStructure->isTaxFree;
	$vehicle->isUsedForDeliveries = $inputStructure->isUsedForDeliveries;
	$vehicle->hasVisitorPlates = $inputStructure->hasVisitorPlates;
	
	return $vehicle;
}



/* fill the CHARGESINQUOTATION Class, for inserting data to CHARGESINQUOTATION table.
These are all the mandatory charges added in the policy. */
function fillChargeDecision(&$chargeInPolicyQuotation)
{
	global $selectedProcessingInfo;
	global $NO_CONSTANT, $YES_CONSTANT, $CANNOT_ADD;

	writeToFile(date("Y-m-d H:i:s"). ": fillChargeDecision() for code $chargeInPolicyQuotation->code, ENTER \n", LOG_LEVEL_INFO);
        
	//$coveragesInPolicyQuotation->decision = $YES_CONSTANT;
	
	foreach($selectedProcessingInfo->addedMandatoryCosts as $eachCharge)
	{
		if($eachCharge->code==$chargeInPolicyQuotation->code)
		{
                        writeToFile(date("Y-m-d H:i:s"). ": match found for code $chargeInPolicyQuotation->code, mandatory costs charge=$eachCharge->charge, chargeInPolicyQuotation->charge=$chargeInPolicyQuotation->charge \n", LOG_LEVEL_INFO);
                        
			$chargeInPolicyQuotation->charge = $eachCharge->charge;
			
			if( $eachCharge->charge > 0 )
				$chargeInPolicyQuotation->decision = $YES_CONSTANT;
			else
				$chargeInPolicyQuotation->decision = $NO_CONSTANT;
				
			break;
			
		}
	}

        writeToFile(date("Y-m-d H:i:s"). ": fillChargeDecision() EXIT, decision=$chargeInPolicyQuotation->decision \n", LOG_LEVEL_INFO);
        
		
}



?>
