<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//FUNCTIONS THAT UPDATE EXISTING DATA IN THE DATABASE

/* update existing user password in the database */
function updateUserPassword($username, $password)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": updateUserPassword() ENTER \n", LOG_LEVEL_INFO);
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "UPDATE systemuser SET password=? WHERE username=?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $password),
			array(PDO::PARAM_STR, $username)));
	
	$error = $pdoConnection->executeStatement();

}



/* update systemuser[consecutiveFailLoginAttempts] in the Global database */
function updateConsecutiveFailLoginAttempts($username, $setClause)
{
        
	writeToFile(date("Y-m-d H:i:s"). ": updateConsecutiveFailLoginAttempts() ENTER \n", LOG_LEVEL_INFO);
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "UPDATE systemuser SET ".$setClause. " WHERE username=?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $username)));
	
	$error = $pdoConnection->executeStatement();	      
	
}


/* update existing user data in the database */
function updateUserData($user)
{
	$user->birthDate = date("Y-m-d H:i:s", strtotime($user->birthDate));
	$user->licenseIssueDate = date("Y-m-d H:i:s", strtotime($user->licenseIssueDate));
	
	writeToFile(date("Y-m-d H:i:s"). ": updateUserData() ENTER \n", LOG_LEVEL_INFO);
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "UPDATE systemuser SET stateId=?, producer=?, gender=?, firstName=?, lastName=?,telephone=?, cellphone=?, profession=?, email=?, birthDate=?, licenseIssueDate=? WHERE username=?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $user->stateId),
			array(PDO::PARAM_STR, $user->producer),
			array(PDO::PARAM_STR, $user->gender),
			array(PDO::PARAM_STR, $user->firstName),
			array(PDO::PARAM_STR, $user->lastName),
			array(PDO::PARAM_STR, $user->telephone),
			array(PDO::PARAM_STR, $user->cellphone),
			array(PDO::PARAM_STR, $user->profession),
			array(PDO::PARAM_STR, $user->email),
			array(PDO::PARAM_STR, $user->birthDate),
			array(PDO::PARAM_STR, $user->licenseIssueDate),
			array(PDO::PARAM_STR, $user->username)));
	
	$error = $pdoConnection->executeStatement();
	
    writeToFile(date("Y-m-d H:i:s"). ": updateUserData() ENTER \n", LOG_LEVEL_INFO);
        
	return $error;
}


/* update existing user data in the database */
function updateEmailPreference($emailPreference)
{
	writeToFile(date("Y-m-d H:i:s"). ": updateEmailPreference() ENTER \n", LOG_LEVEL_INFO);
	
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "insert into emailpreferences(stateId,preferenceCode,preferenceFrequency)  values(?,?,?) 
			on duplicate key UPDATE preferenceCode=?, preferenceFrequency=?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $emailPreference->stateId),
			array(PDO::PARAM_STR, $emailPreference->preferenceCode),
			array(PDO::PARAM_STR, $emailPreference->preferenceFrequency),
			array(PDO::PARAM_STR, $emailPreference->preferenceCode),
			array(PDO::PARAM_STR, $emailPreference->preferenceFrequency)));
	
	$error = $pdoConnection->executeStatement();

	writeToFile(date("Y-m-d H:i:s"). ": updateEmailPreference() ENTER \n", LOG_LEVEL_INFO);
        
	return $error;
}


/* update all tables in the database, related to the contract */
function updateContractData($contract)
{
	global $INSURANCE_TYPE_MOTOR, $INSURANCE_TYPE_EMPLOYER_LIABILITY, $INSURANCE_TYPE_MEDICAL, $INSURANCE_TYPE_LIFE, $INSURANCE_TYPE_FIRE_PROPERTY, $HISTORY_TYPE_CONTRACT;
	global $SALE_STATUS_ACTIVE, $SALE_STATUS_EXPIRED;

	$localError = 0;
	
	writeToFile(date("Y-m-d H:i:s"). ": updateContractData() ENTER \n", LOG_LEVEL_INFO);
        
	//$owner = fillOwnerClass($contract);
	//$owner->printData();
	
	//$license = fillLicenseClass($contract);
	//$license->printData();
	//$sale = fillSaleClass($contract);
	//$sale->printData();
	$quotation = fillQuotationClassForContract($contract);
	//$vehicle = fillVehicleClass($contract);
	
	//$drivers = fillDriversClass($contract);
	
	//Step 1 - retrieve owner state id
	//$oldStateId = retrieveStateIdFromQuotationOwner($owner->quoteId);
	//$owner->oldStateId = $oldStateId;
	//Step 2 - update owner table
	$localError = updateOwnerData($contract->owner);
	if($localError == -1 )
		return $localError;
		
	//$contract->printData();
	$localStartDate = date("d-m-Y", strtotime($contract->sale->saleStartDate));
	$localEndDate = date("d-m-Y", strtotime($contract->sale->saleEndDate));
	
        
	//if contract has already expired, set status to expired
	  //PHP COMPARE DATES - CONVER TO STRTOTIME FIRST!!!
	  if(strtotime($contract->sale->saleEndDate) < strtotime(date("Y-m-d")) && $contract->sale->status == $SALE_STATUS_ACTIVE )
	  {
		  $contract->sale->status = $SALE_STATUS_EXPIRED;
		  //updateSaleStatus($contract->contractNumber, $contract->status);
	  }
	  //contact was EXPIRED or CANCELLED, but we changed the date to future date
	  if(strtotime($contract->sale->saleEndDate) >= strtotime(date("Y-m-d")) && $contract->sale->status != $SALE_STATUS_ACTIVE )
	  {
		  $contract->sale->status = $SALE_STATUS_ACTIVE;
		  //updateSaleStatus($contract->contractNumber, $contract->status);
	  }
	  
	$localError = updateSaleData($contract->sale);
	if($localError == -1 )
		return $localError;
	
	//contract number change 
	if($contract->sale->oldSaleId != $contract->sale->saleId )
	{
		//add history	
		$localHistory = new history();
		$localHistory->username = $_SESSION['username'];
		$localHistory->type = $HISTORY_TYPE_CONTRACT;
		$localHistory->parameterName = "saleId";
		$localHistory->parameterValue = $contract->sale->oldSaleId;
		$localHistory->note = "$localHistory->username : Contract with $localHistory->parameterName = $localHistory->parameterValue. Update contract number from ".$contract->sale->oldSaleId." to ".$contract->sale->saleId;
		insertNewHistory($localHistory);
		
		//rename any uploaded files
		renameUploadedFiles($contract->sale->oldSaleId, $contract->sale->saleId);
	}
	
	//start/end date - add history	
	if($localStartDate != $contract->sale->oldSaleStartDate || $localEndDate != $contract->sale->oldSaleEndDate )
	{
		$localHistory = new history();
		$localHistory->username = $_SESSION['username'];
		$localHistory->type = $HISTORY_TYPE_CONTRACT;
		$localHistory->parameterName = "saleId";
		$localHistory->parameterValue = $contract->sale->oldSaleId;
		$localHistory->note = "$localHistory->username : Contract with $localHistory->parameterName = $localHistory->parameterValue. Update [start,end] dates from [".$contract->sale->oldSaleStartDate.",".$contract->sale->oldSaleEndDate."] to [$localStartDate,$localEndDate]";
		insertNewHistory($localHistory);
	}
	
	$localError = updateQuotationData($quotation);
	if($localError == -1 )
		return $localError;
	
    
    //if we have a change in insurance type, means that the coverages are different
    //delete old coverages - delete entries in tables
    if($contract->sale->insuranceType != $contract->sale->previousInsuranceType )
    {
		//deleteAllCoveragesInPolicy($contract->sale->saleId);    
		
		//delete all data related to PREVIOUS insurance type
		if($contract->sale->previousInsuranceType == $INSURANCE_TYPE_MOTOR )
		{
			deleteFromTable('vehicle', 'saleId' , $contract->sale->saleId);
			deleteFromTable('coveragesinpolicy', 'saleId' , $contract->sale->saleId);
			deleteFromTable('drivers', 'saleId' , $contract->sale->saleId);
		}
		else if($contract->sale->previousInsuranceType == $INSURANCE_TYPE_EMPLOYER_LIABILITY )
		{
			deleteFromTable('employersliability', 'saleId' , $contract->sale->saleId);
			deleteFromTable('coveragesinpolicy', 'saleId' , $contract->sale->saleId);
			
		}
		else if($contract->sale->previousInsuranceType == $INSURANCE_TYPE_MEDICAL )
		{
			deleteFromTable('medical', 'saleId' , $contract->sale->saleId);
			deleteFromTable('medicalinsuredperson', 'saleId' , $contract->sale->saleId);
			deleteFromTable('coveragesinpolicy', 'saleId' , $contract->sale->saleId);
			
		}
		else if($contract->sale->previousInsuranceType == $INSURANCE_TYPE_LIFE )
		{
			deleteFromTable('lifeins', 'saleId' , $contract->sale->saleId);
			deleteFromTable('coveragesinpolicy', 'saleId' , $contract->sale->saleId);			
		}
		else if($contract->sale->previousInsuranceType == $INSURANCE_TYPE_FIRE_PROPERTY )
		{
			deleteFromTable('propertyfire', 'saleId' , $contract->sale->saleId);
			deleteFromTable('coveragesinpolicy', 'saleId' , $contract->sale->saleId);
		}
    }
    
	//MOTOR
	if($contract->sale->insuranceType == $INSURANCE_TYPE_MOTOR )
	{
		insertOwnerLicense($contract->motor->license);
		
		deleteFromTable('vehicle', 'saleId' , $contract->sale->oldSaleId);
		insertVehicle($contract->motor->vehicle);
		
		insertDrivingExperience($contract->motor->drivingExperience);
	}
	//EMPLOYER LIABILITY
	else if($contract->sale->insuranceType == $INSURANCE_TYPE_EMPLOYER_LIABILITY )
	{
		insertEmployersLiability($contract->employersLiability);
	}
	
	//MEDICAL
	else if($contract->sale->insuranceType == $INSURANCE_TYPE_MEDICAL )
	{
		insertMedical($contract->medical);
	}
	
	//LIFE
	else if($contract->sale->insuranceType == $INSURANCE_TYPE_LIFE )
	{
		insertLifeIns($contract->lifeIns);
	}
	
	//PROPERTY FIRE
	else if($contract->sale->insuranceType == $INSURANCE_TYPE_FIRE_PROPERTY )
	{
		insertPropertyFire($contract->propertyFire);
	}
	

	writeToFile(date("Y-m-d H:i:s"). ": updateContractData() ENTER \n", LOG_LEVEL_INFO);
        
	return $localError;
}




/* update data in OWNER table  */
function updateOwnerData($owner)
{
	writeToFile(date("Y-m-d H:i:s"). ": updateOwnerData() ENTER \n", LOG_LEVEL_INFO);

	//$owner->printData();
	$owner->birthDate = date("Y-m-d H:i:s", strtotime($owner->birthDate));
	
	$statementString = "UPDATE owner SET stateId=?, firstName=?, lastName=?, type=?, birthDate=?, countryOfBirth=?, profession=?, telephone=?, cellphone=?, email=?, gender=?, proposerType=? WHERE stateId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $owner->stateId),
			array(PDO::PARAM_STR, $owner->firstName),
			array(PDO::PARAM_STR, $owner->lastName),
			array(PDO::PARAM_STR, $owner->type),
			array(PDO::PARAM_STR, $owner->birthDate),
			array(PDO::PARAM_STR, $owner->countryOfBirth),
			array(PDO::PARAM_STR, $owner->profession),
			array(PDO::PARAM_STR, $owner->telephone),
			array(PDO::PARAM_STR, $owner->cellphone),
			array(PDO::PARAM_STR, $owner->email),
			array(PDO::PARAM_STR, $owner->gender),
			array(PDO::PARAM_STR, $owner->proposerType),
			array(PDO::PARAM_STR, $owner->oldStateId)));
	$error = $pdoConnection->executeStatement();
	
  //update quotationOwner and sale table ONLY when owner table is updated 
    if($error!=-1)
    {
      	//Step2 - update QuotationOwner, in case stateId is updated
		updateQuotationOwnerData($owner);
		
		//Step3 - update sale data, in case stateId is updated
		updateSaleStateId($owner->oldStateId, $owner->stateId);
		
	 }
		
	return $error;
	
}





/* update data in QUOTATIONOWNER table  */
function updateQuotationOwnerData($owner)
{	       
	writeToFile(date("Y-m-d H:i:s"). ": updateQuotationOwnerData() ENTER \n", LOG_LEVEL_INFO);
	
	$statementString = "UPDATE quotationowner SET stateId=? WHERE stateId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $owner->stateId),
			array(PDO::PARAM_STR, $owner->oldStateId)));
	$error = $pdoConnection->executeStatement();
	
}

/* update data in QUOTATION table  */
function updateQuotationData($quotation)
{        
	writeToFile(date("Y-m-d H:i:s"). ": updateQuotationData() ENTER \n", LOG_LEVEL_INFO);
	
	$statementString = "UPDATE quotation SET insuranceCompanyOfferingQuote=?, offerSelected=?, coverageType=? WHERE quoteId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $quotation->insuranceCompanyOfferingQuote),
			array(PDO::PARAM_STR, $quotation->offerSelected),
			array(PDO::PARAM_STR, $quotation->coverageType),
			array(PDO::PARAM_STR, $quotation->quoteId)));
	$error = $pdoConnection->executeStatement();
	  	 	
	return $error;
	
}


/* update owner state Id in SALE table  */
function updateSaleStateId($oldStateId, $newStateId)
{
	writeToFile(date("Y-m-d H:i:s"). ": updateSaleStateId() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "UPDATE sale SET stateId=? WHERE stateId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $newStateId),
			array(PDO::PARAM_STR, $oldStateId)));
	$error = $pdoConnection->executeStatement();
	
	return $error;
	
}


/* update data in SALE table  */
function updateSaleData($sale)
{
	       
	writeToFile(date("Y-m-d H:i:s"). ": updateSaleData() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "UPDATE sale SET saleId=?, company=?, insuranceType=?, coverageType=?, startDate=?, endDate=?, associate=?, producer=?, status=? WHERE saleId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $sale->saleId),
			array(PDO::PARAM_STR, $sale->company),
			array(PDO::PARAM_STR, $sale->insuranceType),
			array(PDO::PARAM_STR, $sale->coverageType),
			array(PDO::PARAM_STR, $sale->saleStartDate),
			array(PDO::PARAM_STR, $sale->saleEndDate),
			array(PDO::PARAM_STR, $sale->associate),
			array(PDO::PARAM_STR, $sale->producer),
			array(PDO::PARAM_STR, $sale->status),
			array(PDO::PARAM_STR, $sale->oldSaleId)));
	$error = $pdoConnection->executeStatement();
	
	
	writeToFile(date("Y-m-d H:i:s"). ": updateSaleData() EXIT \n", LOG_LEVEL_INFO);	

	
	return $error;
	
}



/* update sale STATUS  */
function updateSaleStatus($saleId, $status)
{
	writeToFile(date("Y-m-d H:i:s"). ": updateSaleStatus() ENTER \n", LOG_LEVEL_INFO);
        
	$statementString = "UPDATE sale SET status=? WHERE saleId=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $status),
			array(PDO::PARAM_STR, $saleId)));
	$error = $pdoConnection->executeStatement();
		
	writeToFile(date("Y-m-d H:i:s"). ": updateSaleStatus() EXIT \n", LOG_LEVEL_INFO);
        
	return $error;
	
}


/* update existing user data in the Global Database */
function updateUserDataInGlobalDatabase($user)
{
	
	//$user->printData();

	writeToFile(date("Y-m-d H:i:s"). ": updateUserDataInGlobalDatabase() ENTER \n", LOG_LEVEL_INFO);
	
	$statementString = "UPDATE systemuser SET role=?, status=?, productType=?, consecutiveFailLoginAttempts=? WHERE username=?";
	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $user->role),
			array(PDO::PARAM_STR, $user->status),
			array(PDO::PARAM_STR, $user->productType),
			array(PDO::PARAM_STR, $user->consecutiveFailLoginAttempts),
			array(PDO::PARAM_STR, $user->username)));
	$error = $pdoConnection->executeStatement();
	
     
	writeToFile(date("Y-m-d H:i:s"). ": updateUserDataInGlobalDatabase() ENTER \n", LOG_LEVEL_INFO);
        
	return $error;
}



?>