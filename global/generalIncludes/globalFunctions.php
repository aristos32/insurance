<?php
//has global functions called from many places

require_once($globalFilesLocation."/multilingual/interfaceProperties.php");

/* writes to the error file 
 * $message = the message to write
 * $logLevel = one of the values defined in definitions.php
 */
function writeToFile($message, $logLevel)
{
   //global $globalLogLevel;
	global $clientLogger;
	
	//for ajax calls this is not set
	if(isset($clientLogger))
		$clientLogger->writeToFile($message,$logLevel);
      
}


/* validate the username and password, from the GLOBAL database */
function authenticate($usernameFromInput, $passwordFromInput)
{
	//echo "authenticate: username=$username, password=$password";

	global $USER_STATUS_INVALID_USERNAME, $USER_STATUS_WRONG_PASSWORD, $USER_STATUS_SUSPENDED, $USER_STATUS_ACTIVE;


	$user = new user();

	//Step 1 - check if username exists

	//Step 2 - try to login

	$pdoConnection = new PDOConnector($_SESSION['dbhost'], $_SESSION['dbusername'], $_SESSION['dbpasswd'], $_SESSION['db_name']);
	$statementString = "SELECT count(*) as affectedRows, username, role, status, clientName, productType, consecutiveFailLoginAttempts FROM systemuser
			WHERE username like ? AND password like ?";
	$pdoConnection->setSQLStatement($statementString);
	$pdoConnection->setBindParams(array(array(PDO::PARAM_STR, $usernameFromInput),
			array(PDO::PARAM_STR, $passwordFromInput)));
	$pdoConnection->executeStatement();
	$results = $pdoConnection->fetch();

	 
	//echo "affected rows= $affectedRows \n";
	if($results['affectedRows'] == 1 )
	{
		$user->username = $results['username'];
		$user->role = $results['role'];
		$user->status = $results['status'];
		$user->clientName = $results['clientName'];
		$user->productType = $results['productType'];
		$user->consecutiveFailLoginAttempts = $results['consecutiveFailLoginAttempts'];


	}
	//find what is the error
	else
	{
		$result = checkUserNameExists($usernameFromInput);

		//echo "result=$result";

		//invalid login
		if($result==0)
		{
			$user->status = $USER_STATUS_INVALID_USERNAME;
			//$user->printData();
		}
		//wrong password
		else
		{
			//retrieve again systemuser based only on login
			$updatedUsers = retrieveUserInfoFromGlobalDatabase($usernameFromInput);
			$updatedUser = $updatedUsers[0];
				
			//$updatedUser->printData();
				
			//echo "consecutiveFailLoginAttempts=$updatedUser->consecutiveFailLoginAttempts";
				
			//check if account need to be locked
			if($updatedUser->consecutiveFailLoginAttempts == 3 )
			{
				$updatedUser->status = $USER_STATUS_SUSPENDED;
				$updatedUser->consecutiveFailLoginAttempts = '0';
			  
				updateUserDataInGlobalDatabase($updatedUser);
			}
			else
			{
				//to to increase only when user is still active
				//if it is suspended, do nothing
				if($updatedUser->status == $USER_STATUS_ACTIVE )
					updateConsecutiveFailLoginAttempts($usernameFromInput, 'consecutiveFailLoginAttempts = consecutiveFailLoginAttempts + 1' );
			}
				
			//update account info
			$user->status = $USER_STATUS_WRONG_PASSWORD;
		}
	}


	return $user;

}

/* set the correct language in the session, depending on the $_POST['lang'] and on the $_SESSION['lang']
$lang : $_POST['lang']. When action is changing, this is empty
$languageChanged = 1 Language is Changed
$languageChanged = 0 Language is not Changed */
function setLanguage($lang)
{
	//echo "language send=".$lang."<br/>";
	global $DEFAULT_LANGUAGE;
        
		
	$languageChanged = 0;//has language changed during this instance?

	//writeToFile(date("Y-m-d H:i:s"). ": setLanguage() ENTER \n", LOG_LEVEL_INFO);
        
	//Language Change 
	if(!empty($lang))
	{
		//echo "lang is not empty. Changing from ".$_SESSION['lang']. " to $lang <br/> ";
		
		//language changed
		if($lang!=@$_SESSION['lang'])
		{
			$languageChanged = 1;
			$_SESSION['lang'] = $lang;
		}
			
	}
	//Action change. $lang is empty, and we use the one in session
	else
	{
		//echo "lang is empty <br/>";
		//first load of quotation.php, lang is not in session. Set default language to English. 
		//also set $languageChanged = true; so that we read initialy 
		if(!isset($_SESSION['lang']) || (isset($_SESSION['lang'])&& $_SESSION['lang']==''))
		{
			//echo "setting lang";
			$_SESSION['lang'] = $DEFAULT_LANGUAGE;
			$languageChanged = 1;
		}
	}
	
	if($languageChanged == 1 )
	{
                //writeToFile(date("Y-m-d H:i:s"). ": changing lang to ".$_SESSION['lang']." \n", LOG_LEVEL_INFO);
                
		//echo "changing lang to ".$_SESSION['lang']."  <br>";
		readInterfaceProperties();
		
	}
	
	
	//read coverageDefinitions.xml - only load at the first call
	if(!isset($_SESSION['coveragesDefinitions']))
		$_SESSION['coveragesDefinitions'] = readCoveragesDefinitions($_SESSION['globalFilesLocation'].'/configurationFiles/coveragesDefinitions.xml');
	
	//read coverageDefinitions.xml - only load at the first call
	if(!isset($_SESSION['chargesDefinitions']))
		$_SESSION['chargesDefinitions'] = readChargesDefinitions($_SESSION['globalFilesLocation'].'/configurationFiles/chargesDefinitions.xml');
		
	//echo "languageChanged = $languageChanged <br/> ";

        //writeToFile(date("Y-m-d H:i:s"). ": setLanguage() EXIT \n", LOG_LEVEL_INFO);
}


/* send an automated email every time a new quote is inserted in the system.
$clientEmail = client email. can be empty. */
function sendAutomatedEmailForNewQuote($clientEmail)
{
	global $selectedProcessingInfo;
	global $YES_CONSTANT, $NO_CONSTANT, $SYSTEM_NOREPLY_EMAIL;
        
        
        writeToFile(date("Y-m-d H:i:s"). ": sendAutomatedEmailForNewQuote() ENTER. QuoteId= ".$_SESSION['quoteId']." \n", LOG_LEVEL_INFO);
        
	$username;
			
	if(isset($_SESSION['username']))
		$username = $_SESSION['username'];
	else
		$username = '';
		
	//get current user
	$user;
	$users = retrieveUsersInfo("%".$username."%");
					
	foreach($users as $currentUser)
	{
		/* many users can be returned, since we have LIKE in the SQL query.
		Select the user that matches exactly the username */
		if($username==$currentUser->username)
		{
                        writeToFile(date("Y-m-d H:i:s"). ": found user in database. Username= ".$username." \n", LOG_LEVEL_INFO);
                        
			$user = $currentUser;
			break;
		}
	}
	
	//echo $user->email;
	if(isset($user))
		$visitormail = $user->email;
	else
		$visitormail = "unknown@hotmail.com";
	
				
	$message = "New Quote Created In the system\n".
	"User Name =".$username."\n".
	"Quote Id =".$_SESSION['quoteId']."\n".
	"Entry Date =". date("F j, Y, g:i a")."\n".
	"Company =".$selectedProcessingInfo->insuranceCompanyOfferingQuote."\n".
	"Final Cost =".$selectedProcessingInfo->finalCost."\n".
	"Can Provide Online =".$selectedProcessingInfo->canProvideOnlineQuote."\n";
	
	//only add this when we cannot provide online quote
	if($selectedProcessingInfo->canProvideOnlineQuote==$NO_CONSTANT)
	{
		$message .= "\nReasons we cannot provide online Quote:\n";
		
		foreach($selectedProcessingInfo->reasonsWeCannotProvideOnlineQuote as $reasonsWeCannotProvideOnlineQuote)
		{
			$message .= $reasonsWeCannotProvideOnlineQuote."\n";
		}
	}
	
	$subject = 'New Quote with id:'.$_SESSION['quoteId'];
	$fromEmail = $SYSTEM_NOREPLY_EMAIL;			
													
	//send email to system administrator
	$headers   = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/plain; charset=iso-8859-1";
	$headers[] = "From: ".$_SESSION['SYSTEM_EMAIL'];
	sendEmail($subject, $message, $_SESSION['SYSTEM_EMAIL'], $_SESSION['SYSTEM_EMAIL'], $headers);
	
	//send email to person that makes the quote when is logged in
	if(isset($_SESSION['login']))
	{
		$headers = array();
		$headers   = array();
		$headers[] = "MIME-Version: 1.0";
		$headers[] = "Content-type: text/plain; charset=iso-8859-1";
		$headers[] = "From: ".$_SESSION['SYSTEM_EMAIL'];
		
		sendEmail($subject, $message, $visitormail, $visitormail, $headers);
	}
		
	//if client inserted an email in the quotationForm, send to this also
	if($clientEmail!="")
	{
		$headers = array();
		sendEmail($subject, $message, $clientEmail, $clientEmail, $headers);
	}

        writeToFile(date("Y-m-d H:i:s"). ": sendAutomatedEmailForNewQuote() EXIT \n", LOG_LEVEL_INFO);
}

/* Read all the values from the xml file TODO */
function readGeneralProperties($xmlFileName)
{
	global $YES_CONSTANT, $NO_CONSTANT;
	$generalProperties = new generalProperties();
	
	$doc = new DOMDocument();
	$doc->load( $xmlFileName );
	
	$properties = $doc->getElementsByTagName( "properties" )->item(0);
	
	$generalProperties->stateId = $properties->getElementsByTagName( "stateId" )->item(0)->nodeValue;
	$generalProperties->regNumber = $properties->getElementsByTagName( "regNumber" )->item(0)->nodeValue;
	
	return $generalProperties;
}




/* convert structure coveragesFromCompanyXML to coveragesInPolicyQuotation */
function convertXMLtoDatabase($coverageFromXML)
{
	$coveragesInPolicyQuotation = new coveragesInPolicyQuotation();
	
	$coveragesInPolicyQuotation->code = $coverageFromXML->code;
	$coveragesInPolicyQuotation->parameters[] = $coverageFromXML->param1;
	$coveragesInPolicyQuotation->parameters[] = $coverageFromXML->param2;
	$coveragesInPolicyQuotation->parameters[] = $coverageFromXML->param3;
	
	return $coveragesInPolicyQuotation;
}




/* Read all the coverages coming from quotationTable
related to the insuranceType. Since coverage are dynamically set in quotatationtable, we also read them 
dynamicall using their names from coverageDefinitions.xml
$insuranceType
$coverageType : read from $quoteInfo->coverageType. */
function realAllCoveragesSendFromForm($insuranceType, $coverageType)
{
        
        
	writeToFile(date("Y-m-d H:i:s"). ": realAllCoveragesSendFromForm() ENTER. insuranceType=$insuranceType \n", LOG_LEVEL_INFO);
        
	$coveragesInPolicyQuotationArray = array();
		
	//loop through all coverage definition from coverageDefinitions.xml
 	foreach($_SESSION['coveragesDefinitions'] as $eachCoverageDefinition)
 	{
	 	$multilingualValue = "";
		$param1 = "";
		$param2 = "";
		$param3 = "";
		$param4 = "";//to avoid error in the final loop iteration
		
		
	 	//$eachCoverageDefinition->printData();
	 	//echo $contract->insuranceType;
	 	//show only coverages for the same insurance type. i.e MOTOR, or MEDICAL
	 	if($eachCoverageDefinition->insuranceType==$insuranceType)
	 	{
 			//read only when it is checked
			if(isset($_POST[$eachCoverageDefinition->code])&& $_POST[$eachCoverageDefinition->code]=='on')
			{
				$localCoverage = new coveragesInPolicyQuotation();
				$localCoverage->code = $eachCoverageDefinition->code;
				$localCoverage->parameters[] = @$_POST[$eachCoverageDefinition->code.'Param1'];
				$localCoverage->parameters[] = @$_POST[$eachCoverageDefinition->code.'Param2'];
				$localCoverage->parameters[] = @$_POST[$eachCoverageDefinition->code.'Param3'];
				$localCoverage->categoryType = $coverageType;
				$coveragesInPolicyQuotationArray[] = $localCoverage;
				//$localCoverage->printData();
			}
	 	}
 	}

	writeToFile(date("Y-m-d H:i:s"). ": realAllCoveragesSendFromForm() EXIT \n", LOG_LEVEL_INFO);
        
	return $coveragesInPolicyQuotationArray;
}


/* Read all the values from the xml file.
$xmlFileName = configuration/productTypes.xml  */
function readProductTypes($xmlFileName)
{
	global $YES_CONSTANT, $NO_CONSTANT;
        
        
        $productTypesArray = array();
	
	$doc = new DOMDocument();
	$doc->load( $xmlFileName );
	
	$productTypes = $doc->getElementsByTagName( "productType" );
	
	foreach($productTypes as $eachProductType)
	{
		$productTypesArray[] = $eachProductType->nodeValue;
	}

	return $productTypesArray;
}

/* Read all the values from the xml file.
$xmlFileName = configuration/accounts.xml  */
function readAccounts($xmlFileName)
{
	global $YES_CONSTANT, $NO_CONSTANT;
        
                
	$accountsArray = array();
	
	$doc = new DOMDocument();
	$doc->load( $xmlFileName );
	
	$accounts = $doc->getElementsByTagName( "account" );
	
	foreach($accounts as $eachAccount)
	{
		$account = new account();
		$account->clientName = $eachAccount->getElementsByTagName( "clientName" )->item(0)->nodeValue;
		//removed databasename from systemuser $account->database = $eachAccount->getElementsByTagName( "database" )->item(0)->nodeValue;
		
		$accountsArray[] = $account;
	}

	return $accountsArray;
}

/* Read all the values from the xml file.
$xmlFileName = configurationFiles/coveragesDefinitions.xml  */
function readDatabasePatches($xmlFileName)
{
	global $YES_CONSTANT, $NO_CONSTANT;
        
        
        writeToFile(date("Y-m-d H:i:s"). ": readDatabasePatches() ENTER \n", LOG_LEVEL_INFO);
        
	$databasesArray = array();
	
	$doc = new DOMDocument();
	$doc->load( $xmlFileName );
	
	$databases = $doc->getElementsByTagName( "database" );
	
	foreach($databases as $eachDatabase)
	{
		$database = new database();
		$database->type = $eachDatabase->getElementsByTagName( "type" )->item(0)->nodeValue;
		$database->client = $eachDatabase->getElementsByTagName( "client" )->item(0)->nodeValue;
		$commands = $eachDatabase->getElementsByTagName( "command" );
		foreach($commands as $eachCommand)
			$database->commands[] = $eachCommand->nodeValue;
		
		$databasesArray[] = $database;
	}
	
	
	writeToFile(date("Y-m-d H:i:s"). ": readDatabasePatches() EXIT \n", LOG_LEVEL_INFO);
        
	return $databasesArray;
}



/* Read all the values from the xml file.
$xmlFileName = configurationFiles/config.xml  */
function readClientProperties($xmlFileName)
{
	global $YES_CONSTANT, $NO_CONSTANT;
        
        
        //writeToFile(date("Y-m-d H:i:s"). ": readClientProperties() ENTER \n", LOG_LEVEL_INFO);
        
	$doc = new DOMDocument();
	$doc->load( $xmlFileName );
	
	$parametersArray = array();
	
	$parameters = $doc->getElementsByTagName( "parameter" );
	foreach($parameters as $eachParameter)
	{
		$parameterNameValue = new parameterNameValue();
		$parameterNameValue->name = $eachParameter->getElementsByTagName( "name" )->item(0)->nodeValue;
		$parameterNameValue->value = $eachParameter->getElementsByTagName( "value" )->item(0)->nodeValue;
		$_SESSION[$parameterNameValue->name] = $parameterNameValue->value;
		
		$parametersArray[] = $parameterNameValue;
		//$parameterNameValue->printData();
		
		//echo $_SESSION[$parameterNameValue->name];
		
	}

	//writeToFile(date("Y-m-d H:i:s"). ": readClientProperties() EXIT \n", LOG_LEVEL_INFO);
        
	return $parametersArray;
		
}

/* Read all the values from the xml file.
$xmlFileName = configurationFiles/chargesDefinitions.xml  */
function readChargesDefinitions($xmlFileName)
{
	global $YES_CONSTANT, $NO_CONSTANT;
        
        
        //writeToFile(date("Y-m-d H:i:s"). ": readChargesDefinitions() ENTER \n", LOG_LEVEL_INFO);
        
	$chargesInQuotationArray = array();
	
	$doc = new DOMDocument();
	$doc->load( $xmlFileName );
	
	$charges = $doc->getElementsByTagName( "charge" );
	
	foreach($charges as $eachCharge)
	{
		$chargesInQuotation = new chargesInQuotation();
		$chargesInQuotation->code = $eachCharge->getElementsByTagName( "code" )->item(0)->nodeValue;
		$chargesInQuotation->insuranceType = $eachCharge->getElementsByTagName( "insuranceType" )->item(0)->nodeValue;
		$chargesInQuotation->description = $eachCharge->getElementsByTagName( "description" )->item(0)->nodeValue;
		$chargesInQuotation->default = $eachCharge->getElementsByTagName( "default" )->item(0)->nodeValue;
		
		//echo $coveragesInPolicyQuotation->code;
				
		//read parameters if any
		$parameters = $eachCharge->getElementsByTagName( "parameter" );
		foreach($parameters as $eachParameter)
		{
			$parameterNameValue = new parameterNameValue();
			$parameterNameValue->displayType = $eachParameter->getAttribute( "displayType" );
			$parameterNameValue->name = $eachParameter->getElementsByTagName( "name" )->item(0)->nodeValue;
			$parameterNameValue->value = $eachParameter->getElementsByTagName( "value" )->item(0)->nodeValue;
			$chargesInQuotation->parameters[] = $parameterNameValue;
		}
		
		//$chargesInQuotation->printData();
		
		$chargesInQuotationArray[] = $chargesInQuotation;
	}

       // writeToFile(date("Y-m-d H:i:s"). ": readChargesDefinitions() EXIT \n", LOG_LEVEL_INFO);
		
	return $chargesInQuotationArray;
}

/* Read all the values from the xml file.
$xmlFileName = configurationFiles/coveragesDefinitions.xml  */
function readCoveragesDefinitions($xmlFileName)
{
	global $YES_CONSTANT, $NO_CONSTANT;
        
        
        //writeToFile(date("Y-m-d H:i:s"). ": readCoveragesDefinitions() ENTER \n", LOG_LEVEL_INFO);
		
		
	$coveragesInPolicyQuotationArray = array();
	
	$doc = new DOMDocument();
	$doc->load( $xmlFileName );
	
	$coverages = $doc->getElementsByTagName( "coverage" );
	
	foreach($coverages as $eachCoverage)
	{
		$coveragesInPolicyQuotation = new coveragesInPolicyQuotation();
		$coveragesInPolicyQuotation->code = $eachCoverage->getElementsByTagName( "code" )->item(0)->nodeValue;
		$coveragesInPolicyQuotation->insuranceType = $eachCoverage->getElementsByTagName( "insuranceType" )->item(0)->nodeValue;
		$coveragesInPolicyQuotation->description = $eachCoverage->getElementsByTagName( "description" )->item(0)->nodeValue;
		$coveragesInPolicyQuotation->default = $eachCoverage->getElementsByTagName( "default" )->item(0)->nodeValue;
		
		//echo $coveragesInPolicyQuotation->code;
				
		//read parameters if any
		$parameters = $eachCoverage->getElementsByTagName( "parameter" );
		foreach($parameters as $eachParameter)
		{
			$parameterNameValue = new parameterNameValue();
			$parameterNameValue->displayType = $eachParameter->getAttribute( "displayType" );
			$parameterNameValue->name = $eachParameter->getElementsByTagName( "name" )->item(0)->nodeValue;
			$parameterNameValue->value = $eachParameter->getElementsByTagName( "value" )->item(0)->nodeValue;
			$coveragesInPolicyQuotation->parameters[] = $parameterNameValue;
		}
		
		//read selection levels default values if any
		$selectionLevels = $eachCoverage->getElementsByTagName( "selectionLevel" );
		foreach($selectionLevels as $eachSelectionLevel)
		{
			$coveragesInPolicyQuotation->selectionLevels[] = $eachSelectionLevel->nodeValue;
			//echo $eachSelectionLevel->nodeValue, PHP_EOL;
		}
		
		$coveragesInPolicyQuotationArray[] = $coveragesInPolicyQuotation;
	}

        //writeToFile(date("Y-m-d H:i:s"). ": readCoveragesDefinitions() EXIT \n", LOG_LEVEL_INFO);
		
	return $coveragesInPolicyQuotationArray;
}

/* create new client/configuration/config.xml file
	$filePath = path to delete/create XML files
	$previousFileName = name of the previous file(if different)
	$newFileName = name of the new file(if different)
	$parametersArray = array containing new parameter values
*/
function createUpdatedConfigXml($filePath, $previousFileName, $newFileName, $parametersArray)
{
        
        
        writeToFile(date("Y-m-d H:i:s"). ": createUpdatedConfigXml() ENTER \n", LOG_LEVEL_INFO);
		
	//delete old file
	//echo $filePath.$previousFileName;
	unlink($filePath.$previousFileName);
	
	// Creates a new document according to 1.0 specs
	$dom = new DOMDocument("1.0");
	$dom->formatOutput = true;
	$dom->preserveWhiteSpace = false;
	
	//PROPERTIES
	$properties = $dom->createElement("properties");//root element
	$dom->appendChild($properties);
	
	//insert all new parameters
	foreach($parametersArray as $eachParameter)
	{
		//$eachParameter->printData();
		//<parameter>
		$parameter = $dom->createElement("parameter");
		$properties->appendChild($parameter);	
		
		//<name>
		$parameterName = $dom->createElement("name");
		$parameter->appendChild($parameterName);	
		$text = $dom->createTextNode($eachParameter->name);
		$parameterName->appendChild($text);	
	
		//<value>
		$parameterValue = $dom->createElement("value");
		$parameter->appendChild($parameterValue);	
		$text = $dom->createTextNode($eachParameter->value);
		$parameterValue->appendChild($text);	
		
	}
	
	// save xml tree to file
	$dom->save($filePath.$newFileName);

	writeToFile(date("Y-m-d H:i:s"). ": createUpdatedConfigXml() EXIT \n", LOG_LEVEL_INFO);
}

/* create new company file
	$filePath = path to delete/create XML files
	$previousFileName = name of the previous file(if different)
	$newFileName = name of the new file(if different)
	$generalProperties = structure containing new values
*/
function createUpdatedGeneralPropertiesXml($filePath, $previousFileName, $newFileName, $generalProperties)
{
	//delete old file
	@unlink($filePath.$previousFileName);
	
	// Creates a new document according to 1.0 specs
	$dom = new DOMDocument("1.0");
	$dom->formatOutput = true;
	$dom->preserveWhiteSpace = false;
	
	//PROPERTIES
	$properties = $dom->createElement("properties");//root element
	$dom->appendChild($properties);
	
	//STATE ID
	$stateId = $dom->createElement("stateId");
	$properties->appendChild($stateId);	
	$text = $dom->createTextNode($generalProperties->stateId);
	$stateId->appendChild($text);	
	
	//REG NUMBER
	$regNumber = $dom->createElement("regNumber");
	$properties->appendChild($regNumber);	
	$text = $dom->createTextNode($generalProperties->regNumber);
	$regNumber->appendChild($text);	
	
	// save xml tree to file
	$dom->save($filePath.$newFileName);
	
}


/*set the action and the session['action'] */
function setAction()
{
	global $action, $actionRead, $showEchoes;
	
	if(isset($_POST['action']))
		$action = $_POST['action']; 
	
	if($showEchoes==true)
	{
		echo "ACTION read from POST : $action <br>";
		echo "ACTION in session : " . @$_SESSION['action'] . " <br>";
	}
		
	if($action=='')
	{
		if(isset($_GET['action']))
			$action=$_GET['action'];
		if($showEchoes==true)
			echo "ACTION read from GET : $action <br>";
	}
	
	$actionRead = $action; 
	
	//update action when is send. 
	if(isset($action) && $action!='')
	{
		$_SESSION['action'] = $action;
	}
	//when language is changing, $action is empty, and we use the action in session if any
	else
	{
		if($showEchoes==true)
				echo "Changing Language - user action in session <BR>";
		//first call, action is not set in session
		if(!isset($_SESSION['action']))
		{
			if($showEchoes==true)
				echo "SETTING ACTION TO EMPTY <BR>";
			$_SESSION['action'] = '';
		}
		else
		{
			$action = $_SESSION['action'];
		}
	}
		
	if($showEchoes==true)
		echo "ACTION Taken :  " . $action . " <br>";
	
}

?>