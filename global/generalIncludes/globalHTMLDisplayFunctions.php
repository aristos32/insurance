<?php
//has global functions called from many places for display of HTML pages

/* display all proposers in HTML table.
$proposers: array of all proposers
$allEmails: all the existing emails in a (email1,email2,email3...) list */
function displayAllProposers($proposers, $allEmails, $allStateIds)
{
	global $USER_ROLE_ADMINISTRATOR;
	
	/* CASE 1 : MORE THAN 0 CLIENTS FOUND */
	if(count($proposers) > 0 )
	
	{
	?>
	
	
		<table width="90%" border="1" cellspacing="1" cellpadding="1">
		<tr>
			<td><b><?php echo $_SESSION['proposerfullName']; ?></td>
			<td><b><?php echo $_SESSION['stateIdTab']; ?></td>
			<td><b><?php echo $_SESSION['correspondenceAddress']; ?></td>
			<td><b>Email</td>
			<td><b><?php echo $_SESSION['telephoneTab']; ?></td>
			<?php
			if( $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR )
			{
				?>
				<td><b><?php echo $_SESSION['delete']; ?></td>
				<?php
			}
			?>
		</tr>
		<?php
		foreach($proposers as $proposer)
		{
			
		?>					
		<tr>
			<td>
			<?php 
				$updateStateId = "update_".$proposer->owner->stateId; ?> 
				<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
				<form action="./office.php" id="<?php echo $updateStateId;?>" method="POST" style="display: none;">
					<input type="text" name="action" value="updateClientDataForm" />
					<input type="text" name="stateId" value="<?php echo $proposer->owner->stateId;?>" />
				</form>
				<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $updateStateId;?>').submit()"><?php echo $proposer->owner->firstName." ".$proposer->owner->lastName; ?></a>
			</td>
				
			<td ><?php echo $proposer->owner->stateId;?></td>
			<!-- TODO . now i just display the first address found. It is not necessarilly -->
			<td ><?php if(count($proposer->addresses)>0) echo $proposer->addresses[0]->street.",". $proposer->addresses[0]->areaCode." ".$proposer->addresses[0]->city.",".$proposer->addresses[0]->country;?></td>
			<td ><?php echo $proposer->owner->email;?></td>
			<td ><?php echo $proposer->owner->telephone.", ".$proposer->owner->cellphone;?></td>
			
			<?php
			//1. admin users can only be deleted manually
			//2. admin users can delete all other 'LOWER' rank users
			//3. customers/employee users cannot delete themselves
			if( $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR )
			{
				$deleteStateId = "delete_".$proposer->owner->stateId;?>
				<td>
				<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
				<form action="./office.php" id="<?php echo $deleteStateId;?>" method="POST" style="display: none;">
					<input type="text" name="action" value="deleteClient" />
					<input type="text" name="clientId" value="<?php echo $proposer->owner->stateId;?>" />
				</form>
				<a href="javascript:;" onclick="javascript: if (confirm('<?php echo $_SESSION['areYouSureYouWantToDeleteThisUser'];?>')) document.getElementById('<?php echo $deleteStateId;?>').submit()"><?php echo $_SESSION['delete']; ?></a>
				</td>
				<?php
			}
			?>
		
		</tr>
		<?php
		}
		?>
		<tr>
		<td> <a href="javascript:;" onclick="send_email('<?php echo $allEmails; ?>', '<?php echo $_SESSION['globalFilesLocation']; ?>', '<?php echo $_SESSION['SYSTEM_EMAIL']; ?>', '<?php echo $_SESSION['lang']; ?>', '<?php echo $_SESSION['clientName']; ?>', '<?php echo $allStateIds; ?>' )"><?php echo $_SESSION['sendEmailToAll']; ?></a>
		</td>
		</tr>
		</table>
		<?php
	}
	/* CASE 2 : NO CLIENTS FOUND */
	else
	{
		?>
		No Clients Found.  
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="findLeadForm2" method="POST" style="display: none;">
			<input type="text" name="action" value="findClientForm" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('findLeadForm2').submit()">Continue</a>
	
		<?php
	}

	
}

/* Display the address in all files except at the updateContractData.php */
function displayAddresses($addresses)
{
	$i = 0;
	?>
	<table id="userAddresses">
	<?php
	//show all addresses in database
	foreach( $addresses as $eachAddress )
	{
		//$eachAddress->printData();
		
		?>
		<tr>
		<td class="co1Per"><input type="checkbox" name="chk[]" checked><input type="hidden" name="addressId[]" value="<?php echo $eachAddress->addressId; ?>" /></td>
		<td>
			<select name="addressType[]" id="<?php echo 'addressType'.$i; ?>" >
				<?php
					foreach($_SESSION['addressOptions'] as $eachType)
					{?>
					<OPTION value="<?php echo $eachType->value;?>"><?php echo $eachType->name;?></OPTION> 
					<?php	
					}
					?>
			</select>	
			<script type="text/javascript">
				setPreviousValue("<?php echo $eachAddress->addressType; ?>", "<?php echo 'addressType'.$i; ?>");
			</script>
		</td>
		<!-- ATTENTION - all elements must be in the same row, no spaces. for correct parsing in validateAddresses -->
		<td class="input"><input type="text" name="street[]" size="10" value="<?php echo $eachAddress->street;?>" /><?php echo " ".$_SESSION['code'].":"; ?><input type="text" name="code[]" size="10" value="<?php echo $eachAddress->areaCode;?>" />&nbsp;<?php echo $_SESSION['city']; ?>:<input type="text" name="city[]" size="10" value="<?php echo $eachAddress->city;?>" />
		<?php echo $_SESSION['country'];?>:
		<select name="country[]" id="<?php echo 'correspondenceAddressCountry'.$i; ?>" style="width:6.5em;">
			<?php
				foreach($_SESSION['countriesOptions'] as $eachCountry)
				{?>
				<option value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></option> 
				<?php	
				}
				?>
		</select>	
		<script type="text/javascript">
			setPreviousValue("<?php echo $eachAddress->country; ?>", "<?php echo 'correspondenceAddressCountry'.$i; ?>");
		</script>
		</td>
		</tr>
		<?php
		$i = $i + 1;
		
	}
	
	?>
	</table>
	<table>
	<tr>
		<td>&nbsp;</td>
		<td>
		<INPUT type="button" value="<?php echo $_SESSION['addAddress'];?>" onclick="addRowInUserAddressTable('userAddresses')" />
	 	<INPUT type="button" value="<?php echo $_SESSION['deleteAddress'];?>" onclick="deleteRowFromTable('userAddresses')" />
	 	</td>
	 </tr>
	 </table>
	<?php
}

/* display all types of addresses. If empty, leave the html tags empty. This is only called in the updateContractDate.php.
possible address types are $ADDRESS_TYPE_CORRESPONDENCE, $ADDRESS_TYPE_RESIDENCE, $ADDRESS_TYPE_BUSINESS */
function displayAllAddresses($addresses)
{
	?>
	<tr>
		<td class="col10Per" colspan=2><b><?php echo $_SESSION['typeTab']; ?></b></td>
		<td class="col10Per" colspan=2><b><?php echo $_SESSION['address']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['city'].",".$_SESSION['code']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['country']; ?></b></td>
		<td class="col10Per" colspan=2>
		<a href="./office.php?action=updateClientDataForm"><?php echo $_SESSION['modifyAddresses']; ?></a>
		</td>
	</tr>
	<?php
	foreach( $addresses as $eachAddress )
	{
		?>
			<tr>
			<?php
			foreach($_SESSION['addressOptions'] as $eachAddressOption )
			{
				if($eachAddressOption->value==$eachAddress->addressType)
				{
					?>
					<td class="col10Per" colspan=2><?php echo $eachAddressOption->name; ?></td>
					<?php
				}
			}
			?>
			<td class="col10Per" colspan=2><?php echo $eachAddress->street; ?></td>
			<td class="col10Per"><?php echo $eachAddress->city.",".$eachAddress->areaCode;?></td>
			<td class="col10Per"><?php echo $eachAddress->country; ?></td>
			</tr>
			
		<?php
	} 
		
}

/* display the existing coverages in quote as exist in coveragesinquotation  table
Display NOT-CHANGEBLE values
$coverages : all coverages in database for this quotation
$decision : what was decided for coverage? YES=Added, CANNOT_ADD=Not added
 */
function displayExistingCoveragesInQuotation($coverages, $decision)
{
	global $EURO;
	
        writeToFile(date("Y-m-d H:i:s"). ": displayExistingCoveragesInPolicyQuotation() ENTER. decision=$decision \n", LOG_LEVEL_INFO);
	
        $j = 0;
	
	//all coverages for this quotation/policy retrieved from the database
	foreach($coverages as $eachExistingCoverage)
	{
		//echo $eachExistingCoverage->printData();
		//for policy coverages, decision is empty. Means that it is added, since it exists in the database
		if($eachExistingCoverage->decision==$decision || $eachExistingCoverage->decision=="")
		{
			$multilingualValue = "";
			$param1 = "";
			$param2 = "";
			$param3 = "";
			$param4 = "";//to avoid error in final loop iteration
			
			//find the multilingual display corresponding to this code
			foreach($_SESSION['coveragesInPolicyQuotationOptions'] as $eachCoverage)
			{
				
				
		 		if($eachExistingCoverage->code==$eachCoverage->value)
		 		{
				 	$param1 = $eachExistingCoverage->parameters[0];
 					$param2 = $eachExistingCoverage->parameters[1];
 					$param3 = $eachExistingCoverage->parameters[2];
 					
 					//split in case we have parameters in between
			 		$multilingualValueArray = explode('|',$eachCoverage->name);
			 		
			 		$i=1;//to get the correct parameter
			 		foreach($multilingualValueArray as $eachPiece)
			 		{
				 		$multilingualValue = $multilingualValue.$eachPiece.${"param".$i};
				 		$i++;
			 		}
			 		//display simple text - case of quotation presentation
			 		
			 		//we reached 4 elements, need to create new row
			 		//use modulo(%)
			 		if($j % 4 === 0 )
				 	{	//very first element - create first row
					 	if($j==0)
					 	{?>
				 			<tr>
				 		<?php
			 			}
			 			//new row - close previous one
			 			else
			 			{?> </tr>
				 			<tr>
				 		<?php
			 			}
			 			
			 		} ?>
				 
				 		<td class="info">
				 		<?php echo $multilingualValue."(".$EURO.$eachExistingCoverage->charge.")"; ?>
				 		</td>
				 	<?php
				 	
				 	$j++;
				 	
				 	break;
		 		}
			}
		}
	}
	
        writeToFile(date("Y-m-d H:i:s"). ": displayExistingCoveragesInPolicyQuotation() EXIT.\n" , LOG_LEVEL_INFO);
        	
}


/* display the existing coverages in the policy  exist in coveragesinpolicy tables
Display NOT-CHANGEBLE values
$coverages : all coverages in database for this policy 
$decision : what was decided for coverage? YES=Added, CANNOT_ADD=Not added
$htmlDisplayType : how to show the coverage. Can be 'CHECKBOX' or 'TEXT' */
function displayExistingCoveragesInPolicy($coverages, $decision)
{
	global $COVERAGE_DISPLAY_CHECKBOX, $COVERAGE_DISPLAY_TEXT;
        

	writeToFile(date("Y-m-d H:i:s"). ": displayExistingCoveragesInPolicy() ENTER. decision=$decision \n", LOG_LEVEL_INFO);
        
	//all coverages for this quotation/policy retrieved from the database
	foreach($coverages as $eachExistingCoverage)
	{
		//echo $eachExistingCoverage->printData();
		//for policy coverages, decision is empty. Means that it is added, since it exists in the database
		if($eachExistingCoverage->decision==$decision || $eachExistingCoverage->decision=="")
		{
			$multilingualValue = "";
			$param1 = "";
			$param2 = "";
			$param3 = "";
			$param4 = "";//to avoid error in final loop iteration
			
			//find the multilingual display corresponding to this code
			foreach($_SESSION['coveragesInPolicyQuotationOptions'] as $eachCoverage)
			{
				
				
		 		if($eachExistingCoverage->code==$eachCoverage->value)
		 		{
		 			//foreach($basicCoverageInfo->vehicleTypes[0]->optionalCoveragesFromXMLArray as $eachCoverageFromXML )
				 	//{
					 	//if($eachExistingCoverage->code==$eachCoverageFromXML->code)
		 				//{
						 	$param1 = $eachExistingCoverage->parameters[0];
		 					$param2 = $eachExistingCoverage->parameters[1];
		 					$param3 = $eachExistingCoverage->parameters[2];
		 					
		 					//split in case we have parameters in between
					 		$multilingualValueArray = explode('|',$eachCoverage->name);
					 		
					 		$i=1;//to get the correct parameter
					 		foreach($multilingualValueArray as $eachPiece)
					 		{
						 		$multilingualValue = $multilingualValue.$eachPiece.${"param".$i};
						 		$i++;
					 		}
					 		
						 	?>
						 	<tr>
						 		<td>
						 		<input type="checkbox" onclick="javascript:updateCoverageAjax('<?php echo $eachCoverage->value;?>');" id="<?php echo $eachCoverage->value; ?>" value="<?php echo $eachCoverage->value; ?>" checked><?php echo $multilingualValue; ?><br>
						 		</td>
						 	</tr>
						 	<?php
						 	break;
					 	//}
					 	
				 	//}
		 		}
			}
		}
	}

        writeToFile(date("Y-m-d H:i:s"). ": displayExistingCoveragesInPolicy() EXIT \n", LOG_LEVEL_INFO);
	
}


/* display the discounts in the quotation as exist in QUOTATION_DISCOUNTS table. */
function displayDiscountsInQuotation($discounts)
{
	global $EURO;

	writeToFile(date("Y-m-d H:i:s"). ": displayDiscountsInQuotation() ENTER \n", LOG_LEVEL_INFO);
        
	$j = 0;
	
	//all coverages for this quotation/policy retrieved from the database
	foreach($discounts as $eachDiscountInQuotation)
	{
		$multilingualValue = "";
		$param1 = "";
		$param2 = "";
		$param3 = "";
		$param4 = "";//to avoid error in final loop iteration
		
		//find the multilingual display corresponding to this code
		foreach($_SESSION['discountsInQuotationOptions'] as $eachDiscount)
		{
			
			
	 		if($eachDiscountInQuotation->code==$eachDiscount->value)
	 		{
			 	$param1 = $eachDiscountInQuotation->parameters[0];
				$param2 = $eachDiscountInQuotation->parameters[1];
				$param3 = $eachDiscountInQuotation->parameters[2];
					
					//split in case we have parameters in between
		 		$multilingualValueArray = explode('|',$eachDiscount->name);
		 		
		 		$i=1;//to get the correct parameter
		 		foreach($multilingualValueArray as $eachPiece)
		 		{
			 		$multilingualValue = $multilingualValue.$eachPiece.${"param".$i};
			 		$i++;
		 		}
		 		//display simple text - case of quotation presentation
		 		
		 		//we reached 4 elements, need to create new row
		 		//use modulo(%)
		 		if($j % 4 === 0 )
			 	{	//very first element - create first row
				 	if($j==0)
				 	{?>
			 			<tr>
			 		<?php
		 			}
		 			//new row - close previous one
		 			else
		 			{?> </tr>
			 			<tr>
			 		<?php
		 			}
		 			
		 		} ?>
			 
			 		<td class="info">
			 		<?php echo $multilingualValue."(".$EURO.$eachDiscountInQuotation->charge.")"; ?>
			 		</td>
			 	<?php
			 	
			 	$j++;
			 	
			 	break;
	 		}
		}
	}

	writeToFile(date("Y-m-d H:i:s"). ": displayDiscountsInQuotation() EXIT \n", LOG_LEVEL_INFO);
}


/* display the additional charges in the policy or quote as exist in CHARGESINQUOTATION tables
$additionalCharges : all charges in database for this policy or quotation */
function displayAdditionalChargesInPolicyQuotation($additionalCharges)
{
	global $EURO;
        
        
	writeToFile(date("Y-m-d H:i:s"). ": displayAdditionalChargesInPolicyQuotation() ENTER \n", LOG_LEVEL_INFO);
        
	$j = 0;
	
	//all coverages for this quotation/policy retrieved from the database
	foreach($additionalCharges as $eachChargeInQuotation)
	{
		$multilingualValue = "";
		$param1 = "";
		$param2 = "";
		$param3 = "";
		$param4 = "";//to avoid error in final loop iteration
		
		//find the multilingual display corresponding to this code
		foreach($_SESSION['chargesInPolicyQuotationOptions'] as $eachChargeMultilingual)
		{
	 		if($eachChargeInQuotation->code==$eachChargeMultilingual->value)
	 		{
		 		//we reached 4 elements, need to create new row
		 		//use modulo(%)
		 		if($j % 4 === 0 )
			 	{	//very first element - create first row
				 	if($j==0)
				 	{?>
			 			<tr>
			 		<?php
		 			}
		 			//new row - close previous one
		 			else
		 			{?> </tr>
			 			<tr>
			 		<?php
		 			}
		 			
		 		} ?>
			 	<td class="info">
			 	<?php echo $eachChargeMultilingual->name."(".$EURO.$eachChargeInQuotation->charge.")"; ?>
			 	</td>
			 	<?php
			 	$j++;
			 	break;
	 		}
		}
	}

        writeToFile(date("Y-m-d H:i:s"). ": displayAdditionalChargesInPolicyQuotation() EXIT \n", LOG_LEVEL_INFO);
	
}

/* Display all the coverages related to the insuranceType as taken from coverageDefinitions.xml.
Called from quotation.php */
function displayAllCoveragesRelatedToInsuranceType($insuranceType)
{
    
	writeToFile(date("Y-m-d H:i:s"). ": displayAllCoveragesRelatedToInsuranceType() ENTER. insuranceType=$insuranceType \n", LOG_LEVEL_INFO);
        
	//loop through all coverage definition from coverageDefinitions.xml
 	foreach($_SESSION['coveragesDefinitions'] as $eachCoverageDefinition)
 	{
	 	$multilingualValue = "";
			
	 	//$eachCoverageDefinition->printData();
	 	//show only coverages for the same insurance type. i.e MOTOR, or MEDICAL
	 	if($eachCoverageDefinition->insuranceType==$insuranceType)
	 	{
 			//Display all other related coverages for adding - retrieved from multilingual files
 			foreach($_SESSION['coveragesInPolicyQuotationOptions'] as $eachCoverage)
		 	{
			 	//echo "$eachCoverage->value <br>";
			 	
			 	//match coverageDefinitions.xml with multilingual codes
			 	if($eachCoverageDefinition->code==$eachCoverage->value)
			 	{
				 	$multilingualValue = $eachCoverage->name;
		 			//parameters exist
	 				if(count($eachCoverageDefinition->parameters) > 0 )
	 				{
		 				$multilingualValue = "";
		 				
		 				$i=1;
		 				foreach($eachCoverageDefinition->parameters as $eachParameter)
		 				{	
			 				${"param".$i} = $eachParameter->value;
			 				$i++;
		 				}	
	 		
					 	//split in case we have parameters in between
				 		$multilingualValueArray = explode('|',$eachCoverage->name);
				 		
				 		$i=1;//to get the correct parameter
				 		foreach($multilingualValueArray as $eachPiece)
				 		{
					 		if($i == count($multilingualValueArray) )
					 			$multilingualValue = $multilingualValue.$eachPiece;
					 		else
					 			$multilingualValue = $multilingualValue.$eachPiece.${"param".$i};
					 		$i++;
					 		
				 		}
			 		
		 			}
			 		//echo $multilingualValue;
			 		
			 		//no selection levels - show check box.
			 		//send also the parameters, if any
			 		if($eachCoverageDefinition->selectionLevels == null )
			 		{
					 	?>
					 	<tr>
					 		<td>
					 		<input type="checkbox" name="<?php echo $eachCoverage->value; ?>" id="<?php echo $eachCoverage->value; ?>" <?php echo $eachCoverageDefinition->default; ?> >
					 		<?php echo $multilingualValue; ?>
					 		<?php
					 		if(count($eachCoverageDefinition->parameters) > 0 )
			 				{
				 				/*$i=1;
				 				
				 				foreach($eachCoverageDefinition->parameters as $eachParameter)
				 				{
					 				echo "setting parameter $i = ".${"param".$i}."<br>";
					 				$i++;
				 				} */
				 				
				 				
				 				$i = 1;
				 				foreach($eachCoverageDefinition->parameters as $eachParameter)
				 				{	?>
					 				<input type="hidden" name="<?php echo $eachCoverage->value.'Param'.$i; ?>" id="<?php echo $eachCoverage->value.'Param'.$i; ?>" value="<?php echo ${"param".$i};?>" >
					 				<?php
					 				$i = $i + 1;
					 			
				 				}
				 			}?>
					 		</td>
					 	</tr>
					 	<?php
				 	}
				 	//<selectionlevels> exist - display select button to select from all selection levels
				 	else
				 	{
					 	//echo "inside selection levels";
					 	?>
					 	<tr>
					 		<td>
					 		<input type="checkbox" name="<?php echo $eachCoverage->value; ?>" id="<?php echo $eachCoverage->value; ?>" <?php echo $eachCoverageDefinition->default; ?> >
					 		<?php echo $multilingualValue; ?> 
					 		<select name="<?php echo $eachCoverage->value.'Param1'; ?>" id="<?php echo $eachCoverage->value.'Param1'; ?>">
					 		<?php
					 		foreach($eachCoverageDefinition->selectionLevels as $eachSelectionLevel)
					 		{
					 			?>
					 			<option value="<?php echo $eachSelectionLevel;?>"><?php echo $eachSelectionLevel;?></option>
					 			<?php
				 			}?>
							</select> 
					 		</td>
					 	</tr>
					 	
					 	<?php
				 	}
				 	break;
		 		}
		 	}
	 	}
 	}
 
        writeToFile(date("Y-m-d H:i:s"). ": displayAllCoveragesRelatedToInsuranceType() EXIT \n", LOG_LEVEL_INFO);
}

/* 
Display the coverages section of the contract
1. display existing coverages
2. display coverages that can be added
*/
function displayCoverages($contract)
{
	global $YES_CONSTANT, $NO_CONSTANT, $COVERAGE_DISPLAY_CHECKBOX;
		
	//Display all coverages already in policy - retrieved from table
 	$existingCoveragesInPolicyArray = retrieveAllCoverages("coveragesinpolicy", "saleId", $contract->sale->saleId);
 	
	$multilingualValue = "";
 	//Step 1 - Display existing coverages first
 	displayExistingCoveragesInPolicy($existingCoveragesInPolicyArray, $YES_CONSTANT);
 	
 	//Step 2 - Display remaining coverages
	//loop through all coverage definition from coverageDefinitions.xml
 	foreach($_SESSION['coveragesDefinitions'] as $eachCoverageDefinition)
 	{
	 	//$eachCoverageDefinition->printData();
	 	//echo $contract->insuranceType;
	 	//show only coverages for the same insurance type. i.e MOTOR, or MEDICAL
	 	if($eachCoverageDefinition->insuranceType==$contract->sale->insuranceType)
	 	{
 			//Display all other related coverages for adding - retrieved from multilingual files
 			foreach($_SESSION['coveragesInPolicyQuotationOptions'] as $eachCoverage)
		 	{
			 	//match coverageDefinitions.xml with multilingual codes
			 	if($eachCoverageDefinition->code==$eachCoverage->value)
			 	{
				 	//display the checkbox only if it doesn't already exist in the coverages above
				 	$displayCheckBox = 'true';
				 	
				 	foreach($existingCoveragesInPolicyArray as $eachExistingCoverage)
	 				{
		 				//match found with existing coverage - do not display the checkbox
		 				if($eachExistingCoverage->code==$eachCoverage->value)
		 					$displayCheckBox = 'false';
	 				}
		 				
	 				if($displayCheckBox == 'true')
	 				{
				 					 	
		 				$multilingualValue = $eachCoverage->name;
			 			//parameters exist
		 				if(count($eachCoverageDefinition->parameters) > 0 )
		 				{
			 				$multilingualValue = "";
			 				
			 				$i=1;
			 				foreach($eachCoverageDefinition->parameters as $eachParameter)
			 				{	
				 				${"param".$i} = $eachParameter->value;
				 				$i++;
			 				}	
		 		
						 	//split in case we have parameters in between
					 		$multilingualValueArray = explode('|',$eachCoverage->name);
					 		
					 		$i=1;//to get the correct parameter
					 		foreach($multilingualValueArray as $eachPiece)
					 		{
						 		if($i == count($multilingualValueArray) )
						 			$multilingualValue = $multilingualValue.$eachPiece;
						 		else
						 			$multilingualValue = $multilingualValue.$eachPiece.${"param".$i};
						 		$i++;
						 		
					 		}
				 		
			 			}
				 		
				 		//echo $multilingualValue;
				 		
					 	//no selection levels - show check box.
				 		//send also the parameters, if any
				 		if($eachCoverageDefinition->selectionLevels == null )
				 		{
						 	?>
						 	<tr>
						 		<td>
						 		<input type="checkbox" onclick="javascript:updateCoverageAjax('<?php echo $eachCoverage->value;?>');" id="<?php echo $eachCoverage->value; ?>" value="<?php echo $eachCoverage->value; ?>">
						 		<?php echo $multilingualValue; ?>
						 		<?php
						 		if(count($eachCoverageDefinition->parameters) > 0 )
				 				{
					 				$i=1;
					 				foreach($eachCoverageDefinition->parameters as $eachParameter)
					 				{	?>
							 			<input type="hidden" name="<?php echo $eachCoverage->value.'Param'.$i; ?>" id="<?php echo $eachCoverage->value.'Param'.$i; ?>" value="<?php echo ${"param".$i};?>" >
							 			<?php
							 			$i++;
					 				}
					 			}?>
						 		</td>
						 	</tr>
						 	<?php
					 	}
					 	//<selectionlevels> exist - display select button to select from all selection levels
					 	else
					 	{
						 	//echo "inside selction levels";
						 	?>
						 	<tr>
						 		<td>
						 		<input type="checkbox" onclick="javascript:updateCoverageAjax('<?php echo $eachCoverage->value;?>');" id="<?php echo $eachCoverage->value; ?>" value="<?php echo $eachCoverage->value; ?>">
						 		<?php echo $multilingualValue; ?> 
						 		<select name="<?php echo $eachCoverage->value.'Param1'; ?>" id="<?php echo $eachCoverage->value.'Param1'; ?>">
						 		<?php
						 		foreach($eachCoverageDefinition->selectionLevels as $eachSelectionLevel)
						 		{
						 			?>
						 			<option value="<?php echo $eachSelectionLevel;?>"><?php echo $eachSelectionLevel;?></option>
						 			<?php
					 			}?>
								</select> 
						 		</td>
						 	</tr>
						 	
						 	<?php
					 	}
					 	break;
				 	}
		 		}
		 	}
	 	}
 	}
}

/* display the additional drivers section of the updateContractData screen */
function displayAdditionalDrivers($drivers)
{
	global $NO_CONSTANT;
	?>
	<tr>
	<td class="col10Per" colspan="8"><h4><?php echo $_SESSION['additionalDrivers']; ?></h4></td>
	</tr>
	<tr>
		<td class="col10Per"><b><?php echo $_SESSION['nameTab']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['stateIdTab']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['nationality']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['birthDate']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['licenseDateTab']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['country']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['licenseTypeTab']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['telephoneTab']; ?></b></td>
	</tr>
	<?php
	//SHOW ALL EXISTING DRIVERS
	foreach($drivers as $eachDriver)
	{
		?>
		<tr>
			
			<!-- NAME -->
			<td class="col10Per"><?php echo $eachDriver->firstName." ".$eachDriver->lastName; ?></td>
			<!-- STATE ID -->
			<td class="col10Per"><?php echo $eachDriver->stateId; ?></td>
			<!-- OWNER ETHNICITY-->
			<td class="col10Per"><?php echo $eachDriver->countryOfBirth;?></td>
			<!-- BIRTH DATE -->
			<td class="col10Per"><?php echo $eachDriver->birthDate;?></td>
			<!-- LICENSE DATE -->
			<td class="col10Per"><?php echo $eachDriver->licenseDate;?></td>
			<!-- LICENSE COUNTRY -->
			<td class="col10Per"><?php echo $eachDriver->licenseCountry;?></td>
			<!-- LICENSE TYPE -->
			<td class="col10Per"><?php echo $eachDriver->licenseType;?></td>
			<!-- TELEPHONE -->
			<td class="col10Per"><?php echo $eachDriver->telephone;?></td>
		</tr>
		<?php
	}
}

/* display the additional insured persons in this medical insurance */
function displayMedicalInsuredPersons($medicalInsuredPersons)
{
	writeToFile(date("Y-m-d H:i:s"). ": displayMedicalInsuredPersons() ENTER \n", LOG_LEVEL_INFO);
	?>
	<table id="medicalInsuredPersonsTable">
		<tr>
			<td class="col10Per" colspan="2"><h4><?php echo $_SESSION['insuredPersons']; ?></h4></td>
			<td class="col10Per"></td>
			<td class="col10Per"></td>
		</tr>
		<tr>
			<td class="col10Per"></td>
			<td class="col10Per"><b><?php echo $_SESSION['nameTab']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['surname']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['stateIdTab']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['gender']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['telephoneTab']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['birthDate']; ?></b></td>
			<td class="col10Per"></td>	
		</tr>
		<?php
		$t = 0;//aaresti changed
		//SHOW ALL EXISTING DRIVERS
		foreach($medicalInsuredPersons as $eachPerson)
		{
			//$eachPerson->printData();
			
			?>
			<tr id="<?php echo $eachPerson->personId; ?>">
				<!-- Person Id hidden -->
				<input type="hidden" name="personIdMedical[]" value="<?php echo $eachPerson->personId; ?>" />
				<td class="col10Per"><input type="checkbox" name="chkMedical[]" onclick="javascript:deleteMedicalPersonAjax('<?php echo $eachPerson->personId;?>');"  ></td>
				<!-- NAME -->
				<td class="col10Per"><input type="text" name="firstNameMedical[]" value="<?php echo $eachPerson->firstName; ?>" size="10" /></td>
				<!-- LAST NAME -->
				<td class="col10Per"><input type="text" name="lastNameMedical[]" value="<?php echo $eachPerson->lastName; ?>" size="10"  /></td>
				<!-- STATE ID -->
				<td class="col10Per"><input type="text" name="stateIdMedical[]" value="<?php echo $eachPerson->stateId; ?>" size="10"  /></td>
				<!-- GENDER -->
				<td class="col10Per">
					<select name="genderMedical[]" id=<?php echo "personGender".$t;?> />
						<?php
						foreach($_SESSION['genderOptions'] as $option){
							?>
							 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
							 <?php
						 }
						 ?>
					</select>
				</td>
				<!-- TELEPHONE -->
				<td class="col10Per"><input type="text" name="telephoneMedical[]" value="<?php echo $eachPerson->telephone; ?>" size="10"  /></td>
				<!-- BIRTH DATE -->
				<td class="col10Per"><input class="datepick" type="text" name="birthDateMedical[]" id=<?php echo "birthDate_".$t; ?> value="<?php echo $eachPerson->birthDate; ?>" size="10" /></td>
				<td></td>
			</tr>
			<script type="text/javascript">	
					setPreviousValue("<?php echo $eachPerson->gender; ?>", "<?php echo "personGender".$t; ?>");
			</script>
			<?php
			$t = $t + 1;
		}
		?>
		
		<!--  insert empty row -->
		<tr>
				<input type="hidden" name="personIdMedical[]" value="" />
				<td class="col10Per"><input type="checkbox" name="chkMedical[]" /></td>
				<!-- NAME -->
				<td class="col10Per"><input type="text" name="firstNameMedical[]" size="10" /></td>
				<!-- LAST NAME -->
				<td class="col10Per"><input type="text" name="lastNameMedical[]" size="10"  /></td>
				<!-- STATE ID -->
				<td class="col10Per"><input type="text" name="stateIdMedical[]" size="10"  /></td>
				<!-- GENDER -->
				<td class="col10Per">
					<select name="genderMedical[]" id=<?php echo "personGender".$t;?> />
						<?php
						foreach($_SESSION['genderOptions'] as $option){
							?>
							 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
							 <?php
						 }
						 ?>
					</select>
				</td>
				<!-- TELEPHONE -->
				<td class="col10Per"><input type="text" name="telephoneMedical[]" size="10"  /></td>
				<!-- BIRTH DATE -->
				<td class="col10Per"><input class="datepick" type="text" name="birthDateMedical[]" id=<?php echo "birthDate_".$t; ?> size="10" /></td>
				<td><a class="addnew" id="addnew" href="">Add</a> </td>
			</tr>
			
	</table>
			
	<?php

        writeToFile(date("Y-m-d H:i:s"). ": displayMedicalInsuredPersons() EXIT \n", LOG_LEVEL_INFO);
}

?>