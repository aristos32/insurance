<?php
//COMMON INCLUDES FOR ALL CLIENTS AND CORE FUNCTIONALITY
global $YES_CONSTANT, $NO_CONSTANT, $NONE, $CANNOT_ADD;

/* read all the properties from interfaceProperties.xml.
This is done only when first loading the page */
//when this is called from cyprus-insurances/quotation.php, $_SESSION['lang'] is not set. no need to load the variables.
//readInterfaceProperties();
require_once($_SESSION['globalFilesLocation']."/generalIncludes/definitions.php");
require_once($_SESSION['globalFilesLocation']."/generalIncludes/structures.php");
require_once($_SESSION['globalFilesLocation']."/generalIncludes/globalHTMLDisplayFunctions.php");
require_once($_SESSION['globalFilesLocation']."/database/connectorHierarchy.php");
require_once($_SESSION['globalFilesLocation']."/database/retrieveDatalayers.php");
require_once($_SESSION['globalFilesLocation']."/database/insertDatalayers.php");
require_once($_SESSION['globalFilesLocation']."/database/updateDatalayers.php");
require_once($_SESSION['globalFilesLocation']."/database/deleteDatalayers.php");
require_once($_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/readInsCompanyXMLFilesFunctions.php");
require_once($_SESSION['globalFilesLocation']."/generalIncludes/utilityFunctions.php");
require_once($_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuoteLogic/addQuotationChargesFunctions.php");
require_once($_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/saveInsCompanyXMLForm.php");
require_once($_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/displayInsCompanyXMLFormFunctions.php");
require_once($_SESSION['globalFilesLocation']."/database/storeQuoteInDatabase.php");
require_once($_SESSION['globalFilesLocation']."/javascript/javascriptfunctions.php");
require_once($_SESSION['globalFilesLocation']."/javascript/fillPreviousFormDataFunctions.php");
require_once($_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/generalFunctions.php");

?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<?php
//set language
$lang=@$_POST['lang'];
setLanguage($lang);



//use the database of the appropriate client, or 'cyprus-insurances' if no client is set
//echo "INSIDE".$_SESSION['clientFilesLocation'];
if(isset($_SESSION['clientFilesLocation']))
	include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";

$_SESSION["sessionid"] = session_id();
$a = session_id();
$logMessage = date("Y-m-d H:i:s"). ": Starting Client " .$clientName. "\n";
$logMessage = $logMessage. date("Y-m-d H:i:s"). ": Starting New Session sessionid = ".$_SESSION["sessionid"]." \n";
//writeToFile($logMessage, LOG_LEVEL_INFO);

?>