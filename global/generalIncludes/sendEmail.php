<?php

// Set flag that this is a parent file.
define('_INC', 1);
//set unicode, for multilingual support
header('Content-Type: text/html; charset=utf-8');
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter("must-revalidate");
//$eMail = $_GET['eMail'];
session_start();

$email = $_REQUEST["email"];  
$_SESSION['globalFilesLocation'] = $_REQUEST["globalFilesLocation"];  
$_SESSION['lang'] = $_REQUEST["lang"]; 
$_SESSION['clientName'] = $_REQUEST["clientName"]; 
$_SESSION['systemEmail'] = $_REQUEST["systemEmail"]; 
$_SESSION['allStateIds'] = $_REQUEST["allStateIds"];
//echo $_SESSION['allStateIds'];
//echo $email." ".$_SESSION['clientFilesLocation'];

require_once("../".$_SESSION['globalFilesLocation']."/multilingual/interfaceProperties.php");
require_once("../".$_SESSION['globalFilesLocation']."/generalIncludes/structures.php");

//Read multilingual variables needed
$doc = new DOMDocument();
$doc->load( "../".$_SESSION['globalFilesLocation']."/multilingual/interfaceProperties". $_SESSION['lang'] . ".xml" );
//nameTab
setMultilingualForVariable($doc, "nameTab");
//emailSubject
setMultilingualForVariable($doc, "emailSubject");
//send
setMultilingualForVariable($doc, "send");
//emailTo
setMultilingualForVariable($doc, "emailTo");
//sender
setMultilingualForVariable($doc, "sender");



?>

<script type="text/javascript">	
/* check that the send email Form is valid */
function checkSendEmailForm()
{
	try{
		
		 var complete=true;
		 var error = "Please correct following issues:\n";	 
		 var senderName = document.getElementById("senderName").value;
		 var email = document.getElementById("email").value;
		 var emailSubject = document.getElementById("emailSubject").value;
		 var message = document.getElementById("message").value;
		 
		 if( senderName=="" || email=="" || emailSubject=="" || message=="" )
		 {
			 error = error + "All fields are mandatory \n";
			 complete = false;
		 }
	 }
	 catch(e) {
        alert("Inside checkSendEmailForm. Error:" + e);
    }
 
 if( complete == false )
	window.alert(error);	
	
 return complete;
}

</script>

<form name="sendEmailForm" action="./sendEmailFormProcess.php" method="POST" onSubmit="return checkSendEmailForm()" >
	<table>
		<tr>
			<td class="label"><?php echo $_SESSION['sender']; ?>:</td>
			<td class="input"><input type="text" name="senderName" id="senderName" size="30" value="<?php echo $_SESSION['clientName'];?>" /> </td>
		</tr>
		<tr>
			<td class="label"><?php echo $_SESSION['emailTo']; ?>:</td>
			<td class="input"><input type="text" name="email" id="email" size="30" value="<?php echo $email;?>" /> </td>
		</tr>
		<tr>
			<td class="label"><?php echo $_SESSION['emailSubject']; ?>:</td>
			<td class="input"><input type="text" name="emailSubject" id="emailSubject" size="30" value="" /> </td>
		</tr>
		<tr>
			<td class="label"><?php echo $_SESSION['emailSubject']; ?>:</td>
			<td><textarea rows="8" cols="30" name="message" id="message"> </textarea> </td>
		</tr>
		<tr>
			<td class="label"></td><td class="input"><INPUT type="submit" value=<?php echo $_SESSION['send']; ?> size="15" ></td>
		</tr>
	</table>
</form>