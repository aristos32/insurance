<?php
// Set flag that this is a parent file.
define('_INC', 1);

session_start();//need to have this in every file, to have the session available.
require_once("../".$_SESSION['globalFilesLocation']."/generalIncludes/utilityFunctions.php");

$senderName = $_POST['senderName'];
$toEmail = $_POST['email'];//to email
$emailSubject = $_POST['emailSubject'];
$message = $_POST['message'];
$allStateIds = $_SESSION['allStateIds'];

//look for \n(newline), possible \r(carriage return), possible new \n kai replace with <br/>
$message = "<p>"
	  . preg_replace("\n\r?\n?", "<br/>", htmlspecialchars($message));
//echo $allStateIds;

//separate all toEmails and corresponding allStateIds and send individual email to each
$allEmailsArray = explode(',', $toEmail);
$allStateIdsArray = explode(',', $allStateIds);

//print_r($allEmailsArray);
//print_r($allStateIdsArray);

//send different email to each client
foreach($allEmailsArray as $a => $b)
{
	//make sure the email is empty
	if($allEmailsArray[$a]!='')
	{
		
		//echo $allEmailsArray[$a];
		//echo $allStateIdsArray[$a];
		//convert plain text to html and add unsubscribe link
		$individualMessage = $message."</p>" . '<br><br> If you wish to Unsubscribe from these emails, '.
			//'press <a href=\"http://localhost:8080/global/generalIncludes/unsubscribe.php?toEmail='.$allEmailsArray[$a].'&stateId='.$allStateIdsArray[$a].'&clientName='.$senderName.'\">Unsubscribe</a>';
			'press <a href=\"http://www.online-insurance-app.com/global/generalIncludes/unsubscribe.php?toEmail='.$allEmailsArray[$a].'&stateId='.$allStateIdsArray[$a].'&clientName='.$senderName.'\">Unsubscribe</a>';
			
		//echo "toEmail=$toEmail";
		//echo $individualMessage;
		
		$headers   = array();
		$headers[] = "MIME-Version: 1.0";
		$headers[] = "Content-type: text/html; charset=iso-8859-1";
		$headers[] = "From: ".$_SESSION['systemEmail'];
		$headers[] = "bcc: $allEmailsArray[$a]";//for privacy of emails
		//$headers[] = "cc: $email";//for privacy of emails
		$headers[] = "X-Mailer: PHP/".phpversion();
		//$toEmail = $senderName;//for privacy, don't show
		sendEmail($emailSubject, $individualMessage, $toEmail, $senderName, $headers);
	}
	
}


echo 'Message Send Successfully. <a href="javascript:window.close();">CLOSE WINDOW</a>';

?>