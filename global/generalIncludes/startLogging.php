<?php

require_once($_SESSION['globalFilesLocation']."/common/logger.php");

$globalLogLevel = '';

//read properties of each client - only load at the first call
$_SESSION['parametersArray'] = readClientProperties($_SESSION['clientFilesLocation'].'/configurationFiles/config.xml');
foreach($_SESSION['parametersArray'] as $eachParameter)
{
	if($eachParameter->name=='SYSTEM_EMAIL')
	{
		$_SESSION['SYSTEM_EMAIL'] = $eachParameter->value;		
		
	}
    else if($eachParameter->name=='LOG_LEVEL')
	{
		$_SESSION['LOG_LEVEL'] = $eachParameter->value;		
	}
}

//get all clientNames - databases from the accounts.xml
if(!isset($_SESSION['accounts']))
	$_SESSION['accounts'] = readAccounts($_SESSION['globalFilesLocation'].'/configurationFiles/accounts.xml');
	
//get all product types from productTypes.xml
if(!isset($_SESSION['productTypes']))
	$_SESSION['productTypes'] = readProductTypes($_SESSION['globalFilesLocation'].'/configurationFiles/productTypes.xml');
	
//this variable is set during authentication.php. So, this must come after authentication, successfull or not
if(isset($_SESSION['clientFilesLocation'])&& $_SESSION['clientFilesLocation']!='' && isset($_SESSION['LOG_LEVEL']))
{
	
	
	$globalLogLevel = $_SESSION['LOG_LEVEL']; 
	
		
	$clientLogger = new logger($_SESSION['clientFilesLocation'].'/logs/','logFile.txt', $globalLogLevel);
	
}

//set maximum log level
$auditLogger = new logger($_SESSION['globalFilesLocation'].'/logs/','audit.txt', LOG_LEVEL_ALL);
		
?>