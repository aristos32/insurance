<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

	
include "definitions.php";


/* STRUCTURES RELATED TO READING THE XML FILE */
/* general type-desciption info 
name : name of the type
description : description
value : value of the type, corresponding to a value in definitions.php */
class generalType{
	public $name;
	public $description;
	public $value;
	//variables needed only for <emailMessages>
	public $subject;
	public $line1;
	public $line2;
	
	public function printData()
	{
		echo "GENERAL TYPE:-> START <br>";
		echo "name:-> ".$this->name."<br>";
		echo "description:-> ".$this->description."<br>";
		echo "value:-> ".$this->value."<br>";
		echo "subject:-> ".$this->subject."<br>";
		echo "line1:-> ".$this->line1."<br>";
		echo "line2:-> ".$this->line2."<br>";
		echo "GENERAL TYPE:-> END <br>";
	}
	
}

/* email type info 
subject : subject of email
line1 : 1st line of message
line2 : 2nd line of message
 */
class emailType extends generalType{
	public $subject;
	public $line1;
	public $line2;
	
	public function printData()
	{
		echo "EMAIL TYPE:-> START <br>";
		echo "subject:-> ".$this->subject."<br>";
		echo "line1:-> ".$this->line1."<br>";
		echo "line2:-> ".$this->line2."<br>";
		echo "EMAIL TYPE:-> END <br>";
	}
	
}


/* values read from generalProperties.xml file */
class generalProperties{
	public $stateId;
	public $regNumber;
	
	public function printData()
	{
		echo "GENERALPROPERTIES:-> START <br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "regNumber:-> ".$this->regNumber."<br>";
		echo "GENERALPROPERTIES:-> END <br>";
	}
	
}

class basicCoverage{
	public $companyName;
	public $standardCharges;
	public $vehicleTypes = array();
	
	public function printData()
	{
		echo "BASICCOVERAGE:-> START <br>";
		echo "companyName:-> ".$this->companyName."<br>";
		if( $this->standardCharges <> null )
			$this->standardCharges->printData();
		foreach( $this->vehicleTypes as $vehicleType )
			$vehicleType->printData();
		echo "BASICCOVERAGE:-> END <br>";
	}
}

/* basic coverage information for each vechicle category */
class vehicleType{
	public $name;
	public $value;
	public $coverageTypes = array();
	public $inclusiveBenefits = array();	
	public $additionalChargesFromXMLArray = array();
	public $optionalCoveragesFromXMLArray = array();
	
	public function printData()
	{
		echo "VEHICLE TYPE:-> START <br>";
		echo "name:-> ".$this->name."<br>";
		echo "value:-> ".$this->value."<br>";
		foreach( $this->coverageTypes as $coverageType )
			$coverageType->printData();
		foreach( $this->additionalChargesFromXMLArray as $eachAdditionalChargesFromXML )
			$eachAdditionalChargesFromXML->printData();
		foreach( $this->inclusiveBenefits as $inclusiveBenefit )
			$inclusiveBenefit->printData();
		foreach( $this->optionalCoveragesFromXMLArray as $eachOptionalCoveragesFromXML )
			$eachOptionalCoveragesFromXML->printData();
		echo "VEHICLE TYPE:-> END <br><br>";
	}
}



/* category with types, i.e for license type, owner ethiniciy etc. [to,from] doesn't exist here */
class typeCategory{
	public $type;
	public $euro = 0;//charge in euro
	public $percentCharge = 0;//charge as percentage over basic price
	public $percentChargeComprehensive;//charge as percentage over basic price
	public $allowed; /*is this category allowed for this case? If not, we fail */
	public $minimumEuro;// minimum possible charge after all discounts are applied
	
	public function printData()
	{
		echo "TYPE CATEGORY -> START<br>";
		echo "type:-> ".$this->type."<br>";
		echo "euro:-> ".$this->euro."<br>";
		echo "percentCharge:-> ".$this->percentCharge."<br>";
		echo "percentChargeComprehensive:-> ".$this->percentChargeComprehensive."<br>";
		echo "allowed:-> ".$this->allowed."<br>";
		echo "minimumEuro:-> ".$this->minimumEuro."<br>";
		echo "TYPE CATEGORY -> END<br>";
	}	
}

/* generic category base class */
class genericCategory{
	public $from;//cc
	public $to;//cc
	public $euro = 0;//charge in euro
	public $percentCharge = 0;//charge as percentage over basic price
	public $percentChargeComprehensive;//charge as percentage over basic price
	public $allowed; /*is this category allowed for this case? If not, we fail */
	public $type;
	public $minimumEuro;// minimum possible charge after all discounts are applied
	
	public function printData()
	{
		echo "GENERIC CATEGORY -> START<br>";
		echo "from:-> ".$this->from."<br>";
		echo "to:-> ".$this->to."<br>";
		echo "euro:-> ".$this->euro."<br>";
		echo "percentCharge:-> ".$this->percentCharge."<br>";
		echo "percentChargeComprehensive:-> ".$this->percentChargeComprehensive."<br>";
		//echo "applicable:-> ".$this->applicable."<br>";
		echo "allowed:-> ".$this->allowed."<br>";
		echo "type:-> ".$this->type."<br>";
		//echo "coverageType:-> ".$this->coverageType."<br>";
		//echo "secondType:-> ".$this->secondType."<br>";
		echo "minimumEuro:-> ".$this->minimumEuro."<br>";
		echo "GENERIC CATEGORY -> END<br>";
	}	
}



/* <vehicleType>-><coverageType> info from input XML */
class coverageType{
	public $name;
	public $value;
	public $available;
	public $minimumChargeEuro = 0;
	public $excessEuro = 0;
	public $coverages = array();
	public $discounts;

	public function printData()
	{
		echo "COVERAGETYPE:-> START <br>";
		echo "NAME:-> ".$this->name."<br>";
		echo "value:-> ".$this->value."<br>";
		echo "available:-> ".$this->available."<br>";
		echo "minimumChargeEuro:-> ".$this->minimumChargeEuro."<br>";
		echo "excessEuro:-> ".$this->excessEuro."<br>";
		echo count($this->coverages);
		foreach($this->coverages as $eachCoverage)
			$eachCoverage->printData();
		if( $this->discounts <> null )
			$this->discounts->printData();
		echo "COVERAGETYPE:-> END <br>";		
	}	
}

/* discounts info. Refers for comprehensive insurance.
   Data Read from the input XML */
class discounts{
	public $available;
	public $coverages = array();
	
	public function printData()
	{
		echo "DISCOUNTS:-> START <br>";
		echo "available:-> ".$this->available."<br>";
		foreach($this->coverages as $eachCoverage)
			$eachCoverage->printData();
		echo "DISCOUNTS:-> END <br>";
	}
	
}


/* inclusive benefits. Can represent a company package, or a custom-selected set of benefits.
Used for internal processing */
class inclusiveBenefit{
	public $name;
	public $available;
	public $coverageTypeOfOffer; //which coverage type is the offer available for.
	public $mandatory;
	public $chargeOverBasic = 0;
	
	public $optionalCoveragesFromXMLArray = array();
	
	public function printData()
	{
		echo "INCLUSIVE BENEFITS:-> START <br>";
		echo "Name:-> ".$this->name."<br>";
		echo "available:-> ".$this->available."<br>";
		echo "Mandatory:-> ".$this->mandatory."<br>";
		echo "coverageTypeOfOffer:-> ".$this->coverageTypeOfOffer."<br>";
		echo "chargeOverBasic:-> ".$this->chargeOverBasic."<br>";
		foreach($this->optionalCoveragesFromXMLArray as $eachCoverage)
			$eachCoverage->printData();
		echo "INCLUSIVE BENEFITS:-> END <br>";
	}
}


/* All the user input information for getting a quote
	input comes from carQuoteForm Form */
class quotationInfo{
	public $email;
	public $contractNumber;
	public $oldSaleId;
	public $contractType;
	public $firstName;
	public $lastName;
	public $stateId;
	public $oldStateId;
	public $telephone;
	public $cellphone;
	public $ownerProfession;
	public $ownerCompany;
	public $birthDate;
	public $proposerType;
	public $producer;//who is the producer for this contract
	
	//motor specific
	public $licenseType;//proposer license
	public $licenseDate;
	public $countryOfBirth;
	public $licenseCountry;
	
	//driving experience
	public $hasPreviousInsurance;
	public $countryOfInsurance;
	public $insuranceCompany;
	public $yearsOfExperience;
	
	public $insuranceCompanyOfferingQuote;
	public $regNumber;
	public $vehicleType;
	public $vehicleMake;
	public $vehicleModel;
	public $cubicCapacity;
	public $vehicleManufacturedYear;
	public $steeringWheelSide;
	public $isSportsModel;
	public $vehicleDesign;
	public $isTaxFree;
	public $isUsedForDeliveries;
	public $hasVisitorPlates;
	public $insuranceType;
	public $coverageType;
	public $coverageAmount;
	public $hasDisability;
	public $maxPenaltyPointsOfAnyDriverDuringLastThreeYears;
	public $numberOfAccidentsOfAnyDriverDuringLastThreeYears;
	public $claims = array();
	
	//additional drivers
	public $addAdditionalDriversQuestion;
	public $additionalDriversLicenseCountry;
	public $additionalDriversEthnicity;
	public $additionalDriversLicenseType;//for additional driver
	public $normalDrivingLicenseTotalYearsForAdditionalDrivers = 10;//standard age that causes no charges
	public $ageOfYoungestDriver = 30;//standard age that causes no charges
	public $ageOfOldestDriver = 30;//standard age that causes no charges
	
	public $userInfo;
	public $offerTypeSelected;
	public $namedDriversNumber;
	public $additionalExcess;
	public $noClaimDiscountYears;
	public $drivers = array();
	public $quoteId;//filled after the quote is created
	public $canProvideOnlineQuote;
	public $associate;
	
	public $coveragesInPolicyQuotationArray = array();
	public $additionalChargesInPolicyQuotationArray = array();
	public $discountsInQuotationArray = array();
	
	public $employersLiability;
	
	public $medical;
	
	public $propertyFire;
	
	//constructor - initialize variables
	function quotationInfo()
	{
		global $NO_CONSTANT;
		$this->addAdditionalDriversQuestion = $NO_CONSTANT;
	}
	
	public function printData()
	{
		echo "QUOTATION INFO:-> START <br>";
		echo "email:-> ".$this->email."<br>";
		echo "contractNumber:-> ".$this->contractNumber."<br>";
		echo "contractType:-> ".$this->contractType."<br>";
		echo "oldSaleId:-> ".$this->oldSaleId."<br>";
		echo "firstName:-> ".$this->firstName."<br>";
		echo "lastName:-> ".$this->lastName."<br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "oldStateId:-> ".$this->oldStateId."<br>";
		echo "telephone:-> ".$this->telephone."<br>";
		echo "cellphone:-> ".$this->cellphone."<br>";
		echo "profession:-> ".$this->profession."<br>";
		echo "ownerCompany:-> ".$this->ownerCompany."<br>";
		echo "birthDate:-> ".$this->birthDate."<br>";
		echo "proposerType:-> ".$this->proposerType."<br>";
		echo "licenseType:-> ".$this->licenseType."<br>";
		echo "licenseDate:-> ".$this->licenseDate."<br>";
		echo "countryOfBirth:-> ".$this->countryOfBirth."<br>";
		echo "licenseCountry:-> ".$this->licenseCountry."<br>";
		echo "hasPreviousInsurance:-> ".$this->hasPreviousInsurance."<br>";
		echo "countryOfInsurance:-> ".$this->countryOfInsurance."<br>";
		echo "insuranceCompany:-> ".$this->insuranceCompany."<br>";
		echo "yearsOfExperience:-> ".$this->yearsOfExperience."<br>";
		
		echo "insuranceCompanyOfferingQuote:-> ".$this->insuranceCompanyOfferingQuote."<br>";
		echo "vehicleMake:-> ".$this->vehicleMake."<br>";
		echo "vehicleType:-> ".$this->vehicleType."<br>";
		echo "vehicleModel:-> ".$this->vehicleModel."<br>";
		echo "cubicCapacity:-> ".$this->cubicCapacity."<br>";
		echo "vehicleManufacturedYear:-> ".$this->vehicleManufacturedYear."<br>";
		echo "steeringWheelSide:-> ".$this->steeringWheelSide."<br>";
		echo "isSportsModel:-> ".$this->isSportsModel."<br>";
		echo "vehicleDesign:-> ".$this->vehicleDesign."<br>";
		echo "isTaxFree:-> ".$this->isTaxFree."<br>";
		echo "isUsedForDeliveries:-> ".$this->isUsedForDeliveries."<br>";
		echo "hasVisitorPlates:-> ".$this->hasVisitorPlates."<br>";
		echo "insuranceType:-> ".$this->insuranceType."<br>";
		echo "coverageType:-> ".$this->coverageType."<br>";
		echo "coverageAmount:-> ".$this->coverageAmount."<br>";
		echo "hasDisability:-> ".$this->hasDisability."<br>";
		echo "maxPenaltyPointsOfAnyDriverDuringLastThreeYears:-> ".$this->maxPenaltyPointsOfAnyDriverDuringLastThreeYears."<br>";
		echo "numberOfAccidentsOfAnyDriverDuringLastThreeYears:-> ".$this->numberOfAccidentsOfAnyDriverDuringLastThreeYears."<br>";
		echo "addAdditionalDriversQuestion:-> ".$this->addAdditionalDriversQuestion."<br>";
		echo "additionalDriversLicenseCountry:-> ".$this->additionalDriversLicenseCountry."<br>";
		echo "additionalDriversEthnicity:-> ".$this->additionalDriversEthnicity."<br>";
		echo "additionalDriversLicenseType:-> ".$this->additionalDriversLicenseType."<br>";
		echo "normalDrivingLicenseTotalYearsForAdditionalDrivers:-> ".$this->normalDrivingLicenseTotalYearsForAdditionalDrivers."<br>";
		echo "ageOfYoungestDriver:-> ".$this->ageOfYoungestDriver."<br>";
		echo "ageOfOldestDriver:-> ".$this->ageOfOldestDriver."<br>";
		echo "userInfo:-> ".$this->userInfo."<br>";
		echo "offerTypeSelected:-> ".$this->offerTypeSelected."<br>";
		echo "namedDriversNumber:-> ".$this->namedDriversNumber."<br>";
		echo "additionalExcess:-> ".$this->additionalExcess."<br>";
		echo "noClaimDiscountYears:-> ".$this->noClaimDiscountYears."<br>";
		echo "producer:-> ".$this->producer."<br>";
		
		foreach($this->claims as $claim)
			$claim->printData();		
		foreach($this->drivers as $driver)
			$driver->printData();
		foreach($this->coveragesInPolicyQuotationArray as $eachCoveragesInPolicyQuotation)
			$eachCoveragesInPolicyQuotation->printData();
		foreach($this->additionalChargesInPolicyQuotationArray as $eachChargeInPolicyQuotation)
			$eachChargeInPolicyQuotation->printData();
		foreach($this->discountsInQuotationArray as $eachDiscountInQuotation)
			$eachDiscountInQuotation->printData();
			
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "canProvideOnlineQuote:-> ".$this->canProvideOnlineQuote."<br>";
		echo "associate:-> ".$this->associate."<br>";
		
		if( $this->employersLiability <> null )
			$this->employersLiability->printData();
		
		if( $this->medical <> null )
			$this->medical->printData();
			
		if( $this->propertyFire <> null )
			$this->propertyFire->printData();
			
		echo "QUOTATION:-> END <br>";
	}		
}//quotationInfo



/*STRUCTURES RELATED TO PROCESSING OF THE REQUEST */

/* all information gathered/calculated during the processing of the user request.
   This is returned for each insurance company.
   This is processed for output and recommendations to the user */
class processingInfo{
	
	public $canProvideOnlineQuote = 'YES'; /* =YES=>we can provide a quote online, 
										      =NO=>we cannot provide a quote online. need personal examination of case */
										  
	public $reasonsWeCannotProvideOnlineQuote = array(); /* only filled/used when $canProvideOnlineQuote = NO, i.e when
													we cannot provide and online quote */
													
	public $basicCoverageCost = 0;/* basic coverage before any other charges */
	
	public $addedNotMandatoryCosts = array();/* not mandatory costs */
	
	public $addedMandatoryCosts = array(); /* mandatory costs */
	
	public $standardCharges; /* MIF, fees, stamps and other */
	
	public $finalCost = 0;/* final cost calculated during the processing */
	
	public $inclusiveBenefit; /* package offered */
	
	/* offers related information */
	public $selectedOfferName = 'NONE';
	public $anyOfferSelected = 'NO';
	
	public $insuranceCompanyOfferingQuote = ''; /*name of insurance company */
	
	public $minimumEuro = 0; /* for COMPREHENSIVE, the minimum allowed premioum. $localVehicleType->comprehensive->ccCategory->minimumEuro */
	
	public $excessAmount = 0; /* total Excess amount for COMPREHENSIVE */
	
	public $coveragesInPolicyQuotationArray = array();//all coverages requested for this offer
	
	public $discountsInQuotationArray = array();//all discounts applied to this quotation
	
	public $coveragesInXMLArray = array();//all coverages as read from optional coverages XML file, for this vehicle type
	
	public function printData()
	{
		echo "<br><br><br>PROCESSING INFO:-> START ".$this->insuranceCompanyOfferingQuote."<br>";
		echo "insuranceCompanyOfferingQuote:-> ".$this->insuranceCompanyOfferingQuote."<br>";
		echo "basicCoverageCost:-> ".$this->basicCoverageCost."<br>";
		echo "canProvideOnlineQuote:-> ".$this->canProvideOnlineQuote."<br>";
		echo "finalCost:-> ".$this->finalCost."<br>";		
		echo "selectedOfferName:-> ".$this->selectedOfferName."<br>";
		echo "anyOfferSelected:-> ".$this->anyOfferSelected."<br>";
		echo "minimumEuro:-> ".$this->minimumEuro."<br>";
		echo "ADDED NOT MANDATORY COSTS: ->START<br>";
		foreach($this->addedNotMandatoryCosts as $eachAddedNotMandatoryCosts)
			$eachAddedNotMandatoryCosts->printData();
		echo "ADDED NOT MANDATORY COSTS: ->END<br>";
		echo "ADDED MANDATORY COSTS: ->START<br>";
		foreach($this->addedMandatoryCosts as $eachAddedMandatoryCosts)
			$eachAddedMandatoryCosts->printData();
		echo "ADDED MANDATORY COSTS: ->END<br>";
		if( $this->standardCharges <> null )
			$this->standardCharges->printData();
		if( $this->discount <> null )
			$this->discount->printData();
		if( $this->inclusiveBenefit <> null )
			$this->inclusiveBenefit->printData();
		echo "COVERAGES REQUESTED BY CUSTOMER: ->START<br>";
		foreach($this->coveragesInPolicyQuotationArray as $eachCoveragesInPolicyQuotation)
			$eachCoveragesInPolicyQuotation->printData();
		foreach($this->discountsInQuotationArray as $eachDiscountInQuotation)
			$eachDiscountInQuotation->printData();
		echo "COVERAGES REQUESTED BY CUSTOMER: ->END<br>";
		echo "OPTIONAL COVERAGES READ FROM XML: ->START<br>";
		foreach($this->coveragesInXMLArray as $eachCoveragesXML)
			$eachCoveragesXML->printData();
		echo "OPTIONAL COVERAGES READ FROM XML: ->END<br>";
		echo "REASONS WE CANNOT PROVIDE ONLINE QUOTE: ->START<br>";
		printDebugMessages($this->reasonsWeCannotProvideOnlineQuote);
		echo "REASONS WE CANNOT PROVIDE ONLINE QUOTE: ->END<br>";
		echo "PROCESSING INFO:-> END <br>";
	}	
}




/* all information for storing the standardCharges tag of the input XML */
class standardCharges{
	
	public $MIFPercent;//percentage read from XML
	public $MIFPercentCharge;//quoteCost*MIFPercent. Calculated during processing
	public $stamps;
	public $fees;
	public $other;
	
	public function printData()
	{
		echo "STANDARD CHARGES:-> START <br>";
		echo "MIFPercent:-> ".$this->MIFPercent."<br>";
		echo "MIFPercentCharge:-> ".$this->MIFPercentCharge."<br>";
		echo "stamps:-> ".$this->stamps."<br>";
		echo "fees:-> ".$this->fees."<br>";
		echo "other:-> ".$this->other."<br>";
		echo "TOTAL:-> ".($this->MIFPercentCharge+$this->stamps+$this->fees+$this->other)."<br>";
		echo "STANDARD CHARGES:-> END <br>";
	}	
}

/* STRUCTURES RELATED TO THE DATABASE TABLES */

/* all information needed for creating a new user.
This is provided in newUserForm.php
Corresponds to table SYSTEMUSER */
class user{
	
	public $username;
	public $password;
	public $role;
	public $stateId;
	public $oldStateId;//just for fillOwnerClass commpatibility - not used now
	public $producer;//YES/NO
	public $countryOfBirth;//just for fillOwnerClass commpatibility - not used now
	public $licenseCountry;//just for fillOwnerClass commpatibility - not used now
	public $hasPreviousInsurance;//just for fillOwnerClass commpatibility - not used now
	//public $insuranceCompany;//just for fillOwnerClass commpatibility - not used now
	public $gender;
	public $firstName;
	public $lastName;
	public $addresses = array();
	public $telephone;
	public $cellphone;
	public $profession;
	public $email;
	public $emailPreference;
	public $birthDate;
	public $licenseIssueDate;
	public $clientName;
	public $status;
	public $quoteId;//just for fillOwnerClass commpatibility - not used now
	public $proposerType;//just for fillOwnerClass commpatibility
	public $productType;//what product this user is using, see $PRODUCT_TYPE_
	public $consecutiveFailLoginAttempts;
		
	public function printData()
	{
		echo "USER INFO:-> START <br>";
		echo "username:-> ".$this->username."<br>";
		echo "password:-> ".$this->password."<br>";
		echo "role:-> ".$this->role."<br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "oldStateId:-> ".$this->oldStateId."<br>";
		echo "producer:-> ".$this->producer."<br>";
		echo "gender:-> ".$this->gender."<br>";
		echo "firstName:-> ".$this->firstName."<br>";
		echo "lastName:-> ".$this->lastName."<br>";
		foreach($this->addresses as $address)
			$address->printData();
		echo "telephone:-> ".$this->telephone."<br>";
		echo "cellphone:-> ".$this->cellphone."<br>";
		echo "profession:-> ".$this->profession."<br>";
		echo "email:-> ".$this->email."<br>";
		if( $this->emailPreference <> null )
			$this->emailPreference->printData();
		echo "birthDate:-> ".$this->birthDate."<br>";
		echo "licenseIssueDate:-> ".$this->licenseIssueDate."<br>";
		echo "clientName:-> ".$this->clientName."<br>";
		echo "status:-> ".$this->status."<br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "proposerType:-> ".$this->proposerType."<br>";
		echo "productType:-> ".$this->productType."<br>";
		echo "consecutiveFailLoginAttempts:-> ".$this->consecutiveFailLoginAttempts."<br>";
		echo "USER INFO:-> END <br>";
	}	
	public function __toString() {
        return $this->username;
    }
}

/* emailPreference stored in EMAILPREFERENCES table */
class emailPreference{
	public $stateId;
	public $preferenceCode;
	public $preferenceFrequency;
	
	public function printData()
	{
		echo "<br>EMAIL PREFERENCE:-> START <br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "preferenceCode:-> ".$this->preferenceCode."<br>";
		echo "preferenceFrequency:-> ".$this->preferenceFrequency."<br>";
		echo "EMAIL PREFERENCE:-> END <br>";
	}	
	
}

/* Information needed for creating a new quote.
   Corresponds to table QUOTATION */
class quotation{
	public $canProvideOnlineQuote = 'YES';
	public $reasonsWeCannotProvideOnlineQuote = array(); /* only filled/used when $canProvideOnlineQuote = NO, i.e when
													we cannot provide and online quote */
	public $entryDate;
	public $quoteAmount;
	public $insuranceCompanyOfferingQuote;
	public $offerSelected;
	public $coverageType;
	public $userName;
	public $userInfo;
	public $quoteId = 0;
	
	public $fullName;//not member of quotation, used in retrieveQuoteInfo only
	
	public function printData()
	{
		echo "<br>QUOTATION INFO:-> START <br>";
		echo "canProvideOnlineQuote:-> ".$this->canProvideOnlineQuote."<br>";
		echo "entryDate:-> ".$this->entryDate."<br>";
		echo "quoteAmount:-> ".$this->quoteAmount."<br>";
		echo "insuranceCompanyOfferingQuote:-> ".$this->insuranceCompanyOfferingQuote."<br>";
		echo "userName:-> ".$this->userName."<br>";
		echo "userInfo:-> ".$this->userInfo."<br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "fullName:-> ".$this->fullName."<br>";
		printDebugMessages($this->reasonsWeCannotProvideOnlineQuote);
		echo "QUOTATION INFO:-> END <br>";
	}	
}


/* Information needed for inserting the vehicle related to a quote.
   Corresponds to table VEHICLE */
class vehicle{
	
	public $regNumber;
	public $vehicleType;
	public $make;
	public $model;
	public $submodel;
	public $cubicCapacity;
	public $manufacturedYear;
	public $seatsNo;
	public $sumInsured;
	public $vehicleDesign;
	public $steeringWheelSide;	
	public $isTaxFree;
	public $isUsedForDeliveries;
	public $hasVisitorPlates;
	public $quoteId;//needed to add entry in QuotationVehicle table
	public $saleId;
	public $oldSaleId;
	public $isSportsModel;
	
	public function printData()
	{
		echo "<br>VEHICLE INFO:-> START <br>";
		echo "regNumber:-> ".$this->regNumber."<br>";
		echo "vehicleType:-> ".$this->vehicleType."<br>";
		echo "make:-> ".$this->make."<br>";
		echo "model:-> ".$this->model."<br>";
		echo "submodel:-> ".$this->submodel."<br>";
		echo "cubicCapacity:-> ".$this->cubicCapacity."<br>";
		echo "manufacturedYear:-> ".$this->manufacturedYear."<br>";
		echo "seatsNo:-> ".$this->seatsNo."<br>";
		echo "sumInsured:-> ".$this->sumInsured."<br>";
		echo "vehicleDesign:-> ".$this->vehicleDesign."<br>";
		echo "steeringWheelSide:-> ".$this->steeringWheelSide."<br>";		
		echo "isTaxFree:-> ".$this->isTaxFree."<br>";
		echo "isUsedForDeliveries:-> ".$this->isUsedForDeliveries."<br>";
		echo "hasVisitorPlates:-> ".$this->hasVisitorPlates."<br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "oldSaleId:-> ".$this->oldSaleId."<br>";
		echo "isSportsModel:-> ".$this->isSportsModel."<br>";
		
		echo "VEHICLE INFO:-> END <br>";
	}	
}


/* Information needed for inserting the employers liability info. Corresponds to table EMPLOYERSLIABILITY */
class employersLiability{
	
	public $saleId;
	public $oldSaleId;
	public $employersSocialInsuranceNumber = "";
	public $limitPerEmployee;
	public $limitPerEventOrSeriesOfEvents;
	public $limitDuringPeriodOfInsurance;
	public $employeesNumber;
	public $estimatedTotalGrossEarnings;
	
	public function printData()
	{
		echo "<br>EMPLOYERSLIABILITY INFO:-> START <br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "oldSaleId:-> ".$this->oldSaleId."<br>";
		echo "employersSocialInsuranceNumber:-> ".$this->employersSocialInsuranceNumber."<br>";
		echo "limitPerEmployee:-> ".$this->limitPerEmployee."<br>";
		echo "limitPerEventOrSeriesOfEvents:-> ".$this->limitPerEventOrSeriesOfEvents."<br>";
		echo "limitDuringPeriodOfInsurance:-> ".$this->limitDuringPeriodOfInsurance."<br>";
		echo "employeesNumber:-> ".$this->employeesNumber."<br>";
		echo "estimatedTotalGrossEarnings:-> ".$this->estimatedTotalGrossEarnings."<br>";
		echo "EMPLOYERSLIABILITY INFO:-> END <br>";
	}	
}



/* Information needed for inserting the owner license related to a quote.
   Corresponds to table LICENSE */
class license{
	
	public $stateId;
	public $oldStateId;
	public $licenseType;
	public $licenseDate;
	public $licenseCountry;
		
	public function printData()
	{
		echo "<br>LICENSE:-> START <br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "oldStateId:-> ".$this->oldStateId."<br>";
		echo "licenseType:-> ".$this->licenseType."<br>";
		echo "licenseDate:-> ".$this->licenseDate."<br>";
		echo "licenseCountry:-> ".$this->licenseCountry."<br>";
		echo "LICENSE:-> END <br>";
	}	
}


/* Information needed for inserting the drivers related to a contract.
   Corresponds to table DRIVERS */
class drivers{
	
	public $driverId = 0;
	public $saleId;
	public $stateId;
	public $firstName;
	public $lastName;
	public $birthDate;
	public $countryOfBirth;
	public $licenseCountry;
	public $licenseDate;
	public $licenseType;
	public $telephone;
	public $profession;
	
	public function printData()
	{
		echo "<br>DRIVERS:-> START <br>";
		echo "driverId:-> ".$this->driverId."<br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "firstName:-> ".$this->firstName."<br>";
		echo "lastName:-> ".$this->lastName."<br>";
		echo "birthDate:-> ".$this->birthDate."<br>";
		echo "countryOfBirth:-> ".$this->countryOfBirth."<br>";
		echo "licenseDate:-> ".$this->licenseDate."<br>";
		echo "licenseType:-> ".$this->licenseType."<br>";
		echo "licenseCountry:-> ".$this->licenseCountry."<br>";
		echo "telephone:-> ".$this->telephone."<br>";
		echo "profession:-> ".$this->profession."<br>";
		echo "DRIVERS:-> END <br>";
	}	
}


/* Information needed for inserting the claims related to a quote.
   Corresponds to table CLAIMS */
class claim{
	public $claimId;
	public $quoteId = 0;
	public $amount;
	public $claimDate;//date of claim
	public $description;
	public $stateId;
		
	public function printData()
	{
		echo "<br>CLAIM:-> START <br>";
		echo "claimId:-> ".$this->claimId."<br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "amount:-> ".$this->amount."<br>";
		echo "claimDate:-> ".$this->claimDate."<br>";
		echo "description:-> ".$this->description."<br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "CLAIM:-> END <br>";
	}	
}


/* Information needed for inserting the reasonsWeCannotProvideOnlineQuote related to a quote.
   Corresponds to table REASONSWECANNOTPROVIDEONLINEQUOTE */
class reasonsWeCannotProvideOnlineQuote{
	
	public $quoteId = 0;
	public $reason;
		
	public function printData()
	{
		echo "<br>REASONS WE CANNOT PROVIDE ONLINE QUOTE:-> START <br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "reason:-> ".$this->reason."<br>";
		echo "REASONS WE CANNOT PROVIDE ONLINE QUOTE:-> END <br>";
	}	
}


/* Information needed for inserting the owner related to a quote.
   Corresponds to table OWNER */
class owner{
	
	public $firstName;
	public $lastName;
	public $type;//account or lead
	public $stateId;
	public $oldStateId;
	public $gender;
	public $profession;
	public $company;
	public $countryOfBirth;
	public $countryOfResidence;
	public $birthDate;
	public $telephone;
	public $cellphone;
	public $email;
	public $proposerType;//PERSON or COMPANY
	public $emailPreference;
			
	public function printData()
	{
		echo "<br>OWNER:-> START <br>";
		echo "firstName:-> ".$this->firstName."<br>";
		echo "lastName:-> ".$this->lastName."<br>";
		echo "type:-> ".$this->type."<br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "oldStateId:-> ".$this->oldStateId."<br>";
		echo "gender:-> ".$this->gender."<br>";
		echo "profession:-> ".$this->profession."<br>";
		echo "company:-> ".$this->company."<br>";
		echo "countryOfBirth:-> ".$this->countryOfBirth."<br>";
		echo "countryOfResidence:-> ".$this->countryOfResidence."<br>";
		echo "birthDate:-> ".$this->birthDate."<br>";
		echo "telephone:-> ".$this->telephone."<br>";
		echo "cellphone:-> ".$this->cellphone."<br>";
		echo "email:-> ".$this->email."<br>";
		echo "proposerType:-> ".$this->proposerType."<br>";
		if( $this->emailPreference <> null )
			$this->emailPreference->printData();
		echo "OWNER:-> END <br>";
	}	
}


/* All information related to the insurance proposers. Takes data from different database fields */
class proposer{
	
	public $owner;
	public $addresses = array();
	public $license;
	
	public function printData()
	{
		echo "<br>PROPOSER:-> START <br>";
		if( $this->owner <> null )
			$this->owner->printData();
		foreach($this->addresses as $address)
			$address->printData();
		if( $this->license <> null )
			$this->license->printData();
		echo "PROPOSER:-> END <br>";
	}	
}



/* Information regarding the statistics.
	Used for inserting data to table statistics */
class statistics{
	public $code;
	public $value;

	public function printData()
	{
		echo "<br>STATISTICS:-> START <br>";
		echo "code:-> ".$this->code."<br>";
		echo "value:-> ".$this->value."<br>";
		echo "STATISTICS:-> END <br>";
	}	
}	


/* retrieves all user emails and corresponing ids in a comma separated string */
class emailsIds{
	public $allEmails;
	public $allStateIds;

	public function printData()
	{
		echo "<br>EMAIL IDs:-> START <br>";
		echo "allEmails:-> ".$this->allEmails."<br>";
		echo "allStateIds:-> ".$this->allStateIds."<br>";
		echo "EMAIL IDs:-> END <br>";
	}	
}	

/* Information regarding to processing statistics.
	Used for retrieving data from table statistics */
class retrieveStatistic{
	public $value;//value
	public $valueCount;//count of this value rows

	public function printData()
	{
		echo "<br>RETRIEVESTATISTIC:-> START <br>";
		echo "value:-> ".$this->value."<br>";
		echo "valueCount:-> ".$this->valueCount."<br>";
		echo "RETRIEVESTATISTIC:-> END <br>";
	}	
}
	
	


/* Information related to the sale table */	
class sale{
	public $saleId;
	public $company;
	public $insuranceType;
	public $previousInsuranceType;
	public $coverageType;
	public $saleStartDate;
	public $saleEndDate;
	public $oldSaleStartDate;
	public $oldSaleEndDate;
	public $stateId;
	public $associate;
	public $status;
	
	public function printData()
	{
		echo "<br>SALE:-> START <br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "company:-> ".$this->company."<br>";
		echo "insuranceType:-> ".$this->insuranceType."<br>";
		echo "previousInsuranceType:-> ".$this->previousInsuranceType."<br>";
		echo "coverageType:-> ".$this->coverageType."<br>";
		echo "saleStartDate:-> ".$this->saleStartDate."<br>";
		echo "saleEndDate:-> ".$this->saleEndDate."<br>";
		echo "oldSaleStartDate:-> ".$this->oldSaleStartDate."<br>";
		echo "oldSaleEndDate:-> ".$this->oldSaleEndDate."<br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "associate:-> ".$this->associate."<br>";
		echo "status:-> ".$this->status."<br>";
		echo "SALE:-> END <br>";
	}
}

/* Information related to the ownerAddress table */	
class ownerAddress{
	public $addressId;
	public $addressType;
	public $street;
	public $areaCode;
	public $city;
	public $state;
	public $country;
	public $stateId;
	public $oldStateId;
		
	public function printData()
	{
		echo "<br>OWNERADDRESS:-> START <br>";
		echo "addressId:-> ".$this->addressId."<br>";
		echo "addressType:-> ".$this->addressType."<br>";
		echo "street:-> ".$this->street."<br>";
		echo "areaCode:-> ".$this->areaCode."<br>";
		echo "city:-> ".$this->city."<br>";
		echo "state:-> ".$this->state."<br>";
		echo "country:-> ".$this->country."<br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "oldStateId:-> ".$this->oldStateId."<br>";
		echo "OWNERADDRESS:-> END <br>";
	}
}


/* all data related to contract/sale as retrieved/send from the database tables.
filled by retrieveContractInfo function. */
class contract{
	
	public $insuranceCompanyOfferingQuote;
	public $contractType;//f.e INSURANCE, or 'SALE' etc
	public $quoteId;
	public $offerSelected;
	
	public $previousInsuranceType;//in case we change the type for an existing contract. i.e from MOTOR -> MEDICAL etc.
	public $associate;
	public $remainder;
	public $discount;
	
	public $owner;
	
	public $sale;
	
	public $drivers = array();
	
	public $addresses = array();
	
	public $motor;
	
	public $employersLiability;
	
	public $medical;
	
	public $lifeIns;
	
	public $propertyFire;
	public $producer;
	
	public function printData()
	{
		echo "<br>CONTRACT:-> START <br>";
		echo "insuranceCompanyOfferingQuote:-> ".$this->insuranceCompanyOfferingQuote."<br>";
		echo "contractType:-> ".$this->contractType."<br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "offerSelected:-> ".$this->offerSelected."<br>";
		echo "associate:-> ".$this->associate."<br>";
		echo "remainder:-> ".$this->remainder."<br>";
		echo "discount:-> ".$this->discount."<br>";
		
		if( $this->owner <> null )
			$this->owner->printData();
			
		if( $this->sale <> null )
			$this->sale->printData();
			
		foreach($this->drivers as $driver)
			$driver->printData();
		foreach($this->addresses as $address)
			$address->printData();
			
		if( $this->motor <> null )
			$this->motor->printData();
		if( $this->employersLiability <> null )
			$this->employersLiability->printData();
		if( $this->medical <> null )
			$this->medical->printData();
		if( $this->lifeIns <> null )
			$this->lifeIns->printData();
		if( $this->propertyFire <> null )
			$this->propertyFire->printData();
			
		echo "producer:-> ".$this->producer."<br>";
		echo "CONTRACT:-> END <br>";
	}
	
}

/* Information needed for inserting the MOTOR liability info. Corresponds to table  */
class motor{
	
	public $coverageType;
	
	public $license;
	
	public $vehicle;
	
	public $coveragesInPolicyArray = array();//not used for now
	
	public $drivingExperience;
		
	public function printData()
	{
		echo "<br>MOTOR INFO:-> START <br>";
		if( $this->license <> null )
			$this->license->printData();
			
		if( $this->vehicle <> null )
			$this->vehicle->printData();
			
		echo "coverageType:-> ".$this->coverageType."<br>";
		
		foreach($this->coveragesInPolicyArray as $eachCoverage)
			$eachCoverage->printData();
			
		if( $this->drivingExperience <> null )
			$this->drivingExperience->printData();
			
		echo "MOTOR INFO:-> END <br>";
	}	
}

/* all data relate to TRANSACTION table */
class transaction{
	public $transId;
	public $receiptNo;
	public $transDate;
	public $details;
	public $debit;
	public $credit;
	public $remainder;
	public $saleId;
	public $producer;
	
	public function printData()
	{
		echo "<br>TRANSACTION:-> START <br>";
		echo "transId:-> ".$this->transId."<br>";
		echo "receiptNo:-> ".$this->receiptNo."<br>";
		echo "transDate:-> ".$this->transDate."<br>";
		echo "details:-> ".$this->details."<br>";
		echo "debit:-> ".$this->debit."<br>";
		echo "credit:-> ".$this->credit."<br>";
		echo "remainder:-> ".$this->remainder."<br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "producer:-> ".$this->producer."<br>";
		echo "TRANSACTION:-> END <br>";
	}
	
}

/* company premiums production */
class production{
	public $parameter;//generic value. can get for company, associate or producer
	public $associate;
	public $producer;
	public $production;

	public function printData()
	{
		echo "<br>PRODUCTION:-> START <br>";
		echo "parameter:-> ".$this->parameter."<br>";
		echo "associate:-> ".$this->associate."<br>";
		echo "producer:-> ".$this->producer."<br>";
		echo "production:-> ".$this->production."<br>";
		echo "PRODUCTION:-> END <br>";
	}
}	
	
/* total production in one year, grouped per month */
class monthProductionPerYear{
	public $year;
	public $monthsArray = array();
	
	public function printData()
	{
		echo "<br>MONTHPRODUCTIONPERYEAR:-> START <br>";
		echo "year:-> ".$this->year."<br>";
		foreach( $this->monthsArray as $eachMonth )
			$eachMonth->printData();
		echo "MONTHPRODUCTIONPERYEAR:-> END <br>";
	}
}	

/* stores the month name and the production of that month */
class monthProduction{
	public $month;
	public $monthProduction;
	
	public function printData()
	{
		echo "<br>MONTHPRODUCTION:-> START <br>";
		echo "month:-> ".$this->month."<br>";
		echo "monthProduction:-> ".$this->monthProduction."<br>";
		echo "MONTHPRODUCTION:-> END <br>";
	}
}	



/* stores the notes retrieved from NOTES table */
class note{
	public $notesId;
	public $type;
	public $description;
	public $entryDate;
	public $parameterName;
	public $parameterValue;
	
	public function printData()
	{
		echo "<br>NOTE:-> START <br>";
		echo "notesId:-> ".$this->notesId."<br>";
		echo "type:-> ".$this->type."<br>";
		echo "description:-> ".$this->description."<br>";
		echo "entryDate:-> ".$this->entryDate."<br>";
		echo "parameterName:-> ".$this->parameterName."<br>";
		echo "parameterValue:-> ".$this->parameterValue."<br>";
		echo "NOTE:-> END <br>";
	}
}


/*stores the information related to PROPERTYFIRE and ENDORSEMENTS tables */
class propertyFire{
	public $saleId;
	public $oldSaleId;
	public $description;
	public $typeOfPremises;
	public $buildingValue;
	public $outsideFixturesValue;
	public $contentsValue;
	public $valuableObjectsValue;
	public $yearBuilt;
	public $areaSqMt;
	public $coveragesInPolicyArray = array();//not used for now
	public $endrosements = array();
	
	public function printData()
	{
		echo "<br>PROPERTYFIRE:-> START <br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "oldSaleId:-> ".$this->oldSaleId."<br>";
		echo "description:-> ".$this->description."<br>";
		echo "typeOfPremises:-> ".$this->typeOfPremises."<br>";
		echo "buildingValue:-> ".$this->buildingValue."<br>";
		echo "outsideFixturesValue:-> ".$this->outsideFixturesValue."<br>";
		echo "contentsValue:-> ".$this->contentsValue."<br>";
		echo "valuableObjectsValue:-> ".$this->valuableObjectsValue."<br>";
		echo "yearBuilt:-> ".$this->yearBuilt."<br>";
		echo "areaSqMt:-> ".$this->areaSqMt."<br>";
		foreach( $this->coveragesInPolicyArray as $eachCoverageInPolicy )
			$eachCoverageInPolicy->printData();
		foreach( $this->endrosements as $eachEndrosements )
			$eachEndrosements->printData();
		echo "PROPERTYFIRE:-> END <br>";
	}
	
}


/*stores the information related to ENDORSEMENT tables */
class endrosement{
	public $saleId;
	public $oldSaleId;
	public $description;
	public $code;
	public $parameter;
	
	public function printData()
	{
		echo "<br>ENDORSEMENT:-> START <br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "oldSaleId:-> ".$this->oldSaleId."<br>";
		echo "code:-> ".$this->code."<br>";
		echo "description:-> ".$this->description."<br>";
		echo "parameter:-> ".$this->parameter."<br>";
		echo "ENDORSEMENT:-> END <br>";
	}
	
}

/* Stores the information related to LIFEINS */
class lifeIns{
	public $saleId;
	public $oldSaleId;
	public $planName;
	public $insuredFirstName;
	public $insuredLastName;
	public $frequencyOfPayment;
	public $annualPremium;
	public $monthlyPremium;
	public $endorsement;
	public $basicPlanAmount;
	public $totalPermanentDisabilityAmount;
	public $premiumProtectionAmount;
	public $coveragesInPolicyArray = array();
	
	public function printData()
	{
		echo "<br>LIFE INS:-> START <br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "oldSaleId:-> ".$this->oldSaleId."<br>";
		echo "planName:-> ".$this->planName."<br>";
		echo "insuredFirstName:-> ".$this->insuredFirstName."<br>";
		echo "insuredLastName:-> ".$this->insuredLastName."<br>";
		echo "frequencyOfPayment:-> ".$this->frequencyOfPayment."<br>";
		echo "annualPremium:-> ".$this->annualPremium."<br>";
		echo "monthlyPremium:-> ".$this->monthlyPremium."<br>";
		echo "endorsement:-> ".$this->endorsement."<br>";
		echo "basicPlanAmount:-> ".$this->basicPlanAmount."<br>";
		echo "totalPermanentDisabilityAmount:-> ".$this->totalPermanentDisabilityAmount."<br>";
		echo "premiumProtectionAmount:-> ".$this->premiumProtectionAmount."<br>";
		foreach( $this->coveragesInPolicyArray as $eachCoverageInPolicy )
			$eachCoverageInPolicy->printData();
		echo "LIFE INS:-> END <br>";
	}
	
}


/* Stores the information related to MEDICAL and MEDICALINSUREDPERSONS tables */
class medical{
	public $saleId;
	public $oldSaleId;
	public $frequencyOfPayment;
	public $premium;
	public $planName;
	public $planMaximumLimit;
	public $deductible;
	public $excess;
	public $coInsurancePercentage;
	public $roomType;
	public $outpatientAmount;
	public $coveragesInPolicyArray = array();
	public $medicalInsuredPersonsArray = array();
	
	public function printData()
	{
		echo "<br>MEDICAL:-> START <br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "oldSaleId:-> ".$this->oldSaleId."<br>";
		echo "frequencyOfPayment:-> ".$this->frequencyOfPayment."<br>";
		echo "premium:-> ".$this->premium."<br>";
		echo "planName:-> ".$this->planName."<br>";
		echo "planMaximumLimit:-> ".$this->planMaximumLimit."<br>";
		echo "deductible:-> ".$this->deductible."<br>";
		echo "excess:-> ".$this->excess."<br>";
		echo "coInsurancePercentage:-> ".$this->coInsurancePercentage."<br>";
		echo "roomType:-> ".$this->roomType."<br>";
		echo "outpatientAmount:-> ".$this->outpatientAmount."<br>";
		echo "extentionOfCoverAbroad:-> ".$this->extentionOfCoverAbroad."<br>";
		foreach( $this->coveragesInPolicyArray as $eachCoverageInPolicy )
			$eachCoverageInPolicy->printData();
		foreach( $this->medicalInsuredPersonsArray as $eachMedicalInsuredPerson )
			$eachMedicalInsuredPerson->printData();
		echo "MEDICAL:-> END <br>";
	}
	
}




/*stores the information related to MEDICALINSUREDPERSONS table */
class medicalInsuredPerson{
	public $personId;
	public $saleId;
	public $oldSaleId;
	public $firstName;
	public $lastName;
	public $birthDate;
	public $telephone;
	public $gender;
	public $stateId;
	
	public function printData()
	{
		echo "<br>MEDICALINSUREDPERSONS:-> START <br>";
		echo "personId:-> ".$this->personId."<br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "oldSaleId:-> ".$this->oldSaleId."<br>";
		echo "firstName:-> ".$this->firstName."<br>";
		echo "lastName:-> ".$this->lastName."<br>";
		echo "birthDate:-> ".$this->birthDate."<br>";
		echo "telephone:-> ".$this->telephone."<br>";
		echo "gender:-> ".$this->gender."<br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "MEDICALINSUREDPERSONS:-> END <br>";
	}

}

/* stores a parameter name and value for searching in the database - used in the select statement */
class parameterNameValue{
	public $name;
	public $value;
	public $displayType;//display text box or select. see function displayElements
	
	public function printData()
	{
		echo "<br>PARAMETERNAMEVALUE:-> START <br>";
		echo "name:-> ".$this->name."<br>";
		echo "value:-> ".$this->value."<br>";
		echo "displayType:-> ".$this->displayType."<br>";
		echo "PARAMETERNAMEVALUE:-> END <br>";
	}
}

/* coverage included in policy or quotation.
One of saleId or quoteId is filled.
For saving in database tables COVERAGESINPOLICY, COVERAGESINQUOTATION */
class coveragesInPolicyQuotation{
	public $saleId;
	public $quoteId;
	public $insuranceType;
	public $code;
	public $default;
	public $parameters = array();
	public $selectionLevels = array();
	public $description;
	public $decision;//decision of processing. YES, NO, CANNOT_ADD(BENEFITS REQUESTED BUT NOT ADDED)
	public $unit;
	/* costs added to the quoteCost during processing.
	   no final costs, since some of these may be included in the packages already
	   cost > 0 => some cost is added
	   cost = 0, stillNeeded = 0 => no benefit added, no cost added
	   cost = 0, stillNeeded = -1 => benefit added, but without any cost,
	   stillNeeded = 1, benefit cannot be added  */
	public $stillNeeded;//for internal processing
	public $charge;//charge added for this coverage
	public $selectedCoverage;//for internal processing
	public $categoryType;//match with <category><type>. Can also be empty
	
	public function printData()
	{
		echo "<br>COVERAGES IN POLICY QUOTATION:-> START <br>";
		echo "saleId:-> ".$this->saleId."<br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "insuranceType:-> ".$this->insuranceType."<br>";
		echo "code:-> ".$this->code."<br>";
		echo "default:-> ".$this->default."<br>";
		foreach($this->parameters as $parameter)
			echo "parameter: ".$parameter."<br/>";
		foreach($this->selectionLevels as $selectionLevel)
			$selectionLevel->printData();
		echo "description:-> ".$this->description."<br>";
		echo "decision:-> ".$this->decision."<br>";
		echo "unit:-> ".$this->unit."<br>";
		echo "stillNeeded:-> ".$this->stillNeeded."<br>";
		echo "charge:-> ".$this->charge."<br>";
		echo "selectedCoverage:-> ".$this->selectedCoverage."<br>";
		echo "categoryType:-> ".$this->categoryType."<br>";
		echo "COVERAGES IN POLICY QUOTATION:-> END <br>";
	}
}


/* <coverage> as is Read/Written from the model.xml. */
class coveragesFromCompanyXML{
	public $code;
	public $applicable;
	//public $included;
	public $allowed;
	public $euro;
	public $percentCharge;
	public $param1;//f.e 23-70 with 2 years of license
	public $param2;//f.e 23-70 with 2 years of license
	public $param3;//f.e 23-70 with 2 years of license
	public $unit;//unit for categories. i.e years, euro, cubic centimeters
	public $categories = array();//may be empty
	public $typeCategories = array();//may be empty
	
	public function printData()
	{
		echo "<br>COVERAGES READ FROM COMPANY XML:-> START <br>";
		echo "code:-> ".$this->code."<br>";
		echo "applicable:-> ".$this->applicable."<br>";
		//echo "included:-> ".$this->included."<br>";
		echo "allowed:-> ".$this->allowed."<br>";
		echo "euro:-> ".$this->euro."<br>";
		echo "percentCharge:-> ".$this->percentCharge."<br>";
		echo "param1:-> ".$this->param1."<br>";
		echo "param2:-> ".$this->param2."<br>";
		echo "param3:-> ".$this->param3."<br>";
		echo "unit:-> ".$this->unit."<br>";
		foreach($this->categories as $category)
			$category->printData();
		foreach($this->typeCategories as $category)
			$category->printData();
		echo "COVERAGES READ FROM COMPANY XML:-> END <br>";
	}
}

/* additional mandatory charge in quotation, like for age, vehicle type, sports model, driving license.
Used for inserting in database table */
class chargesInQuotation{
	
	public $quoteId;
	public $code;
	public $charge;
	public $insuranceType;
	public $parameters = array();
	public $description;
	public $decision;
	public $categoryType;//match with <category><type>. Can also be empty
	
	public function printData()
	{
		echo "<br>CHARGES IN QUOTATION:-> START <br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "code:-> ".$this->code."<br>";
		echo "charge:-> ".$this->charge."<br>";
		echo "insuranceType:-> ".$this->insuranceType."<br>";
		foreach($this->parameters as $parameter)
			echo "parameter: ".$parameter."<br/>";
		echo "description:-> ".$this->description."<br>";
		echo "decision:-> ".$this->decision."<br>";
		echo "categoryType:-> ".$this->categoryType."<br>";
		echo "CHARGES IN QUOTATION:-> END <br>";
	}
}


/* any discounts applied to quotation. Used for inserting in database QUOTATION_DISCOUNTS table */
class quotation_discounts{
	
	public $quoteId;
	public $code;
	public $charge;
	public $insuranceType;
	public $parameters = array();
	public $description;
	public $decision;
	public $categoryType;//match with <category><type>. Can also be empty
	
	public function printData()
	{
		echo "<br>QUOTATION DISCOUNTS:-> START <br>";
		echo "quoteId:-> ".$this->quoteId."<br>";
		echo "code:-> ".$this->code."<br>";
		echo "charge:-> ".$this->charge."<br>";
		echo "insuranceType:-> ".$this->insuranceType."<br>";
		foreach($this->parameters as $parameter)
			echo "parameter: ".$parameter."<br/>";
		echo "description:-> ".$this->description."<br>";
		echo "decision:-> ".$this->decision."<br>";
		echo "categoryType:-> ".$this->categoryType."<br>";
		echo "QUOTATION DISCOUNTS:-> END <br>";
	}
}

/* additional charge as is Read/Written from the model.xml. */
class chargesFromCompanyXML{
	public $code;
	public $applicable;
	//public $included;
	public $allowed;
	public $euro;
	public $percentCharge;
	public $param1;//f.e 23-70 with 2 years of license
	public $param2;//f.e 23-70 with 2 years of license
	public $param3;//f.e 23-70 with 2 years of license
	public $unit;//unit for categories. i.e years, euro, cubic centimeters
	public $categories = array();//may be empty
	public $typeCategories = array();//may be empty
	
	public function printData()
	{
		echo "<br>CHARGES READ FROM COMPANY XML:-> START <br>";
		echo "code:-> ".$this->code."<br>";
		echo "applicable:-> ".$this->applicable."<br>";
		//echo "included:-> ".$this->included."<br>";
		echo "allowed:-> ".$this->allowed."<br>";
		echo "euro:-> ".$this->euro."<br>";
		echo "percentCharge:-> ".$this->percentCharge."<br>";
		echo "param1:-> ".$this->param1."<br>";
		echo "param2:-> ".$this->param2."<br>";
		echo "param3:-> ".$this->param3."<br>";
		echo "unit:-> ".$this->unit."<br>";
		foreach($this->categories as $category)
			$category->printData();
		foreach($this->typeCategories as $category)
			$category->printData();
		echo "CHARGES READ FROM COMPANY XML:-> END <br>";
	}
}


/* all data related to HISTORY table */
class history{
	public $historyId;
	public $transDate;
	public $username;
	public $type;
	public $subType;//not used now
	public $parameterName;
	public $parameterValue;
	public $note;
	
	public function printData()
	{
		echo "<br>HISTORY:-> START <br>";
		echo "historyId:-> ".$this->historyId."<br>";
		echo "transDate:-> ".$this->transDate."<br>";
		echo "username:-> ".$this->username."<br>";
		echo "type:-> ".$this->type."<br>";
		echo "subType:-> ".$this->subType."<br>";
		echo "parameterName:-> ".$this->parameterName."<br>";
		echo "parameterValue:-> ".$this->parameterValue."<br>";
		echo "note:-> ".$this->note."<br>";
		echo "HISTORY:-> END <br>";
	}
	
}

/* all data related to DRIVINGEXPERIENCE table */
class drivingExperience{
	public $stateId;
	public $oldStateId;
	public $hasPreviousInsurance;
	public $countryOfInsurance;
	public $insuranceCompany;
	public $yearsOfExperience;
	
	public function printData()
	{
		echo "<br>DRIVINGEXPERIENCE:-> START <br>";
		echo "stateId:-> ".$this->stateId."<br>";
		echo "oldStateId:-> ".$this->oldStateId."<br>";
		echo "hasPreviousInsurance:-> ".$this->hasPreviousInsurance."<br>";
		echo "countryOfInsurance:-> ".$this->countryOfInsurance."<br>";
		echo "insuranceCompany:-> ".$this->insuranceCompany."<br>";
		echo "yearsOfExperience:-> ".$this->yearsOfExperience."<br>";
		echo "DRIVINGEXPERIENCE:-> END <br>";
	}
	
}

/* store <database> from databasePatches.xml */
class database
{
	public $type;//type of database, client or global
	public $client;//name of client
	public $commands = array();//set of commands to execute
	
	public function printData()
	{
		echo "<br>DATABASE:-> START <br>";
		echo "type:-> ".$this->type."<br>";
		echo "client:-> ".$this->client."<br>";
		foreach($this->commands as $command)
			echo $command."<br/>";
		echo "DATABASE:-> END <br>";
	}
}

/* store <account> from accounts.xml */
class account
{
	public $clientName;//type of database, client or global
	//public $database;//name of client
	
	public function printData()
	{
		echo "<br>ACCOUNT:-> START <br>";
		echo "clientName:-> ".$this->clientName."<br>";
		//echo "database:-> ".$this->database."<br>";
		echo "ACCOUNT:-> END <br>";
	}
}

//When dealing with a dynamic number of field values while preparing a statement I find this class useful.
//Usage is pretty simple. Create an instance and use the add method to populate. When you're ready to execute simply use the get method. 
class BindParam{
    private $values = array(), $types = '';
   
    public function add( $type, &$value ){
        $this->values[] = $value;
        $this->types .= $type;
    }
   
    public function get(){
        return array_merge(array($this->types), $this->values);
    }
}
?>