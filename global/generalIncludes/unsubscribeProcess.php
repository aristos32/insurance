<?php

// Set flag that this is a parent file.
define('_INC', 1);

session_start();

require_once("../generalIncludes/definitions.php");
require_once("../generalIncludes/structures.php");
require_once("../database/connect.php");
require_once("../database/updateDatalayers.php");

$unsubscribeEmail = $_POST['unsubscribeEmail'];//to email

//echo $unsubscribeEmail;
//echo $_SESSION['unsubscribeStateId'];

$emailPreference = new emailPreference();
$emailPreference->stateId = $_SESSION['unsubscribeStateId'];
$emailPreference->preferenceCode = $EMAIL_PREFERENCE_CODE_NONE;
$emailPreference->preferenceFrequency = $FREQUENCY_MONTHLY;//for now, set default as monthly

updateEmailPreference($emailPreference);

echo "Email Unsubscribed Correctly";
