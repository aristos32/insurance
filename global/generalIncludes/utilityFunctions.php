<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

	
//ALL UTILITY FUNCTIONS OF GENERAL USAGE - generic functions that can be used from different fils

/*
When the contract number changes, need to update all the uploaded files related to it.
$oldSaleId : old contract number
$newSaleId : new contract number
*/
function renameUploadedFiles($oldSaleId, $newSaleId)
{
	$oldUploadedFiles = getListOfFiles($_SESSION['clientFilesLocation']."/uploadedFiles/", $oldSaleId."_");
	if(count($oldUploadedFiles)>0)
	{
		foreach($oldUploadedFiles as $eachExistingUploadedFiles)
		{
			$completeFilePath = $_SESSION['clientFilesLocation']."/uploadedFiles/".$eachExistingUploadedFiles;
			
			$charsToTrim = strlen($oldSaleId);//trim first characters
			$newName = $newSaleId.substr($eachExistingUploadedFiles, $charsToTrim);
			
			$newCompleteFilePath = $_SESSION['clientFilesLocation']."/uploadedFiles/".$newName;
			
			//echo "old file name: $completeFilePath, new name = $newCompleteFilePath, chartotrim=$charsToTrim <br/>";
			
			rename($completeFilePath, $newCompleteFilePath);
			
		}
	}
	
}


/* read all files in the $path and return their names as an array.
$dir: path to read
$fileNamePretext : the beginning of the file name. i.e 239kdkiie_ (usually the contract number )
				if this is empty, return all files
 */
function getListOfFiles($dir, $fileNamePretext)
{
        writeToFile(date("Y-m-d H:i:s"). ": getListOfFiles() ENTER. dir=$dir, fileNamePretext=$fileNamePretext \n", LOG_LEVEL_INFO);
        
	$listOfFiles = array();
	
	// Open a known directory, and proceed to read its contents
	if (is_dir($dir)) {
	    if ($dh = opendir($dir)) {
	        while (($file = readdir($dh)) !== false) {
		        if(is_file($dir . $file))
		        {
			        //file name pretext must match
			        if($fileNamePretext != "")
			        {
				        //position of string
				        $pos = strpos($file, $fileNamePretext );
				;
					writeToFile(date("Y-m-d H:i:s"). ": pos=$pos \n", LOG_LEVEL_INFO);
                                        
				        //pretext must be in the beginning of name
				        //see http://php.net/manual/en/function.strpos.php
				        if($pos !== false && $pos == 0 )
				        	$listOfFiles[] = $file;				        
			        }
			        //add all files
			        else
			        {
					writeToFile(date("Y-m-d H:i:s"). ": no pretext. adding all files... \n", LOG_LEVEL_INFO);
                                        
		        		$listOfFiles[] = $file;
	        		}
	        	}
	            //echo "filename: $file : filetype: " . filetype($dir . $file) . "\n";
	        }
	        closedir($dh);
	    }
	}	

	writeToFile(date("Y-m-d H:i:s"). ": getListOfFiles() EXIT \n", LOG_LEVEL_INFO);
        
	return $listOfFiles;
}


/* read all directories in the $path and return their names as an array.
$dir: path to read
 */
function getListOfDirectories($dir)
{
	writeToFile(date("Y-m-d H:i:s"). ": getListOfDirectories() ENTER. dir=$dir \n", LOG_LEVEL_INFO);
        
	$listOfDirs = array();
	
	// Open a known directory, and proceed to read its contents
	if (is_dir($dir)) {
	    if ($dh = opendir($dir)) {
	        while (($file = readdir($dh)) !== false) {
		        if(is_dir($dir . $file) && $file!='.' && $file!='..')
		        {
		       		$listOfDirs[] = $file;
	        	}
	            //echo "filename: $file : filetype: " . filetype($dir . $file) . "\n";
	        }
	        closedir($dh);
	    }
	}	

	writeToFile(date("Y-m-d H:i:s"). ": getListOfDirectories() EXIT \n", LOG_LEVEL_INFO);
        
	return $listOfDirs;
}

/* generate a random string.
	used for password generation.
	length= string length.
	randomString : the generated string */
function generateRandomString($length)
{
	$len = $length;
	$base='ABCDEFGHKLMNOPQRSTWXYZabcdefghjkmnpqrstwxyz123456789';
	$max=strlen($base)-1;
	$randomString='';
	mt_srand((double)microtime()*1000000);
	while (strlen($randomString)<$len+1)
	  $randomString.=$base{mt_rand(0,$max)};
	  
	return $randomString;
	
}


/* initvar() * Initialize variables * ? Amit Arora <digitalamit dot com>  * Permission give to use this code for Non-Commericial, 
Commericial use * It would be appreciated if you could provide a link to the site */ 
function initvar() { 
	foreach( func_get_args() as $v ) 
	{ 
		if( is_array( $v ) ) 
		{ 
			while( list( $key, $value ) = each( $v ) ) 
			{ 
				$GLOBALS[$key] = ( !isset($GLOBALS[$key]) ? $value : $GLOBALS[$key] ); 
			} 
		} 
		else 
		{ 
			$GLOBALS[$v] = ( !isset($GLOBALS[$v]) ? '' : $GLOBALS[$v] ); 
		} 
	} 
} 


/* print all debug messages
	that exist in $debugNotes array */
function printDebugMessages($debugNotes)
{
	foreach($debugNotes as $debug)
		echo $debug;

}

/* return the minimun of the 2 numbers */
function getMinimum($number1, $number2)
{
	if($number1<=$number2)
		return $number1;
	else
		return $number2;
}

/* return the minimun of the 2 numbers */
function getMaximum($number1, $number2)
{
	if($number1=='')
		return $number2;
	if($number2=='')
		return $number1;		
	if($number1>=$number2)
		return $number1;
	else
		return $number2;
}

/* Calculate the year difference of current date, from a given  date
 Example: getYearDifference("1986-06-18"); */
function getYearDifference($referenceDate)
{
        // Explode the date into meaningful variables
        list($referenceDateYear,$referenceDateMonth,$referenceDateDay) = explode("-", $referenceDate);

        // Find the differences
        $YearDiff = date("Y") - $referenceDateYear;
        $MonthDiff = date("m") - $referenceDateMonth;
        $DayDiff = date("d") - $referenceDateDay;

        // If the birthday has not occured this year
        if ($DayDiff < 0 || $MonthDiff < 0)
          $YearDiff--;

        return $YearDiff;
}

function formatDate( $dbDate )
{
	$year = substr( $dbDate, 0, 4 );
	$month = substr( $dbDate, 5, 2 );
	$date = substr( $dbDate, 8, 2 );
	$result = $date."-".$month."-".$year;
	return $result; 
}

/* send an email according to the input parameters
	subject: the subject line of the email
	message: the main message
	toEmail: actual email of the receiver
	senderName: what to show in the to: when we want privacy. Sometimes is the same as toEmail, sometimes is just a name
	$headers: additional header paremeters if needed */
function sendEmail($subject, $message, $toEmail, $senderName, $headers)
{
	$logMessage = date("Y-m-d H:i:s"). ": sendEmail() ENTER \n";
	$logMessage = date("Y-m-d H:i:s"). ": message=$message \n";
		
	//echo "toEmail=$toEmail";
	/*	
	$message = $message . '<br><br> If you wish to Unsubscribe from these emails, '.
	'press <a href=\"http://localhost:8080/global/office.php?email='.$toEmail.'"\>Unsubscribe</a>';
	*/
	
	$message = stripcslashes($message);

	//writeToFile(date("Y-m-d H:i:s"). ": message=$message \n", LOG_LEVEL_INFO);
        
	if(mail($senderName, $subject, $message, implode("\r\n", $headers))==false)
	{
        die("error sending email");//       writeToFile(date("Y-m-d H:i:s"). ": ERROR in sending the email \n", LOG_LEVEL_INFO);
	}

	//writeToFile(date("Y-m-d H:i:s"). ": sendEmail() EXIT \n", LOG_LEVEL_INFO);
}


/* send an auto reply email to the client.
	This is send in following case:
		1. the client send us a question from 'Contact Us' section.
		
	subject: the subject line of the email
	message: the main message
	email: the email of the receiver*/
function sendAutoReplyEmail($subject, $message, $email)
{
	$message = "\n".stripcslashes($message);
	
	$headers = 'From: www.Cyprus-Insurances.com' . "\r\n" .
    'Reply-To: noreply <noreply@www.Cyprus-Insurances.com>' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
	
	mail($email, $subject, $message, $headers);
	
}

/* check if number has more than 1 dots.
has > 1 dots => wrong => return false
has <= 1 dots => correct => return true
We need this to capture error which is missed from the javascript function validateOnRunTimeIsNumeric
because that function works on events
NOT USED NOW */
function checkIfNumberMoreThanOneDots($number)
{
	$firstCharacterPosition = stripos($number, ".");
	$lastCharacterPosition = strripos($number, ".");
	//only 1 or no exist exist
	if($firstCharacterPosition==$lastCharacterPosition)
		return true;
	//more than 1 dots exist
	else
		return false;
	
}

?>