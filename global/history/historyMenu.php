<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
?>
<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script>
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "5")

$(document).ready(function(){

	var today = new Date();
	
	var firstDayMonth = new Date(today.getFullYear(), today.getMonth(), 1);
	//firstDayMonth = 1 + '-' +  today.getMonth() + '-' + today.getFullYear();
	
	//alert(firstDayMonth);
	
    $("#historyStartDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#historyStartDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#historyStartDate").datepicker('setDate', firstDayMonth );

    
    $("#historyEndDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#historyEndDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#historyEndDate").datepicker('setDate', today );

          
});

</script>


<form name="historyMenuForm" action="./office.php" method="POST">
	<table>
	<input type="hidden" name="action" value="historyMenuProcess">
		<!-- USERNAME -->
		<tr>
			<td class="label"><?php echo $_SESSION['usernameTab']; ?>:</td>
			<td class="input"><input type="text" name="userName" id="userName" size="30" value="" />
		</tr>
		<!-- STATEID -->
		<tr>
			<td class="label"><?php echo $_SESSION['stateIdTab']; ?>:</td>
			<td class="input"><input type="text" name="stateId" id="stateId" size="30" value="" />
		</tr>
		<!-- CONTRACT NUMBER -->
		<tr>
			<td class="label"><?php echo $_SESSION['contractNumberTab']; ?>:</td>
			<td class="input"><input type="text" name="contractNumber" id="contractNumber" size="30" value="" />
		</tr>
		<tr>
			<td class="label"><?php echo $_SESSION['start']; ?>:</td>
			<td>
			<input type="text" style="width: 80px;" id="historyStartDate" name="historyStartDate" />
			</td>
		</tr>
	
		<tr>
			<td class="label"><?php echo $_SESSION['end']; ?>:</td>
			<td>
			<input type="text" style="width: 80px;" id="historyEndDate" name="historyEndDate" />
			</td>
		</tr>
		
		<tr>
			<td class="label"></td>
			<!-- CONTINUE BUTTON -->						
			<td class="input"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" /></td>
		</tr>
	</table>
</form>	


