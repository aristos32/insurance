/* add one charge in the table
tableId : table to add to
existingCodesListName : already existing codes in table, so we don't allow twice
*/
function addRowInAdditionalChargesTable(tableId, existingCodesListName, tagNamePrefix)
{
	try{
		
		var existingCodesArray = existingCodesListName.split( "," );
		
		//alert(existingCodesArray[0]);
		
		
	    var table = document.getElementById(tableId);
		var columnSize = "10";
	    var rowCount = table.rows.length;
	    var row = table.insertRow(rowCount);
		var element;
		
		//remove checkbox
	    var cell0 = row.insertCell(0);
	    element = document.createElement("input");
	    element.type = "checkbox";
	    element.name = "chkCoverages[]";
	    element.size = columnSize;
	    element.className = "co1Per";
	    cell0.appendChild(element);
	
	    //name
	    var cell1 = row.insertCell(1);
	    element = document.createTextNode("<?php echo $_SESSION['nameTab'].": "; ?>");
	    element.className = "co10Per";
	    cell1.appendChild(element);
	    var cell2 = row.insertCell(2);
	    var selector = document.createElement("select");
		selector.name = tagNamePrefix + "_CodeInc[]";
		
		cell2.appendChild(selector);
	    
	    <?php
			foreach($_SESSION['chargesDefinitions'] as $eachChargeDefinition)
			{
				//if($eachCharge->insuranceType == $INSURANCE_TYPE_MOTOR )
				//{
					?>
					var found = stringExistsInArray('<?php echo $eachChargeDefinition->code;?>', existingCodesArray);
					//if code is not found, then use for this list. We shouldn't add coverages when already exists
					if(found==0)
					{
						var option = document.createElement("option");
						option.value = '<?php echo $eachChargeDefinition->code;?>';
						//for chrome, need to define ALL variables with 'var'
						var name = document.createTextNode('<?php echo $eachChargeDefinition->code;?>');
						option.appendChild(name);
						selector.appendChild(option);
					}
				    <?php
			    //}
			}	
		 ?>
	 }
	 catch(e) {
        alert("Inside addRowInAdditionalChargesTable. Error:" + e);
    }
       
}
