<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
?> 

<script type="text/javascript">		


/* set the correct index of the select tag.
selectTagName :  as in html id */
function setPreviousValue(tagContent, selectTagName) 
{
	try{
		if( document.getElementById(selectTagName) )
		{
			for (var i = 0; i < document.getElementById(selectTagName).options.length; i++)
			{								
				if(document.getElementById(selectTagName).options[i].value == tagContent)
				{
					document.getElementById(selectTagName).selectedIndex = i;
					break;
				}
			}	
		}
	}	
	catch(e) {
        alert("Inside setPreviousValue. Error:" + e);
    }							
}



/* set the correct index of the offers radio button. */
function setOffers(offers_content) 
{
	var offersLength = document.getElementsByName('offers').length;
	for (var i = 0; i < offersLength; i++)
	{					
		if(document.getElementsByName('offers')[i].value == offers_content)
		{
			document.getElementsByName('offers')[i].checked = true;
			break;
		}
	}									
}

/* form adminForm */

/* set the correct index of the select tag.
	selectTag_content = YES or NO,
	tagName = the tag to modify.  */
function setSelectTag(selectTag_content, selectTagName) 
{
	var txt;
	
	//alert(selectTag_content + selectTagName);
	try{
		
		for (var i = 0; i < document.getElementById(selectTagName).options.length; i++)
		{								
			if(document.getElementById(selectTagName).options[i].value == selectTag_content)
			{
				document.getElementById(selectTagName).selectedIndex = i;
				break;
			}
		}									
	}
	catch(err)
	{
	  txt+="Error description: " + err.message + "\n\n";
	  txt+="selectTag_content=" + selectTag_content + "\n\n";
	   txt+="selectTagName=" + selectTagName + "\n\n";
	  alert(txt);
	}
}

/* show the hidden tags related to the coverage type. f.e for comprehensive, show amount, and all discounts
Applies to the quotation table. */
function processInsuranceType()
{
	processProposerType();//show appropriate tags pending on PERSON or COMPANY proposer type
	
	if(document.getElementById('insuranceType').value == "<?php echo $INSURANCE_TYPE_MOTOR; ?>")
	{
		document.getElementById("motor-info").style.display = 'table-row-group';
		//this may not exist in some screens, like in quotationTable
		if(document.getElementById("additional-drivers")!=null)
			document.getElementById("additional-drivers").style.display = 'table-row-group';
		document.getElementById("employer-liability-info").style.display = 'none';
		document.getElementById("lifeins-info").style.display = 'none';
		document.getElementById("medical-info").style.display = 'none';
		document.getElementById("property-fire-info").style.display = 'none';
		showBenefits();//to show or not the benefits. Timing issue? Benefits are not displayed.
	}
	else if(document.getElementById('insuranceType').value == "<?php echo $INSURANCE_TYPE_EMPLOYER_LIABILITY; ?>")
	{
		document.getElementById("motor-info").style.display = 'none';
		//this may not exist in some screens, like in quotationTable
		if(document.getElementById("motor-additional-benefits")!=null)
			document.getElementById("motor-additional-benefits").style.display = 'none';
		if(document.getElementById("additional-drivers")!=null)
			document.getElementById("additional-drivers").style.display = 'none';
		document.getElementById("employer-liability-info").style.display = 'table-row-group';
		document.getElementById("lifeins-info").style.display = 'none';
		document.getElementById("medical-info").style.display = 'none';
		document.getElementById("motor-license-info").style.display = 'none';
		document.getElementById("property-fire-info").style.display = 'none';
	}
	else if(document.getElementById('insuranceType').value == "<?php echo $INSURANCE_TYPE_MEDICAL; ?>")
	{
		document.getElementById("motor-info").style.display = 'none';
		//this may not exist in some screens, like in quotationTable
		if(document.getElementById("motor-additional-benefits")!=null)
			document.getElementById("motor-additional-benefits").style.display = 'none';
		if(document.getElementById("additional-drivers")!=null)
			document.getElementById("additional-drivers").style.display = 'none';
		document.getElementById("employer-liability-info").style.display = 'none';
		document.getElementById("lifeins-info").style.display = 'none';
		document.getElementById("medical-info").style.display = 'table-row-group';
		document.getElementById("motor-license-info").style.display = 'none';
		document.getElementById("property-fire-info").style.display = 'none';
		
	}
	else if(document.getElementById('insuranceType').value == "<?php echo $INSURANCE_TYPE_FIRE_PROPERTY; ?>")
	{
		document.getElementById("motor-info").style.display = 'none';
		//this may not exist in some screens, like in quotationTable
		if(document.getElementById("motor-additional-benefits")!=null)
			document.getElementById("motor-additional-benefits").style.display = 'none';
		if(document.getElementById("additional-drivers")!=null)
			document.getElementById("additional-drivers").style.display = 'none';
		document.getElementById("employer-liability-info").style.display = 'none';
		document.getElementById("lifeins-info").style.display = 'none';
		document.getElementById("medical-info").style.display = 'none';
		document.getElementById("motor-license-info").style.display = 'none';
		document.getElementById("property-fire-info").style.display = 'table-row-group';
	}
	else if(document.getElementById('insuranceType').value == "<?php echo $INSURANCE_TYPE_LIFE; ?>")
	{
		document.getElementById("motor-info").style.display = 'none';
		//this may not exist in some screens, like in quotationTable
		if(document.getElementById("motor-additional-benefits")!=null)
			document.getElementById("motor-additional-benefits").style.display = 'none';
		if(document.getElementById("additional-drivers")!=null)
			document.getElementById("additional-drivers").style.display = 'none';
		document.getElementById("employer-liability-info").style.display = 'none';
		document.getElementById("lifeins-info").style.display = 'table-row-group';
		document.getElementById("medical-info").style.display = 'none';
		document.getElementById("motor-license-info").style.display = 'none';
		document.getElementById("property-fire-info").style.display = 'none';
	}
	
	
}


/* show the appropriate tags related to the proposer type, depending if he is a PERSON or a COMPANY */
function processProposerType()
{
	if(document.getElementById('proposerType').value == "<?php echo $PROPOSER_TYPE_PERSON; ?>")
	{
		document.getElementById("proposer-person-info").style.display = 'table-row-group';
		document.getElementById("motor-license-info").style.display = 'table-row-group';
	}
	//else
	else if(document.getElementById('proposerType').value == "<?php echo $PROPOSER_TYPE_COMPANY; ?>")
	{
		document.getElementById("proposer-person-info").style.display = 'none';
		document.getElementById("motor-license-info").style.display = 'none';
	}
	
}

/* show the hidden tags when user wants to select individual benefits */
function showBenefits()
{
	var offerValue = 0;
	
	//check if elements exist. This function is called also from updateContraData.php, where they don't exist
	//set the correct value from the offers radio button.
	if( document.getElementById('offers_package') )
	{
		if( document.getElementById('offers_package').checked == true )
			offerValue = document.getElementById('offers_package').value;
		else 
			offerValue = document.getElementById('offers_Benefits').value;
	
		//alert(offerValue);
		
		
		//show optional benefits		
		if(offerValue == "<?php echo $OFFER_TYPE_THIRD_PARTY; ?>")
		{
			document.getElementById("motor-additional-benefits").style.display = 'table-row-group';
		}
		//hide optional benefits
		else
		{
			document.getElementById("motor-additional-benefits").style.display = 'none';
		}
	}	
	
}



</script>