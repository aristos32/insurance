<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
?> 

<script type="text/javascript">		

 var rightnow = new Date();
 var currentday = rightnow.getDate();
 var currentmonth = rightnow.getMonth();
 var currentyear = rightnow.getFullYear();

 
function errorStructure (complete, error) {
    this.complete = complete;
    this.error = error;
}

 /* check if the Update Password Form is valid */
function checkUpdatePasswordForm()
{
	try
	{
		var complete=true;
		var error = "Please correct following issues:\n";
		var password2 = document.updatePasswordForm.password2.value;
		var password3 = document.updatePasswordForm.password3.value;
		
		
		if(password2 != password3 )
		{
			error = error + "Passwords must match\n";
			complete = false;
		}
	}
	catch(e) {
        alert("Inside checkUpdatePasswordForm. Error:" + e);
    }
	
	if( complete == false )
		window.alert(error);	
    return complete;
	
}



/* check that the administrator menu form is valid, in administratorMenu.php */
function checkQuotationFindMenu()
{
	try{
		
		 var complete=true;
		 var error = "Please correct following issues:\n";	 
		 var quoteId = document.getElementById("quoteId").value;
		 var username = document.getElementById("username").value;
		 var firstName = document.getElementById("firstName").value;
		 var lastName = document.getElementById("lastName").value;
		 
		 if( firstName=="" && lastName=="" && quoteId=="" && username=="" )
		 {
			 error = error + "Please Insert At Least One Search Criteria \n";
			 complete = false;
		 }
	 }
	 catch(e) {
        alert("Inside checkQuotationFindMenu. Error:" + e);
    }
 
 if( complete == false )
	window.alert(error);	
	
 return complete;
}
 

/* check that the administrator menu form is valid, in administratorMenu.php */
function checkInsCompanyXMLForm()
{
	try{
		
	 var complete=true;
	 var error = "Please correct following issues:\n";	 
	 var fileName = document.getElementById("fileName").value;
	 
	 if( fileName==""  )
	 {
		 error = error + "File Name Cannot be empty \n";
		 complete = false;
	 }
    }
    catch(e) {
        alert("Inside checkInsCompanyXMLForm. Error:" + e);
    }
 
 if( complete == false )
	window.alert(error);	
	
 return complete;
}

/* validate the fields in the coverage type form */
function checkCoverageTypeForm(allTableIdsofCoverages)
{
	try
	{
		
		 var complete=true;
		 var error = "Please correct following issues:\n";	 
		 var minimumChargeEuro = document.getElementById("minimumChargeEuro").value;
		 var excessEuro = document.getElementById("excessEuro").value;
		 
		 var allTableIdsofCoveragesArray = allTableIdsofCoverages.split( "," );
		 
		 for (var i = 0; i < allTableIdsofCoveragesArray.length; i++) 
		 {
			if(validateTableCategories(allTableIdsofCoveragesArray[i]).complete==false)
			{
				error = error + validateTableCategories(allTableIdsofCoveragesArray[i]).error;
				complete = false;
			}
				
		    
		    //Do something
		 }
		 
	 }
	 
	 catch(e) {
        alert("Inside checkCoverageTypeForm. Error:" + e);
    }	
	 
	 if( complete == false )
		window.alert(error);	
		
	 return complete;
}


/* validate the fields in the coverage type form */
function checkPackageForm(allTableIdsofCoverages)
{
	try
	{
		
		 var complete=true;
		 var error = "Please correct following issues:\n";	 
		 var chargeOverBasicName = document.getElementById("chargeOverBasicName").value;
		 
		 var allTableIdsofCoveragesArray = allTableIdsofCoverages.split( "," );
		 
		 for (var i = 0; i < allTableIdsofCoveragesArray.length; i++) 
		 {
			if(validateTableCategories(allTableIdsofCoveragesArray[i]).complete==false)
			{
				error = error + validateTableCategories(allTableIdsofCoveragesArray[i]).error;
				complete = false;
			}
				
		    
		    //Do something
		 }
		 
	 }
	 
	 catch(e) {
        alert("Inside checkPackageForm. Error:" + e);
    }	
	 
	 if( complete == false )
		window.alert(error);	
		
	 return complete;
}

/* validate the fields in the coverage type form */
function checkOptionalCoverages(allTableIdsofCoverages)
{
	try
	{
		
		 complete=true;
		 error = "Please correct following issues:\n";	 
		 
		 var allTableIdsofCoveragesArray = allTableIdsofCoverages.split( "," );
		 
		 for (var i = 0; i < allTableIdsofCoveragesArray.length; i++) 
		 {
			if(validateTableCategories(allTableIdsofCoveragesArray[i]).complete==false)
			{
				error = error + validateTableCategories(allTableIdsofCoveragesArray[i]).error;
				complete = false;
			}
				
		    
		    //Do something
		 }
		 
	 }
	 
	 catch(e) {
        alert("Inside checkOptionalCoverages. Error:" + e);
    }	
	 
	 if( complete == false )
		alert(error);	
		
	 return complete;
}


/*
	Validate the categories of this table. Can be genericCategory or typeCategory
	Table usually referes to a coverage, like ageCharges, personalAccidents etc

*/
function validateTableCategories(tableId)
{
	try
	{
		var error = new errorStructure(true, "");
		//alert(tableId);
		//this.complete = true;
	    //this.error = "";
		var table = document.getElementById(tableId);
		
		//some coverages may have been deleted already
		if(table!=null)
		{
		    var rowCount = table.rows.length;
		    var currentFromValue = 0;
			var currentToValue = 0;
			var currentTypeValue = '';//should compare categories of the same type
		    var previousFromValue = -1;
			var previousToValue = -1;
			var previousTypeValue = "";
		
			
			if(rowCount>0)
			{
				var cellCount = 0;
				cellCount = table.rows[0].cells.length;
				//alert(cellCount);
						
				//generic Category has more elements than normal category
				if(cellCount > 11 )
				{
					//validate numeric values
				    for(var i=0; i<rowCount; i++) 
				    {
				        var row = table.rows[i];
				        cellCount = row.cells.length;
				        //existing categories created with html code, have 3 childNodes
				        if(row.cells[2].childNodes.length>1)
				        {
				        	var from = row.cells[2].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
				        	var to = row.cells[4].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
							var euro = row.cells[6].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
							var percentCharge = row.cells[8].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
							var minimumCharge = row.cells[11].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
						}
				        //categories newly created with javascript, have only 1 childNodes which is the actual html input element
				        else
				        {
				        	var from = row.cells[2].childNodes[0].value;
				        	var to = row.cells[4].childNodes[0].value;
							var euro = row.cells[6].childNodes[0].value;
							var percentCharge = row.cells[8].childNodes[0].value;
							var minimumCharge = row.cells[11].childNodes[0].value;
			        	}
						
				        
				        if(!checkIfNumberMoreThanOneDots(from))
						{
						   error.error = error.error + "From Cannot contain 2 dots \n";
						   error.complete = false;
						}
						if(!checkIfNumberMoreThanOneDots(to))
						{
						   error.error = error.error + "To Cannot contain 2 dots \n";
						   error.complete = false;
						}
						if(!checkIfNumberMoreThanOneDots(euro))
						{
						   error.error = error.error + "euro Cannot contain 2 dots \n";
						   error.complete = false;
						}
						if(!checkIfNumberMoreThanOneDots(percentCharge))
						{
						   error.error = error.error + "percentCharge Cannot contain 2 dots \n";
						   error.complete = false;
						}
						if(!checkIfNumberMoreThanOneDots(minimumCharge))
						{
						   error.error = error.error + "minimumCharge Cannot contain 2 dots \n";
						   error.complete = false;
						}
						//alert(from);
					}
					
					if(error.complete == true)
					{
						//validate that categories ranges are valid
						for(var i=0; i<rowCount; i++) 
					    {
					        var row = table.rows[i];
					         //existing categories created with html code, have 3 childNodes
				        	if(row.cells[2].childNodes.length>1)
				        	{
					        	currentFromValue = parseInt(row.cells[2].childNodes[1].value, 10);//first child is actually the <td>!, second the value, third the </td>!
								currentToValue = parseInt(row.cells[4].childNodes[1].value, 10);//first child is actually the <td>!, second the value, third the </td>!
							}
							//categories newly created with javascript, have only 1 childNodes which is the actual html input element
							else
							{
					        	currentFromValue = parseInt(row.cells[2].childNodes[0].value, 10);
								currentToValue = parseInt(row.cells[4].childNodes[0].value, 10);
							}
					        
							//ALERT NOT TESTED
							//if variable is set, then we read it 
							if (row.cells[12])
							{
								if(row.cells[12].childNodes.length>1)
				        		{
									currentTypeValue = row.cells[12].childNodes[1].value;
								}
								else
									currentTypeValue = row.cells[12].childNodes[0].value;
							}
							
							//first repetition - set previous values
							if(previousFromValue==-1 && previousToValue==-1)
							{
								;//do nothing - its the first repetition
							}
							//other repetition 
							else
							{
								//only compare similar types, f.e THIRD_PARTY, COMPREHENSIVE etc
								if(currentTypeValue == previousTypeValue )
								{
									//previous 'to' value must be smaller or equal to the current 'from' value
									if(previousToValue > currentFromValue )
									{
										error.error = error.error + "Field 'From' of a category cannot be smaller than field 'To' of previous category \n";
						   				error.complete = false;
									}
								}
								
							}
							
							//same category - from >= to 
							if(currentFromValue===null || currentFromValue==="" || isNaN(currentFromValue) || currentToValue===null || currentToValue==="" || isNaN(currentToValue) )
							{
								error.error = error.error + " tableId=" + tableId + ",'From' and 'To' are mandatory for each category \n";
						   		error.complete = false;
							}
							//same category - from >= to - both from and to have values
							else 
							{
								if(currentFromValue >= currentToValue )
								{
									error.error = error.error + "Field 'From' must be smaller than 'To' for same category \n";
						   			error.complete = false;
								}
							}
										
							//update values for next repetition
							previousFromValue = currentFromValue;
							previousToValue = currentToValue;
							previousTypeValue = currentTypeValue;
					       
						}
							//alert(from);
					}
					
				}//if(cellCount > 11 )
				
				//type Category has less elements than generic category
				else 
				{
					//validate numeric values
				    for(var i=0; i<rowCount; i++) 
				    {
				        var row = table.rows[i];
				        //existing categories created with html code, have 3 childNodes
				        if(row.cells[2].childNodes.length>1)
				        {
				        	var type = row.cells[2].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
							var euro = row.cells[4].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
							var percentCharge = row.cells[6].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
							var minimumCharge = row.cells[9].childNodes[1].value;//first child is actually the <td>!, second the value, third the </td>!
						}
				        //categories newly created with javascript, have only 1 childNodes which is the actual html input element
				        else
				        {
				        	var type = row.cells[2].childNodes[0].value;
							var euro = row.cells[6].childNodes[0].value;
							var percentCharge = row.cells[6].childNodes[0].value;
							var minimumCharge = row.cells[9].childNodes[0].value;
			        	}
						
				        
				        if(type=='')
						{
						   error.error = error.error + "Category Type cannot be empty \n";
						   error.complete = false;
						}
						
						if(!checkIfNumberMoreThanOneDots(euro))
						{
						   error.error = error.error + "euro Cannot contain 2 dots \n";
						   error.complete = false;
						}
						if(!checkIfNumberMoreThanOneDots(percentCharge))
						{
						   error.error = error.error + "percentCharge Cannot contain 2 dots \n";
						   error.complete = false;
						}
						if(!checkIfNumberMoreThanOneDots(minimumCharge))
						{
						   error.error = error.error + "minimumCharge Cannot contain 2 dots \n";
						   error.complete = false;
						}
						//alert(from);
					}
				}//else
				
			}//if(rowCount>0)
			
		}//if(table!=null)
		
	}//try
	catch(e) 
	{
        alert("Inside validateTableCategories. tableId=" + tableId + ", i=" + i + ",Error:" + e);
    }
	return error;
}


/* check if number has more than 1 dots.
has > 1 dots => wrong => return false
has <= 1 dots => correct => return true
We need this to capture error which is missed from the javascript function validateOnRunTimeIsNumeric
because that function works on events */
function checkIfNumberMoreThanOneDots(stringName)
{
	try
	{
		var firstCharacterPosition = stringName.indexOf(".");
		var lastCharacterPosition = stringName.lastIndexOf(".");
		//only 1 or no exist exist
		if(firstCharacterPosition==lastCharacterPosition)
			return true;
		//more than 1 dots exist
		else
			return false;
	}
	catch(e) {
        alert("Inside checkIfNumberMoreThanOneDots. Error:" + e);
    }
	
}

/* check if number has more than 1 dots.
has > 1 dots => wrong => return false
has <= 1 dots => correct => return true
We need this to capture error which is missed from the javascript function validateOnRunTimeIsNumeric
because that function works on events */
function checkIfNumberMoreThanOneDots2(id)
{
	try
	{
		var stringName = document.getElementById(id).value;
		var firstCharacterPosition = stringName.indexOf(".");
		var lastCharacterPosition = stringName.lastIndexOf(".");
		//only 1 or no exist exist
		if(firstCharacterPosition==lastCharacterPosition)
			return true;
		//more than 1 dots exist
		else
		{
			alert("More than one dots are not allowed");
			txtBox=document.getElementById(id);
			if(txtBox!=null)
			{
				setTimeout("txtBox.focus();",1);
				setTimeout("txtBox.select();",1);
				//txtBox.focus();
				//txtBox.select();
			}
			return false;
		}
	}
	catch(e) {
        alert("Inside checkIfNumberMoreThanOneDots. Error:" + e);
    }
	
}

/* check that the administrator menu form is valid, in administratorMenu.php */
function checkFindUserFormForm()
{
	try
	{
		 var complete=true;
		 var error = "Please correct following issues:\n";	 
		 var userName = document.findUserForm.userName.value;
	 }
	 catch(e) {
        alert("Inside checkFindUserFormForm. Error:" + e);
    }

 if( userName=="" )
 {
	 error = error + "Please Fill User Name \n";
	 complete = false;
 }
 
 if( complete == false )
	window.alert(error);	
	
 return complete;
}

/* check that the find contract menu form is valid, in findContractForm.php */
function checkFindContractFormForm()
{
	try
	{
		var complete=true;
		var error = "Please correct following issues:\n";	 
		var contractNumber = document.getElementById("contractNumber").value;
		var regNumber = document.getElementById("regNumber").value;
		var insuranceCompany = document.getElementById("insuranceCompany").value;
		var stateId = document.getElementById("stateId").value;
		
		if( contractNumber=="" && regNumber=="" && (insuranceCompany=='' || insuranceCompany=='NONE') && stateId=="")
		{
		 error = error + "You need to fill at least one search value \n";
		 complete = false;
		}
	}
	catch(e) {
        alert("Inside checkFindContractFormForm. Error:" + e);
    }
	
	if( complete == false )
	window.alert(error);	
	
	return complete;
}
 
 /* check that the contact us form is valid */
 function checkContactUsForm()
 {
	 try{
		 
		 var complete=true;
		 var error = "Please correct following issues:\n";	 
		 var visitorName = document.getElementById("visitorName").value;
		 var visitormail = document.getElementById("visitormail").value;
		 var message = document.getElementById("message").value;
	  
		 if( visitorName=="" )
		 {
			 error = error + "Please Insert Your Name \n";
			 complete = false;
		 }
		 
		 if( visitormail=="" )
		 {
			 error = error + "Please Insert Your Email \n";
			 complete = false;
		 }
		 
		 if( message=="" )
		 {
			 error = error + "Please Insert Your Message \n";
			 complete = false;
		 }
	 }
	 catch(e) {
        alert("Inside checkContactUsForm. Error:" + e);
    }
	 
	 if( complete == false )
		window.alert(error);	
		
	 return complete;
 }
 
 
 /* check if the login form is valid */
 function checkLoginForm()
 {	
	 try
	 {
		 
		 var complete=true;
		 var thename=document.getElementById("username").value;
		 var thepassword = document.getElementById("password").value;
		 
		 if(thename=="" || thepassword=="")
		 {
			 alert("Please Insert All Fields");
			 complete = false;
		 }
	 }
	 catch(e) {
        alert("Inside checkLoginForm. Error:" + e);
    }
	 
	 return complete;
 }
 
 
 /* check if the insertUserName form is valid */
 function checkInsertUserNameForm()
 {	
	 try
	 {
		 
		 var complete=true;
		 var thename=document.insertUserName.username.value;
		 
		 if(thename=="")
		 {
			 alert("Please Insert the User Name");
			 complete = false;
		 }
	 }
	 catch(e) {
        alert("Inside checkInsertUserNameForm. Error:" + e);
    }
	 
	 return complete;
 }
 

 
 /* check if the Quotation Form is valid */
 function checkQuotationForm()
 {	
	 try{
		 
		 //alert("inside checkQuotationForm");
		 
		 var complete=true;
		 var error = "Please correct following issues:\n";
		 var firstName=document.getElementById('firstName').value;
		 var lastName=document.getElementById('lastName').value;
		 var ownerProfession=document.getElementById('ownerProfession').value;
		 var cubicCapacity = document.getElementById('cubicCapacity').value;
		 var vehicleMake = document.getElementById('vehicleMake').value;
		 var vehicleModel = document.getElementById('vehicleModel').value;
		 var maxPenaltyPointsOfAnyDriverDuringLastThreeYears = document.getElementById('maxPenaltyPointsOfAnyDriverDuringLastThreeYears').value;
		 var numberOfAccidentsOfAnyDriverDuringLastThreeYears = document.getElementById('numberOfAccidentsOfAnyDriverDuringLastThreeYears').value;
		 var ownerBirthDate = document.getElementById('ownerBirthDate').value;
		 var coverageType = document.getElementById('coverageType').value;
		 var coverageAmount = document.getElementById('coverageAmount').value;
		 var stateId = document.getElementById('stateId').value;
		 var regNumber = document.getElementById('regNumber').value;
		 
		 var telephone = document.getElementById('telephone').value;
		 var cellphone = document.getElementById('cellphone').value;
		 
		 //php:true -> 1, run from global/quotation.php
		 //php:false -> 0, run from global/office.php
		 var runFromGlobalLocation = "<?php echo $runFromGlobalLocation; ?>";
		 var runFromGlobalLocationOffice = "<?php echo $runFromGlobalLocationOffice; ?>";
		 
		 //alert(runFromGlobalLocationOffice);
		 
		 var insuranceType = document.getElementById('insuranceType').value;
		 var proposerType = document.getElementById('proposerType').value;
		 
		 //alert(insuranceType);
		 //alert(proposerType);
		 
		 var i;
		 
		
		 if(firstName=="" )
		 {
			error = error + "Insert the First Name \n";
			complete = false;
		 }
		 
		 if(lastName=="" )
		 {
			error = error + "Insert the Surname \n";
			complete = false;
		 }
		 
		 if(stateId=="" )
		 {
			error = error + "Insert the State Id \n";
			complete = false;
		 }
		 
		 if(ownerProfession=="" )
		 {
			error = error + "Insert the Profession \n";
			complete = false;
		 }
		 
		  if(cellphone=="" && telephone=="")
		 {
			error = error + "Insert at least one telephone number \n";
			complete = false;
		 }
		
		 if(telephone!="")
		 {
			 if( !IsNumeric(telephone)){
				error = error + "Telephone should be filled with a positive numeric value\n";
				complete = false;
			}
		 }
		  
		 if(cellphone!="")
		 {
			 if( !IsNumeric(cellphone)){
				error = error + "Cellphone should be filled with a positive numeric value\n";
				complete = false;
			}
		 }
		 
		 
		 
		 //check these fields only for MOTOR
		 if(insuranceType=="<?php echo $INSURANCE_TYPE_MOTOR; ?>")
		 {
			if(proposerType == "<?php echo $PROPOSER_TYPE_PERSON; ?>")
			{
				 if ( checkBirthDate(ownerBirthDate)==false)
				 {
					 error = error + "The Age of the driver is less than 18 years old \n";
					 complete = false;
				 }
			}
			 
			 //when coverage type is comprehensive, coverage amount should be filled
			 if(coverageType=="<?php echo $COVERAGE_TYPE_FIRE_AND_THIFT; ?>" || coverageType=="<?php echo $COVERAGE_TYPE_COMPREHENSIVE; ?>")
			 {
				 if(coverageAmount=="")
				 {
					error = error + "Coverage Amount should be filled with a positive numeric value\n";
					complete = false;
				}
				else{
					if( !IsNumeric(coverageAmount) || coverageAmount<=0 ){
						error = error + "Coverage Amount should be filled with a positive numeric value\n";
						complete = false;
					}
				}		
			 }
			 
			//validate that all accidents claims values were filled with numeric integers
			for(i=0; i<numberOfAccidentsOfAnyDriverDuringLastThreeYears; i++)
			{
				var tagName = "claim" + (i+1);
				var tagValue = document.getElementById("claim" + (i+1)).value;
				//alert(tagValue);
				if( tagValue=="" ){
					error = error + "All Claims should be filled with a positive numeric value\n";
					//alert(error);
					complete = false;
					break;
				}
				else{
					if( !IsNumeric(tagValue)){
						error = error + "All Claims should be filled with a positive numeric value\n";
						//alert(error);
						complete = false;
						break;
					}
					else if(tagValue<=0){
						error = error + "All Claims should be filled with a positive numeric value\n";
						//alert(error);
						complete = false;
						break;
					}
				}		
			}
			 
			 
			
			 if(regNumber=="" )
			 {
				error = error + "Insert the Registration Number \n";
				complete = false;
			 }
			 
		
			 if(vehicleModel=="" )
			 {
				error = error + "Insert the Vehicle Model \n";
				complete = false;
			 }
			 
			 
			 if(vehicleMake=="" )
			 {
				error = error + "Insert the Vehicle Make \n";
				complete = false;
			 }
			 
			 if(cubicCapacity=="")
			  {
				error = error + "Insert the Cubic Capacity \n";
				complete = false;
			 }
			 else{
				 if( !IsNumeric(cubicCapacity)){
					 error = error + "The Cubic Capacity should be a number \n";
					 complete = false;
				 }
				 else if(cubicCapacity<=0){
					 error = error + "The Cubic Capacity cannot be zero or Negative \n";
					 complete = false;
				 }
			 }
			 
			 
			  if(maxPenaltyPointsOfAnyDriverDuringLastThreeYears=="")
			  {
				error = error + "Insert the Maximum Penalty Points \n";
				complete = false;
			 }
			 else{
				 if( !IsNumeric(maxPenaltyPointsOfAnyDriverDuringLastThreeYears)){
					 error = error + "The Maximum Penalty Points should be a number \n";
					 complete = false;
				 }
				 else if(maxPenaltyPointsOfAnyDriverDuringLastThreeYears<0){
					 error = error + "The Maximum Penalty Points cannot be Negative \n";
					 complete = false;
				 }
				 else if(maxPenaltyPointsOfAnyDriverDuringLastThreeYears>=12){
					 error = error + "The Maximum Penalty Points cannot be more than 11 \n";
					 complete = false;
				 }
			 }
			 
			 if(numberOfAccidentsOfAnyDriverDuringLastThreeYears=="")
			  {
				error = error + "Insert the Number of Accidents \n";
				complete = false;
			 }
			 else{
				 if( !IsNumeric(numberOfAccidentsOfAnyDriverDuringLastThreeYears)){
					 error = error + "The Number of Accidents should be a number \n";
					 complete = false;
				 }
				 else if(numberOfAccidentsOfAnyDriverDuringLastThreeYears<0){
					 error = error + "The Number of Accidents cannot be Negative \n";
					 complete = false;
				 }
			 }
			
		 }
	 }
	 catch(e) {
        alert("Inside checkQuotationForm. Error:" + e);
    }
	 
	
	 if( complete == false )
		window.alert(error);	
		
	 return complete;
 }// function checkQuotationForm()
 
 
  /* check if the New Contract Form is valid */
 function checkNewContractForm()
 {	
	 try
	 {
		 
		 //alert("inside checkNewContractForm");
		 
		 var complete=true;
		 var error = "Please correct following issues:\n";
		 var contractNumber = document.getElementById('contractNumber').value;
		 var firstName=document.getElementById('firstName').value;
		 var lastName=document.getElementById('lastName').value;
		 var ownerProfession=document.getElementById('ownerProfession').value;
		 var cubicCapacity = document.getElementById('cubicCapacity').value;
		 var vehicleMake = document.getElementById('vehicleMake').value;
		 var vehicleModel = document.getElementById('vehicleModel').value;
		 var ownerBirthDate = document.getElementById('ownerBirthDate').value;
		 var coverageType = document.getElementById('coverageType').value;
		 var coverageAmount = document.getElementById('coverageAmount').value;
		 var stateId = document.getElementById('stateId').value;
		 var regNumber = document.getElementById('regNumber').value;
		 
		 var telephone = document.getElementById('telephone').value;
		 var cellphone = document.getElementById('cellphone').value;
		 
		 //employer liability
		 var employersSocialInsuranceNumber = document.getElementById('employersSocialInsuranceNumber').value;
		 var limitPerEmployee = document.getElementById('limitPerEmployee').value;
		 var limitPerEventOrSeriesOfEvents = document.getElementById('limitPerEventOrSeriesOfEvents').value;
		 var limitDuringPeriodOfInsurance = document.getElementById('limitDuringPeriodOfInsurance').value;
		 var employeesNumber = document.getElementById('employeesNumber').value;
		 var estimatedTotalGrossEarnings = document.getElementById('estimatedTotalGrossEarnings').value;
		 
		 //medical
		 var planName = document.getElementById('planName').value;
		 var maximumLimit = document.getElementById('planMaximumLimit').value;
		 var deductible = document.getElementById('deductible').value;
		 var coInsurancePercentage = document.getElementById('coInsurancePercentage').value;
		 var premium = document.getElementById('premium').value;
		 
		  //life
		 var lifeInsPlanName = document.getElementById('lifeInsPlanName').value;
		 var insuredFirstName = document.getElementById('insuredFirstName').value;
		 var insuredLastName = document.getElementById('insuredLastName').value;
		 var basicPlanAmount = document.getElementById('basicPlanAmount').value;
		 var totalPermanentDisabilityAmount = document.getElementById('totalPermanentDisabilityAmount').value;
		 var premiumProtectionAmount = document.getElementById('premiumProtectionAmount').value;
		 
		 
		 //property fire and theft
		 var buildingValue = document.getElementById('buildingValue').value;
		 var outsideFixturesValue = document.getElementById('outsideFixturesValue').value;
		 var contentsValue = document.getElementById('contentsValue').value;
		 var valuableObjectsValue = document.getElementById('valuableObjectsValue').value;
		 var areaSqMt = document.getElementById('areaSqMt').value;
		 
		 //php:true -> 1, run from global/quotation.php
		 //php:false -> 0, run from global/office.php
		 var runFromGlobalLocation = "<?php echo $runFromGlobalLocation; ?>";
		 var runFromGlobalLocationOffice = "<?php echo $runFromGlobalLocationOffice; ?>";
		 
		 //alert(runFromGlobalLocationOffice);
		 
		 var insuranceType = document.getElementById('insuranceType').value;
		 var proposerType = document.getElementById('proposerType').value;
		 
		 //alert(insuranceType);
		 //alert(proposerType);
		 
		 var i;
		 
		 if(contractNumber=="" )
		 {
			error = error + "Insert the Contract Number \n";
			complete = false;
		 }
		 
		 if(firstName=="" )
		 {
			error = error + "Insert the First Name \n";
			complete = false;
		 }
		 
		 if(lastName=="" )
		 {
			error = error + "Insert the Surname \n";
			complete = false;
		 }
		 
		 if(stateId=="" )
		 {
			error = error + "Insert the State Id \n";
			complete = false;
		 }
		 
		 if(ownerProfession=="" )
		 {
			error = error + "Insert the Profession \n";
			complete = false;
		 }
		 
		  if(cellphone=="" && telephone=="")
		 {
			error = error + "Insert at least one telephone number \n";
			complete = false;
		 }
		
		 if(telephone!="")
		 {
			 if( !IsNumeric(telephone)){
				error = error + "Telephone should be filled with a positive numeric value\n";
				complete = false;
			}
		 }
		  
		 if(cellphone!="")
		 {
			 if( !IsNumeric(cellphone)){
				error = error + "Cellphone should be filled with a positive numeric value\n";
				complete = false;
			}
		 }
		 
		 
		 
		 //check these fields only for MOTOR
		 if(insuranceType=="<?php echo $INSURANCE_TYPE_MOTOR; ?>")
		 {
			if(proposerType == "<?php echo $PROPOSER_TYPE_PERSON; ?>")
			{
				 if ( checkBirthDate(ownerBirthDate)==false)
				 {
					 error = error + "The Age of the driver is less than 18 years old \n";
					 complete = false;
				 }
			}
			 
			 //when coverage type is comprehensive, coverage amount should be filled
			 if(coverageType=="<?php echo $COVERAGE_TYPE_FIRE_AND_THIFT; ?>" || coverageType=="<?php echo $COVERAGE_TYPE_COMPREHENSIVE; ?>")
			 {
				 if(coverageAmount=="")
				 {
					error = error + "Coverage Amount should be filled with a positive numeric value\n";
					complete = false;
				}
				else{
					if( !IsNumeric(coverageAmount) || coverageAmount<=0 ){
						error = error + "Coverage Amount should be filled with a positive numeric value\n";
						complete = false;
					}
				}		
			 }
			 
			 if(regNumber=="" )
			 {
				error = error + "Insert the Registration Number \n";
				complete = false;
			 }
			 
		
			 if(vehicleModel=="" )
			 {
				error = error + "Insert the Vehicle Model \n";
				complete = false;
			 }
			 
			 
			 if(vehicleMake=="" )
			 {
				error = error + "Insert the Vehicle Make \n";
				complete = false;
			 }
			 
			 if(cubicCapacity=="")
			  {
				error = error + "Insert the Cubic Capacity \n";
				complete = false;
			 }
			 else{
				 if( !IsNumeric(cubicCapacity)){
					 error = error + "The Cubic Capacity should be a number \n";
					 complete = false;
				 }
				 else if(cubicCapacity<=0){
					 error = error + "The Cubic Capacity cannot be zero or Negative \n";
					 complete = false;
				 }
			 }
			 
			 
			 
			
		 }
		 //check these fields only for EMPLOYER LIABILITY
		 else if(insuranceType=="<?php echo $INSURANCE_TYPE_EMPLOYER_LIABILITY; ?>")
		 {
			 if(employersSocialInsuranceNumber=="" )
			 {
				error = error + "Insert the Employers Social Insurance Number \n";
				complete = false;
			 }
			 
			  if(employeesNumber=="")
			  {
				error = error + "Insert the Number of Employees \n";
				complete = false;
			 }
			 else{
				 if( !IsNumeric(employeesNumber)){
					 error = error + "The Number of Employees should be a number \n";
					 complete = false;
				 }
				 else if(employeesNumber<1){
					 error = error + "The Number of Employees must be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			  if(estimatedTotalGrossEarnings=="")
			  {
				error = error + "Insert the Estimated Total Gross Earnings \n";
				complete = false;
			 }
			 else{
				 if( !IsNumeric(estimatedTotalGrossEarnings)){
					 error = error + "The Estimated Total Gross Earnings should be a number \n";
					 complete = false;
				 }
				 else if(estimatedTotalGrossEarnings<1){
					 error = error + "The Estimated Total Gross Earnings must be a Positive Number \n";
					 complete = false;
				 }
			 }
		 
		 }
		 //check these fields only for MEDICAL
		 else if(insuranceType=="<?php echo $INSURANCE_TYPE_MEDICAL; ?>")
		 {
			 
			  if(planName=="" )
			 {
				error = error + "Insert the Plan Name \n";
				complete = false;
			 }
			 
			 if(maximumLimit=="" )
			 {
				error = error + "Maximum Limit Cannot be Empty. \n";
				complete = false;
			 }
			 else 
			  {
				 if( !IsNumeric(maximumLimit)){
					 error = error + "The Maximum Limit should be a number \n";
					 complete = false;
				 }
				 else if(maximumLimit<0){
					 error = error + "The Maximum Limit must be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 if(deductible=="" )
			 {
				error = error + "Deductible Cannot be Empty. \n";
				complete = false;
			 }
			 else 
			  {
				 if( !IsNumeric(deductible)){
					 error = error + "Deductible should be a number \n";
					 complete = false;
				 }
				 else if(deductible<0){
					 error = error + "Deductible must be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 if(coInsurancePercentage=="" )
			 {
				error = error + "Co-Insurance Cannot be Empty. \n";
				complete = false;
			 }
			 else 
			  {
				 if( !IsNumeric(coInsurancePercentage)){
					 error = error + "Co-Insurance should be a number \n";
					 complete = false;
				 }
				 else if(coInsurancePercentage<0 || coInsurancePercentage>100){
					 error = error + "Co-Insurance must be between 0 and 100 \n";
					 complete = false;
				 }
			 }
			 
			 if(premium!="" )
			 {
				 if( !IsNumeric(premium)){
					 error = error + "Premium should be a number \n";
					 complete = false;
				 }
				 else if(maximumLimit<0){
					 error = error + "Premium should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
		 }
		 
		  //check these fields only for LIFE
		 else if(insuranceType=="<?php echo $INSURANCE_TYPE_LIFE; ?>")
		 {
			 
			 if(lifeInsPlanName=="" )
			 {
				error = error + "Insert the Plan Name \n";
				complete = false;
			 }
			 if(insuredFirstName=="" )
			 {
				error = error + "Insert the Insured Person Name \n";
				complete = false;
			 }
			 if(insuredLastName=="" )
			 {
				error = error + "Insert the Insured Person Surname \n";
				complete = false;
			 }
		 }
		  //check these fields only for PROPERTY FIRE AND THEFT
		 else if(insuranceType=="<?php echo $INSURANCE_TYPE_FIRE_PROPERTY; ?>")
		 {
			 if(buildingValue!="" )
			 {
				 if( !IsNumeric(buildingValue)){
					 error = error + "Building Value should be a number \n";
					 complete = false;
				 }
				 else if(buildingValue<0){
					 error = error + "Building Value should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 if(outsideFixturesValue!="" )
			 {
				 if( !IsNumeric(outsideFixturesValue)){
					 error = error + "Outside Fixtures Value should be a number \n";
					 complete = false;
				 }
				 else if(outsideFixturesValue<0){
					 error = error + "Outside Fixtures Value should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 if(contentsValue!="" )
			 {
				 if( !IsNumeric(contentsValue)){
					 error = error + "Contents Value should be a number \n";
					 complete = false;
				 }
				 else if(contentsValue<0){
					 error = error + "Contents Value should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 if(valuableObjectsValue!="" )
			 {
				 if( !IsNumeric(valuableObjectsValue)){
					 error = error + "Valueable Objects value should be a number \n";
					 complete = false;
				 }
				 else if(valuableObjectsValue<0){
					 error = error + "Valuable Objects Value should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			
			 
			 if(areaSqMt!="" )
			 {
				 if( !IsNumeric(areaSqMt)){
					 error = error + "Area should be a number \n";
					 complete = false;
				 }
				 else if(areaSqMt<0){
					 error = error + "Area should be a Positive Number \n";
					 complete = false;
				 }
			 }
		 }
	 }
	 catch(e) {
        alert("Inside checkNewContractForm. Error:" + e);
    }
	 
	
	 if( complete == false )
		window.alert(error);	
		
	 return complete;
 }// function checkNewContractForm()


/* check if the Create New User Form is valid */
function checkCreateNewUserForm(addressTableId)
{
	var complete = true;
	var error = "Please correct following issues:\n";
		
	try{
		var username = document.createNewUser.newUsername.value;
		var password = document.createNewUser.newPassword.value;
		var password2 = document.createNewUser.password2.value;
		var email = document.createNewUser.email.value;
		
		if(username=="")
		{
			error = error + "Username is mandatory \n";
			complete = false;
		}
		
		if(password=="")
		{
			error = error + "Password is mandatory \n";
			complete = false;
		}
		
		if(email=="")
		{
			error = error + "Email is mandatory \n";
			complete = false;
		}
		else if(isValidEmail(email)==false)
		{
			error = error + "Email must be in the form example@example.com\n";
			//form.txtsender_email.value='';
			//errorsFound=1;		
			complete = false;
		}
	
		
		if(password != password2 )
		{
			error = error + "Passwords must match\n";
			complete = false;
		}
		
		if(validateAddresses(addressTableId).complete==false)
		{
			error = error + validateAddresses(addressTableId).error;
			complete = false;
		}
	}
	catch(e) {
        alert("Inside checkCreateNewUserForm. Error:" + e);
    }
	
	if( complete == false )
		window.alert(error);	
    return complete;
	
}


/* check if the Update User Data Form( updateUserDataForm.php ) is valid */
function checkUpdateUserDataForm(addressTableId)
{
	try{
		
		var complete=true;
		var error = "Please correct following issues:\n";
		var email = document.getElementById('email').value;
		var telephone = document.getElementById('telephone').value;
		
		if(email=="")
		{
			error = error + "email is mandatory \n";
			complete = false;
		}
		
		if(telephone!="")
		{
			if( !IsNumeric(telephone)){
				 error = error + "The Telephone Number should be a number \n";
				 complete = false;
			 }
			
		}
		
		if(validateAddresses(addressTableId).complete==false)
		{
			error = error + validateAddresses(addressTableId).error;
			complete = false;
		}
	}
	catch(e) {
        alert("Inside checkUpdateUserDataForm. Error:" + e);
    }
	
	if( complete == false )
		window.alert(error);	
    return complete;
	
}




/* check if the Update User Data Form( updateUserData.php ) is valid.
addressTableId: the id of the table containing the addresses */
function checkUpdateClientDataForm(addressTableId)
{
	try
	{
		var complete=true;
		var error = "Please correct following issues:\n";
		var stateId = document.getElementById('stateId').value;
		
		if(document.getElementById('notesDate1') != null )
		{
			var notesDate = document.getElementById('notesDate1').value;
			
			if(validateDate(notesDate)==true)
			{
				error = error + "Date must be in the format 'dd-mm-yyyy'\n";
				complete = false;
			}
		}
		
		if(stateId=="")
		{
			error = error + "stateId is mandatory \n";
			complete = false;
		}
		
		if(validateAddresses(addressTableId).complete==false)
		{
			complete = false;
			error = error + validateAddresses(addressTableId).error;
		}
	}
	catch(e) {
        alert("Inside checkUpdateClientDataForm. Error:" + e);
    }
	
	if( complete == false )
		window.alert(error);	
    return complete;
	
}


/* validate all addresses in this table.
addressTableId: the id of the table containing the addresses */
function validateAddresses(addressTableId)
{
	var error = new errorStructure(true, "");
	
	try 
	{
	    var table = document.getElementById(addressTableId);
	    //only when table is found in the form
	    if(table != null )
	    {
		    var rowCount = table.rows.length;
		
		    //bypass first row that contains the title.
		    for(var i=0; i<rowCount; i++) 
		    {
		        var row = table.rows[i];
		        var street = row.cells[2].childNodes[0].value;
				
		        //street must be filled
				if(street=="")
				{
					error.error = this.error + "Street is mandatory\n";
					error.complete = false;
				 }
			 }
		 }
	 }
	 catch(e) {
        alert("Inside validateAddresses. Error:" + e);
     }
    
	return error;
	
}



/* check if the Update Contract Data Form( updateContractData.php ) is valid */
function checkUpdateContractDataForm(medicalInsuredPersonsTableId)
{
	try
	{
		var complete=true;
		var error = "Please correct following issues:\n";
		var contractNumber = document.getElementById('contractNumber').value;
		var stateId = document.getElementById('stateId').value;
		var firstName = document.getElementById('firstName').value;
		var profession = document.getElementById('profession').value;
		var regNumber = document.getElementById('regNumber').value;
		var make = document.getElementById('make').value;
		var vehicleModel = document.getElementById('vehicleModel').value;
		var cubicCapacity = document.getElementById('cubicCapacity').value;
		var insuranceType = document.getElementById('insuranceType').value;
		var coverageType = document.getElementById('coverageType').value;
		var birthDate = document.getElementById('birthDate').value;
		
		//employers liability
		var employersSocialInsuranceNumber = document.getElementById('employersSocialInsuranceNumber').value;
		var limitPerEmployee = document.getElementById('limitPerEmployee').value;
		var limitPerEventOrSeriesOfEvents = document.getElementById('limitPerEventOrSeriesOfEvents').value;
		var limitDuringPeriodOfInsurance = document.getElementById('limitDuringPeriodOfInsurance').value;
		var employeesNumber = document.getElementById('employeesNumber').value;
		var estimatedTotalGrossEarnings = document.getElementById('estimatedTotalGrossEarnings').value;
		
		//medical
		var planName = document.getElementById('planName').value;
		var maximumLimit = document.getElementById('planMaximumLimit').value;
		var deductible = document.getElementById('deductible').value;
		var coInsurancePercentage = document.getElementById('coInsurancePercentage').value;
		var premium = document.getElementById('premium').value;
		
		//life
		var lifeInsPlanName = document.getElementById('lifeInsPlanName').value;
		var insuredFirstName = document.getElementById('insuredFirstName').value;
		var insuredLastName = document.getElementById('insuredLastName').value;
				 
		//property fire
		var buildingValue = document.getElementById('buildingValue').value;
		var outsideFixturesValue = document.getElementById('outsideFixturesValue').value;
		var contentsValue = document.getElementById('contentsValue').value;
		var valuableObjectsValue = document.getElementById('valuableObjectsValue').value;
		var areaSqMt = document.getElementById('areaSqMt').value;
		
		var proposerType = document.getElementById('proposerType').value;
		
		if(contractNumber=="")
		{
			error = error + "contract number is mandatory \n";
			complete = false;
		}
		
			
		if(stateId=="")
		{
			error = error + "state Id is mandatory \n";
			complete = false;
		}
		
		
		if(firstName=="")
		{
			error = error + "first name is mandatory \n";
			complete = false;
		}
		
		
		if(profession=="")
		{
			error = error + "profession is mandatory \n";
			complete = false;
		}
		
		//MOTOR - CASE
		if(insuranceType == "<?php echo $INSURANCE_TYPE_MOTOR; ?>" )
		{
			if(proposerType == "<?php echo $PROPOSER_TYPE_PERSON; ?>")
			{
				 if ( checkBirthDate(birthDate)==false)
				 {
					 error = error + "The Age of the driver is less than 18 years old \n";
					 complete = false;
				 }
			}
			
			if(regNumber=="")
			{
				error = error + "regNumber is mandatory \n";
				complete = false;
			}
			
			if(make=="")
			{
				error = error + "make is mandatory \n";
				complete = false;
			}
			
			if(vehicleModel=="")
			{
				error = error + "vehicleModel is mandatory \n";
				complete = false;
			}
			
			
		}
		//check these fields only for EMPLOYER LIABILITY
		 else if(insuranceType=="<?php echo $INSURANCE_TYPE_EMPLOYER_LIABILITY; ?>")
		 {
			 
			 if(employersSocialInsuranceNumber=="" )
			 {
				error = error + "Insert the Employers Social Insurance Number \n";
				complete = false;
			 }
			 
			 if(employeesNumber=="")
			 {
				error = error + "Insert the Number of Employees \n";
				complete = false;
			 }
			
			 			 
			 if(estimatedTotalGrossEarnings=="")
			 {
				error = error + "Insert the Estimated Total Gross Earnings \n";
				complete = false;
			 }
		 
		 }
		 //check these fields only for MEDICAL
		 else if(insuranceType=="<?php echo $INSURANCE_TYPE_MEDICAL; ?>")
		 {
			 if(planName=="" )
			 {
				error = error + "Insert the Plan Name \n";
				complete = false;
			 }
			 
			 if(maximumLimit=="" )
			 {
				error = error + "Maximum Limit Cannot be Empty. \n";
				complete = false;
			 }
			
			 if(deductible=="" )
			 {
				error = error + "Deductible Cannot be Empty. \n";
				complete = false;
			 }
			
			 
			 if(coInsurancePercentage=="" )
			 {
				error = error + "Co-Insurance Cannot be Empty. \n";
				complete = false;
			 }
			
			 
			 if(premium!="" )
			 {
				
				 if( !IsNumeric(premium)){
					 error = error + "Premium should be a number \n";
					 complete = false;
				 }
			 }

			/*
			if(validateInsuredMedicalPersons(medicalInsuredPersonsTableId).complete==false)
			{
				complete = false;
				error = error + validateInsuredMedicalPersons(medicalInsuredPersonsTableId).error;
			}*/
			
	
		 }	
		 //check these fields only for LIFE
		 else if(insuranceType=="<?php echo $INSURANCE_TYPE_LIFE; ?>")
		 {
			 
			 if(lifeInsPlanName=="" )
			 {
				error = error + "Plan Name cannot be empty\n";
				complete = false;
			 }
			 if(insuredFirstName=="" )
			 {
				error = error + "Insured Person Name cannot be empty\n";
				complete = false;
			 }
			 if(insuredLastName=="" )
			 {
				error = error + "Insured Person Surname cannot be empty \n";
				complete = false;
			 }
		 }
		  //check these fields only for MEDICAL
		 else if(insuranceType=="<?php echo $INSURANCE_TYPE_FIRE_PROPERTY; ?>")
		 {
			 
			 if(buildingValue!="" )
			 {
				 if( !IsNumeric(buildingValue)){
					 error = error + "Building Value should be a number \n";
					 complete = false;
				 }
				 else if(buildingValue<0){
					 error = error + "Building Value should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 if(outsideFixturesValue!="" )
			 {
				 if( !IsNumeric(outsideFixturesValue)){
					 error = error + "Outside Fixtures Value should be a number \n";
					 complete = false;
				 }
				 else if(outsideFixturesValue<0){
					 error = error + "Outside Fixtures Value should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 if(contentsValue!="" )
			 {
				 if( !IsNumeric(contentsValue)){
					 error = error + "Contents Value should be a number \n";
					 complete = false;
				 }
				 else if(contentsValue<0){
					 error = error + "Contents Value should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 if(valuableObjectsValue!="" )
			 {
				 if( !IsNumeric(valuableObjectsValue)){
					 error = error + "Valueable Objects value should be a number \n";
					 complete = false;
				 }
				 else if(valuableObjectsValue<0){
					 error = error + "Valuable Objects Value should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			
			 
			 if(areaSqMt!="" )
			 {
				 if( !IsNumeric(areaSqMt)){
					 error = error + "Area should be a number \n";
					 complete = false;
				 }
				 else if(areaSqMt<0){
					 error = error + "Area should be a Positive Number \n";
					 complete = false;
				 }
			 }
			 
			 
		 }//end else
	}
	catch(e) {
        alert("Inside checkUpdateContractDataForm. Error:" + e);
    }	
	
	
	if( complete == false )
		window.alert(error);	
    return complete;
	
}//function checkUpdateContractDataForm()



/* check if the Find Client Form( findClientForm.php ) is valid */
function checkFindClientForm()
{
	try
	{
		var complete=true;
		var error = "Please correct following issues:\n";
		var firstName = document.getElementById('firstName').value;
		var surname = document.getElementById('surname').value;
		var stateId = document.getElementById('stateId').value;
		var cellphone = document.getElementById('cellphone').value;
		
		if(firstName=="" && surname=="" && stateId=="" && cellphone=="" )
		{
			error = error + "One of the fields should be filled \n";
			complete = false;
		}
	}
	catch(e) {
        alert("Inside checkFindClientForm. Error:" + e);
    }
	
	
	if( complete == false )
		window.alert(error);	
    return complete;
	
}

/* check if the Get Transactions Form( transactionReportsMenu.php ) is valid */
function checkGetTransactions()
{
	try
	{
		var complete=true;
		var error = "Please correct following issues:\n";
		var transactionStartDate = document.getElementById('transactionStartDate').value;
		var transactionEndDate = document.getElementById('transactionEndDate').value;
		
		if(transactionEndDate<transactionStartDate)
		{
			error = error + "End Date cannot be before Start Date \n";
			complete = false;
		}
	}
	catch(e) {
        alert("Inside checkFindClientForm. Error:" + e);
    }
	
	
	if( complete == false )
		window.alert(error);	
    return complete;
	
}

/* Validate that date has the format dd-mm-yyyy. f.e 23-12-2005 */
function validateDate(dateStr)
{
	try{
	
		var day;
		var month;
		var year;
		var leap = 0;
		var i;
		var err = false;
		var separator;
	  	
	   /* Date must have 10 digits, dd-mm-yyyy */
	   if (dateStr.length != 10) 
	   	err = true;
	   
	   	//1st separator
	   separator = 	dateStr.substr(2,1);
	   if(separator != '-' )
	   	err = true;
	   
	   //2nd separator
	   separator = 	dateStr.substr(5,1);
	   if(separator != '-' )
	   	err = true;
	   	
	   /* year must be numberic */
	   year = dateStr.substr(6,4);
	   if (!IsNumeric(year))
	      err = true;
	   
	   /* Validation of month*/
	   month = dateStr.substr(3,2);
	   if (!IsNumeric(month) || ((month < 1) || (month > 12)) )
	   	err = true;
	   
	   /* Validation of day*/
	   day = dateStr.substr(0,2);
	   if (!IsNumeric(day) || day > 31 )
	   	err = true;
	   
	   /* Validation leap-year / february / day */
	   if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
	      leap = 1;
	   }
	   if ((month == 2) && (leap == 1) && (day > 29)) {
	      err = true;
	   }
	   if ((month == 2) && (leap != 1) && (day > 28)) {
	      err = true;
	   }
	   /* Validation of other months */
	   if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
	      err = true;
	   }
	   if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
	      err = true;
	   }
   }
   catch(e) {
        alert("Inside validateDate. Error:" + e);
    }
   return err;
}

/* check if the data for Insert New Transaction ( findContractFormProcess.php ) 
has all the needed data for adding a transaction */
function checkInsertNewTransactionValidity()
{
	try
	{
		var complete=true;
		var transDate = document.getElementById('transDate1').value;
	    var details = document.getElementById('details1').value;
	    var debit = document.getElementById('debit1').value;
	    var credit = document.getElementById('credit1').value;
		var error = "Following information is needed to add a Transaction. :\n";
		
		if(transDate=="")
		{
			error = error + "Date must be filled\n";
			complete = false;
		}
		/*
		else
		{
			if(validateDate(transDate)==true)
			{
				error = error + "Date must be in the format 'dd-mm-yyyy'\n";
				complete = false;
			}
		}*/
		
		if(debit!="")
		{
			if( !IsNumeric(debit)){
				 error = error + "Debit should be a number \n";
				 complete = false;
			 }
			
		}
		
		if(credit!="")
		{
			if( !IsNumeric(credit)){
				 error = error + "Credit should be a number \n";
				 complete = false;
			 }
			
		}
		
		if(debit=="" && credit=="")
		{
			 error = error + "One of Debit and Credit must be filled \n";
			 complete = false;
		}
	}
	catch(e) {
        alert("Inside checkInsertNewTransactionValidity. Error:" + e);
    }
		
	if( complete == false )
	{
		error = error + "Please CORRECT transaction and try again.\n";
		window.alert(error);	
	}
    return complete;
	
}


/* check if the data for Insert New Note ( updateContractData.php ) 
has all the needed data for adding a note */
function checkInsertNewNoteValidity()
{
	try
	{
		var complete=true;
		var description = document.getElementById('description1').value;
	    var notesDate = document.getElementById('notesDate1').value;
		var error = "Following information is needed to add a Note. :\n";
		
		if(notesDate=="")
		{
			error = error + "Date must be filled\n";
			complete = false;
		}
		else
		{
			if(validateDate(notesDate)==true)
			{
				error = error + "Date must be in the format 'dd-mm-yyyy'\n";
				complete = false;
			}
		}
		
		if(description=="")
		{
			error = error + "Description must be filled\n";
			complete = false;
		}
	}
	catch(e) {
        alert("Inside checkInsertNewNoteValidity. Error:" + e);
    }
	
	if( complete == false )
	{
		error = error + "Please CORRECT and try again.\n";
		window.alert(error);	
	}
    return complete;
	
}


/* check if the data for Insert New Claim ( updateContractData.php ) 
has all the needed data for adding a claim */
function checkInsertNewClaimValidity()
{
	try
	{
		var complete=true;
		var amount = document.getElementById('amount1').value;
	    var claimDate = document.getElementById('claimDate1').value;
		var error = "Following information is needed to add a Claim. :\n";
		
		if(claimDate=="")
		{
			error = error + "Date must be filled\n";
			complete = false;
		}
		else
		{
			if(validateDate(claimDate)==true)
			{
				error = error + "Date must be in the format 'dd-mm-yyyy'\n";
				complete = false;
			}
		}
		
		if(amount=="")
		{
			error = error + "Amount must be filled\n";
			complete = false;
		}
		else if( !IsNumeric(amount))
		{
			error = error + "Amount should be a number \n";
			complete = false;
		}
	}	
	catch(e) {
        alert("Inside checkInsertNewClaimValidity. Error:" + e);
    }
		
	
	if( complete == false )
	{
		error = error + "Please CORRECT and try again.\n";
		window.alert(error);	
	}
    return complete;
	
}


/* check if the Update User Data Form( updateUserDataForm.php ) 
has all the needed data for adding a contract */
function checkUpdateUserDataFormForAddContractValidity()
{
	try
	{
		var complete=true;
		var error = "Following information is needed to add a Contract. :\n";
		var stateId = document.updateUserData.stateId.value;
		var firstName = document.updateUserData.firstName.value;
		var lastName = document.updateUserData.lastName.value;
		var email = document.updateUserData.email.value;
		var telephone = document.updateUserData.telephone.value;
		var cellphone = document.updateUserData.cellphone.value;
		
		if(stateId=="")
		{
			error = error + "stateId\n";
			complete = false;
		}
		
		if(firstName=="")
		{
			error = error + "firstName\n";
			complete = false;
		}
		
		if(lastName=="")
		{
			error = error + "lastName\n";
			complete = false;
		}
		
		if(email=="")
		{
			error = error + "email\n";
			complete = false;
		}
		
		if(telephone!="")
		{
			if( !IsNumeric(telephone)){
				 error = error + "The Telephone Number should be a number \n";
				 complete = false;
			 }
			
		}
		
		if(cellphone!="")
		{
			if( !IsNumeric(cellphone)){
				 error = error + "The Cellphone Number should be a number \n";
				 complete = false;
			 }
			
		}
		
		if(telephone=="" && cellphone=="")
		{
			error = error + "Telephone OR Cellphone \n";
			complete = false;
		}
	}
	catch(e) {
        alert("Inside checkUpdateUserDataFormForAddContractValidity. Error:" + e);
    }
	
	if( complete == false )
	{
		error = error + "Please UPDATE user and try again.\n";
		window.alert(error);	
	}
    return complete;
	
}


function printForm()
{
	 document.write("Printing");
}
 


//show the hidden element "object"
function show(object)
{
	try
	{
	
		if (document.getElementById) 
		{
			document.getElementById(object).style.visibility = 'visible';
		}
		//for netscape navigator
		else if (document.layers && document.layers[object]) 
		{
			document.layers[object].visibility = 'visible';
		}
		//for internet explorer
		else if (document.all) 
		{
			document.all[object].style.visibility = 'visible';
		}
	}
	catch(e) {
        alert("Inside show. Error:" + e);
    }
}
	
//hide the element object
function hide(object)
{
	try
	{
		if (document.getElementById) 
		{
			document.getElementById(object).style.visibility = 'hidden';
		}
		else if (document.layers && document.layers[object]) 
		{
			document.layers[object].visibility = 'hidden';
		}
		else if (document.all) 
		{
			document.all[object].style.visibility = 'hidden';
		}
	}
	catch(e) {
        alert("Inside hide. Error:" + e);
    }
}

//checks if the string given is a numeric value( float or integer )
function IsNumeric(sText)
{
	try
	{
	   var ValidChars = "0123456789.";
	   var IsNumber=true;
	   var Char;
	   //alert("inside IsNumeric");
	
	   for (i = 0; i < sText.length && IsNumber == true; i++) 
	   { 
		   Char = sText.charAt(i); 
		   
		   if(i==0)
		      if(Char=='-')
		      	continue;
	      if (ValidChars.indexOf(Char) == -1) 
	      {
	         IsNumber = false;
	      }
	   }
   }
   catch(e) {
        alert("Inside IsNumeric. Error:" + e);
    }
   return IsNumber;   
}



//validate on runtime if the event entered corresponds to a numeric key - 
//Event is numeric =>  return true.
//Event is not numeric =>  return false
function validateOnRunTimeIsNumeric (event)
{
	try
	{
		/* event.which is for mouse events, event.keyCode is for keyboard events */
		var keyCode = ('which' in event) ? event.which : event.keyCode;
		
		isNumeric = (keyCode >= 48 /* KeyboardEvent.DOM_VK_0 */ && keyCode <= 57 /* KeyboardEvent.DOM_VK_9 */) ||
		            (keyCode >= 96 /* KeyboardEvent.DOM_VK_NUMPAD0 */ && keyCode <= 105 /* KeyboardEvent.DOM_VK_NUMPAD9 */) ||
		            keyCode==110 /* .(num lock) */ || keyCode==190/* .> */ || keyCode==9 /* tab key */ || keyCode==46 /* del key */ || keyCode==8 /* backspace key */;
		modifiers = (event.altKey || event.ctrlKey || event.shiftKey );
		/* return true if its numeric, '.', 'tab', 'alt', 'ctr', 'shift' */
		return isNumeric || modifiers;
	}
	catch(e) {
        alert("Inside validateOnRunTimeIsNumeric. Error:" + e);
    }
}
        
        
 /* checks if the birth date is less than 18 years */
 function checkBirthDate(ownerBirthDateStr)
 {
	 try
	 {
		 var ownerBirthYear=ownerBirthDateStr.substring(0,4);	 
		 var ownerBirthMonth=ownerBirthDateStr.substring(5,7);
		 var ownerBirthDay=ownerBirthDateStr.substring(8,10);
		 var ownerBirthDate = new Date();
		 ownerBirthDate.setFullYear(ownerBirthYear,ownerBirthMonth-1,ownerBirthDay);//Jan = 0
		 
		 var date18YearsBefore = new Date();
		 date18YearsBefore.setFullYear(date18YearsBefore.getFullYear()-18); 
		
		 //for input birth date, the age is < 18 years old
		 if(date18YearsBefore < ownerBirthDate )
		 	return false;
	 	 //for input birth date, the age is >= 18 years old
	 	 else
	 		return true;
 	  }
 	  catch(e) {
        alert("Inside checkBirthDate. Error:" + e);
      }
 }
 

/* show the hidden tags regarding the additional drivers 
called from quotationTable.php */ 
function showAdditionalDriversDetails()
{
	try
	{
		//make sure this exists. It is also called from newContractForm, that it doesn't exist
		if(document.getElementById('addAdditionalDriversQuestion'))
		{
			if(document.getElementById('addAdditionalDriversQuestion').value == 'YES' )
		  	{
			  document.getElementById('additional-drivers-info').style.display ='table-row-group';
		  	}
		  	else if(document.getElementById('addAdditionalDriversQuestion').value == 'NO' )
		  	{
			  document.getElementById('additional-drivers-info').style.display ='none';
		  	}
	  	}
  	}
  	catch(e) {
        alert("Inside showAdditionalDriversDetails. Error:" + e);
    }
	
}


/* show the hidden tags regarding the driving experience */ 
function processHasPreviousInsurance()
{
	try
	{
		//make sure this exists. 
		if(document.getElementById('hasPreviousInsurance'))
		{
			if(document.getElementById('hasPreviousInsurance').value == 'YES' )
		  	{
			  document.getElementById('previous-insurance-info').style.display ='table-row-group';
		  	}
		  	else if(document.getElementById('hasPreviousInsurance').value == 'NO' )
		  	{
			  document.getElementById('previous-insurance-info').style.display ='none';
		  	}
	  	}
  	}
  	catch(e) {
        alert("Inside processHasPreviousInsurance. Error:" + e);
    }
	
}


/* show the hidden accidents boxes.
called from quotationTable.php  */
function showAccidents()
{
	try
	{
	  if(document.getElementById('numberOfAccidentsOfAnyDriverDuringLastThreeYears').value == '0' )
	  {
		  document.getElementById('ac1').style.display ='none';
		  document.getElementById('ac2').style.display ='none';
		  document.getElementById('ac3').style.display ='none';
		  document.getElementById('ac4').style.display ='none';
	  }
	  else if(document.getElementById('numberOfAccidentsOfAnyDriverDuringLastThreeYears').value == '1' )
	  {
		  document.getElementById('ac1').style.display ='table-row';
		  document.getElementById('ac2').style.display ='none';
		  document.getElementById('ac3').style.display ='none';
		  document.getElementById('ac4').style.display ='none';
	  }
	  else if(document.getElementById('numberOfAccidentsOfAnyDriverDuringLastThreeYears').value == '2' )
	  {
		  document.getElementById('ac1').style.display ='table-row';
		  document.getElementById('ac2').style.display ='table-row';
		  document.getElementById('ac3').style.display ='none';
		  document.getElementById('ac4').style.display ='none';
	  }
	  else if(document.getElementById('numberOfAccidentsOfAnyDriverDuringLastThreeYears').value == '3' )
	  {
		  document.getElementById('ac1').style.display ='table-row';
		  document.getElementById('ac2').style.display ='table-row';
		  document.getElementById('ac3').style.display ='table-row';
		  document.getElementById('ac4').style.display ='none';
	  }
	  else 
	  {
	    document.getElementById('ac1').style.display ='table-row';
		document.getElementById('ac2').style.display ='table-row';
		document.getElementById('ac3').style.display ='table-row';
		document.getElementById('ac4').style.display ='table-row';
	  }
  }
  catch(e) {
        alert("Inside showAccidents. Error:" + e);
    }
}

//check that the input email string is in the form example@example.something 
function isValidEmail(strString)	
{
	try
	{
		var col_array=strString.split("@");
				
		//@ does not exist
		if (col_array.length!=2)
		{
			return false;
		}
		
		var checkifdot=col_array[1].split(".");
		// dot does not exist
		if (checkifdot.length==1)
		{
			return false;
		}
		
		// dot exists, but after dot there is nothing
		if(checkifdot[1].length==0)
		{
			return false;
		}
	}
	catch(e) {
        alert("Inside isValidEmail. Error:" + e);
    }
	
	return true;
}



/* show the hidden tags related to the coverage type. f.e for comprehensive, show amount, and all discounts
Applies to the quotation table. */
function processCoverageQuotation()
{
	try
	{
		if(document.getElementById('coverageType').value == "<?php echo $COVERAGE_TYPE_THIRD_PARTY; ?>")
		{
			/* Comprehensive discounts */
		  	document.getElementById('ca').style.display ='none';//coverage amount
		  	document.getElementById('compText').style.display ='none';
		  	document.getElementById('ndn').style.display ='none';
		  	document.getElementById('ae').style.display ='none';
		  	document.getElementById('ncdy').style.display ='none';
			/* offers tags */
			document.getElementById('of1').style.display ='table-row';//package offer
		  	document.getElementById('of3').style.display ='table-row';//TP individual benefits offer
		}
		//else
		else if(document.getElementById('coverageType').value == "<?php echo $COVERAGE_TYPE_COMPREHENSIVE; ?>")
		{
			/* Comprehensive discounts */
		  	document.getElementById('ca').style.display ='table-row';
		  	document.getElementById('compText').style.display ='table-row';
		  	document.getElementById('ndn').style.display ='table-row';
		  	document.getElementById('ae').style.display ='table-row';
		  	document.getElementById('ncdy').style.display ='table-row';
			/* offers tags */
		  	document.getElementById('of1').style.display ='table-row';//package offer
		  	document.getElementById('offers_package').checked = true;//is the first in row
		  	document.getElementById('of3').style.display ='none';//TP individual benefits offer
		}
		else if(document.getElementById('coverageType').value == "<?php echo $COVERAGE_TYPE_FIRE_AND_THIFT; ?>")
		{
			document.getElementById('ca').style.display ='table-row';//coverage amount
			/* offers tags */
		  	document.getElementById('of1').style.display ='table-row';//package offer
		  	document.getElementById('offers_package').checked = true;//is the first in row
		  	document.getElementById('of3').style.display ='none';//TP individual benefits offer
		}
		
		showBenefits();
	}
	catch(e) {
        alert("Inside processCoverageQuotation. Error:" + e);
    }
	
}


/* show the hidden tags related to the coverage type. f.e for comprehensive, show amount. Applies to the New Contract Flow */
function processCoverageNewContract()
{
	try
	{
		//third party
		if(document.getElementById('coverageType').value == "<?php echo $COVERAGE_TYPE_THIRD_PARTY; ?>")
		  	document.getElementById('ca').style.display ='none';//coverage amount
		//comprehensive and TP FT
		else 
		  	document.getElementById('ca').style.display ='table-row';
	}
	catch(e) {
        alert("Inside processCoverageNewContract. Error:" + e);
    }
	
	
}



/* show the hidden tags related to the production type in the reports menu */
function processProductionType()
{
	try
	{
		if(document.getElementById('productionType').value == "<?php echo $PRODUCTION_TYPE_PER_COMPANY; ?>")
		{
			document.getElementById('end-year-info').style.display ='none';
			
		}
		else if(document.getElementById('productionType').value == "<?php echo $PRODUCTION_TYPE_PER_MONTH; ?>")
		{
			document.getElementById('end-year-info').style.display ='table-row';
			
		}
	}
	catch(e) {
        alert("Inside processProductionType. Error:" + e);
    }
		
}

/* validate the getProduction Form  */
function checkGetProductionForm()
{
	try
	{
		var complete=true;
		var error = "Please correct following issues:\n";
		var productionType = document.getElementById('productionType').value;
		var startYear = document.getElementById('startYear').value;
		var endYear = document.getElementById('endYear').value;
		
		if(productionType == "<?php echo $PRODUCTION_TYPE_PER_MONTH; ?>")
		{
			if(startYear>endYear)
			{
				error = error + "Start Year must be before End Year \n";
				complete = false;
			}
		 }
	 }
	 catch(e) {
        alert("Inside checkGetProductionForm. Error:" + e);
    }
		 
	
	 if( complete == false )
		window.alert(error);	
		
	 return complete;
		
}




//GENERIC AJAX FUNCTIONS
//USED BY ALL CALLS

var xmlhttp;
function loadXMLDoc(url, cfunc)
{
  //window.alert("inside loadXMLDoc");
  if (window.XMLHttpRequest)
  {
	  // code for IE7+, Firefox, Chrome, Opera, Safari
  	  xmlhttp=new XMLHttpRequest();
  }
  else
  {
	  // code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
 
  if (xmlhttp.overrideMimeType)
 	xmlhttp.overrideMimeType('text/xml');
  xmlhttp.onreadystatechange=cfunc;
  xmlhttp.open("GET",url,true);//what file to open on the server
  xmlhttp.send();
  //alert(xmlhttp.responseText);
}

/* url includes file name + query string. f.e"./users/updateUserDataAjax.php?stateId=123" */
function myFunction(url)
{
  //window.alert("inside myFunction");
  loadXMLDoc(url,function()
  {
	  //window.alert("readyState="+xmlhttp.readyState+" status="+xmlhttp.status);
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	  {
		  //message is defined in php file as div
		  //for multiple messages, we need to put them in the ECHO, separated with comma
		  var mySplitResult = xmlhttp.responseText.split(",");
		  document.getElementById("message").innerHTML=mySplitResult[0];
		  
		  //if second param is found, set it
		  if(mySplitResult.length>1)
		  	document.getElementById("secondParameter").innerHTML=mySplitResult[1];
		  //if not found, set to empty
		  else
		  	document.getElementById("secondParameter").innerHTML="";
		  	
		  //alert(xmlhttp.responseText);
		  //return xmlhttp.responseText;
	    
	  }
  });
}


/* url includes file name + query string. f.e"./users/updateUserDataAjax.php?stateId=123" */
function myFunctionForQuotationLoad(url)
{
  //window.alert("inside myFunction");
  loadXMLDoc(url,function()
  {
	  //window.alert("readyState="+xmlhttp.readyState+" status="+xmlhttp.status);
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	  {
	    //document.getElementById("message").innerHTML=data.quoteId;
	    //document.getElementById("fullName").value=xmlhttp.responseText;
	    alert(xmlhttp.responseText);
	    //return xmlhttp.responseText;
	    
	    
		   try{
				var xmldata=xmlhttp.responseXML; //retrieve result as an XML object
		   		//alert(typeof xmlhttp.responseXML) 
		   		//window.alert(xmldata);
		   		var offerSelected=xmldata.getElementsByTagName('offerSelected')[0].firstChild.nodeValue
		   		//window.alert(offerSelected)
		   		document.getElementById("message").innerHTML = xmldata.getElementsByTagName('quoteId')[0].firstChild.nodeValue+" loaded";
		   		document.getElementById("firstName").value = xmldata.getElementsByTagName('firstName')[0].firstChild.nodeValue;
		   		document.getElementById("lastName").value = xmldata.getElementsByTagName('lastName')[0].firstChild.nodeValue;
		   		//window.alert(document.getElementById("ownerBirthDate").value);
		   		setValueFromDatabaseFormat('ownerBirthDate', xmldata.getElementsByTagName('birthDate')[0].firstChild.nodeValue);
		   		setPreviousValue(xmldata.getElementsByTagName('countryOfBirth')[0].firstChild.nodeValue, 'ownerEthnicity');
		   		document.getElementById("ownerProfession").innerHTML = xmldata.getElementsByTagName('profession')[0].firstChild.nodeValue;
		   		setValueFromDatabaseFormat('licenseDate', xmldata.getElementsByTagName('licenseDate')[0].firstChild.nodeValue);
		   		setPreviousValue(xmldata.getElementsByTagName('licenseType')[0].firstChild.nodeValue, 'licenseType');
		   		setPreviousValue(xmldata.getElementsByTagName('licenseCountry')[0].firstChild.nodeValue, 'licenseCountry');
		   		setPreviousValue(xmldata.getElementsByTagName('hasPreviousInsurance')[0].firstChild.nodeValue, 'hasPreviousInsurance');
		   		setPreviousValue(xmldata.getElementsByTagName('vehicleType')[0].firstChild.nodeValue, 'vehicleType');
		   		document.getElementById("vehicleModel").value = xmldata.getElementsByTagName('model')[0].firstChild.nodeValue;
		   		document.getElementById("vehicleMake").value = xmldata.getElementsByTagName('make')[0].firstChild.nodeValue;
		   		document.getElementById("cubicCapacity").value = xmldata.getElementsByTagName('cubicCapacity')[0].firstChild.nodeValue;
		   		setPreviousValue(xmldata.getElementsByTagName('manufacturedYear')[0].firstChild.nodeValue, 'vehicleManufacturedYear');
		   		setPreviousValue(xmldata.getElementsByTagName('steeringWheelSide')[0].firstChild.nodeValue, 'steeringWheelSide');
		   		setPreviousValue(xmldata.getElementsByTagName('vehicleDesign')[0].firstChild.nodeValue, 'vehicleDesign');
		   		setPreviousValue(xmldata.getElementsByTagName('isSportsModel')[0].firstChild.nodeValue, 'isSportsModel');
		   		setPreviousValue(xmldata.getElementsByTagName('isTaxFree')[0].firstChild.nodeValue, 'isTaxFree');
		   		setPreviousValue(xmldata.getElementsByTagName('isUsedForDeliveries')[0].firstChild.nodeValue, 'isUsedForDeliveries');
		   		setPreviousValue(xmldata.getElementsByTagName('coverageType')[0].firstChild.nodeValue, 'coverageType');
		   		
		   		//show additional fields if needed
		   		processCoverageQuotation();
		   		showBenefits();
			}
			catch(e) {
		        alert("Inside myFunctionForQuotationLoad. Error:" + e);
		    }
    
	  }
  });
}



/* validate the Form  */
function checkModifyAdditionalDriversForm(tableID)
{
	var complete=true;
	var error = "Please correct following issues:\n";
	
	try 
	{
	    var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
	
	    //bypass first row that contains the title.
	    for(var i=1; i<rowCount; i++) 
	    {
	        var row = table.rows[i];
	        var firstName = row.cells[1].childNodes[0].value;
	        var lastName = row.cells[2].childNodes[0].value;
	        var stateId = row.cells[3].childNodes[0].value;
	        //var birthDate = row.cells[5].childNodes[0].value;
			//var licenseDate = row.cells[6].childNodes[0].value;
			
	        //if one of name, lastname is filled, means we need to add this driver
			if(firstName!="" || lastName!="")
			{
				 //for any driver, stateId is mandatory
				if(stateId==""){
					error = error + "State Id for Additional Driver is mandatory\n";
					complete = false;
				}

				/*
				if(birthDate!="")
				{
					//alert(stateId);
					if(validateDate(birthDate)==true)
					{
						error = error + "Date must be in the format 'dd-mm-yyyy'\n";
						complete = false;
					}
				}
				
				if(licenseDate!="")
				{
					if(validateDate(licenseDate)==true)
					{
						error = error + "Date must be in the format 'dd-mm-yyyy'\n";
						complete = false;
					}
				}
				*/
			 }
		 }
	 }
	 
	 catch(e) {
        alert("Inside checkModifyAdditionalDriversForm. Error:" + e);
    }
    
	 if( complete == false )
		window.alert(error);	
		
	 return complete;
		
}

/* delete rows from table with id=tableID
Deletes the rows that have the checkbox as checked.
Checkbox MUST be in the first cell of each row */
function deleteRowFromTable(tableID)
{
    try {
	    var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
	
	    for(var i=0; i<rowCount; i++) {
	        var row = table.rows[i];
	        var chkbox = row.cells[0].childNodes[0];
	        if(null != chkbox && true == chkbox.checked) {
	            table.deleteRow(i);
	            rowCount--;
	            i--;
	        }
	    }
    }
    catch(e) {
        alert("Inside deleteRowFromTable. Error:" + e);
    }
}

function addRowInUserAddressTable(tableID)
{
	try
	{

	    var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
	    var row = table.insertRow(rowCount);
	    var columnSize="10";
		var element;
		
		//remove checkbox
	    var cell0 = row.insertCell(0);
	    element = document.createElement("input");
	    element.type = "checkbox";
	    element.name = "chk[]";
	    element.className = "co1Per";
	    cell0.appendChild(element);
	    
	    //address id
	    element = document.createElement("input");
	    element.type = "hidden";
	    element.name = "addressId[]";
		cell0.appendChild(element);
	    
	    //address type
	    var cell1 = row.insertCell(1);
	    var selector = document.createElement("select");
		selector.name = 'addressType[]';
	    cell1.appendChild(selector);
	    
	    //insert all address types
	    <?php
			foreach($_SESSION['addressOptions'] as $eachAddressType)
			{
				?>
				var option = document.createElement("option");
				option.value = '<?php echo $eachAddressType->value;?>';
				addressName = '<?php echo $eachAddressType->name;?>';
				//for chrome, need to define variables as 'var'
				var name = document.createTextNode(addressName);
				option.appendChild(name);
				selector.appendChild(option);
			<?php
			}	
			
		 ?>
	
		//address    	 
	    var cell2 = row.insertCell(2);
	    element = document.createElement("input");
	    element.type = "text";
	    element.name = "street[]";
	    element.className = "input";
	    element.size = columnSize;
	    cell2.appendChild(element);
		 
		//code
	    element = document.createTextNode(" Code:");
	    cell2.appendChild(element);
	    element = document.createElement("input");
	    element.type = "text";
	    element.name = "code[]";
	    element.size = columnSize;
	    cell2.appendChild(element);
	    
	    //city
	    element = document.createTextNode(" City:");
	    cell2.appendChild(element);
	    element = document.createElement("input");
	    element.type = "text";
	    element.name = "city[]";
	    element.size = columnSize;
	    cell2.appendChild(element);
	    
	    //country
	    element = document.createTextNode(" Country: ");
	    cell2.appendChild(element);
	    var selector = document.createElement("select");
	    selector.style.width = '6.5em';
		selector.name = 'country[]';
	    cell2.appendChild(selector);
	    
	    //insert all countries
	    <?php
			foreach($_SESSION['countriesOptions'] as $eachCountry)
			{
				?>
				var option = document.createElement("option");
				option.value = '<?php echo $eachCountry->value;?>';
				countryName = '<?php echo $eachCountry->name;?>';
				//for chrome, need to define variables as 'var'
				var name = document.createTextNode(countryName);
				option.appendChild(name);
				selector.appendChild(option);
			<?php
			}	
		 ?>
	 }
	 catch(e) {
        alert("Inside addRowInUserAddressTable. Error:" + e);
    }
}


function addRowInMedicalInsuredPersonsTable(tableID)
{
	try
	{
	    var table = document.getElementById(tableID);
		var columnSize = "10";
	    var rowCount = table.rows.length;
	    var row = table.insertRow(rowCount);
		var element;
		
		//remove checkbox
	    var cell0 = row.insertCell(0);
	    element = document.createElement("input");
	    element.type = "checkbox";
	    element.name = "chkMedical[]";
	    element.size = columnSize;
	    cell0.appendChild(element);
	
	    //firstname
	    var cell1 = row.insertCell(1);
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = "firstNameMedical[]";
	    cell1.appendChild(element);
	    
	    //lastName
	    var cell2 = row.insertCell(2);
	    element = document.createElement("input");
	    element.type = "text";
	    element.name = "lastNameMedical[]";
	    element.size = columnSize;
	    cell2.appendChild(element);
	
	    //stateId
	    var cell3 = row.insertCell(3);
	    element = document.createElement("input");
	    element.type = "text";
	    element.name = "stateIdMedical[]";
	    element.size = columnSize;
	    cell3.appendChild(element);
	    
	    //gender
	    var cell4 = row.insertCell(4);
	    var selector = document.createElement("select");
		selector.name = 'genderMedical[]';
	    cell4.appendChild(selector);
	    
	    //insert all gender options
	    <?php
			foreach($_SESSION['genderOptions'] as $eachGender)
			{
				?>
				var option = document.createElement("option");
				option.value = '<?php echo $eachGender->value;?>';
				genderName = '<?php echo $eachGender->name;?>';
				//for chrome, need to define ALL variables with 'var'
				var name = document.createTextNode(genderName);
				option.appendChild(name);
				selector.appendChild(option);
			<?php
			}	
			
		 ?>
	
		//telephone
	    var cell5 = row.insertCell(5);
	    element = document.createElement("input");
	    element.type = "text";
	    element.name = "telephoneMedical[]";
	    element.size = columnSize;
	    cell5.appendChild(element);
	    	 
		//birthDate
	    var cell6 = row.insertCell(6);
	    element = document.createElement("input");
	    element.type = "text";
	    element.name = "birthDateMedical[]";
	    element.size = columnSize;
	    cell6.appendChild(element);
    }
    catch(e) {
        alert("Inside addRowInMedicalInsuredPersonsTable. Error:" + e);
    }
}

/* add one coverage in the table
tableId : table to add to
existingCodesListName : already existing codes in table, so we don't allow twice
*/
function addRowInOptionalCoveragesTable(tableId, existingCodesListName, tagNamePrefix)
{
	try{
		
		var existingCodesArray = existingCodesListName.split( "," );
		
		//alert(existingCodesArray[0]);
		
		
	    var table = document.getElementById(tableId);
		var columnSize = "10";
	    var rowCount = table.rows.length;
	    var row = table.insertRow(rowCount);
		var element;
		
		//remove checkbox
	    var cell0 = row.insertCell(0);
	    element = document.createElement("input");
	    element.type = "checkbox";
	    element.name = "chkCoverages[]";
	    element.size = columnSize;
	    element.className = "co1Per";
	    cell0.appendChild(element);
	
	    //name
	    var cell1 = row.insertCell(1);
	    element = document.createTextNode("<?php echo $_SESSION['nameTab'].": "; ?>");
	    element.className = "co10Per";
	    cell1.appendChild(element);
	    var cell2 = row.insertCell(2);
	    var selector = document.createElement("select");
		selector.name = tagNamePrefix + "_CodeInc[]";
		
		cell2.appendChild(selector);
	    
	    <?php
			foreach($_SESSION['coveragesDefinitions'] as $eachCoverageDefinition)
			{
				if($eachCoverageDefinition->insuranceType == $INSURANCE_TYPE_MOTOR )
				{
					?>
					var found = stringExistsInArray('<?php echo $eachCoverageDefinition->code;?>', existingCodesArray);
					//if code is not found, then use for this list. We shouldn't add coverages when already exists
					if(found==0)
					{
						var option = document.createElement("option");
						option.value = '<?php echo $eachCoverageDefinition->code;?>';
						//for chrome, need to define ALL variables with 'var'
						var name = document.createTextNode('<?php echo $eachCoverageDefinition->code;?>');
						option.appendChild(name);
						selector.appendChild(option);
					}
				    <?php
			    }
			}	
		 ?>
	 }
	 catch(e) {
        alert("Inside addRowInOptionalCoveragesTable. Error:" + e);
    }
       
}



/* check if the sting is part of the array.
stringName: name to search
arrayName: an array of strings
*/
function stringExistsInArray(stringName, arrayName)
{
	try
	{
		
		var result = 0;
		
		for (var i = 0; i < arrayName.length; i++) 
		{
			if(arrayName[i]==stringName)
			{
				result = 1;
				return result;
			}
		}
	}
	catch(e) {
        alert("Inside stringExistsInArray. Error:" + e);
    }
	
	return result;
}


/* validate all insured persons in medical */
function validateInsuredMedicalPersons(medicalInsuredPersonsTableId)
{
	var complete = true;
	var error;
	this.complete = complete;
    this.error = "";
	
	try 
	{
	    var table = document.getElementById(medicalInsuredPersonsTableId);
	    var rowCount = table.rows.length;
	
	    //bypass first 2 rows that contains the header and title.
	    for(var i=2; i<rowCount; i++) 
	    {
	        var row = table.rows[i];
	        var firstName = row.cells[1].childNodes[0].value;
	        var lastName = row.cells[2].childNodes[0].value;
	        var stateId = row.cells[3].childNodes[0].value;
	        			
	        if(stateId=="" || firstName=="" || lastName=="")
	        {
				this.error = this.error + "First name or Surname and StateId are mandatory\n";
				this.complete = false;
			 }
					
		 }
	 }
	 catch(e) {
        alert("Inside validateInsuredMedicalPersons. Error:" + e);
    }
    
	return this;
	
	
}

/* add generic row in a categories table */
function addRowInCategoriesTable(tableID, fromName, toName, euroName, percentChargeName, allowedName, minimumEuroName)
{

	try
	{
		
	    var table = document.getElementById(tableID);
		var columnSize = "<?php echo $TEXT_BOX_SIZE; ?>"
	    var rowCount = table.rows.length;
	    var row = table.insertRow(rowCount);
		var element;
		
		//alert(tagNamePrefix);
		
		//checkbox
	    var cell0 = row.insertCell(0);
	    element = document.createElement("input");
	    element.type = "checkbox";
	    element.name = "chkCategory[]";
	    element.size = columnSize;
	    element.className = "co1Per";
	    cell0.appendChild(element);
	    
	    //FROM text
	    var cell1 = row.insertCell(1);
	    cell1.className = "co1Per";
	    element = document.createTextNode("<?php echo $_SESSION['fromTab'].": "; ?>");
	    cell1.appendChild(element);
	    
	    //from input
	    var cell2 = row.insertCell(2);
	    cell2.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = fromName;
	    cell2.appendChild(element);
	
	    //TO 
	    var cell3 = row.insertCell(3);
	    cell3.className = "co1Per";
	    element = document.createTextNode("<?php echo $_SESSION['toTab'].": "; ?>");
	    cell3.appendChild(element);
	    
	    //to input
	    var cell4 = row.insertCell(4);
	    cell4.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = toName;
	    cell4.appendChild(element);
	    
	    //EURO
	    var cell5 = row.insertCell(5);
	    cell5.className = "co1Per";
	    element = document.createTextNode("<?php echo $EURO." "; ?>");
	    cell5.appendChild(element);
	    
	    //euro input
	    var cell6 = row.insertCell(6);
	    cell6.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = euroName;
	    cell6.appendChild(element);
	    
	    //percentage
	    var cell7 = row.insertCell(7);
	    cell7.className = "co1Per";
	    element = document.createTextNode("<?php echo "% "; ?>");
	    cell7.appendChild(element);
	    
	    //percentage input
	    var cell8 = row.insertCell(8);
	    cell8.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = percentChargeName;
	    cell8.appendChild(element);
	  
	   	 
		//allowed
	    var cell9 = row.insertCell(9);
	    cell9.className = "co10Per";
	    element = document.createTextNode("<?php echo $_SESSION['allowedTab'].' '; ?>");
	    cell9.appendChild(element);
	    var selector = document.createElement("select");
		selector.name = allowedName;;
	    cell9.appendChild(selector);
	    
	    <?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $eachOption)
			{
				?>
				var option = document.createElement("option");
				option.value = '<?php echo $eachOption->value;?>';
				//for chrome, need to define ALL variables with 'var'
				var name = document.createTextNode('<?php echo $eachOption->name;?>');
				option.appendChild(name);
				selector.appendChild(option);
			<?php
			}	
		 ?>
		 
		//minimum charge
	    var cell10 = row.insertCell(10);
	    cell10.className = "co1Per";
	    element = document.createTextNode("<?php echo $_SESSION['minimumCharge'].' '; ?>");
	    cell10.appendChild(element);
	    
	    //minimum charge
	    var cell11 = row.insertCell(11);
	    cell11.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = minimumEuroName;
	    cell11.appendChild(element);
    }
    catch(e) {
        alert("Inside addRowInCategoriesTable. Error:" + e);
    }
	 
	 
}


/* add generic row in a categories table */
function addRowInTypeCategoriesTable(tableID, typeName, euroName, percentChargeName, allowedName, minimumEuroName)
{

	try
	{
		
	    var table = document.getElementById(tableID);
		var columnSize = "<?php echo $TEXT_BOX_SIZE; ?>"
	    var rowCount = table.rows.length;
	    var row = table.insertRow(rowCount);
		var element;
		
		//alert(tagNamePrefix);
		
		//checkbox
	    var cell0 = row.insertCell(0);
	    element = document.createElement("input");
	    element.type = "checkbox";
	    element.name = "chkCategory[]";
	    element.size = columnSize;
	    element.className = "co1Per";
	    cell0.appendChild(element);
	    
	    //type text
	    var cell1 = row.insertCell(1);
	    cell1.className = "co1Per";
	    element = document.createTextNode("<?php echo $_SESSION['typeTab'].": "; ?>");
	    cell1.appendChild(element);
	    
	    //type input
	    var cell2 = row.insertCell(2);
	    cell2.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = typeName;
	    cell2.appendChild(element);
	
	    
	    //EURO
	    var cell3 = row.insertCell(3);
	    cell3.className = "co1Per";
	    element = document.createTextNode("<?php echo $EURO." "; ?>");
	    cell3.appendChild(element);
	    
	    //euro input
	    var cell4 = row.insertCell(4);
	    cell4.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = euroName;
	    cell4.appendChild(element);
	    
	    //percentage
	    var cell5 = row.insertCell(5);
	    cell5.className = "co1Per";
	    element = document.createTextNode("<?php echo "% "; ?>");
	    cell5.appendChild(element);
	    
	    //percentage input
	    var cell6 = row.insertCell(6);
	    cell6.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = percentChargeName;
	    cell6.appendChild(element);
	  
	   	 
		//allowed
	    var cell7 = row.insertCell(7);
	    cell7.className = "co10Per";
	    element = document.createTextNode("<?php echo $_SESSION['allowedTab'].' '; ?>");
	    cell7.appendChild(element);
	    var selector = document.createElement("select");
		selector.name = allowedName;;
	    cell7.appendChild(selector);
	    
	    <?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $eachOption)
			{
				?>
				var option = document.createElement("option");
				option.value = '<?php echo $eachOption->value;?>';
				//for chrome, need to define ALL variables with 'var'
				var name = document.createTextNode('<?php echo $eachOption->name;?>');
				option.appendChild(name);
				selector.appendChild(option);
			<?php
			}	
		 ?>
		 
		//minimum charge
	    var cell8 = row.insertCell(8);
	    cell8.className = "co1Per";
	    element = document.createTextNode("<?php echo $_SESSION['minimumCharge'].' '; ?>");
	    cell8.appendChild(element);
	    
	    //minimum charge
	    var cell9 = row.insertCell(9);
	    cell9.className = "co10Per";
	    element = document.createElement("input");
	    element.type = "text";
	    element.size = columnSize;
	    element.name = minimumEuroName;
	    cell9.appendChild(element);
    }
    catch(e) {
        alert("Inside addRowInTypeCategoriesTable. Error:" + e);
    }
	 
	 
}

/* validate the categories table  */
function checkCategoriesTable(tableID)
{
	var complete=true;
	var error = "Please correct following issues:\n";
	
	try 
	{
	    var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
	
	    for(var i=0; i<rowCount; i++) 
	    {
	        var row = table.rows[i];
	        var from = row.cells[1].childNodes[1].value;
	        alert(from);
	        
		 }
	 }
	 catch(e) {
        alert("Inside checkCategoriesTable. Error:" + e);
    }
    
	 if( complete == false )
		window.alert(error);	
		
	 return complete;
		
}


function deleteTransactionAjax(transId)
{
	//alert("transid=" + transId);
	//file to execute
	url = "./transaction/deleteTransactionAjax.php";
	
	queryString = "?transId=" + transId;
	
	myFunction(url+queryString);
	
	//hide row
	document.getElementById(transId).style.display = 'none';
	
    return true;
}


function deleteUploadedFileAjax(filePath)
{
	//alert("filePath=" + filePath);
	//file to execute
	url = "./uploadFilesManagement/deleteUploadedFileAjax.php";
	
	queryString = "?filePath=" + filePath;
	
	myFunction(url+queryString);
	
	//hide row
	document.getElementById(filePath).style.display = 'none';
	
    return true;
}


function deleteNoteAjax(notesId)
{
	//alert("notesId=" + notesId);
	//file to execute
	url = "./notes/deleteNoteAjax.php";
	
	queryString = "?notesId=" + notesId;
	
	myFunction(url+queryString);
	
	//hide row
	document.getElementById("note" + notesId).style.display = 'none';
	
    return true;
}

/* create a new pop-up on the center of the screen.
Load the file ./generalIncludes/sendEmail.php to fill the email information
email: all emails to send to
globalFilesLocation: send this session variable, to load all the rest session variables
lang: default language
allStateIds: comma separated state id that correspond to the emails
 */
function send_email(email, globalFilesLocation, systemEmail, lang, clientName, allStateIds)
{
	try 
	{
		//alert(lang);
		//if(email=="")
		//	alert("<?php echo $_SESSION['emailIsEmpty']; ?>");
		
		var w = 400;
        var h = 400;
        var left = Number((screen.width/2)-(w/2));
        var tops = Number((screen.height/2)-(h/2));
  		//return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		newwindow=window.open("./generalIncludes/sendEmail.php?email="+email+"&globalFilesLocation="+globalFilesLocation+"&systemEmail="+systemEmail+"&lang="+lang+'&clientName='+clientName+'&allStateIds='+allStateIds,'Send Email','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		if (window.focus) {newwindow.focus()}
			return false;
    }
    catch(e) {
        alert("Inside send_email. Error:" + e);
    }
    
}



</script>