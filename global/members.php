<?php
//crm office console

// Set flag that this is a parent file.
define('_INC', 1);
define('ENV', 'linux');//localhost or linux

//set unicode, for multilingual support
header('Content-Type: text/html; charset=utf-8');
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter("must-revalidate");


//set maximun execution time to 300 seconds, so that no timeouts to occur
ini_set('max_execution_time', 300);

$globalFilesLocation = ".";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/database/connect.php");
require_once($globalFilesLocation."/generalIncludes/globalFunctions.php");

@session_start();
//$standalone = 1;
$_SESSION['globalFilesLocation'] = $globalFilesLocation;
$username = '';
$password = '';

//aristosa added 29/09/2011
$action = '';//initialize action
$actionRead = '';//action read from POST or GET. Can be empty during language change

//@$password=$_POST['password'];
//$clientFilesLocation = '';
$clientName = '';

//shows that session is run using global/quotation.php or global/office.php, instead of client index.php
$runFromGlobalLocation = true;
//session is run from global/office.php
$runFromGlobalLocationQuotation = false;
$runFromGlobalLocationOffice = false;

$productType = $PRODUCT_TYPE_MEMBERS;
$productsArray = array($productType, $PRODUCT_TYPE_ALL);
//print_r($productsArray);

$showEchoes = false;//for testing. shows/hides all echos


//call authentication module - no logs can be printed here, if it is before the startLogging.php
require_once($_SESSION['globalFilesLocation']."/authentication/authentication.php");

//clientFilesLocation is set in authentication.php
//require_once($_SESSION['clientFilesLocation']."/generalIncludes/localConstants.php");

//start logging mechinism
require_once($_SESSION['globalFilesLocation']."/generalIncludes/startLogging.php");

//include common headers and initialization code
require_once($_SESSION['globalFilesLocation']."/generalIncludes/globalIncludes.php");

//include common headers and initialization code
//require_once($_SESSION['globalFilesLocation']."/authentication/retrieveAgents.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	
	
	
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Membership Site</title>
	<link rel="stylesheet" href="./styles.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="./print.css" media="print" />
	
	</head>
	
	<script type="text/javascript">		
	/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
	function setDefaultValues()
	{
	
		
	}

	function printpage()
	{
		
		//hide the additional drivers - not shown in print.
		document.getElementById("colortab").style.display = 'none';
		document.getElementById("message").style.display = 'none';
		window.print();
		//show again after the print
		document.getElementById("colortab").style.display = 'inline';
		document.getElementById("message").style.display = 'inline';
		
	}  
	
	
	</script>
	
	<body onload="setDefaultValues();return true;">
	  
	<?php
	
		setAction();
		
		if($showEchoes==true)	
			echo "Session[login] = " . @$_SESSION['login'] .", isset=" . isset($_SESSION['login']) . "<br>";	
		
		//maybe we also login in same browser, in another product. So logout from here.
		if(isset($_SESSION['productType']) && !in_array($_SESSION['productType'], $productsArray))
		{
			$action = 'logout';
		}
		else
		{
			//during logout, we don't display the menu.
			if($_SESSION['action']!='logout' && $_SESSION['action']!='loginFormProcess')
				require_once($_SESSION['globalFilesLocation']."/members/membersMenu.php");
		}
		
		//Perform actions only when user is logged-in and is not a Customer, 
		//because Customers create accounts themselves in quotation.php and should not have access in office.php
		//echo $_SESSION['role'];
		//echo $_SESSION['login'];
		if(isset($_SESSION['login']) && $_SESSION['login']== true && $_SESSION['role'] > $USER_ROLE_CUSTOMER )
		{
 			switch($action)
			{
				case '':
				case 'homePage':
					?>
					<div id="main">
					<IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/members2.jpg">
					<?php
					break;		
				case 'logout':
					//destroy all session variables
					session_unset();
					session_destroy();
					//echo "inside logout option <br/>";
					//show login screen only
					$_SESSION['globalFilesLocation'] = ".";
					include $_SESSION['globalFilesLocation']."/authentication/authentication.php";
					setLanguage($lang);
					require_once("./members/membersMenu.php");
					break;
				case 'userFindMenu':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/users/userFindMenu.php";
					break;
				case 'userFindMenuProcess':
				?>
				<div id="main">
				<?php
					//process find user menu
					include $_SESSION['globalFilesLocation']."/users/userFindMenuProcess.php";
				break;
				case 'reportsMenu':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/reports/reportsMenu.php";
					break;
				case 'statisticsAnalysis':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/statistics/statistics.php";
					break;
				case 'updateUserDataForm':
				?>
				<div id="main">
				<?php
					if(isset($_POST['searchUserName']))
						$_SESSION['searchUserName'] = $_POST['searchUserName'];
					//echo "searchusername=" . $_SESSION['searchUserName'];
					include $_SESSION['globalFilesLocation']."/users/updateUserDataForm.php";
					break;
				case 'updateUserDataProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/users/updateUserDataProcess.php";
					break;
				case 'displayPreviousQuotes':
				?>
				<div id="main">
				<?php
					include ($_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/displayPreviousQuotes.php");
					break;
				case 'updatePasswordForm':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/authentication/updatePasswordForm.php";
					break;
				case 'updatePasswordFormProcess':
				?>
				<div id="main">
				<?php
					$_SESSION['password2'] = $_POST['password2'];
					include $_SESSION['globalFilesLocation']."/authentication/updatePasswordFormProcess.php";
					break;
				case 'deleteUser':
				?>
				<div id="main">
				<?php
					//delete a user, like an employee or administrator
					if(isset($_POST['deleteUserName']))
						$_SESSION['deleteUserName'] = $_POST['deleteUserName'];
					include $_SESSION['globalFilesLocation']."/users/deleteUser.php";
					break;
				case 'glossary':
				?>
				<div id="main">
				<?php
					@$_SESSION['location'] = $_POST['location'];
					include $_SESSION['globalFilesLocation']."/generalModules/glossary.php";
					break;
				case 'findUserFormProcess':
				?>
				<div id="main">
				<?php
					//process find user menu
					include $_SESSION['globalFilesLocation']."/administrator/findUserFormProcess.php";
					break;
				case 'newUserForm':
				?>
				<div id="main">
				<?php
					//show new user form
					//echo "new user form <br>";
					include $_SESSION['globalFilesLocation']."/users/newUserForm.php";
					break;
				case 'newUserFormProcess':
				?>
				<div id="main">
				<?php
					//create new user
					include $_SESSION['globalFilesLocation']."/users/newUserFormProcess.php";
					break;
				case 'newContractForm':
				?>
				<div id="main">
				<?php
					//show new contract form
					//echo "new contract form with affected rows=".$_SESSION['affectedRows']."<br>";
					include $_SESSION['globalFilesLocation']."/sale/newContractForm.php";
					break;
				case 'newContractFormProcess':
				?>
				<div id="main">
				<?php
					//process new contract form
					//echo "new contract form with affected rows=".$_SESSION['affectedRows']."<br>";
					include $_SESSION['globalFilesLocation']."/sale/newContractFormProcess.php";
					break;
				case 'yourAccount':
				?>
				<div id="main">
				<?php
					//show user account information
					$_SESSION['searchUserName'] = $_SESSION['username'];
					//$_SESSION['calledFromUserNewContract'] = false;
					//$_SESSION['calledFromMyAccountNewContract'] = true;
					$_SESSION['flow'] = $MY_ACCOUNT_NEW_CONTRACT_FLOW;//needed if we want to add a new contract
					include $_SESSION['globalFilesLocation']."/users/updateUserDataForm.php";
					break;
				
				case 'superMenu':
				?>
				<div id="main">
				<?php
					//display the SUPER menu
					include $_SESSION['globalFilesLocation']."/administrator/superMenu.php";
					break;		
				case 'applyDatabasePatches':
				?>
				<div id="main">
				<?php													
					//apply the patches to databases
					include $_SESSION['globalFilesLocation']."/administrator/applyDatabasePatches.php";
					break;	
				case 'manageAccountsMenu':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/administrator/manageAccountsMenu.php";
					break;	
				case 'accountConfiguration':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/administrator/accountConfiguration.php";
					break;	
				case 'accountConfigurationFormProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/administrator/accountConfigurationFormProcess.php";
					break;		
					
		}
		?>
    	</div>
		<?php
		
	}
	//when user is not logged-in, display menu again(only login screen will appear, because no access is allowed from adminTopMenu.php )
	else
	{
		
		
		switch($action)
		{
			case 'loginFormProcess':
				?>
				<div id="main">
				<?php
				include $_SESSION['globalFilesLocation']."/authentication/loginFormProcess.php";
				break;
			default:
				?>
				<div id="main">
				<?php
				//echo "inside admin 2 ";
				require_once($_SESSION['globalFilesLocation']."/members/membersMenu.php");
				//echo "invalid switch case in office <br/>";
				break;
		}
			
	}	
		
		
	?>

	</body>
</html>