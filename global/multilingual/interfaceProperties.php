<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

	
/*reads and stores all information read from interfaceProperties.xml
 it is called only once, at the quotation.php or office.php. */
function readInterfaceProperties()
{
	//echo "reading interface properties <br>";
	
        
	//writeToFile(date("Y-m-d H:i:s"). ": readInterfaceProperties() ENTER \n", LOG_LEVEL_INFO);
        
	//Step 1 - Read multilingual from Gloabl Files Location
	$doc = new DOMDocument();
	$doc->load( $_SESSION['globalFilesLocation']."/multilingual/interfaceProperties". $_SESSION['lang'] . ".xml" );
	
	//yesNoSelectOption
	setMultilingualForVariable($doc, "yesNoSelectOption");
	
	//HOME TAB
	setMultilingualForVariable($doc, "homeTab");
	
	//PACKAGES TAB
	setMultilingualForVariable($doc, "packageTab");
	
	//XML TAB
	setMultilingualForVariable($doc, "xmlTab");
		
	//USER TAB
	setMultilingualForVariable($doc, "usersTab");
	
	//GENERAL TAB
	setMultilingualForVariable($doc, "generalTab");
	
	//STATISTICS TAB
	setMultilingualForVariable($doc, "statisticsTab");
	
	//LOGOUT
	setMultilingualForVariable($doc, "logout");
	
	//LOGIN TAB
	setMultilingualForVariable($doc, "loginTab");
	
	//WELCOME BACK
	setMultilingualForVariable($doc, "welcomeBack");
	
	//youAreNowLoggedIn
	setMultilingualForVariable($doc, "youAreNowLoggedIn");
	
	//youAreNotLoggedIn
	setMultilingualForVariable($doc, "youAreNotLoggedIn");
	
	//password
	setMultilingualForVariable($doc, "password");
	
	//usernameTab
	setMultilingualForVariable($doc, "usernameTab");
	
	//menu
	setMultilingualForVariable($doc, "menu");
	
	//userSearch
	setMultilingualForVariable($doc, "userSearch");
	
	//statisticsAnalysis
	setMultilingualForVariable($doc, "statisticsAnalysis");
	
	//requestsWillBeSentAnonymously
	setMultilingualForVariable($doc, "requestsWillBeSentAnonymously");
	
	//forgotMy
	setMultilingualForVariable($doc, "forgotMy");
	
	//proposerfullName
	setMultilingualForVariable($doc, "proposerfullName");
	
	//birthDate
	setMultilingualForVariable($doc, "birthDate");
	
	//proposerNationality
	setMultilingualForVariable($doc, "proposerNationality");
	
	//profession
	setMultilingualForVariable($doc, "profession");
	
	//countryOfLicense
	setMultilingualForVariable($doc, "countryOfLicense");
	
	//yearsOfNormalLicense
	setMultilingualForVariable($doc, "yearsOfNormalLicense");
	
	//previousCypriotInsuranceTab
	setMultilingualForVariable($doc, "previousCypriotInsuranceTab");
	
	//insuranceCompanyTab
	setMultilingualForVariable($doc, "insuranceCompanyTab");
	
	//vehicleType
	setMultilingualForVariable($doc, "vehicleTypeTab");
	
	//vehicleModelTab
	setMultilingualForVariable($doc, "vehicleModelTab");
	
	//engineCapacity
	setMultilingualForVariable($doc, "engineCapacity");
	
	//proposerDetails
	setMultilingualForVariable($doc, "proposerDetails");
	
	//vehicleDetails
	setMultilingualForVariable($doc, "vehicleDetails");
	
	//manufacturedYear
	setMultilingualForVariable($doc, "manufacturedYear");
	
	//steeringWheelSideTab
	setMultilingualForVariable($doc, "steeringWheelSideTab");
	
	//vehicleSportsModel
	setMultilingualForVariable($doc, "vehicleSportsModel");
	
	//vehicleDesignTab
	setMultilingualForVariable($doc, "vehicleDesignTab");
	
	//vehicleTaxFree
	setMultilingualForVariable($doc, "vehicleTaxFree");
	
	//usefForDeliveries
	setMultilingualForVariable($doc, "usefForDeliveries");
	
	//coverageTypeTab
	setMultilingualForVariable($doc, "coverageTypeTab");
	
	//coverageAmountTab
	setMultilingualForVariable($doc, "coverageAmountTab");
	
	//messageForChoosingComprehensive
	setMultilingualForVariable($doc, "messageForChoosingComprehensive");
	
	//additionalDriversDetails
	setMultilingualForVariable($doc, "additionalDriversDetails");
	
	//namedDriversNumberTab
	setMultilingualForVariable($doc, "namedDriversNumberTab");
	
	//noClaimDiscount
	setMultilingualForVariable($doc, "noClaimDiscount");
	
	//additionalExcessTab
	setMultilingualForVariable($doc, "additionalExcessTab");
	
	//hasDisabilityTab
	setMultilingualForVariable($doc, "hasDisabilityTab");
	
	//penaltyPointsDuringLast3Years
	setMultilingualForVariable($doc, "penaltyPointsDuringLast3Years");
	
	//claimsDuringLast3Years
	setMultilingualForVariable($doc, "claimsDuringLast3Years");
	
	//claim
	setMultilingualForVariable($doc, "claim");
	
	//addAdditionalDrivers
	setMultilingualForVariable($doc, "addAdditionalDrivers");
	
	//additionalDriversQuestions
	setMultilingualForVariable($doc, "additionalDriversQuestions");
	
	//hasLearnersLicenceTab
	setMultilingualForVariable($doc, "hasLearnersLicenceTab");
	
	//ageOfYoungestDriverTab
	setMultilingualForVariable($doc, "ageOfYoungestDriverTab");
	
	//ageOfOldestDriverTab
	setMultilingualForVariable($doc, "ageOfOldestDriverTab");
	
	//selectBenefitsPackage
	setMultilingualForVariable($doc, "selectBenefitsPackage");
	
	//onlyRoadAssistance
	setMultilingualForVariable($doc, "onlyRoadAssistance");
	
	//addSeparateBenefits
	setMultilingualForVariable($doc, "addSeparateBenefits");
	
	//selectSeparateBenefits
	setMultilingualForVariable($doc, "selectSeparateBenefits");
	
	//anyDriverOver23Under70With2YearsLicense
	setMultilingualForVariable($doc, "anyDriverOver23Under70With2YearsLicense");
	
	//anyDriverCanDrive
	setMultilingualForVariable($doc, "anyDriverCanDrive");
	
	//roadAssistanceTab
	setMultilingualForVariable($doc, "roadAssistanceTab");
	
	//passengerLiabilityTab
	setMultilingualForVariable($doc, "passengerLiabilityTab");
	
	//drivingOtherVehiclesOfTheSameTypeTab
	setMultilingualForVariable($doc, "drivingOtherVehiclesOfTheSameTypeTab");
	
	//towTrailer
	setMultilingualForVariable($doc, "towTrailer");
	
	//personalAccidents
	setMultilingualForVariable($doc, "personalAccidents");
	
	//windshieldCoverage
	setMultilingualForVariable($doc, "windshieldCoverage");
	
	//medicalExpenses
	setMultilingualForVariable($doc, "medicalExpenses");
	
	//carLossCoverageTab
	setMultilingualForVariable($doc, "carLossCoverageTab");
	
	//noClaimProtection
	setMultilingualForVariable($doc, "noClaimProtection");
	
	//noChargeDueToAccidentTab
	setMultilingualForVariable($doc, "noChargeDueToAccidentTab");
	
	//deathOfInsuredDriver
	setMultilingualForVariable($doc, "deathOfInsuredDriver");
	
	//anyOtherImportantDetails
	setMultilingualForVariable($doc, "anyOtherImportantDetails");
	
	//submitButton
	setMultilingualForVariable($doc, "submitButton");
	
	//quotationCost
	setMultilingualForVariable($doc, "quotationCost");
	
	//quotationInfo
	setMultilingualForVariable($doc, "quotationInfo");
	
	//packageName
	setMultilingualForVariable($doc, "packageName");
	
	//entryDate
	setMultilingualForVariable($doc, "entryDate");
	
	//previousCompany
	setMultilingualForVariable($doc, "previousCompany");
	
	//coverageType
	setMultilingualForVariable($doc, "coverageType");
	
	//newCompany
	setMultilingualForVariable($doc, "newCompany");
	
	//excessAmount
	setMultilingualForVariable($doc, "excessAmount");
	
	//proposerLicense
	setMultilingualForVariable($doc, "proposerLicense");
	
	//licenseTypeTab
	setMultilingualForVariable($doc, "licenseTypeTab");
	
	//licenseYears
	setMultilingualForVariable($doc, "licenseYears");
	
	//country
	setMultilingualForVariable($doc, "country");
	
	//city
	setMultilingualForVariable($doc, "city");
	
	//additionalChargesTab
	setMultilingualForVariable($doc, "additionalChargesTab");
	
	//comprehensiveDiscounts
	setMultilingualForVariable($doc, "comprehensiveDiscounts");
	
	//years
	setMultilingualForVariable($doc, "years");
	
	//steeringWheelOnLeftSide
	setMultilingualForVariable($doc, "steeringWheelOnLeftSide");
	
	//proposerDoesNotHavePreviousCyprusInsurance
	setMultilingualForVariable($doc, "proposerDoesNotHavePreviousCyprusInsurance");
	
	//driverWithLearnersLicence
	setMultilingualForVariable($doc, "driverWithLearnersLicence");
	
	//vehicleSoftTop
	setMultilingualForVariable($doc, "vehicleSoftTop");
	
	//recentDrivingLicense
	setMultilingualForVariable($doc, "recentDrivingLicense");
	
	//youngDriver
	setMultilingualForVariable($doc, "youngDriver");
	
	//oldDriver
	setMultilingualForVariable($doc, "oldDriver");
	
	//recentClaims
	setMultilingualForVariable($doc, "recentClaims");
	
	//driverHasDisability
	setMultilingualForVariable($doc, "driverHasDisability");
	
	//vehicleOld
	setMultilingualForVariable($doc, "vehicleOld");
	
	//driverWithPenaltyPoints
	setMultilingualForVariable($doc, "driverWithPenaltyPoints");
	
	//additionalBenefits
	setMultilingualForVariable($doc, "additionalBenefits");
	
	//drivingBeyondTheRoad
	setMultilingualForVariable($doc, "drivingBeyondTheRoad");
	
	//euroPerDay
	setMultilingualForVariable($doc, "euroPerDay");
	
	//days
	setMultilingualForVariable($doc, "days");
	
	//notAvailableBenefits
	setMultilingualForVariable($doc, "notAvailableBenefits");
	
	//additionalComments
	setMultilingualForVariable($doc, "additionalComments");
	
	//print
	setMultilingualForVariable($doc, "print");
	
	//purchase
	setMultilingualForVariable($doc, "purchase");
	
	//actualCostMayVary
	setMultilingualForVariable($doc, "actualCostMayVary");
	
	//standardCharges
	setMultilingualForVariable($doc, "standardCharges");
	
	//stamps
	setMultilingualForVariable($doc, "stamps");
	
	//otherCharges
	setMultilingualForVariable($doc, "otherCharges");
	
	//fees
	setMultilingualForVariable($doc, "fees");
	
	//available
	setMultilingualForVariable($doc, "available");
	
	//minimumCharge
	setMultilingualForVariable($doc, "minimumCharge");
	
	//categories
	setMultilingualForVariable($doc, "categories");
	
	//cubicCentimeters
	setMultilingualForVariable($doc, "cubicCentimeters");
	
	//fromTab
	setMultilingualForVariable($doc, "fromTab");
	
	//toTab
	setMultilingualForVariable($doc, "toTab");
	
	//charges
	setMultilingualForVariable($doc, "charges");
	
	//euro
	setMultilingualForVariable($doc, "euro");
	
	//discounts
	setMultilingualForVariable($doc, "discounts");
	
	//number
	setMultilingualForVariable($doc, "number");
		
	//chargeOverBasic
	setMultilingualForVariable($doc, "chargeOverBasic");
	
	//included
	setMultilingualForVariable($doc, "included");
	
	//applicable 
	setMultilingualForVariable($doc, "applicable");
								
	//additionalDrivers
	setMultilingualForVariable($doc, "additionalDrivers");
	
	//age
	setMultilingualForVariable($doc, "age");
	
	//accidentCare
	setMultilingualForVariable($doc, "accidentCare");
	
	//optionalCoverages
	setMultilingualForVariable($doc, "optionalCoverages");
	
	//maxWeight
	setMultilingualForVariable($doc, "maxWeight");
	
	//vehicleAge
	setMultilingualForVariable($doc, "vehicleAge");
	
	//comprehensive
	setMultilingualForVariable($doc, "comprehensive");
	
	//allowedTab
	setMultilingualForVariable($doc, "allowedTab");
	
	//yes
	setMultilingualForVariable($doc, "yes");
	
	//no
	setMultilingualForVariable($doc, "no");
	
	//ageCharges
	setMultilingualForVariable($doc, "ageCharges");
	
	//points
	setMultilingualForVariable($doc, "points");
	
	//chargePerPointTab
	setMultilingualForVariable($doc, "chargePerPointTab");
	
	//typeTab
	setMultilingualForVariable($doc, "typeTab");
	
	//chargeType
	setMultilingualForVariable($doc, "chargeType");
	
	//save
	setMultilingualForVariable($doc, "save");
	
	//stateIdTab
	setMultilingualForVariable($doc, "stateIdTab");
	
	//nameTab
	setMultilingualForVariable($doc, "nameTab");
	
	//surname
	setMultilingualForVariable($doc, "surname");
	
	//residenceAddress
	setMultilingualForVariable($doc, "residenceAddress");
	
	//correspondenceAddress
	setMultilingualForVariable($doc, "correspondenceAddress");
	
	//businessAddress
	setMultilingualForVariable($doc, "businessAddress");
	
	//insuredAddress
	setMultilingualForVariable($doc, "insuredAddress");
	
	//address
	setMultilingualForVariable($doc, "address");
	
	//gender
	setMultilingualForVariable($doc, "gender");
	
	//telephoneTab
	setMultilingualForVariable($doc, "telephoneTab");
	
	//cellphoneTab
	setMultilingualForVariable($doc, "cellphoneTab");
	
	//licenseDateTab
	setMultilingualForVariable($doc, "licenseDateTab");
	
	//updateButton
	setMultilingualForVariable($doc, "updateButton");
	
	//showQuotes
	setMultilingualForVariable($doc, "showQuotes");
	
	//updatePassword
	setMultilingualForVariable($doc, "updatePassword");
	
	//insertNewPassword
	setMultilingualForVariable($doc, "insertNewPassword");
	
	//retypePassword
	setMultilingualForVariable($doc, "retypePassword");
	
	//dontHaveAnAccount
	setMultilingualForVariable($doc, "dontHaveAnAccount");
	
	//create
	setMultilingualForVariable($doc, "create");
	
	//view
	setMultilingualForVariable($doc, "view");
	
	//delete
	setMultilingualForVariable($doc, "delete");
	
	//quotation
	setMultilingualForVariable($doc, "quotation");
	
	//quotationForm
	setMultilingualForVariable($doc, "quotationForm");
	
	//company
	setMultilingualForVariable($doc, "company");
	
	//plan
	setMultilingualForVariable($doc, "plan");
	
	//covers
	setMultilingualForVariable($doc, "covers");
	
	//select
	setMultilingualForVariable($doc, "select");
	
	//findPackages
	setMultilingualForVariable($doc, "findPackages");
	
	//areYouSureYouWantToDeleteThisUser
	setMultilingualForVariable($doc, "areYouSureYouWantToDeleteThisUser");
	
	//areYouSureYouWantToDeleteThisFile
	setMultilingualForVariable($doc, "areYouSureYouWantToDeleteThisFile");
	
	//new
	setMultilingualForVariable($doc, "new");
	
	//open
	setMultilingualForVariable($doc, "open");
	
	//file
	setMultilingualForVariable($doc, "file");
	
	//howDidYouHearAboutUs
	setMultilingualForVariable($doc, "howDidYouHearAboutUs");
	
	//emailSubject
	setMultilingualForVariable($doc, "emailSubject");
	
	//normalDrivingLicenseTotalYears
	setMultilingualForVariable($doc, "normalDrivingLicenseTotalYears");
	
	//areYouSureYouWantToDeleteAllQuotes
	setMultilingualForVariable($doc, "areYouSureYouWantToDeleteAllQuotes");
	
	//deleteAll
	setMultilingualForVariable($doc, "deleteAll");
	
	//canProvideOnline
	setMultilingualForVariable($doc, "canProvideOnline");
	
	//noQuotesFound
	setMultilingualForVariable($doc, "noQuotesFound");
	
	//emailMessages
	setMultilingualForVariable($doc, "emailMessages");
	
	//find
	setMultilingualForVariable($doc, "find");
	
	//code
	setMultilingualForVariable($doc, "code");
	
	//state
	setMultilingualForVariable($doc, "state");
	
	//createUser
	setMultilingualForVariable($doc, "createUser");
	
	//loginFailed
	setMultilingualForVariable($doc, "loginFailed");
	
	//userRole
	setMultilingualForVariable($doc, "userRole");
	
	//userStatus
	setMultilingualForVariable($doc, "userStatus");
	
	//yourAccount
	setMultilingualForVariable($doc, "yourAccount");
	
	//amount
	setMultilingualForVariable($doc, "amount");
	
	//date
	setMultilingualForVariable($doc, "date");
	
	//contractsTab
	setMultilingualForVariable($doc, "contractsTab");
	
	//newContract
	setMultilingualForVariable($doc, "newContract");
	
	//load
	setMultilingualForVariable($doc, "load");
	
	//regNumberTab
	setMultilingualForVariable($doc, "regNumberTab");
	
	//start
	setMultilingualForVariable($doc, "start");
	
	//end
	setMultilingualForVariable($doc, "end");
	
	//contractNumberTab
	setMultilingualForVariable($doc, "contractNumberTab");
	
	//quotationId
	setMultilingualForVariable($doc, "quotationId");
	
	//make
	setMultilingualForVariable($doc, "make");
	
	//debit
	setMultilingualForVariable($doc, "debit");
	
	//credit
	setMultilingualForVariable($doc, "credit");
	
	//remainder
	setMultilingualForVariable($doc, "remainder");
	
	//details
	setMultilingualForVariable($doc, "details");
	
	//transactionDetails
	setMultilingualForVariable($doc, "transactionDetails");
	
	//return
	setMultilingualForVariable($doc, "return");
	
	//nationality
	setMultilingualForVariable($doc, "nationality");
	
	//additionalDriversLicense
	setMultilingualForVariable($doc, "additionalDriversLicense");
	
	//notesTab
	setMultilingualForVariable($doc, "notesTab");
	
	//description
	setMultilingualForVariable($doc, "description");
	
	//administration
	setMultilingualForVariable($doc, "administration");

	//clients
	setMultilingualForVariable($doc, "clients");

	//expired
	setMultilingualForVariable($doc, "expired");
		
	//areYouSureYouWantToDeleteThisContract
	setMultilingualForVariable($doc, "areYouSureYouWantToDeleteThisContract");
	
	//reports
	setMultilingualForVariable($doc, "reports");
	
	//contractsWithRemainder
	setMultilingualForVariable($doc, "contractsWithRemainder");
	
	//discount
	setMultilingualForVariable($doc, "discount");
	
	//total
	setMultilingualForVariable($doc, "total");
	
	//production
	setMultilingualForVariable($doc, "production");
	
	//year
	setMultilingualForVariable($doc, "year");
	
	//cancellations
	setMultilingualForVariable($doc, "cancellations");
	
	//insuranceTypeTab
	setMultilingualForVariable($doc, "insuranceTypeTab");
	
	//employersSocialInsuranceNumber
	setMultilingualForVariable($doc, "employersSocialInsuranceNumber");
	
	//limitPerEmployee
	setMultilingualForVariable($doc, "limitPerEmployee");
	
	//limitPerEventOrSeriesOfEvents
	setMultilingualForVariable($doc, "limitPerEventOrSeriesOfEvents");
	
	//limitDuringPeriodOfInsurance
	setMultilingualForVariable($doc, "limitDuringPeriodOfInsurance");
	
	//employeesNumber
	setMultilingualForVariable($doc, "employeesNumber");
	
	//estimatedTotalGrossEarnings
	setMultilingualForVariable($doc, "estimatedTotalGrossEarnings");
	
	
	//coInsurancePercentage
	setMultilingualForVariable($doc, "coInsurancePercentage");
	
	//deductible
	setMultilingualForVariable($doc, "deductible");
	
	//excess
	setMultilingualForVariable($doc, "excess");
	
	//maximumLimit
	setMultilingualForVariable($doc, "maximumLimit");
	
	//planName
	setMultilingualForVariable($doc, "planName");
	
	//emergencyTravelAssistance
	setMultilingualForVariable($doc, "emergencyTravelAssistance");
	
	//outpatientCoverage
	setMultilingualForVariable($doc, "outpatientCoverage");
	
	//generalMedicalExams
	setMultilingualForVariable($doc, "generalMedicalExams");
	
	//frequencyOfPayment
	setMultilingualForVariable($doc, "frequencyOfPayment");
	
	//premium
	setMultilingualForVariable($doc, "premium");
	
	//employerLiabilityDetails
	setMultilingualForVariable($doc, "employerLiabilityDetails");
	
	//medicalDetails
	setMultilingualForVariable($doc, "medicalDetails");
	
	//propertyFireDetails
	setMultilingualForVariable($doc, "propertyFireDetails");
	
	//insuredPersons
	setMultilingualForVariable($doc, "insuredPersons");
	
	//roomType
	setMultilingualForVariable($doc, "roomType");
	
	//proposerTypeTab
	setMultilingualForVariable($doc, "proposerTypeTab");
		
	//buildingValue
	setMultilingualForVariable($doc, "buildingValue");
	
	//outsideFixturesValue
	setMultilingualForVariable($doc, "outsideFixturesValue");
	
	//contentsValue
	setMultilingualForVariable($doc, "contentsValue");
	
	//valuableObjectsValue
	setMultilingualForVariable($doc, "valuableObjectsValue");
	
	//yearBuilt
	setMultilingualForVariable($doc, "yearBuilt");
	
	//area
	setMultilingualForVariable($doc, "area");
	
	//endorsement
	setMultilingualForVariable($doc, "endorsement");
	
	//propertyType
	setMultilingualForVariable($doc, "propertyType");
	
	//squareMeters
	setMultilingualForVariable($doc, "squareMeters");
	
	//month
	setMultilingualForVariable($doc, "month");
	
	//producerTab
	setMultilingualForVariable($doc, "producerTab");
	
	//all
	setMultilingualForVariable($doc, "all");
	
	//coveragesInPolicyQuotation
	setMultilingualForVariable($doc, "coveragesInPolicyQuotation");
	
	//chargesInPolicyQuotation
	setMultilingualForVariable($doc, "chargesInPolicyQuotation");
	
	//extentionOfCoverAbroad
	setMultilingualForVariable($doc, "extentionOfCoverAbroad");
	
	//trailerLiability
	setMultilingualForVariable($doc, "trailerLiability");
	
	//windshield
	setMultilingualForVariable($doc, "windshield");
	
	//deleteSuccessfull
	setMultilingualForVariable($doc, "deleteSuccessfull");
	
	//continue
	setMultilingualForVariable($doc, "continue");
	
	//addDriver
	setMultilingualForVariable($doc, "addDriver");
	
	//deleteDriver
	setMultilingualForVariable($doc, "deleteDriver");

	// The toolbar menu in the quotation form
	
	//motorTab
	setMultilingualForVariable($doc, "motorTab");
	
	//fireTab
	setMultilingualForVariable($doc, "fireTab");

	//liabilityTab
	setMultilingualForVariable($doc, "liabilityTab");
	
	//employerLiabilityTab
	setMultilingualForVariable($doc, "employerLiabilityTab");
	
	//publicLiabilityTab
	setMultilingualForVariable($doc, "publicLiabilityTab");
	
	//medicalTab
	setMultilingualForVariable($doc, "medicalTab");
	
	//alienMedicalTab
	setMultilingualForVariable($doc, "alienMedicalTab");
	
	//variousTab
	setMultilingualForVariable($doc, "variousTab");
	
	//administratorTab
	setMultilingualForVariable($doc, "administratorTab");
	
	// Sub menu of VARIOUS menu
	
	//transport
	setMultilingualForVariable($doc, "transport");
	
	//glasses
	setMultilingualForVariable($doc, "glasses");
	
	//theft
	setMultilingualForVariable($doc, "theft");
	
	//modifyAddresses
	setMultilingualForVariable($doc, "modifyAddresses");
	
	//addAddress
	setMultilingualForVariable($doc, "addAddress");
	
	//deleteAddress
	setMultilingualForVariable($doc, "deleteAddress");
	
	//addPerson
	setMultilingualForVariable($doc, "addPerson");
	
	//deletePerson
	setMultilingualForVariable($doc, "deletePerson");
	
	//add
	setMultilingualForVariable($doc, "add");
	
	//remove
	setMultilingualForVariable($doc, "remove");
	
	//receipt
	setMultilingualForVariable($doc, "receipt");
	
	//backToContract
	setMultilingualForVariable($doc, "backToContract");
	
	//history
	setMultilingualForVariable($doc, "history");
	
	//tryAgain
	setMultilingualForVariable($doc, "tryAgain");
	
	//parameterName
	setMultilingualForVariable($doc, "parameterName");
	
	//parameterValue
	setMultilingualForVariable($doc, "parameterValue");
	
	//noHistoryFound
	setMultilingualForVariable($doc, "noHistoryFound");
	
	//addSuccessfull
	setMultilingualForVariable($doc, "addSuccessfull");
	
	//noteType
	setMultilingualForVariable($doc, "noteType");
	
	//noDataFound
	setMultilingualForVariable($doc, "noDataFound");

	//Fire Form
	setMultilingualForVariable($doc, "fireFormText");
	
	//basicCoverage
	setMultilingualForVariable($doc, "basicCoverage");
	
	//fireLightningExplosion
	setMultilingualForVariable($doc, "fireLightningExplosion");
	
	//buildings
	setMultilingualForVariable($doc, "buildings");
	
	//buildingValue
	setMultilingualForVariable($doc, "buildingValue");
	
	//additionalDangers
	setMultilingualForVariable($doc, "additionalDangers");
	
	//explosion
	setMultilingualForVariable($doc, "explosion");
	
	//planeFall
	setMultilingualForVariable($doc, "planeFall");
	
	//earthQuake
	setMultilingualForVariable($doc, "earthQuake");
	
	//civilUnrest
	setMultilingualForVariable($doc, "civilUnrest");
	
	//maliciousDamage
	setMultilingualForVariable($doc, "maliciousDamage");
	
	//storm
	setMultilingualForVariable($doc, "storm");
	
	//flooding
	setMultilingualForVariable($doc, "flooding");
	
	//pipesBurst
	setMultilingualForVariable($doc, "pipesBurst");
	
	//impact
	setMultilingualForVariable($doc, "impact");
	
	//outdoorFixtures
	setMultilingualForVariable($doc, "outdoorFixtures");
	
	//solarPanels
	setMultilingualForVariable($doc, "solarPanels");
	
	//outdoorMachinery
	setMultilingualForVariable($doc, "outdoorMachinery");
	
	//antennas
	setMultilingualForVariable($doc, "antennas");
	
	//tentsFixtures
	setMultilingualForVariable($doc, "tentsFixtures");
	
	//contents
	setMultilingualForVariable($doc, "contents");
	
	//valuableObjects
	setMultilingualForVariable($doc, "valuableObjects");
	
	//contentsForBasement
	setMultilingualForVariable($doc, "contentsForBasement");
	
	//optionalCoverages
	setMultilingualForVariable($doc, "optionalCoverages");
	
	//architechsMechanicsFees
	setMultilingualForVariable($doc, "architechsMechanicsFees");
	
	//debrisRemoval
	setMultilingualForVariable($doc, "debrisRemoval");
	
	//rentLoss
	setMultilingualForVariable($doc, "rentLoss");
	
	//reallocationExpenses
	setMultilingualForVariable($doc, "reallocationExpenses");
	
	//unoccupancy
	setMultilingualForVariable($doc, "unoccupancy");
	
	//theft
	setMultilingualForVariable($doc, "theft");
	
	//theftContent
	setMultilingualForVariable($doc, "theftContent");
	
	//theftValObjects
	setMultilingualForVariable($doc, "theftValObjects");
	
	//stock
	setMultilingualForVariable($doc, "stock");
	
	//discounts
	setMultilingualForVariable($doc, "discounts");
	
  //fireExtinguishersOrAlarm
	setMultilingualForVariable($doc, "fireExtinguishersOrAlarm");
	
	//fireSystem
	setMultilingualForVariable($doc, "fireSystem");
	
	//oneMorePolicy
	setMultilingualForVariable($doc, "oneMorePolicy");
	
	//twoMorePolicies
	setMultilingualForVariable($doc, "twoMorePolicies");
		
	//threeOrMorePolicies
	setMultilingualForVariable($doc, "threeOrMorePolicies");
				
	//businessType
	setMultilingualForVariable($doc, "businessType");
				
	//publicLiabilitiy
	setMultilingualForVariable($doc, "publicLiabilitiy");
				
	//employerLiability
	setMultilingualForVariable($doc, "employerLiability");
				
	//totalPremium
	setMultilingualForVariable($doc, "totalPremium");
				
	//stamps
	setMultilingualForVariable($doc, "stamps");
				
	//fees
	setMultilingualForVariable($doc, "fees");
				
	//finalPremium
	setMultilingualForVariable($doc, "finalPremium");
	
	// Employer Liability Form

	// employerLiabilityForm
	setMultilingualForVariable($doc, "employerLiabilityForm");
	
	//territorialLimits
	setMultilingualForVariable($doc, "territorialLimits");
	
	//limitPerEmployee
	setMultilingualForVariable($doc, "limitPerEmployee");
	
	//limitPerEventOrSeriesOfEvents
	setMultilingualForVariable($doc, "limitPerEventOrSeriesOfEvents");
	
	//limitDuringPeriodOfInsurance
	setMultilingualForVariable($doc, "limitDuringPeriodOfInsurance");
	
	//businessType
	setMultilingualForVariable($doc, "businessType");
	
	//employeesNumber
	setMultilingualForVariable($doc, "employeesNumber");
	
	//estimatedTotalGrossEarnings
	setMultilingualForVariable($doc, "estimatedTotalGrossEarnings");
	
	// Public Liability Form
	
	//bodilyInjury
	setMultilingualForVariable($doc, "bodilyInjury");
	
	//propertyDamage
	setMultilingualForVariable($doc, "propertyDamage");
	
	//totalLiabilityLimit
	setMultilingualForVariable($doc, "totalLiabilityLimit");
	
	//foodAndDrinkPoisoning
	setMultilingualForVariable($doc, "foodAndDrinkPoisoning");
	
	//liftsAndEscalators
	setMultilingualForVariable($doc, "liftsAndEscalators");
	
	//fireAndExplosion
	setMultilingualForVariable($doc, "fireAndExplosion");		

	// Medical Form	
	
	//proposer
	setMultilingualForVariable($doc, "proposer");		

	//proposerName
	setMultilingualForVariable($doc, "proposerName");		

	//proposerSurname
	setMultilingualForVariable($doc, "proposerSurname");		

	//email
	setMultilingualForVariable($doc, "email");		

	//identity
	setMultilingualForVariable($doc, "identity");		

	//fax
	setMultilingualForVariable($doc, "fax");		

	//areaCode
	setMultilingualForVariable($doc, "areaCode");		

	//dependents
	setMultilingualForVariable($doc, "dependents");		

	//dependentFirstName
	setMultilingualForVariable($doc, "dependentFirstName");		

	//dependentSurname
	setMultilingualForVariable($doc, "dependentSurname");		

	//dependentBirthDate
	setMultilingualForVariable($doc, "dependentBirthDate");		

	//dependentRelation
	setMultilingualForVariable($doc, "dependentRelation");		

	//maximumCompensation
	setMultilingualForVariable($doc, "maximumCompensation");		

	//outpatientPlanName
	setMultilingualForVariable($doc, "outpatientPlanName");		

	//outpatientMaximumCompensation
	setMultilingualForVariable($doc, "outpatientMaximumCompensation");		

	//additionalCoverages
	setMultilingualForVariable($doc, "additionalCoverages");		

	//Alien Medical Form
	
	//proposerOrEmployer
	setMultilingualForVariable($doc, "proposerOrEmployer");		

	//insured
	setMultilingualForVariable($doc, "insured");		

	//insuredFirstName
	setMultilingualForVariable($doc, "insuredFirstName");		

	//insuredSurname
	setMultilingualForVariable($doc, "insuredSurname");		

	//insuredBirthDate
	setMultilingualForVariable($doc, "insuredBirthDate");		

	//insuredAreaCode
	setMultilingualForVariable($doc, "insuredAreaCode");		

	//insuredCity
	setMultilingualForVariable($doc, "insuredCity");		

	//insuredCountry
	setMultilingualForVariable($doc, "insuredCountry");		

	//insuredProfession
	setMultilingualForVariable($doc, "insuredProfession");		

	
	//loginAgainToSaveTheFile
	setMultilingualForVariable($doc, "loginAgainToSaveTheFile");
	
	//filesManagement
	setMultilingualForVariable($doc, "filesManagement");
	
	//fileName
	setMultilingualForVariable($doc, "fileName");
	
	//notAllowedFileType
	setMultilingualForVariable($doc, "notAllowedFileType");
	
	//fileSizeMustBeSmallerThan
	setMultilingualForVariable($doc, "fileSizeMustBeSmallerThan");
	
	//uploadSuccessfull
	setMultilingualForVariable($doc, "uploadSuccessfull");
	
	//bodilyDamageOrDeath
	setMultilingualForVariable($doc, "bodilyDamageOrDeath");
	
	//thirdPartyPropertyDamage
	setMultilingualForVariable($doc, "thirdPartyPropertyDamage");
	
	//client
	setMultilingualForVariable($doc, "client");
	
	//manageAccounts
	setMultilingualForVariable($doc, "manageAccounts");
	
	//account
	setMultilingualForVariable($doc, "account");
	
	//whenAddingAnAddressStateIdisMandatory
	setMultilingualForVariable($doc, "whenAddingAnAddressStateIdisMandatory");
	
	//userNameAlreadyExists
	setMultilingualForVariable($doc, "userNameAlreadyExists");
	
	//configuration
	setMultilingualForVariable($doc, "configuration");
	
	//updateSuccessfull
	setMultilingualForVariable($doc, "updateSuccessfull");
	
	//yourAccountDoesntAllowMoreUsers
	setMultilingualForVariable($doc, "yourAccountDoesntAllowMoreUsers");
	
	//product
	setMultilingualForVariable($doc, "product");
	
	//coverageTab
	setMultilingualForVariable($doc, "coverageTab");
	
	//existingFiles
	setMultilingualForVariable($doc, "existingFiles");
	
	//insuredPersonName
	setMultilingualForVariable($doc, "insuredPersonName");
	
	//insuredPersonSurname
	setMultilingualForVariable($doc, "insuredPersonSurname");
	
	//basicPlan
	setMultilingualForVariable($doc, "basicPlan");
	
	//totalPermanentDisability
	setMultilingualForVariable($doc, "totalPermanentDisability");
	
	//premiumProtection
	setMultilingualForVariable($doc, "premiumProtection");
	
	//lifeInsDetails
	setMultilingualForVariable($doc, "lifeInsDetails");
	
	//additionalDriversCountryOfLicense
	setMultilingualForVariable($doc, "additionalDriversCountryOfLicense");
	
	//transaction
	setMultilingualForVariable($doc, "transaction");
	
	//transactiontType
	setMultilingualForVariable($doc, "transactionType");
	
	//status
	setMultilingualForVariable($doc, "status");
	
	//discountsInQuotation
	setMultilingualForVariable($doc, "discountsInQuotation");
	
	//previousInsuranceTab
	setMultilingualForVariable($doc, "previousInsuranceTab");
	
	//drivingExperience
	setMultilingualForVariable($doc, "drivingExperience");
	
	//emailIsEmpty
	setMultilingualForVariable($doc, "emailIsEmpty");
	
	//send
	setMultilingualForVariable($doc, "send");
	
	//sendEmailToAll
	setMultilingualForVariable($doc, "sendEmailToAll");
	
	//sendNewsLetter
	setMultilingualForVariable($doc, "sendNewsLetter");
	
	//gettingStartedInstructions
	setMultilingualForVariable($doc, "gettingStartedInstructions");
	
	//subProduct
	setMultilingualForVariable($doc, "subProduct");
	
	//emailPreference
	setMultilingualForVariable($doc, "emailPreference");
        
        //logLevel
	setMultilingualForVariable($doc, "logLevel");
	
	//leads
	setMultilingualForVariable($doc, "leads");
	
	//accountTypeTab
	setMultilingualForVariable($doc, "accountTypeTab");
	
	//payments
	setMultilingualForVariable($doc, "payments");
	
	//SECOND XML FILE - Read multilingual of countries
	$doc = new DOMDocument();
	$doc->load( $_SESSION['globalFilesLocation']."/multilingual/countries". $_SESSION['lang'] . ".xml" );
	//countries
	setMultilingualForVariable($doc, "countries");
	
	//Step 3 - Read multilingual from Client Files Location
	$docClient = new DOMDocument();
	$docClient->load( $_SESSION['clientFilesLocation']."/multilingual/interfaceProperties". $_SESSION['lang'] . ".xml" );
	
	//echo $_SESSION['clientFilesLocation'];
	
	//associate
	setMultilingualForVariable($docClient, "associate");

	//writeToFile(date("Y-m-d H:i:s"). ": readInterfaceProperties() EXIT \n", LOG_LEVEL_INFO);	
}
	
/* read from the loaded file the multilingual description of the variabl.
	$variableName = variable to read. */	

function setMultilingualForVariable($doc, $variableName)
{
	//read variable	
	$types = $doc->getElementsByTagName( $variableName );
	foreach($types as $type)
	{
		//read <display name>
		$names = $type->getElementsByTagName( "displayName" );
		//set variable name dynamically
		foreach($names as $eachName)
			$_SESSION{$variableName} = $eachName->nodeValue;//i.e "findPackages" becomes $_SESSION['findPackages'] and gets a value
		
		//read <displaynamecaps>
		$names = $type->getElementsByTagName( "displayNameCaps" );
		//set variable name dynamically
		foreach($names as $eachName)
			$_SESSION{$variableName."Caps"} = $eachName->nodeValue;//i.e "findPackages" becomes $_SESSION['findPackagesCaps'] and gets a value
			
		//read <displayNamePlural>
		$names = $type->getElementsByTagName( "displayNamePlural" );
		//set variable name dynamically
		foreach($names as $eachName)
			$_SESSION{$variableName."Plural"} = $eachName->nodeValue;//i.e "findPackages" becomes $_SESSION['findPackagesCaps'] and gets a value
			
		//read <displayNamePluralCaps>
		$names = $type->getElementsByTagName( "displayNamePluralCaps" );
		//set variable name dynamically
		foreach($names as $eachName)
			$_SESSION{$variableName."PluralCaps"} = $eachName->nodeValue;//i.e "findPackages" becomes $_SESSION['findPackagesCaps'] and gets a value
			
		//read <displayDescription> if exists
		if( $type->getElementsByTagName( "displayDescription" )->item(0) <> null )
		{
			//set variable name dynamically
			$_SESSION{$variableName."Description"} = $type->getElementsByTagName( "displayDescription" )->item(0)->nodeValue;//i.e $("findPackages") becomes $findPackages and gets a value
		}
			
		//check if <selects> exist, to read the options
		$selects = $type->getElementsByTagName( "select" );
		//select exists -> read <options>
		if($selects->length > 0 )
		{
			foreach($selects as $eachSelect)
			{
				//read all options
				$options = $eachSelect->getElementsByTagName( "option" );
			
				//reset the array to empty each time language is changing
				$_SESSION{$variableName."Options"} = array();
				
				//store options
				foreach($options as $eachOption)
				{
					//options are stored in variable ${$variableName."Options"} with default extention 'Options'
					$optionClass = new generalType();
					
					//$eachOption->nodeValue;
					
					//create new object
					$name;
					$description;
					$value;
						
					$names = $eachOption->getElementsByTagName( "name" );
					foreach($names as $eachName)
						$name = $eachName->nodeValue;
					$values = $eachOption->getElementsByTagName( "value" );
					foreach($values as $eachValue)
						$value = $eachValue->nodeValue;
					if($eachOption->getElementsByTagName( "description" )->item(0) <> null )
					{
						$optionClass -> description = $eachOption->getElementsByTagName( "description" )->item(0)->nodeValue;
					}
					
					//variables that exist only for <emailMessages>
					if($eachOption->getElementsByTagName( "subject" )->item(0) <> null )
					{
						$optionClass -> subject = $eachOption->getElementsByTagName( "subject" )->item(0)->nodeValue;
					}
					if($eachOption->getElementsByTagName( "line1" )->item(0) <> null )
					{
						$optionClass -> line1 = $eachOption->getElementsByTagName( "line1" )->item(0)->nodeValue;
					}
					if($eachOption->getElementsByTagName( "line2" )->item(0) <> null )
					{
						$optionClass -> line2 = $eachOption->getElementsByTagName( "line2" )->item(0)->nodeValue;
					}
						
					//fill object
					$optionClass -> name = $name;
					$optionClass -> value = $value;
					
					//add object to array
					$_SESSION{$variableName."Options"}[] = $optionClass;											
					
				}
			}
		}
	}
	
}
		
	
?>