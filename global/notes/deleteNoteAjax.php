<?php

session_start();

// Set flag that this is a parent file.
define('_INC', 1);

//we have independent file load here.
//we need to move one level down
$globalFilesLocation = "..";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/database/connect.php");
require_once($globalFilesLocation."/database/connectorHierarchy.php");
require_once($globalFilesLocation."/database/deleteDatalayers.php");
require_once($globalFilesLocation."/generalIncludes/globalFunctions.php");

$notesId = $_GET['notesId'];

$affectedRows = deleteFromTable('notes', 'notesId', $notesId);

$_SESSION['affectedRows'] = $affectedRows;

if($_SESSION['affectedRows']==1)
	echo "Note Deleted";
else
	echo "Note Cannot be Delete";
//echo "affected rows = ".$affectedRows."<br>"; //this is printed in the html <div> as the responsetext

//echo "stateId bn is :$user->stateId, firstName is:$user->firstName username = " . $_SESSION['searchUserName'] . "<br>";

?>