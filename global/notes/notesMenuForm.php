<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script>
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "5")
</script>

<script>

$(document).ready(function(){

	var today = new Date();
	
	$("#startDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#startDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#startDate").datepicker('setDate', today);
	    
    $("#endDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#endDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#endDate").datepicker('setDate', today);
   
});
</script>

<table>
	<form name="notesMenuForm" action="./office.php" method="POST">
	<input type="hidden" name="action" value="notesMenuFormProcess">
		<!-- STATEID -->
		<tr>
			<td class="label"><?php echo $_SESSION['stateIdTab']; ?>:</td>
			<td class="input"><input type="text" name="stateId" id="stateId" size="30" value="" />
		</tr>
		<!-- CONTRACT NUMBER -->
		<tr>
			<td class="label"><?php echo $_SESSION['contractNumberTab']; ?>:</td>
			<td class="input"><input type="text" name="contractNumber" id="contractNumber" size="30" value="" />
		</tr>
		<tr>
			<td class="label"><?php echo $_SESSION['start']; ?>:</td>
			<td>
			<input type="text" style="width: 80px;" id="startDate" name="startDate" />
			
			</td>
		</tr>
	
		<tr>
			<td class="label"><?php echo $_SESSION['end']; ?>:</td>
			<td>
			<input type="text" style="width: 80px;" id="endDate" name="endDate" />
			
			</td>
		</tr>
		
		<tr>
			<td class="label"></td>
			<!-- CONTINUE BUTTON -->						
			<td class="input"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" /></td>
		</tr>
	</form>	
</table>

