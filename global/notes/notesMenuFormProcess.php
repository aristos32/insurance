<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$notes = array();

$parameterNameValueArray = array();

if(isset($_POST['contractNumber']) && $_POST['contractNumber'] != '')
{
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "saleId";
	$parameterNameValue->value = "%".$_POST['contractNumber']."%";
	
	$parameterNameValueArray[] = $parameterNameValue;
	
	//$selectStatement = $selectStatement + " userName like ? ";
	//$_SESSION['selectStatement'] = $_POST['userName'];
}

if(isset($_POST['stateId']) && $_POST['stateId'] != '')
{
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "stateId";
	$parameterNameValue->value = "%".$_POST['stateId']."%";
	
	$parameterNameValueArray[] = $parameterNameValue;
	
	//$selectStatement = $selectStatement + " userName like ? ";
	//$_SESSION['selectStatement'] = $_POST['userName'];
}

if(isset($_POST['startDate']))
{
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "startDate";
	$parameterNameValue->value = date("Y-m-d", strtotime($_POST['startDate']));
	$parameterNameValueArray[] = $parameterNameValue;
	
	//$parameterNameValue->printData();
}
//set default date as today
else
{
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "startDate";
	$parameterNameValue->value = date("Y-m-d");
	$parameterNameValueArray[] = $parameterNameValue;
	//$parameterNameValue->printData();
}

if(isset($_POST['endDate']))
{
	//add one day, so that endDate is included in the results
	$endDate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($_POST['endDate'])) . " +1 day"));
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "endDate";
	$parameterNameValue->value = $endDate;
	$parameterNameValueArray[] = $parameterNameValue;
	
	//$parameterNameValue->printData();
}
//set default date as one day after today
else
{
	//add one day, so that endDate is included in the results
	$endDate = date("Y-m-d",strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " +1 day"));
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "endDate";
	$parameterNameValue->value = $endDate;
	$parameterNameValueArray[] = $parameterNameValue;
	//$parameterNameValue->printData();
}


//on language change, POST are not send. So, use ones in session. Keep in session the latest contract we search for.
//if(isset($_POST['startDate']))
//{
	$notes = retrieveNotes($parameterNameValueArray);
	$_SESSION['notes'] = $notes;
//}

if(count($_SESSION['notes']) > 0 )
{
	?>
	<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
		<td><?php echo $_SESSION['date']; ?></td>
		<td><?php echo $_SESSION['typeTab']; ?></td>
		<td><?php echo $_SESSION['parameterName']; ?></td>
		<td><?php echo $_SESSION['parameterValue']; ?></td>
		<td><?php echo $_SESSION['description']; ?></td>
		
	</tr>
	<?php
	foreach($_SESSION['notes'] as $eachNote)
	{
		
		?>					
		<tr id="<?php echo "note".$eachNote->notesId;?>">
			<td><?php echo $eachNote->entryDate; ?></td>
			<td><?php echo $eachNote->type; ?></td>
			<td><?php echo $eachNote->parameterName; ?></td>
			<td><?php echo $eachNote->parameterValue; ?></td>
			<td><?php echo $eachNote->description; ?></td>
			<td class="col1Per"><a href="javascript:;" onclick="javascript:deleteNoteAjax('<?php echo $eachNote->notesId;?>');"><img src="./images/delete-sign.jpg"/> </a></td>
		</tr>	
		
		<?php
	}
	?>
	
	</table>
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="notesMenuForm" method="POST" style="display: none;">
	<input type="text" name="action" value="notesMenu" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('notesMenuForm').submit()"><?php echo $_SESSION['continue']; ?></a>
	
	<?php

}
//no notes found
else 
{
	echo $_SESSION['noDataFound']; ?>.  
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="notesMenuForm" method="POST" style="display: none;">
	<input type="text" name="action" value="notesMenu" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('notesMenuForm').submit()"><?php echo $_SESSION['tryAgain']; ?></a>
	<?php
}
?>
