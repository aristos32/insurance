<?php
//crm office console

// Set flag that this is a parent file.
define('_INC', 1);
define('ENV', 'linux');//localhost or linux

//session_start();

//set unicode(UTF-8), for multilingual support
header('Content-Type: text/html; charset=utf-8');
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter("must-revalidate");


//set maximun execution time to 300 seconds, so that no timeouts to occur
ini_set('max_execution_time', 300);
//phpinfo();

//error_reporting(E_ALL);

$globalFilesLocation = ".";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/generalIncludes/globalFunctions.php");

session_start();
//$standalone = 1;
$_SESSION['globalFilesLocation'] = $globalFilesLocation;
$username = '';
$password = '';

//aristosa added 29/09/2011
$action = '';//initialize action
$actionRead = '';//action read from POST or GET. Can be empty during language change

//@$password=$_POST['password'];
//$clientFilesLocation = '';
$clientName = '';

//shows that session is run using global/quotation.php or global/office.php, instead of client index.php
$runFromGlobalLocation = true;
//session is run from global/office.php
$runFromGlobalLocationQuotation = false;
$runFromGlobalLocationOffice = true;

$productType = $PRODUCT_TYPE_OFFICE;
$productsArray = array($productType, $PRODUCT_TYPE_ALL);

$showEchoes = false;//for testing. shows/hides all echos


//call authentication module - no logs can be printed here, if it is before the startLogging.php
require_once($_SESSION['globalFilesLocation']."/authentication/authentication.php");

//clientFilesLocation is set in authentication.php
//require_once($_SESSION['clientFilesLocation']."/generalIncludes/localConstants.php");

//start logging mechinism
require_once($_SESSION['globalFilesLocation']."/generalIncludes/startLogging.php");

//include common headers and initialization code
require_once($_SESSION['globalFilesLocation']."/generalIncludes/globalIncludes.php");

//include common headers and initialization code
//require_once($_SESSION['globalFilesLocation']."/authentication/retrieveAgents.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	
	
	
	<html>
	<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cyprus Home and Car Insurances. Best Offers for Car Insurance. Get Car Quote Instantly!</title>
	<link rel="stylesheet" href="./styles.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="./print.css" media="print" />
	
      
	<script type="text/javascript">		
	/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
	function setDefaultValues()
	{
	
		
	}

	function printpage()
	{
		
		//hide the additional drivers - not shown in print.
		document.getElementById("colortab").style.display = 'none';
		document.getElementById("message").style.display = 'none';
		window.print();
		//show again after the print
		document.getElementById("colortab").style.display = 'inline';
		document.getElementById("message").style.display = 'inline';
		
	}  
	
	
	</script>
        
	</head>
	
	
	
	<body onload="setDefaultValues();return true;">
	  
	<?php
	
		setAction();
		
		if($showEchoes==true)	
			echo "Session[login] = " . @$_SESSION['login'] .", isset=" . isset($_SESSION['login']) . "<br>";	
		
		//maybe we also login in same browser, in another product. So logout from here.
		if(isset($_SESSION['productType']) && !in_array($_SESSION['productType'], $productsArray))
		{
			$action = 'logout';
		}
		else
		{
			//during logout, we don't display the menu.
			if($_SESSION['action']!='logout' && $_SESSION['action']!='loginFormProcess')
				require_once($_SESSION['globalFilesLocation']."/administrator/officeMenu.php");
		}
		
		//include "./database/fixCharactedEncodings.php";
		//exit;
		
		//Perform actions only when user is logged-in and is not a Customer, 
		//because Customers create accounts themselves in quotation.php and should not have access in office.php
		//echo $_SESSION['role'];
		//echo $_SESSION['login'];
		if(isset($_SESSION['login']) && $_SESSION['login']== true && $_SESSION['role'] > $USER_ROLE_CUSTOMER )
		{
 			switch($action)
			{
				case 'logout':
					//destroy all session variables
					session_unset();
					session_destroy();
					//echo "inside logout option <br/>";
					//show login screen only
					$_SESSION['globalFilesLocation'] = ".";
					include $_SESSION['globalFilesLocation']."/authentication/authentication.php";
					setLanguage($lang);
					require_once("./administrator/officeMenu.php");
					break;
				case 'userFindMenu':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/users/userFindMenu.php";
				?>
				</div>
				<?php 
					break;
				case 'userFindMenuProcess':
				?>
				<div id="main">
				<?php
					//process find user menu
					include $_SESSION['globalFilesLocation']."/users/userFindMenuProcess.php";
				?>
				</div>
				<?php 
					break;
				case 'reportsMenu':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/reports/reportsMenu.php";
					?>
				</div>
				<?php 
					break;
				case 'statisticsAnalysis':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/statistics/statistics.php";
					?>
				</div>
				<?php 
					break;
				case 'emailsThatSearchForComprehensive':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/statistics/emailsThatSearchForComprehensive.php";
					?>
				</div>
				<?php 
					break;
				case 'updateUserDataForm':
				?>
				<div id="main">
				<?php
					if(isset($_POST['searchUserName']))
						$_SESSION['searchUserName'] = $_POST['searchUserName'];
					//echo "searchusername=" . $_SESSION['searchUserName'];
					include $_SESSION['globalFilesLocation']."/users/updateUserDataForm.php";
					?>
				</div>
				<?php 
					break;
				case 'updateUserDataProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/users/updateUserDataProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'displayPreviousQuotes':
				?>
				<div id="main">
				<?php
					include ($_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/displayPreviousQuotes.php");
					?>
				</div>
				<?php 
					break;
				case 'updatePasswordForm':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/authentication/updatePasswordForm.php";
					?>
				</div>
				<?php 
					break;
				case 'updatePasswordFormProcess':
				?>
				<div id="main">
				<?php
					$_SESSION['password2'] = $_POST['password2'];
					include $_SESSION['globalFilesLocation']."/authentication/updatePasswordFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'deleteUser':
				?>
				<div id="main">
				<?php
					//delete a user, like an employee or administrator
					if(isset($_POST['deleteUserName']))
						$_SESSION['deleteUserName'] = $_POST['deleteUserName'];
					include $_SESSION['globalFilesLocation']."/users/deleteUser.php";
					?>
				</div>
				<?php 
					break;
				case 'deleteClient':
				?>
				<div id="main">
				<?php
					//delete a client
					if(isset($_POST['clientId']))
						$_SESSION['clientId'] = $_POST['clientId'];
					include $_SESSION['globalFilesLocation']."/clients/deleteClient.php";
					?>
				</div>
				<?php 
					break;
				case 'deleteContract':
				?>
				<div id="main">
				<?php
					//delete a contract
					if(isset($_POST['contractNumber']))
						$_SESSION['contractNumber'] = $_POST['contractNumber'];
					include $_SESSION['globalFilesLocation']."/sale/deleteContract.php";
					?>
				</div>
				<?php 
					break;
				case 'retrieveAllClientContracts':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/sale/findContractFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'glossary':
				?>
				<div id="main">
				<?php
					@$_SESSION['location'] = $_POST['location'];
					include $_SESSION['globalFilesLocation']."/generalModules/glossary.php";
					?>
				</div>
				<?php 
					break;
				case 'findClientForm':
					//echo "type=".$_POST['type']." <br/>";
					if(isset($_POST['type']))
						$_SESSION['type'] = $_POST['type'];
					//echo "type=".$_SESSION['type']." <br/>";
				?>
				<div id="main">
				<?php
					//show find user form
					include $_SESSION['globalFilesLocation']."/clients/findClientForm.php";
					?>
				</div>
				<?php 
					break;
				case 'findClientFormProcess':
					if(isset($_POST['type']))
						$_SESSION['type'] = $_POST['type'];
				?>
				<div id="main">
				<?php
					//process find client formu
					include $_SESSION['globalFilesLocation']."/clients/findClientFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'updateClientDataForm':
				?>
				<div id="main">
				<?php
					if(isset($_POST['stateId']))
						$_SESSION['searchStateId'] = $_POST['stateId'];
					//$_SESSION['calledFromUserNewContract'] = true;
					//$_SESSION['calledFromMyAccountNewContract'] = false;
					$_SESSION['flow'] = $USER_NEW_CONTRACT_FLOW;//needed later in case we create new contract
					include $_SESSION['globalFilesLocation']."/clients/updateClientDataForm.php";
					?>
				</div>
				<?php 
					break;
				case 'newClientDataForm':
					if(isset($_POST['type']))
						$_SESSION['type'] = $_POST['type'];
					//echo "type=".$_SESSION['type']." <br/>";
					//echo "type=$type <br/>"
				?>
				<div id="main">
				<?php
					/* create a new client, that doesn't have any contracts yet */
					//$_SESSION['flow'] = $NEW_CLIENT_FLOW;
					include $_SESSION['globalFilesLocation']."/clients/newClientDataForm.php";
					?>
				</div>
				<?php 
					break;
				case 'updateClientDataFormProcess':
					
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/clients/updateClientDataFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'newClientDataFormProcess':
					if(isset($_POST['type']))
						$_SESSION['type'] = $_POST['type'];
					//echo "type=".$_SESSION['type']." <br/>";
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/clients/newClientDataFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'findUserFormProcess':
				?>
				<div id="main">
				<?php
					//process find user menu
					include $_SESSION['globalFilesLocation']."/administrator/findUserFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'newUserForm':
				?>
				<div id="main">
				<?php
					//show new user form
					//echo "new user form <br>";
					include $_SESSION['globalFilesLocation']."/users/newUserForm.php";
					?>
				</div>
				<?php 
					break;
				case 'newUserFormProcess':
				?>
				<div id="main">
				<?php
					//create new user
					include $_SESSION['globalFilesLocation']."/users/newUserFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'newContractForm':
				?>
				<div id="main">
				<?php
					//show new contract form
					//echo "new contract form with affected rows=".$_SESSION['affectedRows']."<br>";
					include $_SESSION['globalFilesLocation']."/sale/newContractForm.php";
					?>
				</div>
				<?php 
					break;
				case 'newContractFormProcess':
				?>
				<div id="main">
				<?php
					//process new contract form
					//echo "new contract form with affected rows=".$_SESSION['affectedRows']."<br>";
					include $_SESSION['globalFilesLocation']."/sale/newContractFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'yourAccount':
				?>
				<div id="main">
				<?php
					//show user account information
					$_SESSION['searchUserName'] = $_SESSION['username'];
					//$_SESSION['calledFromUserNewContract'] = false;
					//$_SESSION['calledFromMyAccountNewContract'] = true;
					$_SESSION['flow'] = $MY_ACCOUNT_NEW_CONTRACT_FLOW;//needed if we want to add a new contract
					include $_SESSION['globalFilesLocation']."/users/updateUserDataForm.php";
					?>
				</div>
				<?php 
					break;
				case 'findContractForm':
				?>
				<div id="main">
				<?php
					//show find contract form
					include $_SESSION['globalFilesLocation']."/sale/findContractForm.php";
					?>
				</div>
				<?php 
					break;
				case 'findContractFormProcess':
				?>
				<div id="main">
				<?php
					//find contract form process
					include $_SESSION['globalFilesLocation']."/sale/findContractFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'insertNewTransactionFormProcess':
				?>
				<div id="main">
				<?php
					//find contract form process
					include $_SESSION['globalFilesLocation']."/transaction/insertNewTransactionFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'updateContractData':
				
					//show update contract data form
					include $_SESSION['globalFilesLocation']."/sale/updateContractData.php";
				
					break;
				case 'updateContractDataProcess':
				?>
				<div id="main">
				<?php
					//process update contract data form
					include $_SESSION['globalFilesLocation']."/sale/updateContractDataProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'insertNewNoteFormProcess':
				?>
				<div id="main">
				<?php
					//insert new note form process
					include $_SESSION['globalFilesLocation']."/notes/insertNewNoteFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'insertNewClaimFormProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/claims/insertNewClaimFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'getContractsByExpiryDate':
				?>
				<div id="main">
				<?php
					//REPORTS
					include $_SESSION['globalFilesLocation']."/reports/getContractsByExpiryDateProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'getContractsWithRemainderProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/reports/getContractsWithRemainderProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'getDiscountsByDate':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/reports/getDiscountsByDate.php";
					?>
				</div>
				<?php 
					break;
				case 'getProduction':
				?>
				<div id="main">
				<?php
					if(isset($_POST['productionType']))
						$_SESSION['productionType'] = $_POST['productionType'];
					
					if( $_SESSION['productionType']==$PRODUCTION_TYPE_PER_COMPANY )
						include $_SESSION['globalFilesLocation']."/reports/getProductionPerCompany.php";
					else if( $_SESSION['productionType']==$PRODUCTION_TYPE_PER_MONTH )
						include $_SESSION['globalFilesLocation']."/reports/getProductionPerMonth.php";
					?>
				</div>
				<?php 
					break;
				case 'getContactsPerProducer':
        		?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/reports/getContactsPerProducer.php";
					?>
				</div>
				<?php 
					break;
				case 'modifyAdditionalDrivers':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/sale/modifyAdditionalDrivers.php";
					?>
				</div>
				<?php 
					break;
				case 'modifyAdditionalDriversProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/sale/modifyAdditionalDriversProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'viewAllContractTransactions':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/transaction/viewAllContractTransactions.php";
					?>
				</div>
				<?php 
					break;
				case 'historyMenu':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/history/historyMenu.php";
					?>
				</div>
				<?php 
					break;
				case 'historyMenuProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/history/historyMenuProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'notesMenuForm':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/notes/notesMenuForm.php";
					?>
				</div>
				<?php 
					break;	
				case 'notesMenuFormProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/notes/notesMenuFormProcess.php";
					?>
				</div>
				<?php 
					break;
				case 'uploadFilesManagement':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/uploadFilesManagement/uploadFilesManagement.php";
					?>
				</div>
				<?php 
					break;	
				case 'uploadFilesManagementProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/uploadFilesManagement/uploadFilesManagementProcess.php";
					?>
				</div>
				<?php 
					break;	
				case 'superMenu':
				?>
				<div id="main">
				<?php
					//display the SUPER menu
					include $_SESSION['globalFilesLocation']."/administrator/superMenu.php";
					?>
				</div>
				<?php 
					break;		
				case 'applyDatabasePatches':
				?>
				<div id="main">
				<?php													
					//apply the patches to databases
					include $_SESSION['globalFilesLocation']."/administrator/applyDatabasePatches.php";
					?>
				</div>
				<?php 
					break;	
				case 'manageAccountsMenu':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/administrator/manageAccountsMenu.php";
					?>
				</div>
				<?php 
					break;	
				case 'accountConfiguration':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/administrator/accountConfiguration.php";
					?>
				</div>
				<?php 
					break;	
				case 'accountConfigurationFormProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/administrator/accountConfigurationFormProcess.php";
					?>
				</div>
				<?php 
					break;		
				case 'transactionReportsMenu':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/administrator/transactionReportsMenu.php";
					?>
				</div>
				<?php 
					break;					
				case 'getTransactionsProcess':
				?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/administrator/getTransactionsProcess.php";
					?>
				</div>
				<?php 
					break;			
				case 'reportsContractsMenu':
					 ?>
				<div id="main">
				<?php
					include $_SESSION['globalFilesLocation']."/reports/reportsContractsMenu.php";
					?>
				</div>
				<?php 
					break;		
				case 'reportsProductionMenu':
					?>
					<div id="main">
					<?php
						include $_SESSION['globalFilesLocation']."/reports/reportsProductionMenu.php";
						?>
					</div>
					<?php 
						break;		
				case 'reportsTransactionMenu':
					?>
					<div id="main">
					<?php
						include $_SESSION['globalFilesLocation']."/reports/reportsTransactionMenu.php";
						?>
					</div>
					<?php 
						break;
				case '':
				case 'homePage':
				default:
					?>
									<div id="main">
									<img src="<?php echo $_SESSION['globalFilesLocation']?>/images/CRM-Icon.jpg" />
									
									<h3>
									
									<?php
									echo $_SESSION['gettingStartedInstructions']."<br/>";
									?>
									
									</h3>
									</div>
									<?php
									include $_SESSION['globalFilesLocation']."/notes/notesMenuFormProcess.php";
									break;		
		
					
		}
		?>
    	
		<?php
		
	}
	//when user is not logged-in, display menu again(only login screen will appear, because no access is allowed from officeMenu.php )
	else
	{
		
		
		switch($action)
		{
			case 'loginFormProcess':
				?>
				<div id="main">
				<?php
				include $_SESSION['globalFilesLocation']."/authentication/loginFormProcess.php";
				?>
				</div>
				<?php 
					break;
			default:
				?>
				<div id="main">
				<?php
				//echo "inside admin 2 ";
				require_once($_SESSION['globalFilesLocation']."/administrator/officeMenu.php");
				//echo "invalid switch case in office <br/>";
				?>
				</div>
				<?php 
					break;
		}
			
	}	
		
		
	?>

	</body>
</html>