<?php

// Set flag that this is a parent file.
define('_INC', 1);
define('ENV', 'linux');//localhost or linux

//set unicode, for multilingual support
header('Content-Type: text/html; charset=utf-8');
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter("must-revalidate");

//quotation.php can run as a standalone site, or called using an iFrame from another site html page. For example of www.cyprus-insurances.com
//$standalone=1, runs like an independent quotation.php
//$standalone=0, it is called and displayed as part of another site
//echo "server Query String : ".$_SERVER['QUERY_STRING'];

//initialize login related variables
$username='';
$password='';
$clientName='';

//called with an i-frame from a different site
if($_SERVER['QUERY_STRING'] != "" )
{
	//separate each variable by '&'
	$variables = explode("&", $_SERVER['QUERY_STRING']);
	foreach($variables as $eachVariable)
	{
		//get each parameter value after the '='
		$parameters = explode("=", $eachVariable);
		switch($parameters[0])
		{
			case 'username':
				$username = $parameters[1];
				break;
			case 'password':
				$password = $parameters[1];
				break;
		}
		//echo $eachVariable."<br/>";
		
	}
	//echo "username=$username, password=$password <br/>";
	
	//$standalone=0;
	//echo "not standalone <br/>";
}

 
/*if(!isset($standalone))
{
	$standalone=1;//default is standalone
}*/

//maybe variable is already set in the caller quotation.php
if(!isset($globalFilesLocation))
{
	$globalFilesLocation = ".";
}

require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/generalIncludes/globalFunctions.php");
//client name can
//1. be send by the caller. We read it in 'clientName'
//2. when we access the site independently, but not login, it is hardcoded as cyprus-insurances and uses according datababase.
//3. when we login in this site, it is set dynamically.

//$clientFilesLocation = '';
$user = new user();
$processingInfoArray = array();//stores processing info for all companies xmls
$selectedProcessingInfo;

//shows that session is run using global/quotation.php or global/office.php, instead of client quotation.php
$runFromGlobalLocation = true;
//session is run from global/quotation.php
$runFromGlobalLocationQuotation = true;
$runFromGlobalLocationOffice = false;
$productType = $PRODUCT_TYPE_QUOTATION;
$productsArray = array($productType, $PRODUCT_TYPE_ALL);//all allowed user product types.

//aristosa added 29/09/2011
$action = '';//initialize action
$actionRead = '';//action read from POST or GET. During language change. Very important to check if we have a POST or just language change!!!

$showEchoes = false;//for testing. shows/hides all echos

@session_start();

$_SESSION['globalFilesLocation'] = $globalFilesLocation;

//call authentication module
require_once($_SESSION['globalFilesLocation']."/authentication/authentication.php");

//clientFilesLocation is set in authentication.php
//require_once($_SESSION['clientFilesLocation']."/generalIncludes/localConstants.php");

//start logging mechinism
require_once($_SESSION['globalFilesLocation']."/generalIncludes/startLogging.php");

//include common headers and initialization code
require_once($_SESSION['globalFilesLocation']."/generalIncludes/globalIncludes.php");

//display login form only for standalone case
//if($standalone==1)
//{
	setAction();
	
	//maybe we also login in same browser, in another product. So logout from here.
	if(isset($_SESSION['productType']) && !in_array($_SESSION['productType'], $productsArray))
	{
		$action = 'logout';
	}
?>
	<div id="topbody">
	<div id="logo">&nbsp;</div>
	<div id="authToolbar">

	<!-- To display the toolbar menu -->
	<?php require_once($_SESSION['globalFilesLocation']."/quotation/quotTopMenu.php");
	
	?>
	</div>
	<div id="language">
	<form action="./quotation.php" id="english" method="post" style="display: none;">
		<input type="hidden" name="lang" value="English" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('english').submit()">English</a> /
	<form action="./quotation.php" id="greek" method="post" style="display: none;">
		<input type="hidden" name="lang" value="Greek" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('greek').submit()">Ελληνικά</a> 
	</div>
	
	<div id="clear"> </div>
	
</div>
	
	
<?php
//}

//standalone website
//if($standalone==1)
//{
	?>
	
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	
	
	
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Cyprus Home and Car Insurances. Best Offers for Car Insurance. Get Car Quote Instantly!</title>
	<link rel="stylesheet" href="./styles.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../print.css" media="print" />
	</head>
	
	<script type="text/javascript">		
	/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
	function setDefaultValues()
	{
		
	}
	function printpage()
	{
		window.print()
	}  
	</script>
	
	<body onload="setDefaultValues();return true;">
		<!-- Added on 7/2/12 by Theodoros Pantelides -- Start -->
		<div id="main">
		<!-- End --> 
	<?php
		
		switch($action)
		{
			case '':
			case 'homePage':
				if($showEchoes==true)
					echo "NO ACTION TAKEN <br> ";
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/vehicleQuoteForm.php";
				break;
			case 'logout':
				session_destroy();
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/vehicleQuoteForm.php";
				break;
			case 'loginForm':
				include $_SESSION['globalFilesLocation']."/authentication/loginForm.php";
				break;
			case 'loginFormProcess':
				include $_SESSION['globalFilesLocation']."/authentication/loginFormProcess.php";
				break;
			case 'carQuoteFormProcess':
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuoteLogic/vehicleQuoteFormProcess.php";
				break;
			case 'glossary':
				@$_SESSION['location'] = $_GET['location'];
				include $_SESSION['globalFilesLocation']."/generalModules/glossary.php";
				break;
			case 'FAQ':
				@$_SESSION['location'] = $_GET['location'];
				include $_SESSION['globalFilesLocation']."/generalModules/FAQ.php";
				break;
			case 'createQuote':
				//create quote and store in database
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuoteLogic/createQuoteInDatabase.php";
				break;
			case 'showBenefits':
				//Show the benefits included in this quotation
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuoteLogic/showBenefits.php";
				break;
			case 'carQuoteForm':
				//display carQuote Form
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/vehicleQuoteForm.php";
				break;
			case 'motor':
				// display motor form
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/vehicleQuoteForm.php";
				break;
			case 'fire':
				// display fire form
				include $_SESSION['globalFilesLocation']."/quotation/fire/fireForm.php";
				break;
			case 'employerLiability':
				// display Employer Liability form
				include $_SESSION['globalFilesLocation']."/quotation/liability/employerLiability.php";
				break;
			case 'medical':
				include $_SESSION['globalFilesLocation']."/quotation/medical/medical.php";
				break;
			case 'alienMedical':
				include $_SESSION['globalFilesLocation']."/quotation/medical/alienMedical.php";
				break;
			case 'transport':
				// display transport form
				include $_SESSION['globalFilesLocation']."/quotation/various/transportForm.php";
				break;
			case 'glasses':
				// display glasses form
				include $_SESSION['globalFilesLocation']."/quotation/various/glassesForm.php";
				break;
			case 'theft':
				// display theft form
				include $_SESSION['globalFilesLocation']."/quotation/various/theftForm.php";
				break;
			case 'personalAccidents':
				// display personalAccidents form
				include $_SESSION['globalFilesLocation']."/quotation/various/personalAccidentsForm.php";
				break;
			case 'quotationFindMenu':
				// display quotation form
				include $_SESSION['globalFilesLocation']."/quotation/administrator/quotationFindMenu.php";
				break;
			case 'quotationFindMenuProcess':
				include $_SESSION['globalFilesLocation']."/quotation/administrator/quotationFindMenuProcess.php";
				break;
			case 'quotePresentation':
				//show quotation
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/quotePresentation.php";
				break;
			case 'deleteQuote':
				$_SESSION['quoteId'] = $_POST['quoteId'];
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/deleteQuote.php";
				break;
			case 'deleteAllQuotes':
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/deleteAllQuotes.php";
				break;
			case 'userFindMenu':
				//echo "userFindMenu";
				// display user form
				include $_SESSION['globalFilesLocation']."/users/userFindMenu.php";
				break;
			case 'userFindMenuProcess':
				//echo "userFindMenuProcess";
				//process find user menu
				include $_SESSION['globalFilesLocation']."/users/userFindMenuProcess.php";
				break;
			case 'yourAccount':
				// display yourAccount form
				//echo "searchusername=" . $_SESSION['searchUserName'];
				$_SESSION['searchUserName'] = $_SESSION['username'];//we update the logged-in user
				include $_SESSION['globalFilesLocation']."/users/updateUserDataForm.php";
				break;
			case 'updateUserDataForm':
				//echo "updateUserDataForm";
				if(isset($_POST['searchUserName']))
					$_SESSION['searchUserName'] = $_POST['searchUserName'];
				//echo "searchusername=" . $_SESSION['searchUserName'];
				include $_SESSION['globalFilesLocation']."/users/updateUserDataForm.php";
				break;
			case 'updateUserDataProcess':
				//echo "updateUserDataProcess";
				include $_SESSION['globalFilesLocation']."/users/updateUserDataProcess.php";
				break;
			case 'deleteUser':
				//delete a user, like an employee or administrator
				if(isset($_POST['deleteUserName']))
					$_SESSION['deleteUserName'] = $_POST['deleteUserName'];
				include $_SESSION['globalFilesLocation']."/users/deleteUser.php";
				break;
			case 'newUserForm':
				//show new user form
				//echo "new user form <br>";
				include $_SESSION['globalFilesLocation']."/users/newUserForm.php";
				break;
			case 'newUserFormProcess':
				//create new user
				include $_SESSION['globalFilesLocation']."/users/newUserFormProcess.php";
				break;
			case 'xml':
				// display XML form
				include $_SESSION['globalFilesLocation']."/quotation/administrator/XMLMenu.php";
				break;	
			case 'displayAllXmlFilesInDir':
				displayAllXmlFilesInDir($_SESSION['clientFilesLocation']."/XMLFiles/");
				break;
			case 'loadXMLFile':
				//load an existing xml file
				//load new file only when send
				//when language is changing, POST is empty
				if(isset($_POST['fileName']))
					$_SESSION['currentInputXMLFileName'] = $_POST['fileName'];
					
				//$_SESSION['basicCoverageInfo'] = readValuesFromCompanyXml($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['currentInputXMLFileName']);
				$_SESSION['basicCoverageInfo'] = readValuesFromCompanyXmlNew($_SESSION['clientFilesLocation']."/XMLFiles/", $_SESSION['currentInputXMLFileName']);
				displayInsCompanyXMLFormNew($_SESSION['basicCoverageInfo']);
				break;
			case 'createNewXMLFile':
				//create a new company XML file
				//read the model file.
				$_SESSION['currentInputXMLFileName'] = "model.xml";
				//$_SESSION['basicCoverageInfo'] = readValuesFromCompanyXml($_SESSION['globalFilesLocation']."/quotation/motor/XMLFiles/model/".$_SESSION['currentInputXMLFileName']);
				$_SESSION['basicCoverageInfo'] = readValuesFromCompanyXmlNew($_SESSION['globalFilesLocation']."/quotation/motor/XMLFiles/", $_SESSION['currentInputXMLFileName']);
				displayInsCompanyXMLFormNew($_SESSION['basicCoverageInfo']);
				break;
			case 'deleteCompanyXML':
				//delete an existing company XML file and all related files
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/deleteCompanyXML.php";
				break;		
			case 'insCompanyXMLFormProcess':
				//delete an existing company XML file
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/insCompanyXMLFormProcess.php";
				break;
			case 'superMenu':
				//display the SUPER menu
				include $_SESSION['globalFilesLocation']."/administrator/superMenu.php";
				break;		
			case 'applyDatabasePatches':
				//apply the patches to databases
				include $_SESSION['globalFilesLocation']."/administrator/applyDatabasePatches.php";
				break;	
			case 'manageAccountsMenu':
				include $_SESSION['globalFilesLocation']."/administrator/manageAccountsMenu.php";
				break;	
			case 'accountConfiguration':
				include $_SESSION['globalFilesLocation']."/administrator/accountConfiguration.php";
				break;		
			case 'accountConfigurationFormProcess':
				include $_SESSION['globalFilesLocation']."/administrator/accountConfigurationFormProcess.php";
				break;
			case 'coverageTypesMenu':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/coveragesMenu.php";
				break;
			case 'displayCoverageFile':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/displayCoverageFile.php";
				break;
			case 'coverageTypeFormProcess':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/coverageTypeFormProcess.php";
				break;
			case 'createNewCoverageFile':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/createNewCoverageFile.php";
				break;
			case 'selectCoverageType':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/selectCoverageType.php";
				break;
			case 'packagesMenu':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/packagesMenu.php";
				break;
			case 'displayPackageFile':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/displayPackageFile.php";
				break;
			case 'packageFormProcess':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/packageFormProcess.php";
				break;
			case 'createNewPackageFile':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/createNewPackageFile.php";
				break;
			case 'optionalCoveragesMenu':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/optionalCoveragesMenu.php";
				break;
			case 'displayOptionalCoveragesFile':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/displayOptionalCoveragesFile.php";
				break;
			case 'optionalCoveragesFormProcess':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/optionalCoveragesFormProcess.php";
				break;
			case 'createOptionalCoveragesFile':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/createOptionalCoveragesFile.php";
				break;
			case 'additionalChargesMenu':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/additionalChargesMenu.php";
				break;
			case 'displayAdditionalChargesFile':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/displayAdditionalChargesFile.php";
				break;
			case 'additionalChargesFormProcess':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/additionalChargesFormProcess.php";
				break;
			case 'createAdditionalChargesFile':
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/createAdditionalChargesFile.php";
				break;
			case 'deleteSingleXML':
				//delete an single XML file
				include $_SESSION['globalFilesLocation']."/quotation/motor/XMLFilesProcess/deleteSingleXML.php";
				break;	
			case 'displayPreviousQuotes':
				//delete an single XML file
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/displayPreviousQuotes.php";
				break;
			default:
				//no matching action found - show car quote form again
				echo "invalid action $action";
				include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/vehicleQuoteForm.php";
				break;
		}
		
		
	?>
  	<!-- Added on 7/2/12 by Theodoros Pantelides -- Start -->
		</div>
		<!-- End -->
	
	</body>
	</html>
<?php

?>