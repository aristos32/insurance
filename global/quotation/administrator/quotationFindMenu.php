<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
?>

<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "5")
</script>

<table>
	<form name="quotationFindMenu" action="./quotation.php" method="POST" onSubmit="return checkQuotationFindMenu()">
	<input type="hidden" name="action" value="quotationFindMenuProcess">
		<!-- QUOTEID -->
		<tr>
			<td class="col20Per">Quote Id:</td>
			<td class="col20Per"><input type="text" name="quoteId" id="quoteId" size="20" value="" />
			<td class="col20Per"></td>
			<td class="col20Per"></td>
			<td class="col20Per"></td>
		</tr>
		<!-- USERNAME  -->
		<tr>
			<td class="col20Per"><?php echo $_SESSION['usernameTab']; ?>:</td>
			<td class="col20Per"><input type="text" name="username" id="username" size="20" value="" /><br /></td>
		</tr>
		
		<!-- CLIENT FIRST NAME  -->
		<tr>
			<td class="col20Per"><?php echo $_SESSION['nameTab']; ?>:</td>
			<td class="col20Per"><input type="text" name="firstName" id="firstName" size="20" value="" /><br /></td>
		</tr>
		<!-- CLIENT LASTNAME -->
		<tr>
			<td class="col20Per"><?php echo $_SESSION['surname']; ?>:</td>
			<td class="col20Per"><input type="text" name="lastName" id="lastName" size="20" value="" /><br /></td>
		</tr>
		<tr>
			<td class="col20Per"></td>
			<!-- CONTINUE BUTTON -->						
			<td class="col20Per"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" /></td>
		</tr>
	</form>	
</table>
