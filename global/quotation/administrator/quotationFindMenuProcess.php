<!-- begin main content -->
<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//on language change, POST are not send. So, use ones in session
if(isset($_POST['quoteId']) && $_POST['quoteId']!='' )
{
	//$_SESSION['searchQuoteId'] = $_POST['quoteId'];
	$quotations = retrieveQuoteInfo('q.quoteId', $_POST['quoteId']);
	$_SESSION['quotations'] = $quotations;
}
else if(isset($_POST['username']) && $_POST['username']!='' )
{
	$quotations = retrieveQuoteInfo('q.userName', "%".$_POST['username']."%");
	$_SESSION['quotations'] = $quotations;
}
else if(isset($_POST['firstName']) && $_POST['firstName']!='' )
{
	$quotations = retrieveQuoteInfo('o.firstName', $_POST['firstName']);
	$_SESSION['quotations'] = $quotations;
}
else if(isset($_POST['lastName']) && $_POST['lastName']!='' )
{
	$quotations = retrieveQuoteInfo('o.lastName', $_POST['lastName']);
	$_SESSION['quotations'] = $quotations;
}



/* FIND QUOTE */
//if($_SESSION['searchQuoteId'] != '')
//{
	
	//$quote->printData();
	if(count($_SESSION['quotations'])==0){
			echo $_SESSION['noQuotesFound'].".";
			?>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="./quotation.php" id="quotationMenu" method="POST" style="display: none;">
			<input type="text" name="action" value="quotationFindMenu" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('quotationMenu').submit()"><?php echo $_SESSION['return']; ?></a>
		<?php
	}
	else
	{
		//$_SESSION['quoteId'] = $_SESSION['searchQuoteId'];
		//header('Location:../phpPages/quotePresentation.php');
		//die();
		?>

		<table>
			<tr>
				<td class="col10Per"><b>ID</b></td>
				<td class="col20Per"><b><?php echo $_SESSION['proposerfullName']; ?></b></td>
				<td class="col20Per"><b><?php echo $_SESSION['entryDate']; ?></b></td>
				<td class="col20Per"><b><?php echo $_SESSION['amount']."(".$EURO.")"; ?></b></td>
				<td class="col10Per"><b><?php echo $_SESSION['view']; ?></b></td>
				<td class="col20Per"><b><?php echo $_SESSION['delete']; ?></b></td>
			</tr>
			<?php
			foreach($_SESSION['quotations'] as $eachQuotation)
			{
				$viewQuotationId = "view_".$eachQuotation->quoteId;
				$deleteQuotationId = "delete_".$eachQuotation->quoteId;
				 ?> 
				<tr>
					<td class="col10Per"><?php echo $eachQuotation->quoteId;?></td>
					<td ><?php echo $eachQuotation->fullName;?></td>
					<td><?php echo $eachQuotation->entryDate;?></td>
					<td class="col20Per"><?php if(strcmp($eachQuotation->canProvideOnlineQuote,$NO_CONSTANT)==0)
										  {
												echo "N/A";
										  }
										  else
										  	echo $eachQuotation->quoteAmount;
						?>
					</td>
					<td class="col10Per">
						<!-- create hidden forn, to send the action as a POST -->
						<form action="./quotation.php" id="<?php echo $viewQuotationId;?>" method="post" style="display: none;">
						<input type="text" name="action" value="quotePresentation" />
						<input type="text" name="quoteId" value=<?php echo $eachQuotation->quoteId;?> />
						</form>
						<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $viewQuotationId;?>').submit()"><?php echo $_SESSION['view']; ?></a>
					</td>
					<td class="col20Per">
						<!-- create hidden forn, to send the action as a POST. -->
						<form action="./quotation.php" id="<?php echo $deleteQuotationId;?>" method="post" style="display: none;">
							<input type="text" name="action" value="deleteQuote" />
							<input type="text" name="quoteId" value=<?php echo $eachQuotation->quoteId;?> />
						</form>
						<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $deleteQuotationId;?>').submit()"><?php echo $_SESSION['delete']; ?></a>
					</td>
				</tr>
				<?php
			}
			?>
		</table>
	<?php
	}
//}
?>
