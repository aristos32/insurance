<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "1")
</script>

<h1 id="h1"><?php echo $_SESSION['fireFormText'];?></h1>

<form name="fireForm" action="./quotation.php" method="post">
	<input type="hidden" name="action" value="carQuoteFormProcess">
	
	<table>

	<!-- BASIC COVERAGE SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['basicCoverage']; ?></h3></td></tr>
	
	<!-- Fire/Lightning/Explosion -->
	<tr>
		<td class="label"><?php echo $_SESSION['fireLightningExplosion']; ?>:</td>
		<td class="input">&nbsp;</td>
	</tr>
	
	<!-- BUILDINGS SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['buildings']; ?></h3></td></tr>	
	
	<!-- BUILDING VALUE -->
	<tr>
		<td class="label"><?php echo $_SESSION['buildingValue']; ?>:</td>
		<td class="input"><input type="text" name="buildingValueID" id="buildingValueID" size="30"  /></td>
	</tr>
	
	<!-- ADDITIONAL DANGERS SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['additionalDangers']; ?></h3></td></tr>	
		
	<!-- EXPLOSION -->
	<tr>
		<td class="label"><?php echo $_SESSION['explosion']; ?>:</td>
		<td class="input"><input type="text" name="explosionID" id="explosionID" size="30" /></td>
	</tr>
	
	<!-- PLANE FALL -->
	<tr>
		<td class="label"><?php echo $_SESSION['planeFall']; ?>:</td>
		<td class="input"><input type="text" name="planeFallID" id="planeFallID" size="30" /></td>
	</tr>
	
	<!-- EARTHQUAKE -->
	<tr>
		<td class="label"><?php echo $_SESSION['earthQuake']; ?>:</td>
		<td class="input"><input type="text" name="earthQuakeID" id="earthQuakeID" size="30" /></td>
	</tr>
	
	<!-- CIVIL UNREST -->
	<tr>
		<td class="label"><?php echo $_SESSION['civilUnrest']; ?>:</td>
		<td class="input"><input type="text" name="civilUnrestID" id="civilUnrestID" size="30" /></td>
	</tr>
	
	<!-- MALICIOUS DAMAGE -->
	<tr>
		<td class="label"><?php echo $_SESSION['maliciousDamage']; ?>:</td>
		<td class="input"><input type="text" name="maliciousDamageID" id="maliciousDamageID" size="30" /></td>
	</tr>
	
	<!-- STORM -->
	<tr>
		<td class="label"><?php echo $_SESSION['storm']; ?>:</td>
		<td class="input"><input type="text" name="stormID" id="stormID" size="30" /></td>
	</tr>
	
	<!-- FLOODING -->
	<tr>
		<td class="label"><?php echo $_SESSION['flooding']; ?>:</td>
		<td class="input"><input type="text" name="floodingID" id="floodingID" size="30" /></td>
	</tr>
	
	<!-- PIPES BURST -->
	<tr>
		<td class="label"><?php echo $_SESSION['pipesBurst']; ?>:</td>
		<td class="input"><input type="text" name="pipesBurstID" id="pipesBurstID" size="30" /></td>
	</tr>
	
	<!-- IMPACT -->
	<tr>
		<td class="label"><?php echo $_SESSION['impact']; ?>:</td>
		<td class="input"><input type="text" name="impactID" id="impactID" size="30" /></td>
	</tr>
	
	<!-- OUTDOOR FIXTURES SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['outdoorFixtures']; ?></h3></td></tr>	
		
	<!-- SOLAR PANELS -->
	<tr>
		<td class="label"><?php echo $_SESSION['solarPanels']; ?>:</td>
		<td class="input"><input type="text" name="solarPanelsID" id="solarPanelsID" size="30" /></td>
	</tr>
	
	<!-- OUTDOORS MACHINERY -->
	<tr>
		<td class="label"><?php echo $_SESSION['outdoorMachinery']; ?>:</td>
		<td class="input"><input type="text" name="outdoorMachineryID" id="outdoorMachineryID" size="30"  /></td>
	</tr>
	
	<!-- ANTENNAS -->
	<tr>
		<td class="label"><?php echo $_SESSION['antennas']; ?>:</td>
		<td class="input"><input type="text" name="antennasID" id="antennasID" size="30"  /></td>
	</tr>
	
	<!-- TENTS -->
	<tr>
		<td class="label"><?php echo $_SESSION['tentsFixtures']; ?>:</td>
		<td class="input"><input type="text" name="tentsFixturesID" id="tentsFixturesID" size="30"  /></td>
	</tr>

	<!-- CONTENTS SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['contents']; ?></h3></td></tr>	
	
	<!-- CONTENTS -->
	<tr>
		<td class="label"><?php echo $_SESSION['contents']; ?>:</td>
		<td class="input"><input type="text" name="contentsID" id="contentsID" size="30"  /></td>
	</tr>
	
	<!-- VALUABLE OBJECTS -->
	<tr>
		<td class="label"><?php echo $_SESSION['valuableObjects']; ?>:</td>
		<td class="input"><input type="text" name="valuableObjectsID" id="valuableObjectsID" size="30"  /></td>
	</tr>
	
	<!-- CONTENTS FOR BASEMENT -->
	<tr>
		<td class="label"><?php echo $_SESSION['contentsForBasement']; ?>:</td>
		<td class="input"><input type="text" name="contentsForBasementID" id="contentsForBasementID" size="30" /></td>
	</tr>
		
	<!-- OPTIONAL COVERAGES SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['optionalCoverages']; ?></h3></td></tr>	
	
	<!-- ARCHITECHS AND MECHANICS FEES -->
	<tr>
		<td class="label"><?php echo $_SESSION['architechsMechanicsFees']; ?>:</td>
		<td class="input"><input type="text" name="architechsMechanicsFeesID" id="architechsMechanicsFeesID" size="30"  /></td>
	</tr>
	
	<!-- DEBRIS REMOVAL -->
	<tr>
		<td class="label"><?php echo $_SESSION['debrisRemoval']; ?>:</td>
		<td class="input"><input type="text" name="debrisRemovalID" id="debrisRemovalID" size="30" /></td>
	</tr>
	
	<!-- LOSS OF RENTS -->
	<tr>
		<td class="label"><?php echo $_SESSION['rentLoss']; ?>:</td>
		<td class="input"><input type="text" name="rentLossID" id="rentLossID" size="30" /></td>
	</tr>
	
	<!-- REALLOCATION EXPENSES -->
	<tr>
		<td class="label"><?php echo $_SESSION['reallocationExpenses']; ?>:</td>
		<td class="input"><input type="text" name="reallocationExpensesID" id="reallocationExpensesID" size="30"  /></td>
	</tr>
	
	<!-- UNOCCUPANCY -->
	<tr>
		<td class="label"><?php echo $_SESSION['unoccupancy']; ?>:</td>
		<td class="input"><input type="text" name="unoccupancyID" id="unoccupancyID" size="30"  /></td>
	</tr>
	
	<!-- THEFT -->
	<tr><td class="h3"><h3><?php echo $_SESSION['theft']; ?></h3></td></tr>	
	
	<!-- CONTENT -->
	<tr>
		<td class="label"><?php echo $_SESSION['theftContent']; ?>:</td>
		<td class="input"><input type="text" name="theftContentID" id="theftContentID" size="30"  /></td>
	</tr>
	
	<!-- VALUABLE OBJECTS -->
	<tr>
		<td class="label"><?php echo $_SESSION['theftValObjects']; ?>:</td>
		<td class="input"><input type="text" name="theftValObjectsID" id="theftValObjectsID" size="30"  /></td>
	</tr>
	
	<!-- STOCK -->
	<tr>
		<td class="label"><?php echo $_SESSION['stock']; ?>:</td>
		<td class="input"><input type="text" name="stockID" id="stockID" size="30"  /></td>
	</tr>
	
	<!-- DISCOUNTS -->
	<tr><td class="h3"><h3><?php echo $_SESSION['discounts']; ?></h3></td></tr>	
	
	<!-- FIRE EXTINGUISHERS OR ALARM -->
	<tr>
		<td class="label"><?php echo $_SESSION['fireExtinguishersOrAlarm']; ?>:</td>
		<td class="input">
			<select name="fireExtinguishersOrAlarmSel" id="fireExtinguishersOrAlarmSel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- FIRE SYSTEM -->
	<tr>
		<td class="label"><?php echo $_SESSION['fireSystem']; ?>:</td>
		<td class="input">
			<select name="fireSystemSel" id="fireSystemSel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>

	<!-- ONE MORE POLICY -->
	<tr>
		<td class="label"><?php echo $_SESSION['oneMorePolicy']; ?>:</td>
		<td class="input">
			<select name="oneMorePolicySel" id="oneMorePolicySel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- TWO MORE POLICIES -->
	<tr>
		<td class="label"><?php echo $_SESSION['twoMorePolicies']; ?>:</td>
		<td class="input">
			<select name="twoMorePoliciesSel" id="twoMorePoliciesSel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- THREE OR MORE POLICIES -->
	<tr>
		<td class="label"><?php echo $_SESSION['threeOrMorePolicies']; ?>:</td>
		<td class="input">
			<select name="threeOrMorePoliciesSel" id="threeOrMorePoliciesSel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- BUSINESS TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['businessType']; ?>:</td>
		<td class="input">
			<select name="businessTypeSel" id="businessTypeSel" style="width:230px;">
				<?php
				/*
				foreach($_SESSION['businessTypeOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }*/
				 ?>
			</select>
		</td>
	</tr>	
	
	<!-- PUBLIC LIABILITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['publicLiabilitiy']; ?>:</td>
		<td class="input">
			<select name="publicLiabilitiySel" id="publicLiabilitiySel" style="width:230px;">
				<?php
				/*
				foreach($_SESSION[''] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }*/
				 ?>
			</select>
		</td>
	</tr>	
	
	<!-- EMPLOYER LIABILITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['employerLiability']; ?>:</td>
		<td class="input">
			<select name="employerLiabilitySel" id="employerLiabilitySel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- TOTAL PREMIUM WITH DISCOUNTS -->
	<tr>
		<td class="label"><?php echo $_SESSION['totalPremium']; ?>:</td>
		<td class="input"><input type="text" name="totalPremiumID" id="totalPremiumID" size="30" /></td>
	</tr>
	
	<!-- STAMPS -->
	<tr>
		<td class="label"><?php echo $_SESSION['stamps']; ?>:</td>
		<td class="input"><input type="text" name="stampsID" id="stampsID" size="30" /></td>
	</tr>
	
	<!-- FEES -->
	<tr>
		<td class="label"><?php echo $_SESSION['fees']; ?>:</td>
		<td class="input"><input type="text" name="feesID" id="feesID" size="30" /></td>
	</tr>
	
	<!-- FINAL PREMIUM -->
	<tr>
		<td class="label"><?php echo $_SESSION['finalPremium']; ?>:</td>
		<td class="input"><input type="text" name="finalPremiumID" id="finalPremiumID" size="30"  /></td>
	</tr>

	<!-- CONTINUE BUTTON -->						
	<tr>
		<td><br /></td>
		<p><td><input type="submit" name="send" class="button" value="<?php echo $_SESSION['submitButton']; ?>" size="30" /></td></p>
	</tr>

	
</table>
	
</form>	