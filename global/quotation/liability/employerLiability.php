<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "2")
</script>

<h1 id="h1"><?php echo $_SESSION['employerLiabilityForm'];?></h1>

<form name="employerLiabilityFormID" action="./quotation.php" method="post">
	<input type="hidden" name="action" value="employerLiabilityFormProcess">
	
	<table>
	
	<!-- TERRITORIAL LIMITS -->
	<tr>
		<td class="label"><?php echo $_SESSION['territorialLimits']; ?>:</td>
		<td class="input">&nbsp;</td>
	</tr>

	<!-- LIMIT PER EMPLOYEE -->
	<tr>
		<td class="label"><?php echo $_SESSION['limitPerEmployee']; ?>:</td>
		<td class="input">&nbsp;</td>
	</tr>
	
	<!-- LIMIT PER EVENT -->
	<tr>
		<td class="label"><?php echo $_SESSION['limitPerEventOrSeriesOfEvents']; ?>:</td>
		<td class="input">&nbsp;</td>
	</tr>
	
	<!-- LIMIT PER PERIOD -->
	<tr>
		<td class="label"><?php echo $_SESSION['limitDuringPeriodOfInsurance']; ?>:</td>
		<td class="input">&nbsp;</td>
	</tr>
	
	<!-- BUSINESS TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['businessType']; ?>:</td>
		<td class="input">
			<select name="businessTypeVal" id="businessTypeVal" style="width:230px;">
				<?php
				/*
				foreach($_SESSION[''] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }*/
				 ?>
			</select>
		</td>
	</tr>	
	
	<!-- EMPLOYEES NUMBER -->
	<tr>
		<td class="label"><?php echo $_SESSION['employeesNumber']; ?>:</td>
		<td class="input"><input type="text" name="employeesNumberID" id="employeesNumberID" size="30"  /></td>
	</tr>
	
	<!-- TOTAL GROSS EARNINGS -->
	<tr>
		<td class="label"><?php echo $_SESSION['estimatedTotalGrossEarnings']; ?>:</td>
		<td class="input"><input type="text" name="estimatedTotalGrossEarningsID" id="estimatedTotalGrossEarningsID" size="30" /></td>
	</tr>
	
	<!-- BODILY INJURY -->
	<tr>
		<td class="label"><?php echo $_SESSION['bodilyInjury']; ?>:</td>
		<td class="input"><input type="text" name="bodilyInjuryID" id="bodilyInjuryID" size="30"  /></td>
	</tr>
	
	<!-- PROPERTY DAMAGE -->
	<tr>
		<td class="label"><?php echo $_SESSION['propertyDamage']; ?>:</td>
		<td class="input"><input type="text" name="propertyDamageID" id="propertyDamageID" size="30"  /></td>
	</tr>
	
	<!-- TOTAL LIABILITY LIMIT -->
	<tr>
		<td class="label"><?php echo $_SESSION['totalLiabilityLimit']; ?>:</td>
		<td class="input"><input type="text" name="totalLiabilityLimitID" id="totalLiabilityLimitID" size="30"  /></td>
	</tr>
	
	<!-- EMPLOYER LIABILITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['employerLiability']; ?>:</td>
		<td class="input">
			<select name="employerLiabilitySel" id="employerLiabilitySel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- EMPLOYER LIABILITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['employerLiability']; ?>:</td>
		<td class="input">
			<select name="employerLiabilitySel" id="employerLiabilitySel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- EMPLOYER LIABILITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['employerLiability']; ?>:</td>
		<td class="input">
			<select name="employerLiabilitySel" id="employerLiabilitySel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- CONTINUE BUTTON -->						
	<tr>
		<td><br /></td>
		<p><td><input type="submit" name="send" class="button" value="<?php echo $_SESSION['submitButton']; ?>" size="30" /></td></p>
	</tr>
	

	</table>
</form>