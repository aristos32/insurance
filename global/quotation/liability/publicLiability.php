<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "2")
</script>

<h1 id="h1"><?php echo $_SESSION['publicLiabilityTab'];?></h1>

<form name="publicLiabilityFormID" action="./quotation.php" method="post">
	<input type="hidden" name="action" value="publicLiabilityFormProcess">
	
	<table>
	
	<!-- BODILY INJURY -->
	<tr>
		<td class="label"><?php echo $_SESSION['bodilyInjury']; ?>:</td>
		<td class="input"><input type="text" name="bodilyInjuryID" id="bodilyInjuryID" size="30" value="<?php echo $bodilyInjuryVal; ?>" /></td>
	</tr>

	<!-- PROPERTY DAMAGE -->
	<tr>
		<td class="label"><?php echo $_SESSION['propertyDamage']; ?>:</td>
		<td class="input"><input type="text" name="propertyDamageID" id="propertyDamageID" size="30" value="<?php echo $propertyDamageVal; ?>" /></td>
	</tr>
	
	<!-- TOTAL LIABILITY LIMIT -->
	<tr>
		<td class="label"><?php echo $_SESSION['totalLiabilityLimit']; ?>:</td>
		<td class="input"><input type="text" name="totalLiabilityLimitID" id="totalLiabilityLimitID" size="30" value="<?php echo $totalLiabilityLimitVal; ?>" /></td>
	</tr>
	
	<!-- FOOD AND DRINK POISONING -->
	<tr>
		<td class="label"><?php echo $_SESSION['foodAndDrinkPoisoning']; ?>:</td>
		<td class="input">
			<select name="foodAndDrinkPoisoningSel" id="foodAndDrinkPoisoningSel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- LIFTS AND ESCALATORS -->
	<tr>
		<td class="label"><?php echo $_SESSION['liftsAndEscalators']; ?>:</td>
		<td class="input">
			<select name="liftAndEscalatorsSel" id="liftAndEscalatorsSel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- FIRE AND EXPLOSION -->
	<tr>
		<td class="label"><?php echo $_SESSION['fireAndExplosion']; ?>:</td>
		<td class="input">
			<select name="fireAndExplosionSel" id="fireAndExplosionSel">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- CONTINUE BUTTON -->						
	<tr>
		<td><br /></td>
		<p><td><input type="submit" name="send" class="button" value="<?php echo $_SESSION['submitButton']; ?>" size="30" /></td></p>
	</tr>

	
	</table>
</form>