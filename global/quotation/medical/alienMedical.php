<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "3")
</script>

<h1 id="h1"><?php echo $_SESSION['alienMedicalTab'];?></h1>

<form name="alienMedicalFormID" action="./quotation.php" method="post">
	<input type="hidden" name="action" value="alienMedicalFormProcess">
	
	<table>
	
	<!-- PROPOSEROREMPLOYER SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['proposerOrEmployer']; ?></h3></td></tr>

	<!-- PROPOSERNAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['proposerName']; ?>:</td>
		<td class="input"><input type="text" name="proposerNameID" id="proposerNameID" size="30" /></td>
	</tr>

	<!-- PROPOSERSURNAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['proposerSurname']; ?>:</td>
		<td class="input"><input type="text" name="proposerSurnameID" id="proposerSurnameID" size="30"  /></td>
	</tr>

	<!-- BIRTH DATE -->
	<tr>
		<td class="label"><?php echo $_SESSION['birthDate']; ?>:</td>
		<td class="input"><input type="text" name="birthDateID" id="birthDateID" size="30"  /></td>
	</tr>

	<!-- EMAIL -->
	<tr>
		<td class="label"><?php echo $_SESSION['email']; ?>:</td>
		<td class="input"><input type="text" name="emailID" id="emailID" size="30"  /></td>
	</tr>

	<!-- PROFESSION -->
	<tr>
		<td class="label"><?php echo $_SESSION['profession']; ?>:</td>
		<td class="input"><input type="text" name="professionID" id="professionID" size="30"  /></td>
	</tr>

	<!-- COMPANY -->
	<tr>
		<td class="label"><?php echo $_SESSION['company']; ?>:</td>
		<td class="input"><input type="text" name="companyID" id="companyID" size="30"  /></td>
	</tr>

	<!-- IDENTITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['identity']; ?>:</td>
		<td class="input"><input type="text" name="identityID" id="identityID" size="30" /></td>
	</tr>

	<!-- TELEPHONE -->
	<tr>
		<td class="label"><?php echo $_SESSION['telephoneTab']; ?>:</td>
		<td class="input"><input type="text" name="telephoneTabID" id="telephoneTabID" size="30"  /></td>
	</tr>

	<!-- CELLPHONE -->
	<tr>
		<td class="label"><?php echo $_SESSION['cellphoneTab']; ?>:</td>
		<td class="input"><input type="text" name="cellphoneTabID" id="cellphoneTabID" size="30"  /></td>
	</tr>

	<!-- FAX -->
	<tr>
		<td class="label"><?php echo $_SESSION['fax']; ?>:</td>
		<td class="input"><input type="text" name="faxID" id="faxID" size="30"  /></td>
	</tr>

	<!-- ADDRESS -->
	<tr>
		<td class="label"><?php echo $_SESSION['address']; ?>:</td>
		<td class="input"><input type="text" name="addressID" id="addressID" size="30"  /></td>
	</tr>

	<!-- AREA CODE -->
	<tr>
		<td class="label"><?php echo $_SESSION['areaCode']; ?>:</td>
		<td class="input"><input type="text" name="areaCodeID" id="areaCodeID" size="30"  /></td>
	</tr>

	<!-- CITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['city']; ?>:</td>
		<td class="input"><input type="text" name="cityID" id="cityID" size="30"  /></td>
	</tr>

	<!-- COUNTRY -->
	<tr>
		<td class="label"><?php echo $_SESSION['country']; ?>:</td>
		<td class="input"><input type="text" name="countryID" id="countryID" size="30"  /></td>
	</tr>
	
	<!-- INSURED SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['insured']; ?></h3></td></tr>

	<!-- INSUREDFIRSTNAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredFirstName']; ?>:</td>
		<td class="input"><input type="text" name="insuredFirstNameID" id="insuredFirstNameID" size="30"  /></td>
	</tr>

	<!-- INSUREDSURNAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredSurname']; ?>:</td>
		<td class="input"><input type="text" name="insuredSurnameID" id="insuredSurnameID" size="30"  /></td>
	</tr>

	<!-- INSUREDBIRTHDATE -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredBirthDate']; ?>:</td>
		<td class="input"><input type="text" name="insuredBirthDateID" id="insuredBirthDateID" size="30"  /></td>
	</tr>

	<!-- INSUREDADDRESS -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredAddress']; ?>:</td>
		<td class="input"><input type="text" name="insuredAddressID" id="insuredAddressID" size="30"  /></td>
	</tr>

	<!-- INSUREDAREACODE -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredAreaCode']; ?>:</td>
		<td class="input"><input type="text" name="insuredAreaCodeID" id="insuredAreaCodeID" size="30"  /></td>
	</tr>

	<!-- INSUREDCITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredCity']; ?>:</td>
		<td class="input"><input type="text" name="insuredCityID" id="insuredCityID" size="30"  /></td>
	</tr>

	<!-- INSUREDCOUNTRY -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredCountry']; ?>:</td>
		<td class="input"><input type="text" name="insuredCountryID" id="insuredCountryID" size="30"  /></td>
	</tr>

	<!-- GENDER -->
	<tr>
		<td class="label"><?php echo $_SESSION['gender'] ?>:</td>
		<td class="input"><select name="gender" id="gender">
				<?php
				foreach($_SESSION['genderOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
	</tr>

	<!-- INSUREDPROFESSION -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredProfession']; ?>:</td>
		<td class="input"><input type="text" name="insuredProfessionID" id="insuredProfessionID" size="30"  /></td>
	</tr>

	<!-- NATIONALITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['nationality']; ?>:</td>
		<td class="input"><input type="text" name="nationalityID" id="nationalityID" size="30"  /></td>
	</tr>

	<!-- CONTINUE BUTTON -->						
	<tr>
		<td><br /></td>
		<p><td><input type="submit" name="send" class="button" value="<?php echo $_SESSION['submitButton']; ?>" size="30" /></td></p>
	</tr>
	

	</table>
</form>