<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "3")
</script>

<h1 id="h1"><?php echo $_SESSION['medicalTab'];?></h1>

<form name="medicalFormID" action="./quotation.php" method="post">
	<input type="hidden" name="action" value="medicalFormProcess">
	
	<table>
	
	<!-- PROPOSER SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['proposer']; ?></h3></td></tr>

	<!-- NAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['proposerName']; ?>:</td>
		<td class="input"><input type="text" name="proposerNameID" id="proposerNameID" size="30"  /></td>
	</tr>

	<!-- SURNAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['proposerSurname']; ?>:</td>
		<td class="input"><input type="text" name="proposerSurnameID" id="proposerSurnameID" size="30"  /></td>
	</tr>

	<!-- BIRTH DATE -->
	<tr>
		<td class="label"><?php echo $_SESSION['birthDate']; ?>:</td>
		<td class="input"><input type="text" name="birthDateID" id="birthDateID" size="30"  /></td>
	</tr>

	<!-- EMAIL -->
	<tr>
		<td class="label"><?php echo $_SESSION['email']; ?>:</td>
		<td class="input"><input type="text" name="emailID" id="emailID" size="30"  /></td>
	</tr>

	<!-- PROFESSION -->
	<tr>
		<td class="label"><?php echo $_SESSION['profession']; ?>:</td>
		<td class="input"><input type="text" name="professionID" id="professionID" size="30"  /></td>
	</tr>

	<!-- COMPANY -->
	<tr>
		<td class="label"><?php echo $_SESSION['company']; ?>:</td>
		<td class="input"><input type="text" name="companyID" id="companyID" size="30"  /></td>
	</tr>

	<!-- IDENTITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['identity']; ?>:</td>
		<td class="input"><input type="text" name="identityID" id="identityID" size="30"  /></td>
	</tr>

	<!-- TELEPHONE -->
	<tr>
		<td class="label"><?php echo $_SESSION['telephoneTab']; ?>:</td>
		<td class="input"><input type="text" name="telephoneTabID" id="telephoneTabID" size="30"  /></td>
	</tr>

	<!-- CELLPHONE -->
	<tr>
		<td class="label"><?php echo $_SESSION['cellphoneTab']; ?>:</td>
		<td class="input"><input type="text" name="cellphoneTabID" id="cellphoneTabID" size="30"  /></td>
	</tr>

	<!-- FAX -->
	<tr>
		<td class="label"><?php echo $_SESSION['fax']; ?>:</td>
		<td class="input"><input type="text" name="faxID" id="faxID" size="30"  /></td>
	</tr>

	<!-- ADDRESS -->
	<tr>
		<td class="label"><?php echo $_SESSION['address']; ?>:</td>
		<td class="input"><input type="text" name="addressID" id="addressID" size="30"  /></td>
	</tr>

	<!-- AREA CODE -->
	<tr>
		<td class="label"><?php echo $_SESSION['areaCode']; ?>:</td>
		<td class="input"><input type="text" name="areaCodeID" id="areaCodeID" size="30"  /></td>
	</tr>

	<!-- CITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['city']; ?>:</td>
		<td class="input"><input type="text" name="cityID" id="cityID" size="30"  /></td>
	</tr>

	<!-- COUNTRY -->
	<tr>
		<td class="label"><?php echo $_SESSION['country']; ?>:</td>
		<td class="input"><input type="text" name="countryID" id="countryID" size="30"  /></td>
	</tr>

	<!-- DEPENDENTS SECTION-->
	<tr><td class="h3"><h3><?php echo $_SESSION['dependents']; ?></h3></td></tr>

	<!-- NAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['dependentFirstName']; ?>:</td>
		<td class="input"><input type="text" name="dependentFirstNameID" id="dependentFirstNameID" size="30" /></td>
	</tr>

	<!-- SURNAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['dependentSurname']; ?>:</td>
		<td class="input"><input type="text" name="dependentSurnameID" id="dependentSurnameID" size="30"  /></td>
	</tr>

	<!-- BIRTH DATE -->
	<tr>
		<td class="label"><?php echo $_SESSION['dependentBirthDate']; ?>:</td>
		<td class="input"><input type="text" name="dependentBirthDateID" id="dependentBirthDateID" size="30"  /></td>
	</tr>

	<!-- RELATION -->
	<tr>
		<td class="label"><?php echo $_SESSION['dependentRelation']; ?>:</td>
		<td class="input"><input type="text" name="dependentRelationID" id="dependentRelationID" size="30"  /></td>
	</tr>

	<!-- FREQUENCY OF PAYMENT -->
	<tr>
		<td class="label"><?php echo $_SESSION['frequencyOfPayment']; ?>:</td>
		<td class="input"><input type="text" name="frequencyOfPaymentID" id="frequencyOfPaymentID" size="30"  /></td>
	</tr>

	<!-- PLAN NAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['planName']; ?>:</td>
		<td class="input"><input type="text" name="planNameID" id="planNameID" size="30"  /></td>
	</tr>

	<!-- MAXIMUM COMPENSATION -->
	<tr>
		<td class="label"><?php echo $_SESSION['maximumCompensation']; ?>:</td>
		<td class="input"><input type="text" name="maximumCompensationID" id="maximumCompensationID" size="30"  /></td>
	</tr>

	<!-- ROOM TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['roomType']; ?>:</td>
		<td class="input"><input type="text" name="roomTypeID" id="roomTypeID" size="30"  /></td>
	</tr>

	<!-- EXCESS -->
	<tr>
		<td class="label"><?php echo $_SESSION['excess']; ?>:</td>
		<td class="input"><input type="text" name="excessID" id="excessID" size="30"  /></td>
	</tr>

	<!-- CO INSURANCE -->
	<tr>
		<td class="label"><?php echo $_SESSION['coInsurancePercentage']; ?>:</td>
		<td class="input"><input type="text" name="coInsurancePercentageID" id="coInsurancePercentageID" size="30"  /></td>
	</tr>

	<!-- OUTPATIENT PLAN -->
	<tr>
		<td class="label"><?php echo $_SESSION['outpatientPlanName']; ?>:</td>
		<td class="input"><input type="text" name="outpatientPlanNameID" id="outpatientPlanNameID" size="30"  /></td>
	</tr>

	<!-- OUTPATIENT MAXIMUM COMPENSATION -->
	<tr>
		<td class="label"><?php echo $_SESSION['outpatientMaximumCompensation']; ?>:</td>
		<td class="input"><input type="text" name="outpatientMaximumCompensationID" id="outpatientMaximumCompensationID" size="30" /></td>
	</tr>

	<!-- ADDITIONAL COVERAGES SECTION-->
	<tr><td class="h3"><h3><?php echo $_SESSION['additionalCoverages']; ?></h3></td></tr>

	<!-- CONTINUE BUTTON -->						
	<tr>
		<td><br /></td>
		<p><td><input type="submit" name="send" class="button" value="<?php echo $_SESSION['submitButton']; ?>" size="30" /></td></p>
	</tr>

	</table>
</form>