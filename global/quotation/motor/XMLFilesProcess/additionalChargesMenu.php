<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');


//on language change, POST are not send. So, use ones in session
if(isset($_POST['vehicleType']))
	$_SESSION['vehicleType'] = $_POST['vehicleType'];
	
//echo "INSIDE for vehicletype= ".$_SESSION['vehicleType'];

//find existing coverages in directory
$additionalChargesFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $_SESSION['xmlFileName']."_additionalCharges");
?>
<table>
<?php
if(count($additionalChargesFiles)>0)
{
	?><tr><td> Existing Additional Charges Files: </td>
	<?php
	foreach($additionalChargesFiles as $eachChargesFile)
	{
		?>
		<tr>
			<td/>
			<td>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="./quotation.php" id="<?php echo $eachChargesFile;?>" method="post" style="display: none;">
				<input type="hidden" name="action" value="displayAdditionalChargesFile" />
				<input type="hidden" name="additionalChargesFileName" value="<?php echo $eachChargesFile;?>" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $eachChargesFile;?>').submit()"><?php echo $eachChargesFile; ?></a>
			</td>
		</tr>
		<?php
	}	
}
else
{
	?>
	<tr><td><?php echo $_SESSION['noDataFound']; ?>.
	
 	 <!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="createAdditionalChargesFile" method="post" style="display: none;">
		<input type="hidden" name="action" value="createAdditionalChargesFile" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('createAdditionalChargesFile').submit()"><?php echo $_SESSION['create']; ?></a>
	</td>
	</tr>
	<?php
}
?>
</table>
