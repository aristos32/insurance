<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');


//on language change, POST are not send. So, use ones in session
if(isset($_POST['vehicleType']))
	$_SESSION['vehicleType'] = $_POST['vehicleType'];
	
//echo "INSIDE for vehicletype= ".$_SESSION['vehicleType'].",xmlfilename=".$_SESSION['xmlFileName'];

//find existing coverages in directory
$coveragesFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $_SESSION['xmlFileName']."_coverageType_");
?>
<table>
<?php
if(count($coveragesFiles)>0)
{
	?><tr><td class="col10Per" colspan="2"><?php echo $_SESSION['existingFiles']; ?>:</td>
	<?php
	foreach($coveragesFiles as $eachCoverageFile)
	{
		?>
		<tr>
			<td class="col10Per" />
			<td class="col10Per">
			<!-- create hidden forn, to send the action as a POST -->
			<form action="./quotation.php" id="<?php echo $eachCoverageFile;?>" method="post" style="display: none;">
				<input type="hidden" name="action" value="displayCoverageFile" />
				<input type="hidden" name="coverageFileName" value="<?php echo $eachCoverageFile;?>" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $eachCoverageFile;?>').submit()"><?php echo $eachCoverageFile; ?></a>
			</td>
			<td class="col10Per">
				<?php
				//only administrators can delete xml files
				if($_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
				{
					$deleteId = "delete_".$eachCoverageFile;
					?>
					<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
					<form action="./quotation.php" id="<?php echo $deleteId;?>" method="post" style="display: none;">
						<input type="text" name="action" value="deleteSingleXML" />
						<input type="hidden" name="fileName" value="<?php echo $eachCoverageFile;?>" />
					</form>
					<a href="javascript:;" onclick="javascript: if(confirm('<?php echo $_SESSION['areYouSureYouWantToDeleteThisFile'];?>')) document.getElementById('<?php echo $deleteId;?>').submit()"><?php echo $_SESSION['delete']; ?></a>
					<?php
				}
				?>
			</td>
		</tr>
		<?php
	}	
}
else
{
	?><tr><td><?php	echo $_SESSION['noDataFound']; ?> </td>
		</tr>
		
<?php
}
?>
<tr><td><br/></td></tr>
<tr>
  <td class="col10Per">
  <!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="selectCoverageType" method="post" style="display: none;">
		<input type="hidden" name="action" value="selectCoverageType" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('selectCoverageType').submit()"><?php echo $_SESSION['create']; ?></a>
	</td>
	<td>
	<!-- display all client contracts - 
	create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="returnToPreviousScreen" method="post" style="display: none;">
		<input type="text" name="action" value="loadXMLFile" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('returnToPreviousScreen').submit()"><?php echo $_SESSION['return']; ?></a>
	</td>
</tr>
</table>
