<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//on language change, don't validate and save again, because file will be erases
	if(	$actionRead != "" )
	{
		$newCoverageType = $_POST['coverageType'];
			
		$modelCoverageFileName = "model_coverageType_".$newCoverageType.".xml"; //we read from global
		
		$_SESSION['coverageFileName'] = $_SESSION['xmlFileName']."_coverageType_".$newCoverageType.".xml"; //new file name
		
		$coverageTypeClass = readCoverageXML($_SESSION['globalFilesLocation']."/quotation/motor/XMLFiles/".$_SESSION['vehicleType']."/", $modelCoverageFileName);
		
		displayCoverageTypeFile($coverageTypeClass);
	}
	
?>