<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//on language change, don't validate and save again, because file will be erases
	if(	$actionRead != "" )
	{
		$modelCoverageFileName = "model_optionalCoverages.xml"; //we read from global
		
		$_SESSION['optionalCoveragesFileName'] = $_SESSION['xmlFileName']."_optionalCoverages.xml"; //new file name
		
		$optionalCoveragesArray = readOptionalCoveragesXML($_SESSION['globalFilesLocation']."/quotation/motor/XMLFiles/".$_SESSION['vehicleType']."/", $modelCoverageFileName);
	
		displayOptionalCoveragesFile($optionalCoveragesArray);
	}
	
?>