<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
	
//delete the base XML file and all related files

//delete an existing company XML file
$_SESSION['currentInputXMLFileName'] = $_POST['fileName'];
unlink($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['currentInputXMLFileName']);	
//get name without .xml. From model.xml get only model
$tempArrayName = explode(".", $_SESSION['currentInputXMLFileName']);
$fileNameWithoutExtention = $tempArrayName[0];

//delete all related files
//read all vehicle types
$directories = getListOfDirectories($_SESSION['clientFilesLocation']."/XMLFiles/");
foreach($directories as $eachDirectory)
{
	//read all coverage files available. TP, TPFT, Comprehensive
	$allRelatedFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $fileNameWithoutExtention."_");
	/* read each coveragetype */
	foreach($allRelatedFiles as $eachRelatedFile)
	{
		unlink($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/".$eachRelatedFile);
	}
}

echo $_SESSION['deleteSuccessfull'].".";
?>
<!-- create hidden forn, to send the action as a POST -->
<form action="./quotation.php" id="deleteCompanyXMLForm" method="POST" style="display: none;">
<input type="text" name="action" value="displayAllXmlFilesInDir" />
</form>
<a href="javascript:;" onclick="javascript: document.getElementById('deleteCompanyXMLForm').submit()"><?php echo $_SESSION['continue']; ?></a>
