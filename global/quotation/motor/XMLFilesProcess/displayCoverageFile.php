<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//on language change, POST are not send. So, use ones in session
if(isset($_POST['coverageFileName']))
	$_SESSION['coverageFileName'] = $_POST['coverageFileName'];
	
$coverageTypeClass = readCoverageXML($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $_SESSION['coverageFileName']);

displayCoverageTypeFile($coverageTypeClass);

?>
<!-- display all client contracts - 
create hidden forn, to send the action as a POST -->
<form action="./quotation.php" id="returnToPreviousScreen" method="post" style="display: none;">
	<input type="text" name="action" value="loadXMLFile" />
</form>
<a href="javascript:;" onclick="javascript: document.getElementById('returnToPreviousScreen').submit()"><?php echo $_SESSION['return']; ?></a>
