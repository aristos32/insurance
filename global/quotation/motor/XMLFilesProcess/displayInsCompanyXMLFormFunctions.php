<?php



/* display the insurance company XML form, and fill values if any. f.e MODEL.xml or Trust.xml
When saved, updated file is created */
function displayInsCompanyXMLFormNew($basicCoverageInfo)
{
	global $vehicleTypeOptions, $save, $file, $insuranceCompany, $insuranceCompanyOptions;

	writeToFile(date("Y-m-d H:i:s"). ": displayInsCompanyXMLFormNew() ENTER \n", LOG_LEVEL_INFO);
        
	?>
	
	<form name="insCompanyXMLForm" action="./quotation.php" method="post" enctype="application/x-www-form-urlencoded" onSubmit="return checkInsCompanyXMLForm()">
		
			<input type="hidden" name="action" value="insCompanyXMLFormProcess">			
			<table>
				
				<!--<div id="myDiv"><h2>Let AJAX change this text</h2></div>
				<button type="button" onclick="myFunction()">Change Content</button> -->
				
				<?php
				/* get the file name without extension. i.e MODEL.xml -> MODEL */
				$companyName = $basicCoverageInfo->companyName;
				$_SESSION['xmlFileName'] = substr($_SESSION['currentInputXMLFileName'], 0, strpos($_SESSION['currentInputXMLFileName'], "."));
				?>
				
				<tr>
					<td class="col10Per"><?php echo $_SESSION['file']; ?>:</td>
					<td class="col10Per"><input type="text" name="fileName" id="fileName"  value="<?php echo $_SESSION['xmlFileName'];?>" /></td>
					<td class="col10Per">&nbsp</td>
					<td class="col10Per">&nbsp</td>
					<td class="col10Per">&nbsp</td>
					<td class="col10Per">&nbsp</td>
					<td class="col10Per"/>
				</tr>
				
				<tr>
				<td class="col10Per"><?php echo $_SESSION['insuranceCompanyTab'];?>:</td>
				<td class="col10Per"><select name="companyName" id="companyName" style="width:230px;">
				<?php
				foreach($_SESSION['insuranceCompanyTabOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name"?></option>
					 <?php
				 }
				 ?>
				 </select>	
				</td>
				
				</tr>
				
				<?php echo "<script language=javascript>setSelectTag(\"$basicCoverageInfo->companyName\", \"companyName\")</script>"; ?>
			</table>
			
			<?php
			/* DISPLAY STANDARD CHARGES */
			displayStandardCharges($basicCoverageInfo);
			?>
			
			
		<tr>
		<td><input type="submit" value=<?php echo $_SESSION['save']; ?> /></td>
		</tr>
	  </table>
	</form>
	
<?php

	//read all vehicle types
	$directories = getListOfDirectories($_SESSION['clientFilesLocation']."/XMLFiles/");
	
	?>
	<div id="dataPresentationInTables">
	<table>
		<tr>
			<th><?php echo $_SESSION['vehicleTypeTab']; ?></th>
			<th><?php echo $_SESSION['coverageTabCaps']; ?></th>
			<th><?php echo $_SESSION['packageTab']; ?></th>
			<th><?php echo $_SESSION['optionalCoverages']; ?></th>
			<th><?php echo $_SESSION['additionalChargesTab']; ?></th>
		</tr>
		<?php
		//all VEHICLE TYPES are directories
		foreach($directories as $eachDirectory)
		{
			$coveragesName = "coverages_".$eachDirectory;
			$packagesName = "packages_".$eachDirectory;
			$optionalCoveragesName = "optionalCoverages_".$eachDirectory;
			$additionalChargesName = "additionalCharges_".$eachDirectory;
			?>
			<tr>
				<td><?php echo $eachDirectory; ?></td>
				<td>
					<form action="./quotation.php" id="<?php echo $coveragesName; ?>" method="post" style="display: none;">
						<input type="text" name="action" value="coverageTypesMenu" />
						<input type="text" name="vehicleType" value="<?php echo $eachDirectory;?>" />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $coveragesName;?>').submit()"><?php echo $_SESSION['updateButton']; ?></a>
				</td>
				<td>
					<form action="./quotation.php" id="<?php echo $packagesName; ?>" method="post" style="display: none;">
						<input type="text" name="action" value="packagesMenu" />
						<input type="text" name="vehicleType" value="<?php echo $eachDirectory;?>" />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $packagesName;?>').submit()"><?php echo $_SESSION['updateButton']; ?></a>
				</td>
				<td>
					<form action="./quotation.php" id="<?php echo $optionalCoveragesName; ?>" method="post" style="display: none;">
						<input type="text" name="action" value="optionalCoveragesMenu" />
						<input type="text" name="vehicleType" value="<?php echo $eachDirectory;?>" />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $optionalCoveragesName;?>').submit()"><?php echo $_SESSION['updateButton']; ?></a>
				</td>
				<td>
					<form action="./quotation.php" id="<?php echo $additionalChargesName; ?>" method="post" style="display: none;">
						<input type="text" name="action" value="additionalChargesMenu" />
						<input type="text" name="vehicleType" value="<?php echo $eachDirectory;?>" />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $additionalChargesName;?>').submit()"><?php echo $_SESSION['updateButton']; ?></a>
				</td>
			</tr>
			<?php
		}
		?>
	</table>
	</div>
	<?php

        writeToFile(date("Y-m-d H:i:s"). ": displayInsCompanyXMLFormNew() EXIT \n", LOG_LEVEL_INFO);
}



/* display all html elements inside the coverageType xml file
$coverageTypeClass : class containing the <coverageType> read from xml */
function displayPackageFile($packageFileClass)
{
	global $EURO, $TEXT_BOX_SIZE;

	writeToFile(date("Y-m-d H:i:s"). ": displayPackageFile() ENTER \n", LOG_LEVEL_INFO);
        
	$tableId = "OptionalCoveragesTableId";
	
	
	//${"ExistingCodes_".$tableId} = array();
	$existingCodesListName = "ExistingCodes_".$tableId;
	${$existingCodesListName} = "";
	
	//set all the table id before defining the form, so we can send it to the javascript function
	$allTableIdsofCoverages = "";
	foreach($packageFileClass->optionalCoveragesFromXMLArray as $eachCoverage)
	{
		if($allTableIdsofCoverages!="")
			$allTableIdsofCoverages = $allTableIdsofCoverages.","."OptionalCoverages"."_".$eachCoverage->code;
		else
			$allTableIdsofCoverages = "OptionalCoverages"."_".$eachCoverage->code;
	}
	
	?>
		
   	<form name="packageForm" action="./quotation.php" method="post" enctype="application/x-www-form-urlencoded" onSubmit="return checkPackageForm('<?php echo $allTableIdsofCoverages;?>')" >
		<input type="hidden" name="action" value="packageFormProcess" />
		 
		<table>
		
		<!-- PACKAGE NAME -->
		<tr>
			<td class="col10Per"><?php echo $_SESSION['packageName']; ?>:</td> 
			<td class="col10Per"><input type="text" name="inclusiveBenefitName" id="inclusiveBenefitName" value="<?php echo $packageFileClass->name; ?>" size="<?php echo $TEXT_BOX_SIZE;?>"/></td>
			<td class="col10Per"/>
		<td class="col10Per"/>
		<td class="col10Per"/>
		<td class="col10Per"/>
		<td class="col10Per"/>
		<td class="col10Per"/>
		</tr>
	
	<!-- AVAILABLE  -->
	<tr>
		<td class="col10Per"><?php echo $_SESSION['available']; ?>:</td> 
		<td class="col10Per">
			<select name="inclusiveBenefitAvailableName" id="inclusiveBenefitAvailableName">
				<?php
				foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>	
			</select>
		</td>
	</tr>
	<!-- set correct select option -->
	<?php echo "<script language=javascript>setSelectTag(\"$packageFileClass->available\", 'inclusiveBenefitAvailableName')</script>"; ?>
	
	<!-- COVERAGE TYPE -->
	<tr>
			<td class="col10Per"><?php echo $_SESSION['coverageTypeTab']; ?>:</td>
			<td class="col10Per" colspan="2">
			<select name="inclusiveBenefitCoverageTypeOfOfferName" id="inclusiveBenefitCoverageTypeOfOfferName">
			<?php
			foreach($_SESSION['coverageTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>	
		</select>
		</td>
	</tr>
	<!-- set correct select option -->
	<?php echo "<script language=javascript>setSelectTag(\"$packageFileClass->coverageTypeOfOffer\", 'inclusiveBenefitCoverageTypeOfOfferName')</script>"; ?>
	
	<!-- CHARGE OVER BASIC -->
	<tr>
				<td class="col10Per"><?php echo $_SESSION['chargeOverBasic']; ?>(<?php echo $EURO;?>):</td>
			<td class="col10Per"><input type="text" name="chargeOverBasicName" id="chargeOverBasicName" value="<?php echo $packageFileClass->chargeOverBasic; ?>" size="<?php echo $TEXT_BOX_SIZE;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('chargeOverBasicName')"/> </td>
	</tr>
	</table>
	
	<?php
		//displayOptionalCoveragesFile($packageFileClass->optionalCoveragesFromXMLArray);
		$tagNamePrefix = "OptionalCoverages";//.$postFix;
		//use function because this is called also from packages
		${$existingCodesListName} = displayOptionalCoveragesTable($packageFileClass->optionalCoveragesFromXMLArray, $tableId, $existingCodesListName, $tagNamePrefix );
		?>
		<INPUT type="button" value="<?php echo $_SESSION['add'];?>" onclick="addRowInOptionalCoveragesTable('<?php echo $tableId?>', '<?php echo  ${$existingCodesListName}; ?>', '<?php echo $tagNamePrefix?>')" />
		<INPUT type="button" value="<?php echo $_SESSION['remove'];?>" onclick="deleteRowFromTable('<?php echo $tableId?>')" />
	<br/><br/>
	<input type="submit" value=<?php echo $_SESSION['save']; ?> />
  </form>
  <?php

  writeToFile(date("Y-m-d H:i:s"). ": displayPackageFile() EXIT \n", LOG_LEVEL_INFO);
}


/* display all html elements inside the additionalCharges xml file
$additionalChargesArray : array containg the <coverages> read from xml */
function displayAdditionalChargesFile($additionalChargesArray)
{
	global $EURO, $TEXT_BOX_SIZE;

	writeToFile(date("Y-m-d H:i:s"). ": displayAdditionalChargesFile() ENTER \n", LOG_LEVEL_INFO);
        
	$tableId = "AdditionalChargesTableId";//i.e OptionalCoveragesTableId_OP_1_0 or OptionalCoveragesTableId__1_0
	//$addedCoveragesInTable = "AddedCoverages_".$tableId;
	
	//set all the table id before defining the form, so we can send it to the javascript function
	$allTableIdsofCoverages = "";
	foreach($additionalChargesArray as $eachCoverage)
	{
		if($allTableIdsofCoverages!="")
			$allTableIdsofCoverages = $allTableIdsofCoverages.","."AdditionalCharges_".$eachCoverage->code;
		else
			$allTableIdsofCoverages = "AdditionalCharges_".$eachCoverage->code;
	}
	
	
	//${"ExistingCodes_".$tableId} = array();
	$existingCodesListName = "ExistingCodes_".$tableId;
	${$existingCodesListName} = "";
	//$test = "antonis,george,maria";
	//echo "tableID = $tableId <br/>";
	?>
	<form name="additionalChargesForm" action="./quotation.php" method="post" enctype="application/x-www-form-urlencoded" onSubmit="return checkOptionalCoverages('<?php echo $allTableIdsofCoverages;?>')" >
	<!--<form name="additionalChargesForm" action="./quotation.php" method="post" enctype="application/x-www-form-urlencoded" >-->
		<input type="hidden" name="action" value="additionalChargesFormProcess" />
	
		<?php
		$tagNamePrefix = "AdditionalCharges";//.$postFix;
		//use function because this is called also from packages
		$existingCodesListName = "ExistingCodes_".$tableId;
		${$existingCodesListName} = "";
		$localIndex = 0;
		?>
		<table id="<?php echo $tableId; ?>" >
				
			<?php
	
			$k = 0;	
			foreach($additionalChargesArray as $eachCharge)
			{
				//chargesDefinitions.xml
				foreach($_SESSION['chargesDefinitions'] as $eachChargeDefinition)
				{
					$multilingualValue = "";
					$param1 = "";
					$param2 = "";
					$param3 = "";
					$param4 = "";//to avoid error in the final loop iteration
				
					if($eachCharge->code==$eachChargeDefinition->code)
					{
						
						//Find the multilingual display that corresponds to this code
						foreach($_SESSION['chargesInPolicyQuotationOptions'] as $eachChargeMultilingual)
				 		{
					 		//match codes with multilingual codes
						 	if($eachCharge->code==$eachChargeMultilingual->value)
						 	{
							 	$localCode = $eachChargeMultilingual->name;
							 	
							 	//all other repetitions
							 	if($localIndex>0)
							 		${$existingCodesListName} = ${$existingCodesListName}.",".$eachCharge->code;
							 	//first repetition
							 	else
							 		${$existingCodesListName} = $eachCharge->code;
							 		
							 	$localIndex = $localIndex + 1;
							 	
							 	$tagNamePrefix = "AdditionalCharges";
							 	//$eachCoverage->printData();
								displayElementsNew($eachCharge, $localCode, $tagNamePrefix, $k);
								$k = $k + 1;
								
							}
						}
					}
				}
				
			}
			?>
			</table>
			
			<INPUT type="button" value="<?php echo $_SESSION['add'];?>" onclick="addRowInAdditionalChargesTable('<?php echo $tableId?>', '<?php echo  ${$existingCodesListName}; ?>', '<?php echo $tagNamePrefix?>')" />
			<INPUT type="button" value="<?php echo $_SESSION['remove'];?>" onclick="deleteRowFromTable('<?php echo $tableId?>')" />
			
			<br/> <br/>
			
		<input type="submit" value=<?php echo $_SESSION['save']; ?> />
	</form>
		
	<?php

	writeToFile(date("Y-m-d H:i:s"). ": displayAdditionalChargesFile() ENTER \n", LOG_LEVEL_INFO);	
}

		

/* display all html elements inside the optionalCoverages xml file
$optionalCoveragesFromXMLArray : array containg the <coverages> read from xml */
function displayOptionalCoveragesFile($optionalCoveragesFromXMLArray)
{
	global $EURO, $TEXT_BOX_SIZE;

	writeToFile(date("Y-m-d H:i:s"). ": displayOptionalCoveragesFile() ENTER \n", LOG_LEVEL_INFO);
        
	$tableId = "OptionalCoveragesTableId";//i.e OptionalCoveragesTableId_OP_1_0 or OptionalCoveragesTableId__1_0
	//$addedCoveragesInTable = "AddedCoverages_".$tableId;
	
	//set all the table id before defining the form, so we can send it to the javascript function
	$allTableIdsofCoverages = "";
	foreach($optionalCoveragesFromXMLArray as $eachCoverage)
	{
		if($allTableIdsofCoverages!="")
			$allTableIdsofCoverages = $allTableIdsofCoverages.","."OptionalCoverages"."_".$eachCoverage->code;
		else
			$allTableIdsofCoverages = "OptionalCoverages"."_".$eachCoverage->code;
	}
	
	
	//${"ExistingCodes_".$tableId} = array();
	$existingCodesListName = "ExistingCodes_".$tableId;
	${$existingCodesListName} = "";
	//$test = "antonis,george,maria";
	//echo "tableID = $tableId <br/>";
	?>
	<form name="optionalCoveragesForm" action="./quotation.php" method="post" enctype="application/x-www-form-urlencoded" onSubmit="return checkOptionalCoverages('<?php echo $allTableIdsofCoverages;?>')" >
		<input type="hidden" name="action" value="optionalCoveragesFormProcess" />
	
		<?php
		$tagNamePrefix = "OptionalCoverages";//.$postFix;
		//use function because this is called also from packages
		${$existingCodesListName} = displayOptionalCoveragesTable($optionalCoveragesFromXMLArray, $tableId, $existingCodesListName, $tagNamePrefix );
		?>
		<input type="submit" value=<?php echo $_SESSION['save']; ?> />
	</form>
	<INPUT type="button" value="<?php echo $_SESSION['add'];?>" onclick="addRowInOptionalCoveragesTable('<?php echo $tableId?>', '<?php echo  ${$existingCodesListName}; ?>', '<?php echo $tagNamePrefix?>')" />
	<INPUT type="button" value="<?php echo $_SESSION['remove'];?>" onclick="deleteRowFromTable('<?php echo $tableId?>')" />
		
		
	<?php

	writeToFile(date("Y-m-d H:i:s"). ": displayOptionalCoveragesFile() EXIT \n", LOG_LEVEL_INFO);	
}

//use function because this is called also from packages
function displayOptionalCoveragesTable($optionalCoveragesFromXMLArray, $tableId, $existingCodesListName, $tagNamePrefix)
{
	${$existingCodesListName} = "";
	
	$localIndex = 0;
	?>
	<table id="<?php echo $tableId;?>">
		<?php
		$k = 0;
		//optional coverages from model.xml
		foreach($optionalCoveragesFromXMLArray as $eachCoverage)
		{
			//$eachCoverage->printData();
			
			//coveragesDefinitions.xml
			foreach($_SESSION['coveragesDefinitions'] as $eachCoverageDefinition)
		 	{
			 	$multilingualValue = "";
				$param1 = "";
				$param2 = "";
				$param3 = "";
				$param4 = "";//to avoid error in the final loop iteration
				
				if($eachCoverage->code==$eachCoverageDefinition->code)
				{
					//interfaceproperties.xml - <coveragesInPolicyQuotations>
					//Find the multilingual display that corresponds to this code
					foreach($_SESSION['coveragesInPolicyQuotationOptions'] as $eachCoverageMultilingual)
			 		{
					 	//match coverageDefinitions.xml with multilingual codes
					 	if($eachCoverage->code==$eachCoverageMultilingual->value)
					 	{
						 	//all other repetitions
						 	if($localIndex>0)
						 		${$existingCodesListName} = ${$existingCodesListName}.",".$eachCoverage->code;
						 	//first repetition
						 	else
						 		${$existingCodesListName} = $eachCoverage->code;
						 	
						 	$localIndex = $localIndex + 1;
						 	
						 	$param1 = $eachCoverage->param1;
				 			$param2 = $eachCoverage->param2;
				 			$param3 = $eachCoverage->param3;
				 			
			 				if(isset($_SESSION[$eachCoverage->code] ))
			 					$localCode = $_SESSION[$eachCoverage->code];
			 				else
			 					$localCode = $_SESSION[$eachCoverage->code."Tab"];
						 	//$eachCoverage->printData();
							displayElementsNew($eachCoverage, $localCode, $tagNamePrefix, $k);
							$k = $k + 1;
						}
					}
				}
			}
			
		}
		
		//print_r(${"ExistingCodes_".$tableId});
		
		?>
	</table>
	<?php
	
	return ${$existingCodesListName};
}


/* display all html elements inside the coverageType xml file
$coverageTypeClass : class containing the <coverageType> read from xml */
function displayCoverageTypeFile($coverageTypeClass)
{
	global $EURO, $TEXT_BOX_SIZE;

	writeToFile(date("Y-m-d H:i:s"). ": displayCoverageTypeFile() ENTER \n", LOG_LEVEL_INFO);
	//set all the table id before defining the form, so we can send it to the javascript function
	$allTableIdsofCoverages = "";
	
	foreach($coverageTypeClass->coverages as $eachCoverage)
	{
		//if(count($eachCoverage->categories)>0)
		//{
			if($allTableIdsofCoverages!="")
				$allTableIdsofCoverages = $allTableIdsofCoverages.","."CoverageTypeCoverages"."_".$eachCoverage->code;
			else
				$allTableIdsofCoverages = "CoverageTypeCoverages"."_".$eachCoverage->code;
		//}
		
		/*if(count($eachCoverage->typeCategories)>0)
		{
			if($allTableIdsofCoverages!="")
				$allTableIdsofCoverages = $allTableIdsofCoverages.","."CoverageTypeCoverages"."_typeCategories_".$eachCoverage->code;
			else
				$allTableIdsofCoverages = "CoverageTypeCoverages"."_typeCategories_".$eachCoverage->code;
		}*/
	}
  	if($coverageTypeClass->discounts <> null)
  	{
		foreach($coverageTypeClass->discounts->coverages as $eachCoverage)
		{
			if($allTableIdsofCoverages!="")
				$allTableIdsofCoverages = $allTableIdsofCoverages.","."CoverageTypeDiscountsCoverages"."_".$eachCoverage->code;
			else
				$allTableIdsofCoverages = "CoverageTypeDiscountsCoverages"."_".$eachCoverage->code;
		}
	}
	
	?>
	<form name="coverageTypeForm" action="./quotation.php" method="post" enctype="application/x-www-form-urlencoded" onSubmit="return checkCoverageTypeForm('<?php echo $allTableIdsofCoverages;?>')" >
		<input type="hidden" name="action" value="coverageTypeFormProcess" />
		<table>	
		<?php
		//get multilingual value
		foreach($_SESSION['coverageTypeTabOptions'] as $eachCoverageTabMultilingual)
		{
			if($eachCoverageTabMultilingual->value==$coverageTypeClass->value)
			{
			?>
				<tr>
					<td class="col1Per"/>
					<td class="col10Per"><?php echo $_SESSION['coverageTypeTab']; ?>:</td>
					<td class="col10Per"><?php echo $eachCoverageTabMultilingual->name;?></td>
					<td class="col10Per"/>
					<td class="col10Per"/>
					<td class="col10Per"/>
					<td class="col10Per"/>
					<td class="col10Per"/>
				</tr>
				<?php
				break;
			}
		}
		?>
		<tr>
			<td class="col1Per"/>
			<td class="col10Per">
				<?php echo $_SESSION['available']; ?>			
			</td>
			<td class="col10Per">
				<select name="available" id="availableName">
					<?php
					foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
				</select>
			</td>
			
			<?php echo "<script language=javascript>setSelectTag(\"$coverageTypeClass->available\", 'availableName')</script>"; ?>
		</tr>
		
		<!-- MINIMUM CHARGE EURO -->
		<tr>
			<td class="col1Per"/>
			<td class="col10Per"><?php echo $_SESSION['minimumCharge']; ?>(<?php echo $EURO;?>):</td>
			<td class="col10Per"><input type="text" name="minimumChargeEuro" id="minimumChargeEuro" onkeydown="return validateOnRunTimeIsNumeric(event)" id="minimumChargeEuro" value="<?php echo $coverageTypeClass->minimumChargeEuro; ?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onblur="return checkIfNumberMoreThanOneDots2('minimumChargeEuro')"/> </td>
		</tr>
		
		<!-- EXCESS EURO -->
		<tr>
			<td class="col1Per"/>
			<td class="col10Per"><?php echo $_SESSION['excessAmount']; ?>(<?php echo $EURO;?>):</td>
			<td class="col10Per"><input type="text" name="excessEuro" id="excessEuro" onkeydown="return validateOnRunTimeIsNumeric(event)" id="excessEuro" value="<?php echo $coverageTypeClass->excessEuro; ?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onblur="return checkIfNumberMoreThanOneDots2('excessEuro')"/></td>
		</tr>
		
		<tr>
			<td class="col1Per"/>
			<td class="col10Per"/>
			<td class="col10Per" colspan="6">
				<?php	
				
				//echo "read found coverages:".count($coverageType->coverages);
				//here coverage can be 'engineCapacity' or 'coverageAmount'
				$m = 0;//coverage repetition
				foreach($coverageTypeClass->coverages as $eachCoverage)
				{
					//$eachCoverage->printData();
					displayElementsNew($eachCoverage, $_SESSION[$eachCoverage->code], "CoverageTypeCoverages", $m);
					$m = $m + 1;
				}
			    
				?>
			</td>
		</tr>
		<?php
	    //TODO TODO change all displayElements as per above ARISTOS ARESTI TODO TODO
		  
	  	/* DISCOUNTS - show only when available */
	  	if($coverageTypeClass->discounts <> null)
	  	{
			//$coverageType->discounts->printData();
			?>
			 <tr>
				<td>
					<u> <?php echo $_SESSION['discounts']; ?> </u>
				</td>
			 </tr>
			  
			 <?php
			 
			writeToFile(date("Y-m-d H:i:s"). ": displayCoverageType(), found discount coverages:".count($coverageTypeClass->discounts->coverages)." \n", LOG_LEVEL_INFO);
                        
			$m = 0;//coverage repetition	
			foreach($coverageTypeClass->discounts->coverages as $eachCoverage)
			{
				//$eachCoverage->printData();
				displayElementsNew($eachCoverage, $_SESSION[$eachCoverage->code], "CoverageTypeDiscountsCoverages", $m);
				$m = $m + 1;
			}
		  	  
		  			  	  
	  	 }//end of DISCOUNTS
		
		?>
		<tr>
			<td class="col1Per"/>
			<td class="col10Per"/>
			<td class="col10Per"><input type="submit" value=<?php echo $_SESSION['save']; ?> /></td>
		</tr>
	  </table>
	</form>
	<?php

        writeToFile(date("Y-m-d H:i:s"). ": displayCoverageTypeFile() EXIT \n", LOG_LEVEL_INFO);
}


/* display all html elements contained in $basicCoverageInfo->standardCharges */
function displayStandardCharges($basicCoverageInfo)
{
	global $standardCharges, $stamps, $otherCharges, $fees, $EURO;
	global $TEXT_BOX_SIZE;

	writeToFile(date("Y-m-d H:i:s"). ": displayStandardCharges() ENTER \n", LOG_LEVEL_INFO);
        
	?>
	<table>
		<!-- THESE ELEMENTS ELEMENTS NOT REPEATED -->
		
		<tr><td class="col10Per" colspan="2"><?php echo  mb_strtoupper($_SESSION['standardCharges'], 'UTF-8'); ?></td></tr>
		<tr><td class="col10Per">MIF(%):</td>
		    <td class="col10Per"><input type="text" name="mifPercent" id="mifPercent"  onkeydown="return validateOnRunTimeIsNumeric(event)" value="<?php echo $basicCoverageInfo->standardCharges->MIFPercent; ?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onblur="return checkIfNumberMoreThanOneDots2('mifPercent')"/> </td>
		    <td class="col10Per">&nbsp</td>
			<td class="col10Per">&nbsp</td>
			<td class="col10Per">&nbsp</td>
			<td class="col10Per">&nbsp</td>
			<td class="col10Per"/>
		</tr>
		<tr><td  class="col10Per"><?php echo $_SESSION['stamps']; ?>(<?php echo $EURO;?>):</td>
		<td class="col10Per"><input type="text" name="stampsEuro" id="stampsEuro" onkeydown="return validateOnRunTimeIsNumeric(event)" value="<?php echo $basicCoverageInfo->standardCharges->stamps; ?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onblur="return checkIfNumberMoreThanOneDots2('stampsEuro')"/> 
		</td></tr>
		<tr><td class="col10Per"><?php echo $_SESSION['fees']; ?>(<?php echo $EURO;?>):</td>
		<td class="col10Per"><input type="text" name="feesEuro" id="feesEuro"  onkeydown="return validateOnRunTimeIsNumeric(event)" value="<?php echo $basicCoverageInfo->standardCharges->fees; ?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onblur="return checkIfNumberMoreThanOneDots2('feesEuro')"/> 
		</td></tr>
		<tr><td  class="col10Per"><?php echo $_SESSION['otherCharges']; ?>(<?php echo $EURO;?>):</td>
		<td class="col10Per"><input type="text" name="otherChargesEuro" id="otherChargesEuro" onkeydown="return validateOnRunTimeIsNumeric(event)" value="<?php echo $basicCoverageInfo->standardCharges->other; ?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onblur="return checkIfNumberMoreThanOneDots2('otherChargesEuro')"/> 
		</td></tr>
		<tr><td  class="col10Per"><?php echo $_SESSION['otherCharges']; ?>(%):</td>
		<td class="col10Per"><input type="text" name="otherChargesPercent" id="otherChargesPercent" onkeydown="return validateOnRunTimeIsNumeric(event)" value="<?php echo $basicCoverageInfo->standardCharges->otherPercent; ?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onblur="return checkIfNumberMoreThanOneDots2('otherChargesPercent')"/> 
		</td></tr>
	</table>
	<?php

        writeToFile(date("Y-m-d H:i:s"). ": displayStandardCharges() EXIT \n", LOG_LEVEL_INFO);
}




/* display elements included in displayClass. Generic Function.
	$displayClass : the charge or coverage class
	$tagHtmlName : Name to present for tag in HTML format, f.e "Age Charges"
	$tagNamePrefix : specific tag name, used as prefix for variable names, f.e "ageCharges" or "ageChargesOp"
	$m: coverage repetition
*/
function displayElementsNew($displayClass, $tagHtmlName, $tagNamePrefix, $m)
{
	global $categories, $fromTab, $toTab, $charges, $minimumCharge, $included, $applicable2, $yesNoSelectOptionOptions, $licenseYears, $days;
	global $maxWeight, $comprehensive, $allowedTab, $typeTab, $chargeType, $chargeTypeOptions, $EURO;
	global $TEXT_BOX_SIZE, $TEXT_BOX_SIZE_FOR_CATEGORIES, $YES_CONSTANT;
	
	writeToFile(date("Y-m-d H:i:s"). ": displayElementsNew() ENTER. Element=$tagHtmlName, tagNamePrefix=$tagNamePrefix \n", LOG_LEVEL_INFO);
        
	if($displayClass<>null)
	{	
			$isApplicable = $displayClass->applicable;
		
			$isAllowed = $displayClass->allowed;
				
			if(property_exists($displayClass, 'euro'))
				$isEuro = $displayClass->euro;
			else
				$isEuro = null;
				
			if(property_exists($displayClass, 'percentCharge'))
				$isPercentCharge = $displayClass->percentCharge;
			else
				$isPercentCharge = null;
				
			$isParam1 = $displayClass->param1;
			$isParam2 = $displayClass->param2;
			$isParam3 = $displayClass->param3;
			$isUnit = $displayClass->unit;
			$isCode = $displayClass->code;
				
			writeToFile(date("Y-m-d H:i:s"). ": isApplicable=$isApplicable \n", LOG_LEVEL_INFO);
			
			$tagCodeName = $tagNamePrefix."_CodeInc[]";

			writeToFile(date("Y-m-d H:i:s"). ": adding for tagCodeName=$tagCodeName, code=$displayClass->code \n", LOG_LEVEL_INFO);	
			
			
			//echo "defining table with id = $tableId <br/>";
			
			//echo "adding for tagCodeName=$tagCodeName, code=$displayClass->code <br/>";
			?>
			<tr>
			<!-- does this simple chk name cover all cases ??? -->
				<td class="col1Per"><input type="checkbox" name="chk[]" ></td><td><input type="hidden" name="<?php echo $tagCodeName; ?>" value="<?php echo $displayClass->code ?>" ></td>
				<td colspan="7"> 
				<table>
				 	<tr>
					<td class="col15Per"><u><?php echo $tagHtmlName; ?></u></td>
					<?php	
						
						$tagApplicableName = $tagNamePrefix."_ApplicableInc[]";
						$id = $tagNamePrefix.'_ApplicableInc_'.$m;
						
						?>
						<td class="col15Per">
						<?php echo $_SESSION['applicable']; ?> </b>
						<select name="<?php echo $tagApplicableName?>" id="<?php echo $id ?>">
							<?php
							foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
								?>
								 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
								 <?php
							 }
							 ?>		
						</select>
						</td>
						<!-- set correct select option -->
						<?php echo "<script language=javascript>setSelectTag(\"$isApplicable\", \"$id\")</script>"; ?>
						<?php
						
						$tagAllowedName = $tagNamePrefix."_AllowedInc[]";
						$id = $tagNamePrefix.'_AllowedInc_'.$m;
						
						?>
						<td class="col15Per">
						<?php echo $_SESSION['allowedTab']; ?> </b>
						<select name="<?php echo $tagAllowedName?>" id="<?php echo $id; ?>">
							<?php
							foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
								?>
								 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
								 <?php
							 }
							 ?>		
						</select>
						</td>
						<!-- set correct select option -->
						<?php echo "<script language=javascript>setSelectTag(\"$isAllowed\", \"$id\")</script>"; ?>
						<?php
						
						//EURO - PERCENTAGE
						$id = $tagNamePrefix.'_EuroInc_'.$m;
						$tagEuroName = $tagNamePrefix."_EuroInc[]";
						?>
						<td class="col15Per">
							<?php echo $EURO; ?>
							<input type="text" name="<?php echo $tagEuroName?>" id="<?php echo $id; ?>" value="<?php echo $isEuro;?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('<?php echo $id;?>')"  />
						</td>
						<td>
							<?php
							//PercentCharge
							$id = $tagNamePrefix.'_PercentChargeInc_'.$m;
							$tagPercentChargeName = $tagNamePrefix."_PercentChargeInc[]";
							?>
							% <input type="text" name="<?php echo $tagPercentChargeName?>" id="<?php echo $id?>" value="<?php echo $isPercentCharge;?>" size="<?php echo $TEXT_BOX_SIZE; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('<?php echo $id;?>')"/>
						</td>
						<?php
						
						//read all parameter info from coverageDefinitions.xml, like names of param1,2,3
						if($isCode <> null )
							$paramInfoArray = getParamInfo($displayClass->code);
						else
							$paramInfoArray = array();
						
						//display all parameters
						$k=1;
						foreach($paramInfoArray as $eachParam)
						{
								?>
								<td class="col15Per">
								<?php
								
								${"tagParam".$k."Name"} = $tagNamePrefix."_Param".$k."Inc[]";
							
								if($eachParam->name != "" )
								{
									writeToFile(date("Y-m-d H:i:s"). ": found parameter=$eachParam->name \n", LOG_LEVEL_INFO);
                                                                        
									//$eachParam->printData();
									
								 	echo $_SESSION[$eachParam->name]; ?></b>
								 	
								 	<?php
								 	//display as text box
								 	if($eachParam->displayType == "" )
								 	{ ?>
										<input type="text" name="<?php echo ${"tagParam".$k."Name"};?>" id="<?php echo ${"tagParam".$k."Name"};?>" value="<?php echo ${"isParam".$k};?>" size="<?php echo $TEXT_BOX_SIZE; ?>"  /> 
									<?php
									}	
									//display as select 
									else
									{
										//echo "INSIDE";
										?>
										<select name="<?php echo ${"tagParam".$k."Name"}?>" id="<?php echo ${"tagParam".$k."Name"} ?>">
										<?php
										foreach($_SESSION[$eachParam->name."Options"] as $option){
											?>
											 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
											 <?php
										 }
											 ?>			
										</select>
										<!-- set correct select option -->
										<?php echo "<script language=javascript>setSelectTag(\"${"isParam".$k}\", \"${"tagParam".$k."Name"}\")</script>"; ?>
										<?php
									}
								}
								
								else
								{
                                                                        writeToFile(date("Y-m-d H:i:s"). ": no parameter found \n", LOG_LEVEL_INFO);
									?>
									<input type="hidden" name="<?php echo ${"tagParam".$k."Name"};?>" id="<?php echo ${"tagParam".$k."Name"};?>" value="<?php echo ${"isParam".$k};?>" /> 
									<?php
								}
								?>
								</td>
								<?php
								
								$k++;
						}//foreach($paramInfoArray as $eachParam)
						
								
						$k=0;
										
					?>
					</td>
				</tr>
			<?php
			
			//display the <category>
			if(count($displayClass->categories) > 0 && count($displayClass->typeCategories)==0 )
				displayCategories($displayClass, $tagNamePrefix, $isUnit);
			//display the <typeCategory>
			else if(count($displayClass->typeCategories) > 0 && count($displayClass->categories)==0 )
				displayTypeCategories($displayClass, $tagNamePrefix, $isUnit);
			else
				displayCategories($displayClass, $tagNamePrefix, $isUnit);
			?>		
			
			</table>
		</td><!-- only td in the table -->
	</tr><!--outer tr to be deleted or added -->
	<?php
		
		}//if($displayClass<>null)

                writeToFile(date("Y-m-d H:i:s"). ": displayElementsNew() EXIT \n", LOG_LEVEL_INFO);
		
}//function displayElementsNew($displayClass, $tagName)


/* display the categories part of the tag, if any */
function displayTypeCategories($displayClass, $tagNamePrefix, $isUnit)
{
	global $EURO, $TEXT_BOX_SIZE, $TEXT_BOX_SIZE_FOR_CATEGORIES;
	
	$tableId = $tagNamePrefix."_".$displayClass->code;
			
	//dynamically set the variable names
	${$tagNamePrefix."EuroName"} = $displayClass->code."_typeCategories_".$tagNamePrefix."Euro[]";
	${$tagNamePrefix."PercentChargeName"} = $displayClass->code."_typeCategories_".$tagNamePrefix."PercentCharge[]";
	${$tagNamePrefix."PercentChargeComprehensiveName"} = $displayClass->code."_typeCategories_".$tagNamePrefix."PercentChargeComprehensive[]";
	${$tagNamePrefix."Allowed"} = $displayClass->code."_typeCategories_".$tagNamePrefix."Allowed[]";
	${$tagNamePrefix."MinimumEuro"} = $displayClass->code."_typeCategories_".$tagNamePrefix."MinimumEuro[]";
	${$tagNamePrefix."Type"} = $displayClass->code."_typeCategories_".$tagNamePrefix."Type[]";
	?>
	<tr>
		<td class="col10Per" colspan="3">
		<?php 
			echo $_SESSION['categories'];
		?>
		<INPUT type="button" value="<?php echo $_SESSION['add'];?>" onclick="addRowInTypeCategoriesTable('<?php echo $tableId?>', '<?php echo ${$tagNamePrefix."Type"};?>',  '<?php echo ${$tagNamePrefix."EuroName"};?>', '<?php echo ${$tagNamePrefix."PercentChargeName"};?>', '<?php echo ${$tagNamePrefix."Allowed"};?>', '<?php echo ${$tagNamePrefix."MinimumEuro"};?>')" />
		<INPUT type="button" value="<?php echo $_SESSION['remove'];?>" onclick="deleteRowFromTable('<?php echo $tableId?>')" /></td>
	</tr>
	 
	<!-- define table for categories inside this td -->
	<tr>
		<td class="col10Per" colspan="8">
		
		<table id="<?php echo $tableId;?>">
			<?php
			$k = 0;//category repetition 
			foreach($displayClass->typeCategories as $category)
			{
				$type = $category->type;
				$euro = $category->euro;
				$percentCharge = $category->percentCharge;
				$percentChargeComprehensive = $category->percentChargeComprehensive;
				$allowed = $category->allowed;
				$minimumEuro = $category->minimumEuro;
				
				//$category->printData();
				?>
				
				<tr>
				<td class="col1Per"><input type="checkbox" name="chkCategory[]" >
				<!--<input type="hidden" name='<?php echo ${$tagNamePrefix."Type"}; ?>' id=<?php echo ${$tagNamePrefix."Type"}?> value="<?php echo $type ?>" >-->
				</td>
				
				<!--type -->
				<td class="col1Per">
				<?php echo $_SESSION['typeTab'];?> 
				</td>
				<td class="col10Per">
				<input type="text" name="<?php echo ${$tagNamePrefix."Type"}; ?>" id="<?php echo ${$tagNamePrefix."Type"}?>" value="<?php echo $type; ?>" />
				</td>
				
				<!--EURO -->
				<td class="col1Per">
				<?php echo $EURO;?> 
				</td>
				<td class="col8Per">
				<input type="text" name="<?php echo ${$tagNamePrefix."EuroName"}; ?>" id="<?php echo ${$tagNamePrefix."EuroName"}?>" value="<?php echo $euro; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/> 
				</td>
				
				
				<!-- PERCENTAGE CHARGE-->
				<td class="col1Per"> % </td>
				<td class="col5Per"> 
				<input type="text" name="<?php echo ${$tagNamePrefix."PercentChargeName"}; ?>" id="<?php echo ${$tagNamePrefix."PercentChargeName"}?>" value="<?php echo $percentCharge; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/>
				</td>
				
				
				<!-- ALLOWED -->
				<td class="col12Per">
				<?php echo $_SESSION['allowedTab']; ?>
				<select name="<?php echo ${$tagNamePrefix."Allowed"}; ?>" id="<?php echo $displayClass->code."_".$tagNamePrefix.'_Allowed_'.$k; ?>">
					<?php
					foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>	
				</select>
				</td>
				<?php $id = $displayClass->code."_".$tagNamePrefix.'_Allowed_'.$k; ?>
				<!-- set correct select option -->
				<?php echo "<script language=javascript>setSelectTag(\"$allowed\", \"$id\")</script>"; ?>
				
				
				<td class="col8Per">
				<?php echo $_SESSION['minimumCharge']; ?>
				</td>
				<td class="col8Per">
				<input type="text" name="<?php echo ${$tagNamePrefix."MinimumEuro"}; ?>" id="<?php echo ${$tagNamePrefix."MinimumEuro"}?>" value="<?php echo $minimumEuro; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/> 
				</td>
				<?php
	 
				
				//PERCENTAGE FOR COMPREHENSIVE
				//show only when it exists
				if($percentChargeComprehensive<> null )
				{
					?>
					<td class="col15Per">
					<?php echo $_SESSION['comprehensive']; ?>(%)
					<input type="text" name="<?php echo ${$tagNamePrefix."PercentChargeComprehensiveName"}; ?>" id="<?php echo ${$tagNamePrefix."PercentChargeComprehensiveName"}?>" value="<?php echo $percentChargeComprehensive; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/> 
					</td>
					<?php
				}
				
				
				?>
				</td>
				</tr>
				<?php
				
				$k = $k + 1;
				
			}//foreach($displayClass->categories as $category)
			?>
			</table>
		</td>
	</tr>
			
	<?php
	
}

/* display the categories part of the tag, if any */
function displayCategories($displayClass, $tagNamePrefix, $isUnit)
{
	global $EURO, $TEXT_BOX_SIZE, $TEXT_BOX_SIZE_FOR_CATEGORIES;
	
	$tableId = $tagNamePrefix."_".$displayClass->code;
			
	//dynamically set the variable names
	${$tagNamePrefix."ToName"} = $displayClass->code."_".$tagNamePrefix."To[]";
	${$tagNamePrefix."FromName"} = $displayClass->code."_".$tagNamePrefix."From[]";
	${$tagNamePrefix."EuroName"} = $displayClass->code."_".$tagNamePrefix."Euro[]";
	${$tagNamePrefix."PercentChargeName"} = $displayClass->code."_".$tagNamePrefix."PercentCharge[]";
	${$tagNamePrefix."PercentChargeComprehensiveName"} = $displayClass->code."_".$tagNamePrefix."PercentChargeComprehensive[]";
	${$tagNamePrefix."Allowed"} = $displayClass->code."_".$tagNamePrefix."Allowed[]";
	${$tagNamePrefix."MinimumEuro"} = $displayClass->code."_".$tagNamePrefix."MinimumEuro[]";
	?>
	<tr>
		<td class="col10Per" colspan="3">
		<?php 
			echo $_SESSION['categories'];
			
			//UNIT
			$tagUnitName = $tagNamePrefix."_UnitInc[]";
	
			//show unit when not null - todo allow for user to update
			//if($displayClass->categories <> null && $displayClass->categories[0]->unit<>null)
			//if($displayClass->categories <> null && $displayClass->unit<>null)
				//echo "(".$_SESSION[$displayClass->unit].")"; 
			echo 'Unit:' ?>
				<input type="text" name="<?php echo $tagUnitName?>" id="<?php echo $tagUnitName?>" value="<?php echo $isUnit;?>" size="<?php echo $TEXT_BOX_SIZE; ?>" />
				:
			<INPUT type="button" value="<?php echo $_SESSION['add'];?>" onclick="addRowInCategoriesTable('<?php echo $tableId?>', '<?php echo ${$tagNamePrefix."FromName"};?>', '<?php echo ${$tagNamePrefix."ToName"};?>', '<?php echo ${$tagNamePrefix."EuroName"};?>', '<?php echo ${$tagNamePrefix."PercentChargeName"};?>', '<?php echo ${$tagNamePrefix."Allowed"};?>', '<?php echo ${$tagNamePrefix."MinimumEuro"};?>')" />
			<INPUT type="button" value="<?php echo $_SESSION['remove'];?>" onclick="deleteRowFromTable('<?php echo $tableId?>')" />
			
		</td>
	</tr>
	 
	<!-- define table for categories inside this td -->
	<tr>
		<td class="col10Per" colspan="8">
		
		<table id="<?php echo $tableId;?>">
			<?php
			$k = 0;//category repetition 
			foreach($displayClass->categories as $category)
			{
				$euro = $category->euro;
				$percentCharge = $category->percentCharge;
				$percentChargeComprehensive = $category->percentChargeComprehensive;
				$from = $category->from;
				$to = $category->to;
				//$type = $category->type;//third party, comprehensive, fire and theft etc.
				//$applicable = $category->applicable;
				$allowed = $category->allowed;
				$minimumEuro = $category->minimumEuro;
				
				//$category->printData();
				?>
				
				<tr>
				<td class="col1Per"><input type="checkbox" name="chkCategory[]" >
				<!--<input type="hidden" name='<?php echo ${$tagNamePrefix."Type"}; ?>' id=<?php echo ${$tagNamePrefix."Type"}?> value="<?php echo $type ?>" >-->
				</td>
				
				<!--FROM - TO -->
				<td class="col1Per">	
				<?php echo $_SESSION['fromTab']; ?>: 
				</td>
				<td class="col5Per">
				<input type="text" name='<?php echo ${$tagNamePrefix."FromName"}; ?>' id="<?php echo ${$tagNamePrefix."FromName"}?>" value="<?php echo $from; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES;?>" />
				</td>
				<td class="col1Per">
				<?php echo $_SESSION['toTab']; ?>:
				</td>
				<td class="col5Per">
				<input type="text" name='<?php echo ${$tagNamePrefix."ToName"};?>' id="<?php echo ${$tagNamePrefix."ToName"}?>" value="<?php echo $to; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/>
				</td>
				
				<!--EURO -->
				<td class="col1Per">
				<?php echo $EURO;?> 
				</td>
				<td class="col8Per">
				<input type="text" name="<?php echo ${$tagNamePrefix."EuroName"}; ?>" id="<?php echo ${$tagNamePrefix."EuroName"}?>" value="<?php echo $euro; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/> 
				</td>
				
				
				<!-- PERCENTAGE CHARGE-->
				<td class="col1Per"> % </td>
				<td class="col5Per"> 
				<input type="text" name="<?php echo ${$tagNamePrefix."PercentChargeName"}; ?>" id="<?php echo ${$tagNamePrefix."PercentChargeName"}?>" value="<?php echo $percentCharge; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/>
				</td>
				
				
				<!-- ALLOWED -->
				<td class="col12Per">
				<?php echo $_SESSION['allowedTab']; ?>
				<select name="<?php echo ${$tagNamePrefix."Allowed"}; ?>" id="<?php echo $displayClass->code."_".$tagNamePrefix.'_Allowed_'.$k; ?>">
					<?php
					foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>	
				</select>
				</td>
				<?php $id = $displayClass->code."_".$tagNamePrefix.'_Allowed_'.$k; ?>
				<!-- set correct select option -->
				<?php echo "<script language=javascript>setSelectTag(\"$allowed\", \"$id\")</script>"; ?>
				
				
				<td class="col8Per">
				<?php echo $_SESSION['minimumCharge']; ?>
				</td>
				<td class="col8Per">
				<input type="text" name="<?php echo ${$tagNamePrefix."MinimumEuro"}; ?>" id="<?php echo ${$tagNamePrefix."MinimumEuro"}?>" value="<?php echo $minimumEuro; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/> 
				</td>
				<?php
	 
				
				//PERCENTAGE FOR COMPREHENSIVE
				//show only when it exists
				if($percentChargeComprehensive<> null )
				{
					?>
					<td class="col15Per">
					<?php echo $_SESSION['comprehensive']; ?>(%)
					<input type="text" name="<?php echo ${$tagNamePrefix."PercentChargeComprehensiveName"}; ?>" id="<?php echo ${$tagNamePrefix."PercentChargeComprehensiveName"}?>" value="<?php echo $percentChargeComprehensive; ?>" onkeydown="return validateOnRunTimeIsNumeric(event)" size="<?php echo $TEXT_BOX_SIZE_FOR_CATEGORIES; ?>"/> 
					</td>
					<?php
				}
				
				?>
				</td>
				</tr>
				<?php
				
				$k = $k + 1;
				
			}//foreach($displayClass->categories as $category)
			?>
			</table>
		</td>
		
	</tr>
	
	<?php
	
}

/* Read from coveragesDefinitions.xml
Find coverage with code. 
Get names of param1,param2,param3 */
function getParamInfo($code)
{
	//$paramInfo = array();
	$paramInfo = array_fill(0, 3, new parameterNameValue());
	
	$k = 0;
	
	//look in coveragesDefinitions.xml
	foreach($_SESSION['coveragesDefinitions'] as $eachCoverageDefinition)
 	{
	 	//match coverageDefinitions.xml with code
	 	if($eachCoverageDefinition->code==$code)
	 	{
		 	
		 	foreach($eachCoverageDefinition->parameters as $eachParameter)
		 	{
			 	$parameterNameValue = new parameterNameValue();
			 	$parameterNameValue->name = $eachParameter->name;
			 	$parameterNameValue->displayType = $eachParameter->displayType;
			 	
		 		$paramInfo[$k] =  $parameterNameValue;
		 		$k++;
	 		}
 			
 			return $paramInfo;
		}
	}
	
	return $paramInfo;	
}

?>