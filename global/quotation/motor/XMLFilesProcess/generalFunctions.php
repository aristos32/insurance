<?php


/*  pass two file names
   returns TRUE if files are the same, FALSE otherwise */
function files_identical($fn1, $fn2) {
	global $READ_LEN;
    if(filetype($fn1) !== filetype($fn2))
        return FALSE;

    if(filesize($fn1) !== filesize($fn2))
        return FALSE;

    if(!$fp1 = fopen($fn1, 'rb'))
        return FALSE;

    if(!$fp2 = fopen($fn2, 'rb')) {
        fclose($fp1);
        return FALSE;
    }

    $same = TRUE;
    while (!feof($fp1) and !feof($fp2))
        if(fread($fp1, $READ_LEN) !== fread($fp2, $READ_LEN)) {
            $same = FALSE;
            break;
        }

    if(feof($fp1) !== feof($fp2))
        $same = FALSE;

    fclose($fp1);
    fclose($fp2);

    return $same;
}


/*search in the dir, and show all the xml files found
$dir : directory to search
return : 
*/
function displayAllXmlFilesInDir($dir)
{
	global $delete, $USER_ROLE_ADMINISTRATOR, $areYouSureYouWantToDeleteThisFile, $file;

	writeToFile(date("Y-m-d H:i:s"). ": displayAllXmlFilesInDir() ENTER \n", LOG_LEVEL_INFO);
        
	//Step 1 - Get list of available XML files
	$listOfFiles = getListOfFiles($dir, "");
	
	// files found
	if(count($listOfFiles)>0)
	{
		echo "<br> <br> ";
		//Step 2 - Fill array with the names of all company xmls
		$xmlFileNames = array();
		?>
		<table>
		<tr>
			<td><?php echo $file; ?></td>
			<td><?php echo $delete; ?></td>
		</tr>
		<?php
		foreach($listOfFiles as $companyXmlFile)
		{
			$xmlFileNames[] = $companyXmlFile;
			?>
			<tr>
				<td>
					<form action="./quotation.php" id="<?php echo $companyXmlFile;?>" method="post" style="display: none;">
					<input type="hidden" name="action" value="loadXMLFile" />
					<input type="hidden" name="fileName" value="<?php echo $companyXmlFile;?>" />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $companyXmlFile;?>').submit()"><?php echo $companyXmlFile ?></a>
				</td>
				<td>
					<?php
					//only administrators can delete xml files
					if($_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
					{
						$formId = "delete_".$companyXmlFile;
						?>
						<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
						<form action="./quotation.php" id="<?php echo $formId;?>" method="post" style="display: none;">
							<input type="text" name="action" value="deleteCompanyXML" />
							<input type="hidden" name="fileName" value="<?php echo $companyXmlFile;?>" />
						</form>
						<a href="javascript:;" onclick="javascript: if(confirm('<?php echo $_SESSION['areYouSureYouWantToDeleteThisFile'];?>')) document.getElementById('<?php echo $formId;?>').submit()"><?php echo $_SESSION['delete']; ?></a>
						<?php
					}
					?>
				</td>
			</tr>
			<?php
		
			//echo "$companyXmlFile <br> ";
		}	
		?>
		</table>
		<?php
	}
	//no files found
	else
	{
		echo "<br> <br> ";
		echo "No XML files found <br> ";
	}
        writeToFile(date("Y-m-d H:i:s"). ": displayAllXmlFilesInDir() EXIT \n", LOG_LEVEL_INFO);
}
?>