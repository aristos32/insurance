<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//if we logout in the meantime, don't modify file.
if(isset($_SESSION['login']))
{
	//process and save the current XML file
	processInsCompanyXMLFormNew();
	
	displayInsCompanyXMLFormNew($_SESSION['basicCoverageInfo']);
}
else
{
	echo $_SESSION['loginAgainToSaveTheFile'];
}
?>