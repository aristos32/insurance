<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');


//on language change, POST are not send. So, use ones in session
if(isset($_POST['vehicleType']))
	$_SESSION['vehicleType'] = $_POST['vehicleType'];
	
//echo "INSIDE for vehicletype= ".$_SESSION['vehicleType'];

//find existing coverages in directory
$optionalCoveragesFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $_SESSION['xmlFileName']."_optionalCoverages");
?>
<table>
<?php
if(count($optionalCoveragesFiles)>0)
{
	?><tr><td> Existing Optional Coverages Files: </td>
	<?php
	foreach($optionalCoveragesFiles as $eachCoverageFile)
	{
		?>
		<tr>
			<td/>
			<td>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="./quotation.php" id="<?php echo $eachCoverageFile;?>" method="post" style="display: none;">
				<input type="hidden" name="action" value="displayOptionalCoveragesFile" />
				<input type="hidden" name="optionalCoveragesFileName" value="<?php echo $eachCoverageFile;?>" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $eachCoverageFile;?>').submit()"><?php echo $eachCoverageFile; ?></a>
			</td>
		</tr>
		<?php
	}	
}
else
{
	?>
	<tr><td><?php	echo $_SESSION['noDataFound']; ?>.
	
  <!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="createOptionalCoveragesFile" method="post" style="display: none;">
		<input type="hidden" name="action" value="createOptionalCoveragesFile" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('createOptionalCoveragesFile').submit()"><?php echo $_SESSION['create']; ?></a>
	</td>
	</tr>
	<?php
}
?>
</table>
