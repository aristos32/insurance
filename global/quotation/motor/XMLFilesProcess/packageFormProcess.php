<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//if we logout in the meantime, don't modify file.
if(isset($_SESSION['login']))
{
	//on language change, don't validate and save again, because file will be erases
	if(	$actionRead != "" )
		//process and save the current XML file
		savePackageFile($_SESSION['packageFileName']);
	//$coverageTypeClass->printData();
	
	$packageFileClass = readPackageXML($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $_SESSION['packageFileName']);
	//$packageFileClass->printData();

	displayPackageFile($packageFileClass);
	
	?>	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="returnToPreviousScreen" method="post" style="display: none;">
		<input type="text" name="action" value="loadXMLFile" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('returnToPreviousScreen').submit()"><?php echo $_SESSION['return']; ?></a>
	<?php
}
else
{
	echo $_SESSION['loginAgainToSaveTheFile'];
}
?>