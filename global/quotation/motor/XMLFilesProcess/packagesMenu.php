<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');


//on language change, POST are not send. So, use ones in session
if(isset($_POST['vehicleType']))
	$_SESSION['vehicleType'] = $_POST['vehicleType'];
	
//echo "INSIDE for vehicletype= ".$_SESSION['vehicleType'];

//find existing coverages in directory
$packagesFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $_SESSION['xmlFileName']."_package_");
?>
<table>
<?php
if(count($packagesFiles)>0)
{
	?><tr><td> Existing Packages: </td>
	<?php
	foreach($packagesFiles as $eachPackageFile)
	{
		?>
		<tr>
			<td/>
			<td>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="./quotation.php" id="<?php echo $eachPackageFile;?>" method="post" style="display: none;">
				<input type="hidden" name="action" value="displayPackageFile" />
				<input type="hidden" name="packageFileName" value="<?php echo $eachPackageFile;?>" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $eachPackageFile;?>').submit()"><?php echo $eachPackageFile; ?></a>
			</td>
			<td class="col10Per">
				<?php
				//only administrators can delete xml files
				if($_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
				{
					$deleteId = "delete_".$eachPackageFile;
					?>
					<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
					<form action="./quotation.php" id="<?php echo $deleteId;?>" method="post" style="display: none;">
						<input type="text" name="action" value="deleteSingleXML" />
						<input type="hidden" name="fileName" value="<?php echo $eachPackageFile;?>" />
					</form>
					<a href="javascript:;" onclick="javascript: if(confirm('<?php echo $_SESSION['areYouSureYouWantToDeleteThisFile'];?>')) document.getElementById('<?php echo $deleteId;?>').submit()"><?php echo $_SESSION['delete']; ?></a>
					<?php
				}
				?>
			</td>
		</tr>
		<?php
	}	
}
else
{
	?><tr><td><?php	echo $_SESSION['noDataFound']; ?> </td>
		</tr>
		
<?php
}
?>
<tr><td><br/></td></tr>
<tr>
  <td class="col10Per">
  <!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="createNewPackageFile" method="post" style="display: none;">
		<input type="hidden" name="action" value="createNewPackageFile" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('createNewPackageFile').submit()"><?php echo $_SESSION['create']; ?></a>
	</td>
	<td>
	<!-- display all client contracts - 
	create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="returnToPreviousScreen" method="post" style="display: none;">
		<input type="text" name="action" value="loadXMLFile" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('returnToPreviousScreen').submit()"><?php echo $_SESSION['return']; ?></a>
	</td>
</tr>
</table>
