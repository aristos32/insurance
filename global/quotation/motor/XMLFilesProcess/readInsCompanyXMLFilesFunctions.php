<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

	
$vehicleTypes;
$vehicleTypeClass;
$inclusiveBenefitsClass;
$basicCoverageClass;
	
/* Read ALL the values for the xmlFileName and ALL related files.
   This function is generic and works for all the company xml files
   $xmlFileName : file in the root folder. All other files are related to this name */
function readValuesFromCompanyXmlNew($path, $xmlFileName)
{
	global $YES_CONSTANT, $NO_CONSTANT;
	global $basicCoverageClass;

	writeToFile(date("Y-m-d H:i:s"). ": readValuesFromCompanyXmlNew() for path=$path, file=$xmlFileName ENTER  \n", LOG_LEVEL_INFO);
        
	$doc = new DOMDocument();
	$doc->load( $path.$xmlFileName );
	
	/* BASICCOVERAGE TAG  */
	$basicCoverages = $doc->getElementsByTagName( "basicCoverage" );
	
	 $tempArray = explode(".", $xmlFileName);
	 
	 $xmlFileNameWithoutExtention = $tempArray[0];
	
	foreach($basicCoverages as $basicCoverage)
	{
		$basicCoverageClass = new basicCoverage();
		
		/* INSURANCE COMPANY NAME */
		$basicCoverageClass->companyName = $basicCoverage->getAttribute('companyName');
		//echo $coverage->getAttribute('companyName');
		
		/* STANDARDCHARGES TAG */
		readStandardChargesTag($basicCoverage);
		
		/* for all vehicle types, read all related files */
		//directory name =  vehicle type
		$directories = getListOfDirectories($_SESSION['clientFilesLocation']."/XMLFiles/");
		foreach($directories as $eachDirectory)
		{
			$vehicleTypeClass = new vehicleType();
			
			/* VEHICLETYPE ATTRIBUTE  'name' */
			$vehicleTypeClass->name = $eachDirectory;
			
			/* VEHICLETYPE ATTRIBUTE  'value' */
			$vehicleTypeClass->value = $eachDirectory;
			
			//read all coverage files available. TP, TPFT, Comprehensive
			$coveragesFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $xmlFileNameWithoutExtention."_coverageType_");
			/* read each coveragetype */
			foreach($coveragesFiles as $eachCoverageXML)
			{
				$vehicleTypeClass->coverageTypes[] = readCoverageXML($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $eachCoverageXML);
			}
			/*foreach($vehicleTypeClass->coverageTypes as $eachCoverageXML)
			{
				$eachCoverageXML->printData();
			}*/
			
			/* INCLUSIVE BENEFITS TAG - read all packages for this vehicle type */
			$packagesFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $xmlFileNameWithoutExtention."_package_");
			foreach($packagesFiles as $eachPackageXML)
			{
				$vehicleTypeClass->inclusiveBenefits[] = readPackageXML($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $eachPackageXML);
			}
			
			/* ADDITIONAL CHARGES TAG */
			$additionalCharges = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $xmlFileNameWithoutExtention."_additionalCharges");
			foreach($additionalCharges as $eachAdditionalChargeXML)
			{
				$vehicleTypeClass->additionalChargesFromXMLArray = readAdditionalChargesXML($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $eachAdditionalChargeXML);
			}
			
			
			
			/* OPTIONAL COVERAGES TAG */
			$optionalCoverages = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $xmlFileNameWithoutExtention."_optionalCoverages");
			//print_r($optionalCoverages);
			
			foreach($optionalCoverages as $eachOptionalCoverageXML)
			{
				$vehicleTypeClass->optionalCoveragesFromXMLArray = readOptionalCoveragesXML($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $eachOptionalCoverageXML);
			}
			/*
			if(count($optionalCoverages)>0)
				foreach($vehicleTypeClass->optionalCoveragesFromXMLArray as $eachCoverageType)
					$eachCoverageType->printData();
			*/	
			
			$basicCoverageClass->vehicleTypes[] = $vehicleTypeClass;
			
		}
		
	}

	writeToFile(date("Y-m-d H:i:s"). ": readValuesFromCompanyXmlNew() EXIT \n", LOG_LEVEL_INFO);
        
	return $basicCoverageClass;

} //function readValuesFromCompanyXmlNew($xmlFileName)



/* read the coveragetype XML, inside a vehicleType folder.
	$path : complete directory
	xmlFileName : name of file to read
	*/
function readCoverageXML($path, $xmlFileName)
{
	global $YES_CONSTANT;

        writeToFile(date("Y-m-d H:i:s"). ": readCoverageXML() ENTER \n", LOG_LEVEL_INFO);
        
	$doc = new DOMDocument();
	$doc->load( $path.$xmlFileName );
	
	/* BASICCOVERAGE TAG  */
	$coverageTypes = $doc->getElementsByTagName( "coverageType" );

	$coverageTypeClass = new coverageType();	
	
	foreach($coverageTypes as $eachCoverageType)
	{
		
		/* COVERAGETYPE ATTRIBUTE  'name' */
		$coverageTypeClass->value = $eachCoverageType->getAttribute('value');//todo remove
		
		//$coverageTypeClass->name = $eachCoverageType->getAttribute('name');//todo remove
			
		$coverageTypeClass->available = $eachCoverageType->getElementsByTagName( "available" )->item(0)->nodeValue;
			
		$coverageTypeClass->minimumChargeEuro = $eachCoverageType->getElementsByTagName( "minimumChargeEuro" )->item(0)->nodeValue;
		$coverageTypeClass->excessEuro = $eachCoverageType->getElementsByTagName( "excessEuro" )->item(0)->nodeValue;
		
		$coverages = $eachCoverageType->getElementsByTagName('coverage');
		
		foreach($coverages as $eachCoverage)
		{
			//echo "inside coverage<br>";
			$coverageTypeClass->coverages[] = readCoverageTag($eachCoverage);
		}
			
		/* DISCOUNTS TAG */
		$coverageTypeClass->discounts = readDiscountsTag($eachCoverageType);
	}	

	writeToFile(date("Y-m-d H:i:s"). ": readCoverageXML() EXIT \n", LOG_LEVEL_INFO);
        
	return $coverageTypeClass;
}


/* read the coveragetype XML, inside a vehicleType folder.
	$path : complete directory
	xmlFileName : name of file to read
	*/
function readPackageXML($path, $xmlFileName)
{
	global $YES_CONSTANT;

        writeToFile(date("Y-m-d H:i:s"). ": readPackageXML() ENTER \n", LOG_LEVEL_INFO);
        
	$doc = new DOMDocument();
	$doc->load( $path.$xmlFileName );
	
	/* BASICCOVERAGE TAG  */
	$packages = $doc->getElementsByTagName( "inclusiveBenefits" );

	$packageClass = new inclusiveBenefit();	
	
	foreach($packages as $eachPackage)
	{
		
		/* package ATTRIBUTE  'name' */
		$packageClass->name = $eachPackage->getAttribute('name');//todo remove
		$packageClass->value = $eachPackage->getAttribute('value');//todo remove
			
		$packageClass->available = $eachPackage->getElementsByTagName( "available" )->item(0)->nodeValue;
			
		$packageClass->coverageTypeOfOffer = $eachPackage->getElementsByTagName( "coverageTypeOfOffer" )->item(0)->nodeValue;
		$packageClass->chargeOverBasic = $eachPackage->getElementsByTagName( "chargeOverBasic" )->item(0)->nodeValue;
		
		$coverages = $eachPackage->getElementsByTagName('coverage');
		
		foreach($coverages as $eachCoverage)
		{
			//echo "inside coverage<br>";
			$packageClass->optionalCoveragesFromXMLArray[] = readCoverageTag($eachCoverage);
			
		}
			
		/* DISCOUNTS TAG */
		$packageClass->discounts = readDiscountsTag($eachPackage);
	}	

	writeToFile(date("Y-m-d H:i:s"). ": readPackageXML() EXIT \n", LOG_LEVEL_INFO);
        
	return $packageClass;
}


/* read the standardCharges TAG.
	$coverageType : basicCoverage TAG */
function readStandardChargesTag($coverageType)
{
	global $basicCoverageClass;
	
	$standardCharges = $coverageType->getElementsByTagName('standardCharges');
	foreach($standardCharges as $standardCharge)
	{
		$standardChargesClass = new standardCharges();
		$standardChargesClass->MIFPercent = $standardCharge->getElementsByTagName( "MIFPercent" )->item(0)->nodeValue;
		$standardChargesClass->stamps = $standardCharge->getElementsByTagName( "stamps" )->item(0)->nodeValue;
		$standardChargesClass->fees = $standardCharge->getElementsByTagName( "fees" )->item(0)->nodeValue;
		$standardChargesClass->other = $standardCharge->getElementsByTagName( "other" )->item(0)->nodeValue;
		$standardChargesClass->otherPercent = $standardCharge->getElementsByTagName( "otherPercent" )->item(0)->nodeValue;
	}
	/* add to array */
	$basicCoverageClass->standardCharges = $standardChargesClass;
}



/* read the inclusiveBenefits TAG.
	$vehicleType : vehicleType TAG, like SALOON, COMMERCIAL OWN GOODS etc */
function readInclusiveBenefitsTag($vehicleType)
{
	global $vehicleTypeClass;
	global $inclusiveBenefitsClass;
	global $YES_CONSTANT;
	$optionalCoveragesFromXMLArray = array();

	$inclusiveBenefits = $vehicleType->getElementsByTagName('inclusiveBenefits');
	
	foreach($inclusiveBenefits as $inclusiveBenefit)
	{
		$inclusiveBenefitsClass = new inclusiveBenefit();
		$inclusiveBenefitsClass->name = $inclusiveBenefit->getAttribute('name');
		$inclusiveBenefitsClass->available = $inclusiveBenefit->getElementsByTagName( "available" )->item(0)->nodeValue;
		$inclusiveBenefitsClass->coverageTypeOfOffer = $inclusiveBenefit->getElementsByTagName( "coverageTypeOfOffer" )->item(0)->nodeValue;
					
		/* read the tags that have a single value */
		readSingleValueInInclusiveBenefitsTags($inclusiveBenefit);
		
		/* read the Optional Coverages Tag included in the inclusiveBenefit tag */
		$includedCoverages = $inclusiveBenefit->getElementsByTagName('includedCoverages');
		//$includedCoverages->printData();
		$inclusiveBenefitsClass->optionalCoveragesFromXMLArray = array();
		$inclusiveBenefitsClass->optionalCoveragesFromXMLArray = readOptionalCoveragesTag($includedCoverages);
		//foreach($inclusiveBenefitsClass->optionalCoveragesFromXMLArray as $eachCoverage)
		//	if($eachCoverage->included==$YES_CONSTANT)
			//	$eachCoverage->printData();
		
		/* add to array */
		//$inclusiveBenefitsClass->printData();
		$vehicleTypeClass->inclusiveBenefits[] = $inclusiveBenefitsClass;
		//$vehicleTypeClass->printData();
	}//INCLUSIVE BENEFITS
}//function readInclusiveBenefitsTag($vehicleType)



/* read the <coverage> TAG and put into a PHP object
	read all tag attributes, elements and categories
	$coverageTag : <coverage> XML tag
*/
function readCoverageTag($coverageTag)
{
		
	//get the categories inside the class
	$categories = $coverageTag->getElementsByTagName('category');
	$typeCategories = $coverageTag->getElementsByTagName('typeCategory');
	$localCoveragesFromCompanyXML = new coveragesFromCompanyXML();//TODO - replace this with genericCoverageCharge
	
	//read values of class, outside of categories
	$localCoveragesFromCompanyXML->applicable = $coverageTag->getAttribute('applicable');
	//$localCoveragesFromCompanyXML->included = $coverageTag->getAttribute('included');
	$localCoveragesFromCompanyXML->allowed = $coverageTag->getAttribute('allowed');
	$localCoveragesFromCompanyXML->code = $coverageTag->getElementsByTagName( 'code' )->item(0)->nodeValue;
	if($coverageTag->getElementsByTagName( "euro" )->item(0) <> null )
		$localCoveragesFromCompanyXML->euro = $coverageTag->getElementsByTagName( "euro" )->item(0)->nodeValue;
	if($coverageTag->getElementsByTagName( "percentCharge" )->item(0) <> null )
		$localCoveragesFromCompanyXML->percentCharge = $coverageTag->getElementsByTagName( "percentCharge" )->item(0)->nodeValue;
	if($coverageTag->getElementsByTagName( "param1" )->item(0) <> null )
		$localCoveragesFromCompanyXML->param1 = $coverageTag->getElementsByTagName( "param1" )->item(0)->nodeValue;
	if($coverageTag->getElementsByTagName( "param2" )->item(0) <> null )
		$localCoveragesFromCompanyXML->param2 = $coverageTag->getElementsByTagName( "param2" )->item(0)->nodeValue;
	if($coverageTag->getElementsByTagName( "param3" )->item(0) <> null )
		$localCoveragesFromCompanyXML->param3 = $coverageTag->getElementsByTagName( "param3" )->item(0)->nodeValue;
	if($coverageTag->getElementsByTagName( "unit" )->item(0) <> null )
		$localCoveragesFromCompanyXML->unit = $coverageTag->getElementsByTagName( "unit" )->item(0)->nodeValue;

	writeToFile(date("Y-m-d H:i:s"). ": applicable=$localCoveragesFromCompanyXML->applicable \n", LOG_LEVEL_INFO);
		
	//read all catetories
	foreach($typeCategories as $category)
	{
		$localCoveragesFromCompanyXML->typeCategories[] = readTypeCategory($category);
		//$genericCategory->printData();
	}
	
	//read all catetories
	foreach($categories as $category)
	{
		$localCoveragesFromCompanyXML->categories[] = readGenericCategory($category);
		//$genericCategory->printData();
	}	
		
	return $localCoveragesFromCompanyXML;
	
}

/* read from XML tag and insert into php object.
$category : XML <category> tag
*/
function readGenericCategory($category)
{
	$genericCategory = new genericCategory();
	//$genericCategory->applicable = $category->getAttribute( "applicable" );
	$genericCategory->allowed = $category->getAttribute( "allowed" );
	
	if($category->getElementsByTagName( "type" )->item(0) <> null ){
		$genericCategory->type = $category->getElementsByTagName( "type" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "from" )->item(0) <> null ){
		$genericCategory->from = $category->getElementsByTagName( "from" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "to" )->item(0) <> null ){
		$genericCategory->to = $category->getElementsByTagName( "to" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "euro" )->item(0) <> null ){
		$genericCategory->euro = $category->getElementsByTagName( "euro" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "percentCharge" )->item(0) <> null ){
		$genericCategory->percentCharge = $category->getElementsByTagName( "percentCharge" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "percentChargeComprehensive" )->item(0) <> null ){
		$genericCategory->percentChargeComprehensive = $category->getElementsByTagName( "percentChargeComprehensive" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "minimumEuro" )->item(0) <> null ){
		$genericCategory->minimumEuro = $category->getElementsByTagName( "minimumEuro" )->item(0)->nodeValue;
	}
	
	//$genericCategory->printData();
	
	return $genericCategory;
}

/* read from XML tag and insert into php object.
$category : XML <typeCategory> tag */
function readTypeCategory($category)
{
	$typeCategory = new typeCategory();
	//$genericCategory->applicable = $category->getAttribute( "applicable" );
	$typeCategory->allowed = $category->getAttribute( "allowed" );
	
	if($category->getElementsByTagName( "type" )->item(0) <> null ){
		$typeCategory->type = $category->getElementsByTagName( "type" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "euro" )->item(0) <> null ){
		$typeCategory->euro = $category->getElementsByTagName( "euro" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "percentCharge" )->item(0) <> null ){
		$typeCategory->percentCharge = $category->getElementsByTagName( "percentCharge" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "percentChargeComprehensive" )->item(0) <> null ){
		$typeCategory->percentChargeComprehensive = $category->getElementsByTagName( "percentChargeComprehensive" )->item(0)->nodeValue;
	}
	if($category->getElementsByTagName( "minimumEuro" )->item(0) <> null ){
		$typeCategory->minimumEuro = $category->getElementsByTagName( "minimumEuro" )->item(0)->nodeValue;
	}
	
	//$genericCategory->printData();
	
	return $typeCategory;
}

/* read the optional Coverages TAG included */
function readOptionalCoveragesXML($path, $xmlFileName)
{
	writeToFile(date("Y-m-d H:i:s"). ": readOptionalCoveragesTag() ENTER \n", LOG_LEVEL_INFO);
        
	$doc = new DOMDocument();
	$doc->load( $path.$xmlFileName );
	
	/* BASICCOVERAGE TAG  */
	$optionalCoverages = $doc->getElementsByTagName( "optionalCoverages" );

//	$optionalCoverages = $outerTag->getElementsByTagName('optionalCoverages');
	$optionalCoveragesFromXMLArray = array();
		
	$optionalCoveragesFromXMLArray = readOptionalCoveragesTag($optionalCoverages);

	writeToFile(date("Y-m-d H:i:s"). ": readOptionalCoveragesTag() EXIT \n", LOG_LEVEL_INFO);
        
	return $optionalCoveragesFromXMLArray;
	
}


/* read the additional Charges XML file */
function readAdditionalChargesXML($path, $xmlFileName)
{
	writeToFile(date("Y-m-d H:i:s"). ": readAdditionalChargesXML() ENTER \n", LOG_LEVEL_INFO);
        
	$doc = new DOMDocument();
	$doc->load( $path.$xmlFileName );
	
	/* BASICCOVERAGE TAG  */
	$additionalCharges = $doc->getElementsByTagName( "additionalCharges" );

//	$optionalCoverages = $outerTag->getElementsByTagName('optionalCoverages');
	$additionalChargesFromXMLArray = array();
		
	$additionalChargesFromXMLArray = readAdditionalChargesTag($additionalCharges);

	writeToFile(date("Y-m-d H:i:s"). ": readAdditionalChargesXML() EXIT \n", LOG_LEVEL_INFO);
        
	return $additionalChargesFromXMLArray;
	
}

/* read the optional Coverages TAG included */
function readOptionalCoveragesTag($optionalCoverages)
{
	writeToFile(date("Y-m-d H:i:s"). ": readOptionalCoveragesTag() ENTER \n", LOG_LEVEL_INFO);
        
//	$optionalCoverages = $outerTag->getElementsByTagName('optionalCoverages');
	$optionalCoveragesFromXMLArray = array();
		
	foreach($optionalCoverages as $optionalCoverage)
	{
		$coverages = $optionalCoverage->getElementsByTagName('coverage');
		foreach($coverages as $eachCoverage)
		{
			//echo "inside coverage<br>";
			$optionalCoveragesFromXMLArray[] = readCoverageTag($eachCoverage);
		}
		
		//foreach($optionalCoveragesFromXMLArray as $readCoverage)
			//$readCoverage->printData();
		
	}

	writeToFile(date("Y-m-d H:i:s"). ": readOptionalCoveragesTag() EXIT \n", LOG_LEVEL_INFO);
        
	return $optionalCoveragesFromXMLArray;
	
}



/* read the additionalCharges TAG 
	$vehicleType : vehicleType TAG, like SALOON, COMMERCIAL OWN GOODS etc */
function readAdditionalChargesTag($additionalCharges)
{
	writeToFile(date("Y-m-d H:i:s"). ": readAdditionalChargesTag() ENTER \n", LOG_LEVEL_INFO);
        
	$additionalChargesFromXMLArray = array();
				
	foreach($additionalCharges as $additionalCharge)
	{
		$charges = $additionalCharge->getElementsByTagName('charge');
		foreach($charges as $eachCharge)
		{
			//echo "inside coverage<br>";
			$additionalChargesFromXMLArray[] = readChargeTag($eachCharge);
		}
		
		//foreach($additionalChargesFromXMLArray as $eachCharge)
		//	;//$eachCharge->printData();
		
	}

	writeToFile(date("Y-m-d H:i:s"). ": readAdditionalChargesTag() EXIT \n", LOG_LEVEL_INFO);	
	return $additionalChargesFromXMLArray;
}


/* read the <charge> TAG and put into a PHP object
	read all tag attributes, elements and categories
	$chargeTag : <charge> XML tag */
function readChargeTag($chargeTag)
{
	//get the categories inside the class
	$categories = $chargeTag->getElementsByTagName('category');
	$typeCategories = $chargeTag->getElementsByTagName('typeCategory');
	$localChargesFromCompanyXML = new chargesFromCompanyXML();//TODO - replace this with genericCoverageCharge
	
	//read values of class, outside of categories
	$localChargesFromCompanyXML->applicable = $chargeTag->getAttribute('applicable');
	//$localChargesFromCompanyXML->included = $chargeTag->getAttribute('included');
	$localChargesFromCompanyXML->allowed = $chargeTag->getAttribute('allowed');
	
	$localChargesFromCompanyXML->code = $chargeTag->getElementsByTagName( 'code' )->item(0)->nodeValue;
	if($chargeTag->getElementsByTagName( "euro" )->item(0) <> null )
		$localChargesFromCompanyXML->euro = $chargeTag->getElementsByTagName( "euro" )->item(0)->nodeValue;
	if($chargeTag->getElementsByTagName( "percentCharge" )->item(0) <> null )
		$localChargesFromCompanyXML->percentCharge = $chargeTag->getElementsByTagName( "percentCharge" )->item(0)->nodeValue;
	if($chargeTag->getElementsByTagName( "param1" )->item(0) <> null )
		$localChargesFromCompanyXML->param1 = $chargeTag->getElementsByTagName( "param1" )->item(0)->nodeValue;
	if($chargeTag->getElementsByTagName( "param2" )->item(0) <> null )
		$localChargesFromCompanyXML->param2 = $chargeTag->getElementsByTagName( "param2" )->item(0)->nodeValue;
	if($chargeTag->getElementsByTagName( "param3" )->item(0) <> null )
		$localChargesFromCompanyXML->param3 = $chargeTag->getElementsByTagName( "param3" )->item(0)->nodeValue;
	if($chargeTag->getElementsByTagName( "unit" )->item(0) <> null )
		$localChargesFromCompanyXML->unit = $chargeTag->getElementsByTagName( "unit" )->item(0)->nodeValue;

	writeToFile(date("Y-m-d H:i:s"). ": applicable=$localChargesFromCompanyXML->applicable \n", LOG_LEVEL_INFO);
		
	//read all catetories
	foreach($categories as $category)
	{
		$localChargesFromCompanyXML->categories[] = readGenericCategory($category);
		//$genericCategory->printData();
	}
	
	//read all catetories
	foreach($typeCategories as $category)
	{
		$localChargesFromCompanyXML->typeCategories[] = readTypeCategory($category);
		//$category->printData();
	}	
	
	
	
	return $localChargesFromCompanyXML;
	
}

/* read the discounts tag 
	$comprehensive : vehicleType->comprehensive TAG */
function readDiscountsTag($coverageType)
{
	writeToFile(date("Y-m-d H:i:s"). ": readDiscountsTag() ENTER \n", LOG_LEVEL_INFO);
        
	$discountsClass = new discounts();
	$discounts = $coverageType->getElementsByTagName('discounts')->item(0);

	writeToFile(date("Y-m-d H:i:s"). ": readDiscountsTag() discount tags found:". count($discounts). " \n", LOG_LEVEL_INFO);
        
	//if <discounts> tag is not found, set return value to null
	if($discounts == null)
		$discountsClass =  null;
	//discounts tag found
	else
	{
		$discountsClass->available = $discounts->getElementsByTagName( "available" )->item(0)->nodeValue;
		
		$coverages = $discounts->getElementsByTagName('coverageDiscount');//not using 'coverages' so that the read is done twice
	
		foreach($coverages as $eachCoverage)
		{
			//echo "inside coverage<br>";
			$discountsClass->coverages[] = readCoverageTag($eachCoverage);
		}
		
		
	}

	writeToFile(date("Y-m-d H:i:s"). ": readDiscountsTag() EXIT \n", LOG_LEVEL_INFO);
        
	return $discountsClass;	
	
}
	

/*  read the single value tags.
	$vehicleType : vehicleType->inclusiveBenefit TAG */
function readSingleValueInInclusiveBenefitsTags($inclusiveBenefit)
{
	global $inclusiveBenefitsClass;
	global $YES_CONSTANT;	
	
	$elems =  $inclusiveBenefit->getElementsByTagName( "mandatory" );	
	if($elems&& $elems->item(0))
		$inclusiveBenefitsClass->mandatory = $inclusiveBenefit->getElementsByTagName( "mandatory" )->item(0)->nodeValue;
		
	$elems =  $inclusiveBenefit->getElementsByTagName( "chargeOverBasic" );
	if($elems&& $elems->item(0))
		$inclusiveBenefitsClass->chargeOverBasic = $inclusiveBenefit->getElementsByTagName( "chargeOverBasic" )->item(0)->nodeValue;
		
}




	
?>