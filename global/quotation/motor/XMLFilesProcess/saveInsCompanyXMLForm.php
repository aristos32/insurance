<?php

/* Validate the insCompanyXMLForm form for an insurance company.
If validation is ok, then save as new XML file in server.  */
function processInsCompanyXMLFormNew()
{
	writeToFile(date("Y-m-d H:i:s"). ": processInsCompanyXMLFormNew() ENTER \n", LOG_LEVEL_INFO);
        
	//initialize error variable in every call
	$_SESSION['processCompanyXMLErrorMessage'] = '';
	
	$newFileName = $_POST['fileName'];
	
	$basicCoverageClass = new basicCoverage();
	$basicCoverageClass->companyName = $_POST['companyName'];
	$basicCoverageClass->standardCharges = new standardCharges();
	
	//VALIDATE STANDARD CHARGES
	//validateStandardCharges($basicCoverageClass);//call by reference
	$basicCoverageClass->standardCharges->MIFPercent = $_POST['mifPercent'];
	$basicCoverageClass->standardCharges->stamps = $_POST['stampsEuro'];
	$basicCoverageClass->standardCharges->fees = $_POST['feesEuro'];
	$basicCoverageClass->standardCharges->other = $_POST['otherChargesEuro'];
	$basicCoverageClass->standardCharges->otherPercent = $_POST['otherChargesPercent'];
	
	//clone objects	
	$_SESSION['basicCoverageInfo'] = clone $basicCoverageClass;
	
	//if we had no errors
	
	//create new xml and store in server with appropriate name
	createUpdatedXmlNew($_SESSION['clientFilesLocation']."/XMLFiles/", $_SESSION['currentInputXMLFileName'], $newFileName);

	writeToFile(date("Y-m-d H:i:s"). ": processInsCompanyXMLFormNew() EXIT \n", LOG_LEVEL_INFO);	
	
}


/* create new company file
	$filePath = path to delete/create XML files
	$previousFileName = name of the previous file(if different)
	$newFileName = name of the new file(if different)
*/
function createUpdatedXmlNew($filePath, $previousFileName, $newFileName)
{
	//delete old file
	@unlink($filePath.$previousFileName);
	
	//get name without .xml. From model.xml get only model
	$tempArray = explode(".", $previousFileName);
	
	$previousFileNameWithoutExtention = $tempArray[0];
	
	// Creates a new document according to 1.0 specs
	$dom = new DOMDocument("1.0");
	$dom->formatOutput = true;
	$dom->preserveWhiteSpace = false;
	
	//PROPERTIES
	$properties = $dom->createElement("properties");//root element
	$dom->appendChild($properties);
	
	//BASICCOVERAGE
	$basicCoverage = $dom->createElement("basicCoverage");
	$properties->appendChild($basicCoverage);
	
	// create attribute type node
	$companyName = $dom->createAttribute("companyName");
	$basicCoverage->appendChild($companyName);
	// create attribute value node
	$typeValue = $dom->createTextNode($_SESSION['basicCoverageInfo']->companyName);
	$companyName->appendChild($typeValue);
		
	//STANDART CHARGES
	$standardCharges = $dom->createElement("standardCharges");
	$basicCoverage->appendChild($standardCharges);	
        
	$MIFPercent = $dom->createElement("MIFPercent");
	$standardCharges->appendChild($MIFPercent);
	//fill text
	$text = $dom->createTextNode($_SESSION['basicCoverageInfo']->standardCharges->MIFPercent);
	$MIFPercent->appendChild($text);
        
        
	$stamps = $dom->createElement("stamps");
	$standardCharges->appendChild($stamps);
	//fill text
	$text = $dom->createTextNode($_SESSION['basicCoverageInfo']->standardCharges->stamps);
	$stamps->appendChild($text);	
	$fees = $dom->createElement("fees");
	$standardCharges->appendChild($fees);
	//fill text
	$text = $dom->createTextNode($_SESSION['basicCoverageInfo']->standardCharges->fees);
	$fees->appendChild($text);	
	$other = $dom->createElement("other");
	$standardCharges->appendChild($other);
	//fill text
	$text = $dom->createTextNode($_SESSION['basicCoverageInfo']->standardCharges->other);
	$other->appendChild($text);	
	$otherPercent = $dom->createElement("otherPercent");
	$standardCharges->appendChild($otherPercent);
	//fill text
	$text = $dom->createTextNode($_SESSION['basicCoverageInfo']->standardCharges->otherPercent);
	$otherPercent->appendChild($text);
	
	// save xml tree to file
	$dom->save($filePath.$newFileName.".xml");
	
	//if base file name changed, change also all related files for all vehicles
	if($previousFileName != $newFileName )
	{
		//read all vehicle types
		$directories = getListOfDirectories($_SESSION['clientFilesLocation']."/XMLFiles/");
		foreach($directories as $eachDirectory)
		{
			//read all coverage files available. TP, TPFT, Comprehensive
			$allRelatedFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/", $previousFileNameWithoutExtention."_");
			/* read each coveragetype */
			foreach($allRelatedFiles as $eachRelatedFile)
			{
				//get the last part that remains unchanged. i.e for model_package_THIRD_PARTY, we get the _package_THIRD_PARTY
				$previousFileNameLastPart = substr($eachRelatedFile, strlen($previousFileNameWithoutExtention));
				$newLocalFileName = $newFileName.$previousFileNameLastPart;
				
				//echo "renaming $eachRelatedFile to $newLocalFileName <br/>";
				rename($_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/".$eachRelatedFile, $_SESSION['clientFilesLocation']."/XMLFiles/".$eachDirectory."/".$newLocalFileName);
			}
		}
	}
	
	//update current XML file name to the new file name
	$_SESSION['currentInputXMLFileName'] = $newFileName.".xml";
	
	
			
}


/* create new optional coverages file
	$filePath = path to delete/create XML files
	$previousFileName = name of the previous file(if different)
	$newFileName = name of the new file(if different)
	$optionalCoveragesArray = contains all optional coverages
*/
function createOptionalCoveragesXML($filePath, $previousFileName, $newFileName, $optionalCoveragesArray)
{
	//delete old file
	@unlink($filePath.$previousFileName);
	
	// Creates a new document according to 1.0 specs
	$dom = new DOMDocument("1.0");
	$dom->formatOutput = true;
	$dom->preserveWhiteSpace = false;
	
	//PROPERTIES
	$properties = $dom->createElement("properties");//root element
	$dom->appendChild($properties);
	
	$optionalCoverages = $dom->createElement("optionalCoverages");
	$properties->appendChild($optionalCoverages);
	
	foreach($optionalCoveragesArray as $eachCoverage)
	{
		//$eachCoverage->printData();
		createNewTagInXML($eachCoverage, "coverage",  $optionalCoverages, $dom );
	}
			
	// save xml tree to file
	$dom->save($filePath.$newFileName);
}

/* create new additional charges file
	$filePath = path to delete/create XML files
	$previousFileName = name of the previous file(if different)
	$newFileName = name of the new file(if different)
	$additionalChargesArray = contains all optional coverages
*/
function createAdditionalChargesXML($filePath, $previousFileName, $newFileName, $additionalChargesArray)
{
	//delete old file
	@unlink($filePath.$previousFileName);
	
	// Creates a new document according to 1.0 specs
	$dom = new DOMDocument("1.0");
	$dom->formatOutput = true;
	$dom->preserveWhiteSpace = false;
	
	//PROPERTIES
	$properties = $dom->createElement("properties");//root element
	$dom->appendChild($properties);
	
	$additionalCharges = $dom->createElement("additionalCharges");
	$properties->appendChild($additionalCharges);
	
	foreach($additionalChargesArray as $eachCharge)
	{
		//$eachCoverage->printData();
		createNewTagInXML($eachCharge, "charge",  $additionalCharges, $dom );
	}
			
	// save xml tree to file
	$dom->save($filePath.$newFileName);
}

/* create new optional coverages file
	$filePath = path to delete/create XML files
	$previousFileName = name of the previous file(if different)
	$packageClass = contains the <inclusiveBenefit> info
*/
function createPackageXML($filePath, $previousFileName, $packageClass)
{
	//delete old file
	@unlink($filePath.$previousFileName);
	
	// Creates a new document according to 1.0 specs
	$dom = new DOMDocument("1.0");
	$dom->formatOutput = true;
	$dom->preserveWhiteSpace = false;
	
	//PROPERTIES
	$properties = $dom->createElement("properties");//root element
	$dom->appendChild($properties);
	
	$inclusiveBenefits = $dom->createElement("inclusiveBenefits");
	$properties->appendChild($inclusiveBenefits);
	
	// create attribute type node
	$name = $dom->createAttribute("name");
	$inclusiveBenefits->appendChild($name);
	// create attribute value node
	$nameValue = $dom->createTextNode($packageClass->name);
	$name->appendChild($nameValue);
	
	//available
	$available = $dom->createElement("available");
	$inclusiveBenefits->appendChild($available);	
	$typeValue = $dom->createTextNode($packageClass->available);
	$available->appendChild($typeValue);
	
	//coverageTypeOfOffer
	$coverageTypeOfOffer = $dom->createElement("coverageTypeOfOffer");
	$inclusiveBenefits->appendChild($coverageTypeOfOffer);	
	$typeValue = $dom->createTextNode($packageClass->coverageTypeOfOffer);
	$coverageTypeOfOffer->appendChild($typeValue);
	//echo "$currentInclusiveBenefit->coverageTypeOfOffer <br>";
	
	//chargeOverBasic
	$chargeOverBasic = $dom->createElement("chargeOverBasic");
	$inclusiveBenefits->appendChild($chargeOverBasic);	
	$typeValue = $dom->createTextNode($packageClass->chargeOverBasic);
	$chargeOverBasic->appendChild($typeValue);
	
	foreach($packageClass->optionalCoveragesFromXMLArray as $eachCoverage)
	{
		//$eachCoverage->printData();
		createNewTagInXML($eachCoverage, "coverage",  $inclusiveBenefits, $dom );
	}
			
	//set the new package name based on user input
	$_SESSION['packageFileName'] = $_SESSION['xmlFileName']."_package_".$packageClass->coverageTypeOfOffer."_".$packageClass->name.".xml";

	// save xml tree to file	
	$dom->save($filePath.$_SESSION['packageFileName']);
}

/* create new coverage file
	$filePath = path to delete/create XML files
	$previousFileName = name of the previous file(if different)
	$newFileName = name of the new file(if different)
*/
function createCoverageTypeXML($filePath, $previousFileName, $newFileName, $currentCoverageType)
{
	//delete old file
	@unlink($filePath.$previousFileName);
	
	// Creates a new document according to 1.0 specs
	$dom = new DOMDocument("1.0");
	$dom->formatOutput = true;
	$dom->preserveWhiteSpace = false;
	
	//PROPERTIES
	$properties = $dom->createElement("properties");//root element
	$dom->appendChild($properties);
	
	$coverageType = $dom->createElement("coverageType");
	$properties->appendChild($coverageType);
			
	// create attribute type node
	/* 06/09/2012 aristosa removed. is needed?
	$name = $dom->createAttribute("name");
	$coverageType->appendChild($name);
	// create attribute value node
	$typeValue = $dom->createTextNode($currentCoverageType->name);
	$name->appendChild($typeValue);*/
					
	// create attribute type node
	$value = $dom->createAttribute("value");
	$coverageType->appendChild($value);
	// create attribute value node
	$typeValue = $dom->createTextNode($currentCoverageType->value);
	$value->appendChild($typeValue);
	//available
	$available = $dom->createElement("available");
	$coverageType->appendChild($available);	
	$typeValue = $dom->createTextNode($currentCoverageType->available);
	$available->appendChild($typeValue);
	//minimumChargeEuro
	$minimumChargeEuro = $dom->createElement("minimumChargeEuro");
	$coverageType->appendChild($minimumChargeEuro);	
	$typeValue = $dom->createTextNode($currentCoverageType->minimumChargeEuro);
	$minimumChargeEuro->appendChild($typeValue);
	//excessEuro
	$excessEuro = $dom->createElement("excessEuro");
	$coverageType->appendChild($excessEuro);	
	$typeValue = $dom->createTextNode($currentCoverageType->excessEuro);
	$excessEuro->appendChild($typeValue);
	
	foreach($currentCoverageType->coverages as $eachCoverage)
	{
		//$eachCoverage->printData();
		createNewTagInXML($eachCoverage, "coverage",  $coverageType, $dom );
	}
			
	//discounts
	//if(property_exists($currentCoverageType, 'discounts'))
	//{
		if(count($currentCoverageType->discounts->coverages)>0)
		{
			if(is_object($currentCoverageType->discounts))
			{
				//discounts
				$discounts = $dom->createElement("discounts");
				$coverageType->appendChild($discounts);	
				//available
				$available = $dom->createElement("available");
				$discounts->appendChild($available);	
				$typeValue = $dom->createTextNode($currentCoverageType->discounts->available);
				$available->appendChild($typeValue);
				
				foreach($currentCoverageType->discounts->coverages as $eachCoverage)
				{
					//$eachCoverage->printData();
					createNewTagInXML($eachCoverage, "coverageDiscount",  $discounts, $dom );
				}
			}
		}
	//}
	
	// save xml tree to file
	$dom->save($filePath.$newFileName);
}
			
			

/* create all elements inside the additionalCharges tag
parentObject = php object that contains all the additionalCharges, f.e $currentVehicleType
parentXmlTag = parent xml tag in dom file where to create the "additionalCharges" tag. f.e <vehicleType> Inside this will create all additional coverages.
$dom = document to create the new tags */
function createAdditionalChargesTags($parentObject, $parentXmlTag, $dom)
{
	//additionalCharges
	$additionalCharges = $dom->createElement("additionalCharges");
	$parentXmlTag->appendChild($additionalCharges);	
	
	foreach($parentObject->additionalChargesFromXMLArray as $eachCharge)
	{
		//$eachCharge->printData();
		createNewTagInXML($eachCharge, "charge",  $additionalCharges, $dom );
	}
}

/* create all elements inside the optionalCoverages tag
parentObject = php object that contains all the optional coverages, f.e $currentVehicleType
parentXmlTag = parent xml tag in XML file where to create the "optionalCoverages" tag. Inside this will create all optional coverages.
$optionalCoveragesTagName = name of coverages tag. Can be 'inclusiveCoverages' or 'optionalCoverages' to avoid probles of same name
$dom = document to create the new tags */
//createOptionalCoveragesTags($currentVehicleType->optionalCoveragesFromXML, $vehicleType, $optionalCoveragesTagName, $dom);
function createOptionalCoveragesTags($parentObject, $parentXmlTag, $optionalCoveragesTagName, $dom)
{
	//optionalcoverages
	$optionalCoverages = $dom->createElement($optionalCoveragesTagName);
	$parentXmlTag->appendChild($optionalCoverages);	
	
	foreach($parentObject->optionalCoveragesFromXMLArray as $eachCoverage)
	{
		//$eachCoverage->printData();
		createNewTagInXML($eachCoverage, "coverage",  $optionalCoverages, $dom );
	}
}
	
/* create all the categories tags and values. Also create the parent of the categories.
f.e create "additionalDrivers" and inside create "categories" tags.
parentObject = php object that contains the 'categories'
newXmlTagName = new xml tag to be created inside parentXmlTag
parentXmlTag = parent xml tag in dom file where to create the categories
$dom = document to create the new tags */
function createNewTagInXML($parentObject, $newXmlTagName, $parentXmlTag, $dom)
{
	writeToFile(date("Y-m-d H:i:s"). ": createNewTagInXML() ENTER . newTagName=$newXmlTagName \n", LOG_LEVEL_INFO);
        
	//namedDrivers
	$newXmlTag = $dom->createElement($newXmlTagName);
	$parentXmlTag->appendChild($newXmlTag);
	
	//create applicable only if exists
	if($parentObject->applicable <> null )
	{
		$applicable = $dom->createAttribute("applicable");
		$newXmlTag->appendChild($applicable);
		$typeValue = $dom->createTextNode($parentObject->applicable);
		$applicable->appendChild($typeValue);
	}
	
	//create allowed only if exists
	//if(property_exists($parentObject, 'allowed' ))
	if(property_exists($parentObject, 'allowed' ) && $parentObject->allowed <> null )
	{
		$allowed = $dom->createAttribute("allowed");
		$newXmlTag->appendChild($allowed);
		$typeValue = $dom->createTextNode($parentObject->allowed);
		$allowed->appendChild($typeValue);
	}
	
	if(property_exists($parentObject, 'code'))
	{
		$code = $dom->createElement("code");
		$newXmlTag->appendChild($code);
		$typeValue = $dom->createTextNode($parentObject->code);
		$code->appendChild($typeValue);
	}
	
	if(property_exists($parentObject, 'euro'))
	{
		$euro = $dom->createElement("euro");
		$newXmlTag->appendChild($euro);
		$typeValue = $dom->createTextNode($parentObject->euro);
		$euro->appendChild($typeValue);
	}
	
	if(property_exists($parentObject, 'percentCharge'))
	{
		$percentCharge = $dom->createElement("percentCharge");
		$newXmlTag->appendChild($percentCharge);
		$typeValue = $dom->createTextNode($parentObject->percentCharge);
		$percentCharge->appendChild($typeValue);
	}
	
	if(property_exists($parentObject, 'param1'))
	{
		$param1 = $dom->createElement("param1");
		$newXmlTag->appendChild($param1);
		$typeValue = $dom->createTextNode($parentObject->param1);
		$param1->appendChild($typeValue);
	}
	
	if(property_exists($parentObject, 'param2'))
	{
		$param2 = $dom->createElement("param2");
		$newXmlTag->appendChild($param2);
		$typeValue = $dom->createTextNode($parentObject->param2);
		$param2->appendChild($typeValue);
	}
	
	if(property_exists($parentObject, 'param3'))
	{
		$param3 = $dom->createElement("param3");
		$newXmlTag->appendChild($param3);
		$typeValue = $dom->createTextNode($parentObject->param3);
		$param3->appendChild($typeValue);
	}
	
	$unit = $dom->createElement("unit");
	$newXmlTag->appendChild($unit);
	$typeValue = $dom->createTextNode($parentObject->unit);
	$unit->appendChild($typeValue);
	
	if(property_exists($parentObject, 'categories'))
	{
		//TODO - CREATE GENERIC FUNCTION FOR THIS
		foreach($parentObject->categories as $currentCategory)
		{
			//category
			$category = $dom->createElement("category");
			$newXmlTag->appendChild($category);	
			
			$allowed = $dom->createAttribute("allowed");
			$category->appendChild($allowed);
			$typeValue = $dom->createTextNode($currentCategory->allowed);
			$allowed->appendChild($typeValue);
			
			//from
			$from = $dom->createElement("from");
			$category->appendChild($from);	
			$typeValue = $dom->createTextNode($currentCategory->from);
			$from->appendChild($typeValue);
			//to
			$to = $dom->createElement("to");
			$category->appendChild($to);	
			$typeValue = $dom->createTextNode($currentCategory->to);
			$to->appendChild($typeValue);
			//euro
			$euro = $dom->createElement("euro");
			$category->appendChild($euro);	
			$typeValue = $dom->createTextNode($currentCategory->euro);
			$euro->appendChild($typeValue);
			//percentCharge
			$percentCharge = $dom->createElement("percentCharge");
			$category->appendChild($percentCharge);	
			$typeValue = $dom->createTextNode($currentCategory->percentCharge);
			$percentCharge->appendChild($typeValue);
			//minimumEuro
			$minimumEuro = $dom->createElement("minimumEuro");
			$category->appendChild($minimumEuro);	
			$typeValue = $dom->createTextNode($currentCategory->minimumEuro);
			$minimumEuro->appendChild($typeValue);
			//percentChargeComprehensive
			$percentChargeComprehensive = $dom->createElement("percentChargeComprehensive");
			$category->appendChild($percentChargeComprehensive);	
			$typeValue = $dom->createTextNode($currentCategory->percentChargeComprehensive);
			$percentChargeComprehensive->appendChild($typeValue);
			
		}
	}
	
	
	if(property_exists($parentObject, 'typeCategories'))
	{
		//TODO - CREATE GENERIC FUNCTION FOR THIS
		foreach($parentObject->typeCategories as $currentCategory)
		{
			//$currentCategory->printData();
			//category
			$category = $dom->createElement("typeCategory");
			$newXmlTag->appendChild($category);	
			
			//allowed only if exists
			$allowed = $dom->createAttribute("allowed");
			$category->appendChild($allowed);
			$typeValue = $dom->createTextNode($currentCategory->allowed);
			$allowed->appendChild($typeValue);
			
			//type
			$type = $dom->createElement("type");
			$category->appendChild($type);	
			$typeValue = $dom->createTextNode($currentCategory->type);
			$type->appendChild($typeValue);
			//euro
			$euro = $dom->createElement("euro");
			$category->appendChild($euro);	
			$typeValue = $dom->createTextNode($currentCategory->euro);
			$euro->appendChild($typeValue);
			//percentCharge
			$percentCharge = $dom->createElement("percentCharge");
			$category->appendChild($percentCharge);	
			$typeValue = $dom->createTextNode($currentCategory->percentCharge);
			$percentCharge->appendChild($typeValue);
			//minimumEuro
			$minimumEuro = $dom->createElement("minimumEuro");
			$category->appendChild($minimumEuro);	
			$typeValue = $dom->createTextNode($currentCategory->minimumEuro);
			$minimumEuro->appendChild($typeValue);
			//percentChargeComprehensive
			$percentChargeComprehensive = $dom->createElement("percentChargeComprehensive");
			$category->appendChild($percentChargeComprehensive);	
			$typeValue = $dom->createTextNode($currentCategory->percentChargeComprehensive);
			$percentChargeComprehensive->appendChild($typeValue);
			
		}
	}

	writeToFile(date("Y-m-d H:i:s"). ": createNewTagInXML() EXIT \n", LOG_LEVEL_INFO);
}


/* validate the coverageTypeForm to create a new coverageType table
 */
function saveCoverageTypeFile($fileName)
{
	writeToFile(date("Y-m-d H:i:s"). ": saveCoverageTypeFile() ENTER \n", LOG_LEVEL_INFO);
        
	//delete old file	
	//unlink($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/".$fileName);
	
	$coverageTypeClass = new coverageType();
	
	//get the <name> attribute from the fileName
	
	$tempArray = explode(".", substr($fileName, strlen($_SESSION['xmlFileName']."_coverageType_")));
	$coverageTypeClass->value = $tempArray[0];
	
	//$_SESSION['coverageTypeName'] = $coverageType->name;
		
		//$availableName = 'available_'.$i.'_'.$j;
		//$_SESSION[$availableName] = $_POST[$availableName];
		$coverageTypeClass->available = $_POST['available'];
		
		//$minimumChargeEuroName = "minimumChargeEuro_".$i."_".$j;
		//$_SESSION[$minimumChargeEuroName] = $_POST[$minimumChargeEuroName];
		$coverageTypeClass->minimumChargeEuro = $_POST['minimumChargeEuro'];
		
		//echo $_SESSION[$minimumChargeEuroName]."<br>";
		//$excessEuroName = "excessEuro_".$i."_".$j;
		$coverageTypeClass->excessEuro = $_POST['excessEuro'];
		
		
		$coverageTypeClass->coverages = array();
		
		$coverageTypeClass->coverages = readTagParameters("CoverageTypeCoverages");
		
		/*echo "validation found coverages:".count($coverageType->coverages);
		foreach($coverageType->coverages as $eachCoverage)
		{
			$eachCoverage->printData();
		}*/
		
	
		//DISCOUNT TAG
		//if(property_exists($coverageTypeClass, 'discounts'))
		//{
			//if(is_object($coverageTypeClass->discounts))
			//{
				$coverageTypeClass->discounts = new discounts();
				$coverageTypeClass->discounts->coverages = readTagParameters("CoverageTypeDiscountsCoverages");
			//}
		//}
		
	//create new/updated file
	createCoverageTypeXML($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $fileName, $fileName, $coverageTypeClass ); 

	writeToFile(date("Y-m-d H:i:s"). ": saveCoverageTypeFile() EXIT \n", LOG_LEVEL_INFO);	
}


/* validate the optionalCoveragesForm to create a new optionalCoverages file 
 */
function saveOptionalCoveragesFile($fileName)
{
	writeToFile(date("Y-m-d H:i:s"). ": saveOptionalCoveragesFile() ENTER \n", LOG_LEVEL_INFO);
        
	//delete old file	
	//unlink($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/".$fileName);
	
	$optionalCoveragesArray = readTagParameters("OptionalCoverages");
	
	//foreach($optionalCoveragesArray as $eachCoverage)
	//	$eachCoverage->printData();
		
	//create new/updated file
	createOptionalCoveragesXML($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $fileName, $fileName, $optionalCoveragesArray ); 

	writeToFile(date("Y-m-d H:i:s"). ": saveOptionalCoveragesFile() EXIT \n", LOG_LEVEL_INFO);	
}


/* validate the additionalChargesForm to create a new additionalCharges file 
 */
function saveAdditionalChargesFile($fileName)
{
	writeToFile(date("Y-m-d H:i:s"). ": saveAdditionalChargesFile() ENTER \n", LOG_LEVEL_INFO);
        
	$additionalChargesArray = readTagParameters("AdditionalCharges");
	
	//foreach($optionalCoveragesArray as $eachCoverage)
	//	$eachCoverage->printData();
		
	//create new/updated file
	createAdditionalChargesXML($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $fileName, $fileName, $additionalChargesArray ); 

	writeToFile(date("Y-m-d H:i:s"). ": saveAdditionalChargesFile() EXIT \n", LOG_LEVEL_INFO);	
}


/* save package file */
function savePackageFile($previousFileName)
{
	writeToFile(date("Y-m-d H:i:s"). ": savePackageFile() ENTER \n", LOG_LEVEL_INFO);
        
	//delete old file	
	//unlink($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/".$fileName);
	$packageFileClass = new inclusiveBenefit();
	
	//available
	$packageFileClass->available = $_POST['inclusiveBenefitAvailableName'];
		
	//coverageTypeOfOfferName
	$packageFileClass->coverageTypeOfOffer = $_POST['inclusiveBenefitCoverageTypeOfOfferName'];
	
	//name
	$packageFileClass->name = $_POST['inclusiveBenefitName'];
		
	//chargeoverbasic
	$packageFileClass->chargeOverBasic = $_POST['chargeOverBasicName'];
	
	$fileName = $_SESSION['xmlFileName']."_package_".$packageFileClass->coverageTypeOfOffer."_".$packageFileClass->name.".xml";
	
	$packageFileClass->optionalCoveragesFromXMLArray = readTagParameters("OptionalCoverages");
			
	//foreach($optionalCoveragesArray as $eachCoverage)
	//	$eachCoverage->printData();
		
	//create new/updated file
	createPackageXML($_SESSION['clientFilesLocation']."/XMLFiles/".$_SESSION['vehicleType']."/", $previousFileName, $packageFileClass ); 

        writeToFile(date("Y-m-d H:i:s"). ": savePackageFile() EXIT \n", LOG_LEVEL_INFO);
		
}

/*
	Read all tag parameters from form POST. Also set outetag values, like applicable, learners included etc.
	$outertag : tag that contains the categories, f.e $coverageType->vehicleCubicCentimeters
	$tagNamePrefix : specific tag name, used as prefix for variable names, f.e "ageCharges"
*/
function readTagParameters($tagNamePrefix)
{
	$invalidNumericField = 'false'; //set to true when a numeric field is not valid. f.e '1.2.2.2'

	writeToFile(date("Y-m-d H:i:s"). ": readTagParameters() ENTER . tagNamePrefix=$tagNamePrefix \n", LOG_LEVEL_INFO);
        
	//$outertag->printData();
	
	$coverages = array();
	
	//$k = 0;//category repetition
	$tagCodeName = $tagNamePrefix."_CodeInc";
	$tagCodeNames = @$_POST[$tagCodeName];
	
	//print_r($tagCodeNames);

        writeToFile(date("Y-m-d H:i:s"). ": found tag codes:".count($tagCodeNames)." for tagNamePrefix=$tagNamePrefix, tagCodeName=$tagCodeName \n", LOG_LEVEL_INFO);
	
	$tagApplicableName = $tagNamePrefix."_ApplicableInc";
	//if (isset($_POST[$tagApplicableName])) 
	//	$outertag->applicable = $_POST[$tagApplicableName];
	$tagApplicableNames = @$_POST[$tagApplicableName];
	
	writeToFile(date("Y-m-d H:i:s"). ": found tag codes:".count($tagCodeNames).", applicable:".count($tagApplicableNames)." for tagNamePrefix=$tagNamePrefix, tagCodeName=$tagCodeName \n", LOG_LEVEL_INFO);
        
	$tagAllowedName = $tagNamePrefix."_AllowedInc";
	$tagAllowedNames = @$_POST[$tagAllowedName];
		
	$tagEuroName = $tagNamePrefix."_EuroInc";
	$tagEuroNames = @$_POST[$tagEuroName];
		
	$tagPercentChargeName = $tagNamePrefix."_PercentChargeInc";
	$tagPercentChargeNames = @$_POST[$tagPercentChargeName];
		
	$tagParam1Name = $tagNamePrefix."_Param1Inc";
	$tagParam1Names = @$_POST[$tagParam1Name];
	
	$tagParam2Name = $tagNamePrefix."_Param2Inc";
	$tagParam2Names = @$_POST[$tagParam2Name];
	
	$tagParam3Name = $tagNamePrefix."_Param3Inc";
	$tagParam3Names = @$_POST[$tagParam3Name];
	
	//print_r($tagParam1Names);
	//print_r($tagParam2Names);
	//print_r($tagParam3Names);
	
	$tagUnitName = $tagNamePrefix."_UnitInc";
	$tagUnitNames = @$_POST[$tagUnitName];
	
	$tagTypeName = $tagNamePrefix."_Type";
	$tagTypeNames = @$_POST[$tagTypeName];
	
	//read arrays
	if(isset($tagCodeNames))
	{
		//set all array elements
		foreach($tagCodeNames as $c => $d)
		{
			$localCoverage = new coveragesFromCompanyXML();
			$localCoverage->code = $tagCodeNames[$c];
			$localCoverage->applicable = @$tagApplicableNames[$c];
			$localCoverage->allowed = @$tagAllowedNames[$c];
			$localCoverage->euro = @$tagEuroNames[$c];
			$localCoverage->percentCharge = @$tagPercentChargeNames[$c];
			$localCoverage->param1 = @$tagParam1Names[$c];
			$localCoverage->param2 = @$tagParam2Names[$c];
			$localCoverage->param3 = @$tagParam3Names[$c];
			$localCoverage->unit = @$tagUnitNames[$c];
			
			//READ CATEGORIES fields that come as arrays 
			$froms = @$_POST[$localCoverage->code."_".$tagNamePrefix."From"];
			$tos = @$_POST[$localCoverage->code."_".$tagNamePrefix."To"];
			$euros = @$_POST[$localCoverage->code."_".$tagNamePrefix."Euro"];
			$percentCharges = @$_POST[$localCoverage->code."_".$tagNamePrefix."PercentCharge"];
			$percentChargeComprehensives = @$_POST[$localCoverage->code."_".$tagNamePrefix."PercentChargeComprehensive"];
			$alloweds = @$_POST[$localCoverage->code."_".$tagNamePrefix."Allowed"];
			$minimumEuros = @$_POST[$localCoverage->code."_".$tagNamePrefix."MinimumEuro"];
			$types = @$_POST[$localCoverage->code."_".$tagNamePrefix."Type"];
			
			//print_r($tos);
			
			if(isset($alloweds))
			{
				//set all array elements
				foreach($alloweds as $a => $b)
				{
					
					$category = new genericCategory();
	
					/* if variable is not set, we just want to stop processing the numeric values.
					   we said the variable to true so that it will not compare to and from values afterwards
					   This is not an error case. Some categories don't have 'To' and 'From' fields. */
					if (isset($froms[$a])) 
						$category->from = $froms[$a];//$_POST[$tagNamePrefix."From_".$i."_".$j."_".$k];
					
					/* if variable is not set, we just want to stop processing the numeric values.
					   we said the variable to true so that it will not compare to and from values afterwards
					   This is not an error case. Some categories don't have 'To' and 'From' fields. */
					if (isset($tos[$a])) 
						$category->to = $tos[$a];//$_POST[$tagNamePrefix."To_".$i."_".$j."_".$k];
					
					//euro				
					if(isset($euros[$a]))
						$category->euro = $euros[$a];//$_POST[$tagNamePrefix."Euro_".$i."_".$j."_".$k];
					
					//percentCharge
					if(isset($percentCharges[$a]))
						$category->percentCharge = $percentCharges[$a];//$_POST[$tagNamePrefix."PercentCharge_".$i."_".$j."_".$k];
					
					/* if variable is set, then we check if it's numeric */
					if (isset($percentChargeComprehensives[$a]))
						$category->percentChargeComprehensive = $percentChargeComprehensives[$a];//$_POST[$tagNamePrefix."PercentChargeComprehensive_".$i."_".$j."_".$k];
					
					/* if variable is set, then we check if it's numeric */
					if (isset($minimumEuros[$a])) 
						$category->minimumEuro = $minimumEuros[$a];//$_POST[$tagNamePrefix."MinimumEuro_".$i."_".$j."_".$k];
					
						
					if (isset($alloweds[$a])) 
						$category->allowed = $alloweds[$a];//$_POST[$tagNamePrefix."Allowed_".$i."_".$j."_".$k];
					
					//TODO TODO set also $category->type 
					if (isset($types[$a])) 
						$category->type = $types[$a];
					
						
					//$category->printData();
					
					$localCoverage->categories[] = $category;
					
					//$k = $k + 1;
				}
			}
			
			//$k = 0;//category repetition 
			
			//READ TYPECATEGORIES fields that come as arrays 
			$types = @$_POST[$localCoverage->code."_typeCategories_".$tagNamePrefix."Type"];
			$euros = @$_POST[$localCoverage->code."_typeCategories_".$tagNamePrefix."Euro"];
			$percentCharges = @$_POST[$localCoverage->code."_typeCategories_".$tagNamePrefix."PercentCharge"];
			$percentChargeComprehensives = @$_POST[$localCoverage->code."_typeCategories_".$tagNamePrefix."PercentChargeComprehensive"];
			$alloweds = @$_POST[$localCoverage->code."_typeCategories_".$tagNamePrefix."Allowed"];
			$minimumEuros = @$_POST[$localCoverage->code."_typeCategories_".$tagNamePrefix."MinimumEuro"];
			
			//print_r($tos);
			
			if(isset($types))
			{
				//set all array elements
				foreach($types as $a => $b)
				{
					
					$category = new typeCategory();
	
					//category type
					if (isset($types[$a])) 
						$category->type = $types[$a];
						
					//euro				
					if(isset($euros[$a]))
						$category->euro = $euros[$a];
					
					//percentCharge
					if(isset($percentCharges[$a]))
						$category->percentCharge = $percentCharges[$a];
					
					/* if variable is set, then we check if it's numeric */
					if (isset($percentChargeComprehensives[$a]))
						$category->percentChargeComprehensive = $percentChargeComprehensives[$a];
					
					/* if variable is set, then we check if it's numeric */
					if (isset($minimumEuros[$a])) 
						$category->minimumEuro = $minimumEuros[$a];
					
						
					if (isset($alloweds[$a])) 
						$category->allowed = $alloweds[$a];
					
						
					//$category->printData();
					
					$localCoverage->typeCategories[] = $category;
					
					//$k = $k + 1;
				}
			}
			
			
			$coverages[] = $localCoverage;
			
		}//foreach($tagCodeNames as $c => $d)
		
	}//if(isset($tagCodeNames))

	writeToFile(date("Y-m-d H:i:s"). ": readTagParameters() EXIT \n", LOG_LEVEL_INFO);
        
	return $coverages;
	
}

?>