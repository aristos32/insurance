<?php

//SHOW ALL PREVIOUS QUOTATIONS FOR THIS USERNAME

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');


$currentUsername = $_SESSION['searchUserName'];
//echo "current user name=".$currentUsername;

//save in session
$_SESSION['currentUsername'] = $currentUsername;

$previousQuotes = retrieveAllPreviousUserQuotes($currentUsername);
if(empty($previousQuotes))
{
	?>
	<?php echo $_SESSION['noQuotesFound']; ?>
	<?php	
}
else
{
	?>
<h2><?php echo $_SESSION['username']."=".$currentUsername;?> </h2>	

<table width="90%" border="0" cellspacing="1" cellpadding="3">
	<thead>
	<tr>

		<th>ID</th>
		<th><?php echo $_SESSION['canProvideOnline']; ?></th>
		<th><?php echo $_SESSION['entryDate'];?></th>
		<th><?php echo $_SESSION['quotationCost']; ?>(€)</th>
		<th><?php echo $_SESSION['view']; ?></th>
		<?php
		if(isset($_SESSION['role']) and $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
		{
			?>
			<th><?php echo $_SESSION['delete']; ?></th>
			<?php
		}
		?>
	</tr>
	</thead>
	<?php
	$i = 0;
	foreach($previousQuotes as $quote)
	{
		?>					
	<tr>
		<td ><?php echo $quote->quoteId;?></td><td ><?php echo $quote->canProvideOnlineQuote;?></td><td><?php echo $quote->entryDate;?></td>
		<td ><?php if(strcmp($quote->canProvideOnlineQuote,$NO_CONSTANT)==0)
							  {
									echo "N/A";
							  }
							  else
							  	echo $quote->quoteAmount;?>
		</td>
		<td>
			<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
			<form action="./quotation.php" id="<?php echo $quote->quoteId;?>" method="post" style="display: none;">
				<input type="text" name="action" value="quotePresentation" />
				<input type="text" name="quoteId" value=<?php echo $quote->quoteId;?> />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $quote->quoteId;?>').submit()"><?php echo $_SESSION['view'];?></a>
		</td>
		<?php
		if(isset($_SESSION['role']) and $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
		{
			?>
			<td>
				<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
				<form action="./quotation.php" id="<?php echo "Delete_".$quote->quoteId;?>" method="post" style="display: none;">
					<input type="text" name="action" value="deleteQuote" />
					<input type="text" name="quoteId" value=<?php echo $quote->quoteId;?> />
				</form>
				<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo "Delete_".$quote->quoteId;?>').submit()"><?php echo $_SESSION['delete']; ?></a>
			</td>
			<?php
		}
		?>
	</tr>
	
	<?php
	$i = $i + 1;
	}
	//1. administrator can delete all quotes for any user
	//2. user can delete all his own quotes
	if(isset($_SESSION['role']) and ($_SESSION['role']>=$USER_ROLE_ADMINISTRATOR ||($_SESSION['currentUsername']==$_SESSION['username'])))
	{
		?>
		<tr>
			<td>
				<!-- create hidden forn, to send the action as a POST -->
				<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="delAllQuo" method="post" style="display: none;">
				<input type="text" name="action" value="deleteAllQuotes" />
				</form>
				<a href="javascript:;" onclick="javascript: if(confirm('<?php echo $_SESSION['areYouSureYouWantToDeleteAllQuotes'];?>')) document.getElementById('delAllQuo').submit()"><?php echo $_SESSION['deleteAll']; ?></a>
		</tr>
		<?php
	}
	?>
	
</table>

<?php
}
?>
