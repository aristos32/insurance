<?php

session_start();

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//we have independent file load here.
//we need to move one level down
$globalFilesLocation = "..";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/database/connect.php");
require_once($globalFilesLocation."/database/retrieveDatalayers.php");

$quoteId = $_GET['quoteId'];

$quote = retrieveQuoteInfo('q.quoteId', $quoteId);
$quote = $quote[0];

// Identify as XML
header('Content-type: text/xml');
header ('Cache-Control: no-cache');
header ('Cache-Control: no-store' , false);
// Creates a new document according to 1.0 specs
$dom = new DOMDocument("1.0", "UTF-8");
$dom->formatOutput = true;
$dom->preserveWhiteSpace = false;
$properties = $dom->createElement("properties");//root element
$dom->appendChild($properties);
	
$offerSelected = $dom->createElement("offerSelected");
$properties->appendChild($offerSelected);
$text = $dom->createTextNode($quote->offerSelected);
$offerSelected->appendChild($text);	

$quoteId = $dom->createElement("quoteId");
$properties->appendChild($quoteId);
$text = $dom->createTextNode($quote->quoteId);
$quoteId->appendChild($text);	


//QUOTE FOUND - NOT DELETED
if($quote->quoteId != 0 )
{
	//Step 1 - retrieve owner state id
	$stateId = retrieveStateIdFromQuotationOwner($quote->quoteId);
	//Step 2 - retrieve owner
	$owner = retrieveOwner($stateId);
	
	//Step 1 - retrieve owner state id
	$stateId = retrieveStateIdFromQuotationOwner($quote->quoteId);
	//Step 2 - retrieve owner license
	$license = retrieveLicense($stateId);
	
	//Step 1 - retrieve vehicle registration
	$regNumber = retrieveRegNumberFromQuotationVehicle($quote->quoteId);
	//Step 2 - retrieve vehicle info
	$allVehicles = retrieveVehicleInfo("regNumber",$regNumber);
	foreach($allVehicles as $eachVehicle)
		$vehicle = $eachVehicle;
	
	$additionalCharges = retrieveAdditionalCharges($quote->quoteId);
	
	$firstName = $dom->createElement("firstName");
	$properties->appendChild($firstName);
	$text = $dom->createTextNode($owner->firstName);
	$firstName->appendChild($text);	
	
	$lastName = $dom->createElement("lastName");
	$properties->appendChild($lastName);
	$text = $dom->createTextNode($owner->lastName);
	$lastName->appendChild($text);	
	
	$birthDate = $dom->createElement("birthDate");
	$properties->appendChild($birthDate);
	$text = $dom->createTextNode($owner->birthDate);
	$birthDate->appendChild($text);
	
	$countryOfBirth = $dom->createElement("countryOfBirth");
	$properties->appendChild($countryOfBirth);
	$text = $dom->createTextNode($owner->countryOfBirth);
	$countryOfBirth->appendChild($text);
	
	$profession = $dom->createElement("profession");
	$properties->appendChild($profession);
	$text = $dom->createTextNode($owner->profession);
	$profession->appendChild($text);
	
	$licenseDate = $dom->createElement("licenseDate");
	$properties->appendChild($licenseDate);
	$text = $dom->createTextNode($license->licenseDate);
	$licenseDate->appendChild($text);
	
	$licenseType = $dom->createElement("licenseType");
	$properties->appendChild($licenseType);
	$text = $dom->createTextNode($license->licenseType);
	$licenseType->appendChild($text);
	
	$licenseCountry = $dom->createElement("licenseCountry");
	$properties->appendChild($licenseCountry);
	$text = $dom->createTextNode($license->licenseCountry);
	$licenseCountry->appendChild($text);
	
	$hasPreviousInsurance = $dom->createElement("hasPreviousInsurance");
	$properties->appendChild($hasPreviousInsurance);
	$text = $dom->createTextNode($owner->hasPreviousInsurance);
	$hasPreviousInsurance->appendChild($text);
	
	$insuranceCompany = $dom->createElement("insuranceCompany");
	$properties->appendChild($insuranceCompany);
	$text = $dom->createTextNode($owner->insuranceCompany);
	$insuranceCompany->appendChild($text);
	
	$vehicleType = $dom->createElement("vehicleType");
	$properties->appendChild($vehicleType);
	$text = $dom->createTextNode($vehicle->vehicleType);
	$vehicleType->appendChild($text);
	
	$make = $dom->createElement("make");
	$properties->appendChild($make);
	$text = $dom->createTextNode($vehicle->make);
	$make->appendChild($text);
	
	$model = $dom->createElement("model");
	$properties->appendChild($model);
	$text = $dom->createTextNode($vehicle->model);
	$model->appendChild($text);
	
	$cubicCapacity = $dom->createElement("cubicCapacity");
	$properties->appendChild($cubicCapacity);
	$text = $dom->createTextNode($vehicle->cubicCapacity);
	$cubicCapacity->appendChild($text);
	
	$manufacturedYear = $dom->createElement("manufacturedYear");
	$properties->appendChild($manufacturedYear);
	$text = $dom->createTextNode($vehicle->manufacturedYear);
	$manufacturedYear->appendChild($text);
	
	$steeringWheelSide = $dom->createElement("steeringWheelSide");
	$properties->appendChild($steeringWheelSide);
	$text = $dom->createTextNode($vehicle->steeringWheelSide);
	$steeringWheelSide->appendChild($text);
	
	$vehicleDesign = $dom->createElement("vehicleDesign");
	$properties->appendChild($vehicleDesign);
	$text = $dom->createTextNode($vehicle->vehicleDesign);
	$vehicleDesign->appendChild($text);
	
	$isSportsModel = $dom->createElement("isSportsModel");
	$properties->appendChild($isSportsModel);
	$text = $dom->createTextNode($additionalCharges->isSportsModel);
	$isSportsModel->appendChild($text);
	
	$isTaxFree = $dom->createElement("isTaxFree");
	$properties->appendChild($isTaxFree);
	$text = $dom->createTextNode($vehicle->isTaxFree);
	$isTaxFree->appendChild($text);
	
	$isUsedForDeliveries = $dom->createElement("isUsedForDeliveries");
	$properties->appendChild($isUsedForDeliveries);
	$text = $dom->createTextNode($vehicle->isUsedForDeliveries);
	$isUsedForDeliveries->appendChild($text);
	
	$coverageType = $dom->createElement("coverageType");
	$properties->appendChild($coverageType);
	$text = $dom->createTextNode($quote->coverageType);
	$coverageType->appendChild($text);
	
	//echo $xml - TODO REMOVE 
	echo $dom->saveXML();
}


?>