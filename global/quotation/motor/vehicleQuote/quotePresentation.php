<?php
	
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
		
global $YES_CONSTANT, $NO_CONSTANT, $CANNOT_ADD, $COVERAGE_TYPE_COMPREHENSIVE, $LICENSE_COUNTRY_INTERNATIONAL_LICENSE;
global $USER_ROLE_ADMINISTRATOR;

//retrieve the quoteID
$currentQuoteId;
//called using 'View' in displayPreviousQuotes.php
if (!empty($_POST['quoteId'])) {
	$currentQuoteId = $_POST['quoteId'];
	$_SESSION['quoteId'] = $currentQuoteId;//update session
	//echo "inside if quoteid = $currentQuoteId <br>";
}
//called from carQuoteFormProcess.php or globalFunctions.php
//called when language is changing
else
{
	$currentQuoteId = $_SESSION['quoteId'];
	//echo "inside else quoteid = $currentQuoteId <br>";
}



//retrieve all needed quote info

$quotations = retrieveQuoteInfo('q.quoteId', $currentQuoteId);
	
//QUOTE FOUND - NOT DELETED
if(count($quotations) > 0 )
//if($quote->quoteId != 0 )
{
	//Step 1 - retrieve vehicle registration
	$regNumber = retrieveRegNumberFromQuotationVehicle($quotations[0]->quoteId);
	//Step 2 - retrieve vehicle info
	$allVehicles = retrieveVehicleInfo("regNumber",$regNumber);
	foreach($allVehicles as $eachVehicle)
		$vehicle = $eachVehicle;
	
	$additionalCharges = retrieveAdditionalCharges($quotations[0]->quoteId);
	$optionalCoverages = retrieveOptionalCoverages($quotations[0]->quoteId);
	
	//Step 1 - retrieve owner state id
	$stateId = retrieveStateIdFromQuotationOwner($quotations[0]->quoteId);
	//Step 2 - retrieve owner license
	$license = retrieveLicense($stateId);

	$claims = retrieveClaims("quoteId", $quotations[0]->quoteId);
	
	//Step 2 -retrieve owner
	$owner = retrieveOwner($stateId);
	
	$drivingExperience = retrieveDrivingExperience($stateId);
	
	$discounts = retrieveDiscounts($quotations[0]->quoteId);
	/*foreach($discounts as $eachDiscount)
	{
		$eachDiscount->printData();
	}*/
	
	$user;
	$users = retrieveUsersInfo("%".$quotations[0]->userName."%");
	$_SESSION['searchUserName'] = $quotations[0]->userName;
					
	foreach($users as $currentUser)
	{
		/* many users can be returned, since we have LIKE in the SQL query.
		Select the user that matches exactly the username.
		Compare case-insensitive */
		if(strcasecmp( $quotations[0]->userName, $currentUser->username )==0)
		{
			$user = $currentUser;
			break;
		}
	}
	
	//the quote doesn't belong to a system user
	if(!isset($user))
	{
		$user = new user();
		$user->firstName = 'unknown';
		$user->lastName = '';
		
	}
	
	//CASES:
	// 1. CANNOT PROVIDE ONLINE QUOTE AND USER IS ADMIN
	// 2. CAN PROVIDE ONLINE QUOTE
	// WE DON'T SHOW THIS INFORMATION TO SIMPLE USERS when quote cannot be provided online
	//if(($quotations[0]->canProvideOnlineQuote==$YES_CONSTANT) or
	//(($quotations[0]->canProvideOnlineQuote==$NO_CONSTANT) and (isset($_SESSION['role']) and $_SESSION['role']==$USER_ROLE_ADMINISTRATOR)))
	if($quotations[0]->canProvideOnlineQuote==$YES_CONSTANT)
	{
		?>
		
		 <h2><?php echo $_SESSION['quotationCost']; ?>:<?php echo $EURO.$quotations[0]->quoteAmount; ?></h2>
		
		<div id="dataPresentationInTables">
		<table width="100%" border="0" cellspacing="1" cellpadding="3">
								
			<!-- QUOTE -->
			<tr>
				<!-- this sets the width of the first column -->
				<td colspan="4"><h3><?php echo $_SESSION['quotationInfo']; ?></h3></td>
			</tr>
		
			<tr>
				<td class="label">ID:</td><td class="info"><?php echo $quotations[0]->quoteId;?></td>
			
				<td class="label"><?php echo $_SESSION['usernameTab']; ?>:</td>
				<td class="info"><?php echo $quotations[0]->userName;?></td>
			</tr>
			
  			<tr>
				<td class="label"><?php echo $_SESSION['proposerfullName']; ?>:</td>
    			<td class="info"><?php echo $owner->firstName." ".$owner->lastName;?></td>
			
				<td class="label"><?php echo $_SESSION['packageName']; ?>:</td>
    			<td class="info"><?php echo $quotations[0]->offerSelected;?></td>
 			</tr>
 			 <tr>
				<td class="label"><?php echo $_SESSION['entryDate']; ?>:</td>
    			<td class="info"><?php echo $quotations[0]->entryDate;?></td>
				<td class="label"><?php echo $_SESSION['previousCompany']; ?>:</td>
    			<td class="info"><?php echo $drivingExperience->insuranceCompany;?></td>
  			</tr>
  			<tr>
				<?php
					if(isset($_SESSION['role']) and $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
					{
						?>
						<td class="label"><?php echo $_SESSION['newCompany']; ?>:</td>
        				<td class="info"><?php echo $quotations[0]->insuranceCompanyOfferingQuote;?></td>
						<?php
					}
					?>
					<td class="label"><?php echo $_SESSION['coverageTypeTab']; ?>:</td>
					<td class="info"><?php echo $quotations[0]->coverageType;?></td>
			</tr>
			
			<?php
				if($quotations[0]->coverageType==$COVERAGE_TYPE_COMPREHENSIVE)
				{
					?>
					<tr>
						<td class="label"><?php echo $_SESSION['coverageAmountTab']; ?>(€):</td>
						<td class="info"><?php echo $vehicle->sumInsured;?></td>
					</tr>
					<?php
				}
				?>
			
			<!-- VEHICLE -->
			<tr>
				<td colspan="4"><h3><?php echo $_SESSION['vehicleDetails']; ?></h3></td>
			</tr>
			<tr>
				<td class="label"><?php echo $_SESSION['vehicleTypeTab']; ?>:</td>
				<td class="info"><?php echo $vehicle->vehicleType;?></td>
				<td class="label"><?php echo $_SESSION['vehicleDesignTab']; ?>:</td>
				<td class="info"><?php echo $vehicle->vehicleDesign;?></td>
			</tr>
			
			<tr>
				<td class="label"><?php echo $_SESSION['make']; ?>:</td>
    			<td class="info"><?php echo $vehicle->make;?></td>
				<td class="label"><?php echo $_SESSION['vehicleModelTab']; ?>:</td>
    			<td class="info"><?php echo $vehicle->model;?></td>
			</tr>
			<tr>
				<td class="label"><?php echo $_SESSION['engineCapacity']; ?>:</td>
				<td class="info"><?php echo $vehicle->cubicCapacity;?></td>
				<td class="label"><?php echo $_SESSION['manufacturedYear']; ?>:</td>
				<td class="info"><?php echo $vehicle->manufacturedYear;?></td>
			</tr>
			
			<!-- PROPOSER LICENSE -->
			<tr>
				<td colspan="4"><h3><?php echo $_SESSION['proposerLicense']; ?></h3></td>
			</tr>
			<tr>
				<td class="label"><?php echo $_SESSION['licenseTypeTab']; ?>:</td>
				<td class="info"><?php echo $license->licenseType;?></td>
				<td class="label"><?php echo $_SESSION['licenseDateTab']; ?>:</td>
				<td class="info"><?php echo date("d-m-Y",strtotime($license->licenseDate));?></td>
			</tr>
			<tr>						
				<td class="label"><?php echo $_SESSION['country']; ?>:</td>
				<td class="info"><?php echo $license->licenseCountry;?></td>
			</tr>
			
			<!-- PROPOSER INFORMATION -->
			<tr>
				<td colspan="4"><h3><?php echo $_SESSION['proposerDetails']; ?></h3></td>
			</tr>
			<tr>
				<td class="label"><?php echo $_SESSION['country']; ?>:</td>
				<td class="info"><?php echo $owner->countryOfBirth;?></td>
				<td class="label"><?php echo $_SESSION['birthDate']; ?>:</td>
				<td class="info"><?php echo $owner->birthDate;?></td>
			</tr>
			<tr>
				<td class="label"><?php echo $_SESSION['profession']; ?>:</td>
				<td class="info"><?php echo $owner->profession;?></td>
			</tr>
			
			<!-- DISCOUNTS APPLIED FOR COMPREHENSIVE -->
			<?php
				if($discounts != null )
				{
					?>
					<tr>
						<td colspan="4"><h3><?php echo $_SESSION['comprehensiveDiscounts']; ?></h3></td>
					</tr>
					<?php
					displayDiscountsInQuotation($discounts);
				}
				?>
			
			<!-- ADDITIONAL CHARGES FACTORS -->
			<?php
			
			if(count($additionalCharges)>0 )
			{
				?>
				
				<tr>
					<td colspan="4"><h3><?php echo $_SESSION['additionalChargesTab']; ?></h3></td>
				</tr>
				<?php
				
				displayAdditionalChargesInPolicyQuotation($additionalCharges);
			}
			?>
			
			<!-- ADDITIONAL BENEFITS FACTORS -->
			<tr>
				<td colspan="4"><h3><?php echo $_SESSION['additionalBenefits']; ?></h3></td>
			</tr>
			<?php
			//display optional coverages
			//displayOptionalCoveragesFromDatabase($optionalCoverages);
			
			displayExistingCoveragesInQuotation($optionalCoverages, $YES_CONSTANT);
			//foreach($optionalCoverages as $eachCoverage)
			//	$eachCoverage->printData();
			
			?>
			
			<!-- BENEFITS REQUESTED BUT NOT ADDED -->
			
			<tr>
				<td colspan="4"><h3><?php echo $_SESSION['notAvailableBenefits']; ?></h3></td>
			</tr>
			<?php
			
			displayExistingCoveragesInQuotation($optionalCoverages, $CANNOT_ADD);
			
			?>
			
			<!-- INSURANCE CLAIMS -->
			<?php
			if(count($claims) > 0 )
			{ ?>
				<tr>
					<td colspan="2"><h3><?php echo $_SESSION['claimsDuringLast3Years']; ?></h3></td>
				</tr>
				<?php
				foreach($claims as $claim)
				{
					?>
					<tr>
						<td class="label" colspan="2"><?php echo date("d-m-Y",strtotime($claim->claimDate));?> -- <?php echo $claim->amount ?>€</td>
					</tr>
				<?php
				}
				?>
			<?php
			}				
			?>
			
			<!-- ADDITIONAL COMMENTS BY USER -->
			<?php
			if(strlen($quotations[0]->userInfo) > 0 )
			{ ?>
				<tr>
					<td colspan="2"><h3><?php echo $_SESSION['additionalComments']; ?></h3></td>
				</tr>
				<tr>
					<td class="label" colspan="2"><?php echo $quotations[0]->userInfo ?></td>
				</tr>
			<?php
			}				
			?>
		</table>
		<table>
			<tr>
			<td><input type="button" value=<?php echo $_SESSION['print']; ?> onclick="printpage()" /> </td>
			<?php
			//show these only when run for www.cyprus-insurances.com
			if($runFromGlobalLocation==false)
			{ ?>
				<td><a href="./quotation.php?action=howToPurchaseInsurance&quoteId=<?php echo $quotations[0]->quoteId;?>"><?php echo $_SESSION['purchase']; ?></a></td>
				<?php
			}
			if(isset($_SESSION['role']) and $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
			{
				?>
				<td>
				
				<!-- create hidden forn, to send the action as a POST -->
					<form action="./quotation.php" id="delQuoteInQuotePresentation" method="post" style="display: none;">
					<input type="text" name="action" value="deleteQuote" />
					<input type="text" name="quoteId" value=<?php echo $quotations[0]->quoteId;?> />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('delQuoteInQuotePresentation').submit()">DELETE QUOTE</a>
					
				<!--<a href="./quotation.php?action=deleteQuote&quoteId=<?php echo $quote->quoteId;?>">DELETE</a></td>-->
				</td>
				</tr>
					<?php
			}
			else
			{?>
				</tr>
				<?php
			}
			//show these only when run for www.cyprus-insurances.com
			if($runFromGlobalLocation==false)
			{ ?>
			
				<tr>
					<td colspan="2"><h3><?php echo $_SESSION['actualCostMayVary']; ?> <a href="./quotation.php?action=disclaimer">Disclaimer</a></h3></td>
				</tr>
				<?php
			}
			?>
		</table>
		</div>
		
		<?php
		}//CASES:
		// 1. CANNOT PROVIDE ONLINE QUOTE AND USER IS ADMIN
		// 2. CAN PROVIDE ONLINE QUOTE
		// WE DON'T SHOW THIS INFORMATION TO SIMPLE USERS END
		
		// Theodoros - Test if this case exist - Now it doesn't exist. But is useful. Reconsider the architecture.				
		//CANNOT PROVIDE ONLINE QUOTE
		if($quotations[0]->canProvideOnlineQuote==$NO_CONSTANT) 
		{ 
			$reasonsWeCannotProvideOnlineQuotes = retrieveReasonsWeCannotProvideOnlineQuote($quotations[0]->quoteId);
			?>
			
			<h3>AN ONLINE QUOTATION CANNOT BE PROVIDED BASED ON YOUR INPUT INFORMATION </h3>
			<br >
			<table>
				<!-- QUOTE -->
				<tr>
					<td><b>Quote Info</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td width="10%">Quote Id:</td><td width="10%"><?php echo $quotations[0]->quoteId;?></td>
					<td width="13%">Entry Date:</td><td width="20%"><?php echo $quotations[0]->entryDate;?></td>
					<td width="50%"></td>
				</tr>
				<tr>
						<td width="10%">Name:</td><td width="20%"><?php echo $user->firstName." ".$user->lastName;?></td>
				</tr>
										
				<!-- REASONSWECANNOTPROVIDEONLINEQUOTE -->
				<tr>
					<td><b>Reasons</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<?php
				foreach($reasonsWeCannotProvideOnlineQuotes as $reasonsWeCannotProvideOnlineQuote)
				{
					?>
				<tr>
					<td colspan=5>+<?php echo $reasonsWeCannotProvideOnlineQuote->reason;?></td><td width="10%"></td>
				</tr>
				<?php
				}
			?>
				
			
			</table>
			
	
			<p> Your case requires special handling and we cannot provide an online quote right now. Please contact our offices.
			
			<?php			
		}// END CANNOT PROVIDE ONLINE QUOTE
		
		
	}
	//QUOTE NOT FOUND - JUST DELETED
	else
	{
		?>
		<h3>QUOTE WAS DELETED.<a href="./quotation.php?action=displayPreviousQuotes&username=<?php echo $_SESSION['searchUserName'];?>" title="continue">CONTINUE</a>
		</h3>
		<?php
		
	}
?>

