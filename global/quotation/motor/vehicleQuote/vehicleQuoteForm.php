<?php				
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
    
include $_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/vehicleQuoteFormDefaultValues.php";		
?>
  				
<script type="text/javascript">		
/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
function setDefaultValues()
{
	//must be set before the showBenefits call
	setOffers("<?php echo $offers_content; ?>");
	
	
	//processInsuranceType();
	
	setPreviousValue("<?php echo $licenseCountry_content; ?>", "licenseCountry");
	setPreviousValue("<?php echo $additionalDriversLicenseCountry_content; ?>", "additionalDriversLicenseCountry");
	setPreviousValue("<?php echo $additionalDriversEthnicity_content; ?>", "additionalDriversEthnicity");
	setPreviousValue("<?php echo $ownerEthnicity_content; ?>", "ownerEthnicity");
	setPreviousValue("<?php echo $hasPreviousInsurance_content; ?>", "hasPreviousInsurance");
	processHasPreviousInsurance();
	
	setPreviousValue("<?php echo $vehicleType_content; ?>", "vehicleType");
	setPreviousValue("<?php echo $vehicleManufacturedYear_content; ?>", "vehicleManufacturedYear");
	setPreviousValue("<?php echo $steeringWheelSide_content; ?>", "steeringWheelSide");
	setPreviousValue("<?php echo $isSportsModel_content; ?>", "isSportsModel");
	setPreviousValue("<?php echo $vehicleDesign_content; ?>", "vehicleDesign");	
	setPreviousValue("<?php echo $licenseType_content; ?>", "licenseType");	
	setPreviousValue("<?php echo $isTaxFree_content; ?>", "isTaxFree");	
	setPreviousValue("<?php echo $isUsedForDeliveries_content; ?>", "isUsedForDeliveries");
	setPreviousValue("<?php echo $coverageType_content; ?>", "coverageType");
	processCoverageQuotation();
	
	setPreviousValue("<?php echo $namedDriversNumber_content; ?>", "namedDriversNumber");
	setPreviousValue("<?php echo $additionalExcess_content; ?>", "additionalExcess");
	setPreviousValue("<?php echo $noClaimDiscountYears_content; ?>", "noClaimDiscountYears");
	setPreviousValue("<?php echo $maxPenaltyPointsOfAnyDriverDuringLastThreeYears_content; ?>", "maxPenaltyPointsOfAnyDriverDuringLastThreeYears");
	setPreviousValue("<?php echo $hasDisability_content; ?>", "hasDisability");
	
	<!-- ADDITIONAL DRIVERS QUESTIONS IF ANY -->
	setPreviousValue("<?php echo $addAdditionalDriversQuestion_content; ?>", "addAdditionalDriversQuestion");
	showAdditionalDriversDetails();
	
	setPreviousValue("<?php echo $additionalDriversLicenseType_content; ?>", "additionalDriversLicenseType");		
	setPreviousValue("<?php echo $normalDrivingLicenseTotalYearsForAdditionalDrivers_content; ?>", "normalDrivingLicenseTotalYearsForAdditionalDrivers");
	setPreviousValue("<?php echo $ageOfYoungestDriver_content; ?>", "ageOfYoungestDriver");
	setPreviousValue("<?php echo $ageOfOldestDriver_content; ?>", "ageOfOldestDriver");	
	
	setPreviousValue("<?php echo $numberOfAccidentsOfAnyDriverDuringLastThreeYears_content; ?>", "numberOfAccidentsOfAnyDriverDuringLastThreeYears");
	showAccidents();
	
	//showBenefits();
	

}
<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "0");



$(document).ready(function(){
	
    $("#ownerBirthDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#ownerBirthDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#ownerBirthDate").datepicker('setDate', '01-01-1995');
    
    $("#licenseDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#licenseDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#licenseDate").datepicker('setDate', '01-01-2000');

    $("#claim1Date").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#claim1Date').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#claim1Date").datepicker('setDate', '01-01-2000');

    $("#claim2Date").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#claim2Date').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#claim2Date").datepicker('setDate', '01-01-2000');

    $("#claim3Date").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#claim3Date').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#claim3Date").datepicker('setDate', '01-01-2000');

    $("#claim4Date").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#claim4Date').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#claim4Date").datepicker('setDate', '01-01-2000');
    
});

  
</script>
		
<!-- Changed by Theodoros Pantelides on 8/2/12  <h2></h2> -->
<h1 id="h1"><?php echo $_SESSION['quotationForm'];?></h1>

<form name="carQuoteForm" action="<?php if($runFromGlobalLocation==true) echo "./quotation.php"; else echo "./index.php"; ?>" method="post" onSubmit="return checkQuotationForm()">
	<input type="hidden" name="action" value="carQuoteFormProcess">
	
	<table>
	
	<!-- PROPOSER DETAILS SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['proposerDetails']; ?></h3></td></tr>
	
	<!-- EMAIL -->
	<tr>
		<td class="label">EMAIL</td>
		<td class="input"><input type="text" name="proposerEmail" id="proposerEmail" size="30" value="" />
		   <a href="./quotation.php?action=glossary&location=proposerEmail" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- PROPOSER TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['proposerTypeTab']; ?></td>
		<td class="input"><select name="proposerType" id="proposerType" style="width:230px;" onChange="processProposerType()">
			<?php
			$i = 1;
			foreach($_SESSION['proposerTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>		
			 </select>	
		</td>
	</tr>
	
	<!-- NAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['nameTab']; ?>:</td>
		<td class="input"><input type="text" name="firstName" id="firstName" size="30" value="<?php echo $firstName_content; ?>" /></td>
	</tr>
	<tr>
		<td class="label"><?php echo $_SESSION['surname']; ?>:</td>
		<td class="input"><input type="text" name="lastName" id="lastName" size="30" value="<?php echo $lastName_content; ?>" /></td>
	</tr>								
	
	<!-- ALL HIDDEN FIELDS -->
	<input type="hidden" name="stateId" id="stateId" value="<?php echo $stateId_content; ?>" >
	<input type="hidden" name="telephone" id="telephone" value="<?php echo $telephone_content; ?>" >
	<input type="hidden" name="cellphone" id="cellphone" value="<?php echo $cellphone_content; ?>" >
	<input type="hidden" name="insuranceType" id="insuranceType" value="<?php echo $insuranceType_content; ?>" >
	<input type="hidden" name="regNumber" id="regNumber" value="<?php echo $regNumber_content; ?>" >
	<input type="hidden" name="contractNumber" id="contractNumber" value="<?php echo $DEFAULT_STATE_ID; ?>" >
	
	<!-- OWNER PROFESSION-->
	<tr>
		<td class="label"><?php echo $_SESSION['profession']; ?></td>
		<td class="input"><input type="text" name="ownerProfession" id="ownerProfession" size="30" value="<?php echo $ownerProfession_content; ?>" /></td>			
	</tr>
		
	<tbody id="proposer-person-info">	
	
	<!-- OWNER BIRTHDATE -->
	<tr>
		<td class="label"><?php echo $_SESSION['birthDate']; ?></td>
		<td class="input">
			<input type="text" style="width: 80px;" id="ownerBirthDate" name="ownerBirthDate" />
			
		</td>
	</tr>
	
	<!-- OWNER ETHNICITY-->
	<tr>
		<td class="label"><?php echo $_SESSION['proposerNationality'];?></td>
		<td class="input"><select name="ownerEthnicity" id="ownerEthnicity" style="width:280px;">
			<?php
					foreach($_SESSION['countriesOptions'] as $eachCountry)
					{?>
					<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
					<?php	
					}
					?>
			</select>	
		</td>
		
	</tr>
	
	</tbody>
	
	
	<!-- empty lines -->
	<tr><td><br /></td></tr>
	<tr><td><br /></td></tr>
	
	<tbody id="motor-license-info">	
	<!-- COUNTRY OF DRIVING LICENSE -->
	<tr>
		<td class="label"><?php echo $_SESSION['countryOfLicense']; ?></td>
		<td class="input"><select name="licenseCountry" id="licenseCountry" style="width:250px;">
			<?php
			foreach($_SESSION['countriesOptions'] as $eachCountry)
			{?>
				<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
			<?php	
			}
			?>
			</select>
			<a href="./quotation.php?action=glossary&location=licenseCountry" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>		
	</tr>
	
	<!-- TYPE OF DRIVING LICENSE -->
	<tr>
		<td class="label"><?php echo $_SESSION['licenseTypeTab']; ?></td>
		<td class="input"><select name="licenseType" id="licenseType">
			<?php
			foreach($_SESSION['licenseTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
		
	</tr>
	
	<!-- LICENSE DATE -->
	<tr>
		<td class="label"><?php echo $_SESSION['licenseDateTab']; ?></td>
		<td class="input">
			<input type="text" style="width: 80px;" id="licenseDate" name="licenseDate" />
			
		</td>
	</tr>
	</tbody>
	
	<tbody id="motor-info" style="display:table-row-group;">
	<tr><td class="h3"><h3><?php echo $_SESSION['drivingExperience']; ?></h3></td></tr>
	<!-- HAS PREVIOUS INSURANCE -->
	<tr>
		<td class="label"><?php echo $_SESSION['previousInsuranceTab']; ?></td>
		<td class="input"><select name="hasPreviousInsurance" id="hasPreviousInsurance" onChange="processHasPreviousInsurance()">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	<tbody id="previous-insurance-info" style="display:none;">
	<tr>
		<td class="label"><?php echo $_SESSION['country']; ?></td>
		<td class="input"><select name="countryOfInsurance" id="countryOfInsurance" style="width:280px;">
			<?php
					foreach($_SESSION['countriesOptions'] as $eachCountry)
					{?>
					<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
					<?php	
					}
					?>
			</select>	
			<a href="./quotation.php?action=glossary&location=ethnicity" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	<tr>
		<!-- INSURANCE COMPANY -->
		<td class="label"><?php echo $_SESSION['insuranceCompanyTab']; ?></td>
		<td class="input"><input type="text" name="insuranceCompany" id="insuranceCompany" size="30" "<?php echo $insuranceCompany_content; ?>" />
		</td>
	</tr>
	
	<!-- YEARS OF EXPERIENCE -->
	<tr>
		<td class="label"><?php echo $_SESSION['drivingExperience']."(".$_SESSION['years'].")"; ?></td>
		<td class="input"><select name="yearsOfExperience" id="yearsOfExperience">
			<?php
       			  for ($i=1; $i<=10; $i++)
				  {?>
				  <OPTION value="<?php echo $i;?>"><?php echo $i; ?></OPTION> 
				  <?php
				  }
					?>
			</select>	
		</td>
	</tr>
	</tbody>
	<tr><td></td></tr>
	
	<tr><td class="h3"><h3><?php echo $_SESSION['coverageTypeTab']; ?></h3></td></tr>									
	<!-- COVERAGE TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['coverageTypeTab']; ?></td>
		<td class="input"><select name="coverageType" id="coverageType" style="width:230px;" onChange="processCoverageQuotation()">
			<?php
			$i = 1;
			foreach($_SESSION['coverageTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>		
			 </select>	
			<a href="./quotation.php?action=glossary&location=coverageType" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<tr id="ca" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['coverageAmountTab']; ?>(<?php echo $EURO;?>):</td>
			
		<!-- input is common for both cases -->
		<td class="input"><input type="text" name="coverageAmount" id="coverageAmount" value="<?php echo $coverageAmount_content; ?>"  size="20" />
		<a href="./quotation.php?action=glossary&location=coverageAmount" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
			
	<tr id="compText" style="display:none;"  >
		<td class="h3"><h3><?php echo $_SESSION['messageForChoosingComprehensive']; ?>:</h3></td>
	</tr>
	<!-- NAMED DRIVERS NUMBER -->
	<tr id="ndn" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['namedDriversNumberTab']; ?></td>
		<td class="input">
			<select name="namedDriversNumber" id="namedDriversNumber">
				<?php
				$i = 1;
				foreach($_SESSION['namedDriversNumberTabOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $i = $i + 1;
				 }
				 ?>		
			</select>
		<a href="./quotation.php?action=FAQ&location=namedDriversNumber" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
			
	<!-- ADDITIONAL EXCESS -->
	<tr id="ae" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['additionalExcessTab']; ?>(<?php echo $EURO;?>):</td>
		<td class="input">
			<select name="additionalExcess" id="additionalExcess">
				<?php
				$i = 1;
				foreach($_SESSION['additionalExcessTabOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $i = $i + 1;
				 }
				 ?>		
			</select>
			<a href="./quotation.php?action=FAQ&location=additionalExcess" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
			
	<!-- NO CLAIM DISCOUNT -->
	<tr id="ncdy" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['noClaimDiscount']; ?></td>
		<td class="input">
			<select name="noClaimDiscountYears" id="noClaimDiscountYears">
				<?php
				$i = 1;
				foreach($_SESSION['noClaimDiscountOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $i = $i + 1;
				 }
				 ?>	
			</select>
			<a href="./quotation.php?action=FAQ&location=noClaimDiscountYears" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	
	
	<tr><td class="h3"><h3><?php echo $_SESSION['vehicleDetails']; ?></h3></td></tr>
	
	
	<!-- VEHICLE TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleTypeTab']; ?></td>
		<td class="input"><select name="vehicleType" id="vehicleType"  style="width:230px;">
			<?php
			$i = 1;
			foreach($_SESSION['vehicleTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$i - $option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			 </select>	
		<a href="./quotation.php?action=glossary&location=vehicleTypes" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- VEHICLE MAKE -->
	<tr>
		<td class="label"><?php echo $_SESSION['make']; ?>( i.e MITSUBISHI ):</td>
		<td class="input"><input type="text" name="vehicleMake" id="vehicleMake" size="30" value="<?php echo $vehicleMake_content; ?>" /></td>
	</tr>
	
	<!-- VEHICLE MODEL -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleModelTab']; ?>( i.e COLT ):</td>
		<td class="input"><input type="text" name="vehicleModel" id="vehicleModel" size="30" value="<?php echo $vehicleModel_content; ?>" /></td>
	</tr>
	
	<!-- CUBIC CAPACITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['engineCapacity']; ?>(CC):</td>
		<td class="input">
			<input type="text" name="cubicCapacity" id="cubicCapacity" size="30" value="<?php echo $cubicCapacity_content; ?>" />
			<a href="./quotation.php?action=FAQ&location=cubicCapacity" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	
	<!-- VEHICLE MANUFACTURE YEAR -->
	<tr>
		<td class="label"><?php echo $_SESSION['manufacturedYear']; ?></td>
		<td class="input"><select name="vehicleManufacturedYear" id="vehicleManufacturedYear" style="width:230px;">
			<?php
			$currentYear = date("Y");
			for($i=$currentYear; $i>1950; $i--){
				?>
				 <option  value=<?php echo $i ?> ><?php echo "$i" ?> </option>
				 <?php
			 }
			 ?>
			 </select>
		</td>
	</tr>
	
						
	<!-- STEERING WHEEL SIDE -->
	<tr>
		<td class="label"><?php echo $_SESSION['steeringWheelSideTab']; ?></td>
		<td class="input"><select name="steeringWheelSide" id="steeringWheelSide">
			<?php
			$i = 1;
			foreach($_SESSION['steeringWheelSideTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			</select>
		<a href="./quotation.php?action=glossary&location=steeringWheelSide" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- SPORTS MODEL -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleSportsModel']; ?></td>
		<td class="input"><select name="isSportsModel" id="isSportsModel">
			<?php
			$i = 1;
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			</select>
		<a href="./quotation.php?action=glossary&location=sportsModel" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- VEHICLE DESIGN -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleDesignTab']; ?></td>
		<td class="input"><select name="vehicleDesign" id="vehicleDesign">
			<?php
			$i = 1;
			foreach($_SESSION['vehicleDesignTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>						
			</select>
		<a href="./quotation.php?action=glossary&location=vehicleDesign" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- TAX FREE -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleTaxFree']; ?></td>
		<td class="input"><select name="isTaxFree" id="isTaxFree">
			<?php
			$i = 1;
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>	
			</select>
		</td>
	</tr>
	
	<!-- USED FOR DELIVERIES -->
	<tr>
		<td class="label"><?php echo $_SESSION['usefForDeliveries']; ?></td>
		<td class="input"><select name="isUsedForDeliveries" id="isUsedForDeliveries">
			<?php
			$i = 1;
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>		
			</select>
		<a href="./quotation.php?action=glossary&location=isUsedForDeliveries" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	
			
	<tr><td class="h3" colspan="2"><h3><?php echo $_SESSION['additionalDriversDetails']; ?></h3></td></tr>
	
	<!-- DISABILITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['hasDisabilityTab']; ?></td>
		<td class="input"><select name="hasDisability" id="hasDisability">
			<?php
				$i = 1;
				foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $i = $i + 1;
				 }
				 ?>	
			</select>
		<a href="./quotation.php?action=glossary&location=hasDisability" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- MAXIMUM PENALTY POINTS OF ANY DRIVER DURING LAST 3 YEARS-->
	<tr>
		<td class="label"><?php echo $_SESSION['penaltyPointsDuringLast3Years']; ?></td>
		<td class="input"><select name="maxPenaltyPointsOfAnyDriverDuringLastThreeYears" id="maxPenaltyPointsOfAnyDriverDuringLastThreeYears">
			<?php
				$i = 1;
				foreach($_SESSION['penaltyPointsDuringLast3YearsOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $i = $i + 1;
				 }
				 ?>	
			</select>
			<a href="./quotation.php?action=glossary&location=maxPenaltyPointsOfAnyDriverDuringLastThreeYears" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- NUMBER OF ACCIDENTS OF ANY DRIVER DURING THE LAST 3 YEARS-->
	<tr>
		<td class="label"><?php echo $_SESSION['claimsDuringLast3Years']; ?></td>
		<td class="input"><select name="numberOfAccidentsOfAnyDriverDuringLastThreeYears" id="numberOfAccidentsOfAnyDriverDuringLastThreeYears" onChange="showAccidents()">
			<?php
				$i = 1;
				foreach($_SESSION['claimsDuringLast3YearsOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $i = $i + 1;
				 }
				 ?>	
			</select>
		</td>
	</tr>
	
	<!-- ACCIDENTS VALUES AND DATES -->
	<tr id="ac1" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['date']; ?>

			<input type="text" style="width: 80px;" id="claim1Date" name="claim1Date" />
			
		</td>
		
		

		<td class="input"><?php echo $_SESSION['claim']; ?>:(€)<input type="text" name="claim1" id="claim1" size="20" value="<?php echo $claim1_content; ?>" />
		</td>
	</tr>
	
	<tr id="ac2" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['date']; ?>

			<input type="text" style="width: 80px;" id="claim2Date" name="claim2Date" />
			
		</td>
		<td class="input"><?php echo $_SESSION['claim']; ?>:(€)<input type="text" name="claim2" id="claim2" size="20" value="<?php echo $claim2_content; ?>" />
		</td>
	</tr>
	
	<tr id="ac3" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['date']; ?>

			<input type="text" style="width: 80px;" id="claim3Date" name="claim3Date" />
			
		</td>
		<td class="input"><?php echo $_SESSION['claim']; ?>:(€)<input type="text" name="claim3" id="claim3" size="20" value="<?php echo $claim3_content; ?>" />
		</td>
	</tr>
	
	<tr id="ac4" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['date']; ?>

			<input type="text" style="width: 80px;" id="claim4Date" name="claim4Date" />
			
		</td>
		<td class="input"><?php echo $_SESSION['claim']; ?>:(€)<input type="text" name="claim4" id="claim4" size="20" value="<?php echo $claim4_content; ?>" />
		</td>
	</tr>
	
	
	 <!-- ADDITIONAL DRIVERS ADD QUESTION -->
	<tr>
		<td class="label"><?php echo $_SESSION['addAdditionalDrivers']; ?></td>
		<td class="input"><select name="addAdditionalDriversQuestion" id="addAdditionalDriversQuestion" onChange="showAdditionalDriversDetails()">
			<?php
				$i = 1;
				foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $i = $i + 1;
				 }
				 ?>	
			</select>
		</td>
	</tr>
	
	</tbody>
	
	<!-- ADDITIONAL DRIVERS TABLE BODY INFO -->
	<tbody id="additional-drivers-info" style="display:none;">
	
	<tr><td class="h3"><h3><?php echo $_SESSION['additionalDriversQuestions']; ?></h3></td></tr>
	
	<!-- ADDITIONAL DRIVER ETHNICITY-->
	<tr>
		<td class="label"><?php echo $_SESSION['nationality'];?></td>
		<td class="input"><select name="additionalDriversEthnicity" id="additionalDriversEthnicity" style="width:280px;">
			<?php
					foreach($_SESSION['countriesOptions'] as $eachCountry)
					{?>
					<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
					<?php	
					}
					?>
			</select>	
			<a href="./quotation.php?action=glossary&location=ethnicity" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
		
	</tr>
	
	<!-- ADDITIONAL DRIVERS COUNTRY OF DRIVING LICENSE -->
	<tr>
		<td class="label"><?php echo $_SESSION['countryOfLicense']; ?></td>
		<td class="input"><select name="additionalDriversLicenseCountry" id="additionalDriversLicenseCountry" style="width:250px;">
			<?php
			foreach($_SESSION['countriesOptions'] as $eachCountry)
			{?>
				<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
			<?php	
			}
			?>
			</select>
			<a href="./quotation.php?action=glossary&location=licenseCountry" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>		
	</tr>
	
	<!-- LICENCE TYPE FOR ADDITIONAL DRIVERS  -->
	<tr>
		<td class="label"><?php echo $_SESSION['licenseTypeTab']; ?></td>
		<td class="input"><select name="additionalDriversLicenseType" id="additionalDriversLicenseType">
				<?php
				$i = 1;
				foreach($_SESSION['licenseTypeTabOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $i = $i + 1;
				 }
				 ?>	
			</select>
			<a href="./quotation.php?action=glossary&location=licenseType" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- LEAST LICENCE YEARS FOR ANY OF THE ADDITIONAL DRIVERS  -->
	<tr>
	<td class="label"><?php echo $_SESSION['yearsOfNormalLicense']; ?></td>
	<td class="input"><select name="normalDrivingLicenseTotalYearsForAdditionalDrivers" id="normalDrivingLicenseTotalYearsForAdditionalDrivers">
			<?php
			$i = 1;
			foreach($_SESSION['yearsOfNormalLicenseOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			</select>
			<a href="./quotation.php?action=glossary&location=normalDrivingLicenseTotalYears" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
		</tr>	
			
	<!-- AGE OF YOUNGEST DRIVER -->
	<tr>
		<td class="label"><?php echo $_SESSION['ageOfYoungestDriverTab']; ?></td>
		<td class="input"><select name="ageOfYoungestDriver" id="ageOfYoungestDriver">
			<?php
			$i = 1;
			foreach($_SESSION['ageOfYoungestDriverTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			</select>
		</td>
	</tr>
			
	<!-- AGE OF OLDEST DRIVER -->
	<tr>
		<td class="label"><?php echo $_SESSION['ageOfOldestDriverTab']; ?></td>
		<td class="input"><select name="ageOfOldestDriver" id="ageOfOldestDriver">
			<?php
			$i = 1;
			foreach($_SESSION['ageOfOldestDriverTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			</select>
		</td>
	</tr>
		
	</tbody>
	
	<!-- OFFERS -->
	<tr><td class="h3"><h3><?php echo $_SESSION['selectBenefitsPackage']; ?></h3></td></tr>

	<!-- PACKAGE OF BENEFITS -->
	<tr id="of1">
		<td class="label"><?php echo $_SESSION['findPackages']; ?></td>
		<td class="input"><input type="radio" id="offers_package" name="offers" value="<?php echo $OFFER_TYPE_PACKAGE; ?>" checked onClick="showBenefits()">
		<a href="./quotation.php?action=glossary&location=packageOfBenefits" target="_blank">
		<IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- these is not available for the comprehensive case, since COMPREHENSIVE offers only packages of benefits -->
	
	<!-- THIRD PARTY COVERAGE -->
	<tr id="of3">
		<td class="label"><?php echo $_SESSION['addSeparateBenefits']; ?></td>
		<td class="input"><input type="radio" id="offers_Benefits" name="offers" value="<?php echo $OFFER_TYPE_THIRD_PARTY; ?>" onClick="showBenefits()">
		<a href="./quotation.php?action=FAQ&location=thirdPartyCoverage" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	

	
	<tbody id="motor-additional-benefits" style="display:none;">
	
	<!-- HIDDEN FIELDS FOR ADDITIONAL BENEFITS -->
	
	<tr><td class="h3"><h3><?php echo $_SESSION['selectSeparateBenefits']; ?></h3></td></tr>
	
	<?php
	//Step 1 - Retrieve All Benefits for Motor
	
	//TODO - send also the vehicle type with function. see comments in function	
	displayAllCoveragesRelatedToInsuranceType($INSURANCE_TYPE_MOTOR);
	//Step 2 - Display Benefits
	
	?>
	
	
	</tbody>
	
	
	<!-- empty lines -->
	<tr><td><br /></td></tr>
	<tr><td><br /></td></tr>
	<!-- USER INFO -->
	<tr><td class="userInfo"><?php echo $_SESSION['anyOtherImportantDetails']; ?></td></tr>
	<tr><td class="userInfo"><textarea name="userInfo" rows="10" cols="50" value=""><?php echo $userInfo_content; ?></textarea></td></tr>
																			
	<!-- CONTINUE BUTTON -->						
	<tr>
		<td><br /></td>
		<p><td><input type="submit" name="send" class="button" value="<?php echo $_SESSION['submitButton']; ?>" size="30" /></td></p>
	</tr>
	
	
</table>
	
	
	
</form>

<!-- end main content -->
