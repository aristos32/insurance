<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

  
//read state id from file
$generalProperties = readGeneralProperties($_SESSION['globalFilesLocation'].'/configurationFiles/generalProperties.xml');
//echo "stateid is $stateId";
$stateId_content = $generalProperties->stateId;
$regNumber_content = $generalProperties->regNumber;

//stateid is negative number for not clients, since its not known
//decrease by 1 to keep negative.
$generalProperties->stateId = $generalProperties->stateId - 1;
$generalProperties->regNumber = $generalProperties->regNumber - 1;

//save new state id in file
createUpdatedGeneralPropertiesXml($_SESSION['globalFilesLocation'].'/configurationFiles/', 'generalProperties.xml', 'generalProperties.xml', $generalProperties);
	
$telephone_content = $DEFAULT_TELEPHONE_NUMBER;
$cellphone_content = $DEFAULT_TELEPHONE_NUMBER;	
$firstName_content = 'John';
$lastName_content = 'Doe';
	  
$ownerBirthDateYear_content = 1985;
$ownerBirthDateMonth_content = 5;
$ownerBirthDateDay_content = 1;

$licenseDateYear_content = 2004;
$licenseDateMonth_content = 5;
$licenseDateDay_content = 1;

$ownerProfession_content = 'Programmer';

$insuranceType_content = $INSURANCE_TYPE_MOTOR;//default. because now we support only motor quotation
$proposerType_content = $PROPOSER_TYPE_PERSON;//default.
			
if(isset($_SESSION['ownerEthnicity']))
		$ownerEthnicity_content = $_SESSION['ownerEthnicity'];
else
	$ownerEthnicity_content = "CYPRUS";

if(isset($_SESSION['additionalDriversEthnicity']))
		$additionalDriversEthnicity_content = $_SESSION['additionalDriversEthnicity'];
else
	$additionalDriversEthnicity_content = "CYPRUS";

	
if(isset($_SESSION['licenseCountry']))
		$licenseCountry_content = $_SESSION['licenseCountry'];
else
	$licenseCountry_content = "CYPRUS";
	
if(isset($_SESSION['additionalDriversLicenseCountry']))
		$additionalDriversLicenseCountry_content = $_SESSION['additionalDriversLicenseCountry'];
else
	$additionalDriversLicenseCountry_content = "CYPRUS";
	
if(isset($_SESSION['insuranceCompany'])){
		$insuranceCompany_content = $_SESSION['insuranceCompany'];
}
else
	$insuranceCompany_content = "NONE";
	
	
if(isset($_SESSION['vehicleType'])){
		$vehicleType_content = $_SESSION['vehicleType'];
}
else
	$vehicleType_content = "1";

if(isset($_SESSION['vehicleMake']))
	$vehicleMake_content = $_SESSION['vehicleMake'];
else
	$vehicleMake_content = 'Mitsubishi';
	
if(isset($_SESSION['vehicleModel']))
	$vehicleModel_content = $_SESSION['vehicleModel'];
else
	$vehicleModel_content = 'Mitsubishi Colt';
		
if(isset($_SESSION['cubicCapacity']))
	$cubicCapacity_content = $_SESSION['cubicCapacity'];
else
	$cubicCapacity_content = '1200';
		
if(isset($_SESSION['vehicleManufacturedYear']))
	$vehicleManufacturedYear_content = $_SESSION['vehicleManufacturedYear'];
else
	$vehicleManufacturedYear_content = '2009';
	
if(isset($_SESSION['steeringWheelSide'])){
		$steeringWheelSide_content = $_SESSION['steeringWheelSide'];
}
else
	$steeringWheelSide_content = "0";
	
if(isset($_SESSION['isSportsModel'])){
		$isSportsModel_content = $_SESSION['isSportsModel'];
}
else
	$isSportsModel_content = "0";


if(isset($_SESSION['hasPreviousInsurance'])){
		$hasPreviousInsurance_content = $_SESSION['hasPreviousInsurance'];
}
else
	$hasPreviousInsurance_content = "NO";
	


	
if(isset($_SESSION['vehicleDesign'])){
	$vehicleDesign_content = $_SESSION['vehicleDesign'];
}
else
	$vehicleDesign_content = "0";

if(isset($_SESSION['isTaxFree'])){
	$isTaxFree_content = $_SESSION['isTaxFree'];
}
else
	$isTaxFree_content = "0";
	
if(isset($_SESSION['isUsedForDeliveries'])){
	$isUsedForDeliveries_content = $_SESSION['isUsedForDeliveries'];
}
else
	$isUsedForDeliveries_content = "0";
	
if(isset($_SESSION['hasVisitorPlates'])){
	$hasVisitorPlates_content = $_SESSION['hasVisitorPlates'];
}
else
	$hasVisitorPlates_content = "0";
	
if(isset($_SESSION['coverageType'])){
	$coverageType_content = $_SESSION['coverageType'];
}
else
	$coverageType_content = "0";

if(isset($_SESSION['namedDriversNumber'])){
	$namedDriversNumber_content = $_SESSION['namedDriversNumber'];
}
else
	$namedDriversNumber_content = "0";

if(isset($_SESSION['additionalExcess'])){
	$additionalExcess_content = $_SESSION['additionalExcess'];
}
else
	$additionalExcess_content = "0";
	
if(isset($_SESSION['noClaimDiscountYears'])){
	$noClaimDiscountYears_content = $_SESSION['noClaimDiscountYears'];
}
else
	$noClaimDiscountYears_content = "0";
	
if(isset($_SESSION['hasDisability'])){
	$hasDisability_content = $_SESSION['hasDisability'];
}
else
	$hasDisability_content = "0";

if(isset($_SESSION['maxPenaltyPointsOfAnyDriverDuringLastThreeYears'])){
	$maxPenaltyPointsOfAnyDriverDuringLastThreeYears_content = $_SESSION['maxPenaltyPointsOfAnyDriverDuringLastThreeYears'];
}
else
	$maxPenaltyPointsOfAnyDriverDuringLastThreeYears_content = "0";
		
if(isset($_SESSION['addAdditionalDriversQuestion'])){
	$addAdditionalDriversQuestion_content = $_SESSION['addAdditionalDriversQuestion'];
}
else
	$addAdditionalDriversQuestion_content = "NO";

if(isset($_SESSION['licenseType']))
		$licenseType_content = $_SESSION['licenseType'];
else
	$licenseType_content = $LICENSE_TYPE_REGULAR;
	
if(isset($_SESSION['additionalDriversLicenseType']))
		$additionalDriversLicenseType_content = $_SESSION['additionalDriversLicenseType'];
else
	$additionalDriversLicenseType_content = $LICENSE_TYPE_REGULAR;
	
if(isset($_SESSION['normalDrivingLicenseTotalYearsForAdditionalDrivers']))
		$normalDrivingLicenseTotalYearsForAdditionalDrivers_content = $_SESSION['normalDrivingLicenseTotalYearsForAdditionalDrivers'];
else
	$normalDrivingLicenseTotalYearsForAdditionalDrivers_content = '';
		
if(isset($_SESSION['ageOfYoungestDriver']))
		$ageOfYoungestDriver_content = $_SESSION['ageOfYoungestDriver'];
else
	$ageOfYoungestDriver_content = '';
	
if(isset($_SESSION['ageOfOldestDriver']))
		$ageOfOldestDriver_content = $_SESSION['ageOfOldestDriver'];
else
	$ageOfOldestDriver_content = '';

//hardcode to use up to 4 accidents - must define ALL variables here
for($i=0; $i<4; $i++)
{
	$var = "claim".($i+1);//string concatenation		
	${$var."_content"} = 0;
	$var = "claim".($i+1)."Date"."Year";//string concatenation
	${$var."_content"} = 2010;
	$var = "claim".($i+1)."Date"."Month";//string concatenation
	${$var."_content"} = 1;
	$var = "claim".($i+1)."Date"."Day";//string concatenation
	${$var."_content"} = 1;
			
}
	
//claims are not zero	
if(isset($_SESSION['numberOfAccidentsOfAnyDriverDuringLastThreeYears']))
{
	$numberOfAccidentsOfAnyDriverDuringLastThreeYears_content = $_SESSION['numberOfAccidentsOfAnyDriverDuringLastThreeYears'];
	$i = 0;
	
	//initialize all 4 accident to zero
	for($i=0; $i<4; $i++)
	{
		$var = "claim".($i+1);//string concatenation
		${$var."_content"} = 0;
				
	}
	
	//set only the selected accidents value
	if(isset($_SESSION['claims']))
	{
		for($i=0; $i<$_SESSION['numberOfAccidentsOfAnyDriverDuringLastThreeYears']; $i++)
		{
			$var = "claim".($i+1);//string concatenation
			${$var."_content"} = $_SESSION['claims'][$i]->amount;
			$var = "claim".($i+1)."Date";//string concatenation
			${$var."_content"} = $_SESSION['claims'][$i]->claimDate;
					
			$var = "claim".($i+1)."Date"."Year";//string concatenation
			//echo $var."<br>";
			//echo "session var is:".$_SESSION[$var]."<br>";
			if(isset($_SESSION[$var]))
				${$var."_content"} = $_SESSION[$var];
			else
				${$var."_content"} = 2010;
			//echo "Content".${$var."_content"}."<br>";
			
			$var = "claim".($i+1)."Date"."Month";//string concatenation
			if(isset($_SESSION[$var]))
				${$var."_content"} = $_SESSION[$var];
			else
				${$var."_content"} = 1;
			$var = "claim".($i+1)."Date"."Day";//string concatenation
			if(isset($_SESSION[$var]))
				${$var."_content"} = $_SESSION[$var];
			else
				${$var."_content"} = 1;
			
		}
	}
}
else
	$numberOfAccidentsOfAnyDriverDuringLastThreeYears_content = "0";

	
if(isset($_SESSION['coverageAmount']))
	$coverageAmount_content = $_SESSION['coverageAmount'];
else
		$coverageAmount_content = 0;


if(isset($_SESSION['userInfo']))
		$userInfo_content = $_SESSION['userInfo'];
else
	$userInfo_content = '';
	
if(isset($_SESSION['offers']))
		$offers_content = $_SESSION['offers'];
else
	$offers_content = '';
	
	
	
?>