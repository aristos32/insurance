<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

/* ALL FUNCTIONS NEEDED FOR ADDING CHARGES BASED ON:
   quoteInfo : input inserted in HTML form
   localVehicleType : input read structure from XML file. Represents a vehicleType.
    */
   

   
/* Process the inputs and calculate the quote 
	quoteInfo : input inserted in HTML form
	basicCoverageInfo : input read from XML file
	$inclusiveBenefit : one of the vehicle->inclusive benefits tags READ from company XML file.
	*/
function addAllCharges($quoteInfo, $basicCoverageInfo, $inclusiveBenefit)
{
	/* define as global, so they can be viewable inside the function */
	global $OFFER_TYPE_THIRD_PARTY, $COVERAGE_TYPE_COMPREHENSIVE;
	global $quoteCost;
	global $basicCoverageCost;
	global $localVehicleType;
	global $processingInfo;
		
	$processingInfo->drivers = new drivers();
	
	/* set the common charges, based on vehicleType, ccCategory, and coverageType */
	addCommonCharges($quoteInfo, $basicCoverageInfo);	
	
	/* add mandatory charges */
	addMandatoryCharges($localVehicleType, $quoteInfo);
	
	addDiscounts($localVehicleType, $quoteInfo);
	
	/* add optional charges depending on the selected benefits.
	for these offers, we just add the benefits manually.
	Handling is the same. */
	$processingInfo->selectedOfferName = $quoteInfo->offerTypeSelected;
	if($quoteInfo->offerTypeSelected==$OFFER_TYPE_THIRD_PARTY)
	{
		addOptionalCharges($localVehicleType, $quoteInfo);
	}
	/* Package selected by the user. Add charges for offer represented in $inclusiveBenefit */
	else
	{
		addOfferCharges($localVehicleType, $quoteInfo, $inclusiveBenefit);
	}
	
	/*LAST step : add the standard charges due to MIF, fees and stamps */
	addStandardCharges($basicCoverageInfo, $quoteInfo);	
	
	//round final cost to 1 euro
	$processingInfo->finalCost = round($quoteCost);
	
	//$processingInfo->printData();
		
	
}//function process


/*  add all the mandatory charges. These cannot change, since depend on driver/vehicle/usage
	data that cannot be changed.
	like : age, license years, licenseType, vehicle age, steering wheel side, sports model etc.
	
	quoteInfo : input inserted in HTML form
 	localVehicleType : input read structure from XML file. Represents a vehicleType.
	*/	
function addMandatoryCharges($localVehicleType, $quoteInfo)
{
	global $YES_CONSTANT;
	$isMandatoryCost = $YES_CONSTANT;//mandatory charges
	
	foreach($quoteInfo->additionalChargesInPolicyQuotationArray as $eachChargeFromInput)
	{
		foreach($localVehicleType->additionalChargesFromXMLArray as $eachChargeFromXML)
		{
			if($eachChargeFromInput->code==$eachChargeFromXML->code)
			{
				//claims - TODO TODO TODO
				if($eachChargeFromXML->code=="insuranceClaimsDuringLast3Years")
					addInsuranceClaimsCharges($eachChargeFromXML, $quoteInfo);
				else
					addGenericCharges($eachChargeFromXML, $YES_CONSTANT, $eachChargeFromInput, $isMandatoryCost);
					//$eachChargeFromXML->printData();
			}
		}
	}

}

	
/* add all the optionalCharges charges
	These charges are optional depending on the
	additional benefits the client wants to add.
	
	quoteInfo : input inserted in HTML form
 	localVehicleType : input read structure from XML file. Represents a vehicleType.
	*/	
function addOptionalCharges($localVehicleType, $quoteInfo)
{
	global $NO_CONSTANT, $YES_CONSTANT;
	global $processingInfo;

	writeToFile(date("Y-m-d H:i:s"). ": addOptionalCharges() ENTER \n", LOG_LEVEL_INFO);
        
	$isMandatoryCost = $NO_CONSTANT;
	$valueFromUserInputBenefitRequired;//is benefit required by user? YES/NO
	
	//read all coverages selected in input from the user.
	foreach($quoteInfo->coveragesInPolicyQuotationArray as $eachCoverageFromInput)
	{
		//$eachCoverageFromInput->printData();
		
		//all coverages read from XML file
		foreach($localVehicleType->optionalCoveragesFromXMLArray as $eachCoverageFromXML)
		{
			//echo "from input=".$eachCoverageFromInput->code;
			//echo "from xml=".$eachCoverageFromXML->code;
			
			if($eachCoverageFromInput->code==$eachCoverageFromXML->code)
			{
				addGenericCharges($eachCoverageFromXML, $YES_CONSTANT, $eachCoverageFromInput, $isMandatoryCost);
			}
		}
	}

	writeToFile(date("Y-m-d H:i:s"). ": addOptionalCharges()  EXIT \n", LOG_LEVEL_INFO);
}


/* User selected the one of the available offers
   add related charges - Charges are only chargeOverBasic for entire offer. No individual benefits charges apply.
   quoteInfo : input inserted in HTML form
   localVehicleType : input read structure from XML file. Represents a vehicleType.
	$inclusiveBenefit : one of the vehicle->inclusive benefits tags READ from company XML file.
 */
function addOfferCharges($localVehicleType, $quoteInfo, $inclusiveBenefit)
{
	global $basicCoverageCost;
	global $quoteCost;
	global $debugNotes;
	global $processingInfo;
	global $YES_CONSTANT, $NO_CONSTANT;
	$additionalCharge = 0;

	writeToFile(date("Y-m-d H:i:s"). ": addOfferCharges() ENTER \n", LOG_LEVEL_INFO);
        
	$lastItem = end($localVehicleType->inclusiveBenefits);
	
	$processingInfo->selectedOfferName = $inclusiveBenefit->name;
	if($inclusiveBenefit->available==$YES_CONSTANT)
	{
                writeToFile(date("Y-m-d H:i:s"). ": found offer match:$quoteInfo->offerTypeSelected, that is available \n", LOG_LEVEL_INFO);
		//$inclusiveBenefit->printData();
		$additionalCharge = $inclusiveBenefit->chargeOverBasic;
		$quoteCost = $quoteCost + $additionalCharge;
		$processingInfo->anyOfferSelected = $YES_CONSTANT;
		//set the variables in inclusiveBenefit
		$processingInfo->inclusiveBenefit = new inclusiveBenefit();
		
		//TODO TODO TODO
		foreach($inclusiveBenefit->optionalCoveragesFromXMLArray as $eachCoverage)
		{
			//included is not set
			//if($eachCoverage->included == $YES_CONSTANT )
			//{
				$processingInfo->inclusiveBenefit->optionalCoveragesFromXMLArray[] = $eachCoverage;
				//$eachCoverage->printData();
			//}
			
		}
	}
	//offer is not available - no matching offer found
	else 
	{
		$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
		$processingInfo->reasonsWeCannotProvideOnlineQuote[] = "OFFER IS NOT AVAILABLE FOR THIS INSURANCE COMPANY";

                writeToFile(date("Y-m-d H:i:s"). ": CANNOT ADD OFFER BECAUSE : OFFER IS NOT AVAILABLE FOR THIS INSURANCE COMPANY \n", LOG_LEVEL_INFO);
	}

        writeToFile(date("Y-m-d H:i:s"). ": addOfferCharges()  EXIT \n", LOG_LEVEL_INFO);
}




/* select the common categories, based on which to add the additional charges
	1. select the correct vehicle type
	2. select the basicCoverageCost for the correct CC category
	3. select the correct coverage type	and add percentage charge
	
	quoteInfo : input inserted in HTML form
	basicCoverageInfo : input read from XML file
	*/	
function addCommonCharges($quoteInfo, $basicCoverageInfo)
{
	global $COVERAGE_TYPE_THIRD_PARTY, $COVERAGE_TYPE_FIRE_AND_THIFT, $COVERAGE_TYPE_COMPREHENSIVE;
	global $NO_CONSTANT, $YES_CONSTANT;
	global $localVehicleType;
	global $quoteCost;
	global $basicCoverageCost;
	global $processingInfo;
	$ccCategories = array();

	writeToFile(date("Y-m-d H:i:s"). ": addCommonCharges() ENTER \n", LOG_LEVEL_INFO);
        
	$foundSameCoverageTypeFile = "false";//if requested coverage type doesn't match an XML file
	
	/* SET THE CCCATEGORIES BASED ON COVERAGE TYPE */
	foreach($localVehicleType->coverageTypes as $coverageType)
	{
		if($quoteInfo->coverageType==$coverageType->value)
		{
			$foundSameCoverageTypeFile = "true";
			
			//ENGINE CAPACITY
			foreach($coverageType->coverages as $eachCoverage)
			{
				if($eachCoverage->code == 'engineCapacity' )
				{
					$ccCategories = $eachCoverage->categories;

                                        writeToFile(date("Y-m-d H:i:s"). ": addCommonCharges()  Found categories for engine capacity \n", LOG_LEVEL_INFO);
					break;
				}
			}
			
			//coverage type is not available. i.e THIRD PARTY -> Available = 'NO'
			//if coverage type is not available, no offers are available either for this type.
			if($coverageType->available==$NO_CONSTANT)
			{
				$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
				$processingInfo->reasonsWeCannotProvideOnlineQuote[] = "Coverage type $coverageType->value is not available";	

				writeToFile(date("Y-m-d H:i:s"). ": CANNOT ADD OFFER BECAUSE : Coverage type $coverageType->value is not available \n", LOG_LEVEL_INFO);
			}
			break;
		}
	}
	
	//aristosa added 10/09/2012
	//if requested coverage type doesn't match an XML file
	if($foundSameCoverageTypeFile == "false" )
	{
		$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
		$processingInfo->reasonsWeCannotProvideOnlineQuote[] = "Coverage type $quoteInfo->coverageType is not supported yet";	

                writeToFile(date("Y-m-d H:i:s"). ": CANNOT ADD OFFER BECAUSE : Coverage type $quoteInfo->coverageType is notsupported yet \n", LOG_LEVEL_INFO);
	}
	
	//continue only if coveragetype is available
	if($processingInfo->canProvideOnlineQuote == $YES_CONSTANT)
	{
		foreach($ccCategories as $ccCategory)
		{
			
			//category is available
			//else
			//{
			//for border conditions, first category found is used. i.e 1600cc belongs to [0,1600] and not to [1600,2200]
			if($quoteInfo->cubicCapacity>=$ccCategory->from and $quoteInfo->cubicCapacity<=$ccCategory->to)
			{
				//category is not allowed
				if ($ccCategory->allowed==$NO_CONSTANT)
				{
					$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
					$processingInfo->reasonsWeCannotProvideOnlineQuote[] = "Cubic Capacity $quoteInfo->cubicCapacity is not available";	

					writeToFile(date("Y-m-d H:i:s"). ": CANNOT ADD OFFER BECAUSE : Cubic Capacity $quoteInfo->cubicCapacity is not available \n", LOG_LEVEL_INFO);		
					break;
				}
				//basiko - basic cost based on cubic capacity
				else
				{
					$basicCoverageCost = $ccCategory->euro;
					$quoteCost = $quoteCost + $basicCoverageCost;

					writeToFile(date("Y-m-d H:i:s"). ": addCommonCharges()  Found category match for engine capacity. Adding cost=$basicCoverageCost, Final Cost=$quoteCost \n", LOG_LEVEL_INFO);
                                        
					/* read minimum only for Comprehensive case */
					if( $quoteInfo->coverageType ==	$COVERAGE_TYPE_COMPREHENSIVE )
					{
						$processingInfo->minimumEuro = $ccCategory->minimumEuro;
					}
						
					break;
				}
			}
			//}
		}
		
		$processingInfo->basicCoverageCost = $basicCoverageCost;
		
		/* CHARGES FOR COVERAGE_TYPE_FIRE_AND_THIFT AND COVERAGE_TYPE_COMPREHENSIVE CASES */
		/* coverageAmount exists only when coverageType is not THIRD PARTY.
		   This charge is added to the basicCoverageCost */
		foreach($localVehicleType->coverageTypes as $coverageType)
		{
			if($coverageType->value==$quoteInfo->coverageType && $coverageType->value==$COVERAGE_TYPE_COMPREHENSIVE)
			{
				addPercentageChargeAmountEuro($coverageType, $quoteInfo, $COVERAGE_TYPE_COMPREHENSIVE);
				break;				
			}
			
		}
	}

	writeToFile(date("Y-m-d H:i:s"). ": addCommonCharges() ENTER \n", LOG_LEVEL_INFO);			
		
}//function addCommonCharges($quoteInfo, $basicCoverageInfo)


/* add the discounts applied for a Comprehensive Coverage
   quoteInfo : input inserted in HTML form
   localVehicleType : input read structure from XML file. Represents a vehicleType.
 */
function addDiscounts($localVehicleType, $quoteInfo)
{
	global $YES_CONSTANT, $NO_CONSTANT, $COVERAGE_TYPE_COMPREHENSIVE;
	global $processingInfo;
	
	$isMandatoryCost = $YES_CONSTANT;

	writeToFile(date("Y-m-d H:i:s"). ": addDiscounts() ENTER \n", LOG_LEVEL_INFO);
        
	//add discounts only for comprehensive, and only when comprehensive is available
	foreach($localVehicleType->coverageTypes as $coverageType)
	{
		writeToFile(date("Y-m-d H:i:s"). ": coverageType->name=$coverageType->name,coverageType->available=$coverageType->available \n", LOG_LEVEL_INFO);
                
		if($coverageType->value==$quoteInfo->coverageType && $coverageType->available==$YES_CONSTANT)
		{
                        writeToFile(date("Y-m-d H:i:s"). ": category match found \n", LOG_LEVEL_INFO);
			//set the excessAmount
			$processingInfo->excessAmount = $processingInfo->excessAmount + $coverageType->excessEuro;
			
			if(is_object($coverageType->discounts))
			{
				foreach($coverageType->discounts->coverages as $eachDiscountInXML )
				{
                                        writeToFile(date("Y-m-d H:i:s"). ": discount from XML code=$eachDiscountInXML->code \n", LOG_LEVEL_INFO);
					foreach($quoteInfo->discountsInQuotationArray as $eachDiscountInQuotation)
					{
                                                writeToFile(date("Y-m-d H:i:s"). ": discount from user input code=$eachDiscountInQuotation->code \n", LOG_LEVEL_INFO);
						if($eachDiscountInQuotation->code==$eachDiscountInXML->code)
						{
                                                        writeToFile(date("Y-m-d H:i:s"). ": match found for code=$eachDiscountInXML->code \n", LOG_LEVEL_INFO);
							//addGenericCharges($eachCoverage, $YES_CONSTANT, $eachDiscountInQuotation, $isMandatoryCost);
							addGenericDiscount($eachDiscountInXML, $eachDiscountInQuotation);
						}
					}
				}
			}
			break;
		}
	}

        writeToFile(date("Y-m-d H:i:s"). ": addDiscounts() EXIT \n", LOG_LEVEL_INFO);
		
}


/* add a discounts in quotation
   discountInXML : discount as defined in XML <discounts> section
   discountInQuotation : discount as read from the user input form
 */
function addGenericDiscount($discountInXML, &$discountInQuotation)
{
	global $basicCoverageCost;
	global $quoteCost;
	global $processingInfo;
	global $YES_CONSTANT, $NO_CONSTANT;

	writeToFile(date("Y-m-d H:i:s"). ": addGenericDiscount() ENTER. code= $discountInQuotation->code. Parameter=".$discountInQuotation->parameters[0]." \n", LOG_LEVEL_INFO);
        
	
	/* loop through all claim categories */
	foreach($discountInXML->categories as $xmlCategory)
	{
		if( $discountInQuotation->parameters[0]>$xmlCategory->from and $discountInQuotation->parameters[0]<=$xmlCategory->to )
		{
			$discount = $quoteCost*$xmlCategory->percentCharge + $xmlCategory->euro;
			//$debugNotes[] = "NAMED DRIVERS DISCOUNT APPLIED FOR BRACKET [".$namedDriversCategory->from.",".$namedDriversCategory->to."]=".$discount."<br />";
			//$processingInfo->discount->namedDriversNumber = $namedDriversCategory->to;
			//$processingInfo->discount->namedDriversNumberAmount = $discount;
			$discountInQuotation->charge = $discount;
			//$discountInQuotation->printData();
			$quoteCost = $quoteCost - $discount;
			/* for Comprehensive, cost cannot be lower than a minimum */
			if($quoteCost < $processingInfo->minimumEuro )
				$quoteCost = $processingInfo->minimumEuro;
			break;
		}
	}

	writeToFile(date("Y-m-d H:i:s"). ": addGenericDiscount() EXIT \n", LOG_LEVEL_INFO);
}





//TODO TODO TODO FIX FIX FIX FIX 

//remove $valueFromUserInputBenefitRequired???


/* generic function to add the charges in the total $quoteCost
   $valueFromXml : input read structure from XML file. f.e $localVehicleType->optionalCoveragesFromXML[]
   $valueFromUserInputBenefitRequired : input inserted in HTML form(from quotationInfo)
   		Values: YES(charge or benefit must be added)/NO(charge/benefit not needed to be added)
   $coverageFromInput : coverage as read from input, for updating.
   		$coverageFromInput->parameters[0] : value to match with category[from,to]. Can be amount, years, etc. Not needed:0, Needed: > 0.
   		$coverageFromInput->code : code of current benefit from coverageDefinitions.xml,.Value is as in tables addedNotMandatoryCosts or addedMandatoryCosts, f.e roadAssistance
   $mandatoryCost : YES->add it in the addedMandatoryCosts, NO-> add it in the addedNotMandatoryCosts
   $failureMessage : message to put in reasonsWeCannotProvideOnlineQuote[] f.e "DRIVER HAS LEARNERS LICENSE"
 */
function addGenericCharges($valueFromXml, $valueFromUserInputBenefitRequired, &$coverageFromInput, $mandatoryCost)
{
	global $YES_CONSTANT, $NO_CONSTANT, $OTHER_CONSTANT;
	global $basicCoverageCost;
	global $quoteCost;
	global $processingInfo;
	$additionalCharge = 0;
	$foundMatchingCategory = 0;
	$lastMatchingCategory;
	$otherCategory = null;
	$coverageCostInfo = new coveragesInPolicyQuotation();
	$logMessage = date("Y-m-d H:i:s"). ": addGenericCharges() called for $coverageFromInput->code ENTER \n";
	$logMessage = $logMessage . date("Y-m-d H:i:s"). ": valueFromUserInputBenefitRequired=$valueFromUserInputBenefitRequired, categoryType=$coverageFromInput->categoryType, mandatoryCost=$mandatoryCost \n";

	writeToFile($logMessage, LOG_LEVEL_INFO);
        
	$coverageCostInfo->code = $coverageFromInput->code;
	$coverageCostInfo->stillNeeded = -1;//default
	
	//user input requires this benefit - PROCEED
	if($valueFromUserInputBenefitRequired==$YES_CONSTANT || $valueFromUserInputBenefitRequired==1 )
	{
		if(!is_object($valueFromXml))
		{
			writeToFile(date("Y-m-d H:i:s"). ": ERROR This is not an object \n", LOG_LEVEL_INFO);
		}
		
		//whole coverage is not allowed - fail and return
		if(property_exists($valueFromXml, 'allowed') && $valueFromXml->allowed == $NO_CONSTANT )
		{
			//echo " CANNOT ADD OFFER BECAUSE value for $coverageCostInfo->code is not allowed \n";
			$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
			$processingInfo->reasonsWeCannotProvideOnlineQuote[] = "Property $coverageCostInfo->code is not allowed \n";
			return;
		}
		
		//tag is applicable - PROCEED
		if($valueFromXml->applicable == $YES_CONSTANT )
		{
			//store last categories item
			$lastItem = end($valueFromXml->categories);
			//dynamic object property/field name example
				
			//if <category> tag exists - get charges from categories
			if(count($valueFromXml->categories) > 0 )
			{
				
				//check all categories from XML
				foreach($valueFromXml->categories as $category )
				{
					writeToFile(date("Y-m-d H:i:s"). ": type=$category->type, [from,to]=[$category->from,$category->to], allowed=$category->allowed \n", LOG_LEVEL_INFO);
                                        
					//at least one matching category is found
					$foundMatchingCategory = 1;
					//save last matching category - only if it is allowed
					if($category->allowed==$YES_CONSTANT)
						$lastMatchingCategory = $category;
						
					//match category ranges
					//case 1 : from and to are null. So we consider that the valueFromUserInputBenefitAmount matches when type is same, from previous check
					//case 2 : from and to are not null. Check that input value falls in the range.
					if(($category->from==null && $category->to==null) || 
						( $coverageFromInput->parameters[0] >= $category->from && $coverageFromInput->parameters[0] <= $category->to) )
					{
						//$coverageFromInput->printData();
						//for personal accidents this is not set - BUG
						if(count($coverageFromInput->parameters) > 0)
						{
                                                        writeToFile(date("Y-m-d H:i:s"). ": input parameter 1 = ".$coverageFromInput->parameters[0]." \n", LOG_LEVEL_INFO);
						}

                                                writeToFile(date("Y-m-d H:i:s"). ": found category match for type=$category->type, with [from,to]=[$category->from,$category->to] \n", LOG_LEVEL_INFO);
                                                
						//category is not allowed -> fail
						if($category->allowed == $NO_CONSTANT )
						{
							//echo " CANNOT ADD OFFER BECAUSE value for $coverageCostInfo->code is not allowed \n";
							$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
							$processingInfo->reasonsWeCannotProvideOnlineQuote[] = " Value for $coverageCostInfo->code category is not allowed \n";

                                                        writeToFile(date("Y-m-d H:i:s"). ": Code=".$coverageCostInfo->code.". CANNOT ADD BECAUSE category is not allowed \n", LOG_LEVEL_INFO);
							break;//finish flow - cannot add
		
						}
						//category is allowed - value can be 'YES' or simply empty
						else 
						{
							//TODO - Aristos Aresti
							//check for $category->applicable
							//if NO, we can use the next category available
							
							//calculate additional charge
							$additionalCharge = $basicCoverageCost*($category->percentCharge)+$category->euro;
							//benefit is added, but maybe has no charge associated with it.
							//if additionalCharge is 0, we set the StillNeeded to -1, in order to be stored in optionalCoverages, even though there is no charge for this.
							//see storeQuoteInDatabase.php -> function fillOptionalCoveragesClass($quoteInfo)
							if($additionalCharge==0)
							{	
								$coverageCostInfo->stillNeeded = -1;
                                                                writeToFile(date("Y-m-d H:i:s"). ": benefit $coverageFromInput->code has 0 charge, but still is added \n", LOG_LEVEL_INFO);
							}
							
							//cost is mandatory, add in addedMandatoryCosts structure
							if($mandatoryCost==$YES_CONSTANT)
							{
								$coverageCostInfo->charge = $additionalCharge;
								$processingInfo->addedMandatoryCosts[] = $coverageCostInfo;

                                                                writeToFile(date("Y-m-d H:i:s"). ": adding mandatory charge=$coverageCostInfo->charge, for $coverageCostInfo->code \n", LOG_LEVEL_INFO);
							}
							//cost is not mandatory -> add in addedNotMandatoryCosts structure
							else
							{
								//$localSelectedCoverageName = $coverageFromInput->code."SelectedCoverage";//f,e passengerLiabilitySelectedCoverage
								//set category->to as the amount of coverage
								if( $category->to <> null )
								{
									//$processingInfo->addedNotMandatoryCosts->$localSelectedCoverageName = $category->to;//selecting the maximum of the catetory as the coverage
									$coverageCostInfo->selectedCoverage = $category->to;//selecting the maximum of the catetory as the coverage
								}
								
								else
								{
									$coverageCostInfo->selectedCoverage = $coverageFromInput->param1;
								}
								
								$coverageCostInfo->charge = $additionalCharge;
								$processingInfo->addedNotMandatoryCosts[] = $coverageCostInfo;
							}
							$logMessage =$logMessage . date("Y-m-d H:i:s"). ": quoteCost before is $quoteCost, additionalCharge=$additionalCharge  \n";
							$quoteCost = $quoteCost + $additionalCharge;
							$logMessage = $logMessage . date("Y-m-d H:i:s"). ": quoteCost after is $quoteCost  \n";

							writeToFile($logMessage, LOG_LEVEL_INFO);
                                                        
							break;//out of for loop
						}
						
						
					}//if(($category->from==null && $category->to==null))
					
					//last category reached
					if($category==$lastItem)
					{
						writeToFile(date("Y-m-d H:i:s"). ": last matching category match for type=$lastMatchingCategory->type, with [from,to]=[$lastMatchingCategory->from,$lastMatchingCategory->to] \n", LOG_LEVEL_INFO);
                                                
						//only when the value selected is not zero, add values
						//cases like in personal accidents that we have an amount or drivers license years that we have a number of years 
						if($coverageFromInput->parameters[0] >0 )
						{
							
							//for mandatory costs, if no much is found, then no charge is applied - f.e drivers license years that we have a number of years
							if($mandatoryCost==$YES_CONSTANT)
							{
								writeToFile(date("Y-m-d H:i:s"). ": for $coverageFromInput->code, no charge is applied EXIT \n", LOG_LEVEL_INFO);
                                                                
								$additionalCharge = 0;//todo - extend also for mandatory costs if applicable
							}
							//for non-mandatory costs, if no match is found, then last category is applied. So, the closest possible benefit is provided.
							else
							{
								//matching category is found => use lastMatchingCategory
								if($foundMatchingCategory==1)
								{
									$additionalCharge = $basicCoverageCost*($lastMatchingCategory->percentCharge)+$lastMatchingCategory->euro;
									
									if($additionalCharge==0)
									{	
										$coverageCostInfo->stillNeeded = -1;

                                                                                writeToFile(date("Y-m-d H:i:s"). ": benefit $coverageFromInput->code has 0 charge, but still is added. Adding last matching category \n", LOG_LEVEL_INFO);
									}
									
									
									$coverageCostInfo->charge = $additionalCharge;
									$coverageFromInput->parameters[0] = $lastMatchingCategory->to;//TODO is this needed?
									$coverageCostInfo->selectedCoverage = $lastMatchingCategory->to;
									
								}
								//no matching category found, benefit is not available, is still needed
								else
								{
									$coverageCostInfo->stillNeeded = 1;
								}
							}
							//if this is -1, do not add. has meaning that benefit was added, but without any charge
							$quoteCost = $quoteCost + $additionalCharge;
							$processingInfo->addedNotMandatoryCosts[] = $coverageCostInfo;
							
							
						
						}					
											
					}//if($category==$lastItem)		
					
				}//foreach($valueFromXml->categories as $category )
				
			}//if(count($valueFromXml->categories) > 0 )
			
			////if <typeCategories> tag exists - get charges from typeCategories
			else if(count($valueFromXml->typeCategories) > 0 )
			{
				$foundMatchingCategory = 0;
				
				//check all categories from XML
				foreach($valueFromXml->typeCategories as $category )
				{
					//if OTHER <type> is found, store for later use
					if($category->type==$OTHER_CONSTANT)
					{
						$otherCategory = $category;
						//$otherCategory->printData();
					}
					
					
					//category type matches with input - no match needed when category->type is empty
					if($coverageFromInput->categoryType == $category->type )
					{
						writeToFile(date("Y-m-d H:i:s"). ": found match for type=$category->type \n", LOG_LEVEL_INFO);
                                                
						//at least one matching category is found
						$foundMatchingCategory = 1;
						//save last matching category
						$lastMatchingCategory = $category;
							
						//category is not allowed -> fail
						if($category->allowed == $NO_CONSTANT )
						{
							//echo " CANNOT ADD OFFER BECAUSE value for $coverageCostInfo->code is not allowed \n";
							$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
							$processingInfo->reasonsWeCannotProvideOnlineQuote[] = " Value for $coverageCostInfo->code category is not allowed \n";
							
                                                        writeToFile(date("Y-m-d H:i:s"). ": Code=".$coverageCostInfo->code.". CANNOT ADD BECAUSE category is not allowed \n", LOG_LEVEL_INFO);
							break;//finish flow - cannot add
		
						}
						//category is allowed - value can be 'YES' or simply empty
						else 
						{
															
							//calculate additional charge
							$additionalCharge = $basicCoverageCost*($category->percentCharge)+$category->euro;
							//benefit is added, but maybe has no charge associated with it.
							//if additionalCharge is 0, we set the StillNeeded to -1, in order to be stored in optionalCoverages, even though there is no charge for this.
							//see storeQuoteInDatabase.php -> function fillOptionalCoveragesClass($quoteInfo)
							if($additionalCharge==0)
							{	
								$coverageCostInfo->stillNeeded = -1;
			                                        writeToFile(date("Y-m-d H:i:s"). ": benefit $coverageFromInput->code has 0 charge, but still is added. \n", LOG_LEVEL_INFO);
							}
							
							//cost is mandatory, add in addedMandatoryCosts structure
							if($mandatoryCost==$YES_CONSTANT)
							{
								$coverageCostInfo->charge = $additionalCharge;
								$processingInfo->addedMandatoryCosts[] = $coverageCostInfo;
							
                                                                writeToFile(date("Y-m-d H:i:s"). ": adding mandatory charge=$coverageCostInfo->charge, for $coverageCostInfo->code \n", LOG_LEVEL_INFO);
							}
							//cost is not mandatory -> add in addedNotMandatoryCosts structure
							else
							{
								$coverageCostInfo->selectedCoverage = $coverageFromInput->param1;
								$coverageCostInfo->charge = $additionalCharge;
								$processingInfo->addedNotMandatoryCosts[] = $coverageCostInfo;
							}
							$logMessage =$logMessage . date("Y-m-d H:i:s"). ": quoteCost before is $quoteCost, additionalCharge=$additionalCharge  \n";
							$quoteCost = $quoteCost + $additionalCharge;
							writeToFile(date("Y-m-d H:i:s"). ": quoteCost after is $quoteCost \n", LOG_LEVEL_INFO);	
							break;//out of for loop
						}
							
						
					}//if($coverageFromInput->categoryType == $category->type )
					
					//if we reach here, means that no much was found for category types
					//for mandatory costs, if no match is found, then no charge is applied - f.e drivers license years that we have a number of years
					if($mandatoryCost==$YES_CONSTANT)
					{
						//if OTHER category exists in XML, apply charges of this category
						//if($otherCategory!=null)
						if(is_object($otherCategory) && (count(get_object_vars($otherCategory)) > 0))
						{
							if($otherCategory->allowed==$YES_CONSTANT)
							{
								$additionalCharge = $basicCoverageCost*($otherCategory->percentCharge)+$otherCategory->euro;
								$coverageCostInfo->charge = $additionalCharge;
								$quoteCost = $quoteCost + $additionalCharge;
								$processingInfo->addedMandatoryCosts[] = $coverageCostInfo;
							}
							else
							{
								$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
								$processingInfo->reasonsWeCannotProvideOnlineQuote[] = " Value for $coverageCostInfo->code category is not allowed \n";

                                                                writeToFile(date("Y-m-d H:i:s"). ": Code=".$coverageCostInfo->code.". CANNOT ADD BECAUSE category is not allowed \n", LOG_LEVEL_INFO);
								break;//finish flow - cannot add
							}
						}

						writeToFile(date("Y-m-d H:i:s"). ": for $coverageFromInput->code, no charge is applied EXIT \n", LOG_LEVEL_INFO);
                                                
						$additionalCharge = 0;//todo - extend also for mandatory costs if applicable
					}
					//for non-mandatory costs, if no match is found, then last category is applied. So, the closest possible benefit is provided.
					else
					{
						//matching category is found => use lastMatchingCategory
						if($foundMatchingCategory==1)
						{
							$additionalCharge = $basicCoverageCost*($lastMatchingCategory->percentCharge)+$lastMatchingCategory->euro;
							
							if($additionalCharge==0)
							{	
								$coverageCostInfo->stillNeeded = -1;
                                                                writeToFile(date("Y-m-d H:i:s"). ": benefit $coverageFromInput->code has 0 charge, but still is added. Adding last matching category \n", LOG_LEVEL_INFO);
							}
							
							
							$coverageCostInfo->charge = $additionalCharge;
							$coverageFromInput->parameters[0] = $lastMatchingCategory->to;//TODO is this needed?
							$coverageCostInfo->selectedCoverage = $lastMatchingCategory->to;
							
						}
						//no matching category found, benefit is not available, is still needed
						else
						{
							$coverageCostInfo->stillNeeded = 1;
						}
					}
					//if this is -1, do not add. has meaning that benefit was added, but without any charge
					$quoteCost = $quoteCost + $additionalCharge;
					$processingInfo->addedNotMandatoryCosts[] = $coverageCostInfo;
					
				}
			}
			
			//no categories exist -> get charges from coveragesFromCompanyXML->euro or percentCharge
			else
			{
				//calculate additional charge
				$additionalCharge = $basicCoverageCost*($valueFromXml->percentCharge)+$valueFromXml->euro;
				//benefit is added, but maybe has no charge associated with it.
				//if additionalCharge is 0, we set the anyDriverCanDriveStillNeeded to -1, in order to be stored in optionalCoverages, even though there is no charge for this.
				//see storeQuoteInDatabase.php -> function fillOptionalCoveragesClass($quoteInfo)
				if($additionalCharge==0)
				{	
					$coverageCostInfo->stillNeeded = -1;
                                        writeToFile(date("Y-m-d H:i:s"). ": benefit $coverageFromInput->code has 0 charge, but still is added. \n", LOG_LEVEL_INFO);
				}
				
				//cost is mandatory, add in addedMandatoryCosts structure
				if($mandatoryCost==$YES_CONSTANT)
				{
					$coverageCostInfo->charge = $additionalCharge;
					$processingInfo->addedMandatoryCosts[] = $coverageCostInfo;//f.e processingInfo->addedNotMandatoryCosts->roadAssistanceAddedCost
				}
				//cost is not mandatory -> add in addedNotMandatoryCosts structure
				else
				{
					$localSelectedCoverageName = $coverageFromInput->code."SelectedCoverage";//f,e passengerLiabilitySelectedCoverage
					$coverageCostInfo->charge = $additionalCharge;
				}
				$logMessage =$logMessage . date("Y-m-d H:i:s"). ": quoteCost before is $quoteCost, additionalCharge=$additionalCharge  \n";
				$quoteCost = $quoteCost + $additionalCharge;
				$processingInfo->addedNotMandatoryCosts[] = $coverageCostInfo;
				//$coverageCostInfo->printData();

                                writeToFile(date("Y-m-d H:i:s"). ": quoteCost after is $quoteCost \n", LOG_LEVEL_INFO);
			}
			
		}//if($valueFromXml->applicable == $YES_CONSTANT )
			
		//tag is not applicable - add it in not available benefits list
		else
		{
			writeToFile(date("Y-m-d H:i:s"). ": $coverageFromInput->code is not applicable, cannot be added EXIT \n", LOG_LEVEL_INFO);
                        
			//if($mandatoryCost==$YES_CONSTANT)
				$coverageCostInfo->stillNeeded = 1;
			//else
				//$processingInfo->addedNotMandatoryCosts->$localBenefitName = 1;//set indicator for later processing, since now cannot add this benefit
			//	$coverageCostInfo->stillNeeded = 1;
				
			$processingInfo->addedNotMandatoryCosts[] = $coverageCostInfo;
		}
		
	}//if($valueFromUserInputBenefitRequired == $YES_CONSTANT )
	
	writeToFile(date("Y-m-d H:i:s"). ": addGenericCharges() for $coverageFromInput->code EXIT \n", LOG_LEVEL_INFO);
		
}


/* add the standard charges due to MIF, fees and stamps
   basicCoverageInfo : input read from XML file
 */
function addStandardCharges($basicCoverageInfo, $quoteInfo)
{
	global $basicCoverageCost;
	global $quoteCost;
	global $processingInfo;
	global $COVERAGE_TYPE_COMPREHENSIVE;
	$additionalCharge;

	writeToFile(date("Y-m-d H:i:s"). ": addStandardCharges() ENTER \n", LOG_LEVEL_INFO);
        
	$processingInfo->standardCharges = new standardCharges();
	$processingInfo->standardCharges->MIFPercentCharge = $quoteCost*$basicCoverageInfo->standardCharges->MIFPercent;
	$processingInfo->standardCharges->stamps = $basicCoverageInfo->standardCharges->stamps;
	$processingInfo->standardCharges->fees = $basicCoverageInfo->standardCharges->fees;
	$processingInfo->standardCharges->other = $basicCoverageInfo->standardCharges->other;
	$processingInfo->standardCharges->otherPercent = $basicCoverageInfo->standardCharges->otherPercent;
	
	//useful echo - KEEP
	//echo "STANDARD CHARGES MIF=".$quoteCost*$basicCoverageInfo->standardCharges->MIFPercent.",Stamps=".$basicCoverageInfo->standardCharges->stamps.",Fees=".$basicCoverageInfo->standardCharges->fees.",Other=".$basicCoverageInfo->standardCharges->other."<br />";

	writeToFile(date("Y-m-d H:i:s"). ": STANDARD CHARGES MIF=".$quoteCost*$basicCoverageInfo->standardCharges->MIFPercent.",Stamps=".$basicCoverageInfo->standardCharges->stamps.",Fees=".$basicCoverageInfo->standardCharges->fees.",Other=".$basicCoverageInfo->standardCharges->other."\n", LOG_LEVEL_INFO);
		
	$additionalCharge = $quoteCost*$basicCoverageInfo->standardCharges->MIFPercent + $basicCoverageInfo->standardCharges->stamps + 
		$basicCoverageInfo->standardCharges->fees + $basicCoverageInfo->standardCharges->other + $quoteCost*$basicCoverageInfo->standardCharges->otherPercent;
	
	/* for MINERVA, COMPREHENSIVE there is a 2% additional charge, over the 8% of MIF */ 
	//if($quoteInfo->coverageType==$COVERAGE_TYPE_COMPREHENSIVE and $processingInfo->insuranceCompanyOfferingQuote==$INSURANCE_COMPANY_MINERVA )
	//	$additionalCharge = $additionalCharge + $quoteCost*$basicCoverageInfo->standardCharges->otherPercent;
		
	$quoteCost = $quoteCost + $additionalCharge;

	writeToFile(date("Y-m-d H:i:s"). ": addStandardCharges() called. EXIT \n", LOG_LEVEL_INFO);
}



/* add the charges related to the insurance claims during the last 3 years
   chargeFromXml : input read structure from XML file. Represents a vehicleType
   quoteInfo : all input from HTML form. */
function addInsuranceClaimsCharges($chargeFromXml, $quoteInfo)
{
	global $basicCoverageCost;
	global $quoteCost;
	$additionalCharge = 0;
	global $processingInfo;
	global $NO_CONSTANT, $YES_CONSTANT;
	global $CHARGE_PER_CLAIM, $CHARGE_PER_TOTAL_AMOUNT;
	$coverageCostInfo = new coveragesInPolicyQuotation();
	
	$coverageCostInfo->code = $chargeFromXml->code;
	
	writeToFile(date("Y-m-d H:i:s"). ": addInsuranceClaimsCharges() called. charge type= ". $chargeFromXml->param3 . " ENTER \n", LOG_LEVEL_INFO);
        
	//if not applicable, don't add any charges
	if($chargeFromXml->applicable == $YES_CONSTANT )
	{
		
		/* Case 1 : CHARGE A PERCENTAGE FOR EACH CLAIM */
		if($chargeFromXml->param3==$CHARGE_PER_CLAIM)
		{
			/* loop through all claims */
			foreach($quoteInfo->claims as $claim)
			{
				/* loop through all claim categories */
				foreach($chargeFromXml->categories as $category)
				{
					if($claim->amount>=$category->from and $claim->amount<$category->to)
					{
						/* category is allowed for online quote */
						if($category->allowed==$YES_CONSTANT)
						{
							$additionalCharge = $basicCoverageCost*($category->percentCharge) + $category->euro;;
							$coverageCostInfo->charge = $additionalCharge;
							$processingInfo->addedMandatoryCosts[] = $coverageCostInfo;
							$quoteCost = $quoteCost + $additionalCharge;
						}
						/* cannot provided online quote, need to contact insurance company */
						else
						{
							$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
							$processingInfo->reasonsWeCannotProvideOnlineQuote[] = "HIGH CLAIM DURING THE LAST 3 YEARS";
		
                                                        writeToFile(date("Y-m-d H:i:s"). ": CANNOT ADD OFFER BECAUSE : HIGH CLAIM DURING THE LAST 3 YEARS \n", LOG_LEVEL_INFO);
							break;
						}
							
					}
				}		
			}
		}//END 
		
		/* Case 2 : CHARGE FOR TOTAL AMOUNT OF CLAIMS */
		else if($chargeFromXml->param3==$CHARGE_PER_TOTAL_AMOUNT)
		{
			$totalClaimAmount = 0;
			
			/* Calculate the total claim amount */
			foreach($quoteInfo->claims as $claim)
			{
				$totalClaimAmount = $totalClaimAmount + $claim->amount;
			}
	
			/* loop through all claim categories */
			foreach($chargeFromXml->categories as $category)
			{
				if($totalClaimAmount>=$category->from and $totalClaimAmount<$category->to)
				{
					/* category is allowed for online quote */
					if($category->allowed==$YES_CONSTANT)
					{
						$additionalCharge = $basicCoverageCost*($category->percentCharge) + $category->euro;
						$quoteCost = $quoteCost + $additionalCharge;
						$coverageCostInfo->charge = $additionalCharge;
						$processingInfo->addedMandatoryCosts[] = $coverageCostInfo;
						break;
					}
					/* cannot provided online quote, need to contact insurance company */
					else
					{
						$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
						$processingInfo->reasonsWeCannotProvideOnlineQuote[] = "HIGH CLAIM DURING THE LAST 3 YEARS";

						writeToFile(date("Y-m-d H:i:s"). ": CANNOT ADD OFFER BECAUSE : HIGH CLAIM DURING THE LAST 3 YEARS \n", LOG_LEVEL_INFO);	
										break;
					}
						
				}
			}
		}//END 
	}

        writeToFile(date("Y-m-d H:i:s"). ": addInsuranceClaimsCharges() called. EXIT \n", LOG_LEVEL_INFO);
		
}

/* Calculate the additionalPercentage charge for FIRE AND THIFT and COMPREHENSIVE coverages.
   quoteInfo : input inserted in HTML form
   coverageType : input read structure from XML file. Can be either fireAndTheft or comprehensive
   coverageTypeName : name of the coverage as String. Can be "Fire And Thift" or "Comprehensive" .
   This method supports:
   	1. Fire and Theft for Pancyprian and Minerva
   	2. Comprehensive for Pancyprian and Minerva
   	
   	For Olympic we have no information */
function addPercentageChargeAmountEuro($coverageType, $quoteInfo, $coverageTypeName )
{
	global $YES_CONSTANT, $NO_CONSTANT;
	global $COVERAGE_TYPE_THIRD_PARTY, $COVERAGE_TYPE_FIRE_AND_THIFT, $COVERAGE_TYPE_COMPREHENSIVE;
	global $userOutputNotes;
	global $quoteCost;
	global $processingInfo;
	global $basicCoverageCost;
	$percentageChargeAmountEuro = 0;/* holds the total additional charge for $COVERAGE_TYPE_FIRE_AND_THIFT, $COVERAGE_TYPE_COMPREHENSIVE cases.
								the amount is in euro */
	$amountCategories = array();
	//$debugNotes[] = "MINIMUM CHARGE FOR ".$coverageTypeName."=".$coverageType->minimumChargeEuro."<br />";

	writeToFile(date("Y-m-d H:i:s"). ": addPercentageChargeAmountEuro() ENTER. Initial quoteCost=$quoteCost \n", LOG_LEVEL_INFO);
        
	/*coverage is available */
	if($coverageType->available==$YES_CONSTANT)
	{
		//FIND AMOUNT CATEGORIES
		foreach($coverageType->coverages as $eachCoverage)
		{
			if($eachCoverage->code == 'coverageAmountTab' )
			{
				$amountCategories = $eachCoverage->categories;
				break;
			}
		}
		/* select all the applicable amountCategories */
		foreach($amountCategories as $amountCategory)
		{
			
			/*FINAL AMOUNT CATEGORY is reached. Add amount and break */
			if($quoteInfo->coverageAmount>=$amountCategory->from and $quoteInfo->coverageAmount<=$amountCategory->to)
			{
				$currentCharge = ($quoteInfo->coverageAmount-$amountCategory->from)*$amountCategory->percentCharge + $amountCategory->euro;
				
				$percentageChargeAmountEuro = $percentageChargeAmountEuro + $currentCharge;
				
                                writeToFile(date("Y-m-d H:i:s"). ": addPercentageChargeAmountEuro(). adding for category[$amountCategory->from,$amountCategory->to] charge=$currentCharge \n", LOG_LEVEL_INFO);
				//$debugNotes[] = "FINAL ADDITIONAL CHARGE FOR ".$coverageTypeName."=".($quoteInfo->coverageAmount-$amountCategory->from)*$amountCategory->percentCharge."<br />";
				break;
			}
			/*INTERMEDIATE AMOUNT CATEGORY. NEED TO ADD THE AMOUNT AND MOVE TO THE NEXT */
			else if($quoteInfo->coverageAmount>$amountCategory->to)
			{
				$currentCharge = ($amountCategory->to-$amountCategory->from)*$amountCategory->percentCharge + $amountCategory->euro;
				
				$percentageChargeAmountEuro = $percentageChargeAmountEuro + $currentCharge;
				
				writeToFile(date("Y-m-d H:i:s"). ": addPercentageChargeAmountEuro(). adding for category[$amountCategory->from,$amountCategory->to] charge=$currentCharge \n", LOG_LEVEL_INFO);	
				//$debugNotes[] = "INTERMEDIATE ADDITIONAL CHARGE FOR ".$coverageTypeName."=".($amountCategory->to-$amountCategory->from)*$amountCategory->percentCharge."<br />";
			}
							
		}
						
		//$debugNotes[] =  "TOTAL CHARGE FOR ".$coverageTypeName."=".$percentageChargeAmountEuro."<br />";
		
		/* add the minimum amount to the cost. Only applies for FIRE AND THEFT */
		if( $coverageType->minimumChargeEuro>=$percentageChargeAmountEuro and
			$coverageTypeName==$COVERAGE_TYPE_FIRE_AND_THIFT )
		{
			$quoteCost = $quoteCost + $coverageType->minimumChargeEuro;
			
		}
		/* add the percentageChargeAmountEuro to the cost.
		Applies for FIRE AND THEFT, AND COMPREHENSIVE
		For Minerva, we use the minimum value when we apply the discounts, not here.*/
		else 
		{
			$quoteCost = $quoteCost + $percentageChargeAmountEuro;
		}
		
		writeToFile(date("Y-m-d H:i:s"). ": addPercentageChargeAmountEuro(). percentageChargeAmountEuro=$percentageChargeAmountEuro, quoteCost=$quoteCost \n", LOG_LEVEL_INFO);
                
		/* At this point the quoteCost is equal to the basicCoverageCost.
		The basicCoverageCost should include also the charge for the value of the vehicle
		for the case of FIRE AND THEFT and COMPREHENSIVE */
		$basicCoverageCost = $quoteCost;
		$processingInfo->basicCoverageCost = $basicCoverageCost;
		
		//$debugNotes[] = "COST=".$quoteCost."<br />";
						
	}
	/* User requested this coverage, but it is not offered */
	else
	{
		//$debugNotes[] =  $coverageTypeName." is not provided for this vehicle type.<br />";
		$userOutputNotes[] = $coverageTypeName." is not provided for this vehicle type";
		//$debugNotes[] = "COST=".$quoteCost."<br />";
		$processingInfo->canProvideOnlineQuote = $NO_CONSTANT;
		$processingInfo->reasonsWeCannotProvideOnlineQuote[] = "Coverage type: '".$coverageTypeName."'  IS NOT OFFERED BY OUR SYSTEM YET.";
		
		writeToFile(date("Y-m-d H:i:s"). ": CANNOT ADD OFFER BECAUSE : Coverage type: '".$coverageTypeName."'  IS NOT OFFERED BY OUR SYSTEM YET \n", LOG_LEVEL_INFO);	
	}

	writeToFile(date("Y-m-d H:i:s"). ": addPercentageChargeAmountEuro() EXIT \n", LOG_LEVEL_INFO);
}//function addPercentageChargeAmountEuro





   
?>