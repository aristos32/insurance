<?php
//store new quote in database

$i = 0;
//foreach($_SESSION['processingInfoArray'] as $info)
//	$info->printData();

$processingInfoPosition = @$_POST['processingInfoPosition'];
	
//Step 1 : find selectedProcessingInfo by matching the position in the array
foreach($_SESSION['processingInfoArray'] as $companyProcessingInfo)
{
	//echo "i = $i <br> ";
	if($i==$processingInfoPosition)
	{
		$selectedProcessingInfo = $companyProcessingInfo;
		break;
	}
	
	$i = $i + 1;
}

/*Step 2 - Store the quote information in the database.
Quote is stored in database, even when we cannot provide online. */
$_SESSION['quoteId'] = storeQuoteInDatabase($_SESSION['quoteInfo']);


//new quote is created successfully
if( $_SESSION['quoteId'] > 0 ){
	
	/*Step 3 - Send Automated emails to administrator and client.
	Send the client email, if any */
	sendAutomatedEmailForNewQuote($_SESSION['quoteInfo']->email);

	/*Step 4 - Present the Quote to the client */
	include ($_SESSION['globalFilesLocation']."/quotation/motor/vehicleQuote/quotePresentation.php");
	//echo '<br>'.$_SESSION['quoteId'].'<br>';
}
//error happenend
else
{
	?>
	Error happened. Duplicate contract Id exists in database.

	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="newContractForm" method="POST" style="display: none;">
	<input type="text" name="action" value="newContractForm" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('newContractForm').submit()">Try Again</a>
	<?php
}
?>