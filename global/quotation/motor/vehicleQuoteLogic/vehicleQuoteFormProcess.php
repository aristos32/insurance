<?php	

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');



global $YES_CONSTANT,$NO_CONSTANT;

/*Global Variables */	
$userOutputNotes = array(); /* holds all the notes to be provided to the user */
$quoteCost = 0;/*holds the final cost*/
$localVehicleType;
$basicCoverageCost =0;/*holds the basic coverage cost*/
$processingInfo = new processingInfo(); /* all information gathered/calculated during the processing of the user request */

if(isset($_POST['firstName']))
	$_SESSION['firstName'] = @$_POST['firstName'];
if(isset($_POST['lastName']))
	$_SESSION['lastName'] = @$_POST['lastName'];
if(isset($_POST['ownerProfession']))
	$_SESSION['ownerProfession'] = @$_POST['ownerProfession'];
if(isset($_POST['ownerBirthDate']))
	$_SESSION['ownerBirthDate'] = @date("Y-m-d", strtotime($_POST['ownerBirthDate']));
	
//echo $_SESSION['ownerBirthDate']."<br>";
/*
if(isset($_POST['ownerBirthDate']))
{
	$_SESSION['ownerBirthDateYear'] = substr(@$_POST['ownerBirthDate'], 0, 4);
	$_SESSION['ownerBirthDateMonth'] = substr(@$_POST['ownerBirthDate'], 5, 2);
	$_SESSION['ownerBirthDateDay'] = substr(@$_POST['ownerBirthDate'], 8, 2);
}*/


if(isset($_POST['ownerEthnicity']))
	$_SESSION['ownerEthnicity'] = @$_POST['ownerEthnicity'];
if(isset($_POST['licenseType']))
	$_SESSION['licenseType'] = @$_POST['licenseType'];
if(isset($_POST['licenseCountry']))
	$_SESSION['licenseCountry'] = @$_POST['licenseCountry'];
if(isset($_POST['licenseDate']))
	$_SESSION['licenseDate'] = @date("Y-m-d", strtotime($_POST['licenseDate']));
/*
if(isset($_POST['licenseDate']))
{
	$_SESSION['licenseDateYear'] = substr(@$_POST['licenseDate'], 0, 4);
	$_SESSION['licenseDateMonth'] = substr(@$_POST['licenseDate'], 5, 2);
	$_SESSION['licenseDateDay'] = substr(@$_POST['licenseDate'], 8, 2);
}*/

//driving experience
if(isset($_POST['hasPreviousInsurance']))
	$_SESSION['hasPreviousInsurance'] = @$_POST['hasPreviousInsurance'];
if(isset($_POST['countryOfInsurance']))
	$_SESSION['countryOfInsurance'] = @$_POST['countryOfInsurance'];
if(isset($_POST['insuranceCompany']))
	$_SESSION['insuranceCompany'] = @$_POST['insuranceCompany'];
if(isset($_POST['yearsOfExperience']))
	$_SESSION['yearsOfExperience'] = @$_POST['yearsOfExperience'];
	
if(isset($_POST['vehicleType']))
	$_SESSION['vehicleType'] = @$_POST['vehicleType'];
if(isset($_POST['cubicCapacity']))
	$_SESSION['cubicCapacity'] = @$_POST['cubicCapacity'];
if(isset($_POST['vehicleManufacturedYear']))
	$_SESSION['vehicleManufacturedYear'] = @$_POST['vehicleManufacturedYear'];
if(isset($_POST['steeringWheelSide']))
	$_SESSION['steeringWheelSide'] = @$_POST['steeringWheelSide'];					
if(isset($_POST['isSportsModel']))
	$_SESSION['isSportsModel'] = @$_POST['isSportsModel'];
if(isset($_POST['vehicleDesign']))
	$_SESSION['vehicleDesign'] = @$_POST['vehicleDesign'];
if(isset($_POST['isUsedForDeliveries']))
	$_SESSION['isUsedForDeliveries'] = @$_POST['isUsedForDeliveries'];					
if(isset($_POST['coverageType']))
	$_SESSION['coverageType'] = @$_POST['coverageType'];
if(isset($_POST['insuranceType']))
	$_SESSION['insuranceType'] = @$_POST['insuranceType'];
if(isset($_POST['hasDisability']))
	$_SESSION['hasDisability'] = @$_POST['hasDisability'];
if(isset($_POST['maxPenaltyPointsOfAnyDriverDuringLastThreeYears']))
	$_SESSION['maxPenaltyPointsOfAnyDriverDuringLastThreeYears'] = @$_POST['maxPenaltyPointsOfAnyDriverDuringLastThreeYears'];					
if(isset($_POST['numberOfAccidentsOfAnyDriverDuringLastThreeYears']))
	$_SESSION['numberOfAccidentsOfAnyDriverDuringLastThreeYears'] = @$_POST['numberOfAccidentsOfAnyDriverDuringLastThreeYears'];
if(isset($_POST['addAdditionalDriversQuestion']))
	$_SESSION['addAdditionalDriversQuestion'] = @$_POST['addAdditionalDriversQuestion'];
if(isset($_POST['vehicleModel']))
	$_SESSION['vehicleModel'] = @$_POST['vehicleModel'];
if(isset($_POST['vehicleMake']))
	$_SESSION['vehicleMake'] = @$_POST['vehicleMake'];
if(isset($_POST['offers']))
	$_SESSION['offers'] = @$_POST['offers'];
if(isset($_POST['stateId']))
	$_SESSION['stateId'] = @$_POST['stateId'];
if(isset($_POST['telephone']))
	$_SESSION['telephone'] = @$_POST['telephone'];
if(isset($_POST['cellphone']))
	$_SESSION['cellphone'] = @$_POST['cellphone'];
if(isset($_POST['regNumber']))
	$_SESSION['regNumber'] = @$_POST['regNumber'];
if(isset($_POST['isTaxFree']))
	$_SESSION['isTaxFree'] = @$_POST['isTaxFree'];

/*get all needed information from the session*/
$quoteInfo = new quotationInfo();
$quoteInfo->contractType = "INSURANCE";//type of sale is insurance
$quoteInfo->email = $_POST['proposerEmail'];
$quoteInfo->firstName = $_SESSION['firstName'];
$quoteInfo->lastName = $_SESSION['lastName'];
$quoteInfo->stateId = $_SESSION['stateId'];
$quoteInfo->oldStateId = $_SESSION['stateId'];
$quoteInfo->telephone = $_SESSION['telephone'];
$quoteInfo->cellphone = $_SESSION['cellphone'];
$quoteInfo->profession = $_SESSION['ownerProfession'];
$quoteInfo->birthDate = $_SESSION['ownerBirthDate'];
$quoteInfo->licenseDate = $_SESSION['licenseDate'];
$quoteInfo->licenseType = $_SESSION['licenseType'];
$quoteInfo->countryOfBirth = $_SESSION['ownerEthnicity'];
$quoteInfo->licenseCountry = $_SESSION['licenseCountry'];
 
$quoteInfo->hasPreviousInsurance = $_SESSION['hasPreviousInsurance'];
$quoteInfo->countryOfInsurance = $_SESSION['countryOfInsurance'];
$quoteInfo->insuranceCompany = $_SESSION['insuranceCompany'];
$quoteInfo->yearsOfExperience = $_SESSION['yearsOfExperience'];
$quoteInfo->regNumber = $_SESSION['regNumber'];
$quoteInfo->vehicleMake = $_SESSION['vehicleMake'];
$quoteInfo->vehicleType = $_SESSION['vehicleType'];
$quoteInfo->vehicleModel = $_SESSION['vehicleModel'];
$quoteInfo->cubicCapacity = $_SESSION['cubicCapacity'];
$quoteInfo->vehicleManufacturedYear = $_SESSION['vehicleManufacturedYear'];
$quoteInfo->steeringWheelSide = $_SESSION['steeringWheelSide'];
$quoteInfo->isSportsModel = $_SESSION['isSportsModel'];
$quoteInfo->vehicleDesign = $_SESSION['vehicleDesign'];
$quoteInfo->isUsedForDeliveries = $_SESSION['isUsedForDeliveries'];
$quoteInfo->coverageType = $_SESSION['coverageType'];
$quoteInfo->hasDisability = $_SESSION['hasDisability'];
$quoteInfo->maxPenaltyPointsOfAnyDriverDuringLastThreeYears = $_SESSION['maxPenaltyPointsOfAnyDriverDuringLastThreeYears'];
$quoteInfo->numberOfAccidentsOfAnyDriverDuringLastThreeYears = $_SESSION['numberOfAccidentsOfAnyDriverDuringLastThreeYears'];
$quoteInfo->addAdditionalDriversQuestion = $_SESSION['addAdditionalDriversQuestion'];
$quoteInfo->isTaxFree = $_SESSION['isTaxFree'];
//$quoteInfo->printData();

/* coverageAmount exists only when coverageType is not THIRD PARTY */
if($quoteInfo->coverageType != $COVERAGE_TYPE_THIRD_PARTY )
{
	$_SESSION['coverageAmount'] = @$_POST['coverageAmount'];
	$quoteInfo->coverageAmount = $_SESSION['coverageAmount'];	
}

/* exist only when coverageType is COVERAGE_TYPE_COMPREHENSIVE */
if($quoteInfo->coverageType == $COVERAGE_TYPE_COMPREHENSIVE )
{
	$_SESSION['namedDriversNumber'] = @$_POST['namedDriversNumber'];
	$_SESSION['additionalExcess'] = @$_POST['additionalExcess'];
	$_SESSION['noClaimDiscountYears'] = @$_POST['noClaimDiscountYears'];
	$quoteInfo->namedDriversNumber = $_SESSION['namedDriversNumber'];
	$quoteInfo->additionalExcess = $_SESSION['additionalExcess'];
	$quoteInfo->noClaimDiscountYears = $_SESSION['noClaimDiscountYears'];
	
	//DISCOUNTS 0=MAX ALLOWED=No Discount
	if($quoteInfo->namedDriversNumber!=0)
	{
		$localDiscountInQuotation = new quotation_discounts();
		$localDiscountInQuotation->code = 'namedDriversNumberTab';
		$localDiscountInQuotation->parameters[] = $quoteInfo->namedDriversNumber;
		$quoteInfo->discountsInQuotationArray[] = $localDiscountInQuotation;
	}
	if($quoteInfo->additionalExcess!=0)
	{
		$localDiscountInQuotation = new quotation_discounts();
		$localDiscountInQuotation->code = 'additionalExcessTab';
		$localDiscountInQuotation->parameters[] = $quoteInfo->additionalExcess;
		$quoteInfo->discountsInQuotationArray[] = $localDiscountInQuotation;
	}
	
	if($quoteInfo->noClaimDiscountYears!=0)
	{
		$localDiscountInQuotation = new quotation_discounts();
		$localDiscountInQuotation->code = 'noClaimDiscount';
		$localDiscountInQuotation->parameters[] = $quoteInfo->noClaimDiscountYears;
		$quoteInfo->discountsInQuotationArray[] = $localDiscountInQuotation;
	}
}

/*
foreach($quoteInfo->discountsInQuotationArray as $eachDiscount)
{
	$eachDiscount->printData();
}*/

$i = 0;
$claims = array();

/* case $addAdditionalDriversQuestion='YES' */
if( $quoteInfo->addAdditionalDriversQuestion==$YES_CONSTANT )
{
	$_SESSION['additionalDriversLicenseCountry'] = @$_POST['additionalDriversLicenseCountry'];
	$_SESSION['additionalDriversEthnicity'] = @$_POST['additionalDriversEthnicity'];
	$_SESSION['additionalDriversLicenseType'] = @$_POST['additionalDriversLicenseType'];
	$_SESSION['normalDrivingLicenseTotalYearsForAdditionalDrivers'] = @$_POST['normalDrivingLicenseTotalYearsForAdditionalDrivers'];
	$_SESSION['ageOfYoungestDriver'] = @$_POST['ageOfYoungestDriver'];
	$_SESSION['ageOfOldestDriver'] = @$_POST['ageOfOldestDriver'];
	$quoteInfo->additionalDriversLicenseCountry = $_SESSION['additionalDriversLicenseCountry'];
	$quoteInfo->additionalDriversEthnicity = $_SESSION['additionalDriversEthnicity'];
	$quoteInfo->additionalDriversLicenseType = $_SESSION['additionalDriversLicenseType'];//for additional driver
	$quoteInfo->normalDrivingLicenseTotalYearsForAdditionalDrivers = $_SESSION['normalDrivingLicenseTotalYearsForAdditionalDrivers'];
	$quoteInfo->ageOfYoungestDriver = $_SESSION['ageOfYoungestDriver'];
	$quoteInfo->ageOfOldestDriver = $_SESSION['ageOfOldestDriver'];		
}
	
//Store the accident amounts
if(	$quoteInfo->numberOfAccidentsOfAnyDriverDuringLastThreeYears > 0 )
{
	//$accidents;
	
	for($i=0; $i<$_SESSION['numberOfAccidentsOfAnyDriverDuringLastThreeYears']; $i++)
	{
		$var = "claim".($i+1);//string concatenation
		
		$localClaimClass = new claim();
		$localClaimClass->number = $i+1;
		$localClaimClass->amount = $_POST[$var];
		$localClaimClass->stateId = $quoteInfo->stateId;
		
		$var = "claim".($i+1)."Date";//string concatenation
		//echo "Var name is=".$var."<br>";
		
		$localClaimClass->claimDate = $_POST[$var];
		//echo "claimdate=".$localClaimClass->claimDate."<br>";
		
		$var = "claim".($i+1)."Date"."Year";//string concatenation
		$_SESSION[$var] = substr($localClaimClass->claimDate, 0, 4);
		$var = "claim".($i+1)."Date"."Month";//string concatenation
		$_SESSION[$var] = substr($localClaimClass->claimDate, 5, 2);
		$var = "claim".($i+1)."Date"."Day";//string concatenation
		$_SESSION[$var] = substr($localClaimClass->claimDate, 8, 2);

		/* add to array */
		$claims[] = $localClaimClass;
		
	}
	
	$_SESSION['claims'] = $claims;
}

if($quoteInfo->numberOfAccidentsOfAnyDriverDuringLastThreeYears>0)
{
	foreach($_SESSION['claims'] as $claim)
	{
		/* add to array */
		$quoteInfo->claims[] = $claim;
	}
	
}

/*get all needed information from the form*/
$_SESSION['userInfo'] = @$_POST['userInfo'];
if(isset($_POST['userInfo']))
	$quoteInfo->userInfo = @$_POST['userInfo'];
else
	$quoteInfo->userInfo = $_SESSION['quoteInfo']->userInfo;
	
//$quoteInfo->userInfo = @$_POST['userInfo'];
$quoteInfo->offerTypeSelected = @$_SESSION['offers'];

// individual benefits selected by the user - otherwise this is empty and is filled later in ...
if($quoteInfo->coverageType == $COVERAGE_TYPE_THIRD_PARTY && $quoteInfo->offerTypeSelected==$OFFER_TYPE_THIRD_PARTY)
{
	$quoteInfo->coveragesInPolicyQuotationArray = realAllCoveragesSendFromForm($_SESSION['insuranceType'], $quoteInfo->coverageType);
	//foreach($quoteInfo->coveragesInPolicyQuotationArray as $coverage)
	//	$coverage->printData();
}


//ADD ALL ADDITIONAL CHARGES FACTORS
//tax free
if($quoteInfo->isTaxFree==$YES_CONSTANT)
{
	$localChargeInQuotation = new chargesInQuotation();
	$localChargeInQuotation->code = 'isTaxFree';
	$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
}

//vehicle age
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'vehicleAge';
$localChargeInQuotation->parameters[] = date("Y") - $quoteInfo->vehicleManufacturedYear;//calculate vehicle age
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;


//license years
//get the age of the owner		
$ownerLicenseYears = getYearDifference($quoteInfo->licenseDate);//$_SESSION['licenseDateYear'];
$additionalDriversLicenseYears = $quoteInfo->normalDrivingLicenseTotalYearsForAdditionalDrivers;	
$minimumLicenseYears = getMinimum($ownerLicenseYears, $additionalDriversLicenseYears);
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'namedDriverLicenseYears';
$localChargeInQuotation->parameters[] = $minimumLicenseYears;
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
//echo "MINIMUM LICENSE YEARS=".$minimumLicenseYears."<BR>";


//add charges only for proposer
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'countryOfLicense';
$localChargeInQuotation->categoryType = $quoteInfo->licenseCountry;
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
	
//$localChargeInQuotation->printData();


//additional drivers license country - add charge only if this is different than the proposer license
if( $quoteInfo->addAdditionalDriversQuestion==$YES_CONSTANT )
{
	
	//different license types for proposer and additional drivers
	//only then add charge for second time
	if($quoteInfo->licenseCountry != $quoteInfo->additionalDriversLicenseCountry )
	{
		$localChargeInQuotation = new chargesInQuotation();
		$localChargeInQuotation->code = 'additionalDriversCountryOfLicense';
		$localChargeInQuotation->categoryType = $quoteInfo->additionalDriversLicenseCountry;
		$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
		
		//$localChargeInQuotation->printData();
	}
}
	
// owner ethnicity 
$ownerEthnicity;
$licenseCountry;
//Step 1: set the ethnicity in one of the appropriate categories as found in the xml.
// for ethnicity use same categories as for licenses
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'ownerEthnicity';
$localChargeInQuotation->categoryType = $quoteInfo->countryOfBirth;
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;

//additional drivers license type - add only when it differs from the proposer license
if( $quoteInfo->addAdditionalDriversQuestion==$YES_CONSTANT )
{
	
	if($quoteInfo->countryOfBirth != $quoteInfo->additionalDriversEthnicity )
	{
		
		$localChargeInQuotation = new chargesInQuotation();
		$localChargeInQuotation->code = 'additionalDriversEthnicity';
		$localChargeInQuotation->categoryType = $quoteInfo->additionalDriversEthnicity;
		$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
	}
}

//previous insurance - need to check other driving experience fields
if($quoteInfo->hasPreviousInsurance==$NO_CONSTANT)
{
	$localChargeInQuotation = new chargesInQuotation();
	$localChargeInQuotation->code = 'noPreviousInsurance';
	$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
}

//steering wheel
if($quoteInfo->steeringWheelSide==$LEFT_CONSTANT)
{
	$localChargeInQuotation = new chargesInQuotation();
	$localChargeInQuotation->code = 'vehicleSteeringWheelSideLeft';
	$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
}

//sports model
if($quoteInfo->isSportsModel==$YES_CONSTANT)
{
	$localChargeInQuotation = new chargesInQuotation();
	$localChargeInQuotation->code = 'vehicleSportsModel';
	$localChargeInQuotation->categoryType = $quoteInfo->coverageType;
	$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
}

//vehicle design
if($quoteInfo->vehicleDesign==$VEHICLE_DESIGN_SOFT_TOP || $quoteInfo->vehicleDesign==$VEHICLE_DESIGN_CONVERTIBLE)
{
	$localChargeInQuotation = new chargesInQuotation();
	$localChargeInQuotation->code = 'vehicleSoftTop';
	$localChargeInQuotation->categoryType = $quoteInfo->coverageType;
	$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
}

//owner license type
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'licenseType';
$localChargeInQuotation->categoryType = $quoteInfo->licenseType;
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;

//additional drivers license type - add only when it differs from the proposer license
if( $quoteInfo->addAdditionalDriversQuestion==$YES_CONSTANT )
{
	if($quoteInfo->additionalDriversLicenseType != $quoteInfo->licenseType )
	{
		
		$localChargeInQuotation = new chargesInQuotation();
		$localChargeInQuotation->code = 'namedDriverLicense';
		$localChargeInQuotation->categoryType = $quoteInfo->additionalDriversLicenseType;
		$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
	}
}


//used for deliveries
if($quoteInfo->isUsedForDeliveries==$YES_CONSTANT)
{
	$localChargeInQuotation = new chargesInQuotation();
	$localChargeInQuotation->code = 'vehicleUsedForDeliveries';
	$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
}	

//age charges
//get the age of the owner		
$ownerAge = getYearDifference($quoteInfo->birthDate);
$minimumAge = getMinimum($ownerAge, $quoteInfo->ageOfYoungestDriver);
$maximumAge = getMaximum($ownerAge, $quoteInfo->ageOfOldestDriver);

//minimum age charges
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'youngAgeCharges';
$localChargeInQuotation->parameters[] = $minimumAge;
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;

//maximum age charges
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'oldAgeCharges';
$localChargeInQuotation->parameters[] = $maximumAge;
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;

//penalty point charges
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'penaltyPointsDuringLast3Years';
$localChargeInQuotation->parameters[] = $quoteInfo->maxPenaltyPointsOfAnyDriverDuringLastThreeYears;
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;

//insurance claims
$localChargeInQuotation = new chargesInQuotation();
$localChargeInQuotation->code = 'insuranceClaimsDuringLast3Years';
$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;

//disability charges
if($quoteInfo->hasDisability==$YES_CONSTANT)
{
	$localChargeInQuotation = new chargesInQuotation();
	$localChargeInQuotation->code = 'driverHasDisability';
	$quoteInfo->additionalChargesInPolicyQuotationArray[] = $localChargeInQuotation;
}	

//Step 1 - Gel list of available XML files
$listOfFiles = getListOfFiles($_SESSION['clientFilesLocation']."/XMLFiles/", "");
//print_r($listOfFiles);

//Step 2 - Fill processing info for all company xmls
$processingInfoArray = array();
$temporaryProcessingInfo;//store variable to decide if needs to be added in the processingInfoArray. It is added, only when quotation is available.
unset($_SESSION['processingInfoArray']);

//search all the files
foreach($listOfFiles as $companyXmlFile)
{
	//echo "package";
	writeToFile(date("Y-m-d H:i:s"). ": found file: $companyXmlFile  \n", LOG_LEVEL_INFO);
	/* Read the company XML file */
	//$basicCoverageInfo = readValuesFromCompanyXml($_SESSION['clientFilesLocation']."/XMLFiles/".$companyXmlFile);
	$basicCoverageInfo = readValuesFromCompanyXmlNew($_SESSION['clientFilesLocation']."/XMLFiles/", $companyXmlFile);
	//$basicCoverageInfo->printData();

	/* GET THE VEHICLE TYPE, based on user choice. We select the first found, because we have ONLY one <vehicleType> for each type  */
	foreach($basicCoverageInfo->vehicleTypes as $vehicleType)
	{
		//echo "quoteInfo->vehicleType=".$quoteInfo->vehicleType.", vehicleType->value=".$vehicleType->value."<br />";
		if($quoteInfo->vehicleType==$vehicleType->value)
		{
			$localVehicleType = $vehicleType; //set the selected vehicle type
			//$localVehicleType->printData();
			//echo "VEHICLETYPE=".$localVehicleType->name."<br />";
			break;
		}			
	}

	//$localVehicleType->printData();
	
	//add individual benefits. Applies for third party only
	if($quoteInfo->offerTypeSelected==$OFFER_TYPE_THIRD_PARTY)
	{
		writeToFile(date("Y-m-d H:i:s"). ": user selected individual benefits \n", LOG_LEVEL_INFO);
                
		//store variable to decide if needs to be added in the processingInfoArray. It is added, only when quotation is available.
		$temporaryProcessingInfo = calculateQuoteProcessingInfo($quoteInfo, $companyXmlFile, $basicCoverageInfo, null);
		
		//add coverages as read from user input
		//3/9/2012 REMOVED ARISTOSA
		//foreach($quoteInfo->coveragesInPolicyQuotationArray as $eachCoverage)
			//$temporaryProcessingInfo->coveragesInPolicyQuotationArray[] = convertXMLtoDatabase($eachCoverage);
		
		//all coverages requested by the customer
		$temporaryProcessingInfo->coveragesInPolicyQuotationArray = $quoteInfo->coveragesInPolicyQuotationArray;
		
		//all coverages read from the optional coverages XML
		$temporaryProcessingInfo->coveragesInXMLArray = $localVehicleType->optionalCoveragesFromXMLArray;
		
		//foreach($temporaryProcessingInfo->coveragesInXMLArray as $eachCoverage)
		//	$eachCoverage->printData();
			
		//if($temporaryProcessingInfo->canProvideOnlineQuote == $YES_CONSTANT)
			$processingInfoArray[] = $temporaryProcessingInfo;
		
		//reset variables
		resetGlobalVariables();
	}
	//package option selected. We will check for all available packages, as in <inclusiveBenefits> in xml.
	//applies for Comprehensive and Third Party when selecting Packages.
	else
	{
		writeToFile(date("Y-m-d H:i:s"). ": user selected package benefits \n", LOG_LEVEL_INFO);
                
		//loop through all <inclusiveBenetits> of current <vehicleType>
		foreach($localVehicleType->inclusiveBenefits as $inclusiveBenefit )
		{
			//$inclusiveBenefit->printData();
			
			writeToFile(date("Y-m-d H:i:s"). ": Package: coverageType=$inclusiveBenefit->coverageTypeOfOffer, quoteInfo coverageType=$quoteInfo->coverageType \n", LOG_LEVEL_INFO);
                        
			//add only available offers
			if($inclusiveBenefit->available == $YES_CONSTANT )
			{
				writeToFile(date("Y-m-d H:i:s"). ": AVAILABLE package found with coverageType=$inclusiveBenefit->coverageTypeOfOffer, quoteInfo coverageType=$quoteInfo->coverageType \n", LOG_LEVEL_INFO);
                                
				//$inclusiveBenefit->printData();
				//add only offers for Third Party
				if($inclusiveBenefit->coverageTypeOfOffer==$quoteInfo->coverageType)
				{	
                                        writeToFile(date("Y-m-d H:i:s"). ": matching coverage type found : $quoteInfo->coverageType \n", LOG_LEVEL_INFO);
                                        
					//store variable to decide if needs to be added in the processingInfoArray. It is added, only when quotation is available.
					$temporaryProcessingInfo = calculateQuoteProcessingInfo($quoteInfo, $companyXmlFile, $basicCoverageInfo, $inclusiveBenefit);
					//add coverages for offer, as read from model.xml file
					foreach($inclusiveBenefit->optionalCoveragesFromXMLArray as $eachCoverage)
					{
						//convert one structure to other
						$temporaryProcessingInfo->coveragesInPolicyQuotationArray[] = convertXMLtoDatabase($eachCoverage);
					}
					
					//$temporaryProcessingInfo->coveragesInPolicyQuotationArray = $inclusiveBenefit->optionalCoveragesFromXMLArray;
					//foreach($temporaryProcessingInfo->coveragesInPolicyQuotationArray as $eachCoverage)
					//{
						//convert one structure to other
					//	$eachCoverage->printData();
						
					//}
					
					//$temporaryProcessingInfo->printData();
					
					if($temporaryProcessingInfo->canProvideOnlineQuote == $YES_CONSTANT)
					{
						writeToFile(date("Y-m-d H:i:s"). ": can provide an online quotation \n", LOG_LEVEL_INFO);
                                                
						$temporaryProcessingInfo->inclusiveBenefit = $inclusiveBenefit;
						$processingInfoArray[] = $temporaryProcessingInfo;
						//$temporaryProcessingInfo->printData();
						//foreach($inclusiveBenefit->optionalCoveragesFromXMLArray as $eachCoverage)
						//	{
								//convert one structure to other
						//		$eachCoverage->printData();
						//	}
					}
					else
					{
						writeToFile(date("Y-m-d H:i:s"). ": we cannot provide an online quotation \n", LOG_LEVEL_INFO);
                                                
						$temporaryProcessingInfo->inclusiveBenefit = $inclusiveBenefit;
						$processingInfoArray[] = $temporaryProcessingInfo;					
					}
				}
				//reset variables
				resetGlobalVariables();
			}
		}
	}
	
}

$_SESSION['processingInfoArray'] = $processingInfoArray;
$_SESSION['quoteInfo'] = $quoteInfo;

//Step 3 - Display the list of all companies info
displayListOfAllCompanyOffers();



/* display the list of offers from all companies. Quotation is not stored in database yet.

*/
function displayListOfAllCompanyOffers()
{
//global $processingInfoArray;//processingInfoArray : contains processing info for all companies
global $company, $plan, $quotationCost, $covers, $select, $YES_CONSTANT, $runFromGlobalLocation, $runFromGlobalLocationOffice;

//show table only when some offers were found
if(count($_SESSION['processingInfoArray']) > 0 )
{
?>

<table width="100%" border="0" cellspacing="1" cellpadding="3">
	<tr>
		<td><b><?php echo $_SESSION['company']; ?></td>
		<td><b><?php echo $_SESSION['plan']; ?></td>
		<td><b><?php echo $_SESSION['quotationCost']; ?></td>
		<td><b><?php echo $_SESSION['covers']; ?></td>
		<td><b><?php echo $_SESSION['select']; ?></td>
	</tr>
	
	<?php
	$i = 0;
	foreach($_SESSION['processingInfoArray'] as $companyProcessingInfo)
	{
		//don't show offers that are not available
		//if($companyProcessingInfo->canProvideOnlineQuote == $YES_CONSTANT )
		//{
			?>
			<tr>
				<td><?php echo $companyProcessingInfo->insuranceCompanyOfferingQuote; ?></td>
				<td><?php echo $companyProcessingInfo->selectedOfferName; ?></td>
				<td><?php if($companyProcessingInfo->canProvideOnlineQuote == $YES_CONSTANT ) echo $companyProcessingInfo->finalCost; else echo "N/A"; ?></td>
				
				<td><!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
					<!-- Show the benefits included in this quotation -->
					<form action="<?php if($runFromGlobalLocation==true) echo "./quotation.php"; else echo "./index.php"; ?>" id="<?php echo "ShowBenefits_".$i;?>" method="post" style="display: none;" target="_blank">
						<input type="text" name="action" value="showBenefits" />
						<input type="text" name="processingInfoPosition" value=<?php echo $i;?> />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo "ShowBenefits_".$i;?>').submit()"><?php echo $_SESSION['covers']; ?></a>
				</td>
				
				<td><!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
					<!-- CREATE quote and store in database -->
					<form action="<?php if($runFromGlobalLocation==true && $runFromGlobalLocationOffice==true) echo "./office.php"; else if($runFromGlobalLocation==true) echo "./quotation.php"; else echo "./index.php"; ?>" id="<?php echo "Create_".$i;?>" method="post" style="display: none;" >
						<input type="text" name="action" value="createQuote" />
						<input type="text" name="processingInfoPosition" value=<?php echo $i;?> />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo "Create_".$i;?>').submit()"><?php echo $_SESSION['select']; ?></a>
				</td>
			</tr>
		<?php		
		//}
		$i = $i + 1;
	}
	?>
</table>

<?php
}
else
{
	?>
	No plans were found. Try getting a 
		<!-- CREATE quote and store in database -->
		<form action="<?php if($runFromGlobalLocation==true) echo "./quotation.php"; else echo "./index.php"; ?>" id="carQuoteForm" method="post" style="display: none;" >
			<input type="text" name="action" value="carQuoteForm" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('carQuoteForm').submit()">new quote</a>
	<?php
}
}



/* 	1. Reads the XML file
2. Calculates the Quote

$quoteInfo : input from the Client
$companyXmlFile : company XML file to read
$basicCoverageInfo : the info read from the company xml
$inclusiveBenefit : one of the vehicle->inclusive benefits tags READ from company XML file.
*/
function calculateQuoteProcessingInfo($quoteInfo, $companyXmlFile, $basicCoverageInfo, $inclusiveBenefit)
{
global $processingInfo;

writeToFile(date("Y-m-d H:i:s"). ": calculateQuoteProcessingInfo() ENTER \n", LOG_LEVEL_INFO);

/* SET INSURANCE COMPANY */
//$processingInfo->insuranceCompanyOfferingQuote = substr($companyXmlFile, 0, strpos($companyXmlFile, "."));
$processingInfo->insuranceCompanyOfferingQuote = $basicCoverageInfo->companyName;

//$processingInfo->inclusiveBenefit = $inclusiveBenefit;

/* Process the input from PHP Form and XML file */
addAllCharges($quoteInfo, $basicCoverageInfo, $inclusiveBenefit); 

writeToFile(date("Y-m-d H:i:s"). ": calculateQuoteProcessingInfo() EXIT \n", LOG_LEVEL_INFO);
return $processingInfo;


}


/* reset all global variables, between calls to different insurance companies */
function resetGlobalVariables()
{
global $userOutputNotes;
global $quoteCost;
global $localVehicleType;
global $basicCoverageCost;
global $processingInfo;
global $temporaryProcessingInfo;


$userOutputNotes = array(); /* holds all the notes to be provided to the user */
$quoteCost = 0;/*holds the final cost*/
$localVehicleType;
$basicCoverageCost =0;/*holds the basic coverage cost*/
$processingInfo = new processingInfo();
$temporaryProcessingInfo = new processingInfo();
}


?>
