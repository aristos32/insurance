<script type="text/javascript" src="dropdowntabfiles/dropdowntabs.js">

/***********************************************
* Drop Down Tabs Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<!-- CSS for Drop Down Tabs Menu of quotation.php -->
<link rel="stylesheet" type="text/css" href="dropdowntabfiles/ddcolortabs.css" />

<div id="colortab" class="ddcolortabs">
	<ul>
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="motor" method="post" style="display: none;">
	<input type="text" name="action" value="motor" />
	</form>
	<li><a href="javascript:;" onclick="javascript: document.getElementById('motor').submit()"><span><?php echo $_SESSION['motorTab']; ?></span></a></li>
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="fire" method="post" style="display: none;">
	<input type="text" name="action" value="fire" />
	</form>
	<li><a href="javascript:;" onclick="javascript: document.getElementById('fire').submit()"><span><?php echo $_SESSION['fireTab']; ?></span></a></li>
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="employerLiability" method="post" style="display: none;">
	<input type="text" name="action" value="employerLiability" />
	</form>
	<li><a href="javascript:;" onclick="javascript: document.getElementById('employerLiability').submit()"><span><?php echo $_SESSION['employerLiabilityTab']; ?></span></a></li>
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="medical" method="post" style="display: none;">
	<input type="text" name="action" value="medical" />
	</form>
		
	<li><a href="javascript:;" rel="dropmenu3_a" title="Medical"><span><?php echo $_SESSION['medicalTab']; ?></span></a></li>
	
	<li><a href="javascript:;" rel="dropmenu1_a" title="Various"><span><?php echo $_SESSION['variousTab']; ?></span></a></li>	
	
	<?php
	//show only for administrators
	if(isset($_SESSION['login']) && $_SESSION['login']==true && isset($_SESSION['role']) and $_SESSION['role'] > $USER_ROLE_CUSTOMER)
	{
		?>
		<li><a href="javascript:;" rel="dropmenu2_a" title="Administrator"><span><?php echo $_SESSION['administratorTab']; ?></span></a></li>
		<?php
	}
	?>	
	</ul>
</div>
<?php
//when during logout, don't show this
if(isset($_SESSION['login']) && $_SESSION['login']==true && $_SESSION['action']!='logout')
{ ?>
	<?php echo $_SESSION['usernameTab'].":".$_SESSION['username'] ?>. 
  	<!-- send logout using POST in order for POST to work in office.php -->
  	<form style='display:inline;' name="logout" action="./quotation.php" method="POST" enctype="application/x-www-form-urlencoded">
  		<input type="hidden" name="action" value="logout">
  		<INPUT style='display:inline;' type="submit" class="button" value=<?php echo $_SESSION['logout']; ?> size="10" >
  	</form>
  	<?php
}
//SHOW LOGIN BUTTON
else
{?>
  	<!-- send logout using POST in order for POST to work in office.php -->
  	<form style='display:inline;' name="login" action="./quotation.php" method="POST" enctype="application/x-www-form-urlencoded">
  		<input type="hidden" name="action" value="loginForm">
  		<INPUT style='display:inline;' type="submit" class="button" value=<?php echo $_SESSION['loginTab']; ?> size="10" >
  	</form>
  	<?php
	
}
?>

<!--1st drop down menu -->                                                   
<div id="dropmenu1_a" class="dropmenudiv_a">

	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="transport" method="post" style="display: none;">
	<input type="text" name="action" value="transport" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('transport').submit()"><?php echo $_SESSION['transport'];?></a>
		
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="glasses" method="post" style="display: none;">
	<input type="text" name="action" value="glasses" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('glasses').submit()"><?php echo $_SESSION['glasses']; ?></a>

	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="theft" method="post" style="display: none;">
	<input type="text" name="action" value="theft" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('theft').submit()"><?php echo $_SESSION['theft']; ?></a>
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="personalAccidents" method="post" style="display: none;">
	<input type="text" name="action" value="personalAccidents" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('personalAccidents').submit()"><?php echo $_SESSION['personalAccidents']; ?></a>
		
</div>


<!--2nd drop down menu -->                                                
<div id="dropmenu2_a" class="dropmenudiv_a">
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="quotationFindMenu" method="post" style="display: none;">
	<input type="text" name="action" value="quotationFindMenu" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('quotationFindMenu').submit()"><?php echo $_SESSION['quotation']; ?></a>
	<?php
	if($_SESSION['role']>=$USER_ROLE_ADMINISTRATOR)
	{ 
		?>
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./quotation.php" id="userFindMenu" method="post" style="display: none;">
		<input type="text" name="action" value="userFindMenu" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('userFindMenu').submit()"><?php echo $_SESSION['usersTab']; ?></a>
		<?php
	}
	?>
	
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="yourAccount" method="post" style="display: none;">
		<input type="text" name="action" value="yourAccount" />
		<input type="text" name="searchClientName" value="<?php echo $_SESSION['clientName'];?>" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('yourAccount').submit()"><?php echo $_SESSION['yourAccount']; ?></a>

	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="xml" method="post" style="display: none;">
	<input type="text" name="action" value="xml" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('xml').submit()">XML</a>

	<?php
	if($_SESSION['role'] == $USER_ROLE_SUPER)
	{ 
		?>
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./quotation.php" id="superMenu" method="post" style="display: none;">
		<input type="text" name="action" value="superMenu" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('superMenu').submit()">SUPER</a>
		<?php
	}
	?>
	
</div>


<!--medical drop down menu -->                                                   
<div id="dropmenu3_a" class="dropmenudiv_a">


	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="medical" method="post" style="display: none;">
	<input type="text" name="action" value="medical" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('medical').submit()"><?php echo $_SESSION['medicalTab'];?></a>
		
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./quotation.php" id="alienMedical" method="post" style="display: none;">
	<input type="text" name="action" value="alienMedical" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('alienMedical').submit()"><?php echo $_SESSION['alienMedicalTab']; ?></a>
			
</div>


<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "auto")
</script>