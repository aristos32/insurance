<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$contracts = array();
//on language change, POST are not send. So, use ones in session. Keep in session the latest contract we search for.
if(isset($_POST['contractExpiryDate']))
	$_SESSION['contractExpiryDate'] = $_POST['contractExpiryDate'];
if(isset($_POST['producer']))
	$_SESSION['producer'] = $_POST['producer'];
//echo $_SESSION['contractExpiryDate'];

$parameterNameValueArray = array();

$date1 = $_POST['endYear']."-".$_POST['endMonth'];
$date1 = date("Y-m", strtotime($date1));
//echo $date1;

$parameterNameValue = new parameterNameValue();
$parameterNameValue->name = " date_FORMAT(s.endDate,'%Y-%m') ";
$parameterNameValue->value = substr($date1,0,7);
	
$parameterNameValueArray[] = $parameterNameValue;

$parameterNameValue = new parameterNameValue();
$parameterNameValue->name = ' s.producer ';
$parameterNameValue->value = $_SESSION['producer'];
	  
$parameterNameValueArray[] = $parameterNameValue;

$contracts = retrieveContractInfo($parameterNameValueArray, 1);
$_SESSION['contracts'] = $contracts;

//create a list of all the emails
$allEmails = '';
$allStateIds = '';

foreach($_SESSION['contracts'] as $eachContract)
{
	//we have several contracts for same client. so we need to take the client email only once.
	//1. email must have some value, not empty
	//2. email must not exist in the $allMails list already
	if($eachContract->owner->email != '' && strpos($allEmails,$eachContract->owner->email)==false )
	{
		$allEmails = $allEmails.",".$eachContract->owner->email;
		$allStateIds = $allStateIds.",".$eachContract->owner->stateId;
	}
}

include $_SESSION['globalFilesLocation']."/sale/displayContracts.php";

?>