<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

if(isset($_POST['producerInContractsWithRemainder']))
	$_SESSION['producerInContractsWithRemainder'] = $_POST['producerInContractsWithRemainder'];
	
$contracts = array();
//PROBLEM: on language change, everything is reloaded - need to improve this - TODO TODO
$contracts = retrieveAllContractsWithRemainder($_SESSION['producerInContractsWithRemainder']);

$_SESSION['contracts'] = $contracts;

//create a list of all the emails
$allEmails = '';
$allStateIds = '';

foreach($_SESSION['contracts'] as $eachContract)
{
	//we have several contracts for same client. so we need to take the client email only once.
	//1. email must have some value, not empty
	//2. email must not exist in the $allMails list already
	if($eachContract->owner->email != '' && strpos($allEmails,$eachContract->owner->email)==false )
	{
		$allEmails = $allEmails.",".$eachContract->owner->email;
		$allStateIds = $allStateIds.",".$eachContract->owner->stateId;
	}
}

include $_SESSION['globalFilesLocation']."/sale/displayContracts.php";

?>