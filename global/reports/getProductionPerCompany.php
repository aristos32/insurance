<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$totalNewRenewProduction = 0;
$totalCancelledProduction = 0;
$totalNewProduction = 0;
$totalClearProduction = 0;

//$yearValue = $_POST['year'];
if(isset($_POST['startYear']))
	$_SESSION['startYearValue'] = $_POST['startYear'];

//Retrieve all NEW or RENEW insurance group by company
$newRenewProductionArray = retrieveProductionInfo($_SESSION['startYearValue'], 0, "s.company");

//Retrieve all CANCEL insurances group by company
$cancelledProductionArray = retrieveProductionInfo($_SESSION['startYearValue'], 1, "s.company");

//Retrieve all NEW insurances group by company
$totalNewProduction = retrieveProductionInfo($_SESSION['startYearValue'], 2, "s.company");
//foreach($cancelledProductionArray as $eachCan)
//	$eachCan->printData();
	

//Retrieve all NEW or RENEW insurances group by associate
$associatesProductionArray = retrieveProductionInfo($_SESSION['startYearValue'], 0, "s.associate");

//Retrieve all NEW or RENEW insurances group by producer
$producersProductionArray = retrieveProductionInfo($_SESSION['startYearValue'], 0, "s.producer");

//Step 3 - Find Total Remaining Production = RENEW/RENEW - CANCEL
$totalClearProductionArray = array();

if(count($newRenewProductionArray)>0)
{?>
	<h2 align="center"><?php echo $_SESSION['production']." - ".$_SESSION['year'].": ".$_SESSION['startYearValue']; ?></h2>
	
	<table border=1>
	<tr align="center"> <th> <?php echo $_SESSION['insuranceCompanyTab']; ?> </th> <th> <?php echo $_SESSION['production']; ?> </th> <th> <?php echo $_SESSION['cancellations']; ?> </th> <th> <?php echo $_SESSION['remainder']; ?> </th> </tr>
	<?php
	//use this as main array - ATTENTION - POSSIBLE BUG if for one company we have no new production, but we have a cancellation
	foreach($newRenewProductionArray as $eachCompany)
	{
		?> <tr align="center"> <td> <?php echo $eachCompany->parameter; ?> </td> <td> <?php echo $eachCompany->production; ?> </td> 
		<?php
		//nummber of companies with cancelled contracts
		$number = count($cancelledProductionArray);
		
		$i = 0;
		
		//store the RENEW/RENEW - CANCEL production
		$totalProductionCompany = new production();
		
		//match companies between the new and cancelled array
		foreach($cancelledProductionArray as $eachCancelledCompany)
		{
			//company match found. means total = new/renew - cancelled
			if($eachCancelledCompany->parameter == $eachCompany->parameter )
			{ 
				?>
				<td> <?php echo $eachCancelledCompany->production; ?> </td>
				<?php
				$totalProductionCompany->production = $eachCompany->production - $eachCancelledCompany->production;
				$totalNewRenewProduction = $totalNewRenewProduction + $eachCompany->production;
				$totalCancelledProduction = $totalCancelledProduction + $eachCancelledCompany->production;
				 ?>
				<td> <?php echo $totalProductionCompany->production; ?> </td> </tr>
				<?php
				break;
			}
			$i++;
			
			//no company match found. means total=new/renew.
			if($i==$number)
			{ 
				?>
				<td>0</td>
				<?php
				$totalProductionCompany->production = $eachCompany->production;
				$totalNewRenewProduction = $totalNewRenewProduction + $eachCompany->production;
				?>
				<td> <?php echo $totalProductionCompany->production; ?> </td> </tr>
				<?php
			}
			
			$totalClearProductionArray[] = $totalProductionCompany;
			
		}  ?>
		
		<?php
	}
	?>
	<tr align="center"> <th> <?php echo $_SESSION['total']; ?> </th> <td> <?php echo $totalNewRenewProduction; ?> </td> <td> <?php echo $totalCancelledProduction; ?> </td> <td> <?php echo $totalNewRenewProduction-$totalCancelledProduction; ?> </td> </tr>
	</table>
	
	<?php
	if(count($producersProductionArray)>0)
	{
		?>
		<h2 align="center"><?php echo $_SESSION['producerTab']; ?></h2>
		
		<table align="center" border=1>
		<tr align="center"> <th> <?php echo $_SESSION['producerTab']; ?> </th> <th> <?php echo $_SESSION['production']; ?> </th></tr>
		<?php
		//use this as main array - ATTENTION - POSSIBLE BUG if for one company we have no new production, but we have a cancellation
		foreach($producersProductionArray as $eachProducer)
		{
			?> <tr align="center"> <td> <?php echo $eachProducer->parameter; ?> </td><td> <?php echo $eachProducer->production; ?> </td> 
			<?php
		}?>
			
		</table>
		<?php
	
	}?>
	
	<?php
	if(count($associatesProductionArray)>0)
	{	?>
		
		<h2 align="center"><?php echo $_SESSION['associate']; ?></h2>
	
		<table align="center" border=1>
		<tr align="center"> <th> <?php echo $_SESSION['associate']; ?> </th> <th> <?php echo $_SESSION['production']; ?> </th></tr>
		<?php
		//use this as main array - ATTENTION - POSSIBLE BUG if for one company we have no new production, but we have a cancellation
		foreach($associatesProductionArray as $eachCompany)
		{
			?> <tr align="center"> <td> <?php echo $eachCompany->parameter; ?> </td><td> <?php echo $eachCompany->production; ?> </td> 
			<?php
		}?>
			
		</table>
		<?php
	
	}
}
//no production found for this year
else
{
	echo "No production found for year ".$_SESSION['startYearValue'];
}
