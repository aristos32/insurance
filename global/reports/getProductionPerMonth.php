<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

if(isset($_POST['startYear']))
	$_SESSION['startYearValue'] = $_POST['startYear'];

if(isset($_POST['endYear']))
	$_SESSION['endYearValue'] = $_POST['endYear'];

$allProducers = retrieveAllProducers();

//retrieve production for each producer
foreach($allProducers as $eachProducer)
{
	$monthsProductionPerYearArray = retrieveProductionPerMonth($_SESSION['startYearValue'], $_SESSION['endYearValue'], $eachProducer);
	diplayProductionPerMonthTable($monthsProductionPerYearArray, $eachProducer);
}

//display total production for all producers
$monthsProductionPerYearArray = retrieveProductionPerMonth($_SESSION['startYearValue'], $_SESSION['endYearValue'], "%");
diplayProductionPerMonthTable($monthsProductionPerYearArray, $_SESSION['total']);

function diplayProductionPerMonthTable($monthsProductionPerYearArray, $producer)
{
	if(count($monthsProductionPerYearArray)>0)
	{
		?>
		<h2><?php echo $_SESSION['production']." - ".$_SESSION['year'].": ".$_SESSION['startYearValue']."-".$_SESSION['endYearValue']."-".$_SESSION['producerTab'].":".$producer; ?> </h2><br/>
		
		<table border=1>
		<tr align="center">
		<td><?php echo $_SESSION['year']; ?></td>
		<?php
		//print months header
		foreach($_SESSION['monthOptions'] as $eachMonth)
		{
			?><td><?php echo $eachMonth->name; ?></td><?php
		}
		?>
		<td><?php echo $_SESSION['total']; ?></td>
		</tr>
		<?php
		//print each year
		foreach($monthsProductionPerYearArray as $eachMonthsProductionPerYear)
		{
			?>
			<tr align="center">
			<td><?php echo $eachMonthsProductionPerYear->year; ?></td>
			<?php
			$totalProductionInYear=0;
			$monthNumber = 1;//some years may not have all months filled
			foreach($eachMonthsProductionPerYear->monthsArray as $eachMonthValue)
			{
				//when some months in the middle have no production, they are not returned from database -> so we fill with zeroes
				while($monthNumber!=$eachMonthValue->month)
				{
					?><td>0</td><?php
					$monthNumber = $monthNumber + 1;
				}
				?><td><?php echo $eachMonthValue->monthProduction; ?></td><?php
				$totalProductionInYear = $totalProductionInYear + $eachMonthValue->monthProduction;
				$monthNumber = $monthNumber + 1;
			}
			//for future months of current year, no value is found in the database -> so we fill with zeros
			for($i=$monthNumber; $i<=12; $i++ )
			{
				?><td>0</td><?php
			}
			?>
			<td><?php echo $totalProductionInYear; ?></td>
			</tr>
			<?php
		}
		?>
		
		</table>
		<?php
	}
	//no production found for these years
	else
	{
		echo "No production found for year ".$_SESSION['startYearValue'];
	}
}
