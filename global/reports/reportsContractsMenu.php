<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
    
$allProducers = retrieveAllProducers();

include "./reports/reportsMenu.php"
?>


<!-- EXPIRY DATE FORM -->
<form action="./office.php" id="getContractsByExpiryDate" method="post">
	<?php echo $_SESSION['contractsTab']; ?> --
	<input type="hidden" name="action" value="getContractsByExpiryDate" />
	<?php
		echo $_SESSION['end']."  ";
		
		$finalYear = date("Y")+1;
		$initialYear = $finalYear - 11;
		$currentMonth = date("m")+1; ?>:
			<select name="endYear" id="endYear" style="width:100px;">
					<?php
					for($i=$finalYear; $i>$initialYear; $i--)
					{
						
						?>
						<option  value="<?php echo $i ?>" ><?php echo "$i" ?> </option>
						<?php
					}
					?>
			</select>	
			
			<select name="endMonth"> 
			<?php 
			for($mon=1; $mon<=12; $mon++) 
			{ 
				if($mon!=$currentMonth)
				{
					?>
				   <option value="<?php echo $mon ?>"><?php echo date('F',mktime(0,0,0,$mon,15,2006));?></option> 
				   <?php 
				}
				else
				{
					?>
				   <option value="<?php echo $mon ?>" selected="selected"><?php echo date('F',mktime(0,0,0,$mon,15,2006));?></option> 
				   <?php 
				}
				
			} 
			?> 
			</select>

		
	<?php echo $_SESSION['producerTab'] ?>:
	<select name="producer" id="producer" style="width:100px;">
		<?php
		foreach($allProducers as $eachProducer){
			?>
			 <option  value=<?php echo "$eachProducer" ?> ><?php echo "$eachProducer" ?> </option>
			 <?php
		 }
		 ?>
		  <option selected="selected" value="%" ><?php echo $_SESSION['all']; ?> </option>
	</select>
	
	<input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" />
</form>



<!-- create hidden forn, to send the action as a POST -->
<form action="./office.php" id="getContractsWithRemainderProcess" method="post">
<input type="hidden" name="action" value="getContractsWithRemainderProcess" />
	<?php echo $_SESSION['contractsWithRemainder']; ?> --
	<?php echo $_SESSION['producerTab'] ?>:
	<select name="producerInContractsWithRemainder" id="producerInContractsWithRemainder" style="width:100px;">
		<?php
		foreach($allProducers as $eachProducer){
			?>
			 <option  value=<?php echo "$eachProducer" ?> ><?php echo "$eachProducer" ?> </option>
			 <?php
		 }
		 ?>
		  <option selected="selected" value="%" ><?php echo $_SESSION['all']; ?> </option>
	</select>
	<input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" />
</form>

<!-- LIST OF CONTRACTS PER PRODUCER -->
<form action="./office.php" id="getContactsPerProducer" method="post">
	<?php echo $_SESSION['contractsTab']; ?> --
	<input type="hidden" name="action" value="getContactsPerProducer" />
	<?php echo $_SESSION['producerTab'] ?>:
	<select name="singleProducerSearch" id="singleProducerSearch" style="width:100px;">
		<?php
		foreach($allProducers as $eachProducer){
			?>
			 <option  value=<?php echo "$eachProducer" ?> ><?php echo "$eachProducer" ?> </option>
			 <?php
		 }
		 ?>
		  <option selected="selected" value="%"><?php echo $_SESSION['all']; ?> </option>
	</select>
	<input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" />
</form>
