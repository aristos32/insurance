<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
    
$allProducers = retrieveAllProducers();

?>

<div id="centeredmenu">
<ul>
	<li>
		<form action="./office.php" id="reportsContractsMenu" method="post" style="display: none;">
			<input type="text" name="action" value="reportsContractsMenu" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('reportsContractsMenu').submit()"><?php echo $_SESSION['contractsTab']; ?></a>
	</li>
		
	
	<li>
		<form action="./office.php" id="reportsProductionMenu" method="post" style="display: none;">
			<input type="text" name="action" value="reportsProductionMenu" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('reportsProductionMenu').submit()"><?php echo $_SESSION['production']; ?></a>
	</li>
	
	<li>
		<form action="./office.php" id="reportsTransactionMenu" method="post" style="display: none;">
			<input type="text" name="action" value="reportsTransactionMenu" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('reportsTransactionMenu').submit()"><?php echo $_SESSION['transaction']; ?></a>
	</li>

</ul>
</div>
