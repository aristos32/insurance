<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
    
include "./reports/reportsMenu.php"
?>


<!-- PRODUCTION - create hidden forn, to send the action as a POST -->
<form action="./office.php" id="getProduction" method="post" onSubmit="return checkGetProductionForm()">
<input type="hidden" name="action" value="getProduction" />

<table style="display:inline-block">
<tr>
<td>
<?php echo $_SESSION['production'];?> - 
<select name="productionType" id="productionType" onChange="processProductionType()">
<?php 
foreach($_SESSION['productionOptions'] as $option){
	?>
	 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
	 <?php
 }
 ?>
</select>
</td>
<td>
<?php echo $_SESSION['start'];
$finalYear = date("Y");
$initialYear = $finalYear - 10; ?>:
<select name="startYear" id="startYear" style="width:70px;">
			<?php
			for($i=$finalYear; $i>$initialYear; $i--)
			{
				
				?>
				<option  value=<?php echo "$i" ?> ><?php echo "$i" ?> </option>
				<?php
			}
			?>
</select>

</td>	
<td id="end-year-info" style="display:none;">
	<?php echo $_SESSION['end']; ?>
	<select name="endYear" id="endYear" style="width:70px;">
				<?php
				for($i=$finalYear; $i>$initialYear; $i--)
				{
					
					?>
					<option  value=<?php echo "$i" ?> ><?php echo "$i" ?> </option>
					<?php
				}
				?>
	</select>
</td>
	
<td>	
 	<input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" />
</td>
</tr>
</table> 
</form>