<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
    
$allProducers = retrieveAllProducers();

include "./reports/reportsMenu.php"
		
?>

<!-- DISCOUNTS DATE FORM -->
<form action="./office.php" id="getDiscountsByDate" method="post">
	<?php echo $_SESSION['discounts']; ?> --
	<input type="hidden" name="action" value="getDiscountsByDate" />
	<?php echo $_SESSION['producerTab'] ?>:
	<select name="discountsProducerSearch" id="discountsProducerSearch" style="width:100px;">
		<?php
		foreach($allProducers as $eachProducer){
			?>
			 <option  value=<?php echo "$eachProducer" ?> ><?php echo "$eachProducer" ?> </option>
			 <?php
		 }
		 ?>
		  <option selected="selected" value="%"><?php echo $_SESSION['all']; ?> </option>
	</select>
	<?php echo $_SESSION['year'];
	$finalYear = date("Y");
	$initialYear = $finalYear - 10; ?>:
	<select name="year" id="year" style="width:100px;">
			<?php
			for($i=$finalYear; $i>$initialYear; $i--)
			{
				
				?>
				<option  value=<?php echo "$i" ?> ><?php echo "$i" ?> </option>
				<?php
			}
			?>
	</select>	
	<input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" />
</form>


<!-- TRANSACTION REPORTS MENU -->
<!-- create hidden forn, to send the action as a POST -->
<form action="./office.php" id="transactionReportsMenu" method="post" style="display: none;">
	<input type="hidden" name="action" value="transactionReportsMenu" />
</form>
<a href="javascript:;" onclick="javascript: document.getElementById('transactionReportsMenu').submit()"><?php echo $_SESSION['transactionPlural'];?></a>
