<?php 

 session_start();
 
 define('_INC', 1);

    //--------------------------------------------------------------------------
  // 1) Connect to mysql database
  //--------------------------------------------------------------------------
$globalFilesLocation = "..";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/database/connect.php");
require_once($globalFilesLocation."/generalIncludes/globalFunctions.php");
require_once($globalFilesLocation."/database/retrieveDatalayers.php");
 
  //--------------------------------------------------------------------------
  // 2) Query database for data
  //--------------------------------------------------------------------------
  $users = retrieveUsersInfo('%aris%');                          //fetch result    

 //print_r($users);
 
    echo json_encode($users);

?>