<?php

session_start();

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//we have independent file load here.
//we need to move one level down
$globalFilesLocation = "..";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/database/connect.php");
require_once($globalFilesLocation."/database/deleteDatalayers.php");

$driverId = $_GET['driverId'];

$affectedRows = deleteFromTable('drivers', 'driverId', $driverId);

$_SESSION['affectedRows'] = $affectedRows;

if($_SESSION['affectedRows']==1)
	echo "Driver Deleted";
else
	echo "Driver Cannot be Delete";
//echo "affected rows = ".$affectedRows."<br>"; //this is printed in the html <div> as the responsetext

//echo "stateId bn is :$user->stateId, firstName is:$user->firstName username = " . $_SESSION['searchUserName'] . "<br>";

?>