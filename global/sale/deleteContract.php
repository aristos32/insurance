<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$contractNumber = $_SESSION['contractNumber'];
//echo "deleteing contract ".$contractNumber;

deleteFromTable('sale', 'saleId', $contractNumber);

//vehicle can exist without sale also, if comes from quotation table - so we need to delete it independently
//TODO - this is not needed for non-motor contracts
deleteFromTable('vehicle', 'saleId', $contractNumber);



?>
	Contract Deleted Successfully. 
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="returnToFindContractForm" method="post" style="display: none;">
	<input type="text" name="action" value="findContractForm" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('returnToFindContractForm').submit()">Return</a>
