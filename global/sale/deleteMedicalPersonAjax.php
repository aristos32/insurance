<?php

session_start();

//no direct access to file allowed
define('_INC', 1);

//we have independent file load here.
//we need to move one level down
$globalFilesLocation = "..";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/generalIncludes/globalFunctions.php");
require_once($globalFilesLocation."/database/connect.php");
require_once($globalFilesLocation."/database/deleteDatalayers.php");

$personId = $_GET['personId'];

$affectedRows = deleteFromTable('medicalinsuredperson', 'personId', $personId);

$_SESSION['affectedRows'] = $affectedRows;

if($_SESSION['affectedRows']==1)
	echo "Person Deleted";
else
	echo "Person Cannot be Delete";
//echo "affected rows = ".$affectedRows."<br>"; //this is printed in the html <div> as the responsetext

//echo "stateId bn is :$user->stateId, firstName is:$user->firstName username = " . $_SESSION['searchUserName'] . "<br>";

?>