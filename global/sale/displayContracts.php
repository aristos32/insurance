<script type="text/javascript">		
	/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
	function setDefaultValues()
	{
	
		
	}

	function printpage()
	{
		window.print();
	}  
	
	
	</script>
<?php

if(count($_SESSION['contracts']) > 0 )
{
	?>
	<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
		
		<td class="col10Per"><?php echo $_SESSION['contractNumberTab']; ?></td>
		<td class="col10Per"><?php echo $_SESSION['insuranceCompanyTab']; ?></td>
		<td class="col10Per"><?php echo $_SESSION['proposerfullName']; ?></td>
		<td class="col10Per"><?php echo $_SESSION['typeTab']; ?></td>
		<td class="col10Per"><?php echo $_SESSION['end']; ?></td>
		<td class="col10Per"><?php echo $_SESSION['status']; ?></td>
		<td class="col10Per"><?php echo $_SESSION['telephoneTab']; ?></td>
		<td class="col5Per"><?php echo $_SESSION['remainder']; ?></td>
		<td class="col5Per"><?php echo $_SESSION['discount']; ?></td>
		<td class="col5Per"><?php echo $_SESSION['producerTab']; ?></td>
		<?php
		if( $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR )
		{
			?>
			<td><?php echo $_SESSION['delete']; ?></td>
			<?php
		}
		?>
		
	</tr>
	<?php
	$i=0;
	$totalDiscount = 0;
	$totalRemainder = 0;
	foreach($_SESSION['contracts'] as $eachContract)
	{
		
		?>					
		<tr>	
			<td class="col10Per">
			<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
			<form action="./office.php" id="<?php echo $eachContract->sale->saleId;?>" method="post" style="display: none;">
				<input type="text" name="action" value="updateContractData" />
				<input type="text" name="saleId" value="<?php echo $eachContract->sale->saleId;?>" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $eachContract->sale->saleId;?>').submit()">
				<?php echo $eachContract->sale->saleId;?></a>
			</td>
			<td>
				<?php
				foreach($_SESSION['insuranceCompanyTabOptions'] as $eachCompany )
				{
					if($eachCompany->value == $eachContract->insuranceCompanyOfferingQuote )
					{
						echo $eachCompany->name;
						break;
					}
				}
				?>
			
			</td>
			
			<td>
			
			<?php 
			$updateStateId = "update_".$eachContract->sale->stateId; ?> 
			<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
			<form action="./office.php" id="<?php echo $updateStateId;?>" method="POST" style="display: none;">
				<input type="text" name="action" value="updateClientDataForm" />
				<input type="text" name="stateId" value="<?php echo $eachContract->sale->stateId;?>" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $updateStateId;?>').submit()"><?php echo $eachContract->owner->firstName." ".$eachContract->owner->lastName; ?></a>
			</td>
			
			<td><?php echo $eachContract->sale->insuranceType; if($eachContract->sale->insuranceType==$INSURANCE_TYPE_MOTOR){ echo "-".$eachContract->motor->vehicle->regNumber; } ?></td>
			<td><?php echo $eachContract->sale->saleEndDate; ?></td>
			
			<td><?php  //set contract STATUS
				foreach($_SESSION['statusOptions'] as $eachOption)
				{
					if($eachOption->value == $eachContract->sale->status)
					{
						echo $eachOption->name;
						break;
					}
				}
			 ?></td>
			<td><?php echo $eachContract->owner->cellphone.",".$eachContract->owner->telephone; ?></td>
			<td><?php echo $eachContract->remainder; ?></td>
			<td><?php echo $eachContract->discount; ?></td>
			<td><?php echo $eachContract->sale->producer; ?></td>
			
			<?php
			if(is_numeric($eachContract->discount)){
				$totalDiscount = $totalDiscount + $eachContract->discount;
			}
			$totalRemainder = $totalRemainder + $eachContract->remainder;
			
			if( $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR )
			{
				$deleteContractNumber = "delete_".$eachContract->sale->saleId;?>
				<td>
				<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
				<form action="./office.php" id="<?php echo $deleteContractNumber;?>" method="POST" style="display: none;">
					<input type="text" name="action" value="deleteContract" />
					<input type="text" name="contractNumber" value="<?php echo $eachContract->sale->saleId;?>" />
				</form>
				<a href="javascript:;" onclick="javascript: if (confirm('<?php echo $_SESSION['areYouSureYouWantToDeleteThisContract'];?>')) document.getElementById('<?php echo $deleteContractNumber;?>').submit()"><?php echo $_SESSION['delete']; ?></a>
			  	</td>
				<?php
			}
			?>
		  
		</tr>	
		
		<?php
	}//foreach($contracts as $eachContract)
	?>
	<!-- print results -->
	<tr>
		<td><input type="button" value="<?php echo $_SESSION['print']; ?>" onclick="printpage()" /></td>
		<td><?php echo $_SESSION['total']." ".$_SESSION['discount']."=".$totalDiscount; ?></td>
		<td><?php echo $_SESSION['total']." ".$_SESSION['remainder']."=".$totalRemainder; ?></td>
		<td> <a href="javascript:;" onclick="send_email('<?php echo $allEmails; ?>', '<?php echo $_SESSION['globalFilesLocation']; ?>', '<?php echo $_SESSION['SYSTEM_EMAIL']; ?>', '<?php echo $_SESSION['lang']; ?>', '<?php echo $_SESSION['clientName']; ?>', '<?php echo $allStateIds; ?>' )"><?php echo $_SESSION['sendEmailToAll']; ?></a>
		</td>
	</tr>
	</table>	
	
	<?php

}
//no contracts found
else 
{
	?>
	No contracts found. Please 
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="findContractForm" method="POST" style="display: none;">
	<input type="text" name="action" value="findContractForm" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('findContractForm').submit()">Try Again</a>
	<?php
}
?>