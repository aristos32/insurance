<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

?>

<form name="findContractForm" action="./office.php" method="POST" onSubmit="return checkFindContractFormForm()">
	<table>
	<input type="hidden" name="action" value="findContractFormProcess">
		<p>
		<!-- CONTRACT ID -->
		<tr>
			<td class="label"><?php echo $_SESSION['contractNumberTab']; ?>:</td>
			<td class="input"><input type="text" name="contractNumber" id="contractNumber" size="30" value="" /><br /></td>
		</tr>
		
		<!-- VEHICLE REGISTRATION -->
		<tr>
			<td class="label"><?php echo $_SESSION['regNumberTab']; ?>:</td>
			<td class="input"><input type="text" name="regNumber" id="regNumber" size="30" value="" /><br /></td>
		</tr>
		
		<!-- STATE ID -->
		<tr>
			<td class="label"><?php echo $_SESSION['stateIdTab']; ?>:</td>
			<td class="input"><input type="text" name="stateId" id="stateId" size="30" value="" /><br /></td>
		</tr>
		
		<!-- COMPANY -->
		<tr>
			<td class="label"><?php echo $_SESSION['company']; ?>:</td>
			<td class="input"><select name="insuranceCompany" id="insuranceCompany" style="width:230px;">
				<?php
				foreach($_SESSION['insuranceCompanyTabOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name"?> </option>
					 <?php
				 }
				 ?>
				 </select>	
			</td>
		</tr>
		
		<!-- CONTINUE BUTTON -->						
		<tr>
			<td class="label"></td>
			<td class="input"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" /></td>
		</tr>
		</p>
	</table>
	
</form>	