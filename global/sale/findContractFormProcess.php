<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//aaresti removed bug
//include $_SESSION['globalFilesLocation']."/sale/newContractFormDefaultValues.php";

?>

<?php

$contracts = array();
//on language change, POST are not send. So, we use the 'contracts' in session. Keep in session the latest contract we search for.
if(isset($_POST['contractNumber']) && $_POST['contractNumber']!='' )
{
	$parameterNameValueArray = array();
	
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = ' s.saleId ';
	$parameterNameValue->value = "%".$_POST['contractNumber']."%";
	
	$parameterNameValueArray[] = $parameterNameValue;
	
	$contracts = retrieveContractInfo($parameterNameValueArray, 1);
	$_SESSION['contracts'] = $contracts;
}
else if(isset($_POST['regNumber']) && $_POST['regNumber']!='' )
{
	//step 1 - retrieve vehicle info
	$vehicles = retrieveVehicleInfo('regNumber', "%".$_POST['regNumber']."%");
	//some vehicles found
	//echo "found vehicle=".count($vehicles);
	if(count($vehicles) > 0 )
	//if($vehicle->saleId != '' )
	{
		foreach($vehicles as $eachVehicle)
		{
			$parameterNameValueArray = array();
			
			$saleId = $eachVehicle->saleId;
			
			$parameterNameValue = new parameterNameValue();
			$parameterNameValue->name = ' s.saleId ';
			$parameterNameValue->value = $saleId;
			
			$parameterNameValueArray[] = $parameterNameValue;
			
			$allContracts = retrieveContractInfo($parameterNameValueArray, 1);
			if(count($allContracts)>0)
			{
				//echo "contracts found for saleid = ".$saleId;
				foreach($allContracts as $eachContract)
					$contracts[] = $eachContract;
			}
		}
	}
	//none found - create empty array
	else
		$contracts = array();
	$_SESSION['contracts'] = $contracts;
	
}
else if(isset($_POST['insuranceCompany']) && $_POST['insuranceCompany']!='NONE' )
{
	$parameterNameValueArray = array();
	
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = ' s.company ';
	$parameterNameValue->value = "%".$_POST['insuranceCompany']."%";

	$parameterNameValueArray[] = $parameterNameValue;
	
	$contracts = retrieveContractInfo($parameterNameValueArray, 1);
	$_SESSION['contracts'] = $contracts;
}
else if(isset($_POST['stateId']) && $_POST['stateId']!='' )
{
	//echo "inside with stateId = " . $_POST['stateId'] ;
	$parameterNameValueArray = array();
	
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = ' s.stateId ';
	$parameterNameValue->value = "%".$_POST['stateId']."%";
	
	$parameterNameValueArray[] = $parameterNameValue;
	
	$contracts = retrieveContractInfo($parameterNameValueArray, 1);
	$_SESSION['contracts'] = $contracts;
}

//create a list of all the emails
$allEmails = '';
$allStateIds = '';

foreach($_SESSION['contracts'] as $eachContract)
{
	//we have several contracts for same client. so we need to take the client email only once.
	//1. email must have some value, not empty
	//2. email must not exist in the $allMails list already
	if($eachContract->owner->email != '' && strpos($allEmails,$eachContract->owner->email)==false )
	{
		$allEmails = $allEmails.",".$eachContract->owner->email;
		$allStateIds = $allStateIds.",".$eachContract->owner->stateId;
	}
}


include $_SESSION['globalFilesLocation']."/sale/displayContracts.php";

?>
