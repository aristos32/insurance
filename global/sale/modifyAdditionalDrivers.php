<?php
//has global functions called from many places for display of HTML pages
//echo "INSIDE";

//step 1 = show all existing drivers
//step 2 = add additional drivers
?>

<script>	

$(document).ready(function(){
    
  var tableRowsNum = $('#additionalDriversTable tr').length;
  
  //initialize
  for (var i=0;i<tableRowsNum-1;i++)
  {
	   $('#birthDate_'+i).datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-90:+10',dateFormat: 'dd-mm-yy'});
	   $('#licenseDate_'+i).datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10', dateFormat: 'dd-mm-yy'});
	   	   
  }
 
 

  //bind a click event to the "Add" link
  //$('#addnew').live("click", function() { SOS - for loading js before html dom is fully loaded. This works and outside the ready function
	$('#addnew').click(function() {
	  	//$(".datepick").datepicker();
	  	var newRowNum = 0;
	
	 // Detach all datepickers before cloning - need to manually remove 'id' since it's not destroyed
	   $(".datepick").datepicker("destroy").removeAttr('id');
	
	   // increment the counter
	   newRowNum = $(additionalDriversTable).children('tbody').children('tr').length + 1;
	   
	   
	   // get the entire "Add" row --
	   // "this" refers to the clicked element
	   // and "parent" moves the selection up
	   // to the parent node in the DOM
	   var addRow = $(this).parent().parent();
	
	   // copy the entire row from the DOM with "clone"
	   var newRow = addRow.clone(true);
	
	   // set the values of the inputs in the "Add" row to empty strings
	   $('input', addRow).val('');
	
	   // insert a remove link in the last cell
	   $('td:last-child', newRow).html('<a href="" class="remove"><i class="icon-minus"><\/i><\/a>');
	   
	
	   // insert the new row into the table "before" the Add row
	   addRow.before(newRow);
	
	
	    $(".datepick").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10', dateFormat: 'dd-mm-yy'});
	   
	   
	   // add the remove function to the new row
	   $('a.remove', newRow).click(function() {
	       $(this).closest('tr').remove();
	       return false;
	   });
	
	   //newRowNum = $('#additionalDriversTable tr').length - 2;
	   
	   
	   $('#birthDate', newRow).each(function() {
	       var newID = 'birthDate_' + newRowNum;
	       $(this).attr('id', newID);
	
   });

   $('#licenseDate', newRow).each(function() {
       var newID = 'birthDate_' + newRowNum;
       $(this).attr('id', newID);

   });

  
   // prevent the default click
   return false;
  });


	//remove's default rows
	$('.removeDefault').click(function() {
	    $(this).closest('tr').remove();
	    return false;
	});

}); //document.ready




function addRowInAdditionalDriversTable(tableID)
{

    var table = document.getElementById(tableID);
	var columnSize = "7";
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
	var element;
	
	//remove checkbox
    var cell1 = row.insertCell(0);
    element = document.createElement("input");
    element.type = "checkbox";
    element.name = "checkbox[]";
    element.size = columnSize;
    cell1.appendChild(element);

    //firstname
    var cell2 = row.insertCell(1);
    element = document.createElement("input");
    element.type = "text";
    element.name = "firstName[]";
    element.size = columnSize;
    cell2.appendChild(element);
    
    //lastName
    var cell3 = row.insertCell(2);
    element = document.createElement("input");
    element.type = "text";
    element.name = "lastName[]";
    element.size = columnSize;
    cell3.appendChild(element);

    //stateId
    var cell4 = row.insertCell(3);
    element = document.createElement("input");
    element.type = "text";
    element.name = "stateId[]";
    element.size = columnSize;
    cell4.appendChild(element);
    
    //nationality
    var cell5 = row.insertCell(4);
    var selector = document.createElement("select");
    selector.style.width = '6.5em';
	selector.name = 'countryOfBirth[]';
    cell5.appendChild(selector);
    
    //insert all countries
    <?php
		foreach($_SESSION['countriesOptions'] as $eachCountry)
		{
			?>
			var option = document.createElement("option");
			option.value = '<?php echo $eachCountry->value;?>';
			countryName = '<?php echo $eachCountry->name;?>';
			//for chrome, need to define variables as 'var'
			var name = document.createTextNode(countryName);
			option.appendChild(name);
			selector.appendChild(option);
		<?php
		}	
		
	 ?>
	 
	//birthDate
	/*
    var cell6 = row.insertCell(5);
    element = document.createElement("input");
    element.type = "text";
    element.name = "birthDate[]";
    element.size = columnSize;
    cell6.appendChild(element); */

    $(".birthDate").datepicker();
    var cell6 = row.insertCell(5);
   
    cell6.appendChild( $(".birthDate"));
    
    
	//licenseDate
    var cell7 = row.insertCell(6);
    element = document.createElement("input");
    element.type = "text";
    element.name = "licenseDate[]";
    element.size = columnSize;
    cell7.appendChild(element);
    
    //licenseCountry
    var cell8 = row.insertCell(7);
    var selector = document.createElement("select");
    selector.style.width = '6.5em';
	selector.name = 'licenseCountry[]';
    cell8.appendChild(selector);
    
    //insert all countries
    <?php
		foreach($_SESSION['countriesOptions'] as $eachCountry)
		{
			?>
			var option = document.createElement("option");
			option.value = '<?php echo $eachCountry->value;?>';
			countryName = '<?php echo $eachCountry->name;?>';
			//for chrome, need to define variables as 'var'
			var name = document.createTextNode(countryName);
			option.appendChild(name);
			selector.appendChild(option);
		<?php
		}	
		
	 ?>

	 //licenseType
    var cell9 = row.insertCell(8);
    var selector = document.createElement("select");
    selector.style.width = '6.5em';
	selector.name = 'licenseType[]';
    cell9.appendChild(selector);
    
    //insert all countries
    <?php
		foreach($_SESSION['licenseTypeTabOptions'] as $eachLicense)
		{
			?>
			var option = document.createElement("option");
			option.value = '<?php echo $eachLicense->value;?>';
			licenseName = '<?php echo $eachLicense->name;?>';
			//for chrome, need to define variables as 'var'
			var name = document.createTextNode(licenseName);
			option.appendChild(name);
			selector.appendChild(option);
		<?php
		}	
		
	 ?>
	 
	//telephone
    var cell10 = row.insertCell(9);
    element = document.createElement("input");
    element.type = "text";
    element.name = "telephone[]";
    element.size = columnSize;
    cell10.appendChild(element);
    
    //profession
    var cell11 = row.insertCell(10);
    element = document.createElement("input");
    element.type = "text";
    element.name = "profession[]";
    element.size = columnSize;
    cell11.appendChild(element);
	 
}
</script>

<?php
//retrieve existing drivers
$existingAdditionalDrivers = retrieveDriversInfo($_SESSION['saleId']);
$_SESSION['existingAdditionalDrivers'] = $existingAdditionalDrivers;
?>

<!--<form name="addAdditionalDrivers" action="./office.php" method="POST">-->
<form name="modifyAdditionalDrivers" action="./office.php" method="POST" onSubmit="return checkModifyAdditionalDriversForm('additionalDriversTable')" >
	
	<input type="hidden" name="action" value="modifyAdditionalDriversProcess">
	
	<table id="additionalDriversTable">
		<tbody>
		<tr>
			<td class="col10Per"></td>
			<td class="col10Per"><b><?php echo $_SESSION['nameTab']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['surname']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['stateIdTab']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['nationality']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['birthDate']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['licenseDateTab']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['countryOfLicense']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['licenseTypeTab']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['telephoneTab']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['profession']; ?></b></td>
		</tr>
		<?php
		$i = 0;
		foreach($existingAdditionalDrivers as $eachDriver)
		{
			?>
			<tr>
				<td class="col10Per"><input type="checkbox" name="chk[]" ></td>
				<td class="col10Per"><input type="text" name="firstName[]" value="<?php echo $eachDriver->firstName;?>" size="7" /></td>
				<td class="col10Per"><input type="text" name="lastName[]" value="<?php echo $eachDriver->lastName;?>" size="7"/> </td>
				<td class="col10Per"><input type="text" name="stateId[]" value="<?php echo $eachDriver->stateId;?>" size="7"/></td>
				<td class="col10Per">
					<select name="countryOfBirth[]" id=<?php echo "countryOfBirth".$i; ?> style="width:6.5em;" >
						<?php
								foreach($_SESSION['countriesOptions'] as $eachCountry)
								{?>
								<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
								<?php	
								}
								?>
					</select>						
				</td>
				<td class="col10Per"><input class="datepick" type="text" name="birthDate[]" id=<?php echo "birthDate_".$i; ?> value="<?php echo $eachDriver->birthDate;?>" size="9" /></td>
				  
			    
				<td class="col10Per"><input class="datepick" type="text" name="licenseDate[]" id=<?php echo "licenseDate_".$i; ?> value="<?php echo $eachDriver->licenseDate;?>" size="9" /></td>
				
				
				<td class="col10Per">
					<select name="licenseCountry[]" id=<?php echo "licenseCountry".$i; ?> style="width:6.5em;">
						<?php
						foreach($_SESSION['countriesOptions'] as $eachCountry)
						{?>
						<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
						<?php	
						}
						?>
					</select>
				</td>
				<td class="col10Per">
					<select name="licenseType[]" id=<?php echo "licenseType".$i; ?> style="width:6.5em;">
					<?php
					foreach($_SESSION['licenseTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
					</select>
				</td>
				<td class="col10Per"><input type="text" name="telephone[]" value="<?php echo $eachDriver->telephone;?>" size="7"/></td>
				<td class="col10Per"><input type="text" name="profession[]" value="<?php echo $eachDriver->profession;?>" size="7"/></td>
				<td></td>
				
				<script>	
						setPreviousValue("<?php echo $eachDriver->countryOfBirth; ?>", "<?php echo "countryOfBirth".$i; ?>");
						setPreviousValue("<?php echo $eachDriver->licenseCountry; ?>", "<?php echo "licenseCountry".$i; ?>");
						setPreviousValue("<?php echo $eachDriver->licenseType; ?>", "<?php echo "licenseType".$i; ?>");
				</script>
			</tr>
			<?php
			$i = $i + 1;	
		}
		?>
			
		<!--  insert empty row -->
		<tr>
				<td class="col10Per"><input type="checkbox" name="chk[]" ></td>
				<td class="col10Per"><input type="text" name="firstName[]" value="" size="7" /></td>
				<td class="col10Per"><input type="text" name="lastName[]" value="" size="7"/> </td>
				<td class="col10Per"><input type="text" name="stateId[]" value="" size="7"/></td>
				<td class="col10Per">
					<select name="countryOfBirth[]" id=<?php echo "countryOfBirth".$i; ?> style="width:6.5em;" >
						<?php
								foreach($_SESSION['countriesOptions'] as $eachCountry)
								{?>
								<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
								<?php	
								}
								?>
					</select>						
				</td>
				<td class="col10Per"><input class="datepick" type="text" name="birthDate[]" id=<?php echo "birthDate_".$i; ?> value="" size="9" /></td>
				<!-- <script>
				$("#birthDate_"+"<?php echo $i?>").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
				$("#birthDate_"+"<?php echo $i?>").datepicker('option', 'dateFormat', 'dd-mm-yy');
				$("#birthDate_"+"<?php echo $i?>").datepicker('setDate', "<?php echo $eachDriver->birthDate;?>");
			    </script>
			    -->
				<td class="col10Per"><input class="datepick" type="text" name="licenseDate[]" id=<?php echo "licenseDate_".$i; ?> value="" size="9" /></td>
				
				<td class="col10Per">
					<select name="licenseCountry[]" id=<?php echo "licenseCountry".$i; ?> style="width:6.5em;">
						<?php
						foreach($_SESSION['countriesOptions'] as $eachCountry)
						{?>
						<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
						<?php	
						}
						?>
					</select>
				</td>
				<td class="col10Per">
					<select name="licenseType[]" id=<?php echo "licenseType".$i; ?> style="width:6.5em;">
					<?php
					foreach($_SESSION['licenseTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
					</select>
				</td>
				<td class="col10Per"><input type="text" name="telephone[]" value="" size="7"/></td>
				<td class="col10Per"><input type="text" name="profession[]" value="" size="7"/></td>
				<td><a class="addnew" id="addnew" href="">add</a> </td>
				
				
			</tr>
			</tbody>
	</table>
	
	<!-- <INPUT type="button" value="<?php echo $_SESSION['addDriver'];?>" onclick="addRowInAdditionalDriversTable('additionalDriversTable')" />-->
					
	 
	<INPUT type="button" value="<?php echo $_SESSION['deleteDriver'];?>" onclick="deleteRowFromTable('additionalDriversTable')" />
	
	<br><br>
	
	<input type="submit" name="send" class="button" value="<?php echo $_SESSION['updateButton'];?>" size="50" />
</form>