<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

include $_SESSION['globalFilesLocation']."/sale/newContractFormDefaultValues.php";


?>

<script>

$(document).ready(function(){

	var today = new Date();
	var finishDate = new Date();
	//create contract end date
	finishDate.setYear(today.getFullYear()+1);
	finishDate.setDate(finishDate.getDate()-1);

	$("#saleStartDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#saleStartDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#saleStartDate").datepicker('setDate', today);
	    
    $("#saleEndDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#saleEndDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#saleEndDate").datepicker('setDate', finishDate);

        
    $("#ownerBirthDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#ownerBirthDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#ownerBirthDate").datepicker('setDate', '01-01-1995');
    
    $("#licenseDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#licenseDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#licenseDate").datepicker('setDate', '01-01-2000');

   
});

//AJAX Functionality	
/* 1. call loadQuotationAjax.php to retrieve data asynchronously
   2. loadQuotationAjax.php saves data in xml file
   3. set retrieved data from xml in current html form, using myFunctionForQuotationLoad */
function loadQuotationData(quoteId)
{
	
	//file to execute
	url = "./vehicleQuote/loadQuotationDataAjax.php";
	//read all variables to update automatically
	//alert(quoteId);
    
    queryString = "?quoteId=" + quoteId;
  
    //alert(url+queryString);
  
    //set the retrieved values from the xml to the quotation table
    myFunctionForQuotationLoad(url+queryString);
    
    return true;
} 

/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
function setDefaultValues()
{
	setPreviousValue("<?php echo $licenseType_content; ?>", "licenseType");
	setPreviousValue("<?php echo $licenseCountry_content; ?>", "licenseCountry");
	setPreviousValue("<?php echo $proposerType_content; ?>", "proposerType");
}


</script>

<?php
/* COMMENT OUT THIS FUNCTIONALITY TO LOAD OLD QUOTES. aristosa 04/04/2012

<h2><?php echo $username; ?>=<?php echo $_SESSION['username'];?></h2><!-- aristosa bug : changed 28/02/2012 -->

Load An Existing Quotation for logged-in user : <div id="message" style="display:inline;color:#00FF00"></div>
<?php

$previousQuotes = retrieveAllPreviousUserQuotes($_SESSION['username']);//aristosa bug : changed 28/02/2012
//$previousQuotes = retrieveAllPreviousUserQuotes($_SESSION['user']->username);
if(empty($previousQuotes))
{
	echo $noQuotesFound; 
}
else
{
	?>

<table>
	<tr>
		
		<td class="col20Per"><b>ID</b></td>
		<td class="col20Per"><b><?php echo $canProvideOnline; ?></b></td>
		<td class="col20Per"><b><?php echo $entryDate;?></b></td>
		<td class="col20Per"><b><?php echo $quotationCost; ?>(€)</b></td>
		<td class="col20Per"><b><?php echo $load; ?></b></td>
		
	</tr>
	<?php
	$i = 0;
	foreach($previousQuotes as $quote)
	{
		?>					
	<tr>
		<td ><?php echo $quote->quoteId;?></td>
		<td ><?php echo $quote->canProvideOnlineQuote;?></td>
		<td><?php echo $quote->entryDate;?></td>
		<td ><?php if(strcmp($quote->canProvideOnlineQuote,$NO_CONSTANT)==0)
							  {
									echo "N/A";
							  }
							  else
							  	echo $quote->quoteAmount;
			?>
		</td>
		<td>
			<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
			<form action="./office.php" id="<?php echo $quote->quoteId;?>" method="post" style="display: none;">
				<input type="text" name="action" value="quotePresentation" />
				<input type="text" name="quoteId" value='<?php echo $quote->quoteId;?>' />
			</form>
			<a href="javascript:;" onclick="javascript: loadQuotationData('<?php echo $quote->quoteId;?>')"><?php echo $load;?></a>
		</td>
		
	</tr>
	<?php
	}	
	?>
</table>
<?php
}
 COMMENT OUT THIS FUNCTIONALITY TO LOAD OLD QUOTES */
?>

<form name="newContractFrom" action="./office.php" method="POST" onSubmit="return checkNewContractForm()"  >

	<input type="hidden" name="action" value="newContractFormProcess">
	
	<table>

		<tr>
			<!-- SALE START DATE -->
			<td class="label"><?php echo $_SESSION['start']; ?></td>
			<td class="input">
			<input type="text" style="width: 80px;" id="saleStartDate" name="saleStartDate" />
			</td>
		</tr>
		<tr>
			<!-- SALE END DATE -->
			<td class="label"><?php echo $_SESSION['end']; ?></td>
			<td class="input">
			<input type="text" style="width: 80px;" id="saleEndDate" name="saleEndDate" />
			</td>
		</tr>
		<!-- CONTRACT NUMBER -->
		<tr>
			<td class="label"><?php echo $_SESSION['contractNumberTab']; ?></td>
			<td class="input"><input type="text" name="contractNumber" id="contractNumber" size="30" value="" /></td>
		</tr>
	

	
	<!-- PROPOSER DETAILS SECTION -->
	<tr><td class="h3"><h3><?php echo $_SESSION['proposerDetails']; ?></h3></td></tr>
	
	<!-- PROPOSER TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['proposerTypeTab']; ?></td>
		<td class="input"><select name="proposerType" id="proposerType" style="width:230px;" onChange="processProposerType()">
			<?php
			$i = 1;
			foreach($_SESSION['proposerTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>		
			 </select>	
		</td>
	</tr>
	<!-- NAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['nameTab']; ?>:</td>
		<td class="input"><input type="text" name="firstName" id="firstName" size="30" value="<?php echo $firstName_content; ?>" /></td>
	</tr>
	<tr>
		<td class="label"><?php echo $_SESSION['surname']; ?>:</td>
		<td class="input"><input type="text" name="lastName" id="lastName" size="30" value="<?php echo $lastName_content; ?>" /></td>
	</tr>								
	<!-- STATEID -->
	<tr>
		<td class="label"><?php echo $_SESSION['stateIdTab']; ?></td>
		<td class="input"><input type="text" name="stateId" id="stateId" size="30" value="<?php echo $stateId_content; ?>" />
		</td>
	</tr>
	<!-- TELEPHONE -->
	<tr>
		<td class="label"><?php echo $_SESSION['telephoneTab']; ?></td>
		<td class="input"><input type="text" name="telephone" id="telephone" size="30" value="<?php echo $telephone_content; ?>" />
		</td>
	</tr>
	<!-- CELLPHONE -->
	<tr>
		<td class="label"><?php echo $_SESSION['cellphoneTab']; ?></td>
		<td class="input"><input type="text" name="cellphone" id="cellphone" size="30" value="<?php echo $cellphone_content; ?>" />
		</td>
	</tr>
	
	
	<!-- OWNER PROFESSION-->
	<tr>
		<td class="label"><?php echo $_SESSION['profession']; ?></td>
		<td class="input"><input type="text" name="ownerProfession" id="ownerProfession" size="30" value="<?php echo $ownerProfession_content; ?>" /></td>			
	</tr>
		
	<tbody id="proposer-person-info">	
	
	<!-- OWNER BIRTHDATE -->
	<tr>
		<td class="label"><?php echo $_SESSION['birthDate']; ?></td>
		<td class="input">
			<input type="text" style="width: 80px;" id="ownerBirthDate" name="ownerBirthDate" />
		</td>
	</tr>
	
	<!-- OWNER ETHNICITY-->
	<tr>
		<td class="label"><?php echo $_SESSION['proposerNationality'];?></td>
		<td class="input"><select name="ownerEthnicity" id="ownerEthnicity" style="width:280px;">
			<?php
					foreach($_SESSION['countriesOptions'] as $eachCountry)
					{?>
					<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
					<?php	
					}
					?>
			</select>	
		</td>
		
	</tr>
	</tbody>
	
	<tr><td class="h3"><h3><?php echo $_SESSION['insuranceTypeTab']; ?></h3></td></tr>									
	<!-- INSURANCE TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuranceTypeTab']; ?></td>
		<td class="input"><select name="insuranceType" id="insuranceType" style="width:230px;" onChange="processInsuranceType()">
			<?php
			$i = 1;
			foreach($_SESSION['insuranceTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>		
			 </select>	
			<a href="./quotation.php?action=glossary&location=insuranceType" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	
	<!-- empty lines -->
	<tr><td><br /></td></tr>
	<tr><td><br /></td></tr>
	
	<tbody id="motor-license-info">	
	<!-- COUNTRY OF DRIVING LICENSE -->
	<tr>
		<td class="label"><?php echo $_SESSION['countryOfLicense']; ?></td>
		<td class="input"><select name="licenseCountry" id="licenseCountry" style="width:250px;">
			<?php
			foreach($_SESSION['countriesOptions'] as $eachCountry)
			{?>
			<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
			<?php	
			}
			?>
			<a href="./quotation.php?action=glossary&location=licenseCountry" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
			
		</td>		
	</tr>
	
	<!-- TYPE OF DRIVING LICENSE -->
	<tr>
		<td class="label"><?php echo $_SESSION['licenseTypeTab']; ?></td>
		<td class="input"><select name="licenseType" id="licenseType">
			<?php
			foreach($_SESSION['licenseTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
		
	</tr>
	
	<!-- LICENSE DATE -->
	<tr>
		<td class="label"><?php echo $_SESSION['licenseDateTab']; ?></td>
		<td class="input">
			<input type="text" style="width: 80px;" id="licenseDate" name="licenseDate" />
		</td>
	</tr>
	</tbody>
	
	<tbody id="motor-info" style="display:table-row-group;">
	<!-- HAS PREVIOUS CYPRIOT INSURANCE 
	
	<tr>
		<td class="label"><?php echo $_SESSION['previousInsuranceTab']; ?></td>
		<td class="input"><select name="hasPreviousInsurance" id="hasPreviousInsurance">
			<?php
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
			<select name="previousCypriotInsurance" id="previousCypriotInsurance" style="width:230px;">
			<?php
			foreach($_SESSION['insuranceCompanyOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			 </select>	
			<a href="./quotation.php?action=glossary&location=hasPreviousInsurance" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>-->
	
	
	
	
	<tr><td></td></tr>
	
	<tr><td class="h3"><h3><?php echo $_SESSION['coverageTypeTab']; ?></h3></td></tr>									
	<!-- COVERAGE TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['coverageTypeTab']; ?></td>
		<td class="input"><select name="coverageType" id="coverageType" style="width:230px;" onChange="processCoverageNewContract()">
			<?php
			$i = 1;
			foreach($_SESSION['coverageTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>		
			 </select>	
			<a href="./quotation.php?action=glossary&location=coverageType" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<tr id="ca" style="display:none;"  >
		<td class="label"><?php echo $_SESSION['coverageAmountTab']; ?>(<?php echo $EURO;?>):</td>
			
		<!-- input is common for both cases -->
		<td class="input"><input type="text" name="coverageAmount" id="coverageAmount" value="<?php echo $coverageAmount_content; ?>"  size="20" />
		<a href="./quotation.php?action=glossary&location=coverageAmount" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a></td>
	</tr>
			
	<tr><td class="h3"><h3><?php echo $_SESSION['vehicleDetails']; ?></h3></td></tr>
	
	<!-- REGISTRATION NUMBER -->
	<tr>
		<td class="label"><?php echo $_SESSION['regNumberTab']; ?></td>
		<td class="input"><input type="text" name="regNumber" id="regNumber" size="30" value="" />
		</td>
	</tr>
	
	<!-- VEHICLE TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleTypeTab']; ?></td>
		<td class="input"><select name="vehicleType" id="vehicleType"  style="width:230px;">
			<?php
			$i = 1;
			foreach($_SESSION['vehicleTypeTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$i - $option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			 </select>	
		<a href="./quotation.php?action=glossary&location=vehicleTypes" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- VEHICLE MAKE -->
	<tr>
		<td class="label"><?php echo $_SESSION['make']; ?>( i.e MITSUBISHI ):</td>
		<td class="input"><input type="text" name="vehicleMake" id="vehicleMake" size="30" value="<?php echo $vehicleMake_content; ?>" /></td>
	</tr>
	
	<!-- VEHICLE MODEL -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleModelTab']; ?>( i.e COLT ):</td>
		<td class="input"><input type="text" name="vehicleModel" id="vehicleModel" size="30" value="<?php echo $vehicleModel_content; ?>" /></td>
	</tr>
	
	<!-- CUBIC CAPACITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['engineCapacity']; ?>(CC):</td>
		<td class="input"><input type="text" name="cubicCapacity" id="cubicCapacity" size="30" value="<?php echo $cubicCapacity_content; ?>" />
		<a href="./quotation.php?action=FAQ&location=cubicCapacity" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a></td>
	</tr>
	
	
	<!-- VEHICLE MANUFACTURE YEAR -->
	<tr>
		<td class="label"><?php echo $_SESSION['manufacturedYear']; ?></td>
		<td class="input"><select name="vehicleManufacturedYear" id="vehicleManufacturedYear" style="width:230px;">
			<?php
			$currentYear = date("Y");
			for($i=$currentYear; $i>1950; $i--){
				?>
				 <option  value=<?php echo $i ?> ><?php echo "$i" ?> </option>
				 <?php
			 }
			 ?>
			 </select>
		</td>
	</tr>
	
						
	<!-- STEERING WHEEL SIDE -->
	<tr>
		<td class="label"><?php echo $_SESSION['steeringWheelSideTab']; ?></td>
		<td class="input"><select name="steeringWheelSide" id="steeringWheelSide">
			<?php
			$i = 1;
			foreach($_SESSION['steeringWheelSideTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			</select>
		<a href="./quotation.php?action=glossary&location=steeringWheelSide" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- SPORTS MODEL -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleSportsModel']; ?></td>
		<td class="input"><select name="isSportsModel" id="isSportsModel">
			<?php
			$i = 1;
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>
			</select>
		<a href="./quotation.php?action=glossary&location=sportsModel" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- VEHICLE DESIGN -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleDesignTab']; ?></td>
		<td class="input"><select name="vehicleDesign" id="vehicleDesign">
			<?php
			$i = 1;
			foreach($_SESSION['vehicleDesignTabOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>						
			</select>
		<a href="./quotation.php?action=glossary&location=vehicleDesign" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
	
	<!-- TAX FREE -->
	<tr>
		<td class="label"><?php echo $_SESSION['vehicleTaxFree']; ?></td>
		<td class="input"><select name="isTaxFree" id="isTaxFree">
			<?php
			$i = 1;
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>	
			</select>
		</td>
	</tr>
	
	<!-- USED FOR DELIVERIES -->
	<tr>
		<td class="label"><?php echo $_SESSION['usefForDeliveries']; ?></td>
		<td class="input"><select name="isUsedForDeliveries" id="isUsedForDeliveries">
			<?php
			$i = 1;
			foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
				 $i = $i + 1;
			 }
			 ?>		
			</select>
		<a href="./quotation.php?action=glossary&location=isUsedForDeliveries" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" width="21" height="21" /></a>
		</td>
	</tr>
			
	</tbody>
	
	<tbody id="motor-additional-benefits" style="display:none;">
	
	<!-- HIDDEN FIELDS FOR ADDITIONAL BENEFITS -->
	
	<tr><td class="h3"><h3><?php echo $_SESSION['selectSeparateBenefits']; ?></h3></td></tr>
	
	<?php
	//Step 1 - Retrieve All Benefits for Motor
	
	//TODO - send also the vehicle type with function. see comments in function	
	displayAllCoveragesRelatedToInsuranceType($INSURANCE_TYPE_MOTOR);
	//Step 2 - Display Benefits
	
	?>
	
	
	</tbody>
	
	<tbody id="employer-liability-info" style="display:none;">
	
	<!-- EMPLOYERS SOCIAL SECURITY NUMMBER -->
	<tr>
		<td class="label"><?php echo $_SESSION['employersSocialInsuranceNumber']; ?>:</td>
		<td class="input"><input type="text" name="employersSocialInsuranceNumber" id="employersSocialInsuranceNumber" size="30" value="" /></td>
	</tr>
	
	<!-- LIMIT PER EMPLOYEE -->
	<tr>
		<td class="label"><?php echo $_SESSION['limitPerEmployee']; ?>:</td>
		<td class="input"><input type="text" name="limitPerEmployee" id="limitPerEmployee" size="30" value="" /></td>
	</tr>
	
	<!-- LIMIT PER EVENT OR SERIES OF EVENTS -->
	<tr>
		<td class="label"><?php echo $_SESSION['limitPerEventOrSeriesOfEvents']; ?>:</td>
		<td class="input"><input type="text" name="limitPerEventOrSeriesOfEvents" id="limitPerEventOrSeriesOfEvents" size="30" value="" /></td>
	</tr>
	
	<!-- LIMIT DURING PERIOD OF INSURANCE -->
	<tr>
		<td class="label"><?php echo $_SESSION['limitDuringPeriodOfInsurance']; ?>:</td>
		<td class="input"><input type="text" name="limitDuringPeriodOfInsurance" id="limitDuringPeriodOfInsurance" size="30" value="" /></td>
	</tr>
	
	<!-- EMPLOYEES NUMBER -->
	<tr>
		<td class="label"><?php echo $_SESSION['employeesNumber']; ?>:</td>
		<td class="input"><input type="text" name="employeesNumber" id="employeesNumber" size="30" value="" /></td>
	</tr>
	
	<!-- ESTIMATED TOTAL GROSS EARNINGS -->
	<tr>
		<td class="label"><?php echo $_SESSION['estimatedTotalGrossEarnings']; ?>:</td>
		<td class="input"><input type="text" name="estimatedTotalGrossEarnings" id="estimatedTotalGrossEarnings" size="30" value="" /></td>
	</tr>
	
	</tbody>
	
	<tbody id="lifeins-info" style="display:none;">
	
	<!-- INSUDER NAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['insuredPersonName']; ?>:</td>
		<td class="input"><input type="text" name="insuredFirstName" id="insuredFirstName" size="30" value="" /></td>
	</tr>
	<tr>
		<td class="label"><?php echo $_SESSION['insuredPersonSurname']; ?>:</td>
		<td class="input"><input type="text" name="insuredLastName" id="insuredLastName" size="30" value="" /></td>
	</tr>			
	
	<!-- PLAN NAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['planName']; ?>:</td>
		<td class="input"><input type="text" name="lifeInsPlanName" id="lifeInsPlanName" size="30" value="" /></td>
	</tr>
	
	<!-- FREQUENCY OF PAYMENT -->
	<tr>
		<td class="label"><?php echo $_SESSION['frequencyOfPayment']; ?>:</td>
		<td class="input"><select name="lifeInsFrequencyOfPayment" id="lifeInsFrequencyOfPayment">
			<?php
			foreach($_SESSION['frequencyOfPaymentOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- ENDORSEMENT -->
	<tr>
		<td class="label"><?php echo $_SESSION['endorsement']; ?>:</td>
		<td class="input"><input type="text" name="lifeInsEndorsement" id="lifeInsEndorsement" size="50" value="" /></td>
	</tr>
	
	<!-- BASIC PLAN -->
	<tr>
		<td class="label"><?php echo $_SESSION['basicPlan']."(".$EURO.")"; ?>:</td>
		<td class="input"><input type="text" name="basicPlanAmount" id="basicPlanAmount" size="30" value="0" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('basicPlanAmount')"/></td>
	</tr>
	
	<!-- TOTAL PERMANENT DISABILITY -->
	<tr>
		<td class="label"><?php echo $_SESSION['totalPermanentDisability']."(".$EURO.")"; ?>:</td>
		<td class="input"><input type="text" name="totalPermanentDisabilityAmount" id="totalPermanentDisabilityAmount" size="30" value="0" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('totalPermanentDisabilityAmount')"/></td>
	</tr>
	
	<!-- PREMIUM PROTECTION -->
	<tr>
		<td class="label"><?php echo $_SESSION['premiumProtection']."(".$EURO.")"; ?>:</td>
		<td class="input"><input type="text" name="premiumProtectionAmount" id="premiumProtectionAmount" size="30" value="0" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('premiumProtectionAmount')" /></td>
	</tr>
	
	</tbody>
	
	<tbody id="medical-info" style="display:none;">
	
	<!-- PLAN NAME -->
	<tr>
		<td class="label"><?php echo $_SESSION['planName']; ?>:</td>
		<td class="input"><input type="text" name="planName" id="planName" size="30" value="" /></td>
	</tr>
	
	<!-- FREQUENCY OF PAYMENT -->
	<tr>
		<td class="label"><?php echo $_SESSION['frequencyOfPayment']; ?>:</td>
		<td class="input"><select name="frequencyOfPayment" id="frequencyOfPayment">
			<?php
			foreach($_SESSION['frequencyOfPaymentOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- PREMIUM -->
	<tr>
		<td class="label"><?php echo $_SESSION['premium']; ?>(<?php echo $EURO;?>):</td>
		<td class="input"><input type="text" name="premium" id="premium" size="30" value="0" /></td>
	</tr>
	
	<!-- MAXIMUM LIMIT -->
	<tr>
		<td class="label"><?php echo $_SESSION['maximumLimit']; ?>:</td>
		<td class="input"><input type="text" name="planMaximumLimit" id="planMaximumLimit" size="30" value="0" /></td>
	</tr>
	
	<!-- DEDUCTIBLE -->
	<tr>
		<td class="label"><?php echo $_SESSION['deductible']; ?>:</td>
		<td class="input"><input type="text" name="deductible" id="deductible" size="30" value="0" /></td>
	</tr>
	
	<!-- CO-INSURANCE PERCENTAGE -->
	<tr>
		<td class="label"><?php echo $_SESSION['coInsurancePercentage']; ?>(%):</td>
		<td class="input"><input type="text" name="coInsurancePercentage" id="coInsurancePercentage" size="30" value="0" /></td>
	</tr>
	
	
	<tr>
		<!-- EMERGENCY TRAVEL ASSISTANCE -->
		<td>
 		<input type="checkbox" name="emergencyTravelAssistance" value="true"><?php echo $_SESSION['emergencyTravelAssistance']; ?>
 		</td>
 		<!-- OUTPATIENT COVERAGE -->
 		<td>
 		<input type="checkbox" name="outpatientCoverage" value="true"><?php echo $_SESSION['outpatientCoverage']; ?>
 		</td>
	</tr>
	
	
	<tr>
		<!-- GENERAL MEDICAL EXAMS -->
		<td>
 			<input type="checkbox" name="generalMedicalExams" value="true"><?php echo $_SESSION['generalMedicalExams']; ?>
 		</td>
 		<!-- EXTENTION OF COVER ABROAD -->
		<td>
 			<input type="checkbox" name="extentionOfCoverAbroad" value="true"><?php echo $_SESSION['extentionOfCoverAbroad']; ?>
 		</td>
	</tr>
	
	</tbody>
	
	<tbody id="property-fire-info" style="display:none;">
	
	<!-- PROPERTY TYPE -->
	<tr>
		<td class="label"><?php echo $_SESSION['propertyType']; ?></td>
		<td class="input"><select name="propertyType" id="propertyType">
			<?php
			foreach($_SESSION['propertyTypeOptions'] as $option){
				?>
				 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
				 <?php
			 }
			 ?>
			</select>
		</td>
	</tr>
	
	<!-- DESCRIPTION -->
	<tr>
		<td class="label"><?php echo $_SESSION['description']; ?>:</td>
		<td class="input"><input type="text" name="description" id="description" size="30" value="" /></td>
	</tr>
	
	<!-- BUILDING VALUE -->
	<tr>
		<td class="label"><?php echo $_SESSION['buildingValue']; ?>(<?php echo $EURO;?>):</td>
		<td class="input"><input type="text" name="buildingValue" id="buildingValue" size="30" value="0" /></td>
	</tr>
	
	<!-- OUTSIDE FIXTURES VALUE -->
	<tr>
		<td class="label"><?php echo $_SESSION['outsideFixturesValue']; ?>(<?php echo $EURO;?>):</td>
		<td class="input"><input type="text" name="outsideFixturesValue" id="outsideFixturesValue" size="30" value="0" /></td>
	</tr>
	
	<!-- CONTENTS VALUE -->
	<tr>
		<td class="label"><?php echo $_SESSION['contentsValue']; ?>(<?php echo $EURO;?>):</td>
		<td class="input"><input type="text" name="contentsValue" id="contentsValue" size="30" value="0" /></td>
	</tr>
	
	<!-- VALUABLE OBJECTS VALUE -->
	<tr>
		<td class="label"><?php echo $_SESSION['valuableObjectsValue']; ?>(<?php echo $EURO;?>):</td>
		<td class="input"><input type="text" name="valuableObjectsValue" id="valuableObjectsValue" size="30" value="0" /></td>
	</tr>
	
	<!-- YEAR BUILD -->
	<tr>
		<td class="label"><?php echo $_SESSION['yearBuilt']; ?>:</td>
		<td class="input"><select name="yearBuilt" id="yearBuilt" style="width:230px;">
			<?php
			$currentYear = date("Y");
			for($i=$currentYear; $i>1900; $i--){
				?>
				 <option  value=<?php echo $i ?> ><?php echo "$i" ?> </option>
				 <?php
			 }
			 ?>
			 </select>
		</td>
	</tr>
	
	<!-- AREA -->
	<tr>
		<td class="label"><?php echo $_SESSION['area']."(".$_SESSION['squareMeters']."):"; ?></td>
		<td class="input"><input type="text" name="areaSqMt" id="areaSqMt" size="30" value="0" /></td>
	</tr>
	
	</tbody><!--property-fire-info-->
	
	<!-- empty lines -->
	<tr><td><br /></td></tr>
	<tr><td><br /></td></tr>
	<!-- USER INFO -->
	<tr><td class="userInfo"><?php echo $_SESSION['anyOtherImportantDetails']; ?></td></tr>
	<tr><td class="userInfo"><textarea name="userInfo" rows="10" cols="50" value=""><?php echo $userInfo_content; ?></textarea></td></tr>
																			
	<!-- CONTINUE BUTTON -->						
	<tr>
		<td><br /></td>
		<p><td><input type="submit" name="send" class="button" value="<?php echo $_SESSION['submitButton']; ?>" size="30" /></td></p>
	</tr>
	
	
</table>

</form>
