<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
    

//echo "flow is ".$_SESSION['flow'];

//CASE: creating contract from An existing user -> New Contact => set existing proposer variables
//if(isset($_SESSION['calledFromUserNewContract']) && ($_SESSION['calledFromUserNewContract']==true))
if(isset($_SESSION['flow']))
{
	
	//CASE: creating contract from Your Account -> New Contact. On first call, $_SESSION['proposer'] is not set
	//else if(isset($_SESSION['calledFromMyAccountNewContract']) && ($_SESSION['calledFromMyAccountNewContract']==true))
	if( $_SESSION['flow']==$MY_ACCOUNT_NEW_CONTRACT_FLOW )
	{
		//echo "CREATING NEW CONTRACT FROM MY ACCOUNT FLOW";
		$firstName_content = "";
		$lastName_content = "";
		$stateId_content = "";
		$telephone_content = "";
		$cellphone_content = "";	
		/*
		$ownerBirthDateYear_content = 1985;
		$ownerBirthDateMonth_content = 5;
		$ownerBirthDateDay_content = 1;
		$licenseDateYear_content = 2004;
		$licenseDateMonth_content = 5;
		$licenseDateDay_content = 1;
		*/
		$licenseCountry_content = "Cyprus";
		$licenseType_content = "0";
		$ownerProfession_content = 'Programmer';
		$proposerType_content = $PROPOSER_TYPE_PERSON;
	}
	//Case - creating a contract from an existing proposer
	else
	{
		//echo "CREATING NEW CONTRACT FROM USER FLOW";
		$firstName_content = $_SESSION['proposer']->owner->firstName;
		$lastName_content = $_SESSION['proposer']->owner->lastName;
		$stateId_content = $_SESSION['proposer']->owner->stateId;
		$telephone_content = $_SESSION['proposer']->owner->telephone;
		$cellphone_content = $_SESSION['proposer']->owner->cellphone;
		$ownerProfession_content = $_SESSION['proposer']->owner->profession;
		/*
		$ownerBirthDateYear_content = substr($_SESSION['proposer']->owner->birthDate, 6, 4);
		$ownerBirthDateMonth_content = substr($_SESSION['proposer']->owner->birthDate, 3, 2);
		$ownerBirthDateDay_content = substr($_SESSION['proposer']->owner->birthDate, 0, 2);
		$licenseDateYear_content = substr($_SESSION['proposer']->license->licenseDate, 6, 4);
		$licenseDateMonth_content = substr($_SESSION['proposer']->license->licenseDate, 3, 2);
		$licenseDateDay_content = substr($_SESSION['proposer']->license->licenseDate, 0, 2);*/
		$licenseCountry_content = $_SESSION['proposer']->license->licenseCountry;
		
		$licenseType_content = $_SESSION['proposer']->license->licenseType;
		$proposerType_content = $_SESSION['proposer']->owner->proposerType;
		
		
		//$_SESSION['proposer']->license->printData();
	}
}
//$regNumber_content = $_SESSION['proposer']->vehicle->regNumber;
//echo "state id is $stateId_content";

if(isset($_SESSION['ownerEthnicity'])){
		$ownerEthnicity_content = $_SESSION['ownerEthnicity'];
}
else{
	$ownerEthnicity_content = "Cyprus";
} 

if(isset($_SESSION['insuranceCompany'])){
		$insuranceCompany_content = $_SESSION['insuranceCompany'];
}
else
	$insuranceCompany_content = "NONE";
	
	
if(isset($_SESSION['vehicleType'])){
		$vehicleType_content = $_SESSION['vehicleType'];
}
else
	$vehicleType_content = "1";

if(isset($_SESSION['vehicleMake']))
	$vehicleMake_content = $_SESSION['vehicleMake'];
else
	$vehicleMake_content = 'Mitsubishi';
	
if(isset($_SESSION['vehicleModel']))
	$vehicleModel_content = $_SESSION['vehicleModel'];
else
	$vehicleModel_content = 'Mitsubishi Colt';
		
if(isset($_SESSION['cubicCapacity']))
	$cubicCapacity_content = $_SESSION['cubicCapacity'];
else
	$cubicCapacity_content = '1200';
		
if(isset($_SESSION['vehicleManufacturedYear']))
	$vehicleManufacturedYear_content = $_SESSION['vehicleManufacturedYear'];
else
	$vehicleManufacturedYear_content = '2009';
	
if(isset($_SESSION['steeringWheelSide'])){
		$steeringWheelSide_content = $_SESSION['steeringWheelSide'];
}
else
	$steeringWheelSide_content = "0";
	
if(isset($_SESSION['isSportsModel'])){
		$isSportsModel_content = $_SESSION['isSportsModel'];
}
else
	$isSportsModel_content = "0";


if(isset($_SESSION['hasPreviousInsurance'])){
		$hasPreviousInsurance_content = $_SESSION['hasPreviousInsurance'];
}
else
	$hasPreviousInsurance_content = "NO";
	


	
if(isset($_SESSION['vehicleDesign'])){
	$vehicleDesign_content = $_SESSION['vehicleDesign'];
}
else
	$vehicleDesign_content = "0";

if(isset($_SESSION['isTaxFree'])){
	$isTaxFree_content = $_SESSION['isTaxFree'];
}
else
	$isTaxFree_content = "0";
	
if(isset($_SESSION['isUsedForDeliveries'])){
	$isUsedForDeliveries_content = $_SESSION['isUsedForDeliveries'];
}
else
	$isUsedForDeliveries_content = "0";
	
if(isset($_SESSION['hasVisitorPlates'])){
	$hasVisitorPlates_content = $_SESSION['hasVisitorPlates'];
}
else
	$hasVisitorPlates_content = "0";
	
if(isset($_SESSION['coverageType'])){
	$coverageType_content = $_SESSION['coverageType'];
}
else
	$coverageType_content = "0";

if(isset($_SESSION['namedDriversNumber'])){
	$namedDriversNumber_content = $_SESSION['namedDriversNumber'];
}
else
	$namedDriversNumber_content = "0";

if(isset($_SESSION['additionalExcess'])){
	$additionalExcess_content = $_SESSION['additionalExcess'];
}
else
	$additionalExcess_content = "0";
	
if(isset($_SESSION['noClaimDiscountYears'])){
	$noClaimDiscountYears_content = $_SESSION['noClaimDiscountYears'];
}
else
	$noClaimDiscountYears_content = "0";
	
/* 19/09/2012 remove		
if(isset($_SESSION['hasLearnersLicence']))
		$hasLearnersLicence_content = $_SESSION['hasLearnersLicence'];
else
	$hasLearnersLicence_content = '';
	
*/
	
if(isset($_SESSION['normalDrivingLicenseTotalYearsForAdditionalDrivers']))
		$normalDrivingLicenseTotalYearsForAdditionalDrivers_content = $_SESSION['normalDrivingLicenseTotalYearsForAdditionalDrivers'];
else
	$normalDrivingLicenseTotalYearsForAdditionalDrivers_content = '';
		
if(isset($_SESSION['ageOfYoungestDriver']))
		$ageOfYoungestDriver_content = $_SESSION['ageOfYoungestDriver'];
else
	$ageOfYoungestDriver_content = '';
	
if(isset($_SESSION['ageOfOldestDriver']))
		$ageOfOldestDriver_content = $_SESSION['ageOfOldestDriver'];
else
	$ageOfOldestDriver_content = '';

//hardcode to use up to 4 accidents - must define ALL variables here
for($i=0; $i<4; $i++)
{
	$var = "claim".($i+1);//string concatenation		
	${$var."_content"} = 0;
	$var = "claim".($i+1)."Date"."Year";//string concatenation
	${$var."_content"} = 2010;
	$var = "claim".($i+1)."Date"."Month";//string concatenation
	${$var."_content"} = 1;
	$var = "claim".($i+1)."Date"."Day";//string concatenation
	${$var."_content"} = 1;
			
}
	
if(isset($_SESSION['coverageAmount']))
	$coverageAmount_content = $_SESSION['coverageAmount'];
else
		$coverageAmount_content = 0;


if(isset($_SESSION['userInfo']))
		$userInfo_content = $_SESSION['userInfo'];
else
	$userInfo_content = '';
	
if(isset($_SESSION['offers']))
		$offers_content = $_SESSION['offers'];
else
	$offers_content = '';
	
	
//VALUES SPECIFIC FOR CONTRACT
/*
$saleStartDateYear_content = date("Y");
$saleStartDateMonth_content = date("m");
$saleStartDateDay_content = date("d");

$saleEndDateYear_content = date("Y")+1;
$saleEndDateMonth_content = date("m");
$saleEndDateDay_content = date("d")-1;
//todo - make this complete.
if($saleEndDateDay_content==0)
{
	$saleEndDateMonth_content = date("m")-1;
	$saleEndDateDay_content = 30;
}
*/
	
?>