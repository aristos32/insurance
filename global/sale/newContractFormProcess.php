<?php	

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

		
global $YES_CONSTANT,$NO_CONSTANT;

//STEP 1 - READ DATA

//READ VARIABLES THAT APPLY TO ALL INSURANCE TYPES

//store in the Session All the variables from the previous form
if(isset($_POST['saleStartDate']))
	$_SESSION['saleStartDate'] = @$_POST['saleStartDate'];

if(isset($_POST['saleEndDate']))
	$_SESSION['saleEndDate'] = @$_POST['saleEndDate'];

/*
if(isset($_POST['saleStartDate']))
{
	$_SESSION['saleStartDateYear'] = substr(@$_POST['saleStartDate'], 0, 4);
	$_SESSION['saleStartDateMonth'] = substr(@$_POST['saleStartDate'], 5, 2);
	$_SESSION['saleStartDateDay'] = substr(@$_POST['saleStartDate'], 8, 2);
}


if(isset($_POST['saleEndDate']))
{
	$_SESSION['saleEndDateYear'] = substr(@$_POST['saleEndDate'], 0, 4);
	$_SESSION['saleEndDateMonth'] = substr(@$_POST['saleEndDate'], 5, 2);
	$_SESSION['saleEndDateDay'] = substr(@$_POST['saleEndDate'], 8, 2);
}*/

if(isset($_POST['contractNumber']))
	$_SESSION['contractNumber'] = @$_POST['contractNumber'];
	
if(isset($_POST['firstName']))
	$_SESSION['firstName'] = @$_POST['firstName'];
if(isset($_POST['lastName']))
	$_SESSION['lastName'] = @$_POST['lastName'];
if(isset($_POST['stateId']))
	$_SESSION['stateId'] = @$_POST['stateId'];
if(isset($_POST['telephone']))
	$_SESSION['telephone'] = @$_POST['telephone'];
if(isset($_POST['cellphone']))
	$_SESSION['cellphone'] = @$_POST['cellphone'];

if(isset($_POST['ownerBirthDate']))
	$_SESSION['ownerBirthDate'] = @$_POST['ownerBirthDate'];
/*
if(isset($_POST['ownerBirthDate']))
{
	$_SESSION['ownerBirthDateYear'] = substr(@$_POST['ownerBirthDate'], 0, 4);
	$_SESSION['ownerBirthDateMonth'] = substr(@$_POST['ownerBirthDate'], 5, 2);
	$_SESSION['ownerBirthDateDay'] = substr(@$_POST['ownerBirthDate'], 8, 2);
}*/
if(isset($_POST['ownerEthnicity']))
	$_SESSION['ownerEthnicity'] = @$_POST['ownerEthnicity'];
if(isset($_POST['ownerProfession']))
	$_SESSION['ownerProfession'] = @$_POST['ownerProfession'];
if(isset($_POST['insuranceType']))
	$_SESSION['insuranceType'] = @$_POST['insuranceType'];	
if(isset($_POST['proposerType']))
	$_SESSION['proposerType'] = @$_POST['proposerType'];	
if(isset($_POST['coverageType']))
		$_SESSION['coverageType'] = @$_POST['coverageType'];
			
$contractInfo = new contract();

$contractInfo->owner = new owner();	
$contractInfo->owner->firstName = $_SESSION['firstName'];
$contractInfo->owner->lastName = $_SESSION['lastName'];
$contractInfo->owner->stateId = $_SESSION['stateId'];
$contractInfo->owner->type = $_SESSION['type'];
$contractInfo->owner->oldStateId = $_SESSION['stateId'];
$contractInfo->owner->telephone = $_SESSION['telephone'];
$contractInfo->owner->cellphone = $_SESSION['cellphone'];
$contractInfo->owner->profession = $_SESSION['ownerProfession'];
$contractInfo->owner->countryOfBirth = $_SESSION['ownerEthnicity'];
$contractInfo->owner->birthDate = date("Y-m-d H:i:s", strtotime($_SESSION['ownerBirthDate']));
$contractInfo->owner->proposerType = $_SESSION['proposerType'];
	
$contractInfo->sale = new sale();
$contractInfo->sale->saleId = trim($_SESSION['contractNumber']);
$contractInfo->sale->insuranceType = $_SESSION['insuranceType'];
$contractInfo->sale->coverageType = $_SESSION['coverageType'];
$contractInfo->sale->saleStartDate = date("Y-m-d H:i:s", strtotime($_SESSION['saleStartDate']));
$contractInfo->sale->saleEndDate = date("Y-m-d H:i:s", strtotime($_SESSION['saleEndDate']));
$contractInfo->sale->oldSaleId = $_SESSION['contractNumber'];
$contractInfo->sale->associate = $NONE;
$contractInfo->sale->producer = $_SESSION['username'];
$contractInfo->sale->stateId = $contractInfo->owner->stateId;
$contractInfo->sale->status = $SALE_STATUS_ACTIVE;//default when we insert new contract

//$contractInfo->contractNumber = $_SESSION['contractNumber'];

$contractInfo->contractType = "INSURANCE";//type of sale is insurance


//CASE - MOTOR INSURANCE TYPE
if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_MOTOR)
{
	$contractInfo->motor = new motor();	

	if(isset($_POST['licenseType']))
		$_SESSION['licenseType'] = @$_POST['licenseType'];
	if(isset($_POST['licenseCountry']))
		$_SESSION['licenseCountry'] = @$_POST['licenseCountry'];
	if(isset($_POST['licenseDate']))
		$_SESSION['licenseDate'] = @$_POST['licenseDate'];
	/*
	if(isset($_POST['licenseDate']))
	{
		$_SESSION['licenseDateYear'] = substr(@$_POST['licenseDate'], 0, 4);
		$_SESSION['licenseDateMonth'] = substr(@$_POST['licenseDate'], 5, 2);
		$_SESSION['licenseDateDay'] = substr(@$_POST['licenseDate'], 8, 2);
	}*/
	if(isset($_POST['vehicleType']))
		$_SESSION['vehicleType'] = @$_POST['vehicleType'];
	if(isset($_POST['cubicCapacity']))
		$_SESSION['cubicCapacity'] = @$_POST['cubicCapacity'];
	if(isset($_POST['vehicleManufacturedYear']))
		$_SESSION['vehicleManufacturedYear'] = @$_POST['vehicleManufacturedYear'];
	if(isset($_POST['steeringWheelSide']))
		$_SESSION['steeringWheelSide'] = @$_POST['steeringWheelSide'];					
	if(isset($_POST['isSportsModel']))
		$_SESSION['isSportsModel'] = @$_POST['isSportsModel'];
	if(isset($_POST['vehicleDesign']))
		$_SESSION['vehicleDesign'] = @$_POST['vehicleDesign'];
	if(isset($_POST['isTaxFree']))
		$_SESSION['isTaxFree'] = @$_POST['isTaxFree'];
	if(isset($_POST['isUsedForDeliveries']))
		$_SESSION['isUsedForDeliveries'] = @$_POST['isUsedForDeliveries'];					
	if(isset($_POST['vehicleModel']))
		$_SESSION['vehicleModel'] = @$_POST['vehicleModel'];
	if(isset($_POST['vehicleMake']))
		$_SESSION['vehicleMake'] = @$_POST['vehicleMake'];
	if(isset($_POST['regNumber']))
		$_SESSION['regNumber'] = @$_POST['regNumber'];
	
	/* coverageAmount exists only when coverageType is not THIRD PARTY */
	if($_SESSION['coverageType'] != $COVERAGE_TYPE_THIRD_PARTY )
		$_SESSION['coverageAmount'] = @$_POST['coverageAmount'];
	
	/* exist only when coverageType is COVERAGE_TYPE_COMPREHENSIVE */
	if($_SESSION['coverageType'] == $COVERAGE_TYPE_COMPREHENSIVE )
	{
		$_SESSION['namedDriversNumber'] = @$_POST['namedDriversNumber'];
		$_SESSION['additionalExcess'] = @$_POST['additionalExcess'];
		$_SESSION['noClaimDiscountYears'] = @$_POST['noClaimDiscountYears'];
	}
	
	/*get all needed information from the session*/
	$contractInfo->motor->coverageType = $_SESSION['coverageType'];
	
	$contractInfo->motor->license = new license();
	$contractInfo->motor->license->licenseType = date("Y-m-d H:i:s", strtotime($_SESSION['licenseType']));
	$contractInfo->motor->license->licenseDate = $_SESSION['licenseDate'];
	$contractInfo->motor->license->licenseCountry = $_SESSION['licenseCountry'];
	$contractInfo->motor->license->stateId = $contractInfo->owner->stateId;
	$contractInfo->motor->license->oldStateId = $contractInfo->owner->oldStateId;
	
	$contractInfo->motor->vehicle = new vehicle();
	$contractInfo->motor->vehicle->regNumber = $_SESSION['regNumber'];
	$contractInfo->motor->vehicle->saleId = $contractInfo->sale->saleId;
	//if(isset($_SESSION['saleId']))
	//	$contractInfo->motor->vehicle->oldSaleId = $_SESSION['saleId'];
	$contractInfo->motor->vehicle->make = $_SESSION['vehicleMake'];
	$contractInfo->motor->vehicle->vehicleType = $_SESSION['vehicleType'];
	$contractInfo->motor->vehicle->model = $_SESSION['vehicleModel'];
	$contractInfo->motor->vehicle->cubicCapacity = $_SESSION['cubicCapacity'];
	$contractInfo->motor->vehicle->manufacturedYear = $_SESSION['vehicleManufacturedYear'];
	$contractInfo->motor->vehicle->sumInsured = @$_SESSION['coverageAmount'];
	$contractInfo->motor->vehicle->vehicleDesign = $_SESSION['vehicleDesign'];
	$contractInfo->motor->vehicle->steeringWheelSide = $_SESSION['steeringWheelSide'];
	$contractInfo->motor->vehicle->isTaxFree = @$_SESSION['isTaxFree'];
	$contractInfo->motor->vehicle->isUsedForDeliveries = $_SESSION['isUsedForDeliveries'];
	$contractInfo->motor->vehicle->isSportsModel = @$_SESSION['isSportsModel'];
	
	//mandatory coverages for MOTOR
	$localCoverageInPolicy = new coveragesInPolicyQuotation();
	$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$localCoverageInPolicy->code = 'bodilyDamageOrDeath';
	$localCoverageInPolicy->parameters[] = '30.000.000';//hardcode for now
	$contractInfo->motor->coveragesInPolicyArray[] = $localCoverageInPolicy;
	
	$localCoverageInPolicy = new coveragesInPolicyQuotation();
	$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$localCoverageInPolicy->code = 'thirdPartyPropertyDamage';
	$localCoverageInPolicy->parameters[] = '1.000.000';//hardcode for now
	$contractInfo->motor->coveragesInPolicyArray[] = $localCoverageInPolicy;
	
	$localCoverageInPolicy = new coveragesInPolicyQuotation();
	$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$localCoverageInPolicy->code = 'drivingBeyondTheRoad';
	$contractInfo->motor->coveragesInPolicyArray[] = $localCoverageInPolicy;
	
	//$contractInfo->printData();
	
}
//CASE : EMPLOYERS LIABILITY TYPE
else if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_EMPLOYER_LIABILITY)
{
	$contractInfo->employersLiability = new employersLiability();
	$contractInfo->employersLiability->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$contractInfo->employersLiability->employersSocialInsuranceNumber = @$_POST['employersSocialInsuranceNumber'];
	$contractInfo->employersLiability->limitPerEmployee = @$_POST['limitPerEmployee'];
	$contractInfo->employersLiability->limitPerEventOrSeriesOfEvents = @$_POST['limitPerEventOrSeriesOfEvents'];	
	$contractInfo->employersLiability->limitDuringPeriodOfInsurance = @$_POST['limitDuringPeriodOfInsurance'];
	$contractInfo->employersLiability->employeesNumber = @$_POST['employeesNumber'];
	$contractInfo->employersLiability->estimatedTotalGrossEarnings = @$_POST['estimatedTotalGrossEarnings'];
	
}
//CASE : EMPLOYERS LIABILITY TYPE
else if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_LIFE)
{
	$contractInfo->lifeIns = new lifeIns();
	$contractInfo->lifeIns->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$contractInfo->lifeIns->insuredFirstName = @$_POST['insuredFirstName'];
	$contractInfo->lifeIns->insuredLastName = @$_POST['insuredLastName'];
	$contractInfo->lifeIns->planName = @$_POST['lifeInsPlanName'];	
	$contractInfo->lifeIns->frequencyOfPayment = @$_POST['lifeInsFrequencyOfPayment'];
	$contractInfo->lifeIns->endorsement = @$_POST['lifeInsEndorsement'];
	$contractInfo->lifeIns->basicPlanAmount = @$_POST['basicPlanAmount'];
	$contractInfo->lifeIns->totalPermanentDisabilityAmount = @$_POST['totalPermanentDisabilityAmount'];
	$contractInfo->lifeIns->premiumProtectionAmount = @$_POST['premiumProtectionAmount'];
	
}
//CASE : PROPERTY FIRE
else if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_FIRE_PROPERTY)
{
	$contractInfo->propertyFire = new propertyFire();
	$contractInfo->propertyFire->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$contractInfo->propertyFire->description = @$_POST['description'];
	$contractInfo->propertyFire->typeOfPremises = @$_POST['propertyType'];
	$contractInfo->propertyFire->buildingValue = @$_POST['buildingValue'];	
	$contractInfo->propertyFire->outsideFixturesValue = @$_POST['outsideFixturesValue'];
	$contractInfo->propertyFire->contentsValue = @$_POST['contentsValue'];
	$contractInfo->propertyFire->valuableObjectsValue = @$_POST['valuableObjectsValue'];
	$contractInfo->propertyFire->yearBuilt = @$_POST['yearBuilt'];
	$contractInfo->propertyFire->areaSqMt = @$_POST['areaSqMt'];
	
	//add mandatory coverages for each FIRE policy
	$localCoverageInPolicy = new coveragesInPolicyQuotation();
	$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$localCoverageInPolicy->code = 'explosion';
	$contractInfo->propertyFire->coveragesInPolicyArray[] = $localCoverageInPolicy;
	
	$localCoverageInPolicy = new coveragesInPolicyQuotation();
	$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$localCoverageInPolicy->code = 'fire';
	$contractInfo->propertyFire->coveragesInPolicyArray[] = $localCoverageInPolicy;
	
	$localCoverageInPolicy = new coveragesInPolicyQuotation();
	$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$localCoverageInPolicy->code = 'lightning';
	$contractInfo->propertyFire->coveragesInPolicyArray[] = $localCoverageInPolicy;
	
}

//CASE : MEDICAL INSURANCE TYPE
else if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_MEDICAL)
{
	$contractInfo->medical = new medical();
	$contractInfo->medical->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$contractInfo->medical->planName = @$_POST['planName'];
	$contractInfo->medical->frequencyOfPayment = @$_POST['frequencyOfPayment'];
	$contractInfo->medical->premium = @$_POST['premium'];
	$contractInfo->medical->planMaximumLimit = @$_POST['planMaximumLimit'];
	$contractInfo->medical->deductible = @$_POST['deductible'];
	$contractInfo->medical->coInsurancePercentage = @$_POST['coInsurancePercentage'];
	
	//add only selected coverages
	//mandatory
	$localCoverageInPolicy = new coveragesInPolicyQuotation();
	$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
	$localCoverageInPolicy->code = 'inPatientCoverage';//INPATIENT is mandatory. Add always
	$contractInfo->medical->coveragesInPolicyArray[] = $localCoverageInPolicy;
	
	if(@$_POST['emergencyTravelAssistance']=='true')
	{
		$localCoverageInPolicy = new coveragesInPolicyQuotation();
		$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
		$localCoverageInPolicy->code = 'emergencyTravelAssistance';
		$contractInfo->medical->coveragesInPolicyArray[] = $localCoverageInPolicy;
	}
	if(@$_POST['outpatientCoverage']=='true')
	{
		$localCoverageInPolicy = new coveragesInPolicyQuotation();
		$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
		$localCoverageInPolicy->code = 'outpatientCoverage';
		$contractInfo->medical->coveragesInPolicyArray[] = $localCoverageInPolicy;
	}
	if(@$_POST['generalMedicalExams']=='true')
	{
		$localCoverageInPolicy = new coveragesInPolicyQuotation();
		$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
		$localCoverageInPolicy->code = 'generalMedicalExams';
		$contractInfo->medical->coveragesInPolicyArray[] = $localCoverageInPolicy;
	}
	if(@$_POST['extentionOfCoverAbroad']=='true')
	{
		$localCoverageInPolicy = new coveragesInPolicyQuotation();
		$localCoverageInPolicy->saleId = $contractInfo->sale->saleId;//$contractInfo->contractNumber;
		$localCoverageInPolicy->code = 'extentionOfCoverAbroad';
		$contractInfo->medical->coveragesInPolicyArray[] = $localCoverageInPolicy;
	}
	
		
}

//STEP 2 - INSERT DATA
/* OWNER TABLE */
//$owner = fillOwnerClass($contractInfo);
//if duplicate owner, this will fail. But we already have entry in insertQuotationOwner, so it is OK
//if duplicate owner, this will fail. But we already have entry in insertQuotationOwner, so it is OK
$insertOwnerError = 0;
if(checkEntryExistsInTable('owner', 'stateId', $contractInfo->owner->stateId, '', '')==0){
	$insertOwnerError = insertOwner($owner);
}
//$insertOwnerError = insertOwner($contractInfo->owner);

//when owner already exists, update his data
if($insertOwnerError != 0)
	updateOwnerData($contractInfo->owner);

//$sale = fillSaleClass($contractInfo);
/* INSERT SALE */
$localError = insertSaleInfo($contractInfo->sale);

//INSERT rest only if sale is successfull - Sale fails only when it's duplicate entry
if($localError != -1 )
{
	//CASE - MOTOR INSURANCE TYPE
	if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_MOTOR)
	{
		//insert license info only of proposer type is PERSON
		if($contractInfo->owner->proposerType==$PROPOSER_TYPE_PERSON)
		{
			/*LICENSE TABLE*/
			//$license = fillLicenseClass($contractInfo);
				
			insertOwnerLicense($contractInfo->motor->license);
		}
					
		/* VEHICLE TABLE */
		//$vehicle = fillVehicleClass($contractInfo);
		//if duplicate owner, this will fail. But we already have entry in insertQuotationOwner, so it is OK
		//$localError = insertVehicle($contractInfo->motor->vehicle);//NEEDED?
		//echo "Inserting vehicle. Error:".$localError->error.", Number:".$localError->errno;
		
		$contractInfo->motor->vehicle->saleId = $contractInfo->sale->saleId;
		$contractInfo->motor->vehicle->oldSaleId = $contractInfo->sale->saleId;//aristosa 23/02/2012
		
		//insert after the sale, since we have a foreign key dependency
		//updateVehicleData($vehicle);
		insertVehicle($contractInfo->motor->vehicle);
		
		/* insert coverages in coveragesInPolicy */
	    foreach($contractInfo->motor->coveragesInPolicyArray as $eachCoverage)
	      	insertCoverageInPolicy($eachCoverage);
		
	}
	//CASE : EMPLOYERS LIABILITY TYPE
	else if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_EMPLOYER_LIABILITY)
	{
		/* EMPLOYERSLIABILITY TABLE */
		$localError = insertEmployersLiability($contractInfo->employersLiability);
	}
	
	//CASE : MEDICAL TYPE
	else if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_MEDICAL)
	{
		/* MEDICAL AND MEDICALINSUREDPERSON TABLE */
		$localError = insertMedical($contractInfo->medical);
	}
	//CASE : LIFE TYPE
	else if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_LIFE)
	{
		/* LIFE */
		$localError = insertLifeIns($contractInfo->lifeIns);
	}
	//CASE : PROPERTY FIRE TYPE
	else if($contractInfo->sale->insuranceType==$INSURANCE_TYPE_FIRE_PROPERTY)
	{
		/* PROPERTYFIRE AND ENDORSEMENT(NOT DONE YET) TABLES */
		//$contractInfo->propertyFire->printData();
		$localError = insertPropertyFire($contractInfo->propertyFire);
	}
	
	
	?>
	Contract Added Successfully. 
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="updateContractData" method="post" style="display: none;">
		<input type="text" name="action" value="updateContractData" />
		<input type="text" name="saleId" value="<?php echo $contractInfo->sale->saleId;?>" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('updateContractData').submit()"><?php echo $_SESSION['view'];?></a>
	<?php
		
}
//error happened 
else
{	
	?>
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="newContractForm" method="POST" style="display: none;">
	<input type="text" name="action" value="newContractForm" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('newContractForm').submit()">Try Again</a>
	
	<?php
	
}
?>
