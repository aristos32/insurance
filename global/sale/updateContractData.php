<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//remainder of the last stored transcaction
$previousRemainder = 0;

//retrieve the quoteID
$currentSaleId;
//called using 'View' in displayPreviousQuotes.php
if (!empty($_POST['saleId'])) {
	$currentSaleId = $_POST['saleId'];
	$_SESSION['saleId'] = $currentSaleId;//update session
	//echo "inside if quoteid = $currentQuoteId <br>";
}

//echo "saleid is ".$_SESSION['saleId'];
$parameterNameValueArray = array();

$parameterNameValue = new parameterNameValue();
$parameterNameValue->name =" s.saleId ";
$parameterNameValue->value = $_SESSION['saleId'];

$parameterNameValueArray[] = $parameterNameValue;

$allContracts = array();

$allContracts = retrieveContractInfo($parameterNameValueArray, 0);

$allProducers = retrieveAllProducers();

//ONLY ONE CONTRACT EXISTS AT THIS POINT - SO USE THE FIRST ONE ON ARRAY
$contract = new contract();

$contract = $allContracts[0];

//set current state id
$_SESSION['searchStateId'] = $contract->owner->stateId;

$_SESSION['currentInsuranceType'] = $contract->sale->insuranceType;//store, in case there is a change in insurance type, then all the previous coverages are deleted.

$_SESSION['producer'] = $contract->sale->producer;
//$contract->printData();

include $_SESSION['globalFilesLocation']."/javascript/javascriptfunctions.php";

?>



<script>	
    
$(document).ready(function(){
    $("#saleStartDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#saleStartDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#saleStartDate").datepicker('setDate', "<?php echo $contract->sale->saleStartDate; ?>");
    
    $("#saleEndDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#saleEndDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#saleEndDate").datepicker('setDate', "<?php echo $contract->sale->saleEndDate; ?>");

    $("#birthDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-90:+10'});
    $('#birthDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#birthDate").datepicker('setDate', "<?php echo $contract->owner->birthDate; ?>");

    $("#licenseDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#licenseDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#licenseDate").datepicker('setDate', "<?php echo $contract->motor->license->licenseDate; ?>"); 

    $('#transDate1').datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10',dateFormat: 'dd-mm-yy'});	   	   
    
    $('#notesDate1').datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#notesDate1').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#notesDate1").datepicker('setDate', "<?php echo date('d-m-Y');?>");

    $('#claimDate1').datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#claimDate1').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#claimDate1").datepicker('setDate', "<?php echo date('d-m-Y');?>");  



    var tableRowsNum = $('#medicalInsuredPersonsTable tr').length;
    
    //initialize
    for (var i=0;i<tableRowsNum-1;i++)
    {
  	   $('#birthDate_'+i).datepicker({ showOn: 'button', 
  	  	   								buttonImageOnly: true, 
  	  	   								buttonImage: './images/icon_cal.png', 
  	  	   								changeYear: true, 
  	  	   								dateFormat: 'dd-mm-yy',
  	  	   								yearRange: '-90:+10'});
  	   //$('#birthDate_'+i).datepicker('option', 'dateFormat', 'dd-mm-yy');
    }
   
   

    //bind a click event to the "Add" link
    //$('#addnew').live("click", function() { SOS - for loading js before html dom is fully loaded. This works and outside the ready function
  	$('#addnew').click(function() {
  	  	//$(".datepick").datepicker();
  	  	var newRowNum = 0;
  	
  	 // Detach all datepickers before cloning - need to manually remove 'id' since it's not destroyed
  	   $(".datepick").datepicker("destroy").removeAttr('id');
  	
  	   // increment the counter
  	   newRowNum = $(medicalInsuredPersonsTable).children('tbody').children('tr').length + 1;
  	   
  	   
  	   // get the entire "Add" row --
  	   // "this" refers to the clicked element
  	   // and "parent" moves the selection up
  	   // to the parent node in the DOM
  	   var addRow = $(this).parent().parent();
  	
  	   // copy the entire row from the DOM with "clone"
  	   var newRow = addRow.clone(true);
  	
  	   // set the values of the inputs in the "Add" row to empty strings
  	   $('input', addRow).val('');
  	
  	   // insert a remove link in the last cell
  	   $('td:last-child', newRow).html('<a href="" class="remove"><i class="icon-minus"><\/i><\/a>');
  	   
  	
  	   // insert the new row into the table "before" the Add row
  	   addRow.before(newRow);
  	
  	
  	    $(".datepick").datepicker({ showOn: 'button', dateFormat: 'dd-mm-yy', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
  	   
  	   
  	   // add the remove function to the new row
  	   $('a.remove', newRow).click(function() {
  	       $(this).closest('tr').remove();
  	       return false;
  	   });
  	
  	   //newRowNum = $('#additionalDriversTable tr').length - 2;
  	   
  	   
  	   $('#birthDate', newRow).each(function() {
  	       var newID = 'birthDate_' + newRowNum;
  	       $(this).attr('id', newID);
  	
     });

       
     // prevent the default click
     return false;
    });


  	//remove's default rows
  	$('.removeDefault').click(function() {
  	    $(this).closest('tr').remove();
  	    return false;
  	});
    
});




//AJAX Functionality	
/* fill specific data, before calling the generic AJAX functions */

//not used now. just for compatibility between browser first versions. probably not needed anymore.
function createNamedElement(type, name) {
   var element = null;
   // Try the IE way; this fails on standards-compliant browsers
   try {
      element = document.createElement('<'+type+' name="'+name+'">');
   } catch (e) {
   }
   if (!element || element.nodeName != type.toUpperCase()) {
      // Non-IE browser; use canonical method to create named element
      element = document.createElement(type);
      element.name = name;
   }
   return element;
}

//function NOT USED now. But keep for KNOWLEDGE BASE
//todo - add insided here the validation of the input.
function insertNewTransactionAjax(quoteId)
{
	//file to execute
	url = "./transaction/insertNewTransactionAjax.php";
	//read all variables to update automatically
	
	alert(previousRemainder);
	
	var currentHiddenTransaction = document.getElementById('hiddenTransactionsUsed').innerHTML;
	
    var transDate = document.getElementById('transDate1').value;
    var details = document.getElementById('details1').value;
    var debit = document.getElementById('debit1').value;
    var credit = document.getElementById('credit1').value;
    if(debit=="")
  		debit = 0;
  	if(credit == "")
  		credit = 0;
  		
  	var remainder = parseInt(previousRemainder) + parseInt(debit) - parseInt(credit);
  	
  
  		
  	alert(remainder);
  	
  	//alert(remainder);
  	
    queryString = "?quoteId=" + quoteId + "&transDate=" + transDate + "&details=" + details + "&debit=" + debit + "&credit=" + credit + "&remainder=" + remainder;

    //alert(url+queryString);
  
    myFunction(url+queryString);
    
    //delay(100);
    
    //setTimeout(alert(document.getElementById("secondParameter").innerHTML, 1000);
    
    //show first hidden transaction
    //we support to show only 3 hidden transactions
    if(currentHiddenTransaction < 3 )
    {
    	document.getElementById("backupTransactionDate"+currentHiddenTransaction).innerHTML = transDate;
    	document.getElementById("backupTransactionDetails"+currentHiddenTransaction).innerHTML = details;
    	document.getElementById("backupTransactionDebit"+currentHiddenTransaction).innerHTML = debit;
    	document.getElementById("backupTransactionCredit"+currentHiddenTransaction).innerHTML = credit;
    	document.getElementById("backupTransactionRemainder"+currentHiddenTransaction).innerHTML = remainder;
		document.getElementById("backupTransaction"+currentHiddenTransaction).style.visibility = "visible";
		
		//after every insert, the hidden transaction fields are increased by one
		document.getElementById('hiddenTransactionsUsed').innerHTML++;
	}
	
	//alert(document.getElementById('secondParameter').innerHTML);
    
	
    return true;
} 

function deleteClaimAjax(claimId)
{
	//alert("claimId=" + claimId);
	//file to execute
	url = "./claims/deleteClaimAjax.php";
	
	queryString = "?claimId=" + claimId;
	
	myFunction(url+queryString);
	
	//hide row
	document.getElementById("claim" + claimId).style.display = 'none';
	
    return true;
}


function deleteMedicalPersonAjax(personId)
{
	//alert("personId=" + personId);
	//file to execute
	url = "./sale/deleteMedicalPersonAjax.php";
	
	queryString = "?personId=" + personId;
	
	myFunction(url+queryString);
	
	//hide row
	var row = document.getElementById(personId);
	row.parentElement.removeChild(row); 
	
	//document.getElementById(personId).style.display = 'none';
	
    return true;
}

function updateCoverageAjax(coverageId)
{
	var checked = document.getElementById(coverageId).checked;
	var contractNumber = document.getElementById('contractNumber').value;
	
	var param1 = "";
	var param2 = "";
	var param3 = "";
	if(document.getElementById(coverageId + 'Param1'))
		param1 = document.getElementById(coverageId + 'Param1').value;
	if(document.getElementById(coverageId + 'Param2'))
		param2 = document.getElementById(coverageId + 'Param2').value;
	if(document.getElementById(coverageId + 'Param3'))
		param3 = document.getElementById(coverageId + 'Param3').value;
	//alert(param1);
	//alert("coverageId=" + coverageId);
	//alert("contractNumber=" + contractNumber);
	//alert("checked=" + checked);
	
	//file to execute
	url = "./coverages/updateCoverageAjax.php";
	
	queryString = "?coverageId=" + coverageId + "&checked=" + checked + "&contractNumber=" + contractNumber + "&param1=" + param1 + "&param2=" + param2 + "&param3=" + param3  ;
	
	myFunction(url+queryString);
	
    return true;
}


/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
function setDefaultValues()
{
	setPreviousValue("<?php echo $contract->insuranceCompanyOfferingQuote; ?>", "insuranceCompany");
	setPreviousValue("<?php echo $contract->sale->associate; ?>", "associate");
	setPreviousValue("<?php echo $contract->sale->insuranceType; ?>", "insuranceType");
	setPreviousValue("<?php echo $contract->owner->countryOfBirth; ?>", "countryOfBirth");
	setPreviousValue("<?php echo $contract->motor->license->licenseType; ?>", "licenseType");
	setPreviousValue("<?php echo $contract->motor->license->licenseCountry; ?>", "licenseCountry");
	setPreviousValue("<?php echo $contract->motor->vehicle->vehicleType; ?>", "vehicleType");
	setPreviousValue("<?php echo $contract->motor->vehicle->vehicleDesign; ?>", "vehicleDesign");
	setPreviousValue("<?php echo $contract->owner->proposerType; ?>", "proposerType");
	setPreviousValue("<?php echo $contract->sale->producer; ?>", "producer");
	processInsuranceType();
	setPreviousValue("<?php echo $contract->medical->frequencyOfPayment; ?>", "frequencyOfPayment");
	setPreviousValue("<?php echo $contract->lifeIns->frequencyOfPayment; ?>", "lifeInsFrequencyOfPayment");
	setPreviousValue("<?php echo $contract->medical->roomType; ?>", "roomType");
	setPreviousValue("<?php echo $contract->propertyFire->typeOfPremises; ?>", "propertyType");
	setPreviousValue("<?php echo $contract->propertyFire->yearBuilt; ?>", "yearBuilt");
	setPreviousValue("<?php echo $contract->sale->coverageType; ?>", "coverageType");
	setPreviousValue("<?php echo $contract->sale->status; ?>", "saleStatus");
}


</script>


<!-- main content - left side -->
<div id="main">

	<table>
		<!-- this form cannot be inside the main form -->
		<tr>
			<td class="col10Per"></td>
			<!-- CLIENT NAME -->
			<td colspan="4"><h1 id="contract1">
			<?php 
			$updateStateId = "update_".$contract->owner->stateId; ?> 
			<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
			<form action="./office.php" id="<?php echo $updateStateId;?>" method="POST" style="display: none;">
				<input type="text" name="action" value="updateClientDataForm" />
				<input type="text" name="stateId" value="<?php echo $contract->owner->stateId;?>" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $updateStateId;?>').submit()"><?php echo $contract->owner->firstName." ".$contract->owner->lastName; ?></a>
			
			</h1></td>
			
			<!-- REGISTRATION NUMBER -->
			<td colspan="3"><h1 id="contract2">
					<?php if(is_object($contract->motor->vehicle)) echo $contract->motor->vehicle->regNumber;?>
					 </h1></td>
		</tr>
	</table>		
	<form name="updateContractData" action="./office.php" method="POST" onSubmit="return checkUpdateContractDataForm('medicalInsuredPersonsTable');" >
			<input type="hidden" name="action" value="updateContractDataProcess" />
			<input type="hidden" name="oldSaleStartDate" value="<?php echo $contract->sale->saleStartDate; ?>" />
			<input type="hidden" name="oldSaleEndDate" value="<?php echo $contract->sale->saleEndDate; ?>" />
			
			<table>
				
			<tr>
				<td><h3><?php echo $_SESSION['start']; ?>:</h3></td>
				<td class="col10Per" colspan="2">
                                <input type="text" style="width: 80px;" id="saleStartDate" name="saleStartDate" />
				
				</td>
				<td class="col10Per" colspan="2">
				<select name="saleStatus" id="saleStatus">
					<?php
					foreach($_SESSION['statusOptions'] as $eachOption){
						?>
						 <option  value=<?php echo "$eachOption->value" ?> ><?php echo "$eachOption->name" ?> </option>
						 <?php
					 }
					 ?>		
					 </select>
				</td>
				<td><h3><?php echo $_SESSION['end']; ?>:</h3></td>
				<td class="col10Per" colspan="2">
                                <input type="text" style="width: 80px;" id="saleEndDate" name="saleEndDate" />
				
				</td>
			</tr>
			<tr>
				<td class="col10Per"><?php echo $_SESSION['contractNumberTab']; ?>:</td>
				<td class="col10Per">
					<input type="text" name="contractNumber" id="contractNumber" size="10" value="<?php echo $contract->sale->saleId;?>" />
				</td>
				<td class="col10Per"><?php echo $_SESSION['quotationId']; ?>:</td>
				<td class="col10Per">
					<input type="text" name="quoteId" id="quoteId" size="10" value="<?php echo $contract->quoteId;?>" readonly="readonly" />
				</td>
				<td class="col10Per"><?php echo $_SESSION['company']; ?>:</td>
				<td class="col10Per"><select name="insuranceCompany" id="insuranceCompany" style="width:6.5em;">
					<?php
					foreach($_SESSION['insuranceCompanyTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name"?> </option>
						 <?php
					 }
					 ?>
					 </select>	
				</td>
				<td class="col10Per" ><?php echo $_SESSION['previousCompany']; ?>:</td>
				<td class="col10Per">
					<input type="text" name="previousCompany" id="previousCompany" size="10" value="<?php echo $contract->motor->drivingExperience->insuranceCompany;?>" />
				</td>
			</tr>
			<tr>
				<td class="col10Per"><?php echo $_SESSION['insuranceTypeTab']; ?>:</td>
				<td class="col10Per"><select name="insuranceType" id="insuranceType" style="width:6.5em;" onChange="processInsuranceType()" >
					<?php
					$i = 1;
					foreach($_SESSION['insuranceTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
						 $i = $i + 1;
					 }
					 ?>		
					 </select>	
				</td>
				<td class="col10Per"><?php echo $_SESSION['coverageTypeTab']; ?>:</td>
				<td class="col10Per"><select name="coverageType" id="coverageType" style="width:6.5em;">
					<?php
					$i = 1;
					foreach($_SESSION['coverageTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
						 $i = $i + 1;
					 }
					 ?>		
					 </select>	
				</td>
				<td class="col10Per"><?php echo $_SESSION['associate']; ?>:</td>
				<td class="col10Per"><select name="associate" id="associate" style="width:6.5em;">
						<?php
						foreach($_SESSION['associateOptions'] as $option){
							?>
							 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name"?> </option>
							 <?php
						 }
						 ?>
						 </select>	
									</td>
				<td class="col10Per"><?php echo $_SESSION['producerTab'] ?>:</td>
				<td class="col10Per"><select name="producer" id="producer" style="width:6.5em;">
							<?php
							foreach($allProducers as $eachProducer){
								?>
								 <option  value=<?php echo "$eachProducer" ?> ><?php echo "$eachProducer" ?> </option>
								 <?php
							 }
							 ?>
						</select>
				</td>

			</tr>
			<tr>
				<td class="col10Per" colspan="8"><h4><?php echo $_SESSION['proposerDetails']; ?></h4></td>
			</tr>
			<tr>
				<td class="col10Per"><?php echo $_SESSION['proposerTypeTab']; ?></td>
				<td class="col10Per"><select name="proposerType" id="proposerType" style="width:6.5em;" onChange="processInsuranceType()">
					<?php
					$i = 1;
					foreach($_SESSION['proposerTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
						 $i = $i + 1;
					 }
					 ?>		
					 </select>	
				</td>
				<!-- STATE ID -->
				<td class="col10Per"><?php echo $_SESSION['stateIdTab']; ?>:</td>
				<td class="col10Per"><input type="text" name="stateId" id="stateId" size="10" value="<?php echo $contract->owner->stateId;?>" readonly="readonly" /> </td>
				<td class="col10Per"><?php echo $_SESSION['nameTab']; ?>:</td>
				<td class="col10Per"><input type="text" name="firstName" id="firstName" size="10" value="<?php echo $contract->owner->firstName;?>" /> </td>
				<td class="col10Per"><?php echo $_SESSION['surname']; ?>:</td>
				<td class="col10Per"><input type="text" name="lastName" id="lastName" size="10" value="<?php echo $contract->owner->lastName;?>" /> </td>
			</tr>			
			<tr>
				<td class="col10Per"><?php echo $_SESSION['profession']; ?>:</td>
				<td class="col10Per"><input type="text" name="profession" id="profession" size="10" value="<?php echo $contract->owner->profession;?>" /></td>
				<td class="col10Per"><?php echo $_SESSION['telephoneTab']; ?>:</td>
				<td class="col10Per"><input type="text" name="telephone" id="telephone" size="10" value="<?php echo $contract->owner->telephone;?>" /> </td>
				<td class="col10Per"><?php echo $_SESSION['cellphoneTab']; ?>:</td>
				<td class="col10Per"><input type="text" name="cellphone" id="cellphone" size="10" value="<?php echo $contract->owner->cellphone;?>" /> </td>
			</tr>
			<tr>
		<tbody id="proposer-person-info">
		
				<td class="col10Per"><?php echo $_SESSION['nationality']; ?>:</td>
				<td class="col10Per">
				<select name="countryOfBirth" id="countryOfBirth" style="width:6.5em;">
					<?php
					foreach($_SESSION['countriesOptions'] as $eachCountry)
					{?>
					<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
					<?php	
					}
					?>
				</select>
				</td>
				<td class="col10Per"><?php echo $_SESSION['birthDate']; ?>:</td>
				<td class="col10Per" colspan="2">
					<input type="text" style="width: 80px;" id="birthDate" name="birthDate" />
				
				</td>
			
		</tbody>
			
			
		<tbody id="motor-license-info">
			
				<td class="col10Per"><?php echo $_SESSION['licenseTypeTab']; ?>:</td>
				<td class="col10Per"><select name="licenseType" id="licenseType" style="width:6.5em;">
					<?php
					foreach($_SESSION['licenseTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
					</select>
				</td>
				<td class="col10Per"><?php echo $_SESSION['licenseDateTab']; ?>:</td>
				<td class="col10Per" colspan="2">
					<input type="text" style="width: 80px;" id="licenseDate" name="licenseDate" />
				
				</td>
				<td class="col10Per"></td>
				<td class="col10Per"><?php echo $_SESSION['country']; ?>:</td>
				<td class="col10Per"><select name="licenseCountry" id="licenseCountry" style="width:6.5em;">
					<?php
					foreach($_SESSION['countriesOptions'] as $eachCountry)
					{?>
					<OPTION value="<?php echo $eachCountry->value;?>"><?php echo $eachCountry->name;?></OPTION> 
					<?php	
					}
					?>
				</td>
			
		</tbody>
			</tr>
			
			<?php
				displayAllAddresses($contract->addresses);
			?>
			<!-- VEHICLE DETAILS -->
		<tbody id="motor-info" >
			<tr>
				<td class="col10Per" colspan="8"><h4><?php echo $_SESSION['vehicleDetails']; ?></h4></td>
			</tr>
			<tr>
				<td class="col10Per"><?php echo $_SESSION['regNumberTab']; ?>:</td>
				<td class="col10Per"><input type="text" name="regNumber" id="regNumber" size="10" value="<?php echo $contract->motor->vehicle->regNumber;?>" /> </td>

				<!-- VEHICLE TYPE -->
				<td class="col10Per"><?php echo $_SESSION['vehicleTypeTab']; ?>:</td>
				<td class="col10Per"><select name="vehicleType" id="vehicleType" style="width:6.5em;">
					<?php
					$i = 1;
					foreach($_SESSION['vehicleTypeTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
						 $i = $i + 1;
					 }
					 ?>
					 </select>	
				</td>
				<td class="col10Per"><?php echo $_SESSION['make']; ?>:</td>
				<td class="col10Per"><input type="text" name="make" id="make" size="10" value="<?php echo $contract->motor->vehicle->make;?>" /> </td>
				<td class="col10Per"><?php echo $_SESSION['vehicleModelTab']; ?>:</td>
				<td class="col10Per"><input type="text" name="vehicleModel" id="vehicleModel" size="10" value="<?php echo $contract->motor->vehicle->model;?>" /> </td>
			</tr>
			<tr>
				<td class="col10Per"><?php echo $_SESSION['engineCapacity']; ?>:</td>
				<td class="col10Per"><input type="text" name="cubicCapacity" id="cubicCapacity" size="10" value="<?php echo $contract->motor->vehicle->cubicCapacity;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('cubicCapacity')"/> </td>
				<td class="col10Per"><?php echo $_SESSION['manufacturedYear']; ?>:</td>
				<td class="col10Per"><select name="vehicleManufacturedYear" id="vehicleManufacturedYear" style="width:6.5em;">
					<?php
					$currentYear = date("Y");
					for($i=$currentYear; $i>1950; $i--){
						if($i==$contract->motor->vehicle->manufacturedYear)
						{
							?>
							<option  value=<?php echo $i ?> selected><?php echo "$i" ?> </option>
							<?php
						}
						else
						{
						?>
						 <option  value=<?php echo $i ?> ><?php echo "$i" ?> </option>
						 <?php
					 	}
					 }
					 ?>
					 </select>
				</td>

				<!-- VEHICLE DESIGN -->
				<td class="col10Per"><?php echo $_SESSION['vehicleDesignTab']; ?>:</td>
				<td class="col10Per"><div id="vehDesignLeft"><select name="vehicleDesign" id="vehicleDesign" style="width:6.5em;">
					<?php
					$i = 1;
					foreach($_SESSION['vehicleDesignTabOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
						 $i = $i + 1;
					 }
					 ?>						
					</select></div>
					<div id="vehDesignRight">
				<a href="./quotation.php?action=glossary&location=vehicleDesign" target="_blank"><IMG SRC="<?php echo $_SESSION['globalFilesLocation']?>/images/question_mark.gif" border=0></a></div>
				</td>
				<td class="col10Per"><?php echo $_SESSION['coverageAmountTab']; ?>:</td>
				<td class="col10Per"><input type="text" name="sumInsured" id="sumInsured" size="10" value="<?php echo $contract->motor->vehicle->sumInsured;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('sumInsured')" /> </td>
			</tr>
			
		</tbody><!--motor-info-->
						
		<!-- INFORMATION FOR ADDITIONAL DRIVERS -->
			
		<tbody id="additional-drivers">
			
			<?php
			if(count($contract->drivers)>0)
			{
				displayAdditionalDrivers($contract->drivers); ?>
			<?php
			}
			?>
			<tr>
				<td class="col10Per" colspan=2>
				<a href="./office.php?action=modifyAdditionalDrivers"><?php echo $_SESSION['addAdditionalDrivers']; ?></a>
				</td>
			</tr>
			
		</tbody><!-- ADDITIONAL DRIVERS -->
			

			
			<!-- EMPLOYERS LIABILITY -->
		<tbody id="employer-liability-info" style="display:none;">
			<tr>
				<td class="col10Per" colspan="8"><h4><?php echo $_SESSION['employerLiabilityDetails']; ?></h4></td>
			</tr>
	
			<!-- EMPLOYERS SOCIAL SECURITY NUMMBER -->
			<tr>
				<td class="col10Per"><?php echo $_SESSION['employersSocialInsuranceNumber']; ?>:</td>
				<td class="col10Per"><input type="text" name="employersSocialInsuranceNumber" id="employersSocialInsuranceNumber" size="10" value="<?php echo $contract->employersLiability->employersSocialInsuranceNumber;?>" /></td>
			
			
			<!-- LIMIT PER EMPLOYEE -->
			
				<td class="col10Per"><?php echo $_SESSION['limitPerEmployee']; ?>:</td>
				<td class="col10Per"><input type="text" name="limitPerEmployee" id="limitPerEmployee" size="10" value="<?php echo $contract->employersLiability->limitPerEmployee;?>" /></td>
			
			<!-- LIMIT PER EVENT OR SERIES OF EVENTS -->
				<td class="col10Per"><?php echo $_SESSION['limitPerEventOrSeriesOfEvents']; ?>:</td>
				<td class="col10Per"><input type="text" name="limitPerEventOrSeriesOfEvents" id="limitPerEventOrSeriesOfEvents" size="10" value="<?php echo $contract->employersLiability->limitPerEventOrSeriesOfEvents;?>" /></td>
			</tr>
			<!-- LIMIT DURING PERIOD OF INSURANCE -->
			<tr>
				<td class="col10Per"><?php echo $_SESSION['limitDuringPeriodOfInsurance']; ?>:</td>
				<td class="col10Per"><input type="text" name="limitDuringPeriodOfInsurance" id="limitDuringPeriodOfInsurance" size="10" value="<?php echo $contract->employersLiability->limitDuringPeriodOfInsurance;?>" /></td>
			
			
			<!-- EMPLOYEES NUMBER -->
			
				<td class="col10Per"><?php echo $_SESSION['employeesNumber']; ?>:</td>
				<td class="col10Per"><input type="text" name="employeesNumber" id="employeesNumber" size="10" value="<?php echo $contract->employersLiability->employeesNumber;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('employeesNumber')"/></td>

			
			<!-- ESTIMATED TOTAL GROSS EARNINGS -->
				<td class="col10Per"><?php echo $_SESSION['estimatedTotalGrossEarnings']; ?>:</td>
				<td class="col10Per"><input type="text" name="estimatedTotalGrossEarnings" id="estimatedTotalGrossEarnings" size="10" value="<?php echo $contract->employersLiability->estimatedTotalGrossEarnings;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('estimatedTotalGrossEarnings')"/></td>
			</tr>
			
		</tbody>
			
			
			
			
			<!-- PROPERTY FIRE -->
			<tbody id="property-fire-info" style="display:none;">
			
			<tr>
				<td class="col10Per" colspan="8"><h4><?php echo $_SESSION['propertyFireDetails']; ?></h4></td>
			</tr>
			
			<tr>
				<!-- PROPERTY TYPE -->
				<td class="col10Per"><?php echo $_SESSION['propertyType']; ?></td>
				<td class="col10Per"><select name="propertyType" id="propertyType" style="width:6.5em;">
					<?php
					foreach($_SESSION['propertyTypeOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
					</select>
				</td>
				<!-- DESCRIPTION -->
				<td class="col10Per"><?php echo $_SESSION['description']; ?>:</td>
				<td class="col10Per"><input type="text" name="description" id="description" size="10" value="<?php echo $contract->propertyFire->description; ?>" /></td>

				<!-- BUILDING VALUE -->
				<td class="col10Per"><?php echo $_SESSION['buildingValue']; ?></td>
				<td class="col10Per"><input type="text" name="buildingValue" id="buildingValue" size="10" value="<?php echo $contract->propertyFire->buildingValue; ?>" /></td>
				
				<!-- OUTSIDE FIXTURES VALUE -->
				<td class="col10Per"><?php echo $_SESSION['outsideFixturesValue']; ?>:</td>
				<td class="col10Per"><input type="text" name="outsideFixturesValue" id="outsideFixturesValue" size="10" value="<?php echo $contract->propertyFire->outsideFixturesValue; ?>" /></td>
			</tr>
			<tr>
				<!-- CONTENTS VALUE -->
				<td class="col10Per"><?php echo $_SESSION['contentsValue']; ?></td>
				<td class="col10Per"><input type="text" name="contentsValue" id="contentsValue" size="10" value="<?php echo $contract->propertyFire->contentsValue; ?>" /></td>
				
				<!-- VALUABLE OBJECTS VALUE -->
				<td class="col10Per"><?php echo $_SESSION['valuableObjectsValue']; ?>:</td>
				<td class="col10Per"><input type="text" name="valuableObjectsValue" id="valuableObjectsValue" size="10" value="<?php echo $contract->propertyFire->valuableObjectsValue; ?>" /></td>
				<!-- YEAR BUILT -->
				<td class="col10Per"><?php echo $_SESSION['yearBuilt']; ?></td>
				<td class="col10Per"><select name="yearBuilt" id="yearBuilt" style="width:6.5em;">
					<?php
					$currentYear = date("Y");
					for($i=$currentYear; $i>1900; $i--){
						?>
						 <option  value=<?php echo $i ?> ><?php echo "$i" ?> </option>
						 <?php
					 }
					 ?>
					 </select>
				</td>
				
				<!-- AREA SQUARE METERS -->
				<td class="col10Per"><?php echo $_SESSION['area']; ?>:</td>
				<td class="col10Per"><input type="text" name="areaSqMt" id="areaSqMt" size="10" value="<?php echo $contract->propertyFire->areaSqMt; ?>" /></td>
			</tr>
			
		</tbody>
			
		<!-- MEDICAL -->
		<tbody id="medical-info" style="display:none;">
			<tr>
				<td class="col10Per" colspan="8"><h4><?php echo $_SESSION['medicalDetails']; ?></h4></td>
			</tr>
			
			<!-- PLAN NAME -->
			<tr>
				<td class="col10Per"><?php echo $_SESSION['planName']; ?>:</td>
				<td class="col10Per"><input type="text" name="planName" id="planName" size="10" value="<?php echo $contract->medical->planName;?>" /></td>
			
			<!-- FREQUENCY OF PAYMENT -->
				<td class="col10Per"><?php echo $_SESSION['frequencyOfPayment']; ?>:</td>
				<td class="col10Per"><select name="frequencyOfPayment" id="frequencyOfPayment" style="width:6.5em;">
					<?php
					foreach($_SESSION['frequencyOfPaymentOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
					</select>
				</td>
			
			<!-- PREMIUM -->
				<td class="col10Per"><?php echo $_SESSION['premium']; ?>(€):</td>
				<td class="col10Per"><input type="text" name="premium" id="premium" size="10" value="<?php echo $contract->medical->premium;?>" /></td>
			
			<!-- MAXIMUM LIMIT -->
				<td class="col10Per"><?php echo $_SESSION['maximumLimit']; ?>:</td>
				<td class="col10Per"><input type="text" name="planMaximumLimit" id="planMaximumLimit" size="10" value="<?php echo $contract->medical->planMaximumLimit;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('planMaximumLimit')"/></td>
			</tr>
			
			<tr>
			<!-- DEDUCTIBLE -->
				<td class="col10Per"><?php echo $_SESSION['deductible']; ?>:</td>
				<td class="col10Per"><input type="text" name="deductible" id="deductible" size="10" value="<?php echo $contract->medical->deductible;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('deductible')"/></td>
				
			<!-- CO-INSURANCE PERCENTAGE -->
				<td class="col10Per"><?php echo $_SESSION['coInsurancePercentage']; ?>(%):</td>
				<td class="col10Per"><input type="text" name="coInsurancePercentage" id="coInsurancePercentage" size="10" value="<?php echo $contract->medical->coInsurancePercentage;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('coInsurancePercentage')"/></td>
			
		
			
			<tr>
					
			<!-- ROOM TYPE -->
				<td class="col10Per"><?php echo $_SESSION['roomType']; ?></td>
				<td class="col10Per"><select name="roomType" id="roomType" style="width:6.5em;">
						<?php
						foreach($_SESSION['roomTypeOptions'] as $option){
							?>
							 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
							 <?php
						 }
						 ?>
						</select>
				</td>
			</tr>
			
			<tr>
				<td colspan=8>
				<?php
				displayMedicalInsuredPersons($contract->medical->medicalInsuredPersonsArray);
				?>
				</td>
			</tr>
		</tbody>
		
		<!-- LIFE -->
		<tbody id="lifeins-info" style="display:none;">
			<tr>
				<td class="col10Per" colspan="8"><h4><?php echo $_SESSION['lifeInsDetails']; ?></h4></td>
			</tr>
			
			<!-- PLAN NAME -->
			<tr>
				<td class="col10Per"><?php echo $_SESSION['planName']; ?>:</td>
				<td class="col10Per"><input type="text" name="lifeInsPlanName" id="lifeInsPlanName" size="10" value="<?php echo $contract->lifeIns->planName;?>" /></td>
			
			<!-- FREQUENCY OF PAYMENT -->
				<td class="col10Per"><?php echo $_SESSION['frequencyOfPayment']; ?>:</td>
				<td class="col10Per"><select name="lifeInsFrequencyOfPayment" id="lifeInsFrequencyOfPayment" style="width:6.5em;">
					<?php
					foreach($_SESSION['frequencyOfPaymentOptions'] as $option){
						?>
						 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
						 <?php
					 }
					 ?>
					</select>
				</td>
			
			<!-- INSURED PERSON NAME -->
				<td class="col10Per"><?php echo $_SESSION['insuredPersonName']; ?>:</td>
				<td class="col10Per"><input type="text" name="insuredFirstName" id="insuredFirstName" size="10" value="<?php echo $contract->lifeIns->insuredFirstName;?>" /></td>
			
				<td class="col10Per"><?php echo $_SESSION['insuredPersonSurname']; ?>:</td>
				<td class="col10Per"><input type="text" name="insuredLastName" id="insuredLastName" size="10" value="<?php echo $contract->lifeIns->insuredLastName;?>" /></td>
			</tr>
			
			<!-- BASIC PLAN AMOUNT -->
			<tr>
				<td class="col10Per"><?php echo $_SESSION['basicPlan']."(".$EURO.")"; ?>:</td>
				<td class="col10Per"><input type="text" name="basicPlanAmount" id="basicPlanAmount" size="10" value="<?php echo $contract->lifeIns->basicPlanAmount;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('basicPlanAmount')"/></td>
			
			<!-- TOTAL PERMANENT DISABILITY OF PAYMENT -->
				<td class="col10Per"><?php echo $_SESSION['totalPermanentDisability']."(".$EURO.")"; ?>:</td>
				<td class="col10Per"><input type="text" name="totalPermanentDisabilityAmount" id="totalPermanentDisabilityAmount" size="10" value="<?php echo $contract->lifeIns->totalPermanentDisabilityAmount;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('totalPermanentDisabilityAmount')"/></td>
			
			<!-- PREMIUM PROTECTION -->
				<td class="col10Per"><?php echo $_SESSION['premiumProtection']."(".$EURO.")"; ?>:</td>
				<td class="col10Per"><input type="text" name="premiumProtectionAmount" id="premiumProtectionAmount" size="10" value="<?php echo $contract->lifeIns->premiumProtectionAmount;?>" onkeydown="return validateOnRunTimeIsNumeric(event)" onblur="return checkIfNumberMoreThanOneDots2('premiumProtectionAmount')"/></td>
			</tr>
			
		</tbody>
		
		<tr>
			<td class="col10Per"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['updateButton'];?>" size="50" /></td>
			
		</tr>
		</table>
	</form>			
</div><!-- main -->
 
<div id="main">
 
 	<!-- TRANSACTIONS -->
 	<div class="sidebar-box">
 	<table>
 		<tr><td><h3><?php echo $_SESSION['payments']; ?></h3></td></tr>
 		<tr>
			<td class="col15Per"><b><?php echo $_SESSION['date']; ?></b></td>
			<td class="col15Per"><b><?php echo $_SESSION['details']; ?></b></td>
			<td class="col15Per"><b><?php echo $_SESSION['receipt']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['debit']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['credit']; ?></b></td>
			<td class="col10Per"><b><?php echo $_SESSION['remainder']; ?></b></td>
		</tr>
 		<?php
 		$transactions = array();
 		
 		$parameterNameValueArray = array();
		$parameterNameValue = new parameterNameValue();
		$parameterNameValue->name = "saleId";
		$parameterNameValue->value = $contract->sale->saleId;
		$parameterNameValueArray[] = $parameterNameValue;
			
 		$transactions = array_reverse(retrieveTransactions($parameterNameValueArray, " limit 5"));//reverse array to become ascending by date
 		//$transactions = array_reverse(retrieveTransactions("saleId", $contract->sale->saleId, " limit 5"));//reverse array to become ascending by date
 		
 		
 		//this code is NOT USED now. But keep for KNOWLEDGE BASE - START
 		//insert 3 hidden row. They keep up to 3 newly added transactions.
 		$hiddenTransactions = 3;
 		for($j=0; $j<$hiddenTransactions; $j++)
 		{
 		?>
 		<tbody  style="visibility:hidden" id="<?php echo 'backupTransaction'.$j;?>">
 			<tr>
	 			<td class="col18Per" id="<?php echo 'backupTransactionDate'.$j;?>"></td>
		 		<td class="col18Per" id="<?php echo 'backupTransactionDetails'.$j;?>"></td>
		 		<td class="col18Per" id="<?php echo 'backupTransactionDebit'.$j;?>"></td>
		 		<td class="col18Per" id="<?php echo 'backupTransactionCredit'.$j;?>"></td>
		 		<td class="col18Per" id="<?php echo 'backupTransactionRemainder'.$j;?>"></td>
		 		<!-- dont allow to delete newly inserted transactions for now
			 	<td><a href="javascript:;" onclick="javascript:deleteTransactionAjax('<?php echo 'backupTransaction'.$j;?>');"><img src="./images/delete-sign.jpg"/> </a></td>
			 	-->
		 	</tr>
 		</tbody>
 		<?php
 		//this code is NOT USED now. But keep for KNOWLEDGE BASE - END
 		
		}
			
		//display existing transactions
		if(count($transactions)==0)
			$previousRemainder = 0;
			
		$j = 0;	
 		foreach($transactions as $transaction)
 		{
	 		//store previous remainder, which comes first from the query
	 		if( $j == 0 )
	 			$previousRemainder = $transaction->remainder;
	 		?>
	 		<tr id="<?php echo $transaction->transId;?>">
			<td class="col15Per"><?php echo $transaction->transDate; ?></td>
			<td class="col15Per"><?php echo $transaction->details; ?></td>
			<td class="col15Per"><?php echo $transaction->receiptNo; ?></td>
			<td class="col10Per"><?php echo $transaction->debit; ?></td>
			<td class="col10Per"><?php echo $transaction->credit; ?></td>
			<td class="col10Per"><?php echo $transaction->remainder; ?></td>
			<td class="col1Per"><a href="javascript:;" onclick="javascript:deleteTransactionAjax('<?php echo $transaction->transId;?>');"><img src="./images/delete-sign.jpg"/> </a></td>
			</tr>
	 		<?php
	 		$j = $j + 1;
 		}
 		$emptyCharacters = "_ _ _";
 		?>
 		<tr>
			<td class="col15Per"><?php echo $emptyCharacters; ?></td>
			<td class="col15Per"><?php echo $emptyCharacters; ?></td>
			<td class="col15Per"><?php echo $emptyCharacters; ?></td>
			<td class="col10Per"><?php echo $emptyCharacters; ?></td>
			<td class="col10Per"><?php echo $emptyCharacters; ?></td>
			<td class="col10Per"><?php echo $emptyCharacters; ?></td>
		</tr>
 		
 		<?php
 		//MADE THIS A FORM, NOT AJAX - FINISH FINISH FORM PROCESSING
 		?>
 		<form name="insertTransactionForm" action="./office.php" method="POST" onSubmit="return checkInsertNewTransactionValidity();">
 			<input type="hidden" name="action" value="insertNewTransactionFormProcess">
 			<input type="hidden" name="saleId" value="<?php echo $contract->sale->saleId;?>">
 			<input type="hidden" name="producer" value="<?php echo $contract->sale->producer;?>">
 			
	 		<tr>
				<td class="col15Per"><input class="datepick" type="text" size="8" name="transDate1" id="transDate1" value="<?php echo date('d-m-Y');?>" /></td>
				<td class="col15Per"><select name="details1" id="details1" style="width:5em;">
				<?php
				$j = 1;
				foreach($_SESSION['transactionDetailsOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
					 $j = $j + 1;
				 }
				 ?>
				 </select>	</td>
				<td class="col15Per"><input type="text" name="receiptNo1" size="3" id="receiptNo1" value="" /></td>
				<td class="col10Per"><input type="text" name="debit1" size="3" id="debit1" value="" /></td>
				<td class="col10Per"><input type="text" name="credit1" size="3" id="credit1" value="" /></td>
				<td class="col1Per"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['new'];?>" size="30" /></td>
				<td class="col10Per"><a href="./office.php?action=viewAllContractTransactions"><?php echo $_SESSION['all']; ?></a></td>
		
	 		</tr>
 		</form>
 	</table>
 	
 	<!-- store number of transactions used, to be accessed by the javasctipt -->
 	<div id="hiddenTransactionsUsed" style="display:none">0</div>
	</div>
 	
 	<!-- NOTES -->
 	<div class="sidebar-box">
 	<table>
		
 		<tr><td><h3><?php echo $_SESSION['notesTab']; ?></h3></td></tr>
 		<tr>
 			<td class="col10Per"><b><?php echo $_SESSION['date']; ?></b></td>
 			<td class="col20Per"><b><?php echo $_SESSION['typeTab']; ?></b></td>
			<td class="col60Per" colspan="2"><b><?php echo $_SESSION['description']; ?></b></td>
			<td class="col1Per"></td>
		</tr>
 		<?php
 		$notes = array();
 		$parameterNameValueArray = array();
 		
 		$parameterNameValue = new parameterNameValue();
		$parameterNameValue->name = "saleId";
		$parameterNameValue->value = $contract->sale->saleId;
		$parameterNameValueArray[] = $parameterNameValue;
		
 		$notes = retrieveNotes($parameterNameValueArray);
 		
 			
		$j = 0;	
 		foreach($notes as $note)
 		{
	 		?>
	 		<tr id="<?php echo "note".$note->notesId;?>">
			<td class="col10Per"><?php echo $note->entryDate; ?></td>
			<td class="col20Per"><?php echo $note->type; ?></td>
			<td class="col60Per" colspan="2"><?php echo $note->description; ?></td>
	 		<td class="col1Per"><a href="javascript:;" onclick="javascript:deleteNoteAjax('<?php echo $note->notesId;?>');"><img src="./images/delete-sign.jpg"/> </a></td>
			</tr>
	 		<?php
	 		$j = $j + 1;
 		}
 		//MADE THIS A FORM, NOT AJAX - FINISH FINISH FORM PROCESSING
 		?>
 		<form name="insertNoteForm" action="./office.php" method="POST" onSubmit="return checkInsertNewNoteValidity()">
 			<input type="hidden" name="action" value="insertNewNoteFormProcess">
 			<input type="hidden" name="saleId" value="<?php echo $contract->sale->saleId;?>">
 			<input type="hidden" name="stateId" value="<?php echo $contract->owner->stateId;?>">
	 		<tr>
	 			<td class="col10Per"><input type="text" size="8" class="datepick" name="notesDate1" id="notesDate1"  /></td>
	 			<td class="col20Per">
				<select name="noteType1" id="noteType1" style="width:8em;">
				<?php
				foreach($_SESSION['noteTypeOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name"?> </option>
					 <?php
				 }
				 ?>
				 </select>
			</td>
	 			<td class="col60Per" colspan="2"><input type="text" size="40" name="description1" id="description1"  value="" /></td>
	 			<td class="col1Per"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['new'];?>" size="5" /></td>
	 		</tr>
 		</form>
 	</table>
 	
 	</div>
 	
 	<!-- CLAIMS -->
 	<div class="sidebar-box">
 	<table>
		<tr><td colspan="6"><h3><?php echo $_SESSION['claim']; ?></h3></td></tr>
 		<tr><td class="col18Per"><b><?php echo $_SESSION['date']; ?></b></td>
		<td class="col18Per" colspan="4"><b><?php echo $_SESSION['amount']; ?></b></td>
		</tr>
 		<?php
 		$claims = array();
 		$claims = retrieveClaims("stateId", $contract->owner->stateId);
 		
 			
		$j = 0;	
 		foreach($claims as $claim)
 		{
	 		?>
	 		<tr id="<?php echo "claim".$claim->claimId;?>">
			<td class="col18Per"><?php echo $claim->claimDate; ?></td>
			<td class="col18Per" colspan="4"><?php echo $claim->amount; ?></td>
	 		<td class="col10Per"><a href="javascript:;" onclick="javascript:deleteClaimAjax('<?php echo $claim->claimId;?>');"><img src="./images/delete-sign.jpg"/> </a></td></tr>
	 		<?php
	 		$j = $j + 1;
 		}
 		//MADE THIS A FORM, NOT AJAX - FINISH FINISH FORM PROCESSING
 		?>
 		<form name="insertNewClaimForm" action="./office.php" method="POST" onSubmit="return checkInsertNewClaimValidity()">
 			<input type="hidden" name="action" value="insertNewClaimFormProcess">
 			<input type="hidden" name="stateId" value="<?php echo $contract->owner->stateId;?>">
 			<input type="hidden" name="quoteId" value="<?php echo $contract->quoteId;?>">
 			
	 		<tr><td class="col18Per"><input type="text" size="8" class="datepick" name="claimDate1" id="claimDate1" /></td>
	 		<td class="col18Per" colspan="4"><input type="text" name="amount1" id="amount1"  value="" size="8"/></td>
	 		<td class="col10Per"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['new'];?>" size="8" /></td>
	 		</tr>
 		</form>
 	</table>
 	
 	</div>
 
 	<!-- COVERAGES -->
 	<div class="sidebar-box" id="coverages">
 	<table>
		<tr><td><h3><?php echo  $_SESSION['coveragesInPolicyQuotation']; ?>
			<div id="message" style="display:inline;color:#00FF00">&nbsp;</div>
			<div id="secondParameter" style="display:inline;color:#00FF00">&nbsp;</div></h3></td>
		</tr>
 		<?php
 		
 			displayCoverages($contract);
 			
 			
		 	?>		 	
 	</table>

 	</div>
 	
 	<div class="sidebar-box">
 		<table>
			<tr>
			<td class="col10Per">
			<!-- display all client contracts - 
			create hidden forn, to send the action as a POST -->
			<form action="./office.php" id="retrieveAllClientContracts" method="post" style="display: none;">
				<input type="text" name="action" value="retrieveAllClientContracts" />
				<input type="text" name="stateId" value="<?php echo $contract->owner->stateId;?>" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('retrieveAllClientContracts').submit()"><?php echo $_SESSION['contractsTab']; ?></a>
			</td>
			<td class="col10Per" colspan=2>
				<form action="./office.php" id="uploadFilesManagement" method="post" style="display: none;">
				<input type="text" name="action" value="uploadFilesManagement" />
				</form>
				<a href="javascript:;" onclick="javascript: document.getElementById('uploadFilesManagement').submit()"><?php echo $_SESSION['filesManagement']; ?></a>
			</td>
			<td class="col10Per"><input type="button" value="<?php echo $_SESSION['print']; ?>" onclick="printpage()" /></td>
		</tr>
	</table>	 	
 	
 	</div>
         
 