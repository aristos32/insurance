<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
	
//echo "actionRead=$actionRead";
//process only real requests - not language change
if($actionRead!='')
{
	
	$contract = new contract();
	$contract->insuranceCompanyOfferingQuote = $_POST['insuranceCompany'];
	$contract->quoteId = $_POST['quoteId'];
	
	$contract->sale = new sale();
	$contract->sale->saleId = $_POST['contractNumber'];
	$contract->sale->oldSaleId = $_SESSION['saleId'];
	$contract->sale->saleStartDate = date("Y-m-d", strtotime($_POST['saleStartDate']));
	$contract->sale->saleEndDate = date("Y-m-d", strtotime($_POST['saleEndDate']));
	$contract->sale->oldSaleStartDate = date("Y-m-d", strtotime($_POST['oldSaleStartDate']));
	$contract->sale->oldSaleEndDate = date("Y-m-d", strtotime($_POST['oldSaleEndDate']));
	$contract->sale->insuranceType = $_POST['insuranceType'];
	$contract->sale->previousInsuranceType = $_SESSION['currentInsuranceType'];
	$contract->sale->coverageType = $_POST['coverageType'];
	$contract->sale->associate = $_POST['associate'];
	$contract->sale->producer = $_POST['producer'];
	$contract->sale->stateId = $_POST['stateId'];
	$contract->sale->company = $_POST['insuranceCompany'];
	$contract->sale->status = $_POST['saleStatus'];
	
	$contract->owner = new owner();	
	$contract->owner->firstName = $_POST['firstName'];
	$contract->owner->lastName = $_POST['lastName'];
	$contract->owner->stateId = $_POST['stateId'];
	$contract->owner->type = 'account';//only accounts have contracts
	$contract->owner->oldStateId = $contract->owner->stateId;//we cannot update stateid here. so use the same for new and old.
	$contract->owner->telephone = $_POST['telephone'];
	$contract->owner->cellphone = $_POST['cellphone'];
	$contract->owner->profession = $_POST['profession'];
	$contract->owner->countryOfBirth = $_POST['countryOfBirth'];
	$contract->owner->birthDate = date("Y-m-d", strtotime($_POST['birthDate']));//$_POST['birthDate'];
	$contract->owner->proposerType = $_POST['proposerType'];
	
	
	$contract->motor = new motor();
	
	$contract->motor->license = new license();
	$contract->motor->license->stateId = $contract->owner->stateId;
	$contract->motor->license->oldStateId = $contract->owner->oldStateId;
	$contract->motor->license->licenseType = $_POST['licenseType'];
	$contract->motor->license->licenseDate = date("Y-m-d", strtotime($_POST['licenseDate']));//$_POST['licenseDate'];
	$contract->motor->license->licenseCountry = $_POST['licenseCountry'];
		
	$contract->motor->vehicle = new vehicle();
	$contract->motor->vehicle->saleId = $contract->sale->saleId;
	$contract->motor->vehicle->oldSaleId = $contract->sale->oldSaleId;
	$contract->motor->vehicle->regNumber = $_POST['regNumber'];
	$contract->motor->vehicle->make = $_POST['make'];
	$contract->motor->vehicle->vehicleType = $_POST['vehicleType'];
	$contract->motor->vehicle->model = $_POST['vehicleModel'];
	$contract->motor->vehicle->cubicCapacity = $_POST['cubicCapacity'];
	$contract->motor->vehicle->manufacturedYear = $_POST['vehicleManufacturedYear'];
	$contract->motor->vehicle->sumInsured = @$_POST['sumInsured'];
	$contract->motor->vehicle->vehicleDesign = $_POST['vehicleDesign'];
	
	$contract->motor->drivingExperience = new drivingExperience();
	$contract->motor->drivingExperience->stateId = $contract->owner->stateId;
	$contract->motor->drivingExperience->oldStateId = $contract->owner->oldStateId;
	$contract->motor->drivingExperience->insuranceCompany = $_POST['previousCompany'];
	if($contract->motor->drivingExperience->insuranceCompany=='')
		$contract->motor->drivingExperience->hasPreviousInsurance = 'NO';
	else
		$contract->motor->drivingExperience->hasPreviousInsurance = 'YES';
		
	//$contract->motor->drivingExperience->printData();
	
	//echo "ROLE is $user->role <br>";
	//$contract->printData();
	
	$contract->employersLiability = new employersLiability();
	$contract->employersLiability->saleId = $contract->sale->saleId;
	$contract->employersLiability->oldSaleId = $contract->sale->oldSaleId;
	$contract->employersLiability->employersSocialInsuranceNumber = $_POST['employersSocialInsuranceNumber'];
	$contract->employersLiability->limitPerEmployee = $_POST['limitPerEmployee'];
	$contract->employersLiability->limitPerEventOrSeriesOfEvents = $_POST['limitPerEventOrSeriesOfEvents'];
	$contract->employersLiability->limitDuringPeriodOfInsurance = $_POST['limitDuringPeriodOfInsurance'];
	$contract->employersLiability->employeesNumber = $_POST['employeesNumber'];
	$contract->employersLiability->estimatedTotalGrossEarnings = $_POST['estimatedTotalGrossEarnings'];
	
	$contract->medical = new medical();
	$contract->medical->saleId = $contract->sale->saleId;
	$contract->medical->oldSaleId = $contract->sale->oldSaleId;
	$contract->medical->planName = $_POST['planName'];
	$contract->medical->premium = $_POST['premium'];
	$contract->medical->planMaximumLimit = $_POST['planMaximumLimit'];
	$contract->medical->deductible = $_POST['deductible'];
	$contract->medical->frequencyOfPayment = $_POST['frequencyOfPayment'];
	//$contract->medical->excess = $_POST['excess'];
	$contract->medical->coInsurancePercentage = $_POST['coInsurancePercentage'];
	$contract->medical->roomType = $_POST['roomType'];
	
	$contract->lifeIns = new lifeIns();
	$contract->lifeIns->saleId = $contract->sale->saleId;//$contractInfo->contractNumber;
	$contract->lifeIns->insuredFirstName = @$_POST['insuredFirstName'];
	$contract->lifeIns->insuredLastName = @$_POST['insuredLastName'];
	$contract->lifeIns->planName = @$_POST['lifeInsPlanName'];	
	$contract->lifeIns->frequencyOfPayment = @$_POST['lifeInsFrequencyOfPayment'];
	$contract->lifeIns->endorsement = @$_POST['lifeInsEndorsement'];
	$contract->lifeIns->basicPlanAmount = @$_POST['basicPlanAmount'];
	$contract->lifeIns->totalPermanentDisabilityAmount = @$_POST['totalPermanentDisabilityAmount'];
	$contract->lifeIns->premiumProtectionAmount = @$_POST['premiumProtectionAmount'];
	
	//read fields that come as arrays
	$personIds = @$_POST["personIdMedical"];
	$firstNames = @$_POST["firstNameMedical"];
	$firstNames = @$_POST["firstNameMedical"];
	$lastNames = @$_POST['lastNameMedical'];
	$stateIds = @$_POST['stateIdMedical'];
	$genders = @$_POST['genderMedical'];
	$telephones = @$_POST['telephoneMedical'];
	$birthDates = @$_POST['birthDateMedical'];
	
	if(isset($firstNames) && is_array($firstNames) && count($firstNames)>0 )
	{
		
		//set all array elements
		foreach($firstNames as $a => $b)
		{
			//only if id or name is set, add driver
			if($stateIds[$a]!=''||$firstNames[$a]!='')
			{
				//echo $firstNames[$a];
		  		$eachMedicalInsuredPerson = new medicalInsuredPerson();
		  		$eachMedicalInsuredPerson->saleId = $contract->sale->saleId;
				$eachMedicalInsuredPerson->oldSaleId = $contract->sale->oldSaleId;
				$eachMedicalInsuredPerson->personId = $personIds[$a];
				$eachMedicalInsuredPerson->firstName = $firstNames[$a];
				$eachMedicalInsuredPerson->lastName = $lastNames[$a];
				$eachMedicalInsuredPerson->stateId = $stateIds[$a];
				$eachMedicalInsuredPerson->gender = $genders[$a];
				$eachMedicalInsuredPerson->telephone = $telephones[$a];
				$eachMedicalInsuredPerson->birthDate = $birthDates[$a];
				
				//$eachMedicalInsuredPerson->printData();
				
				$contract->medical->medicalInsuredPersonsArray[] = $eachMedicalInsuredPerson;
				
			}
	  		
		}
	}
	
	
	
	//$marika = $_POST['aristos33'];
	
	$contract->propertyFire = new propertyFire();
	$contract->propertyFire->saleId = $contract->sale->saleId;
	$contract->propertyFire->oldSaleId = $contract->sale->oldSaleId;
	$contract->propertyFire->description = @$_POST['description'];
	$contract->propertyFire->typeOfPremises = @$_POST['propertyType'];
	$contract->propertyFire->buildingValue = @$_POST['buildingValue'];	
	$contract->propertyFire->outsideFixturesValue = @$_POST['outsideFixturesValue'];
	$contract->propertyFire->contentsValue = @$_POST['contentsValue'];
	$contract->propertyFire->valuableObjectsValue = @$_POST['valuableObjectsValue'];
	$contract->propertyFire->yearBuilt = @$_POST['yearBuilt'];
	$contract->propertyFire->areaSqMt = @$_POST['areaSqMt'];
	
	$error = updateContractData($contract);
	
	/* set back the session variable to the login user */
	//$_SESSION['searchUserName'] = $_SESSION['username'];
	
	
	if($error != -1)
	{
		//contractNumber may have been updated
		$_SESSION['saleId'] = $contract->sale->saleId;
		?>
		Contract Data updated correctly.
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="updateContractDataForm" method="POST" style="display: none;">
		<input type="text" name="action" value="updateContractData" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updateContractDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
		<?php					
	}
	else
	{
		echo "Error:".$error->error.", Number:".$error->errno;
		?>
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="updateContractDataForm" method="POST" style="display: none;">
		<input type="text" name="action" value="updateContractData" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updateContractDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
		<?php				
	}
}
else
{
	?>
		
	Contract Data updated correctly.
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="updateContractDataForm" method="POST" style="display: none;">
	<input type="text" name="action" value="updateContractData" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('updateContractDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>
	<?php
}

				

