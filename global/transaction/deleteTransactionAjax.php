<?php

session_start();

//no direct access to file allowed
define('_INC', 1);

//we have independent file load here.
//we need to move one level down
$globalFilesLocation = "..";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/generalIncludes/globalFunctions.php");
require_once($globalFilesLocation."/database/connect.php");
require_once($globalFilesLocation."/database/connectorHierarchy.php");
require_once($globalFilesLocation."/database/deleteDatalayers.php");
require_once($globalFilesLocation."/database/insertDatalayers.php");
require_once($globalFilesLocation."/database/retrieveDatalayers.php");


$transId = $_GET['transId'];

$parameterNameValueArray = array();
$parameterNameValue = new parameterNameValue();
$parameterNameValue->name = "transId";
$parameterNameValue->value = $transId;
$parameterNameValueArray[] = $parameterNameValue;

//retrieve all transaction info
$transactions = retrieveTransactions($parameterNameValueArray, "");

$affectedRows = deleteFromTable('transaction', 'transId', $transId);

//add to history table
$localHistory = new history();
$localHistory->username = $_SESSION['username'];
$localHistory->type = $HISTORY_TYPE_CONTRACT;
$localHistory->parameterName = "saleId";
$localHistory->parameterValue = $transactions[0]->saleId;
$localHistory->note = "$localHistory->username : Contract with $localHistory->parameterName = $localHistory->parameterValue. Deleting transaction with Date=".$transactions[0]->transDate.",Receipt No=".$transactions[0]->receiptNo.",Details=".$transactions[0]->details.",Debit=".$transactions[0]->debit.",Credit=".$transactions[0]->credit.",Remainder=".$transactions[0]->remainder;
insertNewHistory($localHistory);
		
$_SESSION['affectedRows'] = $affectedRows;

if($_SESSION['affectedRows']==1)
	echo "Transaction Deleted";
else
	echo "Transaction Cannot be Delete";
//echo "affected rows = ".$affectedRows."<br>"; //this is printed in the html <div> as the responsetext

//echo "stateId bn is :$user->stateId, firstName is:$user->firstName username = " . $_SESSION['searchUserName'] . "<br>";

?>