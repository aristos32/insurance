<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//echo "actionRead=$actionRead";
//process only real requests - not language change
if($actionRead!='')
{
	$previousRemainder;
	
	$transaction = new transaction();
	$transaction->transDate = $_POST['transDate1'];
	$transaction->details = $_POST['details1'];
	$transaction->receiptNo = $_POST['receiptNo1'];
	$transaction->debit = $_POST['debit1'];
	$transaction->credit = $_POST['credit1'];
	$transaction->saleId = $_POST['saleId'];
	$transaction->producer = $_SESSION['producer'];
	
	//$transaction->printData();
	
	//retrieve last transaction
	$parameterNameValueArray = array();
	$parameterNameValue = new parameterNameValue();
	$parameterNameValue->name = "saleId";
	$parameterNameValue->value = $transaction->saleId;
	$parameterNameValueArray[] = $parameterNameValue;
	
	$transactions = retrieveTransactions($parameterNameValueArray, " limit 5");
	
	//some transactions exist already
	if(count($transactions) > 0 )
		$previousRemainder = $transactions[0]->remainder;
	//no transaction exists on database
	else
		$previousRemainder = 0;
	
	
	//new remainder = old remainder + debit - credit
	$transaction->remainder = $previousRemainder + $transaction->debit - $transaction->credit;
	
	$transId = insertNewTransaction($transaction);
	
	if($transId>0)
	{
		//add to history table only on success
		$localHistory = new history();
		$localHistory->username = $_SESSION['username'];
		$localHistory->type = $HISTORY_TYPE_CONTRACT;
		$localHistory->parameterName = "saleId";
		$localHistory->parameterValue = $transaction->saleId;
		$localHistory->note = "$localHistory->username : Contract with $localHistory->parameterName = $localHistory->parameterValue. Insert new transaction with TranId=$transId, Date=".$transaction->transDate.",Receipt No=".$transaction->receiptNo.",Details=".$transaction->details.",Debit=".$transaction->debit.",Credit=".$transaction->credit.",Remainder=".$transaction->remainder;
		insertNewHistory($localHistory);

		echo $_SESSION['addSuccessfull'] ?> .
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="updateContractDataForm" method="POST" style="display: none;">
		<input type="text" name="action" value="updateContractData" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updateContractDataForm').submit()"><?php echo $_SESSION['backToContract'];?></a>
		
		<?php
	}
	else
	{
		?>
		Failure
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="updateContractDataForm" method="POST" style="display: none;">
		<input type="text" name="action" value="updateContractData" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updateContractDataForm').submit()">Continue</a>
		<?php
	}
}
else
{
	?>
	Transaction Added Successfully
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="updateContractDataForm" method="POST" style="display: none;">
	<input type="text" name="action" value="updateContractData" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('updateContractDataForm').submit()">Continue</a>
	
	<?php
}

?>