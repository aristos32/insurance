<script>
$(document).ready(function(){
    
  //initialize
  $('#transDate1').datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10', dateFormat: 'dd-mm-yy'});	   	   
  
}); //ready
</script>

<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//remainder of the last stored transcaction
$previousRemainder = 0;

$transactions = array();
$parameterNameValueArray = array();
$parameterNameValue = new parameterNameValue();
$parameterNameValue->name = "saleId";
$parameterNameValue->value = $_SESSION['saleId'];
$parameterNameValueArray[] = $parameterNameValue;
$transactions = array_reverse(retrieveTransactions($parameterNameValueArray, ""));//reverse array to become ascending by date

?>

<table>
	<tr>
		<td colspan="2"><div id="message" style="display:inline;color:#00FF00">&nbsp;</div></td>
		<td><div id="secondParameter" style="display:inline;color:#00FF00">&nbsp;</div></td>
	</tr>
	<tr>
		<td class="col15Per"><b><?php echo $_SESSION['date']; ?></b></td>
		<td class="col15Per"><b><?php echo $_SESSION['details']; ?></b></td>
		<td class="col15Per"><b><?php echo $_SESSION['receipt']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['debit']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['credit']; ?></b></td>
		<td class="col10Per"><b><?php echo $_SESSION['remainder']; ?></b></td>
	</tr>
	<?php
	$j = 0;	
	foreach($transactions as $transaction)
	{
		//store previous remainder, which comes first from the query
		if( $j == 0 )
			$previousRemainder = $transaction->remainder;
		?>
		<tr id="<?php echo $transaction->transId;?>">
			<td class="col15Per"><?php echo $transaction->transDate; ?></td>
			<td class="col15Per"><?php echo $transaction->details; ?></td>
			<td class="col15Per"><?php echo $transaction->receiptNo; ?></td>
			<td class="col10Per"><?php echo $transaction->debit; ?></td>
			<td class="col10Per"><?php echo $transaction->credit; ?></td>
			<td class="col10Per"><?php echo $transaction->remainder; ?></td>
			<td class="col1Per"><a href="javascript:;" onclick="javascript:deleteTransactionAjax('<?php echo $transaction->transId;?>');"><img src="./images/delete-sign.jpg"/> </a></td>
		</tr>
		<?php
		$j = $j + 1;
	}
	//MADE THIS A FORM, NOT AJAX - FINISH FINISH FORM PROCESSING
	?>
	<form name="insertTransactionForm" action="./office.php" method="POST" onSubmit="return checkInsertNewTransactionValidity();">
		<input type="hidden" name="action" value="insertNewTransactionFormProcess">
		<input type="hidden" name="saleId" value="<?php echo $_SESSION['saleId'];?>">
		
		<tr>
		<td class="col15Per"><input type="text" class="datepick" size="8" name="transDate1" id="transDate1" value="<?php echo date('d-m-Y');?>" /></td>
		<td class="col15Per"><select name="details1" id="details1" style="width:8em;">
		<?php
		$j = 1;
		foreach($_SESSION['transactionDetailsOptions'] as $option){
			?>
			 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
			 <?php
			 $j = $j + 1;
		 }
		 ?>
		 </select>	</td>
		<td class="col15Per"><input type="text" name="receiptNo1" size="3" id="receiptNo1" value="" /></td>
		<td class="col10Per"><input type="text" name="debit1" size="3" id="debit1" value="" /></td>
		<td class="col10Per"><input type="text" name="credit1" size="3" id="credit1" value="" /></td>
		<td class="col1Per"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['new'];?>" size="30" /></td>
		<td class="col10Per"><a href="./office.php?action=viewAllContractTransactions"><?php echo $_SESSION['all']; ?></a></td>
		<!-- <td><a href="javascript:;" onclick="javascript:if(checkInsertNewTransactionValidity()) insertNewTransactionAjax('<?php echo $contract->quoteId;?>');"><?php echo $new;?></a></td> -->
	
		</tr>
	</form>
	<tr>
		<td><a href="./office.php?action=updateContractData"><?php echo $_SESSION['backToContract']; ?></a></td>
	</tr>
</table>
