<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

?>

<script type="text/javascript">	

function addRowInUploadFilesManagementTable(tableID)
{

    var table = document.getElementById(tableID);
	var columnSize = "7";
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
	var element;
	
    //firstname
    var cell1 = row.insertCell(0);
    element = document.createElement("input");
    element.type = "file";
    element.name = "fileArray[]";
    element.size = columnSize;
    cell1.appendChild(element);
    
    //fileName
    var cell2 = row.insertCell(1);
    element = document.createElement("input");
    element.type = "text";
    element.name = "fileName[]";
    element.size = columnSize;
    cell2.appendChild(element);

   
	 
}
</script>


<?php	
//Load all existing files
//'saleId' folder doesn't exist
$existingUploadedFiles = getListOfFiles($_SESSION['clientFilesLocation']."/uploadedFiles/", $_SESSION['saleId']."_");
if(count($existingUploadedFiles)>0)
//if(is_dir($_SESSION['clientFilesLocation']."/uploadedFiles/".$_SESSION['saleId']))
{
	?>
	<table>
	<tr>
		<td colspan="2"><div id="message" style="display:inline;color:#00FF00">&nbsp;</div></td>
		<td><div id="secondParameter" style="display:inline;color:#00FF00">&nbsp;</div></td>
	</tr>
	<?php
	//Get list of already existing uploaded files
	foreach($existingUploadedFiles as $eachExistingUploadedFiles)
	{
		$completeFilePathForAjax = "../".$_SESSION['clientFilesLocation']."/uploadedFiles/".$eachExistingUploadedFiles;
		
		?>
		<tr id="<?php echo $completeFilePathForAjax;?>">
			<td class="col15Per"><?php echo $eachExistingUploadedFiles; ?></td>
			<td class="col10Per">
				<a href="<?php echo $completeFilePathForAjax; ?>" target=\"_BLANK\"><?php echo $_SESSION['view'];?></a>
			<td><a href="javascript:;" onclick="javascript:deleteUploadedFileAjax('<?php echo $completeFilePathForAjax;?>');"><img src="./images/delete-sign.jpg"/> </a></td>
		</tr>
		<?php
	}
	?>
	</table>
	<br/><br/>
	<?php
//Upload new files
}
?>
<form name="uploadFilesManagement" method="post" enctype="multipart/form-data">
	<input type="hidden" name="action" value="uploadFilesManagementProcess">
	<table id='uploadFilesManagementTable'>

	<tr>
		<td class="col10Per"></td>
		<td><?php echo $_SESSION['fileName']; ?></td>
	</tr>
	<tr>
		<!-- set default language in the browser to display for 'file' -->
		<td><input type="file" name="fileArray[]" /> </td>
		<td><input type="text" name="fileName[]" size="10"/></td>
	</tr>
	</table>
	<INPUT type="button" value="<?php echo $_SESSION['add'];?>" onclick="addRowInUploadFilesManagementTable('uploadFilesManagementTable')" />
	 
	<br/><br/>
	<input type="submit" name="submit" value="<?php echo $_SESSION['continue'];?>" />
</form>

<!-- create hidden forn, to send the action as a POST -->
<form action="./office.php" id="updateContractDataForm" method="POST" style="display: none;">
<input type="text" name="action" value="updateContractData" />
</form>
<a href="javascript:;" onclick="javascript: document.getElementById('updateContractDataForm').submit()"><?php echo $_SESSION['backToContract'];?></a>