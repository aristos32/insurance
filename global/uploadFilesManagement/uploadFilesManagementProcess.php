<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//process only real requests - not language change
if($actionRead!='')
{
	$errorFound = 'false';
	$fileFound = 'false';
	
	//read fields that come as arrays
	$fileNames = @$_POST['fileName'];
		
	//see http://php.net/manual/en/control-structures.foreach.php
	//loop through all new files to upload
	foreach ($_FILES["fileArray"]["name"] as $key => $name)
	{
		
	
		$extension = strtolower(end(explode(".", $_FILES["fileArray"]["name"][$key])));
		//print_r($allowedExts);
		//print_r($extension);	
		
		//echo "Upload: " . $_FILES["fileArray"]["name"][$key] . "<br />";
		//echo "Type: " . $_FILES["fileArray"]["type"][$key] . "<br />";
		//echo "Size: " . ($_FILES["fileArray"]["size"][$key] / 1024) . " Kb<br />";
		//echo "Temp file: " . $_FILES["fileArray"]["tmp_name"][$key] . "<br />";
		
		//empty file name - use default
		if($fileNames[$key]=='')
			$fileNames[$key] = $_FILES["fileArray"]["name"][$key];
		//rename filename and add extension
		else
			$fileNames[$key] = $fileNames[$key].".".$extension;
		
			
		//Check FILE SIZE 
		if ($_FILES["fileArray"]["size"][$key] > $MAX_UPLOAD_FILE_SIZE)
		{
			 echo $_FILES["fileArray"]["name"][$key].":".$_SESSION['fileSizeMustBeSmallerThan']." ".$MAX_UPLOAD_FILE_SIZE." bytes. <br/>";
			 $errorFound = 'true';
			
		}
		else
		{
			//Check file TYPE
			if((($_FILES["fileArray"]["type"][$key] == "image/gif")
			|| ($_FILES["fileArray"]["type"][$key] == "image/jpeg")
			|| ($_FILES["fileArray"]["type"][$key] == "image/pjpeg")
			|| ($_FILES["fileArray"]["type"][$key] == "application/pdf"))
			&& in_array($extension, $UPLOAD_FILE_EXTENTIONS))
			{
			  if ($_FILES["fileArray"]["error"][$key] > 0)
			  {
				  echo "ERROR: " . $_FILES["fileArray"]["error"][$key] . "<br />";
			  }
			  else
			  {
				  //'saleId' folder doesn't exist
				  /*if(!is_dir($_SESSION['clientFilesLocation']."/uploadedFiles/".$_SESSION['saleId']))
				  {
					  mkdir($_SESSION['clientFilesLocation']."/uploadedFiles/".$_SESSION['saleId']);	
				
				  }*/
				
				  //$scriptUid = getmyuid();
				 // echo "the script user Id = $scriptUid <br/>";
				  
				  //$scriptOwnerName = get_current_user();
				  //echo "the script owner user name = $scriptOwnerName";
				  
				  //Give all access rights
				  //chmod($_SESSION['clientFilesLocation']."/uploadedFiles/".$_SESSION['saleId'], 0777);			    
				  
				  //chown($_SESSION['clientFilesLocation']."/uploadedFiles/".$_SESSION['saleId'], $scriptOwnerName);
				  
				  //Get list of already existing uploaded files
				  $existingUploadedFiles = getListOfFiles($_SESSION['clientFilesLocation']."/uploadedFiles/", $_SESSION['saleId']."_");
				  
				   //files were already uploaded
				  if(count($existingUploadedFiles)>0)
				  {
					  //echo "found uploaded files <br/>";
					  //delete unwanted uploade files
					  foreach($existingUploadedFiles as $eachExistingUploadedFile)
					  {
						  //file found
						  if($eachExistingUploadedFile == $_SESSION['saleId']."_".$_FILES["fileArray"]["name"][$key])
						  {
							 //echo "same file name found <br/>";
							  //files not identical
							  if(!files_identical($_SESSION['clientFilesLocation']."/uploadedFiles/".$eachExistingUploadedFile, $_FILES["fileArray"]["tmp_name"][$key]))
							  {
								  echo "Files not identical. Deleting old... Uploading new... <br/>";
								  //delete existing
								  unlink($_SESSION['clientFilesLocation']."/uploadedFiles/".$eachExistingUploadedFile);
								  //upload new one
								  move_uploaded_file($_FILES["fileArray"]["tmp_name"][$key],  $_SESSION['clientFilesLocation']."/uploadedFiles/".$_SESSION['saleId']."_". $fileNames[$key]);
							  }
							  else
							  {
								  echo "File ".$_SESSION['saleId']."_".$_FILES["fileArray"]["name"][$key] . " already exists. Not uploading again... <br/>";
							  }
							  $fileFound = 'true';
							  break;
						  }
					  }
					  //file not found in new list - delete it
					  if($fileFound == 'false')
					  {
						  echo $_SESSION['fileName']."=".$_FILES["fileArray"]["name"][$key].". Uploading...<br/>";
					      move_uploaded_file($_FILES["fileArray"]["tmp_name"][$key],  $_SESSION['clientFilesLocation']."/uploadedFiles/".$_SESSION['saleId']."_". $fileNames[$key]);
					  }
					  $fileFound == 'false';//reset variable
				  }
			      //no files were uploaded - just upload all
			      else
			      {
				      echo "New file found=".$_FILES["fileArray"]["name"][$key].". Uploading... <br/>";
				      move_uploaded_file($_FILES["fileArray"]["tmp_name"][$key],  $_SESSION['clientFilesLocation']."/uploadedFiles/".$_SESSION['saleId']."_". $_FILES["fileArray"]["name"][$key]);
				       // open some file for reading
						/*$file = $fileNames[$key];
						$fp = fopen($file, 'r');
						
						// set up basic connection
						$conn_id = ftp_connect('188.121.62.170');
						
						// login with username and password
						$login_result = ftp_login($conn_id, 'cyprusins', 'KzJZ*WB6gUJvrI');
						
						// try to upload $file
						if (ftp_fput($conn_id, $file, $fp, FTP_ASCII)) {
						    echo "Successfully uploaded $file\n";
						} else {
						    echo "There was a problem while uploading $file\n";
						}
						
						// close the connection and the file handler
						ftp_close($conn_id);
						fclose($fp);*/
			      }
			  }
			}
			else
			{
				echo $_SESSION['notAllowedFileType'].".".$_SESSION['allowedTab'].": ";
				foreach($UPLOAD_FILE_EXTENTIONS as $eachExtention)
				{
					echo $eachExtention.", ";//add all allowed extentions
				}
				
			 	$errorFound = 'true';
				
			}
		}

	}
	
	
	
	
	//error in file processing
	if($errorFound=='true')
	{
		?>
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="uploadFilesManagementForm" method="POST" style="display: none;">
		<input type="text" name="action" value="uploadFilesManagement" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('uploadFilesManagementForm').submit()"><?php echo $_SESSION['tryAgain']; ?></a>
		<?php				
	}
	else
	{
		echo $_SESSION['uploadSuccessfull'].".";
		?>
		<!-- create hidden forn, to send the action as a POST -->
		<form action="./office.php" id="updateContractDataForm" method="POST" style="display: none;">
		<input type="text" name="action" value="updateContractData" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('updateContractDataForm').submit()"><?php echo $_SESSION['continue'];?></a>
		<?php	
		
	}
}
else
{
	?>
	<!-- create hidden forn, to send the action as a POST -->
	<form action="./office.php" id="uploadFilesManagementForm" method="POST" style="display: none;">
	<input type="text" name="action" value="uploadFilesManagement" />
	</form>
	<?php echo $_SESSION['uploadSuccessfull']."."; ?>
	<a href="javascript:;" onclick="javascript: document.getElementById('uploadFilesManagementForm').submit()"><?php echo $_SESSION['continue']; ?></a>
	<?php
}
  
?> 