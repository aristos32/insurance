<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$deleteUserName = $_SESSION['deleteUserName'];

//delete from global database
include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
//store info for database and client name before deleting
$usersFromGlobal = retrieveUserInfoFromGlobalDatabase($deleteUserName);
$userFromGlobalToDelete = $usersFromGlobal[0];

deleteFromTable('systemuser', 'username', $deleteUserName);

//delete from appropriate client database
//connect to database of user to be deleted
$userClientFilesLocation =  "../clients/".$userFromGlobalToDelete->clientName;
include $userClientFilesLocation."/database/localDatabaseConstants.php";

//retrieve stateId from systemUser
$users = retrieveUsersInfo($deleteUserName);
$deleteStateId = $users[0]->stateId;
deleteFromTable('owner', 'stateId', $deleteStateId);

deleteFromTable('systemuser', 'username', $deleteUserName);


//return to database of logged-in user
include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";


//add to history table only on success
$localHistory = new history();
$localHistory->username = $_SESSION['username'];
$localHistory->type = $HISTORY_TYPE_USER;
$localHistory->parameterName = "username";
$localHistory->parameterValue = $deleteUserName;
$localHistory->note = "$localHistory->username : Deleted User with username=$deleteUserName";
insertNewHistory($localHistory);

echo $_SESSION['deleteSuccessfull']; ?>. 

<!-- create hidden forn, to send the action as a POST -->
<form action="<?php if($runFromGlobalLocationQuotation==true) echo "./quotation.php"; else echo "./office.php"; ?>" id="userFindMenuProcess" method="post" style="display: none;">
<input type="text" name="action" value="userFindMenuProcess" />
</form>
<a href="javascript:;" onclick="javascript: document.getElementById('userFindMenuProcess').submit()"><?php echo $_SESSION['continue']; ?></a>
