<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');
?>
<script>

$(document).ready(function(){

    $("#birthDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#birthDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
	    
    $("#licenseIssueDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#licenseIssueDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
        
});
</script>

<h2>Create New User</h2>
<form name="createNewUser" action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" method="post" onSubmit="return checkCreateNewUserForm('userAddresses')">
	<input type="hidden" name="action" value="newUserFormProcess">
	
	<table>
			
		<tr><td class="label"><?php echo $_SESSION['usernameTab']; ?>:<span class="required">(*)</span></td>
		<td class="input"><input type="text" name="newUsername" id="newUsername" size="30" value="" /><br /></td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['password']; ?>:<span class="required">(*)</span></td>
		<td class="input"><input type="password" name="newPassword" id="newPassword" value="" /><br /></td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['retypePassword']; ?>:<span class="required">(*)</span></td>
		<td class="input"><input type="password" name="password2" id="password2" value="" /><br /></td></tr>	
	
		<tr><td class="label">Email:<span class="required">(*)</span><br /></td>
		<td class="input"><input type="text" name="email" id="email" size="30" value="" /><br /></td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['emailPreference'] ?>:</td>
		<td class="input"><select name="preferenceCode" id="preferenceCode">
				<?php
				foreach($_SESSION['emailPreferenceOptions'] as $option)
				{
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<?php
		//admin - set default clientName
		if($_SESSION['role'] == $USER_ROLE_ADMINISTRATOR )
		{
			?>
			<input type="hidden" name="clientName" value="<?php echo $_SESSION['clientName']; ?>" />
			<?php
		}
		//SUPER - select which client this user belongs to
		else if($_SESSION['role'] == $USER_ROLE_SUPER )
		{
			//show all available database names
			?>
			<tr>
				<td class="label"><?php echo $_SESSION['account'] ?>:</td>
				<td class="input"><select name="clientName" id="clientName">
					<?php
					foreach($_SESSION['accounts'] as $eachAccount)
					{
						?>
						 <option  value=<?php echo "$eachAccount->clientName" ?> ><?php echo "$eachAccount->clientName" ?> </option>
						 <?php
					 }
					 ?>
				</select>
				</td>
			</tr>
			<?php
			
			//on change, update fields clientName
		}
		
		?>
		
		<tr>
			<td class="label"><?php echo $_SESSION['product'] ?>:</td>
			<td class="input"><select name="productType">
				<?php
				foreach($_SESSION['productTypes'] as $eachProductType)
				{
					?>
					 <option  value=<?php echo "$eachProductType" ?> ><?php echo "$eachProductType" ?> </option>
					 <?php
				 }
				 ?>
			</select>
			</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['userRole'] ?>:</td>
		<td class="input"><select name="userRole" id="userRole">
				<?php
				foreach($_SESSION['userRoleOptions'] as $option)
				{
					//can create only of lower role than this
					if($option->value <= $_SESSION['role'] )
					{
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 	}
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['producerTab'] ?>:</td>
		<td class="input"><select name="producer" id="producer">
				<?php
				foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
			
		<tr><td class="label"><?php echo $_SESSION['stateIdTab']; ?>:</td>
		<td class="input"><input type="text" name="stateId" id="stateId" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['nameTab']; ?>:</td>
		<td class="input"><input type="text" name="firstName" id="firstName" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['surname']; ?>:</td>
		<td class="input"><input type="text" name="lastName" id="lastName" size="30" value="" /> </td></tr>
		
		</table>
		<?php
		
		
		$emptyAddressesArray = array();
		displayAddresses($emptyAddressesArray);
		
		?>	
		<table>
		<tr><td class="label"><?php echo $_SESSION['gender'] ?>:</td>
		<td class="input"><select name="gender" id="gender">
				<?php
				foreach($_SESSION['genderOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
				
		<tr><td class="label"><?php echo $_SESSION['telephoneTab']; ?>:</td>
		<td class="input"><input type="text" name="telephone" id="telephone" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['cellphoneTab']; ?>:</td>
		<td class="input"><input type="text" name="cellphone" id="cellphone" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['profession']; ?>:</td>
		<td class="input"><input type="text" name="profession" id="profession" size="30" value="" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['birthDate']; ?>:</td>
		<td class="input">
		<input type="text" style="width: 80px;" id="birthDate" name="birthDate" />
		
		</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['licenseDateTab']; ?>:</td>
		<td class="input">
		<input type="text" style="width: 80px;" id="licenseIssueDate" name="licenseIssueDate" />
				
		</td>
		</tr>
		
		<tr>
			<td class="label"><?php echo $_SESSION['howDidYouHearAboutUs'];?></td>
			<td class="input"><select name="howDidYouHearAboutUs" id="howDidYouHearAboutUs">
				<?php
				foreach($_SESSION['howDidYouHearAboutUsOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
				</select>
			</td>
		</tr>
                <tr>
			<td class="label"><img id="captcha" src="./securimage/securimage_show.php" alt="CAPTCHA Image" /></td>
			<td class="input"><input type="text" name="captcha_code" size="10" maxlength="6" />
                            <a href="#" onclick="document.getElementById('captcha').src = './securimage/securimage_show.php?' + Math.random(); return false">[ Different Image ]</a>
			</td>
		</tr>
		
		<tr><td class="label"><INPUT type="submit" class="button" value="<?php echo $_SESSION['create']; ?>" size="15" ></td><td class="input"></td></tr>
	</table>
</form>


