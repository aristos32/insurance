<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

include_once './securimage/securimage.php';
 
$securimage = new Securimage();

//echo "actionRead=$actionRead";
//process only real requests - not language change
if($actionRead!='')
{
	$newUser = new user();
	$newUser->clientName = $_POST['clientName'];
	$newUser->productType = $_POST['productType'];
	$clientFilesLocation =  "../clients/".$newUser->clientName;

	$maxNumberOfUsers;
	
	//get maximum number of users from config.xml
	$_SESSION['parametersArray'] = readClientProperties($clientFilesLocation.'/configurationFiles/config.xml');
	foreach($_SESSION['parametersArray'] as $eachParameter)
	{
		if($eachParameter->name=='MAX_USERS_NUMBER')
		{
			$maxNumberOfUsers = $eachParameter->value;
			//echo "maxNumberOfUsers=$maxNumberOfUsers. <br/>";
			break;
		}
	}
	unset($_SESSION['parametersArray']);
	
	//get current number of users in GLOBAL database for this account
	include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
	$currentNumberOfUsers = retrieveCurrentNumberOfUsers($newUser->clientName);
	//echo "currentNumberOfUsers=$currentNumberOfUsers. <br/>";
	
        //check if CAPCHA was correct
        if ($securimage->check($_POST['captcha_code']) == false)
        {
            // the code was incorrect
            // you should handle the error so that the form processor doesn't continue
            // or you can use the following code if there is no validation or you do not know how
            echo "The security code entered was incorrect.<br /><br />";
            echo "Please go <a href='javascript:history.go(-1)'>back</a> and try again.";
            exit;

        }
          
	//check if maximum number of users is exceeded
	if($currentNumberOfUsers>=$maxNumberOfUsers)
	{
		//echo "Error:".$localError->error.", Number:".$localError->errno.".";
		echo $_SESSION['yourAccountDoesntAllowMoreUsers'].":$maxNumberOfUsers";
		?>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="homePage" method="POST" style="display: none;">
			<input type="text" name="action" value="homePage" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('homePage').submit()"><?php echo $_SESSION['continue']; ?>. </a>
		 
			<?php
	}
	else
	{
		//store variables from createNewUser form
		$statistics = new statistics();
		
		$userCity;//store user city for statistic purposes
		
		$newUser->username = $_POST['newUsername'];
		$newUser->password = md5($_POST['newPassword']);
		$newUser->firstName = $_POST['firstName'];
		$newUser->lastName = $_POST['lastName'];
		$newUser->email = $_POST['email'];
		$newUser->stateId = $_POST['stateId'];
		
		//generage random stateId because is needed for internal functionality
		if($newUser->stateId=='')
			$newUser->stateId = generateRandomString(10);
		
		$newUser->emailPreference = new emailPreference();
		$newUser->emailPreference->stateId = $newUser->stateId;
		$newUser->emailPreference->preferenceCode = $_POST['preferenceCode'];
		$newUser->emailPreference->preferenceFrequency = $FREQUENCY_MONTHLY;//for now, set default as monthly
		
		//read fields that come as arrays
		$addressTypes = @$_POST['addressType'];
		$addressIds = @$_POST['addressId'];
		$streets = @$_POST['street'];
		$codes = @$_POST['code'];
		$citys = @$_POST['city'];
		$countrys = @$_POST['country'];
		
		$newUser->addresses = array();//initialize to empty array
		
		
		//cannot add address when stateid is empty
		//todo - remove this
		if($newUser->stateId=="" && isset($addressTypes))
		{
			echo $_SESSION['whenAddingAnAddressStateIdisMandatory']; ?>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="createNewUserForm" method="POST" style="display: none;">
			<input type="text" name="action" value="newUserForm" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('createNewUserForm').submit()"><?php echo $_SESSION['tryAgain']; ?>. </a>
		 
			<?php
			
		}
		else
		{
			if($newUser->stateId!="")
			{
				if(isset($addressTypes))
				{
					//set all array elements
					foreach($addressTypes as $a => $b)
					{
				  		$localAddress = new ownerAddress();
				  		$localAddress->stateId = $newUser->stateId;
				  		$localAddress->addressType = $addressTypes[$a];
						$localAddress->addressId = $addressIds[$a];
						$localAddress->street = $streets[$a];
						$localAddress->areaCode = $codes[$a];
						$localAddress->city = $citys[$a];
						$localAddress->country = $countrys[$a];
						
						$userCity = $localAddress->city;
						//$localAddress->printData();
						
						$newUser->addresses[] = $localAddress;
				  		
					}
				}
			}
		
			$newUser->role = $_POST['userRole'];
			$newUser->status = $USER_STATUS_ACTIVE;//status for new user is set to 'Active' by default
			$newUser->gender = $_POST['gender'];
			$newUser->producer = $_POST['producer'];
			$newUser->telephone = $_POST['telephone'];
			$newUser->cellphone = $_POST['cellphone'];
			$newUser->profession = $_POST['profession'];
			$newUser->birthDate = $_POST['birthDate'];
			$newUser->licenseIssueDate = $_POST['licenseIssueDate'];
			$newUser->consecutiveFailLoginAttempts = 0;
			$howDidYouHearAboutUs = $_POST['howDidYouHearAboutUs'];
			
			//insert new user in GLOBAL database
			//include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
			$localError = insertNewUserInGlobalDatabase($newUser);
			
			//connect to appropriate database to add the user and all related information
			$clientFilesLocation =  "../clients/".$newUser->clientName;
			include $clientFilesLocation."/database/localDatabaseConstants.php";
					
			//an error is thrown
			if($localError == -1 )
			{
				//echo "Error:".$localError->error.", Number:".$localError->errno.".";
				echo $_SESSION['userNameAlreadyExists'];
				?>
					<!-- create hidden forn, to send the action as a POST -->
					<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="createNewUserForm" method="POST" style="display: none;">
					<input type="text" name="action" value="newUserForm" />
					</form>
					<a href="javascript:;" onclick="javascript: document.getElementById('createNewUserForm').submit()"><?php echo $_SESSION['tryAgain']; ?>. </a>
				 
					<?php
			}
			//user was added successfully in global database -> continue to add in appropriate client database
			else
			{
				/* Insert into client database */
				insertNewUser($newUser);
				
				/* Insert emailPreference */
				insertEmailPreference($newUser->emailPreference);
				
				//add to history table only on success
				$localHistory = new history();
				$localHistory->username = $_SESSION['username'];
				$localHistory->type = $HISTORY_TYPE_USER;
				$localHistory->parameterName = "username";
				$localHistory->parameterValue = $newUser->username;
				$localHistory->note = "$localHistory->username : Added new User with username=$newUser->username";
				insertNewHistory($localHistory);
		
				/* success */
				/*Send Automated email to administrator */
				//sendAutomatedEmail($newUser->username);
				$subject = "New User Created";
				$message = "New user created in the system with username: $newUser->username";
				$headers   = array();
				$headers[] = "MIME-Version: 1.0";
				$headers[] = "Content-type: text/plain; charset=iso-8859-1";
				$headers[] = "From: ".$SYSTEM_NOREPLY_EMAIL;
				sendEmail($subject, $message, $_SESSION['SYSTEM_EMAIL'], $_SESSION['SYSTEM_EMAIL'], $headers);//system_email is of the account the user is create in, not current account
				
				//echo "system email =".$_SESSION['SYSTEM_EMAIL'];
				
				$message = "Dear user, <br><br> Your login Information is username: $newUser->username, password:".$_POST['newPassword']." <br> Please keep this in a safe place. <br>Best Regards,\nOnline-Insurance-App";
				$headers   = array();
				$headers[] = "MIME-Version: 1.0";
				$headers[] = "Content-type: text/plain; charset=iso-8859-1";
				$headers[] = "From: ".$SYSTEM_NOREPLY_EMAIL;
				sendEmail($subject, $message, $newUser->email, $newUser->email, $headers);
				
				/* insert the statistics */
				$statistics->code = $HOW_DID_YOU_HEAR_ABOUT_US;
				$statistics->value = $howDidYouHearAboutUs;
				insertStatistics($statistics);
				
				$statistics->code = $OWNER_RESIDENCE;
				$statistics->value = @$localAddress->city;//aristosa : is correct to use this city??
				insertStatistics($statistics);
				
				
			  	echo $_SESSION['addSuccessfull']; ?>.  
			  	<!-- create hidden forn, to send the action as a POST -->
				<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="homePage" method="POST" style="display: none;">
				<input type="text" name="action" value="homePage" />
				</form>
				<a href="javascript:;" onclick="javascript: document.getElementById('homePage').submit()"><?php echo $_SESSION['continue']; ?></a>
			  	<?php
			}
			
			//return to local database for any following queries
			//ATTENTION - use include instead of require_once to override previous values
			include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";
		}
	}
	
}
else
{
	echo $_SESSION['addSuccessfull']; ?>. 
	<!-- create hidden forn, to send the action as a POST -->
	<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="homePage" method="POST" style="display: none;">
	<input type="text" name="action" value="homePage" />
	</form>
	<a href="javascript:;" onclick="javascript: document.getElementById('homePage').submit()"><?php echo $_SESSION['continue']; ?></a>
	<?php
}
	
?>
			
	