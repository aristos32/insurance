<?php

session_start();

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//we have independent file load here.
//we need to move one level down
$globalFilesLocation = "..";
require_once($globalFilesLocation."/generalIncludes/structures.php");
require_once($globalFilesLocation."/database/connect.php");
require_once($globalFilesLocation."/database/updateDatalayers.php");

$user = new user();
$user->username = $_SESSION['searchUserName'];
$user->stateId = $_GET['stateId'];
$user->gender = $_GET['gender'];
$user->firstName = $_GET['firstName'];
$user->lastName = $_GET['lastName'];
$user->telephone = $_GET['telephone'];
$user->cellphone = $_GET['cellphone'];
$user->profession = $_GET['profession'];
$user->email = $_GET['email'];
$user->birthDate = $_GET['birthDate'];
$user->licenseIssueDate = $_GET['licenseIssueDate'];

$localAddress = new ownerAddress();
$localAddress->stateId = $user->stateId;
$localAddress->addressType = $ADDRESS_TYPE_RESIDENCE;
$localAddress->addressId = $_GET['residenceAddressId'];
$localAddress->street = $_GET['residenceAddressStreet'];
$localAddress->areaCode = $_GET['residenceAddressAreaCode'];
$localAddress->city = $_GET['residenceAddressCity'];
$localAddress->state = $_GET['residenceAddressState'];
$localAddress->country = $_GET['residenceAddressCountry'];
$user->addresses[] = $localAddress;

$localAddress = new ownerAddress();
$localAddress->stateId = $user->stateId;
$localAddress->addressType = $ADDRESS_TYPE_CORRESPONDENCE;
$localAddress->addressId = $_GET['correspondenceAddressId'];
$localAddress->street = $_GET['correspondenceAddressStreet'];
$localAddress->areaCode = $_GET['correspondenceAddressAreaCode'];
$localAddress->city = $_GET['correspondenceAddressCity'];
$localAddress->state = $_GET['correspondenceAddressState'];
$localAddress->country = $_GET['correspondenceAddressCountry'];
$user->addresses[] = $localAddress;

$localAddress = new ownerAddress();
$localAddress->stateId = $user->stateId;
$localAddress->addressType = $ADDRESS_TYPE_BUSINESS;
$localAddress->addressId = $_GET['businessAddressId'];
$localAddress->street = $_GET['businessAddressStreet'];
$localAddress->areaCode = $_GET['businessAddressAreaCode'];
$localAddress->city = $_GET['businessAddressCity'];
$localAddress->state = $_GET['businessAddressState'];
$localAddress->country = $_GET['businessAddressCountry'];
$user->addresses[] = $localAddress;

//update user data in local database
//need to go one level lower now
include "../".$_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";
$localError = updateUserData($user);
$affectedRows = $localError->affectedRows;

$_SESSION['affectedRows'] = $affectedRows;

echo "affected rows = ".$affectedRows."<br>"; //this is printed in the html <div> as the responsetext

//echo "stateId bn is :$user->stateId, firstName is:$user->firstName username = " . $_SESSION['searchUserName'] . "<br>";

?>