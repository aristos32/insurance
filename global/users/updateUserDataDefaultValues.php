<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

$gender_content = $user->gender;
$userRole_content = $user->role;
$producer_content = $user->producer;
$userStatus_content = $user->status;
$productType_content = $user->productType;
$preferenceCode_content = $user->emailPreference->preferenceCode;

//echo "user role is $user->role <br>";

$birthDateDay_content =  substr( $user->birthDate, 0, 2 );
$birthDateMonth_content = substr( $user->birthDate, 3, 2 );
$birthDateYear_content = substr( $user->birthDate, 6, 4 );

$licenseIssueDateDay_content =  substr( $user->licenseIssueDate, 0, 2 );
$licenseIssueDateMonth_content = substr( $user->licenseIssueDate, 3, 2 );
$licenseIssueDateYear_content = substr( $user->licenseIssueDate, 6, 4 );	
?>