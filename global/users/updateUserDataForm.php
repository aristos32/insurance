<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
//test
tabdropdown.init("colortab", "5")
</script>

<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

?>
<script type="text/javascript">	
//AJAX Functionality - NOT USED NOW - NOT TESTED AFTER CHANGES
/* fill specific data, before calling the generic AJAX functions */
function updateUserDataAjax()
{
	
	//file to execute
	url = "./users/updateUserDataAjax.php";
	//read all variables to update automatically
    var stateId = document.getElementById('stateId').value;
    var firstName = document.getElementById('firstName').value;
    var lastName = document.getElementById('lastName').value;
    var email = document.getElementById('email').value;
  	var telephone = document.getElementById('telephone').value;
    var cellphone = document.getElementById('cellphone').value; 
    var residenceAddressStreet = document.getElementById('residenceAddressStreet').value;
    var residenceAddressAreaCode = document.getElementById('residenceAddressAreaCode').value;
    var residenceAddressCity = document.getElementById('residenceAddressCity').value;
  	var residenceAddressState = document.getElementById('residenceAddressState').value;
    var residenceAddressCountry = document.getElementById('residenceAddressCountry').value;
    var correspondenceAddressStreet = document.getElementById('correspondenceAddressStreet').value;
    var correspondenceAddressAreaCode = document.getElementById('correspondenceAddressAreaCode').value;
    var correspondenceAddressCity = document.getElementById('correspondenceAddressCity').value;
  	var correspondenceAddressState = document.getElementById('correspondenceAddressState').value;
    var correspondenceAddressCountry = document.getElementById('correspondenceAddressCountry').value;
    
    //NOT TESTED START
    var residenceAddressId = document.getElementById('residenceAddressId').value;
    var correspondenceAddressId = document.getElementById('correspondenceAddressId').value;
    var businessAddressId = document.getElementById('businessAddressId').value;
    var businessAddressStreet = document.getElementById('businessAddressStreet').value;
    var businessAddressAreaCode = document.getElementById('businessAddressAreaCode').value;
    var businessAddressCity = document.getElementById('businessAddressCity').value;
  	var businessAddressState = document.getElementById('businessAddressState').value;
    var businessAddressCountry = document.getElementById('businessAddressCountry').value;
    //NOT TESTED END
    
    var profession = document.getElementById('profession').value;
    var birthDate = document.getElementById('birthDate').value;
    var licenseIssueDate = document.getElementById('licenseIssueDate').value;
    var gender = document.getElementById('gender').value;
    
    //NOT TESTED START
    queryString = "?stateId=" + stateId + "&firstName=" + firstName + "&lastName=" + lastName + "&email=" + email + "&telephone=" + telephone + "&cellphone=" + cellphone + "&residenceAddressStreet=" + residenceAddressStreet + "&residenceAddressAreaCode=" + residenceAddressAreaCode + "&residenceAddressCity=" + residenceAddressCity + "&residenceAddressState=" + residenceAddressState + "&residenceAddressCountry=" + residenceAddressCountry + "&correspondenceAddressStreet=" + correspondenceAddressStreet + "&correspondenceAddressAreaCode=" + correspondenceAddressAreaCode + "&correspondenceAddressCity=" + correspondenceAddressCity + "&correspondenceAddressState=" + correspondenceAddressState + "&correspondenceAddressCountry=" + correspondenceAddressCountry + "&birthDate=" + birthDate + "&licenseIssueDate=" + licenseIssueDate + "&profession=" + profession + "&gender=" + gender + "&residenceAddressId=" + residenceAddressId + "&correspondenceAddressId=" + correspondenceAddressId + "&businessAddressId=" + businessAddressId + "&businessAddressStreet=" + businessAddressStreet + "&businessAddressAreaCode=" + businessAddressAreaCode + "&businessAddressCity=" + businessAddressCity + "&businessAddressState=" + businessAddressState + "&businessAddressCountry=" + businessAddressCountry;
  	//NOT TESTED END
  	
    //alert(url+queryString);
  
    myFunction(url+queryString);
    //loadXMLDoc("updateUserDataAjax.php");
    return true;
} 

</script>

<?php

$user;

//retrieve the username we search for
$currentUsername = $_SESSION['searchUserName'];

if(isset($_POST['searchClientName']))
	$_SESSION['searchClientName'] = $_POST['searchClientName'];

//$_SESSION['searchClientName'] = $searchClientName;

//connect to appropriate database to add the user and all related information
$clientFilesLocation =  "../clients/".$_SESSION['searchClientName'];
include $clientFilesLocation."/database/localDatabaseConstants.php";

	
$users = retrieveUsersInfo("%".$currentUsername."%");
//echo "users found=".count($users);

//connect to global database to get the remaining user info
include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
$usersFromGlobal = retrieveUserInfoFromGlobalDatabase("%".$_SESSION['searchUserName']."%");

//return to local database for any following queries
include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";

foreach($users as $currentUser)
{
	/* many users can be returned, since we have LIKE in the SQL query.
	Select the user that matches exactly the username */
	if($currentUsername==$currentUser->username)
	{
		$user = $currentUser;
		
		//set role and status taken from Global database
		foreach($usersFromGlobal as $userFromGlobal)
		{
			if($userFromGlobal->username==$user->username)
			{
				$user->role = $userFromGlobal->role;
				$user->status = $userFromGlobal->status;
				$user->clientName = $userFromGlobal->clientName;
				$user->productType = $userFromGlobal->productType;
				$user->consecutiveFailLoginAttempts = $userFromGlobal->consecutiveFailLoginAttempts;
			}
		}
		
		//after we found the user, also retrieve the email preference
		$user->emailPreference = retrieveEmailPreference($user->stateId);
		//var_dump($user->emailPreference);
		//store stateId in case it is updated
		$_SESSION['stateId'] = $user->stateId;
		//$user->emailPreference->printData();
		
		break;
	}
	
	
}

//$_SESSION['user'] = $user;

//$user->printData();


include $_SESSION['globalFilesLocation']."/users/updateUserDataDefaultValues.php";

//echo "runFromGlobalLocationQuotation = $runFromGlobalLocationQuotation";
?>

<h2><?php echo $_SESSION['usernameTab']; ?>=<?php echo $user->username?></h2>


<script>


$(document).ready(function(){

    $("#birthDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#birthDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#birthDate").datepicker('setDate', "<?php echo $user->birthDate; ?>");
    
    $("#licenseIssueDate").datepicker({ showOn: 'button', buttonImageOnly: true, buttonImage: './images/icon_cal.png', changeYear: true, yearRange: '-70:+10'});
    $('#licenseIssueDate').datepicker('option', 'dateFormat', 'dd-mm-yy');
    $("#licenseIssueDate").datepicker('setDate', "<?php echo $user->licenseIssueDate; ?>");
        
});

/* SET THE DEFAULT VALUES OF ALL THE SELECT TAGS DURING LOAD-RELOAD */
function setDefaultValues()
{
	setPreviousValue("<?php echo $gender_content; ?>", "gender");
	setPreviousValue("<?php echo $userRole_content; ?>", "userRole");
	setPreviousValue("<?php echo $producer_content; ?>", "producer");
	setPreviousValue("<?php echo $userStatus_content; ?>", "userStatus");
	setPreviousValue("<?php echo $productType_content; ?>", "productType");
	setPreviousValue("<?php echo $preferenceCode_content; ?>", "preferenceCode");
}
</script>


<form name="updateUserData" action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" method="POST" onSubmit="return checkUpdateUserDataForm('userAddresses')">
	<table>
		<input type="hidden" name="action" value="updateUserDataProcess">
		<input type="hidden" name="oldRole" value="<?php echo $user->role;?>">
		<input type="hidden" name="oldStatus" value="<?php echo $user->status;?>">
		<input type="hidden" name="oldProducer" value="<?php echo $user->producer;?>">
		<input type="hidden" name="searchClientName" value="<?php echo $_SESSION['searchClientName'];?>">
		<input type="hidden" name="consecutiveFailLoginAttempts" value="<?php echo $user->consecutiveFailLoginAttempts;?>">
		
		<p>
		<tr><td class="label"><?php echo $_SESSION['stateIdTab']; ?>:</td>
		<td class="input"><input type="text" name="stateId" id="stateId" size="30" value="<?php echo $user->stateId;?>" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['nameTab']; ?>:</td>
		<td class="input"><input type="text" name="firstName" id="firstName" size="30" value="<?php echo $user->firstName;?>" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['surname']; ?>:</td>
		<td class="input"><input type="text" name="lastName" id="lastName" size="30" value="<?php echo $user->lastName;?>" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['account']; ?>:</td>
		<td class="input"><?php echo $user->clientName; ?></td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['userRole'] ?>:</td>
		<td class="input">
		<?php
		//only administrator or higher can update a user ROLE, but not his
		//only SUPER can update the administrator role
		if($_SESSION['role']>=$USER_ROLE_ADMINISTRATOR && $_SESSION['role']>$user->role)
		{
			?>	
			<select name="userRole" id="userRole">
			<?php
		}
		//other roles cannot update 
		else
		{
		?>
			<input type="hidden" name="userRole" value="<?php echo $user->role; ?>" >
			<select name="userRole" id="userRole" disabled>
		<?php
		}
				foreach($_SESSION['userRoleOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<tr>
    <td class="label"><?php echo $_SESSION['producerTab'] ?>:</td>
		<td class="input"><select name="producer" id="producer">
				<?php
				foreach($_SESSION['yesNoSelectOptionOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['userStatus'] ?>:</td>
		<td class="input">
		<?php
		//only administrator can update a user status
		if($_SESSION['role']>=$USER_ROLE_ADMINISTRATOR && $_SESSION['role']>$user->role)
		{
			?>	
			<select name="userStatus" id="userStatus">
			<?php
		}
		//other roles cannot update 
		else
		{
		?>
			<input type="hidden" name="userStatus" value="<?php echo $user->status; ?>" >
			<select name="userStatus" id="userStatus" disabled>
		<?php
		}
				foreach($_SESSION['userStatusOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		
		<?php
		//only SUPER can update a user product
		if($_SESSION['role']>=$USER_ROLE_SUPER)
		{
			?>	
			<tr>
				<td class="label"><?php echo $_SESSION['product'] ?>:</td>
				<td class="input">
				<select name="productType" id="productType">
				<?php
				foreach($_SESSION['productTypes'] as $eachProductType)
				{
					?>
					 <option  value=<?php echo "$eachProductType" ?> ><?php echo "$eachProductType" ?> </option>
					 <?php
				 }
				 ?>
				</select>
				</td>
			</tr>
			<?php
		}
		?>
		
		</table>
		<?php
		
		displayAddresses($user->addresses);
		
		?>
		<table>
		<tr><td class="label"><?php echo $_SESSION['gender'] ?>:</td>
		<td class="input"><select name="gender" id="gender">
				<?php
				foreach($_SESSION['genderOptions'] as $option){
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['telephoneTab']; ?>:</td>
		<td class="input"><input type="text" name="telephone" id="telephone" size="30" value="<?php echo $user->telephone;?>" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['cellphoneTab']; ?>:</td>
		<td class="input"><input type="text" name="cellphone" id="cellphone" size="30" value="<?php echo $user->cellphone;?>" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['profession']; ?>:</td>
		<td class="input"><input type="text" name="profession" id="profession" size="30" value="<?php echo $user->profession;?>" /> </td></tr>
		
		<tr><td class="label">Email:</td>
		<td class="input"><input type="text" name="email" id="email" size="30" value="<?php echo $user->email;?>" /> </td></tr>
		
		<tr><td class="label"><?php echo $_SESSION['emailPreference'] ?>:</td>
		<td class="input"><select name="preferenceCode" id="preferenceCode">
				<?php
				foreach($_SESSION['emailPreferenceOptions'] as $option)
				{
					?>
					 <option  value=<?php echo "$option->value" ?> ><?php echo "$option->name" ?> </option>
					 <?php
				 }
				 ?>
			</select>
		</td>
		</tr>
		
		<tr><td class="label"><?php echo $_SESSION['birthDate']; ?>:</td>
		<td class="input">
		<input type="text" style="width: 80px;" id="birthDate" name="birthDate" />
		</td>
		
		<tr><td class="label"><?php echo $_SESSION['licenseDateTab']; ?>:</td>
		<td class="input">
		<input type="text" style="width: 80px;" id="licenseIssueDate" name="licenseIssueDate" />
		</td>
		
		<?php
		//allow for update when:
		//1. user is updating his own data
		//2. logged-in user is ADMINISTRATOR or SUPER
		if($_SESSION['username']==$currentUsername || $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR )
		{
		?>
			<tr><td class="label">&nbsp</td><td class="input">&nbsp</td></tr>
			<tr>
			<td class="label"></td><td class="input"><INPUT type="submit" value=<?php echo $_SESSION['updateButton']; ?> size="15" ></td>
			</tr>
		<?php
		}
		?>
		</table>
		</form>
		
		<table>
		<tr>
			<td width="20%">
				<div id="message" style="color:#00FF00"></div>
			</td>
			<td>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="./office.php" id="newContractForm" method="post" style="display: none;">
				<input type="text" name="action" value="newContractForm" />
			</form>
			<a href="javascript:;" onclick="javascript: if(checkUpdateUserDataFormForAddContractValidity()){ document.getElementById('newContractForm').submit()}"><?php echo $_SESSION['newContract']; ?></a>
			<!-- following code is not working very well. update is done only when alert()exists.
			<a href="javascript:;" onclick="javascript: if(checkUpdateUserDataFormForAddContractValidity()){ updateUserDataAjax();document.getElementById('newContractForm').submit()}"><?php echo $newContract; ?></a>
			-->
			</td>
		</tr>
		
		<tr>
			<td>
			<!-- create hidden forn, to send the action as a POST -->
			<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="retrieveAllUserQuotes" method="post" style="display: none;">
				<input type="text" name="action" value="displayPreviousQuotes" />
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('retrieveAllUserQuotes').submit()"><?php echo $_SESSION['showQuotes']; ?></a>
			<!--<a href="./quotation.php?action=displayPreviousQuotes&username=<?php echo $user->username;?>" title="retrieve user quotes">Retrieve User Quotes</a>-->
			</td>
		      <td>
		      </td>
		</tr>
		
		<tr>
		<td>
			<?php
			//allow for password update when:
			//1. user is updating his own password
			//2. logged-in user is ADMINISTRATOR
			if($_SESSION['username']==$currentUsername || $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR )
			{
				?>
			<!-- create hidden forn, to send the action as a POST-->
			<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="updatePasswordFormInUpdateUserDataForm" method="post" style="display: none;">
				<input type="text" name="action" value="updatePasswordForm" />				
			</form>
			<a href="javascript:;" onclick="javascript: document.getElementById('updatePasswordFormInUpdateUserDataForm').submit()"><?php echo $_SESSION['updatePassword']; ?></a>
			<?php
			}
			?>
		</td>
		<td>
		</td>
		</tr>
		
		</p>
	</table>


	