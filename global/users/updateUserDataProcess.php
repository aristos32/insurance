<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "5")
</script>

<?php

//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//echo "actionRead=$actionRead";
//process only real requests - not language change
if($actionRead!='')
{
	//echo "updating user";
	
	$user = new user();
	$user->username = $_SESSION['searchUserName'];
	$user->stateId = @$_POST['stateId'];
	$user->oldStateId = @$_SESSION['stateId'];
	$user->gender = @$_POST['gender'];
	$user->role = @$_POST['userRole'];
	$user->status = @$_POST['userStatus'];
	$user->productType = @$_POST['productType'];
	$user->producer = @$_POST['producer'];
	$user->firstName = @$_POST['firstName'];
	$user->lastName = @$_POST['lastName'];
	$user->telephone = @$_POST['telephone'];
	$user->cellphone = @$_POST['cellphone'];
	$user->profession = @$_POST['profession'];
	$user->email = @$_POST['email'];
	$user->consecutiveFailLoginAttempts = @$_POST['consecutiveFailLoginAttempts'];
	
	$user->emailPreference = new emailPreference();
	$user->emailPreference->stateId = $user->stateId;
	$user->emailPreference->preferenceCode = $_POST['preferenceCode'];
	$user->emailPreference->preferenceFrequency = $FREQUENCY_MONTHLY;//for now, set default as monthly
		
	//read previous values for history
	$oldRole = $_POST['oldRole'];
	$oldStatus = $_POST['oldStatus'];
	$oldProducer = $_POST['oldProducer'];
	
	$searchClientName = $_POST['searchClientName'];
	
	//read fields that come as arrays
	$addressTypes = @$_POST['addressType'];
	$addressIds = @$_POST['addressId'];
	$streets = @$_POST['street'];
	$codes = @$_POST['code'];
	$citys = @$_POST['city'];
	$countrys = @$_POST['country'];
	
	if($user->stateId!="")
	{
		if(isset($addressTypes))
		{
			//set all array elements
			foreach($addressTypes as $a => $b)
			{
		  		$localAddress = new ownerAddress();
		  		$localAddress->stateId = $user->stateId;
		  		$localAddress->addressType = $addressTypes[$a];
				$localAddress->addressId = $addressIds[$a];
				$localAddress->street = $streets[$a];
				$localAddress->areaCode = $codes[$a];
				$localAddress->city = $citys[$a];
				$localAddress->country = $countrys[$a];
				
				$user->addresses[] = $localAddress;
		  		
			}
		}
	}
	
	$user->birthDate = @$_POST['birthDate'];
	$user->licenseIssueDate = @$_POST['licenseIssueDate'];
	
	//echo "ROLE is $user->role <br>";
	
	//$user->printData();
	include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
	updateUserDataInGlobalDatabase($user);
	
	//connect to appropriate database to add the user and all related information
	$clientFilesLocation =  "../clients/".$searchClientName;
	include $clientFilesLocation."/database/localDatabaseConstants.php";
	$localError = updateUserData($user);
	
	//we always have an owner, so we need to update him
	$owner = fillOwnerClass($user);
	updateOwnerData($owner);
	
	updateEmailPreference($user->emailPreference);
	
	//add to history table only on success
	$localHistory = new history();
	$localHistory->username = $_SESSION['username'];
	$localHistory->type = $HISTORY_TYPE_USER;
	$localHistory->parameterName = "username";
	$localHistory->parameterValue = $user->username;
	$localHistory->note = "$localHistory->username : Updated information for User $user->username. Changed[role,status,producer] from [$oldRole,$oldStatus,$oldProducer] to [$user->role,$user->status,$user->producer]";
	insertNewHistory($localHistory);
	
	//insert also owner and all addresses only when stateId is filled      
	if($user->stateId != '' )
	{
	    //insert owner - only because addresses is dependend on stateId
	    $owner = fillOwnerClass($user);
	    if(checkEntryExistsInTable('owner', 'stateId', $owner->stateId, '', '')==0){
	    	$insertOwnerError = insertOwner($owner);
	    }
	    //insertOwner($owner);
	      
	 	//delete all previous address
		deleteFromTable('owneraddress', 'stateId', $user->stateId);
		
		//insert again new/updated addresses
		foreach($user->addresses as $eachAddress)
		{
			//$ownerAddress = fillOwnerAddressClass($owner);
			//$ownerAddress->printData();
			insertOwnerAddress($eachAddress);
		}
	}
	
	//return to local database for any following queries
	//ATTENTION - use include instead of require_once to override previous values
	include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";
	
	/* set back the session variable to the login user */
	//$_SESSION['searchUserName'] = $_SESSION['username'];
	
	//$affectedRows=1 => data updated
	//$affectedRows=-1 => no data had to be updated					
}
?>
User data updated correctly. 
<!-- create hidden forn, to send the action as a POST -->
<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="updateUserDataForm" method="POST" style="display: none;">
	<input type="text" name="action" value="updateUserDataForm" />
	<input type="text" name="searchClientName" value="<?php echo $searchClientName;?>" />
</form>
<a href="javascript:;" onclick="javascript: document.getElementById('updateUserDataForm').submit()"><?php echo $_SESSION['continue']; ?></a>

					

