<!-- Script to call following the HTML for both of the above to initialize a Tab Menu instance -->
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("colortab", "5")
</script>

<?php
$allEmailsIds = retrieveAllUserEmailsIds();
$allEmails = $allEmailsIds->allEmails;
$allStateIds = $allEmailsIds->allStateIds;
//echo $allEmails;
//echo $allStateIds;

?>

<table>
	<form name="findUserForm" action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" method="POST" onSubmit="return checkFindUserFormForm()">
	<input type="hidden" name="action" value="userFindMenuProcess">
		<!-- USERNAME -->
		<tr>
			<td class="label"><label for="userName"><?php echo $_SESSION['usernameTab']; ?>:</label></td>
			<td class="input"><input type="text" name="userName" id="userName" size="30" value="" />
		</tr>
		<tr>
			<td class="label"></td>
			<!-- CONTINUE BUTTON -->						
			<td class="input"><input type="submit" name="send" class="button" value="<?php echo $_SESSION['find'];?>" size="30" /></td>
		</tr>
	</form>	
	<?php
	//only administrators can create new users
	if($_SESSION['role'] >= $USER_ROLE_ADMINISTRATOR)
	{
		?>
		<tr>
		<td>
		<!-- create hidden forn, to send the action as a POST -->
				<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="newUserForm" method="post" style="display: none;">
					<input type="hidden" name="action" value="newUserForm" />
				</form>
				<a href="javascript:;" onclick="javascript: document.getElementById('newUserForm').submit()"><?php echo $_SESSION['createUser']; ?></a>
		</td>
		</tr>
		<tr>
		<td> <a href="javascript:;" onclick="send_email('<?php echo $allEmails; ?>', '<?php echo $_SESSION['globalFilesLocation']; ?>', '<?php echo $_SESSION['SYSTEM_EMAIL']; ?>', '<?php echo $_SESSION['lang']; ?>', '<?php echo $_SESSION['clientName']; ?>', '<?php echo $allStateIds; ?>' )"><?php echo $_SESSION['sendNewsLetter']; ?></a>
		</tr>
	<?php
	}
	?>
</table>

