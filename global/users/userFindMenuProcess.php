<?php
//no direct access to file allowed
defined('_INC') or die('Direct access not premitted');

//on language change, POST are not send. So, use ones in session
if(isset($_POST['userName']))
	$_SESSION['searchUserName'] = $_POST['userName'];
	
	
/* FIND USER */
if($_SESSION['searchUserName'] != '')
{
	$users = array();
	
	//connect to global database to get the remaining user info
	include $_SESSION['globalFilesLocation']."/database/globalDatabaseConstants.php";
	$usersFromGlobal = retrieveUserInfoFromGlobalDatabase("%".$_SESSION['searchUserName']."%");
	
	$distinctClientNames = array();
	
	//get all different client names
	$_SESSION['accounts'] = readAccounts($_SESSION['globalFilesLocation'].'/configurationFiles/accounts.xml');

	foreach($_SESSION['accounts'] as $eachAccount)
		$distinctClientNames[] = $eachAccount->clientName;
	//$distinctClientNames = retrieveDistinctValues('clientName', 'systemuser' );
	//print_r($distinctClientNames);
	unset($_SESSION['accounts']);//not needed any more
	
	//administrator can see only users in this account
	if($_SESSION['role'] == $USER_ROLE_ADMINISTRATOR )
	{
		//use local adminstrator database
		include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";
		$users = retrieveUsersInfo("%".$_SESSION['searchUserName']."%");
	}
	//SUPER can see all users from all accounts
	else if($_SESSION['role'] == $USER_ROLE_SUPER )
	{
		//login to all client databases and retrieve users
		foreach($distinctClientNames as $eachClientName)
		{
			$clientFilesLocation =  "../clients/".$eachClientName;
			include $clientFilesLocation."/database/localDatabaseConstants.php";
			//append arrays
			$users = array_merge($users, retrieveUsersInfo("%".$_SESSION['searchUserName']."%"));
			$users = array_unique($users);
		}
		
		
	}
	else
	{
		echo "INVALID CASE. <br/>";
	}
	
	//create a list of all the emails
	$allEmails = '';
	$allStateIds = '';
	foreach($users as $eachUser)
	{
		//we have several contracts for same client. so we need to take the client email only once.
		//1. email must have some value, not empty
		//2. email must not exist in the $allMails list already because maybe same user has 2 accounts
		if($eachUser->email != '' && strpos($allEmails,$eachUser->email)==false )
		{
			$allEmails = $allEmails.",".$eachUser->email;
			$allStateIds = $allStateIds.",".$eachUser->stateId;
		}
	}
	//echo $allEmails;
	//echo $allStateIds;
	
	//return to local database for any following queries
	include $_SESSION['clientFilesLocation']."/database/localDatabaseConstants.php";
		
	/* CASE 1 : MORE THAN 0 USERS FOUND */
	if(count($users) > 0 )
	
	{
	?>
	
		<h2><?php echo $_SESSION['usersTab']; ?></h2>	
	
		<table width="90%" border="0" cellspacing="1" cellpadding="3">
		<tr>
			<td width="15%"><b><?php echo $_SESSION['usernameTab']; ?></td>
			<td width="20%"><b><?php echo $_SESSION['proposerfullName']; ?></td>
			<td width="15%"><b>Email</td>
			<td width="10%"><b><?php echo $_SESSION['product']; ?></td>
			<td width="10%"><b><?php echo $_SESSION['userRole']; ?></td>
			<td width="10%"><b><?php echo $_SESSION['userStatus']; ?></td>
			<?php
			if($_SESSION['role'] >= $USER_ROLE_SUPER )
			{
				?>
				<td width="15%"><b><?php echo $_SESSION['client']; ?></td>
				<?php
			}
			?>
			<td width='5%' ><b><?php echo $_SESSION['delete']; ?></td>
		</tr>
		<?php
		foreach($users as $user)
		{
			//set variables than only exist in GLOBAL Database
			foreach($usersFromGlobal as $userFromGlobal)
			{
				if($userFromGlobal->username==$user->username)
				{
					$user->role = $userFromGlobal->role;
					$user->productType = $userFromGlobal->productType;
					$user->status = $userFromGlobal->status;
					$user->clientName = $userFromGlobal->clientName;
					//echo "$userFromGlobal->username: role is $user->role <br>";
				}
			}
		?>					
		<tr>
			<td>
				<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
				<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="<?php echo $user->username;?>" method="POST" style="display: none;">
					<input type="text" name="action" value="updateUserDataForm" />
					<input type="text" name="searchUserName" value="<?php echo $user->username;?>" />
					<input type="text" name="searchClientName" value="<?php echo $user->clientName;?>" />
				</form>
				<a href="javascript:;" onclick="javascript: document.getElementById('<?php echo $user->username;?>').submit()"><?php echo $user->username;?></a>
			</td>
			
			<td><?php echo $user->lastName." ".$user->firstName;?></td>
			<td ><?php echo $user->email;?></td>
			<td ><?php echo $user->productType;?></td>
			<td ><?php
				foreach($_SESSION['userRoleOptions'] as $option)
				{
					if($option->value == $user->role)
						echo $option->name;
					
				 }
				  ?>
			</td>
			<td ><?php echo $user->status;?></td>
			<?php
			if($_SESSION['role'] >= $USER_ROLE_SUPER )
			{
				?>
				<td ><?php echo $user->clientName;?></td>
				<?php
			}
			?>
			<td>
			<?php
			//echo "user->role=".$user->role.", session role = ".$_SESSION['role']." <br>";
			//1. SUPER users cannot be deleted
			//2. ADMIN or SUPER users can delete all other 'LOWER' rank users
			//3. customers/employee/anonumous/agents users cannot be delete
			if(($user->role <= $USER_ROLE_ADMINISTRATOR) && $_SESSION['role']>=$USER_ROLE_ADMINISTRATOR && $_SESSION['role']>$user->role )
			{ 
				$deleteName = "delete_".$user->username; ?>
				<!-- create hidden forn, to send the action as a POST. Each form has a different name, since in the loop-->
				<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="<?php echo $deleteName;?>" method="POST" style="display: none;">
					<input type="text" name="action" value="deleteUser" />
					<input type="text" name="deleteUserName" value=<?php echo $user->username;?> />
				</form>
				<a href="javascript:;" onclick="javascript: if (confirm('<?php echo $_SESSION['areYouSureYouWantToDeleteThisUser'];?>')) document.getElementById('<?php echo $deleteName;?>').submit()"><?php echo $_SESSION['delete']; ?></a>
			
				<?php
			}
			?>
			</td>
		</tr>
		<?php
		}
		?>
		<tr>
		<td> <a href="javascript:;" onclick="send_email('<?php echo $allEmails; ?>', '<?php echo $_SESSION['globalFilesLocation']; ?>', '<?php echo $_SESSION['SYSTEM_EMAIL']; ?>', '<?php echo $_SESSION['lang']; ?>', '<?php echo $_SESSION['clientName']; ?>','<?php echo $allStateIds; ?>' )"><?php echo $_SESSION['sendNewsLetter']; ?></a>
		</tr>
		</table>
		<?php
	}
	/* CASE 2 : NO USERS FOUND */
	else
	{
		?>
		No users Found.
		<!-- create hidden forn, to send the action as a POST -->
		<form action="<?php if($runFromGlobalLocationQuotation==true) echo './quotation.php'; else echo './office.php'; ?>" id="administratorMenu" method="POST" style="display: none;">
		<input type="text" name="action" value="userFindMenu" />
		</form>
		<a href="javascript:;" onclick="javascript: document.getElementById('administratorMenu').submit()">Continue</a>

		<?php
	}
	?>
<?php
}
	
?>