<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta name="Description" content="Information architecture, Web Design, Web Standards." />
<meta name="Keywords" content="your, keywords" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="Distribution" content="Global" />
<meta name="Author" content="Erwin Aligam - ealigam@gmail.com" />
<meta name="Robots" content="index,follow" />

<link rel="stylesheet" href="images/HarvestField.css" type="text/css" />

<title>Online Insurance App</title></head>

<body>
<!-- wrap starts here -->
<div id="wrap">

	<!--header -->
	<div id="header">			
				
		<h1 id="logo-text"><a href="index.php?action=homePage" title="">Online-Insurance<span>-App</span></a></h1>		
		<p id="slogan">all it's on the network</p>		
			
		<div id="header-links">
			<p>
				<a href="./index.php?action=homePage">Home</a> | 
				<a href="./index.php?action=contactUs">Contact</a> 			
			</p>		
		</div>				
				
	<!--header ends-->					
	</div>
		
	<!-- navigation starts-->	
	<div  id="nav">
		
		<!--<div id="light-brown-line"></div>	-->
		
		<ul>
			<li><a href="index.php?action=homePage">Home</a></li>
			<li><a href="index.php?action=products">Products</a></li>
			<li><a href="index.php?action=crmInfo">CRM</a></li>
			<li><a href="index.php?action=quotationInfo">Quotation</a></li>
			<li><a href="index.php?action=quotationDemo">Live Quotation Demo</a></li>
			<li><a href="index.php?action=updates">Updates</a></li>
			<li><a href="index.php?action=aboutUs">About Us</a></li>		
			<li><a href="index.php?action=contactUs">Contact Us</a></li>
		</ul>
		
	<!-- navigation ends-->	
	</div>	
		
	<!-- content-wrap starts -->
	<div id="content-wrap">

	<?php
		require_once("./html/phpFunctions.php");
		$action = @$_GET['action']; 
		?>
		<div id="main">
			<?php
			switch($action)
			{
				case '':
				case 'homePage':
					include "./html/homePage.html";
					break;
				case 'contactUs':
					include "./html/contactUs.php";
					break;
				case 'updates':
					include "./html/updates.html";
					break;
				case 'products':
					include "./html/products.html";
					break;
				case 'crmInfo':
					include "./html/crmInfo.html";
					break;
				case 'quotationInfo':
					include "./html/quotationInfo.html";
					break;
				case 'aboutUs':
					include "./html/aboutUs.html";
					break;
				case 'quotationDemo':
					include "./html/quotationDemo.php";
					break;
				case 'sendEmail':
					$visitorName = $_GET['visitorName'];
					$visitormail = $_GET['visitormail'];
					$message = $_GET['message'];
					$subject = $_GET['subject'];
					include "./html/sendEmail.php";
					break;
				default:
					include "./html/underConstruction.html";
					break;
			}
			?>
				
			
			
		<!-- main ends -->	
		</div>
		
		<div id="sidebar">
			
			<h3>Sidebar Menu</h3>
			<ul class="sidemenu">				
				<li><a href="index.php?action=homePage">Home</a></li>
				<li><a rel="nofollow" href="http://www.bullionvault.com/#aristos32">BullionVault</a></li>
			</ul>	
				
			<h3>Links</h3>
			<ul class="sidemenu">
				<li><a href="http://www.expatfinder.com/" target="_blank" title="Expat Guide"> www.ExpatFinder.com - Relocation Services, Insurance, Finance and more</a></li>
				<li><a href="http://www.fairinvestment.co.uk/insurance.aspx" target="_blank" title="Fair Investment">Fair Investment</a></li>
				<li><a href="http://www.mif.org.cy/rocysite/home.html" target="_blank" title="MIF Website">MIF</a></li>
				<li><a rel="nofollow" href="http://www.bullionvault.com/#aristos32">BullionVault</a></li>
			</ul>
			
			<h3>Our Sponsors</h3>
            <ul class="sidemenu">
            	 <li><a rel="nofollow" href="http://www.bullionvault.com/#aristos32">BullionVault</a></li>
                <li><a href="http://www.cyprus-insurances.com" title="cyprus insurances" target="_blank">
                Cyprus Insurances</a></li>
            </ul>
				
			<h3>Wise Words</h3>
			<p>&quot;Every adversity, every failure, every heartache 
			carries with it the seed on an equal or greater benefit.&quot; </p>
					
			<p class="align-right">- Napoleon Hill</p>
					
		<!-- sidebar ends -->		
		</div>
		
	<!-- content-wrap ends-->	
	</div>
	
	
	<!-- footer starts -->
	<div id="footer">		
			
		<p>
		    &copy; 2013 <strong>Online-Insurance-Apps</strong>

            &nbsp;&nbsp;&nbsp;&nbsp;

		    <a href="http://www.bluewebtemplates.com/" title="Website Templates">website templates</a> by <a href="http://www.styleshout.com/">styleshout</a>

            &nbsp;&nbsp;&nbsp;&nbsp;

		    <a href="index.php?action=homePage">Home</a> |
            <a href="http://validator.w3.org/check?uri=referer">XHTML</a> |
		    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>
   	    </p>
   	    <a rel="nofollow" href="http://www.bullionvault.com/#aristos32"><img src="http://banners.bullionvault.com/en/BV_468x60_v1.gif" title="BullionVault" alt="BullionVault" border="0" width="468" height="60"></a> 

	<!-- footer ends -->		
	</div>	

<!-- wrap ends here -->
</div>

</body>
</html>
